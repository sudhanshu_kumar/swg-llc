var effect_duration = 250;
var effect_rotation;
var effect_rotation_interval = '2000'; // Setting a 5 second interval for automate move and fed effects

function make_it_visible(tDiv){
	document.getElementById(tDiv).style.visibility = 'hidden';
}

function show(whichOne){
	var which_img = $('which_img')

	if(whichOne == which_img.value){ return false;}
	var vDiv = "div"+whichOne;
	var tDiv = "div"+which_img.value
    var cDiv = "sdiv"+whichOne ;
//    var pDiv = "sdiv"+ (whichOne - 1) ;
	which_img.value = whichOne;
	var image2fed = $(vDiv);
	var img2fedFx = new Fx.Style(image2fed, 'opacity', {
			wait: false,
			duration: effect_duration + 200,
			transition: Fx.Transitions.Quart.easeInOut
		});

	img2fedFx.start(0, 1);
	image2fed.style.visibility = 'visible';
//    alert(whichOne) ;
//    alert(sDiv) ;
    var cDivObj = document.getElementById(cDiv) ;
    var vDivObj = document.getElementById(vDiv) ;
//    var pDivObj = document.getElementById(pDiv) ;
//	sDiv.style.background = 'url("../images/referenzen_li_hover_bg.jpg") no-repeat scroll right center transparent' ;

    document.getElementById("sdiv1").className = 'scrollDiv' ;
    document.getElementById("sdiv2").className = 'scrollDiv' ;
    document.getElementById("sdiv3").className = 'scrollDiv' ;
    document.getElementById("sdiv4").className = 'scrollDiv' ;
   // document.getElementById("sdiv5").className = 'scrollDiv' ;

//    var slDiv = document.getElementById("target1") ;
	var image2vanish = $(tDiv);
	var img2vanishFx = new Fx.Style(image2vanish, 'opacity', {
			wait: false,
			duration: effect_duration + 200,
			transition: Fx.Transitions.Quart.easeInOut
		});

	img2vanishFx.start(1, 0);
    cDivObj.className = 'selected' ;
//    slDiv.style.top = '71px';
//    slDiv.style.right = '0px';
    vDivObj.className = '' ;


}

function move_down(top_end, image2show, auto_move){
	var target = $('target');
	var box = $('box');
	var fx = new Fx.Styles(target, {duration: effect_duration, wait: true});

	var transition = 'Cubic';
	var ease = 'easeIn';
	fx.options.transition = Fx.Transitions[transition][ease];
	transition = 'Fx.Transitions.' + transition + '.' + ease;

    var slDiv = document.getElementById("target") ;

if(auto_move != 1){
		fx.start({
			'top': top_end,
			'right': 0
		});
		slDiv.style.top = top_end;
		show(image2show);

		clearInterval(effect_rotation);
		effect_rotation = setInterval('move_down(72, 2, 1)', effect_rotation_interval);
	}
	else{
		var which_img = $('which_img');
		
//		alert("A: "+which_img);

		if(which_img.value == 1){

        slDiv.style.top = '86px';

//			fx.start({
//				'top': 71,
//				'right': 0
//			});
			show(2);
			which_img.value = 2;
		}
		else if(which_img.value == 2){
            slDiv.style.top = '149px';
//			fx.start({
//				'top': 120,
//				'right': 0
//			});
			show(3);
			which_img.value = 3;
		}
		else if(which_img.value == 3){
            slDiv.style.top = '212px';
//			fx.start({
//				'top': 169,
//				'right': 0
//			});
			show(1);
			which_img.value = 1;
		}
		else if(which_img.value == 4){
            slDiv.style.top = '23px';
//			fx.start({
//				'top': 218,
//				'right': 0
//			});
			show(1);
			which_img.value = 1;
		}
/*		else if(which_img.value == 5){
            slDiv.style.top = '23px';
//			fx.start({
//				'top': 23,
//				'right': 0
//			});
			show(1);
			which_img.value = 1;
		}
*/		
	}
}


function start_auto_moving(){
	effect_rotation = setInterval('move_down(72, 2, 1)', effect_rotation_interval);
}

window.addEvent('domready', function(){
	start_auto_moving();
	var mySlide = new Fx.Slide('log_tog');
	mySlide.hide();

	$('toggle').addEvent('click', function(e){
		e = new Event(e);
		mySlide.toggle();
		e.stop();
	});
});