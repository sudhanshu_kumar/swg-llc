/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
  function openCloseDiv(showDiv,hideDiv1,hideDiv2){
    if(showDiv =="closeDivId"){
      $('#'+showDiv).show();
      clearDivElement(hideDiv1);
      clearDivElement(hideDiv2);
      $('#mode').val('close');
      $('#'+hideDiv1).hide();
      $('#'+hideDiv2).hide();
      $('#error_slt_user').html('');
      $('#error_txt_support_assign_cmt').html('');
      $('#error_txt_support_respond_cmt').html('');
    }else if(showDiv == "assignToDivId"){
      $('#'+showDiv).show();
      clearDivElement(hideDiv1);
      clearDivElement(hideDiv2);
      $('#mode').val('assign');
      $('#'+hideDiv1).hide();
      $('#'+hideDiv2).hide();
      $('#error_txt_support_close_cmt').html('');
      $('#error_txt_support_respond_cmt').html('');
    }else if(showDiv == "respondDivId"){
      $('#'+showDiv).show();
      clearDivElement(hideDiv1);
      clearDivElement(hideDiv2);
      $('#mode').val('respond');
      $('#'+hideDiv1).hide();
      $('#'+hideDiv2).hide();
      $('#error_slt_user').html('');
      $('#error_txt_support_assign_cmt').html('');
      $('#error_txt_support_close_cmt').html('');
    }
  }
  function clearDivElement(divId){
    $('#'+divId).children().find('select,textarea').each(function(){
      $(this).val('');
    });

  }

  function validateRequestForm(){
    var err = 0;
    if($('#mode').val() == 'close'){

      if($('#txt_support_close_cmt').val() == ''){
        err = err +1;
        $('#error_txt_support_close_cmt').html('Please enter comments');
      }else{
        $('#error_txt_support_close_cmt').html('');
      }
    }else if($('#mode').val() == 'assign'){
      if($('#slt_user').val() == ''){
        err = err +1;
        $('#error_slt_user').html('Please select Support User');
      }else{
        $('#error_slt_user').html('');
      }

      if($('#txt_support_assign_cmt').val() == ''){
        err = err +1;
        $('#error_txt_support_assign_cmt').html('Please enter comments');
      }else{
        $('#error_txt_support_assign_cmt').html('');
      }
    }else if($('#mode').val() == 'respond'){
      if($('#txt_support_respond_cmt').val() == ''){
        err = err +1;
        $('#error_txt_support_respond_cmt').html('Please enter comments');
      }else{
        $('#error_txt_support_respond_cmt').html('');
      }

    }else{
      $('#error_txt_support_close_cmt').html('');
      $('#error_txt_support_assign_cmt').html('');
      $('#error_txt_support_respond_cmt').html('');
      $('#error_slt_user').html('');
    }
    if(err == 0){
      return true;
    }else{
      return false;
    }
}

