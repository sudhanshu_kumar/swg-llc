function setSession(timeOut)
{
    setInterval("getTime()",timeOut);
}

function getTime()
{
        document.getElementById("sessionTimeOut").style.display='block';
        window.clearInterval();
}

//session reset function created by vineet
function resetSession(url,timeOut)
{
    $.post(url,'', function(data){
        if(data == 'logout')
        {
            document.getElementById("sessionTimeOut").innerHTML='Your session has been expired. Please login again to start application.';
        }
        else
        {
            document.getElementById("sessionTimeOut").style.display='none';
            setInterval("getTime()",timeOut);
        }
    });
    return false;
}