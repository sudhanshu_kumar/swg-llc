function instructionPopup(url)
{
    window.open(url, 'popUpWindow', 'width=610,height=600,left=320,top=0,scrollbars=yes')
    return false;
}

function ajax_call(divId, uri, formId)
{
    $.post(uri,$("#"+formId).serialize(), function(data){
        $('#'+divId).html(data);
    });

    return false;
}


function validatePhone(phoneNumber) {
    //      var reg = /^((\+)?(\d{2}[-]))?(\d{10}){1}?$/;
    //var reg = /^((\+)?(\d{2}))?(\d{10-15}){1}?$/;
    var reg = /^[0-9 [\]+()-]{8,20}?$/;
    if(reg.test(phoneNumber) == false) {
      return true;
    }

    return false;
  }

 function compare_date(date){
      var today = new Date;
      var entered_date = date.split("-") ;
      var dob = new Date;
      dob.setDate(entered_date[0]);
      dob.setMonth(entered_date[1]-1);
      dob.setFullYear(entered_date[2]);
      if (dob >= today) {
        return false;
      }
      else{
        return true;
      }

}


 function validateAddress(address) {
   if((address.length > 255)) {
      return true;
    }

    return false;
  }

  function ajax_paginator(divId, uri)
{
    $.post(uri,$("#search").serialize(), function(data){
        $('#'+divId).html(data);
    });
    return false;
}

/**
 * [WP:081] => CR:117
 */
function validateMultipleRadio(formId){
    if(formId == undefined){
        var totalInput = $('input:radio').length;
    }else{
        var totalInput = $('#'+formId+' input:radio').length;
    }
    var checkedValues = '';
    if(totalInput > 0){
        $(":input:radio:checked").each(function(){
            checkedValues = checkedValues + ',' + this.value;
        })
    }
    return checkedValues;
}

function validateMultipleCheckBox(formId){
    if(formId == undefined){
        var totalInput = $('input:checkbox').length;
    }else{
        var totalInput = $('#'+formId+' input:checkbox').length;
    }
    var checkedValues = '';
    if(totalInput > 0){
        $(":input:checkbox:checked").each(function(){
            checkedValues = checkedValues + ',' + this.value;
        })
    }
    return checkedValues;
}


