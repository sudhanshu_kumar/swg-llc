
ALTER TABLE `applicant_vault` ADD `user_id` INT( 11 ) NULL DEFAULT NULL AFTER `agent_proof` ;

ALTER TABLE `applicant_vault` ADD INDEX ( `user_id` );

ALTER TABLE `applicant_vault` ADD FOREIGN KEY ( `user_id` ) REFERENCES `ip4m`.`sf_guard_user` (`id`);

update `applicant_vault` set `user_id` = `created_by`;