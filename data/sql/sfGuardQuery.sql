-- phpMyAdmin SQL Dump
-- version 2.11.3deb1ubuntu1.3
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 13, 2010 at 11:04 AM
-- Server version: 5.0.51
-- PHP Version: 5.2.4-2ubuntu5.10

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `pay4meInt`
--

-- --------------------------------------------------------

--
-- Table structure for table `sf_guard_group`
--

CREATE TABLE IF NOT EXISTS `sf_guard_group` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) default NULL,
  `description` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `sf_guard_group`
--

INSERT INTO `sf_guard_group` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'portal_admin', 'Portal Admin', '2010-05-28 18:22:02', '2010-05-28 18:22:02'),
(2, 'admin', 'Admin', '2010-05-28 18:22:48', '2010-05-28 18:22:48'),
(3, 'payment_group', 'payment_group', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `sf_guard_group_permission`
--

CREATE TABLE IF NOT EXISTS `sf_guard_group_permission` (
  `group_id` int(11) NOT NULL default '0',
  `permission_id` int(11) NOT NULL default '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY  (`group_id`,`permission_id`),
  KEY `sf_guard_group_permission_permission_id_sf_guard_permission_id` (`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sf_guard_group_permission`
--

INSERT INTO `sf_guard_group_permission` (`group_id`, `permission_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2010-05-28 18:25:12', '2010-05-28 18:25:12'),
(2, 2, '2010-05-28 18:26:04', '2010-05-28 18:26:04'),
(3, 3, '2010-05-28 18:26:04', '2010-05-28 18:26:04');

-- --------------------------------------------------------

--
-- Table structure for table `sf_guard_permission`
--

CREATE TABLE IF NOT EXISTS `sf_guard_permission` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) default NULL,
  `description` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `sf_guard_permission`
--

INSERT INTO `sf_guard_permission` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'portal_admin', 'Portal Admin', '2010-05-28 18:25:12', '2010-05-28 18:25:12'),
(2, 'admin', 'Admin', '2010-05-28 18:26:04', '2010-05-28 18:26:04'),
(3, 'payment_user', 'payment_user', '2010-05-28 18:26:04', '2010-05-28 18:26:04');

-- --------------------------------------------------------

--
-- Table structure for table `sf_guard_user`
--

CREATE TABLE IF NOT EXISTS `sf_guard_user` (
  `id` int(11) NOT NULL auto_increment,
  `username` varchar(128) NOT NULL,
  `algorithm` varchar(128) NOT NULL default 'sha1',
  `salt` varchar(128) default NULL,
  `password` varchar(128) default NULL,
  `is_active` tinyint(1) default '1',
  `is_super_admin` tinyint(1) default '0',
  `last_login` datetime default NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `username` (`username`),
  KEY `is_active_idx_idx` (`is_active`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `sf_guard_user`
--

INSERT INTO `sf_guard_user` (`id`, `username`, `algorithm`, `salt`, `password`, `is_active`, `is_super_admin`, `last_login`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'sha1', 'd693cbac2f2984cc9f395f0f436ac5a9', '904b4a58d90dde989b67fe9e27089f3697888b7a', 1, 1, '2010-07-16 09:07:14', '2010-05-28 18:28:47', '2010-07-16 21:07:14'),
(2, 'user', 'sha1', '638fa059a6124d0be0e7be2761b81c19', 'b77a3dfb67d4f5575c656be0a2a79c6d272b3428', 1, 1, '2010-07-28 05:47:33', '2010-05-28 19:23:45', '2010-07-28 17:47:33'),
(3, 'FunSun', 'sha1', '7d62f13f8d51d101b0ebc67c9ad286fe', '00ee9cda60f1d2b32874719acbdda3b92f9f2d4f', 1, 0, '2010-06-14 18:01:15', '2010-06-14 12:59:00', '2010-06-14 18:01:15'),
(4, 'ankur', 'sha1', '7181eebf8f01f0190c4a86fd39df57d3', 'eb66f449929ba6175e938d1ed6218bd137d23d64', 1, 0, NULL, '2010-06-14 15:32:39', '2010-06-14 15:32:39'),
(5, 'ankur1', 'sha1', 'b3fd8930d564018cec375aa8f2f85b1a', '2c27bf7111568b666a973fbd8267c3423ad22d97', 1, 0, NULL, '2010-06-14 15:34:22', '2010-06-14 15:34:22'),
(6, 'ankur2', 'sha1', 'b8becaac5c658ed503818433172af98e', 'b9b75c8dd25d7f6011d6969e90b761b04687ed8d', 1, 0, NULL, '2010-06-14 15:34:55', '2010-06-14 15:34:55'),
(7, '1', 'sha1', 'ac8b74b0c91384313ae6bdf0c575f1ee', '89d1c87a6b45c7cccd6d7b0f65a21d376a3bf130', 1, 0, NULL, '2010-06-14 15:35:46', '2010-06-14 15:35:46'),
(8, 'abcd', 'sha1', 'cef30bf1fefe06048edbc30adb907ba9', 'bf2347afd13c55a4e44f94218cf0b824e6a8a9c3', 1, 0, NULL, '2010-06-14 18:39:08', '2010-06-14 18:39:08'),
(9, 'abcd1', 'sha1', '19a364c3a65e42f594a07aaa10637365', '2893a6b4b72f1fba7e1f824446546d307484f0cc', 1, 0, NULL, '2010-06-14 18:46:52', '2010-06-14 18:46:52'),
(10, 'abcd2', 'sha1', '007d093a4085080cdd89b14abd755e7e', '16b60cb56638d0cb10acd92e2ba50033e22923fb', 1, 0, NULL, '2010-06-14 18:47:01', '2010-06-14 18:47:01'),
(11, 'abcd3', 'sha1', '2ed78c0cc7b3f39d43964119cd2df08f', '3de6c6d98694be57aed353dfbdc5ec29323f062a', 1, 0, NULL, '2010-06-14 18:47:13', '2010-06-14 18:47:13'),
(12, 'abcd4', 'sha1', 'f72b2479c405ac26fb277d1332f771c8', 'c4024ff5b7709098b482491f0d06c5e590016d4d', 1, 0, NULL, '2010-06-14 18:47:20', '2010-06-14 18:47:20'),
(15, 'https://www.google.com/accounts/o8/id?id=AItOawnS3fdyz5NeZGbjqpw8oKmPRy3jRAFg4pk', 'sha1', 'cc97a80b11be9017496a41747ad80717', 'fea574e3ab280f4a4fe85774c9cc80ea60821695', 1, 0, '2010-06-14 21:17:58', '2010-06-14 21:17:58', '2010-06-14 21:17:58'),
(17, 'http://spark/~uprakash/ipay4me-openid/webdir/identity/uday123', 'sha1', 'dea1cb33c6130a7a097d5207b269031d', '757707e302a8ac2cad684a87097d77705f9ee02e', 1, 0, NULL, '2010-06-19 15:32:36', '2010-06-19 15:32:36'),
(18, 'http://spark/~uprakash/ipay4me-openid/webdir/identity/ashutosh', 'sha1', 'e06cd055f50f36c8bf4872a0d6bf0a7c', 'fa927e34cd8d98af24d32e317955f64f49c56146', 1, 0, NULL, '2010-06-19 17:11:25', '2010-06-19 17:11:25'),
(19, 'asdf', 'sha1', '0d2122156ac1cf3b1b251bdcd856ddc4', 'a1551ef334f4f2b6e7270991c008b3710beb1d75', 1, 0, NULL, '2010-06-23 14:58:48', '2010-06-23 14:58:48'),
(20, 'http%3A%2F%2Fspark%2F%7Euprakash%2Fipay4me-openid%2Fwebdir%2Fidentity%2Fashutosh', 'sha1', '0eb2787235fc5cd86f14caf6d971c3d9', 'b654c2af9ce3d58de99f1203e4bc251ac59dc673', 1, 0, NULL, '2010-06-23 15:02:33', '2010-06-23 15:02:33'),
(21, 'http://spark/~uprakash/ipay4me-openid/webdir/identity/anurag', 'sha1', 'b263a49361a79c4afc8bb9a75a2739bf', '66cc6b6e8ec729213125af13576c7e33e3b26f91', 1, 0, NULL, '2010-07-02 10:32:17', '2010-07-02 10:32:17'),
(22, '', 'sha1', '29d354394a3e178dacd5ba396e9c894d', '3ae9872e9d5283c0198ad893ca28f78d5cd67c6f', 1, 0, NULL, '2010-07-02 14:02:48', '2010-07-02 14:02:48'),
(23, 'http://spark/~uprakash/ipay4me-openid/webdir/identity/varun123', 'sha1', '89e27854f00f33103b739a91f188f98c', '916151538b503351fdc8a87230d8787adeff3f17', 1, 0, NULL, '2010-07-02 14:18:44', '2010-07-02 14:18:44'),
(24, 'http://spark/~uprakash/ipay4me-openid/webdir/identity/navin', 'sha1', '345602e6a7431ff9d72004d68216d78a', '40e9ac87d18c8573a726f19b018c292bacf27f36', 1, 0, NULL, '2010-07-06 10:55:48', '2010-07-06 10:55:48');

-- --------------------------------------------------------

--
-- Table structure for table `sf_guard_user_group`
--

CREATE TABLE IF NOT EXISTS `sf_guard_user_group` (
  `user_id` int(11) NOT NULL default '0',
  `group_id` int(11) NOT NULL default '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY  (`user_id`,`group_id`),
  KEY `sf_guard_user_group_group_id_sf_guard_group_id` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sf_guard_user_group`
--

INSERT INTO `sf_guard_user_group` (`user_id`, `group_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2010-05-28 18:28:47', '2010-05-28 18:28:47'),
(2, 2, '2010-05-28 19:23:45', '2010-05-28 19:23:45'),
(15, 3, '2010-06-14 21:17:58', '2010-06-14 21:17:58'),
(17, 3, '2010-06-19 15:32:36', '2010-06-19 15:32:36'),
(18, 3, '2010-06-19 17:11:25', '2010-06-19 17:11:25'),
(19, 3, '2010-06-23 14:58:48', '2010-06-23 14:58:48'),
(20, 3, '2010-06-23 15:02:33', '2010-06-23 15:02:33'),
(21, 3, '2010-07-02 10:32:17', '2010-07-02 10:32:17'),
(22, 3, '2010-07-02 14:02:48', '2010-07-02 14:02:48'),
(23, 3, '2010-07-02 14:18:44', '2010-07-02 14:18:44'),
(24, 3, '2010-07-06 10:55:49', '2010-07-06 10:55:49');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `sf_guard_group_permission`
--
ALTER TABLE `sf_guard_group_permission`
  ADD CONSTRAINT `sf_guard_group_permission_group_id_sf_guard_group_id` FOREIGN KEY (`group_id`) REFERENCES `sf_guard_group` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `sf_guard_group_permission_permission_id_sf_guard_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `sf_guard_permission` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `sf_guard_user_group`
--
ALTER TABLE `sf_guard_user_group`
  ADD CONSTRAINT `sf_guard_user_group_group_id_sf_guard_group_id` FOREIGN KEY (`group_id`) REFERENCES `sf_guard_group` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `sf_guard_user_group_user_id_sf_guard_user_id` FOREIGN KEY (`user_id`) REFERENCES `sf_guard_user` (`id`) ON DELETE CASCADE;
