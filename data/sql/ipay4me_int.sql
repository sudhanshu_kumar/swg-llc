ALTER TABLE `transaction` DROP FOREIGN KEY `transaction_user_id_sf_guard_user_id` ;

ALTER TABLE `transaction` DROP FOREIGN KEY `transaction_merchant_request_id_merchant_request_id` ;

ALTER TABLE `request` DROP FOREIGN KEY `request_transaction_id_transaction_id` ;

ALTER TABLE `request` DROP `transaction_id`  ;

ALTER TABLE `merchant_service` DROP FOREIGN KEY `merchant_service_merchant_id_merchant_id` ;

ALTER TABLE `merchant_request` DROP FOREIGN KEY `merchant_request_merchant_id_merchant_id` ;

ALTER TABLE `merchant_request` DROP FOREIGN KEY `merchant_request_merchant_service_id_merchant_service_id` ;

ALTER TABLE `merchant_request` DROP FOREIGN KEY `merchant_request_merchant_item_id_merchant_item_id` ;

ALTER TABLE `merchant_item` DROP FOREIGN KEY `merchant_item_merchant_service_id_merchant_service_id` ;

DROP TABLE `merchant_item`;

DROP TABLE `merchant_request`;

DROP TABLE `merchant_service`;

DROP TABLE `transaction`;

DROP TABLE `validation_rules`;

DROP TABLE `merchant_request_details`;

DROP TABLE `merchant`;

CREATE TABLE merchant (id BIGINT AUTO_INCREMENT, name VARCHAR(100) NOT NULL UNIQUE, merchant_code INT NOT NULL, merchant_key VARCHAR(255), notification_url VARCHAR(255) NOT NULL, homepage_url VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, deleted TINYINT(1) DEFAULT '0' NOT NULL, created_by INT, updated_by INT, PRIMARY KEY(id)) ENGINE = INNODB;

CREATE TABLE order_request (id BIGINT AUTO_INCREMENT, user_id INT, version VARCHAR(10) NOT NULL, merchant_id BIGINT NOT NULL, transaction_number BIGINT UNIQUE NOT NULL, name VARCHAR(100) NOT NULL, description TEXT NOT NULL, amount FLOAT(18, 2) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, deleted TINYINT(1) DEFAULT '0' NOT NULL, created_by INT, updated_by INT, UNIQUE INDEX unique_parameter_idx (transaction_number), INDEX merchant_id_idx (merchant_id), INDEX user_id_idx (user_id), PRIMARY KEY(id)) ENGINE = INNODB;

ALTER TABLE order_request ADD CONSTRAINT order_request_merchant_id_merchant_id FOREIGN KEY (merchant_id) REFERENCES merchant(id) ON DELETE CASCADE;

ALTER TABLE order_request ADD CONSTRAINT order_request_user_id_sf_guard_user_id FOREIGN KEY (user_id) REFERENCES sf_guard_user(id) ON DELETE CASCADE;

ALTER TABLE `request` ADD `order_id` BIGINT NOT NULL AFTER `id` ;

ALTER TABLE request ADD CONSTRAINT request_order_id_order_request_id FOREIGN KEY (order_id) REFERENCES order_request(id) ON DELETE CASCADE;