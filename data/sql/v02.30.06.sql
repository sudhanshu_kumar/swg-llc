## NIS QUERY ##
ALTER TABLE  `tbl_vap_application` ADD  `arrival_date` DATE NOT NULL AFTER  `paid_date`;


## IPAY4ME QUERY ##
INSERT INTO `ep_menu` (`name`, `label`, `parent_id`, `sfguardperm`, `url`, `eval`, `env`, `sequence`, `created_at`, `updated_at`, `deleted`, `created_by`, `updated_by`) VALUES
( 'Update VOAP Arrival Details', 'Update VOAP Arrival Details', 25, 'payment_user', 'visaArrival/searchFlightDetail', NULL, 'custom', 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, NULL);
