ALTER TABLE `support_refund_list`
  DROP `order_number`,
  DROP `amount`,
  DROP `refund_type`,
  DROP `refund_desc`;

ALTER TABLE `vault_refund_list` ADD `amount` FLOAT NOT NULL AFTER `order_number` ;

CREATE TABLE batch_process_request (id BIGINT AUTO_INCREMENT, vault_num BIGINT NOT NULL, order_number BIGINT NOT NULL, amount FLOAT(18, 2) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, created_by INT, updated_by INT, PRIMARY KEY(id)) ENGINE = INNODB;
CREATE TABLE batch_process_response (id BIGINT AUTO_INCREMENT, request_id BIGINT NOT NULL, response VARCHAR(20) NOT NULL, response_text VARCHAR(20) NOT NULL, authcode VARCHAR(20) NOT NULL, transactionid VARCHAR(20) NOT NULL, type VARCHAR(20) NOT NULL, orderid VARCHAR(20) NOT NULL, amount FLOAT(18, 2) NOT NULL, avsresponse VARCHAR(20) NOT NULL, cvvresponse VARCHAR(20) NOT NULL, response_code VARCHAR(20) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, created_by INT, updated_by INT, INDEX request_id_idx (request_id), PRIMARY KEY(id)) ENGINE = INNODB;

DROP TABLE `support_reason`
CREATE TABLE IF NOT EXISTS `support_reason` (
  `id` bigint(20) NOT NULL auto_increment,
  `key` varchar(255) default NULL,
  `parent_id` int(10) NOT NULL default '0',
  `reason` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_by` int(11) default NULL,
  `updated_by` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `support_reason`
--

INSERT INTO `support_reason` (`id`, `key`, `parent_id`, `reason`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(1, 'Refund Request', 0, 'Refund due to Duplicate payment or other difficulties faced during payment process', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(2, 'Reschedule Interview Date', 0, 'Need of Re-scheduling of interview date due to un-availability  or due to some other reasons ', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(3, 'Change Application Details', 0, 'Changed in name , date of birth or change in office area ', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(4, 'Get Application Details', 0, 'To know current status of Application and other details regarding Application.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(5, 'Duplicate Payment', 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(6, 'Others', 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(9, 'Name', 3, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(10, 'Date Of Birth', 3, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(11, 'Processing Office', 3, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(12, 'Others', 3, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL);


INSERT INTO `pay4meInt`.`ep_menu` (
`id` ,
`name` ,
`label` ,
`parent_id` ,
`sfguardperm` ,
`url` ,
`eval` ,
`sequence` ,
`created_at` ,
`updated_at` ,
`deleted` ,
`created_by` ,
`updated_by`
)
VALUES (
NULL , 'Vault Processing', 'Vault Processing ', '10', 'portal_admin', 'paymentGateway/vaultProcess', NULL , '3', '', '', '0', NULL , NULL
);
