ALTER TABLE  `currency_rate` ADD  `additional_charges_for_vap` FLOAT( 22 ) NOT NULL AFTER  `visa_arrival`;
ALTER TABLE  `currency_rate_version` ADD  `additional_charges_for_vap` FLOAT( 22 ) NOT NULL AFTER  `visa_arrival`;

ALTER TABLE  `currency_rate` CHANGE  `additional_charges_for_vap`  `additional_charges_for_vap` FLOAT( 8, 2 ) NOT NULL;
ALTER TABLE  `currency_rate_version` CHANGE  `additional_charges_for_vap`  `additional_charges_for_vap` FLOAT( 8, 2 ) NOT NULL;

INSERT INTO `ep_menu` (`id`, `name`, `label`, `parent_id`, `sfguardperm`, `url`, `eval`, `env`, `sequence`, `created_at`, `updated_at`, `deleted`, `created_by`, `updated_by`) VALUES (NULL, 'Application Fees in GBP', 'Application Fees in GBP', '78', 'configure_manager', 'configurationManager/currencyRate', NULL, 'custom', '6', now(), now(), '0', NULL, NULL);