ALTER TABLE `ipay4me_recharge_order` ADD INDEX ( `recharge_order_id` );
ALTER TABLE `ipay4me_recharge_order` ADD FOREIGN KEY ( `recharge_order_id` ) REFERENCES `recharge_order` (
`id`
);



INSERT INTO `sf_guard_group` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(5, 'report_user', 'Report User', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

INSERT INTO `sf_guard_permission` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(5, 'admin_report', 'admin_report', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

INSERT INTO `sf_guard_group_permission` (`group_id`, `permission_id`, `created_at`, `updated_at`) VALUES
(1, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

UPDATE `ep_menu` SET `sfguardperm` = 'admin_report' WHERE `ep_menu`.`id` =7 LIMIT 1 ;


INSERT INTO `ep_menu` ( `name` , `label` , `parent_id` , `sfguardperm` , `url` , `eval` , `sequence` , `created_at` , `updated_at` , `deleted` , `created_by` , `updated_by` )
VALUES (
'Report User', 'Report User', 16, 'portal_admin', 'userManagement/index', NULL , 2, '2010-10-14 10:30:09', '2010-10-14 10:31:09', 0, NULL , NULL
);

