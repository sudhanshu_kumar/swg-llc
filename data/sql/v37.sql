
CREATE TABLE vault_config (id BIGINT AUTO_INCREMENT, is_vault_active VARCHAR(255) DEFAULT 'no' NOT NULL, trans_date DATETIME, amount FLOAT(18, 2) NOT NULL, card_type VARCHAR(10) NOT NULL  COMMENT 'A=001,M=010,V=001',  created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, deleted TINYINT(1) DEFAULT '0' NOT NULL, PRIMARY KEY(id)) ENGINE = INNODB;

INSERT INTO `fps_rule` (`id`, `description`, `deleted`) VALUES (4, 'Black List Card', 0);

INSERT INTO `vault_config` (`id`, `is_vault_active`, `trans_date`, `amount`, `card_type`, `created_at`, `updated_at`, `deleted`) VALUES
(1, 'no',now(), 60000.00, '011',now(),now(), 0);