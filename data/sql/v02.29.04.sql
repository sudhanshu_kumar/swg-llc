CREATE TABLE currency_rate (id BIGINT AUTO_INCREMENT, country_id VARCHAR(20) NOT NULL, currency VARCHAR(10) NOT NULL, passport FLOAT(18, 2) NOT NULL, visa FLOAT(18, 2) NOT NULL, visa_arrival FLOAT(18, 2) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, created_by INT, updated_by INT, version BIGINT, PRIMARY KEY(id)) ENGINE = INNODB;
CREATE TABLE currency_rate_version (id BIGINT, country_id VARCHAR(20) NOT NULL, currency VARCHAR(10) NOT NULL, passport FLOAT(18, 2) NOT NULL, visa FLOAT(18, 2) NOT NULL, visa_arrival FLOAT(18, 2) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, created_by INT, updated_by INT, version BIGINT, PRIMARY KEY(id, version)) ENGINE = INNODB;

ALTER TABLE currency_rate_version ADD CONSTRAINT currency_rate_version_id_currency_rate_id FOREIGN KEY (id) REFERENCES currency_rate(id) ON UPDATE CASCADE ON DELETE CASCADE;


ALTER TABLE  `money_order` ADD  `convert_amount` FLOAT( 18, 2 ) NOT NULL AFTER  `amount` , ADD  `currency` ENUM(  'dollar',  'pound' ) NOT NULL AFTER  `convert_amount`;

ALTER TABLE  `cart_tracking_number` ADD  `convert_amount` FLOAT( 18, 2 ) NOT NULL AFTER  `cart_amount` , ADD  `currency` ENUM(  'dollar',  'pound' ) NOT NULL AFTER  `convert_amount`;

ALTER TABLE  `order_request` ADD  `convert_amount` FLOAT( 18, 2 ) NOT NULL AFTER  `amount` , ADD  `currency` ENUM(  'dollar',  'pound' ) NOT NULL AFTER  `convert_amount`;
ALTER TABLE  `order_request_details` ADD  `convert_amount` FLOAT( 18, 2 ) NOT NULL AFTER  `amount` , ADD  `currency` ENUM(  'dollar',  'pound' ) NOT NULL AFTER  `convert_amount`;


