CREATE TABLE card_country_list (id BIGINT AUTO_INCREMENT, bin INT NOT NULL, process_bin INT NOT NULL, region TINYINT NOT NULL, country VARCHAR(5) NOT NULL, unknown VARCHAR(5) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id)) ENGINE = INNODB;
CREATE TABLE card_not_in_country_list (id BIGINT AUTO_INCREMENT, order_number BIGINT NOT NULL, bin INT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id)) ENGINE = INNODB;

ALTER TABLE  `card_country_list` ADD INDEX (  `bin` );
ALTER TABLE  `card_country_list` CHANGE  `bin`  `bin` INT( 6 ) UNSIGNED ZEROFILL NOT NULL;
ALTER TABLE  `card_country_list` CHANGE  `process_bin`  `process_bin` INT( 6 ) UNSIGNED ZEROFILL NOT NULL;

--
-- Dumping data for table `card_country_list`
--
INSERT INTO `card_country_list` (`id`, `bin`, `process_bin`, `region`, `country`, `unknown`, `created_at`, `updated_at`) VALUES
(1, 011300, 455448, 4, 'PH', 'I', '2012-07-23 10:10:10', '2012-07-23 10:10:10'),
(2, 030009, 030009, 1, 'US', 'N', '2012-07-23 10:10:11', '2012-07-23 10:10:11'),
(3, 030011, 030011, 1, 'US', 'N', '2012-07-23 10:10:12', '2012-07-23 10:10:12'),
(4, 030012, 030012, 1, 'US', 'N', '2012-07-23 10:10:13', '2012-07-23 10:10:13'),
(5, 030057, 030057, 1, 'US', 'N', '2012-07-23 10:10:14', '2012-07-23 10:10:14');


--
-- Dumping data for table `fps_rule`
--

INSERT INTO `fps_rule` (`id`, `description`, `deleted`) VALUES (6, 'Surname', 0),(7, 'Credit Card Country', 0);