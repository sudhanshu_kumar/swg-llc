UPDATE `sf_guard_group` SET `description` = 'Support User' WHERE `sf_guard_group`.`id` =6 LIMIT 1 ;
UPDATE `sf_guard_group` SET `description` = 'Payment Group' WHERE `sf_guard_group`.`id` =3 LIMIT 1 ;

CREATE TABLE IF NOT EXISTS `payment_mode` (
  `id` bigint(20) NOT NULL auto_increment,
  `card_type` varchar(50) default NULL,
  `gateway_id` bigint(20) default NULL,
  `group_id` float(5,1) default NULL,
  `display_name` varchar(50) default NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_by` int(11) default NULL,
  `updated_by` int(11) default NULL,
  PRIMARY KEY  (`id`),
  KEY `gateway_id_idx` (`gateway_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

ALTER TABLE `payment_mode` ADD FOREIGN KEY ( `gateway_id` ) REFERENCES `gateway` (`id`);

 ALTER TABLE `payment_mode` ADD UNIQUE (`card_type`);
--
-- Dumping data for table `payment_mode`
--

INSERT INTO `payment_mode` (`id`, `card_type`, `gateway_id`, `group_id`, `display_name`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(1, 'A', 1, 1.0, 'AMEX', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(2, 'M', 1, 1.0, 'MASTER', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(3, 'V', 1, 1.0, 'VISA', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(4, 'Vc', 4, 4.0, 'VISA', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(5, 'nmi_vbv', 5, 5.1, 'Verified by Visa', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(6, 'nmi_mcs', 5, 5.2, 'Master Card Secure', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL);


CREATE TABLE IF NOT EXISTS `country_payment_mode` (
  `id` bigint(20) NOT NULL auto_increment,
  `country_code` varchar(20) default NULL,
  `card_type` varchar(255) default NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_by` int(11) default NULL,
  `updated_by` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


--
-- Dumping data for table `country_payment_mode`
--

INSERT INTO `country_payment_mode` (`id`, `country_code`, `card_type`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(1, 'ALL', 'A,V,M,nmi_vbv,nmi_mcs', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0),
(2, 'ZA', 'A,Vc', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0);


INSERT INTO `ep_menu` (
`id` ,
`name` ,
`label` ,
`parent_id` ,
`sfguardperm` ,
`url` ,
`eval` ,
`sequence` ,
`created_at` ,
`updated_at` ,
`deleted` ,
`created_by` ,
`updated_by`
)
VALUES (
NULL , 'GrayPay Vault ', 'GrayPay Vault ', '10', 'portal_admin', 'settings/status', NULL , '2', '', '', '0', NULL , NULL
);


CREATE TABLE global_settings (id BIGINT AUTO_INCREMENT, var_key VARCHAR(255) NOT NULL, var_value VARCHAR(255), created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, created_by INT, updated_by INT, PRIMARY KEY(id)) ENGINE = INNODB;

INSERT INTO `global_settings` (`id`, `var_key`, `var_value`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(1, 'graypay-vault', 'inactive', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL);