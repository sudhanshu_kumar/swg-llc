ALTER TABLE `tbl_passport_application` CHANGE `paid_naira_amount` `paid_local_currency_amount` DECIMAL( 18, 2 ) NULL DEFAULT NULL;
ALTER TABLE `tbl_passport_application` ADD `local_currency_id` INT NULL AFTER `paid_local_currency_amount` ;
