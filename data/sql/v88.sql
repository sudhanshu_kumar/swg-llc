UPDATE `ep_menu` SET `name` = 'MoneyOrder',
`label` = 'Money Order',
`parent_id` = '0',
`sfguardperm` = NULL ,
`url` = NULL,
`sfguardperm` = '',
`url` = '',
`sequence` = '7' WHERE `ep_menu`.`name` ='SubmitMoneyOrder' LIMIT 1 ;



INSERT INTO `ep_menu` (
`id` ,
`name` ,
`label` ,
`parent_id` ,
`sfguardperm` ,
`url` ,
`eval` ,
`env` ,
`sequence` ,
`created_at` ,
`updated_at` ,
`deleted` ,
`created_by` ,
`updated_by`
)
VALUES (
NULL , 'AssociateTrackingNumber', 'Associate Tracking Number', '49', 'payment_user', 'mo_configuration/saveMoneyorder', NULL , 'custom', '1', '', '', '0', NULL , NULL
);

INSERT INTO `ep_menu` (
`id` ,
`name` ,
`label` ,
`parent_id` ,
`sfguardperm` ,
`url` ,
`eval` ,
`env` ,
`sequence` ,
`created_at` ,
`updated_at` ,
`deleted` ,
`created_by` ,
`updated_by`
)
VALUES (
NULL , 'ReceivePayments', 'Receive Payments', '49', 'admin_support_user', 'supportTool/searchByMoneyOrder', NULL , 'custom', '1', '', '', '0', NULL , NULL
);