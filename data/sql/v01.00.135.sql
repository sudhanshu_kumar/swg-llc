ALTER TABLE `applicant_vault` ADD `transaction_type` VARCHAR( 50 ) NULL AFTER `comments`;
ALTER TABLE `applicant_vault` ADD `reason` TEXT NULL AFTER `transaction_type` ;
ALTER TABLE `applicant_vault` ADD `agent_proof` VARCHAR( 255 ) NOT NULL AFTER `reason` ;


ALTER TABLE `tbl_user_transaction_limit` CHANGE `cart_capacity` `cart_capacity` BIGINT( 20 ) NULL DEFAULT NULL ;

ALTER TABLE `tbl_user_transaction_limit` CHANGE `cart_amount_capacity` `cart_amount_capacity` DOUBLE( 18, 2 ) NULL DEFAULT NULL;

ALTER TABLE `tbl_user_transaction_limit` CHANGE `number_of_transaction` `number_of_transaction` BIGINT( 20 ) NULL DEFAULT NULL;

ALTER TABLE `tbl_user_transaction_limit` CHANGE `transaction_period` `transaction_period` BIGINT( 20 ) NULL DEFAULT NULL;