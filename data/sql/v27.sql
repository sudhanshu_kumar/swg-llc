ALTER TABLE `user_detail` ADD `sms_charge` FLOAT NOT NULL AFTER `master_account_id` ,

ADD `is_sms_charge_paid` ENUM( 'yes', 'no' ) NOT NULL DEFAULT 'no' AFTER `sms_charge` ;


CREATE TABLE IF NOT EXISTS `user_recharge_config` (
  `id` bigint(20) NOT NULL auto_increment,
  `user_id` int(11) NOT NULL,
  `amount` float(18,2) NOT NULL,
  `days` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_by` int(11) default NULL,
  `updated_by` int(11) default NULL,
  `deleted` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `user_id_idx` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


ALTER TABLE `user_recharge_config`
  ADD CONSTRAINT `user_recharge_config_user_id_sf_guard_user_id` FOREIGN KEY (`user_id`) REFERENCES `sf_guard_user` (`id`);

--INSERT INTO `ep_menu` (`name`, `label`, `parent_id`, `sfguardperm`, `url`, `eval`, `sequence`, `created_at`, `updated_at`, `deleted`, `created_by`, `updated_by`) VALUES
--('Ewallet Recharge', 'Ewallet Recharge', 10, 'portal_admin', 'settings/ewalletRecharge', NULL, 1, now(), now(), 0, NULL, NULL);

 ALTER TABLE `user_detail` ADD UNIQUE (
`mobile_phone`
)
ADD `is_sms_charge_paid` ENUM( 'yes', 'no' ) NOT NULL DEFAULT 'no' AFTER `sms_charge` ;

ALTER TABLE `recharge_order`  DROP `sms_charge`,  DROP `is_sms_charge_paid`;


CREATE TABLE service_charge(
id BIGINT AUTO_INCREMENT ,
gateway_id SMALLINT NOT NULL ,
TYPE VARCHAR( 20 ) NOT NULL ,
charge_amount FLOAT( 18, 2 ) ,
charge_amount_type VARCHAR( 20 ) NOT NULL ,
lower_limit FLOAT( 18, 2 ) ,
upper_limit FLOAT( 18, 2 ) ,
created_at DATETIME NOT NULL ,
updated_at DATETIME NOT NULL ,
created_by INT,
updated_by INT,
INDEX gateway_id_idx( gateway_id ) ,
PRIMARY KEY ( id )
) ENGINE = INNODB;

ALTER TABLE `service_charge` ADD FOREIGN KEY ( `gateway_id` ) REFERENCES `gateway` (
`id`
);

INSERT INTO `service_charge` (
`id` ,
`gateway_id` ,
`type` ,
`charge_amount` ,
`charge_amount_type` ,
`lower_limit` ,
`upper_limit` ,
`created_at` ,
`updated_at` ,
`created_by` ,
`updated_by`
)
VALUES (
NULL , '4', 'ewallet_recharge', '2', 'percent', '5', '25', now(), now(), NULL , NULL
);

