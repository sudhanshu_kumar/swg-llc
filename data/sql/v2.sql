DROP TABLE `validation_rules`;
TRUNCATE TABLE `merchant_item`  ;
DROP TABLE `merchant_request_details`  ;
DROP TABLE `merchant_request`;
DROP TABLE `merchant_item`  ;
DROP TABLE `merchant_service`;
TRUNCATE TABLE `transaction`;


DROP TABLE `response`  ;
DROP TABLE `request`  
DROP TABLE `transaction`  ;
CREATE TABLE order_request (id BIGINT AUTO_INCREMENT, user_id INT NOT NULL, version VARCHAR(10) NOT NULL, merchant_id BIGINT NOT NULL, transaction_number BIGINT UNIQUE NOT NULL, name VARCHAR(100) NOT NULL, description TEXT NOT NULL, amount FLOAT(18, 2) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, deleted TINYINT(1) DEFAULT '0' NOT NULL, created_by INT, updated_by INT, UNIQUE INDEX unique_parameter_idx (transaction_number), INDEX merchant_id_idx (merchant_id), INDEX user_id_idx (user_id), PRIMARY KEY(id)) ENGINE = INNODB;


ALTER TABLE order_request ADD CONSTRAINT order_request_user_id_sf_guard_user_id FOREIGN KEY (user_id) REFERENCES sf_guard_user(id) ON DELETE CASCADE;
ALTER TABLE order_request ADD CONSTRAINT order_request_merchant_id_merchant_id FOREIGN KEY (merchant_id) REFERENCES merchant(id) ON DELETE CASCADE;

CREATE TABLE request (id BIGINT AUTO_INCREMENT, order_id BIGINT NOT NULL, request_order_number BIGINT NOT NULL, amount FLOAT(18, 2), card_details_first SMALLINT, card_details_last SMALLINT, cvv SMALLINT, exp_month SMALLINT, exp_year SMALLINT, card_type VARCHAR(255), created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, deleted TINYINT(1) DEFAULT '0' NOT NULL, created_by INT, updated_by INT, UNIQUE INDEX unique_parameter_idx (request_order_number), INDEX order_id_idx (order_id), PRIMARY KEY(id)) ENGINE = INNODB;
CREATE TABLE response (id BIGINT AUTO_INCREMENT, request_id BIGINT NOT NULL, response_code TINYINT NOT NULL, response_reason VARCHAR(10), response_reason_txt VARCHAR(255), response_reason_sub_code TINYINT, response_transaction_code INT, response_transaction_date DATETIME, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, deleted TINYINT(1) DEFAULT '0' NOT NULL, created_by INT, updated_by INT, INDEX request_id_idx (request_id), PRIMARY KEY(id)) ENGINE = INNODB;
ALTER TABLE request ADD CONSTRAINT request_order_id_order_request_id FOREIGN KEY (order_id) REFERENCES order_request(id) ON DELETE CASCADE;
ALTER TABLE response ADD CONSTRAINT response_request_id_request_id FOREIGN KEY (request_id) REFERENCES request(id) ON DELETE CASCADE;

CREATE TABLE user_detail (id BIGINT AUTO_INCREMENT, user_id INT NOT NULL, name VARCHAR(255), last_name VARCHAR(255), dob DATE, address VARCHAR(255), email VARCHAR(255), mobile_no VARCHAR(20), work_phone VARCHAR(20), send_sms VARCHAR(255) DEFAULT 'gsmno', failed_attempt SMALLINT, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, deleted TINYINT(1) DEFAULT '0' NOT NULL, created_by INT, updated_by INT, INDEX user_id_idx (user_id), PRIMARY KEY(id)) ENGINE = INNODB;
ALTER TABLE user_detail ADD CONSTRAINT user_detail_user_id_sf_guard_user_id FOREIGN KEY (user_id) REFERENCES sf_guard_user(id) ON DELETE CASCADE;


ALTER TABLE `order_request` ADD `payment_status` ENUM( '0', '1' ) NOT NULL DEFAULT '1' AFTER `amount` ;

 ALTER TABLE `merchant` DROP `auth_info`  ;

 ALTER TABLE `merchant` ADD `notification_url` VARCHAR( 255 ) NOT NULL AFTER `merchant_key`  ;
ALTER TABLE `merchant` ADD `homepage_url` VARCHAR( 255 ) NOT NULL AFTER `notification_url` ;

----------------------------------------------------------------------------------
--inserting dummy merchant data into merchant table as in pay4me mapping table----
----------------------------------------------------------------------------------
INSERT INTO `merchant` (`id`, `name`, `merchant_code`, `merchant_key`, `notification_url`, `homepage_url`, `created_at`, `updated_at`, `deleted`, `created_by`, `updated_by`) VALUES
(1, 'nis passport', 23412, 'frwef34234', 'http://192.168.29.217/pay4me_6mar/web/frontend_dev.php/', 'http://192.168.29.217/pay4me_6mar/web/frontend_dev.php/', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, NULL),
(2, 'nis visa', 23423, 'ergfer34324', 'http://192.168.29.217/pay4me_6mar/web/frontend_dev.php/', 'http://192.168.29.217/pay4me_6mar/web/frontend_dev.php/', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, NULL),
(3, 'nis mrp', 4234234, 'werewr23423', 'http://192.168.29.217/pay4me_6mar/web/frontend_dev.php/', 'http://192.168.29.217/pay4me_6mar/web/frontend_dev.php/', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, NULL);


 ALTER TABLE `user_detail` CHANGE `mobile_no` `mobile_phone` VARCHAR( 20 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL  ;
 ALTER TABLE `user_detail` CHANGE `name` `first_name` VARCHAR( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL  ;
 ALTER TABLE `user_detail` DROP `send_sms`  ;
 ALTER TABLE `user_detail` CHANGE `user_id` `user_id` INT( 11 ) NULL  ;



ALTER TABLE `order_request` ADD `retry_id` BIGINT( 20 ) NOT NULL AFTER `transaction_number` ;

ALTER TABLE `order_request` ADD UNIQUE (merchant_id, transaction_number, retry_id);



INSERT INTO `sf_guard_group` (`name`, `description`, `created_at`, `updated_at`) VALUES
('payment_group', 'payment_group', '0000-00-00 00:00:00', '0000-00-00 00:00:00');


ALTER TABLE `order_request` CHANGE `user_id` `user_id` INT( 11 ) NULL;

