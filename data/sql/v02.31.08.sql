ALTER TABLE  `transaction_service_charges` ADD INDEX (  `app_id` ,  `app_type` ) ;

INSERT INTO `ep_menu` (`name`, `label`, `parent_id`, `sfguardperm`, `url`, `eval`, `env`, `sequence`, `created_at`, `updated_at`, `deleted`, `created_by`, `updated_by`) VALUES
( 'UnblockUsers', 'Unblock Users', 27, 'refund_support_user', 'supportTool/unblockUser', NULL, 'custom', 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, NULL);