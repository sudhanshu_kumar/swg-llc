ALTER TABLE  `card_country_list` CHANGE  `process_bin`  `processor_bin` INT( 6 ) UNSIGNED ZEROFILL NOT NULL;

INSERT INTO `ep_menu` (`id`, `name`, `label`, `parent_id`, `sfguardperm`, `url`, `eval`, `env`, `sequence`, `created_at`, `updated_at`, `deleted`, `created_by`, `updated_by`) VALUES (NULL, 'Credit Card Country (BIN)', 'Credit Card Country', '83', 'configure_manager', 'configurationManager/creditCardCountry', NULL, 'custom', '7', '2012-07-25 18:29:55', '2012-07-25 18:29:58', '0', NULL, NULL);
