UPDATE `support_reason` SET `key` = 'Request for Refund',
`reason` = 'You can request for refund due to made duplicate payment or any other valid reason.' WHERE `support_reason`.`id` =1 LIMIT 1 ;


UPDATE `support_reason` SET `key` = 'Request for Re-scheduling of interview date',
`reason` = 'You can request for Re-scheduling of interview date due to unavailability or any other valid reason.' WHERE `support_reason`.`id` =2 LIMIT 1 ;


UPDATE `support_reason` SET `key` = 'Request for change Application Details',
`reason` = 'You can request for change application details for Name, Date of birth or processing office area.' WHERE `support_reason`.`id` =3 LIMIT 1 ;


UPDATE `support_reason` SET `key` = 'Request for Application Details',
`reason` = 'You can request to know current status of Application and other details regarding Application.' WHERE `support_reason`.`id` =4 LIMIT 1 ;