CREATE TABLE IF NOT EXISTS `support_request_list` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `order_number` bigint(20) NOT NULL,
  `status` varchar(255) NOT NULL DEFAULT 'Pending',
  `user_id` int(11) NOT NULL,
  `support_category` varchar(255) DEFAULT NULL,
  `sub_support_category` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id_idx` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `support_request_application_details` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `request_id` bigint(20) NOT NULL,
  `app_id` bigint(20) NOT NULL,
  `app_type` varchar(20) NOT NULL,
  `request_for` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `request_id_idx` (`request_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `support_request_comments` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `request_id` bigint(20) NOT NULL,
  `support_user_comments` varchar(255) DEFAULT NULL,
  `customer_comments` varchar(255) DEFAULT NULL,
  `upload_document1` varchar(255) DEFAULT NULL,
  `upload_document2` varchar(255) DEFAULT NULL,
  `upload_document3` varchar(255) DEFAULT NULL,
  `upload_document4` varchar(255) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `request_id_idx` (`request_id`),
  KEY `user_id_idx` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;



CREATE TABLE IF NOT EXISTS `support_request_assignment` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `request_id` bigint(20) NOT NULL,
  `assigned_to` int(11) NOT NULL,
  `assigned_from` bigint(20) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `request_id_idx` (`request_id`),
  KEY `assigned_from_idx` (`assigned_from`),
  KEY `assigned_to` (`assigned_to`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;



ALTER TABLE `support_request_list`
  ADD CONSTRAINT `support_request_list_user_id_sf_guard_user_id` FOREIGN KEY (`user_id`) REFERENCES `sf_guard_user` (`id`) ON DELETE CASCADE;

--
ALTER TABLE `support_request_application_details`
  ADD CONSTRAINT `support_request_application_details_ibfk_1` FOREIGN KEY (`request_id`) REFERENCES `support_request_list` (`id`) ON DELETE CASCADE;

--
ALTER TABLE `support_request_comments`
  ADD CONSTRAINT `support_request_comments_request_id_support_request_list_id` FOREIGN KEY (`request_id`) REFERENCES `support_request_list` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `support_request_comments_user_id_sf_guard_user_id` FOREIGN KEY (`user_id`) REFERENCES `sf_guard_user` (`id`) ON DELETE CASCADE;

--
ALTER TABLE `support_request_assignment`
  ADD CONSTRAINT `support_request_assignment_ibfk_2` FOREIGN KEY (`assigned_to`) REFERENCES `sf_guard_user` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `support_request_assignment_ibfk_1` FOREIGN KEY (`assigned_from`) REFERENCES `support_request_assignment` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `support_request_assignment_request_id_support_request_list_id` FOREIGN KEY (`request_id`) REFERENCES `support_request_list` (`id`) ON DELETE CASCADE;
INSERT INTO `ep_menu` (`name`, `label`, `parent_id`, `sfguardperm`, `url`, `eval`, `env`, `sequence`, `created_at`, `updated_at`, `deleted`, `created_by`, `updated_by`) VALUES
('Support Request History', 'Support Request History', 3, 'payment_user', 'requestRefund/supportRequestList', NULL, 'custom', 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, NULL),
('Support Request', 'Support Request', 0, NULL, NULL, NULL, 'custom', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, NULL),
('Pending Support Request', 'Pending Support Request', 79, 'admin_support_user', 'requestRefund/supportRequestPendingList', NULL, 'custom', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, NULL),
('Assigned Support Request', 'Assigned Support Request', 79, 'admin_support_user', 'requestRefund/supportRequestAssignedList', NULL, 'custom', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, NULL),
('Pending Support Request Report', 'Pending Support Request Report', 79, 'admin_support_user', 'requestRefund/pendingReqReport', NULL, 'custom', 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, NULL);



ALTER TABLE `support_request_assignment` ADD `assign_active` VARCHAR( 5 ) NOT NULL AFTER `assigned_from`;

ALTER TABLE `support_request_comments` CHANGE `upload_document1` `upload_document1` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,

ALTER TABLE `support_request_comments` CHANGE `upload_document2` `upload_document2` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
CHANGE `upload_document3` `upload_document3` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,

ALTER TABLE `support_request_comments` CHANGE `upload_document4` `upload_document4` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;

ALTER TABLE `tbl_payment_privilage_user` ADD `mid_service` VARCHAR( 10 ) NOT NULL AFTER `status` ,
ADD `card_type` VARCHAR( 50 ) NOT NULL AFTER `mid_service` ;