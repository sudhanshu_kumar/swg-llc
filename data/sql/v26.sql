ALTER TABLE `recharge_order` ADD `gateway_id` SMALLINT NOT NULL AFTER `user_id`  ;
ALTER TABLE `recharge_order` ADD INDEX ( `gateway_id` )  ;

ALTER TABLE `recharge_order` ADD FOREIGN KEY ( `gateway_id` ) REFERENCES `gateway` (`id`); 


ALTER TABLE `gateway` ADD `display_name` VARCHAR( 50 ) NOT NULL AFTER `name` ;

UPDATE `gateway` SET `display_name` = 'Gray Pay' WHERE `gateway`.`id` =1 LIMIT 1 ;

UPDATE `gateway` SET `display_name` = 'Pay Easy' WHERE `gateway`.`id` =2 LIMIT 1 ;

UPDATE `gateway` SET `display_name` = 'Ewallet' WHERE `gateway`.`id` =3 LIMIT 1 ;

UPDATE `gateway` SET `display_name` = 'Visa Credit/Visa Debit' WHERE `gateway`.`id` =4 LIMIT 1 ;



INSERT INTO `transaction_charges` (
`id` ,
`gateway_id` ,
`transaction_charges` ,
`split_type` ,
`created_at` ,
`updated_at` ,
`deleted` ,
`created_by` ,
`updated_by` ,
`version`
)
VALUES (
NULL , '4', '4', 'percentage', now(),now(), '0', NULL , NULL , '');


INSERT INTO `merchant` (`id`, `name`, `merchant_code`, `merchant_key`, `notification_url`, `homepage_url`, `address`, `email`, `contact_phone`, `description`, `abbr`, `user_id`, `created_at`, `updated_at`, `deleted`, `created_by`, `updated_by`) VALUES
(6, 'vbv', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, now(), now(), 0, NULL, NULL);


UPDATE `gateway` SET `merchant_id` = '6' WHERE `gateway`.`id` =4 LIMIT 1 ;

INSERT INTO `sf_guard_user` (`id`, `username`, `algorithm`, `salt`, `password`, `is_active`, `is_super_admin`, `last_login`, `created_at`, `updated_at`) VALUES
(7, 'vbv', 'sha1', 'b8becaac5c658ed503818433172af98e', 'b9b75c8dd25d7f6011d6969e90b761b04687ed8d', 1, 0, NULL, now(), now());


INSERT INTO `ep_master_account` (`id`, `clear_balance`, `unclear_balance`, `account_number`, `account_name`, `type`, `sortcode`, `bankname`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(5, 0, 0, 34534534, 'vbv', 'collection', NULL, NULL, NULL, '2010-10-08 15:38:21', 46, 46);

INSERT INTO `user_detail` ( `user_id`, `first_name`, `last_name`, `dob`, `address`, `email`, `mobile_phone`, `work_phone`, `failed_attempt`, `master_account_id`, `kyc_status`, `disapprove_reason`, `approved_by`, `created_at`, `updated_at`, `deleted`, `created_by`, `updated_by`) VALUES
(7, 'vbv', 'vbv', NULL, NULL, NULL, NULL, NULL, NULL, 5, 0, '', 0, now(), now(), 0, NULL, NULL);

UPDATE `ep_master_account` SET `id` = '9' WHERE `ep_master_account`.`id` =5 LIMIT 1 ;

UPDATE `merchant` SET `user_id` = '7' WHERE `merchant`.`id` =6 LIMIT 1 ;


CREATE TABLE ewallet_consolidation (id BIGINT AUTO_INCREMENT, user_id INT NOT NULL, gateway_id SMALLINT NOT NULL, service_charge BIGINT, actual_amount FLOAT(18, 2) NOT NULL, wallet_amount FLOAT(18, 2) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, created_by INT, updated_by INT, INDEX user_id_idx (user_id), INDEX gateway_id_idx (gateway_id), PRIMARY KEY(id)) ENGINE = INNODB;

ALTER TABLE ewallet_consolidation ADD CONSTRAINT ewallet_consolidation_user_id_sf_guard_user_id FOREIGN KEY (user_id) REFERENCES sf_guard_user(id);
ALTER TABLE ewallet_consolidation ADD CONSTRAINT ewallet_consolidation_gateway_id_gateway_id FOREIGN KEY (gateway_id) REFERENCES gateway(id);
