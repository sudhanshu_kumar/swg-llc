-- phpMyAdmin SQL Dump
-- version 2.11.3deb1ubuntu1.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 05, 2010 at 03:37 AM
-- Server version: 5.0.51
-- PHP Version: 5.2.4-2ubuntu5.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `ipay4me_dev_0406`
--

-- --------------------------------------------------------

--
-- Table structure for table `ep_job`
--

CREATE TABLE IF NOT EXISTS `ep_job` (
  `id` bigint(20) NOT NULL auto_increment,
  `name` varchar(40) default NULL,
  `url` varchar(50) default NULL,
  `sf_application` varchar(20) default NULL,
  `start_time` datetime default NULL,
  `end_time` datetime default NULL,
  `schedule_type` enum('once','repeated') default 'once',
  `state` enum('active','suspended','finished') default 'active',
  `last_execution_status` enum('notexecuted','pass','failed') default 'notexecuted',
  `max_retry_attempts` tinyint(4) default '0',
  `execution_attempts` tinyint(4) default NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ep_job`
--


-- --------------------------------------------------------

--
-- Table structure for table `ep_job_data`
--

CREATE TABLE IF NOT EXISTS `ep_job_data` (
  `id` bigint(20) NOT NULL auto_increment,
  `output_type` tinyint(4) default NULL,
  `output_text` longtext,
  `job_execution_id` bigint(20) default NULL,
  PRIMARY KEY  (`id`),
  KEY `ep_job_data_output_type_idx` (`output_type`),
  KEY `ep_job_data_job_execution_id_ep_job_execution_id` (`job_execution_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ep_job_data`
--


-- --------------------------------------------------------

--
-- Table structure for table `ep_job_execution`
--

CREATE TABLE IF NOT EXISTS `ep_job_execution` (
  `id` bigint(20) NOT NULL auto_increment,
  `job_id` bigint(20) default NULL,
  `start_time` datetime default NULL,
  `end_time` datetime default NULL,
  `exit_code` smallint(6) default NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `job_id_idx` (`job_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ep_job_execution`
--


-- --------------------------------------------------------

--
-- Table structure for table `ep_job_parameters`
--

CREATE TABLE IF NOT EXISTS `ep_job_parameters` (
  `id` bigint(20) NOT NULL auto_increment,
  `job_id` bigint(20) default NULL,
  `name` varchar(40) default NULL,
  `value` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `unique_parameter_idx` (`job_id`,`name`),
  KEY `job_id_idx` (`job_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ep_job_parameters`
--


-- --------------------------------------------------------

--
-- Table structure for table `ep_job_queue`
--

CREATE TABLE IF NOT EXISTS `ep_job_queue` (
  `id` bigint(20) NOT NULL auto_increment,
  `job_id` bigint(20) default NULL,
  `scheduled_start_time` datetime default NULL,
  `start_time` datetime default NULL,
  `current_status` enum('scheduled','running') default 'scheduled',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `schstrtindex_idx` (`scheduled_start_time`),
  KEY `strtindex_idx` (`start_time`),
  KEY `job_id_idx` (`job_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ep_job_queue`
--


-- --------------------------------------------------------

--
-- Table structure for table `ep_job_repeat_schedule`
--

CREATE TABLE IF NOT EXISTS `ep_job_repeat_schedule` (
  `id` bigint(20) NOT NULL auto_increment,
  `job_id` bigint(20) default NULL,
  `minutes` varchar(200) default NULL,
  `hours` varchar(200) default NULL,
  `day_of_month` varchar(200) default NULL,
  `month` varchar(200) default NULL,
  `day_of_week` varchar(200) default NULL,
  PRIMARY KEY  (`id`),
  KEY `job_id_idx` (`job_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ep_job_repeat_schedule`
--


-- --------------------------------------------------------

--
-- Table structure for table `ep_menu`
--

CREATE TABLE IF NOT EXISTS `ep_menu` (
  `id` bigint(20) NOT NULL auto_increment,
  `name` varchar(100) default NULL,
  `label` varchar(100) default NULL,
  `parent_id` bigint(20) NOT NULL,
  `sfguardperm` varchar(100) default NULL,
  `url` varchar(200) default NULL,
  `eval` varchar(100) default NULL,
  `sequence` bigint(20) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL default '0',
  `created_by` int(11) default NULL,
  `updated_by` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ep_menu`
--


-- --------------------------------------------------------

--
-- Table structure for table `merchant`
--

CREATE TABLE IF NOT EXISTS `merchant` (
  `id` bigint(20) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `merchant_code` bigint(20) default NULL,
  `merchant_key` varchar(255) default NULL,
  `auth_info` varchar(255) default NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL default '0',
  `created_by` int(11) default NULL,
  `updated_by` int(11) default NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `merchant`
--


-- --------------------------------------------------------

--
-- Table structure for table `merchant_item`
--

CREATE TABLE IF NOT EXISTS `merchant_item` (
  `id` bigint(20) NOT NULL auto_increment,
  `item_number` varchar(150) NOT NULL,
  `merchant_service_id` bigint(20) NOT NULL,
  `payment_amount_requested` float(18,2) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL default '0',
  `created_by` int(11) default NULL,
  `updated_by` int(11) default NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `unique_parameter_idx` (`item_number`,`merchant_service_id`),
  KEY `merchant_service_id_idx` (`merchant_service_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `merchant_item`
--


-- --------------------------------------------------------

--
-- Table structure for table `merchant_request`
--

CREATE TABLE IF NOT EXISTS `merchant_request` (
  `id` bigint(20) NOT NULL auto_increment,
  `txn_ref` varchar(20) NOT NULL,
  `merchant_id` bigint(20) NOT NULL,
  `merchant_service_id` bigint(20) NOT NULL,
  `merchant_item_id` bigint(20) NOT NULL,
  `item_fee` float(18,2) NOT NULL,
  `payment_status_code` int(11),
  `paid_amount` float(18,2) default NULL,
  `paid_date` datetime default NULL,
  `service_charge` float(18,2) default NULL,
  `gateway_charge` float(18,2) default NULL,
  `payforme_amount` float(18,2) default NULL,
  `service_configuration_id` bigint(20) default NULL,
  `payment_mandate_id` bigint(20) default NULL,
  `transaction_location` varchar(20) default NULL,
  `validation_number` bigint(20) default NULL,
  `created_by` int(11) default NULL,
  `updated_by` int(11) default NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `merchant_id_idx` (`merchant_id`),
  KEY `merchant_service_id_idx` (`merchant_service_id`),
  KEY `merchant_item_id_idx` (`merchant_item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `merchant_request`
--


-- --------------------------------------------------------

--
-- Table structure for table `merchant_request_details`
--

CREATE TABLE IF NOT EXISTS `merchant_request_details` (
  `id` bigint(20) NOT NULL auto_increment,
  `merchant_item_id` bigint(20) NOT NULL,
  `merchant_service_id` bigint(20) NOT NULL,
  `merchant_request_id` bigint(20) NOT NULL,
  `iparam_one` bigint(20) default NULL,
  `iparam_two` bigint(20) default NULL,
  `iparam_three` bigint(20) default NULL,
  `iparam_four` bigint(20) default NULL,
  `iparam_five` bigint(20) default NULL,
  `iparam_six` bigint(20) default NULL,
  `iparam_seven` bigint(20) default NULL,
  `iparam_eight` bigint(20) default NULL,
  `iparam_nine` bigint(20) default NULL,
  `iparam_ten` bigint(20) default NULL,
  `name` varchar(100) NOT NULL,
  `sparam_one` varchar(100) default NULL,
  `sparam_two` varchar(100) default NULL,
  `sparam_three` varchar(100) default NULL,
  `sparam_four` varchar(100) default NULL,
  `sparam_five` varchar(100) default NULL,
  `sparam_six` varchar(100) default NULL,
  `sparam_seven` varchar(100) default NULL,
  `sparam_eight` varchar(100) default NULL,
  `sparam_nine` varchar(100) default NULL,
  `sparam_ten` varchar(100) default NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL default '0',
  `created_by` int(11) default NULL,
  `updated_by` int(11) default NULL,
  PRIMARY KEY  (`id`),
  KEY `merchant_item_id_idx` (`merchant_item_id`),
  KEY `merchant_service_id_idx` (`merchant_service_id`),
  KEY `merchant_request_id_idx` (`merchant_request_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `merchant_request_details`
--


-- --------------------------------------------------------

--
-- Table structure for table `merchant_service`
--

CREATE TABLE IF NOT EXISTS `merchant_service` (
  `id` bigint(20) NOT NULL auto_increment,
  `merchant_id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `notification_url` varchar(255) NOT NULL,
  `merchant_home_page` varchar(255) default NULL,
  `version` varchar(100) default NULL,
  `clubbed` enum('no','yes') NOT NULL default 'no',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL default '0',
  `created_by` int(11) default NULL,
  `updated_by` int(11) default NULL,
  PRIMARY KEY  (`id`),
  KEY `merchant_id_idx` (`merchant_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `merchant_service`
--


-- --------------------------------------------------------

--
-- Table structure for table `request`
--

CREATE TABLE IF NOT EXISTS `request` (
  `id` bigint(20) NOT NULL auto_increment,
  `transaction_id` bigint(20) NOT NULL,
  `amount` float(18,2) default NULL,
  `card_details_first` smallint(6) default NULL,
  `card_details_last` smallint(6) default NULL,
  `cvv` smallint(6) default NULL,
  `exp_month` smallint(6) default NULL,
  `exp_year` smallint(6) default NULL,
  `card_type` enum('Amex','Discover','Master','Visa') default NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL default '0',
  `created_by` int(11) default NULL,
  `updated_by` int(11) default NULL,
  PRIMARY KEY  (`id`),
  KEY `transaction_id_idx` (`transaction_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `request`
--


-- --------------------------------------------------------

--
-- Table structure for table `response`
--

CREATE TABLE IF NOT EXISTS `response` (
  `id` bigint(20) NOT NULL auto_increment,
  `request_id` bigint(20) NOT NULL,
  `response_code` tinyint(4) NOT NULL,
  `response_reason` varchar(10) default NULL,
  `response_reason_txt` varchar(255) default NULL,
  `response_reason_sub_code` tinyint(4) default NULL,
  `response_transaction_code` int(11) default NULL,
  `response_transaction_date` datetime default NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL default '0',
  `created_by` int(11) default NULL,
  `updated_by` int(11) default NULL,
  PRIMARY KEY  (`id`),
  KEY `request_id_idx` (`request_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `response`
--


-- --------------------------------------------------------

--
-- Table structure for table `sf_guard_group`
--

CREATE TABLE IF NOT EXISTS `sf_guard_group` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) default NULL,
  `description` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `sf_guard_group`
--


-- --------------------------------------------------------

--
-- Table structure for table `sf_guard_group_permission`
--

CREATE TABLE IF NOT EXISTS `sf_guard_group_permission` (
  `group_id` int(11) NOT NULL default '0',
  `permission_id` int(11) NOT NULL default '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY  (`group_id`,`permission_id`),
  KEY `sf_guard_group_permission_permission_id_sf_guard_permission_id` (`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sf_guard_group_permission`
--


-- --------------------------------------------------------

--
-- Table structure for table `sf_guard_permission`
--

CREATE TABLE IF NOT EXISTS `sf_guard_permission` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) default NULL,
  `description` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `sf_guard_permission`
--


-- --------------------------------------------------------

--
-- Table structure for table `sf_guard_remember_key`
--

CREATE TABLE IF NOT EXISTS `sf_guard_remember_key` (
  `id` int(11) NOT NULL auto_increment,
  `user_id` int(11) default NULL,
  `remember_key` varchar(32) default NULL,
  `ip_address` varchar(50) NOT NULL default '',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY  (`id`,`ip_address`),
  KEY `user_id_idx` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `sf_guard_remember_key`
--


-- --------------------------------------------------------

--
-- Table structure for table `sf_guard_user`
--

CREATE TABLE IF NOT EXISTS `sf_guard_user` (
  `id` int(11) NOT NULL auto_increment,
  `username` varchar(128) NOT NULL,
  `algorithm` varchar(128) NOT NULL default 'sha1',
  `salt` varchar(128) default NULL,
  `password` varchar(128) default NULL,
  `is_active` tinyint(1) default '1',
  `is_super_admin` tinyint(1) default '0',
  `last_login` datetime default NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `username` (`username`),
  KEY `is_active_idx_idx` (`is_active`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `sf_guard_user`
--


-- --------------------------------------------------------

--
-- Table structure for table `sf_guard_user_group`
--

CREATE TABLE IF NOT EXISTS `sf_guard_user_group` (
  `user_id` int(11) NOT NULL default '0',
  `group_id` int(11) NOT NULL default '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY  (`user_id`,`group_id`),
  KEY `sf_guard_user_group_group_id_sf_guard_group_id` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sf_guard_user_group`
--


-- --------------------------------------------------------

--
-- Table structure for table `sf_guard_user_permission`
--

CREATE TABLE IF NOT EXISTS `sf_guard_user_permission` (
  `user_id` int(11) NOT NULL default '0',
  `permission_id` int(11) NOT NULL default '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY  (`user_id`,`permission_id`),
  KEY `sf_guard_user_permission_permission_id_sf_guard_permission_id` (`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sf_guard_user_permission`
--


-- --------------------------------------------------------

--
-- Table structure for table `transaction`
--

CREATE TABLE IF NOT EXISTS `transaction` (
  `id` bigint(20) NOT NULL auto_increment,
  `user_id` int(11) NOT NULL,
  `merchant_request_id` bigint(20) NOT NULL,
  `pfm_transaction_number` bigint(20) NOT NULL,
  `payment_status_code` float(18,2) default NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL default '0',
  `created_by` int(11) default NULL,
  `updated_by` int(11) default NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `unique_parameter_idx` (`pfm_transaction_number`),
  KEY `merchant_request_id_idx` (`merchant_request_id`),
  KEY `user_id_idx` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `transaction`
--


-- --------------------------------------------------------

--
-- Table structure for table `user_detail`
--

CREATE TABLE IF NOT EXISTS `user_detail` (
  `id` bigint(20) NOT NULL auto_increment,
  `user_id` int(11) NOT NULL,
  `name` varchar(255) default NULL,
  `last_name` varchar(255) default NULL,
  `dob` date default NULL,
  `address` varchar(255) default NULL,
  `email` varchar(255) default NULL,
  `mobile_no` varchar(20) default NULL,
  `work_phone` varchar(20) default NULL,
  `send_sms` enum('gsmno','workphone','both','none') default 'gsmno',
  `failed_attempt` smallint(6) default NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL default '0',
  `created_by` int(11) default NULL,
  `updated_by` int(11) default NULL,
  PRIMARY KEY  (`id`),
  KEY `user_id_idx` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `user_detail`
--


-- --------------------------------------------------------

--
-- Table structure for table `validation_rules`
--

CREATE TABLE IF NOT EXISTS `validation_rules` (
  `id` bigint(20) NOT NULL auto_increment,
  `merchant_service_id` bigint(20) NOT NULL,
  `param_name` varchar(100) collate utf8_unicode_ci default NULL,
  `param_description` text collate utf8_unicode_ci,
  `is_mandatory` tinyint(1) default NULL,
  `param_type` enum('integer','string') collate utf8_unicode_ci default NULL,
  `mapped_to` enum('iparam_one','iparam_two','iparam_three','iparam_four','iparam_five','iparam_six','iparam_seven','iparam_eight','iparam_nine','iparam_ten','sparam_one','sparam_two','sparam_three','sparam_four','sparam_five','sparam_six','sparam_seven','sparam_eight','sparam_nine','sparam_ten') collate utf8_unicode_ci default NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL default '0',
  `created_by` int(11) default NULL,
  `updated_by` int(11) default NULL,
  PRIMARY KEY  (`id`),
  KEY `merchant_service_id_idx` (`merchant_service_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

--
-- Dumping data for table `validation_rules`
--


--
-- Constraints for dumped tables
--

--
-- Constraints for table `ep_job_data`
--
ALTER TABLE `ep_job_data`
  ADD CONSTRAINT `ep_job_data_job_execution_id_ep_job_execution_id` FOREIGN KEY (`job_execution_id`) REFERENCES `ep_job_execution` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ep_job_execution`
--
ALTER TABLE `ep_job_execution`
  ADD CONSTRAINT `ep_job_execution_job_id_ep_job_id` FOREIGN KEY (`job_id`) REFERENCES `ep_job` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ep_job_parameters`
--
ALTER TABLE `ep_job_parameters`
  ADD CONSTRAINT `ep_job_parameters_job_id_ep_job_id` FOREIGN KEY (`job_id`) REFERENCES `ep_job` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ep_job_queue`
--
ALTER TABLE `ep_job_queue`
  ADD CONSTRAINT `ep_job_queue_job_id_ep_job_id` FOREIGN KEY (`job_id`) REFERENCES `ep_job` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ep_job_repeat_schedule`
--
ALTER TABLE `ep_job_repeat_schedule`
  ADD CONSTRAINT `ep_job_repeat_schedule_job_id_ep_job_id` FOREIGN KEY (`job_id`) REFERENCES `ep_job` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `merchant_item`
--
ALTER TABLE `merchant_item`
  ADD CONSTRAINT `merchant_item_merchant_service_id_merchant_service_id` FOREIGN KEY (`merchant_service_id`) REFERENCES `merchant_service` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `merchant_request`
--
ALTER TABLE `merchant_request`
  ADD CONSTRAINT `merchant_request_merchant_id_merchant_id` FOREIGN KEY (`merchant_id`) REFERENCES `merchant` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `merchant_request_merchant_item_id_merchant_item_id` FOREIGN KEY (`merchant_item_id`) REFERENCES `merchant_item` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `merchant_request_merchant_service_id_merchant_service_id` FOREIGN KEY (`merchant_service_id`) REFERENCES `merchant_service` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `merchant_request_details`
--
ALTER TABLE `merchant_request_details`
  ADD CONSTRAINT `merchant_request_details_merchant_item_id_merchant_item_id` FOREIGN KEY (`merchant_item_id`) REFERENCES `merchant_item` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `merchant_request_details_merchant_request_id_merchant_request_id` FOREIGN KEY (`merchant_request_id`) REFERENCES `merchant_request` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `merchant_request_details_merchant_service_id_merchant_service_id` FOREIGN KEY (`merchant_service_id`) REFERENCES `merchant_service` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `merchant_service`
--
ALTER TABLE `merchant_service`
  ADD CONSTRAINT `merchant_service_merchant_id_merchant_id` FOREIGN KEY (`merchant_id`) REFERENCES `merchant` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `request`
--
ALTER TABLE `request`
  ADD CONSTRAINT `request_transaction_id_transaction_id` FOREIGN KEY (`transaction_id`) REFERENCES `transaction` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `response`
--
ALTER TABLE `response`
  ADD CONSTRAINT `response_request_id_request_id` FOREIGN KEY (`request_id`) REFERENCES `request` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `sf_guard_group_permission`
--
ALTER TABLE `sf_guard_group_permission`
  ADD CONSTRAINT `sf_guard_group_permission_group_id_sf_guard_group_id` FOREIGN KEY (`group_id`) REFERENCES `sf_guard_group` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `sf_guard_group_permission_permission_id_sf_guard_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `sf_guard_permission` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `sf_guard_remember_key`
--
ALTER TABLE `sf_guard_remember_key`
  ADD CONSTRAINT `sf_guard_remember_key_user_id_sf_guard_user_id` FOREIGN KEY (`user_id`) REFERENCES `sf_guard_user` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `sf_guard_user_group`
--
ALTER TABLE `sf_guard_user_group`
  ADD CONSTRAINT `sf_guard_user_group_group_id_sf_guard_group_id` FOREIGN KEY (`group_id`) REFERENCES `sf_guard_group` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `sf_guard_user_group_user_id_sf_guard_user_id` FOREIGN KEY (`user_id`) REFERENCES `sf_guard_user` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `sf_guard_user_permission`
--
ALTER TABLE `sf_guard_user_permission`
  ADD CONSTRAINT `sf_guard_user_permission_permission_id_sf_guard_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `sf_guard_permission` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `sf_guard_user_permission_user_id_sf_guard_user_id` FOREIGN KEY (`user_id`) REFERENCES `sf_guard_user` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `transaction`
--
ALTER TABLE `transaction`
  ADD CONSTRAINT `transaction_merchant_request_id_merchant_request_id` FOREIGN KEY (`merchant_request_id`) REFERENCES `merchant_request` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `transaction_user_id_sf_guard_user_id` FOREIGN KEY (`user_id`) REFERENCES `sf_guard_user` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `user_detail`
--
ALTER TABLE `user_detail`
  ADD CONSTRAINT `user_detail_user_id_sf_guard_user_id` FOREIGN KEY (`user_id`) REFERENCES `sf_guard_user` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `validation_rules`
--
ALTER TABLE `validation_rules`
  ADD CONSTRAINT `validation_rules_merchant_service_id_merchant_service_id` FOREIGN KEY (`merchant_service_id`) REFERENCES `merchant_service` (`id`) ON DELETE CASCADE;


--
-- Dumping data for table `merchant_service`
--


INSERT INTO `merchant` (`id`, `name`, `merchant_code`, `merchant_key`, `auth_info`, `created_at`, `updated_at`, `deleted`, `created_by`, `updated_by`) VALUES
(1, 'NIS', 1211523582, 'daa5a077f2b0493e94590c2d344acaa8', 'TVRJeE1UVXlNelU0TWpJek1ETTFNek0xTUE9PQ==', '2010-06-03 17:06:17', '2010-06-03 17:06:17', 0, NULL, NULL);



INSERT INTO `merchant_service` (`id`, `merchant_id`, `name`, `notification_url`, `merchant_home_page`, `version`, `clubbed`, `created_at`, `updated_at`, `deleted`, `created_by`, `updated_by`) VALUES
(1, 1, 'NIS Passport', 'http://spark.tekmindz.com/~cjajoria/v2.0.3.15/web/index.php/payforme/setResponse', 'http://spark.tekmindz.com/~cjajoria/v2.0.3.15/web/index.php', 'v1', 'no', '2010-06-03 17:07:00', '2010-06-03 17:07:00', 0, NULL, NULL);


INSERT INTO `sf_guard_group` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'portal_admin', 'Portal Admin', '2010-05-28 18:22:02', '2010-05-28 18:22:02'),
(2, 'payment_group', 'user', '2010-05-28 18:22:48', '2010-05-28 18:22:48');


INSERT INTO `sf_guard_permission` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'portal_admin', 'Portal Admin', '2010-05-28 18:25:12', '2010-05-28 18:25:12'),
(2, 'payment_user', 'User', '2010-05-28 18:26:04', '2010-05-28 18:26:04');

INSERT INTO `sf_guard_group_permission` (`group_id`, `permission_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2010-05-28 18:25:12', '2010-05-28 18:25:12'),
(2, 2, '2010-05-28 18:26:04', '2010-05-28 18:26:04');



INSERT INTO `sf_guard_user` (`id`, `username`, `algorithm`, `salt`, `password`, `is_active`, `is_super_admin`, `last_login`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'sha1', 'd693cbac2f2984cc9f395f0f436ac5a9', '230b9e013cbd1ab8fa2cb102878c52438b4bf70e', 1, 1, '2010-06-04 17:49:33', '2010-05-28 18:28:47', '2010-06-04 17:49:33'),
(2, 'user', 'sha1', '638fa059a6124d0be0e7be2761b81c19', 'b77a3dfb67d4f5575c656be0a2a79c6d272b3428', 1, 0, '2010-05-28 19:24:13', '2010-05-28 19:23:45', '2010-05-28 19:24:13');



INSERT INTO `sf_guard_user_group` (`user_id`, `group_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2010-05-28 18:28:47', '2010-05-28 18:28:47'),
(2, 2, '2010-05-28 19:23:45', '2010-05-28 19:23:45');



INSERT INTO `sf_guard_user_permission` (`user_id`, `permission_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2010-05-28 18:28:47', '2010-05-28 18:28:47'),
(2, 2, '2010-05-28 19:23:45', '2010-05-28 19:23:45');



INSERT INTO `validation_rules` (`id`, `merchant_service_id`, `param_name`, `param_description`, `is_mandatory`, `param_type`, `mapped_to`, `created_at`, `updated_at`, `deleted`, `created_by`, `updated_by`) VALUES
(1, 1, 'app_id', 'Application Id', 1, 'integer', 'iparam_one', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, NULL),
(2, 1, 'ref_num', 'Reference No.', 1, 'integer', 'iparam_two', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, NULL);


