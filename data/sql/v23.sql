 ALTER TABLE `bill` ADD UNIQUE (
`bill_number`
) ;

UPDATE `merchant` SET `user_id` = '6' WHERE `merchant`.`id` =4 LIMIT 1 ;

delete from ep_master_account where id in (3);
delete from sf_guard_user where id in (6);

INSERT INTO `ep_master_account` (`id`, `clear_balance`, `unclear_balance`, `account_number`, `account_name`, `type`, `sortcode`, `bankname`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(3, 0, 0, 56567575, 'grayPay', 'collection', NULL, NULL, NULL, '2010-10-05 14:43:53', 29, 29);


INSERT INTO `sf_guard_user` (`id`, `username`, `algorithm`, `salt`, `password`, `is_active`, `is_super_admin`, `last_login`, `created_at`, `updated_at`) VALUES
(6, 'grayPay', 'sha1', 'b8becaac5c658ed503818433172af98e', 'b9b75c8dd25d7f6011d6969e90b761b04687ed8d', 1, 0, NULL, '2010-06-14 15:34:55', '2010-06-14 15:34:55'),

INSERT INTO `user_detail` (`user_id`, `first_name`, `last_name`, `dob`, `address`, `email`, `mobile_phone`, `work_phone`, `failed_attempt`, `kyc_status`, `disapprove_reason`, `approved_by`, `master_account_id`, `created_at`, `updated_at`, `deleted`, `created_by`, `updated_by`) VALUES
(6, 'grayPay', 'grayPay', NULL, NULL, NULL, NULL, NULL, NULL, 0, '', 0, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, NULL);

 ALTER TABLE `gateway` CHANGE `merchant_id` `merchant_id` BIGINT( 20 ) NULL  ;

INSERT INTO `gateway` (`id`, `name`, `merchant_id`) VALUES
(3, 'ewallet', NULL);


INSERT INTO `transaction_charges` (`id`, `gateway_id`, `transaction_charges`, `split_type`, `created_at`, `updated_at`, `deleted`, `created_by`, `updated_by`, `version`) VALUES
(3, 3, 6.00, 'percentage', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, NULL, 0);

ALTER TABLE `ipay4me_order` ADD `validation_number` BIGINT NOT NULL AFTER `order_number` ; 
/*Data migration script*/
INSERT INTO `bill` (`bill_number`,`user_id`, `order_request_id`,`expiry_date`,`status`,`created_at`,`updated_at`,`deleted`,`created_by`,`updated_by`) select FLOOR(00000000 + (RAND() * 99999999)), user_id, order_request_id, NULL , 'paid' , created_at, updated_at, deleted, created_by, updated_by from order_request_details where payment_status='0'
