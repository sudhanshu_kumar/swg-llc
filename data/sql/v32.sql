ALTER TABLE `user_billing_profile` ADD `card_first` INT( 11 ) NOT NULL AFTER `phone` ,
ADD `card_last` INT( 11 ) NOT NULL AFTER `card_first` ,
ADD `card_len` SMALLINT( 6 ) NULL AFTER `card_last` ,
ADD `card_type` CHAR( 1 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL AFTER `card_len` ,
ADD `expiry_month` VARCHAR( 5 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL AFTER `card_type` ,
ADD `expiry_year` VARCHAR( 10 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL AFTER `expiry_month` ,
ADD `vault_id` BIGINT( 20 ) NOT NULL AFTER `expiry_year` ;