CREATE TABLE IF NOT EXISTS `ep_nmi_request` (
  `id` bigint(20) NOT NULL auto_increment,
  `order_id` varchar(100) NOT NULL,
  `transaction_id` bigint(11) default NULL,
  `amount` float(18,2) default NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_by` int(11) default NULL,
  `updated_by` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

CREATE TABLE IF NOT EXISTS `ep_nmi_response` (
  `id` bigint(20) NOT NULL auto_increment,
  `nmi_req_id` bigint(20) NOT NULL,
  `token_id` varchar(50) NOT NULL,
  `order_id` varchar(100) NOT NULL,
  `result` int(11) default NULL,
  `result_text` varchar(20) default NULL,
  `transaction_id` bigint(11) default NULL,
  `result_code` varchar(20) default NULL,
  `authorization_code` double(18,2) default NULL,
  `avs_result` varchar(30) NOT NULL,
  `action_type` varchar(30) default 'sale',
  `amount` float(18,2) default NULL,
  `ip_address` varchar(20) default NULL,
  `industry` varchar(50) default NULL,
  `city` varchar(50) default NULL,
  `processor_id` double(18,2) default NULL,
  `currency` varchar(20) default NULL,
  `tax_amount` varchar(50) default NULL,
  `shipping_amount` double(18,2) default NULL,
  `billing` varchar(255) default NULL,
  `first_name` varchar(30) default NULL,
  `last_name` varchar(30) default NULL,
  `email` varchar(100) default NULL,
  `phone` varchar(20) default NULL,
  `address1` varchar(255) default NULL,
  `address2` varchar(255) default NULL,
  `town` varchar(255) default NULL,
  `postal` varchar(55) default NULL,
  `country` varchar(55) default NULL,
  `state` varchar(255) default NULL,
  `cc_number` varchar(255) default NULL,
  `cc_exp` datetime default NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_by` int(11) default NULL,
  `updated_by` int(11) default NULL,
  PRIMARY KEY  (`id`),
  KEY `nmi_req_id_idx` (`nmi_req_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

ALTER TABLE `ep_nmi_response` ADD FOREIGN KEY ( `nmi_req_id` ) REFERENCES `ep_nmi_request` (
`id`
);


INSERT INTO `merchant` (
`id` ,
`name` ,
`merchant_code` ,
`merchant_key` ,
`notification_url` ,
`homepage_url` ,
`address` ,
`email` ,
`contact_phone` ,
`description` ,
`abbr` ,
`user_id` ,
`created_at` ,
`updated_at` ,
`deleted` ,
`created_by` ,
`updated_by`
)
VALUES (
7 , 'NMI', NULL , NULL , '', '', NULL , NULL , NULL , NULL , NULL , NULL , '', '', '0', NULL , NULL
);

INSERT INTO `gateway` (
`id` ,
`name` ,
`display_name` ,
`merchant_id`
)
VALUES (
'5', 'NMI', 'NMI', '7'
);

INSERT INTO `pay4meInt`.`transaction_charges` (
`id` ,
`gateway_id` ,
`transaction_charges` ,
`split_type` ,
`created_at` ,
`updated_at` ,
`deleted` ,
`created_by` ,
`updated_by` ,
`version`
)
VALUES (
'5', '5', '6.00', 'percentage', '2010-12-01 23:10:57', '', '0', '1', NULL , ''
);
INSERT INTO `ip4m`.`transaction_charges` (
`id` ,
`gateway_id` ,
`transaction_charges` ,
`split_type` ,
`created_at` ,
`updated_at` ,
`deleted` ,
`created_by` ,
`updated_by` ,
`version`
)
VALUES (
NULL , '5', '4.00', 'percentage', '2010-10-19 15:11:51', '2010-10-19 15:11:51', '0', NULL , NULL , '0'
);

