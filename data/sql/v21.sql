
ALTER TABLE `user_detail` ADD `kyc_status` SMALLINT( 6 ) NOT NULL DEFAULT '0' COMMENT '0= not applied , 1=Approved, 2=Applied, 3=Rejected' AFTER `failed_attempt` ,
ADD `disapprove_reason` VARCHAR( 255 ) NOT NULL AFTER `kyc_status` ,
ADD `approved_by` SMALLINT( 6 ) NOT NULL COMMENT '1=admin,2-withPin' AFTER `disapprove_reason` ;

CREATE TABLE bill (id BIGINT AUTO_INCREMENT, bill_number INT NOT NULL, user_id INT NOT NULL, order_request_id BIGINT NOT NULL, expiry_date DATETIME, status VARCHAR(255) DEFAULT 'active' NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, deleted TINYINT(1) DEFAULT '0' NOT NULL, created_by INT, updated_by INT, INDEX order_request_id_idx (order_request_id), INDEX user_id_idx (user_id), PRIMARY KEY(id)) ENGINE = INNODB;


ALTER TABLE bill ADD CONSTRAINT bill_user_id_sf_guard_user_id FOREIGN KEY (user_id) REFERENCES sf_guard_user(id) ON DELETE CASCADE;
ALTER TABLE bill ADD CONSTRAINT bill_order_request_id_order_request_id FOREIGN KEY (order_request_id) REFERENCES order_request(id) ON DELETE CASCADE;

-------------------------
CREATE TABLE recharge_order (id BIGINT AUTO_INCREMENT, user_id INT NOT NULL, account_no INT NOT NULL, amount FLOAT(18, 2) NOT NULL, payment_status ENUM('0', '1') DEFAULT '1', paid_date DATETIME, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, deleted TINYINT(1) DEFAULT '0' NOT NULL, created_by INT, updated_by INT, INDEX user_id_idx (user_id), PRIMARY KEY(id)) ENGINE = INNODB;


CREATE TABLE ipay4me_recharge_order (id BIGINT AUTO_INCREMENT, order_number BIGINT NOT NULL, gateway_id SMALLINT NOT NULL, recharge_order_id BIGINT NOT NULL, payment_status ENUM('0', '1') DEFAULT '1', response_code VARCHAR(30), response_text TEXT, payor_name VARCHAR(255), card_holder VARCHAR(255), card_first INT, card_last INT, card_len SMALLINT, card_type VARCHAR(10), address VARCHAR(255), phone VARCHAR(20), created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, created_by INT, updated_by INT, UNIQUE INDEX unique_parameter_idx (order_number), INDEX gateway_id_idx (gateway_id), PRIMARY KEY(id)) ENGINE = INNODB;


ALTER TABLE recharge_order ADD CONSTRAINT recharge_order_user_id_sf_guard_user_id FOREIGN KEY (user_id) REFERENCES sf_guard_user(id) ON DELETE CASCADE;

ALTER TABLE ipay4me_recharge_order ADD CONSTRAINT ipay4me_recharge_order_gateway_id_gateway_id FOREIGN KEY (gateway_id) REFERENCES gateway(id);

INSERT INTO `ep_menu` (`id`, `name`, `label`, `parent_id`, `sfguardperm`, `url`, `eval`, `sequence`, `created_at`, `updated_at`, `deleted`, `created_by`, `updated_by`) VALUES
(18, 'Recharge ewallet', 'eWallet', 0, 'payment_user', '', 'menu/KycApproved', 5, now(), now(), 0, NULL, NULL);


INSERT INTO `ep_menu` (`id`,`name`, `label`, `parent_id`, `sfguardperm`, `url`, `eval`, `sequence`, `created_at`, `updated_at`, `deleted`, `created_by`, `updated_by`) VALUES
(19, 'Recharge ewallet', 'Recharge eWallet', 18, 'payment_user', 'ewallet/recharge', 'menu/KycApproved', 1, now(), now(), 0, NULL, NULL);

--------------------------
INSERT INTO `merchant` (
`id` ,
`name` ,
`merchant_code` ,
`merchant_key` ,
`notification_url` ,
`homepage_url` ,
`address` ,
`email` ,
`contact_phone` ,
`description` ,
`abbr` ,
`created_at` ,
`updated_at` ,
`deleted` ,
`created_by` ,
`updated_by`
)
VALUES ('3', 'Easy Pay', NULL , NULL , '', '', NULL , NULL , NULL , NULL , NULL , now(), now(), '0', NULL , NULL),
('4', 'Gray Pay', NULL , NULL , '', '', NULL , NULL , NULL , NULL , NULL , now(), now(), '0', NULL , NULL),
(NULL , 'iPay4me', NULL , NULL , '', '', NULL , NULL , NULL , NULL , NULL , now(), now(), '0', NULL , NULL);


ALTER TABLE `merchant` ADD user_id INT( 11 ) NULL AFTER `abbr` ;

ALTER TABLE `merchant` ADD INDEX ( `user_id` )  ;


ALTER TABLE `merchant` ADD FOREIGN KEY ( `user_id` ) REFERENCES `sf_guard_user` (
`id`
);

ALTER TABLE `user_detail` ADD `master_account_id` BIGINT NULL AFTER `failed_attempt` ;



CREATE TABLE IF NOT EXISTS `ep_master_account` (
  `id` bigint(20) NOT NULL auto_increment,
  `clear_balance` bigint(20) default '0',
  `unclear_balance` bigint(20) default '0',
  `account_number` BIGINT NOT NULL,
  `account_name` varchar(100) NOT NULL,
  `type` enum('collection','receiving','ewallet') NOT NULL,
  `sortcode` varchar(40) default NULL,
  `bankname` varchar(100) default NULL,
  `created_at` datetime default NULL,
  `updated_at` datetime default NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;



CREATE TABLE IF NOT EXISTS `ep_master_ledger` (
  `id` bigint(20) NOT NULL auto_increment,
  `master_account_id` bigint(20) default NULL,
  `entry_type` enum('credit','debit') NOT NULL,
  `is_cleared` enum('1','0') default NULL,
  `amount` bigint(20) NOT NULL,
  `description` varchar(255) NOT NULL,
  `transaction_date` datetime NOT NULL,
  `payment_transaction_number` bigint(20) NOT NULL,
  `created_at` datetime default NULL,
  `updated_at` datetime default NULL,
  `created_by` int(11) default NULL,
  `updated_by` int(11) default NULL,
  PRIMARY KEY  (`id`),
  KEY `master_account_id_idx` (`master_account_id`),
  KEY `payment_transaction_number` (`payment_transaction_number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


ALTER TABLE `user_detail` ADD INDEX ( `master_account_id` )  ;

ALTER TABLE `user_detail` ADD FOREIGN KEY ( `master_account_id` ) REFERENCES `ep_master_account` (`id`) ON DELETE CASCADE ;

ALTER TABLE `gateway` ADD `merchant_id` BIGINT NOT NULL  ;

 ALTER TABLE `gateway` ADD INDEX ( `merchant_id` )  ;

ALTER TABLE `gateway` ADD FOREIGN KEY ( `merchant_id` ) REFERENCES `ipay4me`.`merchant` (
`id`
) ON DELETE CASCADE ;


UPDATE `gateway` SET `merchant_id` = '4' WHERE `gateway`.`id` =1 LIMIT 1 ;

UPDATE `gateway` SET `merchant_id` = '3' WHERE `gateway`.`id` =2 LIMIT 1 ;

ALTER TABLE `transaction_charges` DROP FOREIGN KEY `transaction_charges_merchant_id_merchant_id` ;

ALTER TABLE `transaction_charges` DROP `merchant_id` ;






 ALTER TABLE `bill` CHANGE `status` `status` VARCHAR( 255 ) NOT NULL DEFAULT 'unpaid';



INSERT INTO `ep_menu` (`name`, `label`, `parent_id`, `sfguardperm`, `url`, `eval`, `sequence`, `created_at`, `updated_at`, `deleted`, `created_by`, `updated_by`) VALUES
('Unpaid Bills', 'Unpaid Bills', 3, 'payment_user', 'report/unpaidBills', NULL, 2, now(), now(), 0, NULL, NULL);


INSERT INTO `ep_menu` (`id`, `name`, `label`, `parent_id`, `sfguardperm`, `url`, `eval`, `sequence`, `created_at`, `updated_at`, `deleted`, `created_by`, `updated_by`) VALUES
(13, 'Pin', 'Pin Management', 0, 'payment_user', '', 'menu/pin', 2, now(), now(), 0, NULL, NULL);


INSERT INTO `ep_menu` (`name`, `label`, `parent_id`, `sfguardperm`, `url`, `eval`, `sequence`, `created_at`, `updated_at`, `deleted`, `created_by`, `updated_by`) VALUES
('Enter Pin', 'Enter Pin', 13, 'payment_user', 'ewallet_pin/new', NULL, 1, now(), now(), 0, NULL, NULL);

INSERT INTO `ep_menu` (`name`, `label`, `parent_id`, `sfguardperm`, `url`, `eval`, `sequence`, `created_at`, `updated_at`, `deleted`, `created_by`, `updated_by`) VALUES
('Regenerate Pin', 'Regenerate Pin', 13, 'payment_user', 'ewallet_pin/pinUpdation', NULL, 2, now(), now(), 0, NULL, NULL);

CREATE TABLE ewallet_pin (id BIGINT AUTO_INCREMENT, user_id INT NOT NULL, pin_number VARCHAR(255) NOT NULL, status VARCHAR(255) DEFAULT 'active' NOT NULL, no_of_retries SMALLINT DEFAULT 0, activated_till VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, deleted TINYINT(1) DEFAULT '0' NOT NULL, created_by INT, updated_by INT, INDEX user_id_idx (user_id), PRIMARY KEY(id)) ENGINE = INNODB;



ALTER TABLE ewallet_pin ADD CONSTRAINT ewallet_pin_user_id_sf_guard_user_id FOREIGN KEY (user_id) REFERENCES sf_guard_user(id) ON DELETE CASCADE;



ALTER TABLE `ewallet_pin` ADD `pin_retries` SMALLINT( 6 ) NOT NULL DEFAULT '0' AFTER `no_of_retries` ;


UPDATE `ep_menu` SET `eval` = 'menu/Kyc' WHERE `ep_menu`.`id` =13 LIMIT 1 ;

UPDATE `ep_menu` SET `eval` = 'menu/Kyc' WHERE `ep_menu`.`id` =14 LIMIT 1 ;

UPDATE `ep_menu` SET `eval` = 'menu/Kyc' WHERE `ep_menu`.`id` =15 LIMIT 1 ;

---------menu ------------------------
INSERT INTO `ep_menu` (`id`, `name`, `label`, `parent_id`, `sfguardperm`, `url`, `eval`, `sequence`, `created_at`, `updated_at`, `deleted`, `created_by`, `updated_by`) VALUES
(16, 'User Management', 'User Management', 0, 'portal_admin', '', '', 2, '2010-10-05 13:30:58', '2010-10-05 13:30:58', 0, NULL, NULL),
(17, 'New KYC eWallet User', 'New KYC eWallet User', 16, 'portal_admin', 'ewallet/newEwalletUser', NULL, 1, '2010-10-05 13:31:09', '2010-10-05 13:31:09', 0, NULL, NULL);

UPDATE `ep_menu` SET `eval` = 'menu/KycApplied' WHERE `ep_menu`.`id` =13 LIMIT 1 ;

UPDATE `ep_menu` SET `eval` = 'menu/KycApplied' WHERE `ep_menu`.`id` =14 LIMIT 1 ;

UPDATE `ep_menu` SET `eval` = 'menu/KycApplied' WHERE `ep_menu`.`id` =15 LIMIT 1 ;

UPDATE `ep_menu` SET `eval` = 'menu/KycApproved' WHERE `ep_menu`.`id` =17 LIMIT 1 ;

UPDATE `ep_menu` SET `eval` = 'menu/KycApproved' WHERE `ep_menu`.`id` =18 LIMIT 1 ;

UPDATE `ep_menu` SET `eval` = 'menu/KycApproved' WHERE `ep_menu`.`id` =19 LIMIT 1 ;
-----------------------------
delete from sf_guard_user where id in (4,5);

INSERT INTO `sf_guard_user` (`id`, `username`, `algorithm`, `salt`, `password`, `is_active`, `is_super_admin`, `last_login`, `created_at`, `updated_at`) VALUES
(4, 'ipay4me', 'sha1', 'ad2f9d30d7c1e14719a52e6f33b8b711', '734682f071903f9b6d753b5e26582bf381c8ae53', 1, 0, NULL, now(), now()),
(5, 'payEasy', 'sha1', 'ad2f9d30d7c1e14719a52e6f33b8b711', '734682f071903f9b6d753b5e26582bf381c8ae53', 1, 0, NULL, now(), now());

INSERT INTO `user_detail` (`user_id`, `first_name`, `last_name`, `dob`, `address`, `email`, `mobile_phone`, `work_phone`, `failed_attempt`, `kyc_status`, `disapprove_reason`, `approved_by`, `master_account_id`, `created_at`, `updated_at`, `deleted`, `created_by`, `updated_by`) VALUES
(4, 'ipay4me', 'ipay4me', NULL, NULL, NULL, NULL, NULL, NULL, 0, '', 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, NULL),
(5, 'Pay', 'Easy', NULL, NULL, NULL, NULL, NULL, NULL, 0, '', 0, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, NULL);


INSERT INTO `ep_master_account` (`id`, `clear_balance`, `unclear_balance`, `account_number`, `account_name`, `type`, `sortcode`, `bankname`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(1, 470, 0, 75847583475, 'iPay4me', 'collection', NULL, NULL, NULL, '2010-10-05 14:43:53', 29, 29),
(2, 30, 0, 573457348753, 'PayEasy', 'collection', NULL, NULL, NULL, '2010-10-05 14:43:53', 29, 29);


UPDATE `merchant` SET `user_id` = '5' WHERE `merchant`.`id` =3 LIMIT 1 ;

UPDATE `merchant` SET `user_id` = '4' WHERE `merchant`.`id` =5 LIMIT 1 ;

UPDATE `ep_menu` SET `eval` = 'menu/KycApproved' WHERE `ep_menu`.`id` =12 LIMIT 1 ;
INSERT INTO `ep_menu` (`id`, `name`, `label`, `parent_id`, `sfguardperm`, `url`, `eval`, `sequence`, `created_at`, `updated_at`, `deleted`, `created_by`, `updated_by`) VALUES
(20, 'eWallet Statement', 'eWallet Statement', 3, 'payment_user', 'ewallet/acctStmtSearch', 'menu/KycApproved', 2, '2010-10-05 14:58:41', '2010-10-05 14:58:41', 0, NULL, NULL);

UPDATE `ep_menu` SET `url` = 'bill/unpaidBills' WHERE `ep_menu`.`id` =12 LIMIT 1 ;



INSERT INTO `ep_menu` (`id`, `name`, `label`, `parent_id`, `sfguardperm`, `url`, `eval`, `sequence`, `created_at`, `updated_at`, `deleted`, `created_by`, `updated_by`) VALUES
(21, 'Bill', 'Bill', 0, 'payment_user', '', NULL, 2, now(), now(), 0, NULL, NULL);

UPDATE `ep_menu` SET `parent_id` = '21' WHERE `ep_menu`.`id` =12 LIMIT 1 ;

ALTER TABLE `recharge_order` ADD `is_sms_charge_paid` ENUM( 'yes', 'no' ) NOT NULL DEFAULT 'no' AFTER `sms_charge`




