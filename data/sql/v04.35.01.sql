ALTER TABLE `order_request` CHANGE `currency` `currency` TINYINT NOT NULL DEFAULT '1';
UPDATE `order_request` SET currency = '5' WHERE convert_amount > 0;
ALTER TABLE `order_request` CHANGE `currency` `currency` TINYINT( 4 ) NOT NULL DEFAULT '1' COMMENT '1 => dollar; 4 => yuan; 5 => pound';
ALTER TABLE  `order_request` ADD INDEX (  `currency` );

ALTER TABLE `order_request_details` CHANGE `currency` `currency` TINYINT NOT NULL DEFAULT '1';
UPDATE `order_request_details` SET currency = '5' WHERE convert_amount > 0;
ALTER TABLE `order_request_details` CHANGE `currency` `currency` TINYINT( 4 ) NOT NULL DEFAULT '1' COMMENT '1 => dollar; 4 => yuan; 5 => pound';
ALTER TABLE  `order_request_details` ADD INDEX (  `currency` );

ALTER TABLE `transaction_service_charges` CHANGE `app_currency` `app_currency` TINYINT NOT NULL DEFAULT '1';
UPDATE `transaction_service_charges` SET app_currency = '5' WHERE app_convert_amount > 0;
ALTER TABLE `transaction_service_charges` CHANGE `app_currency` `app_currency` TINYINT( 4 ) NOT NULL DEFAULT '1' COMMENT '1 => dollar; 4 => yuan; 5 => pound';
ALTER TABLE  `transaction_service_charges` ADD INDEX (  `app_currency` );


ALTER TABLE cart_tracking_number CHANGE `currency` `currency` TINYINT( 4 ) NOT NULL DEFAULT '1' COMMENT '1 => dollar; 4 => yuan; 5 => pound';
ALTER TABLE  `cart_tracking_number` ADD INDEX (  `currency` );

ALTER TABLE money_order CHANGE `currency` `currency` TINYINT( 4 ) NOT NULL DEFAULT '1' COMMENT '1 => dollar; 4 => yuan; 5 => pound';
ALTER TABLE  `money_order` ADD INDEX (  `currency` );


