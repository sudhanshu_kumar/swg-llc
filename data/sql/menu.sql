CREATE TABLE IF NOT EXISTS `ep_menu` (
  `id` bigint(20) NOT NULL auto_increment,
  `name` varchar(100) default NULL,
  `label` varchar(100) default NULL,
  `parent_id` bigint(20) NOT NULL,
  `sfguardperm` varchar(100) default NULL,
  `url` varchar(200) default NULL,
  `eval` varchar(100) default NULL,
  `sequence` bigint(20) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL default '0',
  `created_by` int(11) default NULL,
  `updated_by` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `ep_menu`
--

INSERT INTO `ep_menu` (`id`, `name`, `label`, `parent_id`, `sfguardperm`, `url`, `eval`, `sequence`, `created_at`, `updated_at`, `deleted`, `created_by`, `updated_by`) VALUES
(1, 'Home', 'Home', 0, 'portal_admin', NULL, NULL, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, NULL),
(2, 'Change Password', 'Change Password', 1, 'portal_admin', 'signup/changePassword', NULL, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, NULL),
(3, 'Payment Details', 'Payment Details', 0, 'payment_user', NULL, NULL, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, NULL),
(4, 'View Detail', 'View Detail', 3, 'payment_user', 'order/paymentHistory', NULL, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, NULL);
