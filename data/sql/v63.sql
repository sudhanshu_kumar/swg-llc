--- NIS Query ----
CREATE TABLE tbl_revenue_edit_request (id BIGINT AUTO_INCREMENT, current_amount BIGINT NOT NULL, date DATE NOT NULL, project_id BIGINT NOT NULL, account_id BIGINT NOT NULL, comments text NOT NULL, admin_comments text NOT NULL, status VARCHAR(255) DEFAULT 'Requested', created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, created_by INT, updated_by INT, INDEX project_id_idx (project_id), INDEX account_id_idx (account_id), PRIMARY KEY(id)) ENGINE = INNODB;
ALTER TABLE `tbl_revenue_edit_request` CHANGE `project_id` `project_id` INT( 11 ) NOT NULL ;

ALTER TABLE `tbl_revenue_edit_request` ADD FOREIGN KEY ( `project_id` ) REFERENCES `tbl_project_name` (
`id`
) ON DELETE CASCADE ;
ALTER TABLE `tbl_revenue_edit_request` ADD FOREIGN KEY ( `account_id` ) REFERENCES `tbl_bank_details` (
`id`
) ON DELETE CASCADE ;

ALTER TABLE `tbl_revenue_edit_request` CHANGE `status` `status` ENUM( 'Requested', 'Approved', 'Declined' ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT 'Requested';

---Ipay4me Query --
INSERT INTO `ep_menu` (
`id` ,
`name` ,
`label` ,
`parent_id` ,
`sfguardperm` ,
`url` ,
`eval` ,
`env` ,
`sequence` ,
`created_at` ,
`updated_at` ,
`deleted` ,
`created_by` ,
`updated_by`
)
VALUES (
NULL , 'RevenueEditRequest', 'Revenue Edit Request', '3', 'portal_admin', 'report/revenueEditRequest', NULL , 'custom', '6', '', '', '0', NULL , NULL
);
UPDATE `ep_menu` SET `label` = 'Revenue Management' WHERE `ep_menu`.`id` =39 LIMIT 1 ;

UPDATE `ep_menu` SET `label` = 'Revenue Report' WHERE `ep_menu`.`id` =40 LIMIT 1 ;


