CREATE TABLE wire_tracking_number (id BIGINT AUTO_INCREMENT, cart_id BIGINT NOT NULL, tracking_number VARCHAR(50) NOT NULL, order_number BIGINT, wire_tracking_id BIGINT, order_request_detail_id BIGINT NOT NULL, cart_amount FLOAT(18, 2) NOT NULL, user_id BIGINT, associated_date DATETIME, status VARCHAR(255) DEFAULT 'New', created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, created_by INT, updated_by INT, deleted TINYINT(1) DEFAULT '0' NOT NULL, INDEX wire_tracking_id_idx (wire_tracking_id), PRIMARY KEY(id)) ENGINE = INNODB;



CREATE TABLE wire_transfer (id BIGINT AUTO_INCREMENT, wire_transfer_number VARCHAR(30), amount FLOAT(18, 2) NOT NULL, wire_transfer_date DATETIME, paid_date DATETIME, address VARCHAR(255), wire_transfer_proof VARCHAR(255), phone VARCHAR(20), wire_transfer_office VARCHAR(100), comments VARCHAR(255), status VARCHAR(255) DEFAULT 'New', created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, created_by INT, updated_by INT, PRIMARY KEY(id)) ENGINE = INNODB;

ALTER TABLE wire_tracking_number ADD CONSTRAINT wire_tracking_number_wire_tracking_id_wire_transfer_id FOREIGN KEY (wire_tracking_id) REFERENCES wire_transfer(id);



INSERT INTO `gateway` (
`id` ,
`name` ,
`display_name` ,
`merchant_id`
)
VALUES (
NULL , 'Wt', 'Wire Transfer', '7'
);


INSERT INTO `payment_mode` (
`id` ,
`card_type` ,
`gateway_id` ,
`group_id` ,
`display_name` ,
`created_at` ,
`updated_at` ,
`created_by` ,
`updated_by`
)
VALUES (
NULL , 'Wt', '7', '7.0', 'Wire Transfer', '', '', NULL , NULL
);


INSERT INTO `ep_menu` (`id`, `name`, `label`, `parent_id`, `sfguardperm`, `url`, `eval`, `env`, `sequence`, `created_at`, `updated_at`, `deleted`, `created_by`, `updated_by`) VALUES
(57, 'WireTransfer', 'Wire Transfer', 0, NULL, NULL, NULL, 'custom', 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, NULL),
(58, 'AssociateTrackingNumber', 'Associate Tracking Number', 57, 'payment_user', 'wt_configuration/saveWiretransfer', NULL, 'custom', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, NULL),
(59, 'WireTransferReport', 'Wire Transfer Report', 57, 'payment_user', 'supportTool/wireTransferReport', NULL, 'custom', 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, NULL);






INSERT INTO `ep_menu` (`id`, `name`, `label`, `parent_id`, `sfguardperm`, `url`, `eval`, `env`, `sequence`, `created_at`, `updated_at`, `deleted`, `created_by`, `updated_by`) VALUES
(60, 'AssociateTrackingNumberWt', 'Associate Tracking Number', 57, 'admin_support_user', 'wt_configuration/searchAppForWT', NULL, 'custom', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, NULL),
(61, 'ReceivePaymentsWt', 'Receive Payments', 57, 'admin_support_user', 'supportTool/searchByWireTransfer', NULL, 'custom', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, NULL),
(62, 'WireTransferReportWt', 'Wire Transfer Report', 57, 'admin_support_user', 'supportTool/wireTransferReport', NULL, 'custom', 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, NULL),
(63, 'DisAssociateWireTransfer', 'DisAssociate Wire Transfer', 57, 'admin_support_user', 'supportTool/searchByWireTransferNumber', NULL, 'custom', 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, NULL);

 
