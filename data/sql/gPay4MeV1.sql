-- phpMyAdmin SQL Dump
-- version 2.11.3deb1ubuntu1.3
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 30, 2010 at 05:20 PM
-- Server version: 5.0.51
-- PHP Version: 5.2.4-2ubuntu5.10

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `p4mint15`
--

-- --------------------------------------------------------

--
-- Table structure for table `currency_value`
--

CREATE TABLE IF NOT EXISTS `currency_value` (
  `id` bigint(20) NOT NULL auto_increment,
  `value` float(18,2) NOT NULL,
  `currency_date` datetime NOT NULL,
  `conversion_type` enum('naira_to_dollor','dollor_to_naira') NOT NULL default 'dollor_to_naira',
  `description` varchar(128) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `currency_value`
--

INSERT INTO `currency_value` (`id`, `value`, `currency_date`, `conversion_type`, `description`) VALUES
(1, 150.29, '2010-06-30 10:06:00', 'dollor_to_naira', 'From google API');

-- --------------------------------------------------------

--
-- Table structure for table `ep_action_audit_event`
--

CREATE TABLE IF NOT EXISTS `ep_action_audit_event` (
  `id` bigint(20) NOT NULL auto_increment,
  `user_id` int(11) default NULL,
  `username` varchar(128) default NULL,
  `ip_address` varchar(15) NOT NULL,
  `category` varchar(40) NOT NULL,
  `subcategory` varchar(40) NOT NULL,
  `description` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `user_id_idx` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ep_action_audit_event`
--


-- --------------------------------------------------------

--
-- Table structure for table `ep_action_audit_event_attributes`
--

CREATE TABLE IF NOT EXISTS `ep_action_audit_event_attributes` (
  `id` bigint(20) NOT NULL auto_increment,
  `audit_event_id` bigint(20) default NULL,
  `name` varchar(40) default NULL,
  `svalue` varchar(40) default NULL,
  `ivalue` bigint(20) default NULL,
  PRIMARY KEY  (`id`),
  KEY `audit_event_id_idx` (`audit_event_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ep_action_audit_event_attributes`
--


-- --------------------------------------------------------

--
-- Table structure for table `ep_job`
--

CREATE TABLE IF NOT EXISTS `ep_job` (
  `id` bigint(20) NOT NULL auto_increment,
  `name` varchar(40) default NULL,
  `url` varchar(50) default NULL,
  `sf_application` varchar(20) default NULL,
  `start_time` datetime default NULL,
  `end_time` datetime default NULL,
  `schedule_type` varchar(255) default 'once',
  `state` varchar(255) default 'active',
  `last_execution_status` varchar(255) default 'notexecuted',
  `max_retry_attempts` tinyint(4) default '0',
  `execution_attempts` tinyint(4) default NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ep_job`
--


-- --------------------------------------------------------

--
-- Table structure for table `ep_job_data`
--

CREATE TABLE IF NOT EXISTS `ep_job_data` (
  `id` bigint(20) NOT NULL auto_increment,
  `output_type` tinyint(4) default NULL,
  `output_text` longtext,
  `job_execution_id` bigint(20) default NULL,
  PRIMARY KEY  (`id`),
  KEY `ep_job_data_output_type_idx` (`output_type`),
  KEY `ep_job_data_job_execution_id_ep_job_execution_id` (`job_execution_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `ep_job_data`
--


-- --------------------------------------------------------

--
-- Table structure for table `ep_job_execution`
--

CREATE TABLE IF NOT EXISTS `ep_job_execution` (
  `id` bigint(20) NOT NULL auto_increment,
  `job_id` bigint(20) default NULL,
  `start_time` datetime default NULL,
  `end_time` datetime default NULL,
  `exit_code` smallint(6) default NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `job_id_idx` (`job_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `ep_job_execution`
--


-- --------------------------------------------------------

--
-- Table structure for table `ep_job_parameters`
--

CREATE TABLE IF NOT EXISTS `ep_job_parameters` (
  `id` bigint(20) NOT NULL auto_increment,
  `job_id` bigint(20) default NULL,
  `name` varchar(40) default NULL,
  `value` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `unique_parameter_idx` (`job_id`,`name`),
  KEY `job_id_idx` (`job_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=49 ;

--
-- Dumping data for table `ep_job_parameters`
--


-- --------------------------------------------------------

--
-- Table structure for table `ep_job_queue`
--

CREATE TABLE IF NOT EXISTS `ep_job_queue` (
  `id` bigint(20) NOT NULL auto_increment,
  `job_id` bigint(20) default NULL,
  `scheduled_start_time` datetime default NULL,
  `start_time` datetime default NULL,
  `current_status` varchar(255) default 'scheduled',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `schstrtindex_idx` (`scheduled_start_time`),
  KEY `strtindex_idx` (`start_time`),
  KEY `job_id_idx` (`job_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=82 ;

--
-- Dumping data for table `ep_job_queue`
--


-- --------------------------------------------------------

--
-- Table structure for table `ep_job_repeat_schedule`
--

CREATE TABLE IF NOT EXISTS `ep_job_repeat_schedule` (
  `id` bigint(20) NOT NULL auto_increment,
  `job_id` bigint(20) default NULL,
  `minutes` varchar(200) default NULL,
  `hours` varchar(200) default NULL,
  `day_of_month` varchar(200) default NULL,
  `month` varchar(200) default NULL,
  `day_of_week` varchar(200) default NULL,
  PRIMARY KEY  (`id`),
  KEY `job_id_idx` (`job_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `ep_job_repeat_schedule`
--


-- --------------------------------------------------------

--
-- Table structure for table `ep_menu`
--

CREATE TABLE IF NOT EXISTS `ep_menu` (
  `id` bigint(20) NOT NULL auto_increment,
  `name` varchar(100) default NULL,
  `label` varchar(100) default NULL,
  `parent_id` bigint(20) NOT NULL,
  `sfguardperm` varchar(100) default NULL,
  `url` varchar(200) default NULL,
  `eval` varchar(100) default NULL,
  `sequence` bigint(20) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL default '0',
  `created_by` int(11) default NULL,
  `updated_by` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `ep_menu`
--

INSERT INTO `ep_menu` (`id`, `name`, `label`, `parent_id`, `sfguardperm`, `url`, `eval`, `sequence`, `created_at`, `updated_at`, `deleted`, `created_by`, `updated_by`) VALUES
(1, 'Home', 'Home', 0, 'portal_admin', NULL, NULL, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, NULL),
(2, 'Change Password', 'Change Password', 1, 'portal_admin', 'sfGuardAuth/changePassword', NULL, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, NULL),
(3, 'License Management', 'License Management', 0, 'portal_admin', NULL, NULL, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, NULL),
(4, 'License Application', 'License Application', 3, 'portal_admin', 'license/index', NULL, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, NULL),
(7, 'Split Payment', 'Split Payment', 2, 'portal_admin', 'report/splitPaymentReport', NULL, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, NULL),
(8, 'User List', 'User List', 0, 'portal_admin', 'signup/userList', NULL, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ep_password_policy`
--

CREATE TABLE IF NOT EXISTS `ep_password_policy` (
  `id` int(11) NOT NULL auto_increment,
  `user_id` int(11) default NULL,
  `password` varchar(128) default NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `user_id_idx` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ep_password_policy`
--


-- --------------------------------------------------------

--
-- Table structure for table `ep_vbv_request`
--

CREATE TABLE IF NOT EXISTS `ep_vbv_request` (
  `id` bigint(20) NOT NULL auto_increment,
  `order_id` bigint(20) NOT NULL,
  `mer_id` varchar(100) default NULL,
  `acqbin` bigint(20) default NULL,
  `purchase_amount` bigint(20) default NULL,
  `visual_amount` double default NULL,
  `currency` smallint(6) default NULL,
  `ret_url_approve` text,
  `ret_url_decline` text,
  `post_url` text,
  `user_id` int(11) default NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `unique_parameter_idx` (`order_id`),
  KEY `order_id_idx` (`order_id`),
  KEY `user_id_idx` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ep_vbv_request`
--


-- --------------------------------------------------------

--
-- Table structure for table `ep_vbv_response`
--

CREATE TABLE IF NOT EXISTS `ep_vbv_response` (
  `id` bigint(20) NOT NULL auto_increment,
  `vbvrequest_id` bigint(20) NOT NULL,
  `msg_date` datetime default NULL,
  `version` varchar(20) default NULL,
  `order_id` varchar(200) default NULL,
  `transaction_type` varchar(100) default NULL,
  `pan` varchar(50) default NULL,
  `purchase_amount` float(18,2) default NULL,
  `currency` float(18,2) default NULL,
  `tran_date_time` datetime default NULL,
  `response_code` varchar(50) default NULL,
  `response_description` varchar(200) default NULL,
  `order_status` varchar(50) default NULL,
  `approval_code` varchar(50) default NULL,
  `order_description` varchar(100) default NULL,
  `approval_code_scr` varchar(100) default NULL,
  `purchase_amount_scr` varchar(100) default NULL,
  `currency_scr` varchar(50) default NULL,
  `order_status_scr` varchar(50) default NULL,
  `shop_order_id` varchar(50) default NULL,
  `three_ds_verificaion` varchar(50) default NULL,
  `response_xml` longblob NOT NULL,
  `payment_transaction_number` bigint(20) default NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `vbvrequest_id_idx` (`vbvrequest_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ep_vbv_response`
--


-- --------------------------------------------------------

--
-- Table structure for table `merchant`
--

CREATE TABLE IF NOT EXISTS `merchant` (
  `id` bigint(20) NOT NULL auto_increment,
  `name` varchar(100) NOT NULL,
  `merchant_code` int(11) NOT NULL,
  `merchant_key` varchar(255) default NULL,
  `notification_url` varchar(255) NOT NULL,
  `homepage_url` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL default '0',
  `created_by` int(11) default NULL,
  `updated_by` int(11) default NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `merchant`
--

INSERT INTO `merchant` (`id`, `name`, `merchant_code`, `merchant_key`, `notification_url`, `homepage_url`, `created_at`, `updated_at`, `deleted`, `created_by`, `updated_by`) VALUES
(1, 'Nis Passport', 23412, 'frwef34234', 'http://127.0.0.1:8080/pay4me_vbv/web/frontend_dev.php/ipayForMe/captureNotification', '', '2010-06-03 17:06:17', '2010-06-03 17:06:17', 0, NULL, NULL),
(2, 'Entry Visa', 893899666, '9d686658c99c23018136c861312f74b6', 'http://127.0.0.1:8080/pay4me_vbv/web/frontend_dev.php/ipayForMe/captureNotification', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, NULL),
(3, 'Freezone Visa', 1865109956, 'a1fcc493ae7ab6181a26dbac8af8067e', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, NULL),
(4, 'Swglobal', 73997547, '724af596633d3b8d0766d07c506e280b', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, NULL),
(5, 'MRP', 1404165639, 'f72a00d059b512c912bff644c46e45d6', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `order_request`
--

CREATE TABLE IF NOT EXISTS `order_request` (
  `id` bigint(20) NOT NULL auto_increment,
  `version` varchar(10) NOT NULL,
  `merchant_id` bigint(20) NOT NULL,
  `transaction_number` bigint(20) NOT NULL,
  `amount` float(18,2) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL default '0',
  `created_by` int(11) default NULL,
  `updated_by` int(11) default NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `unique_parameter_idx` (`merchant_id`,`transaction_number`),
  KEY `merchant_id_idx` (`merchant_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `order_request`
--


-- --------------------------------------------------------

--
-- Table structure for table `order_request_details`
--

CREATE TABLE IF NOT EXISTS `order_request_details` (
  `id` bigint(20) NOT NULL auto_increment,
  `order_request_id` bigint(20) NOT NULL,
  `user_id` int(11) default NULL,
  `merchant_id` bigint(20) NOT NULL,
  `retry_id` bigint(20) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `amount` float(18,2) NOT NULL,
  `payment_status` varchar(255) default '1',
  `paid_date` datetime default NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL default '0',
  `created_by` int(11) default NULL,
  `updated_by` int(11) default NULL,
  PRIMARY KEY  (`id`),
  KEY `order_request_id_idx` (`order_request_id`),
  KEY `merchant_id_idx` (`merchant_id`),
  KEY `user_id_idx` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `order_request_details`
--


-- --------------------------------------------------------

--
-- Table structure for table `order_request_split`
--

CREATE TABLE IF NOT EXISTS `order_request_split` (
  `id` bigint(20) NOT NULL auto_increment,
  `order_request_id` bigint(20) NOT NULL,
  `order_detail_id` bigint(20) NOT NULL,
  `amount` bigint(20) NOT NULL COMMENT 'Array',
  `merchant_code` int(11) NOT NULL,
  `merchant_id` bigint(20) NOT NULL,
  `pay_merchant_fee` varchar(255) default '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL default '0',
  `created_by` int(11) default NULL,
  `updated_by` int(11) default NULL,
  PRIMARY KEY  (`id`),
  KEY `order_request_id_idx` (`order_request_id`),
  KEY `order_detail_id_idx` (`order_detail_id`),
  KEY `merchant_id_idx` (`merchant_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=65 ;

--
-- Dumping data for table `order_request_split`
--


-- --------------------------------------------------------

--
-- Table structure for table `payment_response`
--

CREATE TABLE IF NOT EXISTS `payment_response` (
  `id` bigint(20) NOT NULL auto_increment,
  `request_order_number` bigint(20) NOT NULL,
  `response_code` tinyint(4) NOT NULL,
  `response_reason` varchar(10) default NULL,
  `response_reason_txt` varchar(255) default NULL,
  `response_reason_sub_code` tinyint(4) default NULL,
  `response_transaction_code` bigint(20) default NULL,
  `response_authorization_code` varchar(10) default NULL,
  `response_transaction_date` datetime default NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL default '0',
  `created_by` int(11) default NULL,
  `updated_by` int(11) default NULL,
  PRIMARY KEY  (`id`),
  KEY `request_order_number_idx` (`request_order_number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `payment_response`
--


-- --------------------------------------------------------

--
-- Table structure for table `request`
--

CREATE TABLE IF NOT EXISTS `request` (
  `id` bigint(20) NOT NULL auto_increment,
  `order_detail_id` bigint(20) NOT NULL,
  `request_order_number` bigint(20) NOT NULL,
  `amount` float(18,2) default NULL,
  `card_details_first` smallint(6) default NULL,
  `card_details_last` smallint(6) default NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL default '0',
  `created_by` int(11) default NULL,
  `updated_by` int(11) default NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `unique_parameter_idx` (`request_order_number`),
  KEY `request_order_number_idx` (`request_order_number`),
  KEY `order_detail_id_idx` (`order_detail_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `request`
--


-- --------------------------------------------------------

--
-- Table structure for table `response`
--

CREATE TABLE IF NOT EXISTS `response` (
  `id` bigint(20) NOT NULL auto_increment,
  `request_id` bigint(20) NOT NULL,
  `response_code` tinyint(4) NOT NULL,
  `response_reason` varchar(10) default NULL,
  `response_reason_txt` varchar(255) default NULL,
  `response_reason_sub_code` tinyint(4) default NULL,
  `response_transaction_code` bigint(20) default NULL,
  `response_authorization_code` varchar(10) default NULL,
  `response_transaction_date` datetime default NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL default '0',
  `created_by` int(11) default NULL,
  `updated_by` int(11) default NULL,
  PRIMARY KEY  (`id`),
  KEY `request_id_idx` (`request_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `response`
--


-- --------------------------------------------------------

--
-- Table structure for table `sf_guard_group`
--

CREATE TABLE IF NOT EXISTS `sf_guard_group` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) default NULL,
  `description` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `sf_guard_group`
--

INSERT INTO `sf_guard_group` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'portal_admin', 'Portal Admin', '2010-05-28 18:22:02', '2010-05-28 18:22:02'),
(2, 'admin', 'Admin', '2010-05-28 18:22:48', '2010-05-28 18:22:48'),
(3, 'payment_group', 'Payment Group', '2010-05-28 18:22:48', '2010-05-28 18:22:48');

-- --------------------------------------------------------

--
-- Table structure for table `sf_guard_group_permission`
--

CREATE TABLE IF NOT EXISTS `sf_guard_group_permission` (
  `group_id` int(11) NOT NULL default '0',
  `permission_id` int(11) NOT NULL default '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY  (`group_id`,`permission_id`),
  KEY `sf_guard_group_permission_permission_id_sf_guard_permission_id` (`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sf_guard_group_permission`
--

INSERT INTO `sf_guard_group_permission` (`group_id`, `permission_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2010-05-28 18:25:12', '2010-05-28 18:25:12'),
(2, 2, '2010-05-28 18:26:04', '2010-05-28 18:26:04'),
(3, 3, '2010-05-28 18:26:04', '2010-05-28 18:26:04');

-- --------------------------------------------------------

--
-- Table structure for table `sf_guard_permission`
--

CREATE TABLE IF NOT EXISTS `sf_guard_permission` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) default NULL,
  `description` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `sf_guard_permission`
--

INSERT INTO `sf_guard_permission` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'portal_admin', 'Portal Admin', '2010-05-28 18:25:12', '2010-05-28 18:25:12'),
(2, 'admin', 'Admin', '2010-05-28 18:26:04', '2010-05-28 18:26:04'),
(3, 'payment_user', 'Payment Group', '2010-05-28 18:26:04', '2010-05-28 18:26:04');

-- --------------------------------------------------------

--
-- Table structure for table `sf_guard_remember_key`
--

CREATE TABLE IF NOT EXISTS `sf_guard_remember_key` (
  `id` int(11) NOT NULL auto_increment,
  `user_id` int(11) default NULL,
  `remember_key` varchar(32) default NULL,
  `ip_address` varchar(50) NOT NULL default '',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY  (`id`,`ip_address`),
  KEY `user_id_idx` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `sf_guard_remember_key`
--


-- --------------------------------------------------------

--
-- Table structure for table `sf_guard_user`
--

CREATE TABLE IF NOT EXISTS `sf_guard_user` (
  `id` int(11) NOT NULL auto_increment,
  `username` varchar(128) NOT NULL,
  `algorithm` varchar(128) NOT NULL default 'sha1',
  `salt` varchar(128) default NULL,
  `password` varchar(128) default NULL,
  `is_active` tinyint(1) default '1',
  `is_super_admin` tinyint(1) default '0',
  `last_login` datetime default NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `username` (`username`),
  KEY `is_active_idx_idx` (`is_active`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `sf_guard_user`
--

INSERT INTO `sf_guard_user` (`id`, `username`, `algorithm`, `salt`, `password`, `is_active`, `is_super_admin`, `last_login`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'sha1', 'd693cbac2f2984cc9f395f0f436ac5a9', '904b4a58d90dde989b67fe9e27089f3697888b7a', 1, 1, '2010-06-29 11:37:18', '2010-05-28 18:28:47', '2010-06-29 11:37:18'),
(2, 'user', 'sha1', '638fa059a6124d0be0e7be2761b81c19', 'b77a3dfb67d4f5575c656be0a2a79c6d272b3428', 1, 0, '2010-06-24 11:11:52', '2010-05-28 19:23:45', '2010-06-24 11:11:52'),
(5, 'https://www.google.com/accounts/o8/id?id=AItOawndGmbn02OcAgFvAmhf7K0sDNtsnu-TYMo', 'sha1', 'a4534ec6eba923d2edf648c7880e7139', 'dcdf2bb9686f9cc2f6ab69346d1494e09425472c', 1, 0, '2010-06-17 10:17:50', '2010-06-14 18:19:53', '2010-06-17 10:17:50'),
(6, 'http://spark/~uprakash/ipay4me-openid/webdir/identity/shuchi', 'sha1', '0e83d1749b4822b022df3601066599ab', 'fa2b847c7091c64e2501eb1c4406bf5a2cad4eff', 1, 0, '2010-06-21 09:44:04', '2010-06-18 10:47:23', '2010-06-21 09:44:05');

-- --------------------------------------------------------

--
-- Table structure for table `sf_guard_user_group`
--

CREATE TABLE IF NOT EXISTS `sf_guard_user_group` (
  `user_id` int(11) NOT NULL default '0',
  `group_id` int(11) NOT NULL default '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY  (`user_id`,`group_id`),
  KEY `sf_guard_user_group_group_id_sf_guard_group_id` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sf_guard_user_group`
--

INSERT INTO `sf_guard_user_group` (`user_id`, `group_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2010-05-28 18:28:47', '2010-05-28 18:28:47'),
(2, 2, '2010-05-28 19:23:45', '2010-05-28 19:23:45'),
(5, 3, '2010-06-14 18:19:53', '2010-06-14 18:19:53'),
(6, 3, '2010-06-18 10:47:23', '2010-06-18 10:47:23');

-- --------------------------------------------------------

--
-- Table structure for table `sf_guard_user_permission`
--

CREATE TABLE IF NOT EXISTS `sf_guard_user_permission` (
  `user_id` int(11) NOT NULL default '0',
  `permission_id` int(11) NOT NULL default '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY  (`user_id`,`permission_id`),
  KEY `sf_guard_user_permission_permission_id_sf_guard_permission_id` (`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sf_guard_user_permission`
--


-- --------------------------------------------------------

--
-- Table structure for table `user_detail`
--

CREATE TABLE IF NOT EXISTS `user_detail` (
  `id` bigint(20) NOT NULL auto_increment,
  `user_id` int(11) NOT NULL,
  `first_name` varchar(255) default NULL,
  `last_name` varchar(255) default NULL,
  `dob` date default NULL,
  `address` varchar(255) default NULL,
  `email` varchar(255) default NULL,
  `mobile_phone` varchar(20) default NULL,
  `work_phone` varchar(20) default NULL,
  `failed_attempt` smallint(6) default NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL default '0',
  `created_by` int(11) default NULL,
  `updated_by` int(11) default NULL,
  PRIMARY KEY  (`id`),
  KEY `user_id_idx` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `user_detail`
--

INSERT INTO `user_detail` (`id`, `user_id`, `first_name`, `last_name`, `dob`, `address`, `email`, `mobile_phone`, `work_phone`, `failed_attempt`, `created_at`, `updated_at`, `deleted`, `created_by`, `updated_by`) VALUES
(3, 5, 'Shuchi', 'Sethi', NULL, 'shuchi', 'shuchi.sarpal@gmail.com', '9899573867', NULL, NULL, '2010-06-14 18:19:53', '2010-06-14 18:19:53', 0, 0, 0),
(4, 6, 'shuchi', 'sethi', NULL, '33/29 First Floor \r\nOld Rajinder nagar\r\nnew delhi', 'ssethi@swglobal.com', '+9899573867', NULL, NULL, '2010-06-18 10:47:23', '2010-06-18 10:47:23', 0, 0, 0),
(5, 2, 'user', 'user', '2010-06-17', NULL, NULL, NULL, NULL, 4, '0000-00-00 00:00:00', '2010-06-25 12:39:09', 0, NULL, 0),
(6, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2010-06-29 11:34:57', '2010-06-29 11:34:57', 0, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `vbv_order`
--

CREATE TABLE IF NOT EXISTS `vbv_order` (
  `id` bigint(20) NOT NULL auto_increment,
  `order_id` bigint(20) NOT NULL,
  `order_detail_id` bigint(20) NOT NULL,
  `type` varchar(255) NOT NULL default 'pay',
  PRIMARY KEY  (`id`),
  KEY `order_id_idx` (`order_id`),
  KEY `order_detail_id_idx` (`order_detail_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `vbv_order`
--


--
-- Constraints for dumped tables
--

--
-- Constraints for table `ep_action_audit_event`
--
ALTER TABLE `ep_action_audit_event`
  ADD CONSTRAINT `ep_action_audit_event_user_id_sf_guard_user_id` FOREIGN KEY (`user_id`) REFERENCES `sf_guard_user` (`id`);

--
-- Constraints for table `ep_action_audit_event_attributes`
--
ALTER TABLE `ep_action_audit_event_attributes`
  ADD CONSTRAINT `eaei` FOREIGN KEY (`audit_event_id`) REFERENCES `ep_action_audit_event` (`id`);

--
-- Constraints for table `ep_job_data`
--
ALTER TABLE `ep_job_data`
  ADD CONSTRAINT `ep_job_data_job_execution_id_ep_job_execution_id` FOREIGN KEY (`job_execution_id`) REFERENCES `ep_job_execution` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ep_job_execution`
--
ALTER TABLE `ep_job_execution`
  ADD CONSTRAINT `ep_job_execution_job_id_ep_job_id` FOREIGN KEY (`job_id`) REFERENCES `ep_job` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ep_job_parameters`
--
ALTER TABLE `ep_job_parameters`
  ADD CONSTRAINT `ep_job_parameters_job_id_ep_job_id` FOREIGN KEY (`job_id`) REFERENCES `ep_job` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ep_job_queue`
--
ALTER TABLE `ep_job_queue`
  ADD CONSTRAINT `ep_job_queue_job_id_ep_job_id` FOREIGN KEY (`job_id`) REFERENCES `ep_job` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ep_job_repeat_schedule`
--
ALTER TABLE `ep_job_repeat_schedule`
  ADD CONSTRAINT `ep_job_repeat_schedule_job_id_ep_job_id` FOREIGN KEY (`job_id`) REFERENCES `ep_job` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ep_password_policy`
--
ALTER TABLE `ep_password_policy`
  ADD CONSTRAINT `ep_password_policy_user_id_sf_guard_user_id` FOREIGN KEY (`user_id`) REFERENCES `sf_guard_user` (`id`);

--
-- Constraints for table `ep_vbv_request`
--
ALTER TABLE `ep_vbv_request`
  ADD CONSTRAINT `ep_vbv_request_user_id_sf_guard_user_id` FOREIGN KEY (`user_id`) REFERENCES `sf_guard_user` (`id`);

--
-- Constraints for table `ep_vbv_response`
--
ALTER TABLE `ep_vbv_response`
  ADD CONSTRAINT `ep_vbv_response_vbvrequest_id_ep_vbv_request_id` FOREIGN KEY (`vbvrequest_id`) REFERENCES `ep_vbv_request` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `order_request`
--
ALTER TABLE `order_request`
  ADD CONSTRAINT `order_request_merchant_id_merchant_id` FOREIGN KEY (`merchant_id`) REFERENCES `merchant` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `order_request_details`
--
ALTER TABLE `order_request_details`
  ADD CONSTRAINT `order_request_details_merchant_id_merchant_id` FOREIGN KEY (`merchant_id`) REFERENCES `merchant` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `order_request_details_order_request_id_order_request_id` FOREIGN KEY (`order_request_id`) REFERENCES `order_request` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `order_request_details_user_id_sf_guard_user_id` FOREIGN KEY (`user_id`) REFERENCES `sf_guard_user` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `order_request_split`
--
ALTER TABLE `order_request_split`
  ADD CONSTRAINT `order_request_split_merchant_id_merchant_id` FOREIGN KEY (`merchant_id`) REFERENCES `merchant` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `order_request_split_order_detail_id_order_request_details_id` FOREIGN KEY (`order_detail_id`) REFERENCES `order_request_details` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `order_request_split_order_request_id_order_request_id` FOREIGN KEY (`order_request_id`) REFERENCES `order_request` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `payment_response`
--
ALTER TABLE `payment_response`
  ADD CONSTRAINT `prrr` FOREIGN KEY (`request_order_number`) REFERENCES `request` (`request_order_number`) ON DELETE CASCADE;

--
-- Constraints for table `request`
--
ALTER TABLE `request`
  ADD CONSTRAINT `request_order_detail_id_order_request_details_id` FOREIGN KEY (`order_detail_id`) REFERENCES `order_request_details` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `response`
--
ALTER TABLE `response`
  ADD CONSTRAINT `response_request_id_request_id` FOREIGN KEY (`request_id`) REFERENCES `request` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `sf_guard_group_permission`
--
ALTER TABLE `sf_guard_group_permission`
  ADD CONSTRAINT `sf_guard_group_permission_group_id_sf_guard_group_id` FOREIGN KEY (`group_id`) REFERENCES `sf_guard_group` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `sf_guard_group_permission_permission_id_sf_guard_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `sf_guard_permission` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `sf_guard_remember_key`
--
ALTER TABLE `sf_guard_remember_key`
  ADD CONSTRAINT `sf_guard_remember_key_user_id_sf_guard_user_id` FOREIGN KEY (`user_id`) REFERENCES `sf_guard_user` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `sf_guard_user_group`
--
ALTER TABLE `sf_guard_user_group`
  ADD CONSTRAINT `sf_guard_user_group_group_id_sf_guard_group_id` FOREIGN KEY (`group_id`) REFERENCES `sf_guard_group` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `sf_guard_user_group_user_id_sf_guard_user_id` FOREIGN KEY (`user_id`) REFERENCES `sf_guard_user` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `sf_guard_user_permission`
--
ALTER TABLE `sf_guard_user_permission`
  ADD CONSTRAINT `sf_guard_user_permission_permission_id_sf_guard_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `sf_guard_permission` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `sf_guard_user_permission_user_id_sf_guard_user_id` FOREIGN KEY (`user_id`) REFERENCES `sf_guard_user` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `user_detail`
--
ALTER TABLE `user_detail`
  ADD CONSTRAINT `user_detail_user_id_sf_guard_user_id` FOREIGN KEY (`user_id`) REFERENCES `sf_guard_user` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `vbv_order`
--
ALTER TABLE `vbv_order`
  ADD CONSTRAINT `vbv_order_order_detail_id_order_request_details_id` FOREIGN KEY (`order_detail_id`) REFERENCES `order_request_details` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `vbv_order_order_id_ep_vbv_request_order_id` FOREIGN KEY (`order_id`) REFERENCES `ep_vbv_request` (`order_id`) ON DELETE CASCADE;

  
  INSERT INTO `ep_job` (`id`, `name`, `url`, `sf_application`, `start_time`, `end_time`, `schedule_type`, `state`, `last_execution_status`, `max_retry_attempts`, `execution_attempts`, `created_at`, `updated_at`) VALUES
(1, 'CurrencyJob', 'currencyExchange/storeNairaVal', 'frontend', '2010-06-30 11:25:40', '2020-03-12 12:12:12', 'repeated', 'active', 'pass', 2, 0, now(), now());

INSERT INTO `ep_job_queue` (`id`, `job_id`, `scheduled_start_time`, `start_time`, `current_status`, `created_at`, `updated_at`) VALUES
(16, 1, now(), NULL, 'scheduled', now(), now());


INSERT INTO `ep_job_parameters` (`id`, `job_id`, `name`, `value`, `created_at`, `updated_at`) VALUES
(1, 1, 'ep_int_hostname', 'localhost', now(), now()),
(1, 1, 'ep_int_schema', 'HTTP', now(), now());



INSERT INTO `ep_job_repeat_schedule` (`id`, `job_id`, `minutes`, `hours`, `day_of_month`, `month`, `day_of_week`) VALUES
(1, 1, '26', '11', '*', '*', NULL); 
