INSERT INTO `ep_menu` (`id`, `name`, `label`, `parent_id`, `sfguardperm`, `url`, `eval`, `env`, `sequence`, `created_at`, `updated_at`, `deleted`, `created_by`, `updated_by`) VALUES
(86, 'Country Payment Validations', 'Country Payment Validations', 83, 'configure_manager', 'configurationManager/countryPaymentModeValidations', 'privilageUser/index', 'custom', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, NULL),
(87, 'Country Wise Register Card Configuration', 'Country Wise Register Card Configuration', 83, 'configure_manager', 'configurationManager/view', NULL, 'custom', 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, NULL);

ALTER TABLE  `tbl_block_applicant` ADD  `app_id` BIGINT NOT NULL AFTER  `app_type` ,
ADD  `ref_no` BIGINT NOT NULL AFTER  `app_id` ,
ADD  `card_first` VARCHAR( 5 ) NOT NULL AFTER  `ref_no` ,
ADD  `card_last` VARCHAR( 5 ) NOT NULL AFTER  `card_first`,
ADD  `card_holder_name` VARCHAR( 255 ) NOT NULL AFTER  `card_last` ,
ADD  `email` VARCHAR( 100 ) NOT NULL AFTER  `card_holder_name` ,
ADD  `processing_country` VARCHAR( 2 ) NOT NULL AFTER  `email` ,
ADD  `embassy` BIGINT NOT NULL AFTER  `processing_country` ;
