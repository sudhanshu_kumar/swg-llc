CREATE TABLE ep_password_policy (id INT AUTO_INCREMENT, user_id INT, password VARCHAR(128), created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX user_id_idx (user_id), PRIMARY KEY(id)) ENGINE = INNODB;

ALTER TABLE ep_password_policy ADD CONSTRAINT ep_password_policy_user_id_sf_guard_user_id FOREIGN KEY (user_id) REFERENCES sf_guard_user(id);

UPDATE `sf_guard_group` SET `name` = 'admin',`description` = 'Admin' WHERE `sf_guard_group`.`id` =2 ;

UPDATE `sf_guard_permission` SET `name` = 'admin',`description` = 'Admin' WHERE `sf_guard_permission`.`id` =2  ;

INSERT INTO `ep_menu` (`id`, `name`, `label`, `parent_id`, `sfguardperm`, `url`, `eval`, `sequence`, `created_at`, `updated_at`, `deleted`, `created_by`, `updated_by`) VALUES
(7, 'Split Payment', 'Split Payment', 2, 'portal_admin', 'report/splitPaymentReport', NULL, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, NULL);

UPDATE `merchant` SET `name` = 'Nis Passport' WHERE `merchant`.`id` =1 LIMIT 1 ;

INSERT INTO `ep_menu` ( `id` , `name` , `label` , `parent_id` , `sfguardperm` , `url` , `eval` , `sequence` , `created_at` , `updated_at` , `deleted` , `created_by` , `updated_by` )
VALUES (
NULL , 'User List', 'User List', '0', 'portal_admin', 'signup/userList', NULL , '4', '', '', '0', NULL , NULL
);




UPDATE `ep_menu` SET `sfguardperm` = 'portal_admin'  WHERE `id` =2  ;