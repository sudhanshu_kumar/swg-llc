ALTER TABLE `mid_master` ADD `visa_amount_limit` INT( 11 ) NULL AFTER `validation` ,
ADD `master_amount_limit` INT( 11 ) NULL AFTER `visa_amount_limit` ,
ADD `cart_amount_limit` INT( 11 ) NULL AFTER `master_amount_limit` ;


update `mid_master` set visa_amount_limit = 15000,master_amount_limit=5000,cart_amount_limit=20000;
update `mid_master` set visa_amount_limit = 1500000,master_amount_limit=500000,cart_amount_limit=200000 where id='3' limit 1;

ALTER TABLE `gateway`
  DROP `visa_amount_limit`,
  DROP `master_amount_limit`,
  DROP `cart_amount_limit`;

UPDATE `tbl_country` SET `country_name` = 'Republic of Benin' WHERE `tbl_country`.`id` = 'BJ';

ALTER TABLE `ep_nmi_response` CHANGE `processor_id` `processor_id` TEXT NULL DEFAULT NULL  ;