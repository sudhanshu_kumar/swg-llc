CREATE TABLE app_sessions (sess_id VARCHAR(64), user_id INT, username VARCHAR(128), ip_address VARCHAR(50), sess_data TEXT NOT NULL, sess_time INT NOT NULL, INDEX user_id_idx (user_id), PRIMARY KEY(sess_id)) ENGINE = INNODB;

ALTER TABLE app_sessions ADD CONSTRAINT app_sessions_user_id_sf_guard_user_id FOREIGN KEY (user_id) REFERENCES sf_guard_user(id);