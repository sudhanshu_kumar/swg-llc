INSERT INTO `ep_menu` (
`id` ,
`name` ,
`label` ,
`parent_id` ,
`sfguardperm` ,
`url` ,
`eval` ,
`sequence` ,
`created_at` ,
`updated_at` ,
`deleted` ,
`created_by` ,
`updated_by`
)
VALUES (
NULL , 'Unblock Black Listed Cards', 'Unblock Black Listed Cards', '27', 'refund_support_user', 'supportTool/blackListedCards', NULL , '8', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0', NULL , NULL
);

CREATE TABLE IF NOT EXISTS `support_refund_list` (
  `id` bigint(20) NOT NULL auto_increment,
  `order_number` bigint(20) NOT NULL,
  `amount` float(18,2) NOT NULL,
  `refund_type` varchar(200) default NULL,
  `refund_desc` text,
  `message` text,
  `is_read` enum('no','yes') NOT NULL default 'no',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_by` int(11) default NULL,
  `updated_by` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

