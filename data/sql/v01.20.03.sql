CREATE TABLE mid_master (id BIGINT AUTO_INCREMENT, mid_service VARCHAR(10), validation VARCHAR(255), created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, created_by INT, updated_by INT, PRIMARY KEY(id)) ENGINE = INNODB;
INSERT INTO `mid_master` (`id`, `mid_service`, `validation`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(1, 'PS1', 'FPS,SN', '2011-09-13 15:36:30', '2011-09-13 15:36:30', NULL, NULL),
(2, 'LLC1', 'FPS,SN', '2011-09-13 15:36:30', '2011-09-13 15:36:30', NULL, NULL),
(3, 'JNA1', 'No', '2011-09-13 15:37:10', '2011-09-13 15:37:12', NULL, NULL);

ALTER TABLE `country_payment_mode` ADD `service` VARCHAR( 20 ) NOT NULL AFTER `card_type` ,
ADD `validation` VARCHAR( 25 ) NOT NULL AFTER `service` ;

TRUNCATE TABLE `country_payment_mode`;
INSERT INTO `country_payment_mode` (`id`, `country_code`, `card_type`, `cart_capacity`, `cart_amount_capacity`, `number_of_transaction`, `transaction_period`,`service`,`validation`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(1, 'ALL', 'jna_nmi_vbv', 5, 800, 1, 43200,'JNA1','No', '2011-07-07 19:34:09', '2011-07-07 19:34:12', NULL, NULL),
(2, 'JP', 'nmi_mcs,nmi_vbv,nmi_amx', 5, 800, 1, 43200,'PS1','FPS,SN', '2011-07-07 19:34:09', '2011-07-07 19:34:12', NULL, NULL),
(3, 'US', 'nmi_mcs,nmi_vbv,nmi_amx,Mo', 5, 800, 1, 43200,'PS1','FPS,SN', '2011-07-07 19:34:09', '2011-07-07 19:34:12', NULL, NULL),
(4, 'CA', 'nmi_mcs,nmi_vbv,nmi_amx,Mo', 5, 800, 1, 43200,'PS1','FPS,SN', '2011-07-07 19:34:09', '2011-07-07 19:34:12', NULL, NULL),
(5, 'FR', 'nmi_mcs,nmi_vbv,nmi_amx', 5, 800, 1, 43200,'PS1','FPS,SN', '2011-07-07 19:34:09', '2011-07-07 19:34:12', NULL, NULL),
(6, 'SE', 'nmi_mcs,nmi_vbv,nmi_amx', 5, 800, 1, 43200,'PS1','FPS,SN', '2011-07-07 19:34:09', '2011-07-07 19:34:12', NULL, NULL),
(7, 'AR', 'nmi_mcs,nmi_vbv,nmi_amx', 5, 800, 1, 43200,'PS1','FPS,SN', '2011-07-07 19:34:09', '2011-07-07 19:34:12', NULL, NULL),
(8, 'TT', 'nmi_mcs,nmi_vbv,nmi_amx', 5, 800, 1, 43200,'PS1','FPS,SN', '2011-07-07 19:34:09', '2011-07-07 19:34:12', NULL, NULL),
(9, 'IN', 'nmi_mcs,nmi_vbv,nmi_amx', 5, 800, 1, 43200,'PS1','FPS,SN', '2011-07-07 19:34:09', '2011-07-07 19:34:12', NULL, NULL),
(10, 'DE', 'A,M,V', 5, 800, 1, 43200,'LLC1','FPS,SN', '2011-07-07 19:34:09', '2011-07-07 19:34:12', NULL, NULL),
(11, 'AU', 'A,M,V', 5, 800, 1, 43200,'LLC1','FPS,SN', '2011-07-07 19:34:09', '2011-07-07 19:34:12', NULL, NULL),
(12, 'SG', 'A,M,V', 5, 800, 1, 43200,'LLC1','FPS,SN', '2011-07-07 19:34:09', '2011-07-07 19:34:12', NULL, NULL),
(13, 'BR', 'nmi_mcs,nmi_vbv,nmi_amx', 5, 800, 1, 43200,'PS1','FPS,SN', '2011-07-07 19:34:09', '2011-07-07 19:34:12', NULL, NULL);

DROP TABLE `country_based_mid_service`;