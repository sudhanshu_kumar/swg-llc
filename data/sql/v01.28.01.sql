INSERT INTO `ep_menu` (`name`, `label`, `parent_id`, `sfguardperm`, `url`, `eval`, `env`, `sequence`, `created_at`, `updated_at`, `deleted`, `created_by`, `updated_by`) VALUES ('NIS', 'NIS', '0', NULL, NULL, NULL, 'custom', '11', '', '', '1', NULL, NULL);

INSERT INTO `ep_menu` (`name`, `label`, `parent_id`, `sfguardperm`, `url`, `eval`, `env`, `sequence`, `created_at`, `updated_at`, `deleted`, `created_by`, `updated_by`) VALUES ('Edit NIS Application', 'Edit NIS Application', '98', 'configure_manager', 'supportTool/nisApplicationDetails', NULL, 'custom', '2', '', '', '1', NULL, NULL);


### Start ----  Below queries will run on NIS database ###

ALTER TABLE `tbl_country` CHANGE `id` `id` VARCHAR( 2 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;

CREATE TABLE tbl_vap_application (id BIGINT AUTO_INCREMENT, title ENUM('MR', 'MRS', 'MISS', 'DR'), first_name VARCHAR(50) NOT NULL, surname VARCHAR(50) NOT NULL, middle_name VARCHAR(50), gender ENUM('Male', 'Female'), marital_status ENUM('Single', 'Married', 'Widowed', 'Divorced', 'None') DEFAULT 'None', email VARCHAR(100), date_of_birth DATE, place_of_birth VARCHAR(100) NOT NULL, present_nationality_id VARCHAR(2), previous_nationality_id VARCHAR(2), hair_color ENUM('Black', 'Brown', 'White', 'Gray', 'None') DEFAULT 'None', eyes_color ENUM('Brown', 'Blue', 'Green', 'Gray', 'None') DEFAULT 'None', id_marks VARCHAR(100), height MEDIUMINT, permanent_address_id BIGINT, perm_phone_no VARCHAR(20), profession VARCHAR(100), office_address_id BIGINT, office_phone_no VARCHAR(20), applicant_type ENUM('GO', 'OC') COMMENT 'GO-Government Official, OC-Oil Company', vap_company_id BIGINT, customer_service_number VARCHAR(150), document_1 VARCHAR(150), document_2 VARCHAR(150), document_3 VARCHAR(150), document_4 VARCHAR(150), document_5 VARCHAR(150), boarding_place VARCHAR(150), flight_carrier VARCHAR(100), flight_number VARCHAR(100), issusing_govt VARCHAR(192) NOT NULL, passport_number VARCHAR(192) NOT NULL, date_of_issue DATE NOT NULL, date_of_exp DATE NOT NULL, place_of_issue VARCHAR(192) NOT NULL, applying_country_id VARCHAR(2) NOT NULL, type_of_visa VARCHAR(100), trip_money DECIMAL(18,2), local_company_name VARCHAR(255), local_company_address_id BIGINT, applied_nigeria_visa ENUM('Yes', 'No'), nigeria_visa_applied_place VARCHAR(255), applied_nigeria_visa_status ENUM('Granted', 'Rejected'), applied_nigeria_visa_reject_reason VARCHAR(255), have_visited_nigeria ENUM('Yes', 'No'), visited_reason_type_id BIGINT, applying_country_duration SMALLINT, contagious_disease ENUM('Yes', 'No'), police_case ENUM('Yes', 'No'), narcotic_involvement ENUM('Yes', 'No'), deported_status ENUM('Yes', 'No'), deported_county_id VARCHAR(2), visa_fraud_status ENUM('Yes', 'No'), processing_country_id BIGINT, processing_center_id BIGINT, ispaid TINYINT(1), is_email_valid TINYINT(1), ref_no BIGINT, payment_gateway_id BIGINT, paid_dollar_amount DECIMAL(18,2), paid_naira_amount DECIMAL(18,2), paid_date DATE, status ENUM('New', 'Paid', 'Vetted', 'Approved', 'Rejected') DEFAULT 'New', created_at DATETIME, updated_at DATETIME, INDEX vap_company_id_idx (vap_company_id), INDEX office_address_id_idx (office_address_id), INDEX permanent_address_id_idx (permanent_address_id), INDEX local_company_address_id_idx (local_company_address_id), INDEX processing_center_id_idx (processing_center_id), INDEX visited_reason_type_id_idx (visited_reason_type_id), INDEX applying_country_id_idx (applying_country_id), INDEX deported_county_id_idx (deported_county_id), INDEX previous_nationality_id_idx (previous_nationality_id), INDEX present_nationality_id_idx (present_nationality_id), INDEX processing_country_id_idx (processing_country_id), INDEX payment_gateway_id_idx (payment_gateway_id), PRIMARY KEY(id)) ENGINE = INNODB;

ALTER TABLE `tbl_vap_application` CHANGE `present_nationality_id` `present_nationality_id` VARCHAR( 2 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
CHANGE `previous_nationality_id` `previous_nationality_id` VARCHAR( 2 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
CHANGE `applying_country_id` `applying_country_id` VARCHAR( 2 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
CHANGE `deported_county_id` `deported_county_id` VARCHAR( 2 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
CHANGE `processing_country_id` `processing_country_id` VARCHAR( 2 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ;

CREATE TABLE tbl_vap_company (id BIGINT AUTO_INCREMENT, name VARCHAR(255), created_at DATETIME, updated_at DATETIME, created_by VARCHAR(80), updated_by VARCHAR(80), deleted TINYINT(1) DEFAULT '0' NOT NULL, PRIMARY KEY(id)) ENGINE = INNODB;

ALTER TABLE tbl_vap_application ADD FOREIGN KEY (visited_reason_type_id) REFERENCES global_master(id);
ALTER TABLE tbl_vap_application ADD FOREIGN KEY (vap_company_id) REFERENCES tbl_vap_company(id);
ALTER TABLE tbl_vap_application ADD FOREIGN KEY (processing_country_id) REFERENCES tbl_country(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE tbl_vap_application ADD FOREIGN KEY (processing_center_id) REFERENCES global_master(id);
ALTER TABLE tbl_vap_application ADD FOREIGN KEY (previous_nationality_id) REFERENCES tbl_country(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE tbl_vap_application ADD FOREIGN KEY (present_nationality_id) REFERENCES tbl_country(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE tbl_vap_application ADD FOREIGN KEY (permanent_address_id) REFERENCES tbl_international_address(id);
ALTER TABLE tbl_vap_application ADD FOREIGN KEY (payment_gateway_id) REFERENCES global_master(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE tbl_vap_application ADD FOREIGN KEY (office_address_id) REFERENCES tbl_international_address(id);
ALTER TABLE tbl_vap_application ADD FOREIGN KEY (local_company_address_id) REFERENCES tbl_address(id);
ALTER TABLE tbl_vap_application ADD FOREIGN KEY (deported_county_id) REFERENCES tbl_country(id);
ALTER TABLE tbl_vap_application ADD FOREIGN KEY (applying_country_id) REFERENCES tbl_country(id);

ALTER TABLE `tbl_ipay4me_payment_request` ADD `visa_arrival_program_id` BIGINT( 20 ) NOT NULL AFTER `freezone_id`;
ALTER TABLE  `tbl_ipay4me_payment_request` ADD INDEX (  `visa_arrival_program_id` );

ALTER TABLE tbl_ipay4me_payment_request ADD FOREIGN KEY (visa_arrival_program_id) REFERENCES tbl_vap_application(id);
ALTER TABLE `tbl_vap_application` ADD `payment_trans_id` VARCHAR( 50 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL AFTER `status` ;

### End ---- ###