RENAME TABLE `country_based_graypay_service`  TO `country_based_mid_service` ;

INSERT INTO `gateway` (
`id` ,
`name` ,
`display_name` ,
`merchant_id`
)
VALUES (
'9', 'NMI', 'NMI', '7'
);
ALTER TABLE `ep_nmi_request` ADD `gateway_id` BIGINT NOT NULL DEFAULT '5' AFTER `id` ;



TRUNCATE TABLE `country_payment_mode`;
INSERT INTO `country_payment_mode` (`id`, `country_code`, `card_type`, `cart_capacity`, `cart_amount_capacity`, `number_of_transaction`, `transaction_period`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(1, 'ALL', 'Vc', 5, 800, 1, 43200, '2011-07-07 19:34:09', '2011-07-07 19:34:12', NULL, NULL),
(2, 'IN', 'nmi_mcs,nmi_vbv,nmi_amx', 5, 800, 1, 43200, '2011-07-07 19:34:09', '2011-07-07 19:34:12', NULL, NULL),
(3, 'JP', 'nmi_mcs,nmi_vbv,nmi_amx', 5, 800, 1, 43200, '2011-07-07 19:34:09', '2011-07-07 19:34:12', NULL, NULL),
(4, 'IE', 'nmi_mcs,nmi_vbv,nmi_amx', 5, 800, 1, 43200, '2011-07-07 19:34:09', '2011-07-07 19:34:12', NULL, NULL),
(5, 'CA', 'nmi_mcs,nmi_vbv,nmi_amx,Mo', 5, 800, 1, 43200, '2011-07-07 19:34:09', '2011-07-07 19:34:12', NULL, NULL),
(6, 'US', 'nmi_mcs,nmi_vbv,nmi_amx,Mo', 5, 800, 1, 43200, '2011-07-07 19:34:09', '2011-07-07 19:34:12', NULL, NULL),
(7, 'AR', 'A,M,V', 5, 800, 1, 43200, '2011-07-07 19:34:09', '2011-07-07 19:34:12', NULL, NULL),
(8, 'AU', 'A,M,V', 5, 800, 1, 43200, '2011-07-07 19:34:09', '2011-07-07 19:34:12', NULL, NULL),
(9, 'AT', 'A,M,V', 5, 800, 1, 43200, '2011-07-07 19:34:09', '2011-07-07 19:34:12', NULL, NULL),
(10, 'BE', 'A,M,V', 5, 800, 1, 43200, '2011-07-07 19:34:09', '2011-07-07 19:34:12', NULL, NULL),
(11, 'BR', 'A,M,V', 5, 800, 1, 43200, '2011-07-07 19:34:09', '2011-07-07 19:34:12', NULL, NULL),
(12, 'EG', 'A,M,V', 5, 800, 1, 43200, '2011-07-07 19:34:09', '2011-07-07 19:34:12', NULL, NULL),
(13, 'FR', 'A,M,V', 5, 800, 1, 43200, '2011-07-07 19:34:09', '2011-07-07 19:34:12', NULL, NULL),
(14, 'DE', 'A,M,V', 5, 800, 1, 43200, '2011-07-07 19:34:09', '2011-07-07 19:34:12', NULL, NULL),
(15, 'HU', 'A,M,V', 5, 800, 1, 43200, '2011-07-07 19:34:09', '2011-07-07 19:34:12', NULL, NULL),
(16, 'PT', 'A,M,V', 5, 800, 1, 43200, '2011-07-07 19:34:09', '2011-07-07 19:34:12', NULL, NULL),
(17, 'RO', 'A,M,V', 5, 800, 1, 43200, '2011-07-07 19:34:09', '2011-07-07 19:34:12', NULL, NULL),
(18, 'SE', 'A,M,V', 5, 800, 1, 43200, '2011-07-07 19:34:09', '2011-07-07 19:34:12', NULL, NULL),
(19, 'TT', 'A,M,V', 5, 800, 1, 43200, '2011-07-07 19:34:09', '2011-07-07 19:34:12', NULL, NULL);

TRUNCATE TABLE `country_based_mid_service`;
INSERT INTO `country_based_mid_service` (`id`, `country_code`, `service`, `created_at`, `updated_at`, `deleted`, `created_by`, `updated_by`) VALUES
(1, 'ALL', 'MID2', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, NULL),
(2, 'US', 'MID1', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, NULL),
(3, 'JP', 'MID1', '2011-07-18 09:40:05', '2011-07-18 09:40:05', 0, NULL, NULL),
(4, 'IN', 'MID1', '2011-07-18 09:40:05', '2011-07-18 09:40:05', 0, NULL, NULL),
(5, 'CA', 'MID1', '2011-07-18 09:40:05', '2011-07-18 09:40:05', 0, NULL, NULL),
(6, 'IE', 'MID1', '2011-07-18 09:40:05', '2011-07-18 09:40:05', 0, NULL, NULL);


