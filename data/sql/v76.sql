
CREATE TABLE country_based_payment (id BIGINT AUTO_INCREMENT, country_code VARCHAR(20), service VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, deleted TINYINT(1) DEFAULT '0' NOT NULL, created_by INT, updated_by INT, PRIMARY KEY(id)) ENGINE = INNODB;

CREATE TABLE country_based_zip (id BIGINT AUTO_INCREMENT, country_code VARCHAR(20), zip VARCHAR(255) DEFAULT 'no', created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, created_by INT, updated_by INT, PRIMARY KEY(id)) ENGINE = INNODB;

 ALTER TABLE `country_based_zip` CHANGE `zip` `zip` ENUM( 'no', 'yes' ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT 'no' ;

INSERT INTO `country_based_payment` (`country_code`, `service`, `created_at`, `updated_at`, `deleted`, `created_by`, `updated_by`) VALUES
( 'US', 'nmi_vbv', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, NULL);

INSERT INTO `country_based_zip` ( `country_code`, `zip`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
('US', 'yes', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL);
