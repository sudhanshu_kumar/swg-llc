CREATE TABLE transaction_charges (id INT AUTO_INCREMENT, merchant_id BIGINT NOT NULL, transaction_charges FLOAT(18, 2) DEFAULT 0 NOT NULL, split_type ENUM('flat', 'percentage') DEFAULT 'percentage', created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, deleted TINYINT(1) DEFAULT '0' NOT NULL, created_by INT, updated_by INT, INDEX merchant_id_idx (merchant_id), PRIMARY KEY(id)) ENGINE = INNODB;
ALTER TABLE transaction_charges ADD CONSTRAINT transaction_charges_merchant_id_merchant_id FOREIGN KEY (merchant_id) REFERENCES merchant(id) ON DELETE CASCADE;
ALTER TABLE `order_request_split` ADD `transaction_charges` FLOAT( 18, 2 ) NULL COMMENT 'cents' AFTER `amount` ;

ALTER TABLE `transaction_charges` ADD UNIQUE (`merchant_id`) ;

 DELETE FROM `merchant` WHERE `merchant`.`id` = 2 LIMIT 1;
DELETE FROM `merchant` WHERE `merchant`.`id` = 3 LIMIT 1;
DELETE FROM `merchant` WHERE `merchant`.`id` = 5 LIMIT 1;
UPDATE `merchant` SET `id` = '2' WHERE `merchant`.`id` =4 LIMIT 1 ;
UPDATE `merchant` SET `name` = 'NIS', `description` = 'Nigerian Service' WHERE `merchant`.`id` =1 LIMIT 1 ;

DELETE FROM `ep_menu` WHERE `id` = 7 ;
