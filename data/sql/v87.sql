INSERT INTO `gateway` (
`id` ,
`name` ,
`display_name` ,
`merchant_id`
)
VALUES (
NULL , 'Mo', 'Money Order', '7'
);

INSERT INTO `country_payment_mode` (`id`, `country_code`, `card_type`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(NULL , 'US', 'Mo', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL);

INSERT INTO `payment_mode` (
`id` ,
`card_type` ,
`gateway_id` ,
`group_id` ,
`display_name` ,
`created_at` ,
`updated_at` ,
`created_by` ,
`updated_by`
)
VALUES (
NULL , 'Mo', '6', '6', 'Money Order', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL , NULL
);

CREATE TABLE IF NOT EXISTS `cart_tracking_number` (
  `id` bigint(20) NOT NULL auto_increment,
  `cart_id` bigint(20) NOT NULL,
  `tracking_number` varchar(50) NOT NULL,
  `order_number` bigint(20) default NULL,
  `order_request_detail_id` bigint(20) default NULL,
  `cart_amount` float(18,2) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `associated_date` date NOT NULL,
  `status` enum('New','Associated','Paid') NOT NULL default 'New',
  `created_at` datetime NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `updated_at` datetime NOT NULL,
  `updated_by` bigint(20) NOT NULL,
  `deleted` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `money_order` (
  `id` bigint(20) NOT NULL auto_increment,
  `moneyorder_number` varchar(30) NOT NULL,
  `amount` float(18,2) NOT NULL,
  `moneyorder_date` date default NULL,
  `paid_date` date default NULL,
  `address` varchar(255) default NULL,
  `phone` varchar(20) default NULL,
  `moneyorder_office` varchar(100) default NULL,
  `comments` text,
  `status` enum('New','Paid') NOT NULL default 'New',
  `created_at` datetime NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `updated_at` datetime NOT NULL,
  `updated_by` bigint(20) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;


CREATE TABLE IF NOT EXISTS `tracking_money_order` (
  `id` bigint(20) NOT NULL auto_increment,
  `cart_track_id` bigint(20) NOT NULL,
  `moneyorder_id` bigint(20) NOT NULL,
  `created_at` datetime default NULL,
  `updated_at` datetime default NULL,
  `created_by` bigint(20) default NULL,
  `updated_by` bigint(20) default NULL,
  PRIMARY KEY  (`id`),
  KEY `cart_track_id` (`cart_track_id`),
  KEY `moneyorder_id` (`moneyorder_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

ALTER TABLE `tracking_money_order`
  ADD CONSTRAINT `tracking_money_order_ibfk_1` FOREIGN KEY (`cart_track_id`) REFERENCES `cart_tracking_number` (`id`),
  ADD CONSTRAINT `tracking_money_order_ibfk_2` FOREIGN KEY (`moneyorder_id`) REFERENCES `money_order` (`id`);


INSERT INTO `ep_menu` (
`id` ,
`name` ,
`label` ,
`parent_id` ,
`sfguardperm` ,
`url` ,
`eval` ,
`env` ,
`sequence` ,
`created_at` ,
`updated_at` ,
`deleted` ,
`created_by` ,
`updated_by`
)
VALUES (
NULL , 'SubmitMoneyOrder', 'Submit Money Order', '25', 'payment_user', 'mo_configuration/saveMoneyorder', NULL , 'custom', '3', '', '', '0', NULL , NULL
);



CREATE TABLE IF NOT EXISTS `cart_tracking_number` (
 `id` bigint(20) NOT NULL auto_increment,
 `cart_id` bigint(20) NOT NULL,
 `tracking_number` varchar(50) NOT NULL,
 `order_number` bigint(20) default NULL,
 `order_request_detail_id` bigint(20) default NULL,
 `cart_amount` float(18,2) NOT NULL,
 `user_id` bigint(20) NOT NULL,
 `associated_date` date NOT NULL,
 `status` enum('New','Associated','Paid') NOT NULL default 'New',
 `created_at` datetime NOT NULL,
 `created_by` bigint(20) NOT NULL,
 `updated_at` datetime NOT NULL,
 `updated_by` bigint(20) NOT NULL,
 `deleted` tinyint(1) NOT NULL default '0',
 PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS `money_order` (
 `id` bigint(20) NOT NULL auto_increment,
 `moneyorder_number` varchar(30) NOT NULL,
 `amount` float(18,2) NOT NULL,
 `moneyorder_date` date default NULL,
 `paid_date` date default NULL,
 `address` varchar(255) default NULL,
 `phone` varchar(20) default NULL,
 `moneyorder_office` varchar(100) default NULL,
 `comments` text,
 `status` enum('New','Paid') NOT NULL default 'New',
 `created_at` datetime NOT NULL,
 `created_by` bigint(20) NOT NULL,
 `updated_at` datetime NOT NULL,
 `updated_by` bigint(20) NOT NULL,
 PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `tracking_money_order` (
 `id` bigint(20) NOT NULL auto_increment,
 `cart_track_id` bigint(20) NOT NULL,
 `moneyorder_id` bigint(20) NOT NULL,
 `created_at` datetime default NULL,
 `updated_at` datetime default NULL,
 `created_by` bigint(20) default NULL,
 `updated_by` bigint(20) default NULL,
 PRIMARY KEY  (`id`),
 KEY `cart_track_id` (`cart_track_id`),
 KEY `moneyorder_id` (`moneyorder_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tracking_money_order`
--
ALTER TABLE `tracking_money_order`
 ADD CONSTRAINT `tracking_money_order_ibfk_1` FOREIGN KEY (`cart_track_id`) REFERENCES `cart_tracking_number` (`id`),
 ADD CONSTRAINT `tracking_money_order_ibfk_2` FOREIGN KEY (`moneyorder_id`) REFERENCES `money_order` (`id`);

