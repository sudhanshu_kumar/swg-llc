-------SQL FOR NIS------

ALTER TABLE `tbl_nis_dollar_revenue` CHANGE `amount` `amount` DOUBLE( 18, 2 ) NULL DEFAULT NULL;
 CREATE TABLE IF NOT EXISTS `tbl_project_name` (
   `id` int(11) NOT NULL AUTO_INCREMENT,
   `name` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
   `updated_at` datetime NOT NULL,
   `created_by` int(11) DEFAULT NULL,
   `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
 ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;


INSERT INTO `tbl_project_name` (`id`, `name`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
 (1, 'NIS', '2010-12-29 13:49:27', '2010-12-29 13:49:30', NULL, NULL);



 CREATE TABLE IF NOT EXISTS `tbl_bank_details` (
   `id` int(11) NOT NULL AUTO_INCREMENT,
   `bank_name` varchar(100) NOT NULL,
  `account_number` varchar(100) NOT NULL,
   `account_name` varchar(100) NOT NULL,
   `city` varchar(100) NOT NULL,
   `state` varchar(150) NOT NULL,
   `country` varchar(150) NOT NULL,
   `project_id` int(11) NOT NULL,
   `created_at` datetime NOT NULL,
   `updated_at` datetime NOT NULL,
  `created_by` int(11) DEFAULT NULL,
   `updated_by` int(11) DEFAULT NULL,
   PRIMARY KEY (`id`),
   KEY `project_id` (`project_id`)
 ) ENGINE=InnoDB ;


 ALTER TABLE `tbl_bank_details`
   ADD CONSTRAINT `tbl_bank_details_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `tbl_project_name` (`id`) ON DELETE CASCADE;




 ALTER TABLE `tbl_nis_dollar_revenue` ADD `account_id` INT NOT NULL AFTER `remark` ;


 ADD CONSTRAINT `tbl_nis_dollar_revenue_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `tbl_bank_details` (`id`) ON DELETE CASCADE;



 INSERT INTO `tbl_bank_details` (
 `id` ,
 `bank_name` ,
 `account_number` ,
 `city` ,
 `state` ,
 `country` ,
 `project_id` ,
 `created_at` ,
 `updated_at` ,
 `created_by` ,
 `updated_by`
 )
 VALUES (
 NULL , 'Test bank', '12334', 'Delhi', 'Delhi', 'India', '1', '2010-12-29 13:51:00', '', NULL , NULL
 );

 UPDATE `tbl_nis_dollar_revenue` SET `account_id` = '1';

 INSERT INTO `tbl_bank_details` (`id`, `bank_name`, `account_number`, `account_name`, `city`, `state`, `country`, `project_id`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
 (1, 'Test Bank', '123456766441', 'Test', 'Noida', 'UP', 'India', 1, '2010-12-29 17:57:39', '2010-12-30 15:23:06','' , '');


-----SQL FOR Ipay4me----

INSERT INTO `sf_guard_permission` (
`id` ,
`name` ,
`description` ,
`created_at` ,
`updated_at`
)
VALUES (
NULL , 'admin_support_user', 'Admin Support User', '', ''
);


INSERT INTO `sf_guard_group_permission` (
`group_id` ,
`permission_id` ,
`created_at` ,
`updated_at`
)
VALUES (
'1', '7', '', ''
),(
'2', '7', '', ''
), (
'6', '7', '', ''
);


UPDATE `ep_menu` SET `sfguardperm` = 'admin_support_user' WHERE `ep_menu`.`id` =39 LIMIT 1 ;

UPDATE `ep_menu` SET `sfguardperm` = 'admin_support_user' WHERE `ep_menu`.`id` =40 LIMIT 1 ;

UPDATE `ep_menu` SET `name` = 'Dollar Sweep Report',
`label` = 'Dollar Sweep Report' WHERE `ep_menu`.`id` =40 LIMIT 1 ;

ALTER TABLE `ep_menu` ADD `env` VARCHAR( 20 ) NULL DEFAULT 'custom' AFTER `eval` ;



UPDATE `ep_menu` SET `name` = 'ChangePassword' WHERE `ep_menu`.`name` = 'Change Password' LIMIT 1;
UPDATE `ep_menu` SET `name` = 'PaymentHistory' WHERE `ep_menu`.`name` = 'Payment History' LIMIT 1;
UPDATE `ep_menu` SET `name` = 'AuditTrail' WHERE `ep_menu`.`name` = 'Audit Trail' LIMIT 1;
UPDATE `ep_menu` SET `name` = 'ViewReport' WHERE `ep_menu`.`name` = 'View Report' LIMIT 1;
UPDATE `ep_menu` SET `name` = 'SWGlobalReport' WHERE `ep_menu`.`name` = 'SWGlobal Report' LIMIT 1;
UPDATE `ep_menu` SET `name` = 'UserList' WHERE `ep_menu`.`name` = 'User List' LIMIT 1;
UPDATE `ep_menu` SET `name` = 'PaymentReport' WHERE `ep_menu`.`name` = 'Payment Report' LIMIT 1;
UPDATE `ep_menu` SET `name` = 'TransactionCharges' WHERE `ep_menu`.`name` = 'Transaction Charges' LIMIT 1;
UPDATE `ep_menu` SET `name` = 'UnpaidBills' WHERE `ep_menu`.`name` = 'Unpaid Bills' LIMIT 1;
UPDATE `ep_menu` SET `name` = 'EnterPin' WHERE `ep_menu`.`name` = 'Enter Pin' LIMIT 1;
UPDATE `ep_menu` SET `name` = 'RegeneratePin' WHERE `ep_menu`.`name` = 'Regenerate Pin' LIMIT 1;
UPDATE `ep_menu` SET `name` = 'UserManagement' WHERE `ep_menu`.`name` = 'User Management' LIMIT 1;
UPDATE `ep_menu` SET `name` = 'NewKYCeWalletUser' WHERE `ep_menu`.`name` = 'New KYC eWallet User' LIMIT 1;
UPDATE `ep_menu` SET `name` = 'EwalletRecharge' WHERE `ep_menu`.`name` = 'Recharge ewallet' LIMIT 1;
UPDATE `ep_menu` SET `name` = 'Rechargeewallet' WHERE `ep_menu`.`name` = 'Recharge ewallet' LIMIT 1;
UPDATE `ep_menu` SET `name` = 'eWalletStatement' WHERE `ep_menu`.`name` = 'eWallet Statement' LIMIT 1;
UPDATE `ep_menu` SET `name` = 'ReportUser' WHERE `ep_menu`.`name` = 'Report User' LIMIT 1;
UPDATE `ep_menu` SET `name` = 'ResponseRequestReport' WHERE `ep_menu`.`name` = 'Response Request Report' LIMIT 1;
UPDATE `ep_menu` SET `name` = 'EwalletRechargeSetting' WHERE `ep_menu`.`name` = 'Ewallet Recharge Setting' LIMIT 1;
UPDATE `ep_menu` SET `name` = 'FindApplicationsbyOrderNumber' WHERE `ep_menu`.`name` = 'Find Applicationsby Order Number' LIMIT 1;
UPDATE `ep_menu` SET `name` = 'SearchApplicationByApplicationId' WHERE `ep_menu`.`name` = 'Search Application By Application Id' LIMIT 1;
UPDATE `ep_menu` SET `name` = 'SearchApplicationBySWLLCEmailId' WHERE `ep_menu`.`name` = 'Search Application By SWLLC Email Id' LIMIT 1;
UPDATE `ep_menu` SET `name` = 'RefundSWLLCOrderNumber' WHERE `ep_menu`.`name` = 'Refund SWLLC Order Number' LIMIT 1;
UPDATE `ep_menu` SET `name` = 'RefundSWLLCOrderNumberAndBlock' WHERE `ep_menu`.`name` = 'Refund SWLLC Order Number And Block' LIMIT 1;
UPDATE `ep_menu` SET `name` = 'ChargebackSWLLCOrderNumber' WHERE `ep_menu`.`name` = 'Chargeback SWLLC Order Number' LIMIT 1;
UPDATE `ep_menu` SET `name` = 'SearchOrdernumberbycarddetails' WHERE `ep_menu`.`name` = 'Search Order number by card details' LIMIT 1;
UPDATE `ep_menu` SET `name` = 'AddNewUser' WHERE `ep_menu`.`name` = 'Add New User' LIMIT 1;
UPDATE `ep_menu` SET `name` = 'GrayPayVault' WHERE `ep_menu`.`name` = 'GrayPay Vault' LIMIT 1;
UPDATE `ep_menu` SET `name` = 'UnblockBlackListedCards' WHERE `ep_menu`.`name` = 'Unblock Black Listed Cards' LIMIT 1;
UPDATE `ep_menu` SET `name` = 'VaultProcessing' WHERE `ep_menu`.`name` = 'Vault Processing' LIMIT 1;
UPDATE `ep_menu` SET `name` = 'NISDollarRevenueReport' WHERE `ep_menu`.`name` = 'NIS Dollar Revenue Report' LIMIT 1;
UPDATE `ep_menu` SET `name` = 'DollarSweepReport' WHERE `ep_menu`.`name` = 'Dollar Sweep Report' LIMIT 1;
UPDATE `ep_menu` SET `name` = 'NISApplication' WHERE `ep_menu`.`name` ='NIS Application' LIMIT 1 ;
