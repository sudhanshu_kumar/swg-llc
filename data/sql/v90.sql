INSERT INTO `ep_menu` (
`id` ,
`name` ,
`label` ,
`parent_id` ,
`sfguardperm` ,
`url` ,
`eval` ,
`env` ,
`sequence` ,
`created_at` ,
`updated_at` ,
`deleted` ,
`created_by` ,
`updated_by`
)
VALUES (
NULL , 'MoneyOrderReport', 'Money Order Report', '50', 'payment_user', 'supportTool/moneyOrderReport', NULL , 'custom', '2', '', '', '0', NULL , NULL
), (
NULL , 'MoneyOrderReport', 'Money Order Report', '50', 'admin_support_user', 'supportTool/moneyOrderReport', NULL , 'custom', '3', '', '', '0', NULL , NULL
);