ALTER TABLE `gateway` ADD `visa_amount_limit` INT( 11 ) NOT NULL AFTER `merchant_id` ,
ADD `master_amount_limit` INT( 11 ) NOT NULL AFTER `visa_amount_limit` ,
ADD `cart_amount_limit` INT( 11 ) NOT NULL AFTER `master_amount_limit` ;

update `gateway` set `visa_amount_limit` = 15000,`master_amount_limit`=5000,`cart_amount_limit`=2000;

update `gateway` set visa_amount_limit = 1500000,master_amount_limit=500000,cart_amount_limit=200000 where id='10' limit 1;