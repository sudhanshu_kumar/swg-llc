ALTER TABLE `tbl_vap_application` CHANGE `applicant_type` `applicant_type` ENUM( 'GB', 'PB' ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT 'GO-Government Official, OC-Oil Company';
ALTER TABLE `tbl_vap_application` CHANGE `applicant_type` `applicant_type` ENUM( 'GB', 'PB' ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT 'GB-Government Business, PB-Public Business';



ALTER TABLE `tbl_vap_application` ADD `business_address` VARCHAR( 255 ) NOT NULL ,
ADD `sponsore_type` ENUM( 'SS', 'CS' ) NOT NULL ;