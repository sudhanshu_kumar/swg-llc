<?php

require_once dirname(__FILE__).'/../lib/vendor/symfony/lib/autoload/sfCoreAutoload.class.php';
sfCoreAutoload::register();

class ProjectConfiguration extends sfProjectConfiguration
{
    public function setup()
    {
        $this->enablePlugins('epAccountingPlugin');
        $this->enablePlugins('epActionAuditPlugin');
        $this->enablePlugins('epGrayPayPlugin');
        $this->enablePlugins('epJobSchedulerPlugin');
        $this->enablePlugins('epMenuPlugin');
        $this->enablePlugins('epNISPlugin');
        //$this->enablePlugins('epNMIPlugin');
        $this->enablePlugins('epOpenIDPlugin');
        $this->enablePlugins('epPasswordPolicyPlugin');
        $this->enablePlugins('epPayEasyPlugin');
        $this->enablePlugins('epSessionManagerPlugin');
        $this->enablePlugins('epSMSPlugin');
        $this->enablePlugins('epVbVPlugin');

        $this->enablePlugins('sfDoctrinePlugin');
        $this->enablePlugins('sfDoctrineGuardPlugin');
        $this->enablePlugins('sfPasswordStrengthPlugin');
        $this->enablePlugins('sfWebBrowserPlugin');
        $this->enablePlugins('sfGoogleAnalyticsPlugin'); 
        $this->enablePlugins('sfTCPDFPlugin');       
        
        $this->enablePlugins('sfTCPDFPlugin');
        $this->enablePlugins('sfImageTransformPlugin');
        $this->enablePlugins('epNmiPlugin');
        $this->enablePlugins('sfPHPUnit2Plugin');
        $this->enablePlugins('stOfcPlugin');        
        /**
         * [WP: 086] => CR:125
         * BIN feature...
         */
        $this->enablePlugins('sfExcelReaderPlugin');
  }

    public function configureDoctrine(Doctrine_Manager $manager)
    {
        //   Using Native Enum - Use it ONLY IF you are sure you need it
        //    $manager->setAttribute('use_native_enum', true);
        $manager->setAttribute(Doctrine::ATTR_USE_DQL_CALLBACKS, true);

    }
}
