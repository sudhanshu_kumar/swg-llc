<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="title" content="">
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="robots" content="index, follow">
<meta name="language" content="de">
<title>SW Global LLC</title>
<?php
    $request = sfContext::getInstance()->getRequest();
$host = $request->getUriPrefix() ;
 $url_root = $request->getRelativeUrlRoot();

$faviconPath = $host.$url_root;

      ?>
<link rel="shortcut icon" href="<?php echo $faviconPath ?>/favicon.ico" />
<link rel="stylesheet" type="text/css" href="css/style.css">
    <?php include_http_metas() ?>
    <?php include_metas() ?>
    <?php include_title() ?>
    <?php include_javascripts() ?>
    <?php include_stylesheets() ?>

    <script type="text/javascript">
        $(window).load(function() {
            if($('#tmz-slider').length > 0){
                $('#tmz-slider').nivoSlider();
            }
        });
    </script>
</head>
<body>
<?php include_partial('global/header');?>
<div class="content_wrapper"> <?php echo $sf_content ?> </div>
<?php include_partial('global/footer');?>
