<div class="main_wrap clearfix">
<div class="header_bg_left">
  <div class="swg_logo"> <?php echo image_tag('/images/logo_swg.png',array('id'=>'top_header_logo','alt'=>'SW Global','class'=>'')); ?></div>
</div>
<div class="header_bg">
  <!--	<a href=""></a>-->
  <?php echo image_tag('/images/logo.png',array('alt'=>'SW Global LLC','class'=>'top_logo')); ?>
  <!--cart logo-->
  <!--  <a href="<?php //echo url_for('cart/list') ?>" class="card_logo"><?php //echo image_tag('/images/cartBtn.gif',array('alt'=>'Cart')); ?></a> -->
  <!--cart logo-->
</div>
<div class="header_bg_right"></div>
<div class="top_nav_section">
  <div class="topnav_left"></div>
  <div class="top_nav_bg">
    <div id="smoothmenu1" class="ddsmoothmenu">
      <ul>
        <li class="left_li"><?php echo link_to('Home','@homepage') ?></li>
        <!--     <li><?php //echo link_to('Features','cms/features') ?></li>-->
        <li><?php echo link_to('Services','cms/services') ?></li>
        <!--   <li><?php //echo link_to('Pricing','cms/pricing') ?></li>-->
        <li><?php echo link_to('FAQs','cms/faq') ?></li>
        <li><?php echo link_to('Feedback','cms/feedback') ?></li>
      </ul>
    </div>
  </div>
  <div class="top_nav_right"></div>
</div>
