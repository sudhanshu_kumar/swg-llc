<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <title></title>

    <?php include_http_metas() ?>
    <?php include_metas() ?>
    <?php include_title() ?>
    <style media="print">
   </style>
    <?php use_stylesheet('print','',array('media'=>'print'));?>
    <?php use_stylesheet('style','',array('media'=>'print'));?>
    <link rel="shortcut icon" href="/favicon.ico" />
    <?php //include_javascripts() ?>
    <?php include_stylesheets() ?>

<!--[if IE 6]>
    <?php echo stylesheet_tag('ie6'); ?>
    <![endif]-->
    <style media="print">
   </style>
  </head>
  <body>
  <div id="wrapperPrintTop">
    <div id="wrapperLogo"><?php echo image_tag('/images/logo.png',array('title'=>"SW Global LLC", 'alt'=>"SW Global LLC")); ?></div>
  </div>

<div id="wrapperPrint">
  <div id="wrapperMid">
    <div id="tpcurve">
      <div id="tpcurveControltop"></div>
      <div id="tpcurveLoadAreatop"></div>
    </div>
    <div id="btcurve">
      <div id="btcurveControlbottom"></div>
      <div id="btcurveLoadAreabottom"></div>

    </div>

    <div id="loadAreaPrint">
      <div class="descriptionArea">
        <div class="wrapForm2" id="printBody">

    </div>
      </div>
    </div>
    <div class="clear"></div>
  </div>
  </div>

<div style="height:10px;">&nbsp;</div>
<div class="footer_wrapbg clearfix">
  <div class="footer_wrap">
    
      <center>Copyright&nbsp;&copy; <?php echo date('Y');?> SW Global LLC. All rights reserved.<br>
      </center>
        <div class="clear"></div>
      
      <div class="r">
     <?php if($_SERVER['SERVER_NAME'] == 'swgloballlc.com'){ ?>

      <table width="135" border="0" cellpadding="2" cellspacing="0" title="Click to Verify - This site chose VeriSign SSL for secure e-commerce and confidential communications.">
        <tr>
        <td width="135" align="center" valign="top"><script type="text/javascript" src="https://seal.verisign.com/getseal?host_name=www.swgloballlc.com&amp;size=S&amp;use_flash=NO&amp;use_transparent=NO&amp;lang=en"></script><br />
        <a href="http://www.verisign.com/ssl-certificate/" target="_blank"  style="color:#000000; text-decoration:none; font:bold 7px verdana,sans-serif; letter-spacing:.5px; text-align:center; margin:0px; padding:0px;">ABOUT SSL CERTIFICATES</a></td>
        </tr>
      </table>

      <?php } ?>
</div>


    </div>





    <p class="footer_rightshade"></p>
  </div>
</div>
<!--[if lte IE 6]></div><![endif]-->
<!-- etracker PARAMETER 2.4 

<a target="_blank" href=""><?php //echo image_tag('/images/cnt.gif',array('alt'=>'SW GLOBAL LLC')); ?></a>-->
<!-- etracker CODE NOSCRIPT -->
<noscript>

</noscript>
<!-- etracker CODE END -->



</body>
</html>
</div>
</html>
