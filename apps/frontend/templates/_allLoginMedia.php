  <div class="login_heading">
        <div class="login_head_text">To start application,<br />
          login here using</div>
      </div>
    <?php
    if(isset($appVars)){
             $appVarsJs = $appVars;
         }else{
             $appVarsJs = '';
         }
         if(isset($appVars)){
         $googleURL = url_for('authModule/openIdDetails?openid_identifier=google&appVars='.$appVars);
         }else{
             $googleURL = url_for('authModule/openIdDetails?openid_identifier=google');
         }
         if(isset($appVars)){
         $yahooURL = url_for('authModule/openIdDetails?openid_identifier=yahoo&appVars='.$appVars);
         }else{
             $yahooURL = url_for('authModule/openIdDetails?openid_identifier=yahoo');
         }
         if(isset($appVars)){
             $pay4meURL = url_for('authModule/openIdDetails?openid_identifier=pay4me&appVars='.$appVars);
         }else{
             $pay4meURL = url_for('authModule/openIdDetails?openid_identifier=pay4me');
         }
         ?>
          <div class="logos">
                <span id="googleActiveId" class="logos_block"><?php echo image_tag('google-active.png', array('alt' => 'GOOGLE', 'width' => 104, 'height' => 47, 'title' => 'Login with Google OpenID', 'class' => 'hand', 'onmouseover' => "addHoverImage('google')", 'onclick' => "openOpenIdWindow('".$googleURL."', 'google')")); ?></span>
                <span id="googleHoverId" class="logos_none"><?php echo image_tag('google-hover.png', array('alt' => 'GOOGLE', 'width' => 104, 'height' => 47, 'title' => 'Login with Google OpenID', 'class' => 'hand', 'onmouseout' => "removeHoverImage('google')",    'onclick' => "openOpenIdWindow('".$googleURL."', 'google')" )); ?></span>
                
                <span id="openidActiveId" class="logos_block"><?php echo image_tag('openid-active.png', array('alt' => 'PAY4ME', 'width' => 104, 'height' => 47, 'title' => 'Login with SW Global LLC OpenID', 'class' => 'hand', 'onmouseover' => "addHoverImage('openid')",  'onclick' => "openOpenIdWindow('".$pay4meURL."', 'pay4me')" )) ?></span>
                <span id="openidHoverId" class="logos_none"><?php echo image_tag('openid-hover.png', array('alt' => 'PAY4ME', 'width' => 104, 'height' => 47, 'title' => 'Login with SW Global LLC OpenID', 'class' => 'hand', 'onmouseout' => "removeHoverImage('openid')", 'onclick' => "openOpenIdWindow('".$pay4meURL."', 'pay4me')" )) ?></span>
                
                <span id="yahooActiveId" class="logos_block"><?php echo image_tag('yahoo-active.png', array('alt' => 'YAHOO', 'width' => 104, 'height' => 47, 'title' => 'Login with Yahoo OpenID', 'class' => 'hand', 'onmouseover' => "addHoverImage('yahoo')",  'onclick' => "openOpenIdWindow('".$yahooURL."', 'yahoo')" )); ?></span>
                <span id="yahooHoverId" class="logos_none"><?php echo image_tag('yahoo-hover.png', array('alt' => 'YAHOO', 'width' => 104, 'height' => 47, 'title' => 'Login with Yahoo OpenID', 'class' => 'hand', 'onmouseout' => "removeHoverImage('yahoo')",  'onclick' => "openOpenIdWindow('".$yahooURL."', 'yahoo')" )); ?></span>
                
                <span id="facebookActiveId" class="logos_block"><?php echo image_tag('facebook-disable.png', array('alt' => 'FACEBOOK', 'width' => 104, 'height' => 47, 'title' => 'Facebook login will be available soon.')); ?></span> <!-- , 'class' => 'hand', 'onmouseover' => "addHoverImage('facebook')", 'onclick' => "facebookLogin()"  -->
                <span id="facebookHoverId" class="logos_none"><?php echo image_tag('facebook-hover.png', array('alt' => 'FACEBOOK', 'width' => 104, 'height' => 47, 'title' => 'Facebook Login will be available soon.' )); ?></span> <!-- , 'class' => 'hand',  'onmouseout' => "removeHoverImage('facebook')",  'onclick' => "facebookLogin()" -->                
          </div>
          <div class="separator-login">
                      </div><!-- separator ends here-->
          <div class="login_info">Once logged in, you can start your application. Prices will vary based on destination country.</div>

        <div class="clear"></div>
      <div id="fb-root"></div>
<?php
    ## [WP: 069] => CR:102
    ## Getting application http path...
    $sitePath = Settings::getHTTPpath();
    $returnUrl = $sitePath.'/authModule/facebookReturnLogin/';

    if(isset($appVars)){
        $returnUrl = $returnUrl.'?appVars='.$appVars;
    }

    ## Create our Application instance...
    $facebook = new FacebookApi($returnUrl);
    
    $facebookLoginUrl = $facebook->getLoginUrl(array('scope' => 'email'));

    ## This variable will used when user comes from email click...
    $redirectEmailUri = sfContext::getInstance()->getRequest()->getUri();
    sfContext::getInstance()->getUser()->setAttribute('redirectEmailUri', $redirectEmailUri);

    ##$facebookLogoutUrl = $facebook->getLogoutUrl();
    ##$facebook->destroySession();

    ## Add code for logout from gmail, yahoo, openID and Facebook...
    $loginType = sfContext::getInstance()->getUser()->getAttribute('loginType');
    if($loginType == "google"){
        echo "<iframe style='display:none' src ='https://www.google.com/accounts/Logout' width='1%' height='1%'></iframe>";
    }else if($loginType == "yahoo"){
        echo "<iframe style='display:none' src ='https://login.yahoo.com/config/login?logout=3D1' width='1%' height='1%'></iframe>";
    }else if($loginType == "aol"){
        echo "<iframe style='display:none' src ='http://my.screenname.aol.com/_cqr/logout/mcLogout.psp' width='1%' height='1%'></iframe>";
    }
    ## Add code for logout from gmail, yahoo, openID and Facebook...
?>

   <script>
    /*
    window.fbAsyncInit = function() {
        FB.init({
            appId: '<?php //echo sfConfig::get('app_facebook_appId'); ?>',
            cookie: true,
            xfbml: true,
            oauth: true
        });
        FB.Event.subscribe('auth.statusChange', function(response) {
            //alert(response.status);
            if (response.status === 'connected') {

                var uid = response.authResponse.userID;
                var accessToken = response.authResponse.accessToken;
                var appVars = '<?php //echo $appVarsJs; ?>';

                FB.api('/me', function(response) {

                    //alert(response);
                    var email = response.email;
                    var name = response.name;
                    var username = response.username;

                    //alert('Good to see you, ' + response.email + '.');
                    var url = '<?php //echo url_for('authModule/makeFacebookData'); ?>';
                    $.ajax({
                        url: url,
                        type: 'POST',
                        data: { 'uid': uid, 'accessToken': accessToken, 'email': email, 'username' : username, 'fullname' : name, 'appVars': appVars},
                        success: function(data) {
                            if(data == 1){
                                var url = '<?php //echo url_for('authModule/authUserDetails'); ?>';
                                location.href = url;
                            }else{
                                alert("Error found while logged in. Please try again.");
                            }
                        }
                    });
                });
            }else{
                alert("Error found while logged in. Please try again.");
            }
        });

    };
    (function () {        
        var e = document.createElement('script');
        e.async = true;
        e.type = 'text/javascript';
        e.src = 'https://connect.facebook.net/en_US/all.js';        
        document.getElementsByTagName('head')[0].appendChild(e);
    }());


    function loginFacebook() {
        FB.login(function(response) {}, {scope:'email'});
    }
    */
    function facebookLogin(){
        var url = '<?php echo $facebookLoginUrl; ?>';
        var type = 'facebook';
        openOpenIdWindow(url, type);
    }

</script>

<script>
    function getCenteredCoords(w, h) {
        var xPos = null;
        var yPos = null;
        if (window.ActiveXObject) {
            //xPos = window.event.screenX - (width/2) + 100;
            //yPos = window.event.screenY - (height/2) - 100;
            var xPos = (screen.width/2)-(w/2);
            var yPos = (screen.height/2)-(h/2);
        } else {
            var parentSize = [window.outerWidth, window.outerHeight];
            var parentPos = [window.screenX, window.screenY];
            xPos = parentPos[0] +
                Math.max(0, Math.floor((parentSize[0] - w) / 2));
            yPos = parentPos[1] +
                Math.max(0, Math.floor((parentSize[1] - (h*1.25)) / 2));
        }
        return [xPos, yPos];
    }
    function openOpenIdWindow(url, type){
        var width = '450';
        var height = '500';
        if(type == 'yahoo'){
            width = '500';
            height = '550';
        }else if(type == 'pay4me'){
            width = '440';
            height = '480';
        }
        var w = window.open(url, 'openid_popup', 'width='+width+',height='+height+',scrollbars=yes,resizable=no');
        var coords = getCenteredCoords(450,500);
        w.moveTo(coords[0],coords[1]);
    }
    
    function addHoverImage(id){
        $('#'+id+'ActiveId').hide();
        $('#'+id+'HoverId').show();
    }

    function removeHoverImage(id){
        $('#'+id+'HoverId').hide();
        $('#'+id+'ActiveId').show();
    }
</script>
