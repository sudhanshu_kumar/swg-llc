<script type="text/javascript">
ddsmoothmenu.init({
mainmenuid: "smoothmenu1", //menu DIV id
orientation: 'h', //Horizontal or vertical menu: Set to "h" or "v"
classname: 'ddsmoothmenu', //class added to menu's outer DIV
//customtheme: ["#1c5a80", "#18374a"],
contentsource: "markup" //"markup" or ["container_id", "path_to_menu_file"]
})
</script>
<?php if($sf_context->getUser()->isAuthenticated()){ ?>

<div class="main_wrap clearfix">
<div class="header_bg_left">
  <div class="swg_logo"> <a href="<?php echo url_for('welcome/index');?>" border="0"><?php echo image_tag('/images/logo_swg.png',array('alt'=>'SW Global','class'=>'top_logoswglobal noprint')); ?> </a>
    <?php //echo image_tag('/images/logo_swg.png',array('alt'=>'SW Global','class'=>'noprint')); ?>
  </div>
</div>
<div class="header_bg"></div>
<div class="header_bg_right">
  <div class="flRight">
    <?php // use_stylesheet('fg.menu.css'); ?>
    <?php //echo image_tag('/images/logo.png',array('alt'=>'','class'=>'top_logo mg10')); ?>
    <!--cart logo-->
    <?php
   if($sf_user->getGuardUser()->hasGroup("payment_group")){
   
   ?>
    <a href="<?php echo url_for('cart/list') ?>" class="card_logo"><?php echo image_tag('/images/cartBtn.png',array('alt'=>'Cart')); ?></a>
    <?php }?>
    <!--cart logo-->
    <div >
      <?php if($sf_context->getUser()->isAuthenticated()){ $groups = $sf_context->getUser()->getGroupDescription();

            ?>
      <div class="mg10 toploginTxt"> Hello <b>
        <?php

        if($groups[0] == "Portal Admin"){
          echo ucfirst($sf_context->getUser()->getGuardUser()->getUsername());
        }else{
          echo ucfirst($sf_context->getUser()->getGuardUser()->getUserDetail()->getFirstName());
        }
        ?>
        [<span><?php echo link_to('Logout','@sf_guard_signout'); ?></span>] </b><br />
        <?php //if($groups[0]=='payment_group') echo 'Payment User'; else echo $groups[0];
            }
            ?>
      </div>
    </div>
  </div>
</div>
<div class="top_nav_section">
  <div class="topnav_left"></div>
  <div class="top_nav_bg">
    <div class="top_links ddsmoothmenu" id="smoothmenu1">
      <?php   include_partial('global/menuAdmin');   ?>
    </div>
  </div>
  <div class="top_nav_right"></div>
</div>
<?php

}
else
{
?>
<?php //echo image_tag('/images/logo.png',array('alt'=>'SW Global LLC','class'=>'top_logo')); ?>
<div class="main_wrap clearfix" style="position: relative;">
<div class="header_bg_left">
  <div class="swg_logo"> <a href="<?php echo url_for('welcome/index');?>" border="0"><?php echo image_tag('/images/logo_swg.png',array('alt'=>'SW Global','class'=>'top_logoswglobal noprint')); ?> </a>
    <?php //echo image_tag('/images/logo_swg.png',array('alt'=>'SW Global','class'=>'noprint')); ?>
  </div>
</div>
<div class="header_bg"> </div>
<div class="header_bg_right"></div>
<div class="top_nav_section">
  <div class="topnav_left"></div>
  <div class="top_nav_bg">
    <div id="smoothmenu1" class="ddsmoothmenu">
      <ul>
        <li class="left_li"><?php echo link_to('Home','@homepage') ?></li>
        <li><?php echo link_to('Services','cms/services') ?></li>
        <li><?php echo link_to('FAQs','cms/faq') ?></li>
        <li><?php echo link_to('Feedback','cms/feedback') ?></li>
      </ul>
    </div>
  </div>
  <div class="top_nav_right"></div>
</div>
<?php
}
?>
<?php
//--added by vineet to show pre alert before sesson time out

//fetch session time out value from ep session manager plugin
$params = sfContext::getInstance()->getUser()->getOptions();
$sessionTimeOut = $params['timeout'];

//$preAlertSession variable to show message time out
$preAlertSession = sfConfig::get('app_session_time_out_limit');

if(!(isset($preAlertSession) && $preAlertSession !=''))
{
    $preAlertSession = 60;
}

$timeOut = ($sessionTimeOut - $preAlertSession);

//converting it to javascript compatible neno seconds
$timeOut = $timeOut * 1000;

$currentTime = date('i',mktime(0,0,$preAlertSession,0,0,0));
?>
<div id="sessionTimeOut" style="display: none" class="alertBox"> Your session is going to expire in <b id='timer' style="font-size:20px;"><?php echo $preAlertSession; ?> </b> seconds. If you need more time, please <a href="javascript:;" onclick="resetSession('<?php echo url_for('authModule/resetSession'); ?>','<?php echo $timeOut; ?>')">click here</a>. </div>
<script>
    setSession(<?php echo $timeOut; ?>);


  var intInterval = 0;
  $(document).ready(function () {
    intInterval = window.setInterval( gotoThanks , 1000 );
  });

  function gotoThanks(){
        var timer = $('#timer').text();
        var newTimer = parseInt(timer) - 1;
        if(newTimer == 0){
            document.getElementById("sessionTimeOut").innerHTML='Your session has been expired. Please login again to start application.';
             intInterval = window.clearInterval(intInterval);
        }else{
            $('#timer').html(newTimer);
        }
    }
</script>
