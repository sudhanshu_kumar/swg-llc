    <?php
    /**
     * [WP: 104] => CR: 149
     * Below code is used if processing service is only JNA1
     */

    ## Removing footer address session varible...
    if($sf_context->getModuleName() == 'welcome' || ($sf_context->getModuleName() == 'nis' && $sf_context->getActionName() != 'index') || $sf_context->getModuleName() == 'mo_configuration' || $sf_context->getModuleName() == 'report'  || $sf_context->getModuleName() == 'userAuth'  || $sf_context->getModuleName() == 'supportTool' ){
        sfContext::getInstance()->getUser()->getAttributeHolder()->remove('sess_jna_processing');
        sfContext::getInstance()->getUser()->getAttributeHolder()->remove('sess_paymentMode_for_footer');
    }
    
    $addressChangedFlag = false;
    if(sfConfig::get('app_footer_address_dynamic')){
        $arrCartItems = sfContext::getInstance()->getUser()->getCartItems();
        if(count($arrCartItems)){
            $paymentOptionsObj = new paymentOptions();
            $processingCountry = $paymentOptionsObj->getProcessingCountry();
            if($processingCountry != ''){
                $sess_jna_processing = Functions::setFooterAddressFlag($processingCountry);
                if($sess_jna_processing){
                    $addressChangedFlag = true;
                }
            }//End of if($processingCountry != ''){...

            ## Removing jna processing flag...
            sfContext::getInstance()->getUser()->getAttributeHolder()->remove('sess_jna_processing');

            ## This will work when country has multiple processor...
            $paymentMode = sfContext::getInstance()->getUser()->getAttribute('sess_paymentMode_for_footer');
            /**
             * [WP: 111] => CR: 157
             * Added condition of MasterCard SecureCode for JNA service...
             */
            if($paymentMode == '10,jna_nmi_vbv' || $paymentMode == '10,jna_nmi_mcs'){
                $addressChangedFlag = true;
            }

        }else{
            if(sfContext::getInstance()->getUser()->getAttribute('sess_jna_processing')){
                $addressChangedFlag = true;
            }else{
                sfContext::getInstance()->getUser()->getAttributeHolder()->remove('sess_jna_processing');
                sfContext::getInstance()->getUser()->getAttributeHolder()->remove('sess_paymentMode_for_footer');
            }
        }//End of if(count($arrCartItems)){...

        if($sf_user->isAuthenticated()){
            ## If user is privilege then message will come according to payment mode assinged to user...
            if(sfContext::getInstance()->getUser()->isPrivilegeUser()){
                $privilegeUserObj = Doctrine::getTable('PaymentPrivilageUser')->findByUserId(sfContext::getInstance()->getUser()->getGuardUser()->getId());
                if(count($privilegeUserObj)){
                    if($privilegeUserObj->getFirst()->getStatus() == 'active'){
                        $midService = $privilegeUserObj->getFirst()->getMidService();
                        $midService = explode(",", $midService);
                        if(in_array('JNA1', $midService) && count($midService) == 1){
                            $addressChangedFlag = true;
                        }else{
                            $addressChangedFlag = false;
                        }
                    }//End of if($privilegeUserObj->getFirst()->getStatus() == 'active'){...
                }//End of if(count($privilegeUserObj)){...
            }//End of if(sfContext::getInstance()->getUser()->isPrivilegeUser()){...
        }//End of if($sf_user->isAuthenticated()){ ...
    }//End of if(sfConfig::get('app_footer_address_dynamic')){...

    if($addressChangedFlag){
        $address = sfConfig::get('app_footer_ng_address');
        $footerCss = 'footerNG';
        ?>
            <style>
            .copyright{
                color: #FFFFFF;
                float: left;
                font-size: 11px;
                padding: 4px 0 0 20px;
            }
            </style>
        <?php
    }else{
        $footerCss = 'footer';
        $address = sfConfig::get('app_footer_us_address');
    }
    ?>
    <div class="<?php echo $footerCss; ?>">
      <p class="footer_link">
      <?php
        if($sf_context->getUser()->isAuthenticated())
            { echo link_to('Home','@home'); }
        else
            { echo link_to('Home','@homepage') ; }
      ?>
    <?php //echo link_to('About SW GLOBAL LLC','cms/aboutus') ?>
    | <?php echo link_to('Terms of Use','cms/termsOfUse') ?> | <?php echo link_to('Privacy Policy','cms/privacy') ?>| <?php echo link_to('Refund Policy','cms/refundPolicy') ?> | <?php echo link_to('Contact Us','cms/contact') ?></p>
  <div class="copyright"><?php echo $address; ?><br/>
    <br/>
    Copyright &copy; <?php echo date('Y');?> SW Global LLC. All rights reserved.
      <?php
      if(1==Settings::isVbvActive() && false)
      {
      ?>
    <br/>
    <?php echo image_tag('/images/valuecard.jpg',array('alt'=>'')); ?><br/>
    Service Provided by ValuCard Nigeria Plc
      <?php } ?>
  </div>
  <div class="leftlogo"><br />
     <?php if($_SERVER['SERVER_NAME'] == 'swgloballlc.com'){ ?>
    <script type="text/javascript" src="https://seal.verisign.com/getseal?host_name=www.swgloballlc.com&amp;size=S&amp;use_flash=NO&amp;use_transparent=NO&amp;lang=en"></script>
    <br />
        <a href="http://www.verisign.com/ssl-certificate/" target="_blank"  style="color:#000000; text-decoration:none; font:bold 7px verdana,sans-serif; letter-spacing:.5px; text-align:center; margin:0px; padding:0px;">ABOUT SSL CERTIFICATES</a>
      <?php } ?>
</div>
    </div>
</body></html>
