<?php

/**
 * To define all error code and display error mesages in proper format
 * @package Sw Global Llc
 * @author  Navin Sa<nsavar@swglobal.com>
 *
 */

//define ('E001','Payment not done');

final class ErrorMsg{
/**
 *
 * ERROR CONSTANT
 */
  private
  $E001 = array("DisplayText"=>"ZIP code verification failed.","Description"=>"If Valid ZIP Code is enable for any country in backend, and we get invalid ZIP Code response from payment gateway."),
  $E002 = array("DisplayText"=>"Your card is not enabled for Verified by Visa or Master Card Secure Code.","Description"=>"If a country if enabled for “Three D” Verification from backend. And system does not found 3D verification status as approved from Payment gateway."),
  $E003 = array("DisplayText"=>"Your bank has declined the transaction, please contact your bank for further details. Please try another card.","Description"=>"If the payment has been declined from the bank."),
  $E004 = array("DisplayText"=>"You have reached the maximum allowed number of transactions form this computer at the current time. Please try again after <time> or see below.","Description"=>"Number of attempt from one system (IP) is configurable from the backend. If the user are exceding this limit, this message will be displayed."),
  $E005 = array("DisplayText"=>"You have reached the maximum allowed number of transactions from this card for <time>. Please try again after <time> or see below.","Description"=>"Number of successful transaction at a time is configurable from the backend. If the user is exceeding that limit, This message will displayed."),
  $E006 = array("DisplayText"=>"Due to anti fraud reasons this transaction can not be processed currently. Please try again after <time>.","Description"=>"Number of transaction from single email address is configurable from backend. If the user is exceding that limit, This message will be displayed."),
  $E007 = array("DisplayText"=>"Your card has been declined by the Processor, please use another card.","Description"=>"If the Card is Blacklisted by the FPS(Froud Prevention System), This message will be displayed From NMI."),
  $E008 = array("DisplayText"=>"This card is black listed by Admin, please use another card.","Description"=>"If a user has requested or asked for refund, chargeback or refund and block for a order or application which is paid by this card, user will see this message for NMI Gateway."),
  $E009 = array("DisplayText"=>"Your bank has declined the transaction, please contact your bank for further details. Please try another card.","Description"=>"If payment is not done using Graypay gateway by any reason, user will see this message."),
  $E010 = array("DisplayText"=>"It seems that javascript is disabled on your browser. Please retry after enabling it.","Description"=>"If payment user tried to temper the card type and other data or his javascript is disabled, user will see this message."),
  $E011 = array("DisplayText"=>"You have reached the maximum allowed number of transactions form this computer at the current time, please try again after <time> or see below.","Description"=>"If a payment user send payment requests from a particular IP more than allowed number in backend, user will see this message for Graypay Gateway."),
  $E012 = array("DisplayText"=>"You have reached the maximum allowed number of transactions from this card for <time>. Please try again after <time> or see below.","Description"=>"If a payment user try to make successfull payment more than allowed number of payment in backend by a particular card, user will see this message for Graypay Gateway."),
  $E013 = array("DisplayText"=>"Due to anti fraud reasons this transaction can not be processed currently, Please try again after <time>.","Description"=>"If a payment user try to make payment using an email id more than allowed numbers in backend for email, user will see this message for Graypay Gateway."),
  $E014 = array("DisplayText"=>"Your card has been declined by the Processor, please use another card.","Description"=>"If the Card is Blacklisted by the FPS(Froud Prevention System), This message will be displayed From GrayPay."),
  $E015 = array("DisplayText"=>"Your bank has declined the transaction, please contact your bank for further details. Please try another card.","Description"=>"If payment is not done using Graypay gateway for any reason for registered card, user will see this message."),
  $E016 = array("DisplayText"=>"Payment Status updated Successfully","Description"=>"Payment Status updated Successfully"),
  $E017 = array("DisplayText"=>"SW Global LLC order number is invalid.","Description"=>"SW Global LLC order number is invalid."),
  $E018 = array("DisplayText"=>"Application not found.","Description"=>"Application not found."),
  $E019 = array("DisplayText"=>"No record found","Description"=>"No record found"),
  $E020 = array("DisplayText"=>"SW Global LLC order number <orderNumber> has successfully refunded.","Description"=>"SW Global LLC order number <orderNumber> has successfully refunded."),
  $E021 = array("DisplayText"=>"Due to some problem refund request can not process.","Description"=>"Due to some problem refund request can not process."),
  $E022 = array("DisplayText"=>"All application of this order number <orderNumber> has already been refunded.","Description"=>"All application of this order number <orderNumber> has already been refunded."),
  $E023 = array("DisplayText"=>"Some of application of this order number <orderNumber> has already been <refundStatus>ed. so this order number can not be refund and blocked.","Description"=>"Some of application of this order number <orderNumber> has already been <refundStatus>ed. so this order number can not be refund and blocked."),
  $E024 = array("DisplayText"=>"SW Global LLC order number <orderNumber> has successfully refunded and user has blocked","Description"=>"SW Global LLC order number <orderNumber> has successfully refunded and user has blocked"),
  $E025 = array("DisplayText"=>"Due to some problem refund request can not process.","Description"=>"There can be so many reason behind this message as given below:<br/>1. Order number is already refunded with some amount so now amount is more than balance amount.<br/>2. Payment is not done on backend."),
  $E026 = array("DisplayText"=>"All application of this order number <orderNumber> has already been refunded.","Description"=>"All application of this order number <orderNumber> has already been refunded."),
  $E027 = array("DisplayText"=>"Some of application of this order number <orderNumber> has already been <refundStatus>ed. so this order number can not be chargebacked.","Description"=>"Some of application of this order number <orderNumber> has already been <refundStatus>ed. so this order number can not be chargebacked."),
  $E028 = array("DisplayText"=>"SW Global LLC order number <orderNumber> has successfully chargedback.","Description"=>"SW Global LLC order number <orderNumber> has successfully chargedback."),
  $E029 = array("DisplayText"=>"Due to some problem chargeback request can not process.","Description"=>"Due to some problem, Backend cannot be updated for chargeback."),
  $E030 = array("DisplayText"=>"All application of this order number <orderNumber> has already been refunded.","Description"=>"All application of this order number <orderNumber> has already been refunded."),
  $E031 = array("DisplayText"=>"SW Global LLC Order Number is not paid.","Description"=>"SW Global LLC Order Number is not paid."),
  $E032 = array("DisplayText"=>"Order can not refund or chargeback, one or more NIS Application associated with this order number has already process.","Description"=>"Order can not refund or chargeback, one or more NIS Application associated with this order number has already process."),
  $E033 = array("DisplayText"=>"SW Global LLC Order Number is invalid","Description"=>"SW Global LLC Order Number is invalid"),
  $E034 = array("DisplayText"=>"Please insert card first or last digits","Description"=>"Please insert card first or last digits"),
  $E035 = array("DisplayText"=>"Card: <cardNumber> has been unblocked","Description"=>"Card: <cardNumber> has been unblocked"),
  $E036 = array("DisplayText"=>"Refund is not allowed for this order as it has been paid using <gatewayName> gateway.","Description"=>"Refund is not allowed for this order as it has been paid using <gatewayName> gateway."),
  $E037 = array("DisplayText"=>"Invalid Request.","Description"=>"Invalid Request."),
  $E038 = array("DisplayText"=>"Please provide comments.","Description"=>"Please provide comments."),
  $E039 = array("DisplayText"=>"Request successfully <action>","Description"=>"Request successfully <action>"),
  $E040 = array("DisplayText"=>"Invalid Request.","Description"=>"If support user tempered with auth id on approving or rejecting a registered card."),
  $E041 = array("DisplayText"=>"Please provide comments.","Description"=>"Please provide comments."),
  $E042 = array("DisplayText"=>"Request successfully <action>","Description"=>"Request successfully <action>"),
  $E043 = array("DisplayText"=>"Approval Code is not valid.","Description"=>"Approval Code is not valid."),
  $E044 = array("DisplayText"=>"<b><font color=red>You are not authorized, Please try again !</font></b>","Description"=>"<b><font color=red>If payment user login with open id using google or yahoo and goes back and again forward with browser back an forward button then user lost its auth status, user will see this message.</font></b>"),
  $E045 = array("DisplayText"=>"<b><font color=red>Your phone number already exists on this site associated with a different email address.  Please log in with the associated email address or contact customer service for assistance.</font></b>","Description"=>"<b><font color=red>If given mobile is already registered with another payment user, user will see this message.</font></b>"),
  $E046 = array("DisplayText"=>"This applicant is blocked on the protal.","Description"=>"If applicant has been asked for refund, chargeback or refund and block then we block that applicant after refund on our portal so that (s)he can not apply for any NIS application."),
  $E047 = array("DisplayText"=>"Cart Capacity has been full, you can add maximum upto <msg> in cart","Description"=>"Payment user can pay only defined (in backend) number of applications  using cart at a time."),
  $E048 = array("DisplayText"=>"Cart Capacity has been full, total value of cart cannot be above \$<msg>","Description"=>"Payment user can pay applications having defined(in backend) amount using cart at a time."),
  $E049 = array("DisplayText"=>"This application can not be added as <isRefunded> is already initiated for this application.","Description"=>"This application can not be added as <Refund or Refund & Block or Charge Back> is already initiated for this application."),
  $E050 = array("DisplayText"=>"Recharge Unsuccessful,Please try again","Description"=>"Recharge Unsuccessful,Please try again"),
  $E051 = array("DisplayText"=>"Invalid payment.","Description"=>"Invalid payment."),
  $E052 = array("DisplayText"=>"Payment Unsuccessful, Please try again.","Description"=>"This message comes when unsuccessful response comes from VBV payment gateway."),
  $E053 = array("DisplayText"=>"Invalid payment.","Description"=>"This message comes when unsuccessful response comes from VBV payment gateway."),
  $E054 = array("DisplayText"=>"Invalid payment.","Description"=>"This message comes when unsuccessful response comes from VBV payment gateway."),
  $E055 = array("DisplayText"=>"<b><font color=red>This Email already exists on this site. Please log in with another email address. </font></b>","Description"=>"<b><font color=red>This Email already exists on our system. Please use another email address.</font></b>"),
  $E056 = array("DisplayText"=>"Your transaction can not be processed at the moment.\n <URL>","Description"=>"Card holder's last name do not match with Applicant's last name."),
  $E057 = array("DisplayText"=>"Payment Unsuccessful, Please try again.","Description"=>"This message comes when we verify the payment from VBV payment gatewayby using VBV Web Services."),
  $E058 = array("DisplayText"=>"Your visa application has been changed .Please remove it from the cart and add it again.","Description"=>"This message comes when we verify the payment from VBV payment gatewayby using VBV Web Services."),
  $E059 = array("DisplayText"=>"Tampering URL is not allowed!!!.","Description"=>"This message comes when user try to temper URL."),
  $E060 = array("DisplayText"=>"This application has been rejected. Please contact support for further details.","Description"=>"This application has been rejetec. Please contact support for further details."),
  $E061 = array("DisplayText"=>"Your card is not authorized. Please try with another card." , "Description" => "This card is declined by FPS(card country does not match)."),
  $DEFAULT_ERROR = "Payment Cannot be done";

  /**
   * default message format
   */
  private $ERR_MSG_FORMAT = "REFERENCE CODE - <request-detail-id>X<err-code>: <error-message>";

  /**
   *
   * @param string $errorCode
   * @param int $requestDatailId
   * @param array $options for future use
   * @return String Error message in proper format <br> You can change the format of error message by chaging variable <b>ERR_MSG_FORMAT</b>
   *
   */
  public function displayErrorMessage($errorCode, $requestDatailId, $options = array())
  {
    if(isset ($errorCode) && $errorCode!='' && isset ($requestDatailId) && $requestDatailId!=''){


      $tempMsg = $this->$errorCode;

      $displayCode = substr($errorCode, 1);
      $errorMsg = $this->ERR_MSG_FORMAT;

      if($requestDatailId != '001000'){
        $errorMsg = str_replace("<err-code>", $displayCode, $errorMsg);

        $errorMsg = str_replace("<request-detail-id>", $requestDatailId, $errorMsg);
      }else{
        $errorMsg = str_replace("<request-detail-id>X<err-code>:", '', $errorMsg);
        $errorMsg = str_replace("REFERENCE CODE - ", '', $errorMsg);
      }
      $errorMsg = str_replace("<error-message>", $tempMsg['DisplayText'], $errorMsg);

      if(isset($options) && is_array($options) && count($options)>0){
        foreach ($options as $key=>$value){
          $errorMsg = str_replace("<$key>", $value, $errorMsg);
        }
      }
   return $errorMsg;

    }else{
      
      return $this->DEFAULT_ERROR.".";
      
    }
  }
  /**
   *
   */
  public function getErrorMgs($errorCode){
    if(isset ($errorCode) && $errorCode!=''){
      if(property_exists('ErrorMsg',$errorCode))
        {
          $tempMsg = $this->$errorCode;
          return $tempMsg['DisplayText'];          
        }
        else
        {
          return false;
        }

    }else{
      return  "Invalid error code"
;    }
  }
   public function getErrorDescription($errorCode){

    if(isset ($errorCode) && $errorCode!=''){
      if(property_exists('ErrorMsg',$errorCode))
        {
          $tempMsg = $this->$errorCode;
          return $tempMsg['Description'];
        }
        else
        {
          return false;
        }

    }else{
      return  "Invalid error code"
;    }
  }
}
