<table>
  <tbody>
    <tr>
      <th>Id:</th>
      <td><?php echo $transaction_charges->getId() ?></td>
    </tr>
    <tr>
      <th>Merchant:</th>
      <td><?php echo $transaction_charges->getMerchantId() ?></td>
    </tr>
    <tr>
      <th>Gateway:</th>
      <td><?php echo $transaction_charges->getGatewayId() ?></td>
    </tr>
    <tr>
      <th>Transaction charges:</th>
      <td><?php echo $transaction_charges->getTransactionCharges() ?></td>
    </tr>

  </tbody>
</table>

<hr />

<a href="<?php echo url_for('settings/edit?id='.$transaction_charges->getId()) ?>">Edit</a>
&nbsp;
<a href="<?php echo url_for('settings/index') ?>">List</a>
