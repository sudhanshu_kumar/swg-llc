<?php use_helper('Form');?>
<?php use_helper('Url');?>
<style type="text/css">
    /* CSS Document */


    .dlForm {
        display:block;
        position:relative;
        z-index:0;
    }
    .dlForm .multiForm {
        margin-left:auto;
        margin-right:auto;
        width:90%;
    }
    .dlForm fieldset {
        border-color:#CCCCCC!important;
    }
    .dlForm fieldset {
        border-width:0;
        display:block;
    }
    .dlForm fieldset {
        border:1px solid #6ACA83;
        display:block;
        margin:15px auto;
        padding:0 0 15px;
        position:relative;
    }
    .dlForm .multiForm fieldset {
        border-width:1px;
    }
    .dlForm .multiForm {
        margin-left:auto;
        margin-right:auto;
        width:90%;
    }
    .dlForm fieldset {
        border:1px solid #6ACA83;
        display:block;
        margin:15px auto;
        padding:0 0 15px;
        position:relative;
        border-color:#CCCCCC;
    }
    .dlForm legend, .dlForm .legend {
        background-color:#FFFFFF;
        color:#999999;
    }
    .dlForm dl {
        clear:both;
        display:block;
        float:none;
        margin:0;
        min-height:20px;
        padding:10px 0 0;
        position:relative;
        width:auto;
        z-index:0;
    }
    .dlForm .multiForm fieldset dt {
        width:35%;
    }
    .dlForm dt {
        clear:left;
        display:block;
        float:left;
        margin:0 10px 5px 0;
        width:25%;
    }
    dt {
        font-weight:bold;
        line-height:25px;
    }
    .dlForm dt label, .dlForm dt .label {
        display:block;
        font-weight:normal;
        line-height:18px;
        padding-left:5px;
        padding-right:5px;
        text-align:right;
    }
    .dlForm label sup {
        color:#CC0000;
    }
    .dlForm .multiForm fieldset dd {
        width:60%;
    }
    .dlForm dd {
        display:block;
        float:left;
        line-height:18px;
        margin:0 5px 0 0;
    }
    .dlForm ul {
        margin:0;
        padding:0 0 0 30px;
        line-height:20px;
    }
    .dlForm ul.fcol {
        list-style:none outside none;
        margin:0;
        padding:0;
    }
    .dlForm ul.fcol li {
        display:inline;
        list-style:none outside none;
        margin:0 0 0 5px;
        padding:0;
    }
    .X50 {
        margin-left:71px;
        margin-right:71px;
    }
    .Y20 {
        margin-bottom:20px;
        margin-top:20px;
    }
   
   
    .dlForm .inputText, .dlForm input[type="text"] {
        background:none repeat scroll 0 0 #FCFCFC;
        border:1px groove #CCCCCC;
        font-size:inherit;
    }
    .dlForm select {
        background:none repeat scroll 0 0 #FCFCFC;
        border:1px groove #CCCCCC;
        font-size:inherit;
        max-width:305px;
        z-index:0;
    }
   
   
    .dlForm .error {color:red}
    .dlForm .error_list {padding: 5px; margin: 0px 20px;color:red}

    .popupBody
    {background-color:#ccc;margin:0px auto;padding:5px;font-family:arial;}
    .popupContent {height:auto;margin:0px auto;width:480px; *width:100%; font-size:12px;color:#333333;background-color:#fff;padding:10px;font-family:arial;z-index:1000;}
    .flLft
    {float:left;font-family:arial;}
    .flRt
    {float:right;font-family:arial; margin-top:15px;}
    .greenButton {
        background:#6998b0 url(../../images/buttonBg.gif) repeat-x scroll left top;border:1px solid #3e7693;color:#FFFFFF;cursor:pointer;font-family:arial,verdana,helvetica,sans-serif;font-size:12px;height:22px;width:130px;padding:0 6px;text-align:center; font-weight:bold;}
    ul.pBold
    {
        font:bold 11pxArial, Helvetica, sans-serif;list-style-type:none;padding:0px;margin:10px 0 0 0px;
    }
    ul.fontNor, ul.fontNor li
    {font:normal 12px Arial, Helvetica, sans-serif!important;}
    ul.pBold li
    {
        font:bold 11px Arial, Helvetica, sans-serif;list-style-type:none;background:transparent url(../../images/bulletArrow.gif) no-repeat left -1px;padding-left:15px;text-align:left;padding-bottom:10px;
    }
    H3 {
        color: #3e7693;
        font-weight: bold;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 15px;
        margin-top: 0px;
        margin-right: 0px;
        margin-bottom: 3px;
        margin-left: 0px;
        text-transform: capitalize;
        padding: 0px;
    }
    .clear {
        clear: both;
    }
    iframe {overflow:hidden;}
</style>

<body id='popupContent'>
<?php echo form_tag('settings/OpenInfoBox',array('name'=>'pfm_response_form','class'=>'dlForm', 'method'=>'post','id'=>'pfm_response_form')) ?>
<div class="popupBody">
    <div  class="popupContent">
        <center><h3>Edit Limit</h3></center>
        <div class="pBold">

            <div class="wrapForm2">
            <dl id="rechargelimit_amount_row">
                <dt>
                    <label for="rechargelimit_days">
                   <b> User Name</b>
                    </label>
                </dt>
                <dd>
                    <ul class="fcol">
                        <li class="fElement">
                            <?php echo $userDetail->getFirst()->getFirstName();?><input type="hidden" name="uid" value="<?php echo $userId?>">
                        </li>
                        <li class="help"/>
                        <li class="error"/>
                        <li class="hidden"/>
                    </ul>
                </dd>
              </dl>
                    <?php echo $form;?>

               

            </div>


        </div>

        <div align="center">
            <div class="flRt"><input name="" type="submit" value="Save" class="greenButton" />
                &nbsp;
            <input name="" type="button" value="Close" class="greenButton" onClick=" parent.emailwindow.hide();" /></div>
            </form>
        </div>
        <div class="clear"></div>
    </div>
</div>
</body>
</html>
