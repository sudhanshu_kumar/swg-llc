<?php use_helper('Pagination');
use_helper('EPortal');
?>
<?php
use_javascript('dhtmlwindow.js');
use_stylesheet('dhtmlwindow.css');
use_javascript('modal.js');
use_stylesheet('modal.css');
?>
<?php
$sf = sfContext::getInstance()->getUser();
if($sf->hasFlash('notice')){
  ?><div id="flash_notice" class="alertBox" ><?php
  echo nl2br($sf->getFlash('notice'));

  ?></div><?php }?>
<div class="clearfix">

    <table class="innerTable_content" >
        <tr>

            <td align="right" class="blbar" colspan="6">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td align="center"><b>User Name</b></td>
            <td align="center"><b>Email</b></td>
            <td align="center"><b>Limit<br/>Days | Amount</b></td>
            <td align="center"><b>Limit Type</b></td>
            <td align="center"><b>Action</b></td>
        </tr>
        <?php
        if(($pager->getNbResults())>0) {
            $limit = sfConfig::get('app_records_per_page');
            $i = max(($page-1),0)*$limit ;
            foreach ($pager->getResults() as $result):

            $i++;
            if($username =='')
            $username = "BLANK";
            if($email =='')
            $email = "BLANK";
            if(count($result->getSfGuardUser()->getUserRechargeConfig())){
                $amount = $result->getSfGuardUser()->getUserRechargeConfig()->getFirst()->getAmount();
                $globalSetting = 'Customized';
                $days = $result->getSfGuardUser()->getUserRechargeConfig()->getFirst()->getDays();
            }
            else{
                $amount = Settings::getRechargeMaxAmount();
                $days = Settings::getRechargeNoOfDays();
                $globalSetting = 'Global';}
            ?>
        <tr>
            <td align="center"><?php echo $result->getFirstName() ?>&nbsp;<?php echo $result->getLastName() ?></td>
            <td align="center"><?php echo $result->getEmail() ?></td>
            <td align="center"><?php echo $days?> | <?php echo format_amount($amount,1)?></td>
            <td align="center">
                  <?php  echo $globalSetting;  ?>
            </td>
          

            <td align="center">
            <a href="#" onClick="opennEditRecharge(<?php echo $result->getUserId()?>); return false">Edit Limit</a>
                <?php if($globalSetting == 'Customized'){?>
                   | <a  href="#" title="Set As Global" id='act' onclick="activateDeactivate(<?php echo $result->getUserId(); ?>);">Set As Global</a>
                   <?php }?>
           </td>

        </tr>
        <?php endforeach; ?>

        <tr>
            <td colspan="7" class="blbar" height="25" align="right">
                <div class="paging pagingFoot"><?php  echo ajax_pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName().'?username='.$username.'&email='.$email), 'result_div')   ?>
                </div>
            </td>
        </tr>
        <?php }
    else { ?>
        <tr><td  align='center' colspan="7" ><div style="color:red"> No record found</div></td></tr>
        <?php } ?>



    </table>




    <div>&nbsp;</div>

</div>
<script>

    function activateDeactivate(uid){
        var url = '<?php echo url_for('settings/configDelete'); ?>';
        if(uid != ''){

            $.post(url, { uid: uid},
            function(data){
                $.post('ewalletRechargeSearch',$("#pfm_response_form").serialize(), function(data){

                if(data == "logout"){
                    location.reload();
                }else{
                    $("#result_div").html(data);
                }
            });
                
            });
        }

    }




    function opennEditRecharge(uid){
        emailwindow=dhtmlmodal.open('EmailBox', 'iframe', 'openInfoBox?uid='+uid, 'Edit ewallet recharge limit', 'width=550px,height=335px,center=1,border=0, scrolling=no')

        emailwindow.onclose=function(){
            var theform=this.contentDoc.forms[0]
            var theemail=this.contentDoc.getElementById("emailfield")
            if (theemail.value.indexOf("@")==-1){
                alert("Please enter a valid email address")
                return false
            }
            else{
                document.getElementById("youremail").innerHTML=theemail.value //Assign the email to a span on the page
                return true ;
            }
        }
    }

</script>
