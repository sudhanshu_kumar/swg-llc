<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>

<form action="<?php echo url_for('settings/update?id='.$id) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>

<?php if (!$form->getObject()->isNew()): ?>
<input type="hidden" name="sf_method" value="put" />
<?php endif; ?>
<table width="100%" style="margin:auto; vertical-align:top;" >
      <input type="hidden" name="id" id="id" value="<?php echo $id;?>" />
        <tr>
            <td><b>Gateway</b></td>
            <td><?php echo $gateway; ?></td>
        </tr>

        <tr>
            <td><b><?php echo $form['transaction_charges']->renderLabel(); ?></b><span class="red">*</span></td>
            <td><?php echo $form['transaction_charges']->render(); echo ' '.'%' ;?>
                <br>
                <div class="red" id="transaction_charges_error">
                    <?php echo $form['transaction_charges']->renderError(); ?>
                </div>

            </td>
        </tr>
    <tfoot>
      <tr>
        <td colspan="2" align="center">
        
          &nbsp;       
          <input type="button" value="Back" id="Back" name="Back" class="normalbutton" onclick="window.location.href='<?php echo url_for('settings/index')?>'" />
         
        <input type="submit" value="Update" id="update" name="update" class="normalbutton" />
        </td>
      </tr>
    </tfoot>
  </table>
  
</form>
