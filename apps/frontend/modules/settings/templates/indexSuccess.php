
<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">
<?php use_helper('Pagination');
include_partial('global/innerHeading',array('heading'=>'Transaction Charges List'));?>
  <div class="clear"></div>
<div class="tmz-spacer"></div>
 <table width="100%">
            <tr>
                <td class="blbar"  colspan="5" align="left"><div style="float:left">Transaction Charges</div>

                </td>
            </tr>
            <tr>
                <td width="1%" class="txtBold">Id</td>                
                <td width="10%" class="txtBold">Gateway</td>
                <td width="15%" class="txtBold">Transaction charges</td>
                <td width="10%" class="txtBold">Action</td>
            </tr>


          <?php foreach ($transaction_chargess as $transaction_charges): ?>
            <tr>
                <td width="50px"><span ><!--<a href="<?php echo url_for('settings/show?id='.$transaction_charges->getId()) ?>">--><?php echo $transaction_charges->getId() ?></span></td>
              
                <td width="50px"><span ><?php echo $transaction_charges->getGateway()->getName() ?></span></td>
                <td width="50px"><span ><?php echo $transaction_charges->getTransactionCharges() ?></span></td>
                <td><a href="<?php echo url_for('settings/edit?id='.Settings::encryptInput($transaction_charges->getId())) ?>"><img src="<?php echo image_path('edit_icon.png'); ?>" alt="Edit" title="Edit" /></a></td>
            </tr>
            <?php endforeach; ?>

        </table>
  
</div>
</div>
<div class="content_wrapper_bottom"></div>