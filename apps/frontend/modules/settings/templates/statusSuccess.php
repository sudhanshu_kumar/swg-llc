


<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">
  <?php use_helper('Pagination');
include_partial('global/innerHeading',array('heading'=>'GrayPay Vault'));?>
 <div class="clear"></div>
<div class="tmz-spacer"></div>
  <table width="100%">
    <tr>
      <td class="blbar"  colspan="5" align="left"><div style="float:left">GrayPay Vault</div>

      </td>
    </tr>
    <tr>
      <td width="1%" class="txtBold" align="center">Id</td>
      <td width="10%" class="txtBold" align="center">GrayPay Vault Status</td>
      <td width="15%" class="txtBold" align="center">Action</td>
    </tr>


    <?php foreach ($status as $status): ?>
    <tr>
      <td width="50px" align="center"><span ><!--<a href="<?php echo url_for('settings/show?id='.$status->getId()) ?>">--><?php echo $status->getId() ?></span></td>

      <td width="50px" align="center"><span ><?php echo ucwords($status->getVarValue()) ?></span></td>
      <td align="center">
        <?php if($status->getVarValue() == 'active'){ //echo $status->getIsActive();?>
        <a class="deactiveInfo"  href="#" title="De-activate" id='act' onclick="activateDeactivate(<?php echo $status->getId(); ?>,'inactive');"></a>
        <?php }else { ?>
        <a class="activeInfo" title="Activate" id='dact' onclick="activateDeactivate(<?php echo $status->getId(); ?>,'active');"> </a>
        <?php }?>    
      </td>
    </tr>
    <?php endforeach; ?>

  </table>

</div>
</div>
<div class="content_wrapper_bottom"></div>
<script>
    function activateDeactivate(id,status){

        var  showStatus;
        if(status == 'active'){
            showStatus = 'active';
        }
        if(status == 'inactive'){
            showStatus = 'inactive';

    }
        var answer = confirm("Do you want to "+showStatus+" GrayPay Vault ?")
        if(answer){
            var url = "<?php echo url_for('settings/setStatus'); ?>";         
//alert(url);
                $.post(url, {id: id,status: status},
                function(data){ window.location.reload( true );
                  //alert(data);
                    //setTimeout( refresh(showStatus), 10 );
                });
            
        }
    }
 
</script>