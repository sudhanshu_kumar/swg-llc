<?php use_helper('Form');?>
<?php include_partial('global/innerHeading',array('heading'=>'Ewallet Recharge Setting'));?>
<?php
use_javascript('dhtmlwindow.js');
use_stylesheet('dhtmlwindow.css');
use_javascript('modal.js');
use_stylesheet('modal.css');
?>
<div class="global_content4 clearfix">
    <div class="brdBox">
        <div class="clearfix"/>
        <?php echo form_tag('settings/ewalletRechargeSearch',array('name'=>'pfm_response_form','class'=>'', 'method'=>'post','id'=>'pfm_response_form')) ?>
        <div class="wrapForm2">
            <table width="50%">                
                <tr>
                    <th>User Name</th>
                    <td><input id="username" class="txt-input" type="text"   name="username"/></td>
                </tr>
                <tr>
                    <th>Email</th>
                    <td><input id="email" class="txt-input" type="text"   name="email"/></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td><div class="lblButton">
                            
                            <?php   echo submit_tag('Search',array('class' => 'button','onclick'=>"return validate()")); ?>

                        </div>
                        <div class="lblButtonRight">
                            <div class="btnRtCorner"></div>
                    </div></td>
                </tr>
            </table>
            <br />
            <div id="result_div" style="width:500px" align="center"></div>
        </div>
        </form>
    </div>
</div>

<script>
  
 function validate() {     
        document.getElementById("result_div").innerHTML='';
        var errmsg = "";
        var flag = 0;
        if( ($('#username').val()=='')&&($('#email').val()=='')){
            flag++;          
        }        

        if(flag++)
        {
            alert('Please enter user name or email ')
        }
        else{
            validate_search_form();
        }
        return false;
    }

    function validate_search_form(){
        $('#result_div').html('<?php echo image_tag('../images/ajax-loader.gif'); ?>');
        var err = 0;
        if(err == 0){
            $.post('ewalletRechargeSearch',$("#pfm_response_form").serialize(), function(data){

                if(data == "logout"){
                    location.reload();
                }else{
                    $("#result_div").html(data);
                }
            });
        }

    }


</script>


