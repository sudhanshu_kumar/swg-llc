<?php

/**
 * settings actions.
 *
 * @package    ama
 * @subpackage settings
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class settingsActions extends sfActions {

    public function executeIndex(sfWebRequest $request) {
        $this->transaction_chargess = Doctrine::getTable('TransactionCharges')
                        ->createQuery('a')
                        ->execute();
    }

    public function executeStatus(sfWebRequest $request) {
        $this->status = Doctrine::getTable('GlobalSettings')
                        ->createQuery('a')
                        ->execute();
    }

    public function executeSetStatus(sfWebRequest $request) {
        $this->setTemplate(null);
        $id = $request->getParameter('id');
        $status = $request->getParameter('status');
        if ($status != '') {
            $user = Doctrine::getTable('GlobalSettings')->find(array($id));
            if ($status == "active") {
                $user->setVarValue('active');
                $user->save();
                $this->getUser()->setFlash('notice', 'User De-activated successfully.');
            }
            if ($status == "inactive") {
                $user->setVarValue('inactive');
                $user->save();
                $this->getUser()->setFlash('notice', 'User Activated successfully.');
            }
        }
        $this->forward('settings', 'status');
    }

    public function executeShow(sfWebRequest $request) {
        $this->transaction_charges = Doctrine::getTable('TransactionCharges')->find(array($request->getParameter('id')));
        $this->forward404Unless($this->transaction_charges);
    }

    public function executeNew(sfWebRequest $request) {
        $this->form = new TransactionChargesForm();
    }

    public function executeCreate(sfWebRequest $request) {
        $this->forward404Unless($request->isMethod(sfRequest::POST));

        $this->form = new TransactionChargesForm();

        $this->processForm($request, $this->form);

        $this->setTemplate('new');
    }

    public function executeEdit(sfWebRequest $request) {
        $this->forward404Unless($transaction_charges = Doctrine::getTable('TransactionCharges')->find(array(Settings::decryptInput($request->getParameter('id')))), sprintf('Object transaction_charges does not exist (%s).', $request->getParameter('id')));
        $this->id = $transaction_charges->getId();
        // $this->merchant = $transaction_charges->getMerchant()->getName();
        $this->gateway = strtoupper($transaction_charges->getGateway()->getName());
        $this->form = new TransactionChargesForm($transaction_charges);
        $this->form->setDefault('transaction_charges', $transaction_charges->getTransactionCharges());
    }

    public function executeUpdate(sfWebRequest $request) {
        $this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
        $this->forward404Unless($transaction_charges = Doctrine::getTable('TransactionCharges')->find(array($request->getParameter('id'))), sprintf('Object transaction_charges does not exist (%s).', $request->getParameter('id')));
        $this->form = new TransactionChargesForm();
        $this->id = $transaction_charges->getId();
        //$this->merchant = $transaction_charges->getMerchant()->getName();
        $this->gateway = strtoupper($transaction_charges->getGateway()->getName());

        $this->processForm($request, $this->form);

        $this->setTemplate('edit');
    }

    public function executeDelete(sfWebRequest $request) {
        $request->checkCSRFProtection();

        $this->forward404Unless($transaction_charges = Doctrine::getTable('TransactionCharges')->find(array($request->getParameter('id'))), sprintf('Object transaction_charges does not exist (%s).', $request->getParameter('id')));
        $transaction_charges->delete();

        $this->redirect('settings/index');
    }

    protected function processForm(sfWebRequest $request, sfForm $form) {
        $form->bind($request->getParameter('trans_charge'));
        if ($form->isValid()) {

            $transaction_chargesObj = Doctrine::getTable('TransactionCharges')->find(array($request->getParameter('id')));
            $transaction_chargesObj->setTransactionCharges($form['transaction_charges']->getvalue());
            $transaction_charges = $transaction_chargesObj->save();
            $username = $this->getUser();
            $user_id = $this->getUser()->getGuardUser()->getId();
            $event_description = EpAuditEvent::$MSG_SUBCATEGORY_TRANSACTION_CHARGE_CHANGE;
            $applicationArr = array(new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_USERINFO, $username, $user_id));

            $eventHolder = new pay4meAuditEventHolder(
                            EpAuditEvent::$CATEGORY_TRANSACTION,
                            EpAuditEvent::$SUBCATEGORY_TRANSACTION_CHARGE_CHANGE,
                            EpAuditEvent::getFomattedMessage($event_description, array('username' => $username)),
                            $applicationArr);

            $this->dispatcher->notify(new sfEvent($eventHolder, 'epAuditEvent'));
            $this->redirect('settings/index');
        } else {
            //echo $form['transaction_charges']->renderError();
            // die('sd');
        }
    }

    public function executeEwalletRecharge(sfWebRequest $request) {}

    public function executeEwalletRechargeSearch(sfWebRequest $request) {

        $this->username = $request->getParameter('username');
        $this->email = $request->getParameter('email');
        $userObj = Doctrine::getTable('UserDetail')->getUserDetail($this->username, $this->email);
        //                              echo "<pre>";print_r($userObj->execute()->toArray());die;
        $this->page = 1;

        if ($request->hasParameter('page')) {
            $this->page = $request->getParameter('page');
        }

        $this->pager = new sfDoctrinePager('UserDetail', sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($userObj);
        $this->pager->setPage($this->page);
        $this->pager->init();
    }

    public function executeModifyLimit(sfWebRequest $request) {
        $userId = $request->getParameter('uid');
        $postDataArray = $request->getParameterHolder()->getAll();
        $postDataArray = $postDataArray['rechargelimit'];
        $rechargeCongif = Doctrine::getTable('UserRechargeConfig')->findByUserId($userId);
        if (count($rechargeCongif)) {
            $rechargeCongif->getFirst()->setAmount($postDataArray['amount']);
            $rechargeCongif->getFirst()->setDays($postDataArray['days']);
        } else {

            $rechargeCongif = new UserRechargeConfig();
            $rechargeCongif->setUserId($userId);
            $rechargeCongif->setAmount($postDataArray['amount']);
            $rechargeCongif->setDays($postDataArray['days']);
        }
        $rechargeCongif->save();
        $this->getUser()->setFlash('notice', sprintf('Recharge limit  updated  successfully'));
        return $this->renderText("<script>parent.emailwindow.hide();parent.validate_search_form();</script>");
    }

    public function executeConfigDelete(sfWebRequest $request) {
        $userId = $request->getParameter('uid');
        $rechargeCongif = Doctrine::getTable('UserRechargeConfig')->findByUserId($userId);
        $rechargeCongif->delete();
        $this->getUser()->setFlash('notice', sprintf('Recharge limit  globalized successfully'));
        $this->forward($this->getModuleName(), 'ewalletRechargeSearch');
    }

    public function executeOpenInfoBox(sfWebRequest $request) {
        $userId = $request->getParameter('uid');
        $this->form = new rechargeLimitForm();
        $this->userId = $request->getParameter('uid');
        $limitRecharge = Doctrine::getTable('UserRechargeConfig')->findByUserId($this->userId);
        $this->userDetail = Doctrine::getTable('UserDetail')->findByUserId($this->userId);
        if (count($limitRecharge)) {
            $amount = $limitRecharge->getFirst()->getAmount();
            $days = $limitRecharge->getFirst()->getDays();
            $this->form->setDefault('amount', $amount);
            $this->form->setDefault('days', $days);
        }
        if ($request->isMethod('post')) {

            $this->form->bind($request->getParameter('rechargelimit'));
            if ($this->form->isValid()) {
                $this->forward('settings', 'modifyLimit');
            }
        }
        $this->setTemplate('openInfoBox');
        $this->setLayout(false);
    }

}
