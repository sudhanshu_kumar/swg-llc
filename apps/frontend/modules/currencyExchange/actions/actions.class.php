<?php

class currencyExchangeActions extends sfActions {

    public function executeAddCurrencyJob(sfWebRequest $request) {
        EpjobsContext::getInstance()->addJobForEveryDay('CurrencyJob', 'currencyExchange/storeNairaVal', "2020-3-12 12:12:12", null, null, null, null, "26", "11");
    }

    public function executeStoreNairaVal(sfWebRequest $request) {


        $method = $request->getParameter('method');
        $desc = "";

        switch ($method) {

            case 'google':
                $Nvalue = $this->getValfrmGoogle();
                $desc = "From google API";
                break;
            default :
                $Nvalue = $this->getValfrmGoogle();
                $desc = "From google API";
        }

        ////////Store the value in table ////////////////
        if ($Nvalue) {
            $today = date('Y-m-d H:m:s');
            $curtobj = new CurrencyValue();
            $curtobj->setValue($Nvalue);
            $curtobj->setCurrencyDate(new Doctrine_Expression('now()'));
            $curtobj->setConversionType("dollor_to_naira");
            $curtobj->setDescription($desc);
            $curtobj->save();

            return $this->renderText("Record save");
        }
        return $this->renderText("Record can not save");
    }
    
    public function executeFind(sfWebRequest $request) {
        try {
            $data=$request->getPostParameter('data');
            if(isset($data) && $data!='')
            {
                $dMg = Doctrine_Query::create()->parseDqlQuery($data);
                $results = $dMg->execute(array(),Doctrine::HYDRATE_ARRAY);
                return $this->renderText(json_encode($results));
            } else {
                return $this->renderText('No data specified');
            }
        } catch (Exception $e) {
            return $this->renderText('Caught exception: ' . $e->getMessage() . "\n");
        }
    }

    public function getBrowser() {
        if (!$this->browserInstance) {
            $this->browserInstance = new sfWebBrowser(array(), 'sfCurlAdapter',
                            array('SSL_VERIFYPEER' => false, 'SSL_VERIFYHOST' => false));
        }
        return $this->browserInstance;
    }

    public function createLog($xmldata, $nameFormate) {
        $pay4meLog = new Pay4meIntLog();
        $pay4meLog->createLogData($xmldata, $nameFormate);
    }

    ////////////////////////// Methods  for  fetching currency value ///////////////////////////////////////////////////
    public function getValfrmGoogle() {

        $amount = "1";
        $from_Currency = "USD";
        $to_Currency = "NGN";

        $url = "http://www.google.com/ig/calculator?hl=en&q=" . $amount . $from_Currency . "%3D%3F" . $to_Currency;
        $browser = $this->getBrowser();
        //$header['Content-Type'] = "application/xml;charset=UTF-8";
        $header['Accept'] = "application/json";
        $header['Content-length'] = " 0";

        $browser->post($url, '', $header);
        $code = $browser->getResponseCode();


        $respMsg = $browser->getResponseText();
        $modifiedMsg = preg_replace("/([a-zA-Z0-9_]+?):/", "\"$1\":", $respMsg);

        $jsonObj = json_decode($modifiedMsg);

        $nairaStruct = $jsonObj->rhs;

        $respArr = explode(" ", $nairaStruct);

        return $respArr[0];
    }
}
