<?php use_helper('EPortal');?>
<b>Hello <?php echo ucwords($name); ?>,</b>

<br /><br />Your Transaction Limit is successfully added.<br /><br />


<table style="background-color: rgb(255, 255, 255); border: 1px solid rgb(229, 236, 249);" width="100%" border="0" cellpadding="4" cellspacing="0">
  <tr>
    <td>
      <table style="background-color: rgb(229, 236, 249);" width="100%" border="0" cellpadding="2" cellspacing="0"><tbody>

          <tr><td>Details are mentioned below</td></tr>

      </tbody></table>
    </td>
  </tr>
  <tr>
    <td>
      <table style="background-color: rgb(255, 255, 255); border: 1px solid rgb(229, 236, 249);" width="100%" border="0" cellpadding="4" cellspacing="0">


       <?php  if ($cart_capacity != "") {?>
        <tr style="margin-right: 15px; margin-left: 5px;" valign="top">
          <td colspan="10" style="font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); font-weight: bold; color: black; width: 15%; text-align: left; padding-right: 1em;" nowrap="nowrap">&nbsp;<b>Cart Capacity:</b>&nbsp;</td>
          <td colspan="50" style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); width: 15%;" align="left">
          <?php echo $cart_capacity; ?></td>
        </tr>

      <?php } if ($cart_amount_capacity != "") {?>
        <tr style="margin-right: 15px; margin-left: 5px;" valign="top">
          <td colspan="10" style="font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); font-weight: bold; color: black; width: 15%; text-align: left; padding-right: 1em;" nowrap="nowrap">&nbsp;<b>Cart Amount capacity:</b>&nbsp;</td>
          <td colspan="50" style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); width: 15%;" align="left">
          <?php echo $cart_amount_capacity; ?></td>
        </tr>

      <?php } if ($number_of_transaction != "") {?>
        <tr style="margin-right: 15px; margin-left: 5px;" valign="top">
          <td colspan="10" style="font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); font-weight: bold; color: black; width: 15%; text-align: left; padding-right: 1em;" nowrap="nowrap">&nbsp;<b>Allowed Number of Transaction(s):</b>&nbsp;</td>
          <td colspan="50" style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); width: 15%;" align="left">
          <?php echo $number_of_transaction; ?></td>
        </tr>
      <?php } if ($transaction_period != "") {?>
        <tr style="margin-right: 15px; margin-left: 5px;" valign="top">
          <td colspan="10" style="font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); font-weight: bold; color: black; width: 15%; text-align: left; padding-right: 1em;" nowrap="nowrap">&nbsp;<b>Transaction Period:</b>&nbsp;</td>
          <td colspan="50" style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); width: 15%;" align="left">
          <?php echo $transaction_period; ?></td>
        </tr>
        <?php } ?>


      </table>
    </td>
  </tr>
</table>
<br><br>
<br /><br />

  <?php echo $signature;?>


