<?php use_helper('EPortal'); ?>
<b>Hello <?php echo ucwords($name); ?>,</b> <br/><br/>
<?php $rDetails = unserialize(base64_decode($rDetails));  ?>

<table style="background-color: rgb(255, 255, 255); border: 1px solid rgb(229, 236, 249);" width="100%" border="0" cellpadding="4" cellspacing="0">
    <tr>
        <td>
            <table style="background-color: rgb(229, 236, 249);" width="100%" border="0" cellpadding="2" cellspacing="0"><tbody>

                    <tr>
                        <td style="font-size: 83%;">&nbsp;Support Category: <b><?php echo nl2br($supportCategory); ?></b></td>
                        <td style="padding-right: 15px;" nowrap="nowrap" align="right"></td>
                    </tr>
                    <?php if(isset ($orderNumber) && $orderNumber != '') {  ?>
                    <tr>
                        <td style="font-size: 83%;">&nbsp;Order Number: <b><?php echo nl2br($orderNumber); ?></b></td>
                        <td style="padding-right: 15px;" nowrap="nowrap" align="right"></td>
                    </tr>
                    <?php } ?>
                    <?php if($request_for != 'N/A') {  ?>
                    <tr>
                        <td style="font-size: 83%;">&nbsp;Request For: <b><?php echo nl2br($request_for); ?></b></td>
                        <td style="padding-right: 15px;" nowrap="nowrap" align="right"></td>
                    </tr>
                    <?php } ?>

<?php
foreach ($supportList as $list):
?>
                    <tr>
                        <td style="font-size: 83%;">&nbsp;Customer Comments On <?php echo date_format(date_create($list['created_at']),'Y-m-d');?> :<b><?php if(isset ($list['customer_comments'])) { echo nl2br($list['customer_comments']); } else { echo "N/A"; } ?></b></td>
                        <td style="padding-right: 15px;" nowrap="nowrap" align="right"></td>
                    </tr>
                    <tr>
                        <td style="font-size: 83%;">&nbsp;SWGlobal Comments On <?php echo date_format(date_create($list['updated_at']),'Y-m-d');?> :<b><?php if(isset ($list['support_user_comments'])) {echo nl2br($list['support_user_comments']); } else { echo "N/A"; }?></b></td>
                        <td style="padding-right: 15px;" nowrap="nowrap" align="right"></td>
                    </tr>
<?php endforeach; ?>


                </tbody></table>
            <table>
                <tr>
                <br/><br/>
<?php echo "You can view your support request detail comment(s) by "; ?>
<?php echo link_to1('Clicking Here', $rDetails['url']); ?>
                <br/>
                <table style="background-color: rgb(229, 236, 249);" width="100%" border="0" cellpadding="2" cellspacing="0"><tbody>
                        <tr>
                            <td style="font-size: 83%;">&nbsp;<strong>NOTE:</strong>If you are not able to click on above link please copy paste below url in your browser address bar</td>
                        </tr>
                        <tr>
                            <td style="font-size: 83%;"><?php $url = url_for($rDetails['url']);
                    echo $url; ?></td>
                            </tr>
                        </tbody></table>
                    <br/><br/>
        </tr>
    </table>
    </td>
    </tr>


    </table>
<?php echo $signature; ?>


