<?php
$currencySymbol = CurrencyManager::currencySymbolByCurrencyId($currencyId);
?>
<?php use_helper('EPortal');?>
<b>Hello <?php echo ucwords($name); ?>,</b>
<br /><br />Your refund has been made from <b><a href="https://www.swgloballlc.com">SW Global LLC</a><br /><br /></b>


<?php //$rDetails = unserialize(base64_decode($request_details));?>
<!--<br /><br />You have successfully done payment on SW Global LLC , transfers and other services (published terms of use apply).<br /><br/>
-->

<table style="background-color: rgb(255, 255, 255); border: 1px solid rgb(229, 236, 249);" width="100%" border="0" cellpadding="4" cellspacing="0">
  <tr>
    <td>
    <table style="background-color: rgb(229, 236, 249);" width="100%" border="0" cellpadding="2" cellspacing="0"><tbody>

        <tr>
            <td style="font-size: 83%;">&nbsp;SW Global LLC order number: <b><?php  echo $orderNumber;;?></b></td>
            <td style="padding-right: 15px;" nowrap="nowrap" align="right"></td></tr>
        </tbody></table>
    </td>
  </tr>
 
  <tr>
    <td>
        <table style="background-color: rgb(255, 255, 255); border: 1px solid rgb(229, 236, 249);" width="100%" border="0" cellpadding="4" cellspacing="0">
            <tr style="margin-right: 15px; margin-left: 5px;" valign="top">
                <td colspan="10" style="font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); font-weight: bold; color: black; width: 15%; text-align: left; padding-right: 1em;" nowrap="nowrap">&nbsp;<b>Payment Status</b>&nbsp;</td>
                <td colspan="75" style="font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); font-weight: bold; color: black; width: 75%; text-align: left;"><b>Item</b></td>

                <td colspan="5" style="font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); font-weight: bold; color: black; width: 5%; text-align: right; padding-right: 1em;">&nbsp;&nbsp;<b>Price</b></td>
                <td colspan="5" style="font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); font-weight: bold; color: black; width: 5%;">&nbsp;</td>
             </tr>
            <tr style="margin-right: 15px; margin-left: 5px;" valign="top">
                <td colspan="10" style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); width: 15%;" align="left">
            &nbsp;&nbsp;<b>Refunded</b></td>
                <td colspan="75" style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); width: 75%;" align="left">
                <b><span style="font-weight: bold; color: black;"><?php  echo $merchantName;?></span></b>
            &nbsp;&nbsp;


            </td>
                <td colspan="5" style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); width: 5%; text-align: right; padding-right: 1em;"><?php echo format_amount($amount,1,0,$currencySymbol); ?></td>

                <td colspan="5" style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); width: 5%;" align="right">&nbsp;</td>
            </tr>
            <tr style="background-color: rgb(229, 236, 249);">
                <td rowspan="4" colspan="70" style="width: 70%;"></td>
                <td colspan="20" style="font-weight: bold; font-size: 105%; width: 20%; text-align: right; vertical-align: text-top;">Total:</td>
                <td colspan="5" style="font-weight: bold; font-size: 105%; width: 5%; text-align: right; white-space: nowrap; padding-right: 1em;"><?php  echo format_amount($amount,1,0,$currencySymbol);  ?></td>
                <td colspan="5" style="font-weight: bold; font-size: 105%; width: 5%;">&nbsp;</td></tr>
        </table>
    </td>
  </tr>
   
 </table>
<br><br>
Please be adviced that respective NIS applications (Visa and/or Passport) will be cancelled immediately. <br />
If a Passport or Visa has already been provided to you, it will be REVOKED and any attempt to use <br />
such will be subject to prosecution.

<br /><br /><br /><br />

<?php echo $signature;?>


