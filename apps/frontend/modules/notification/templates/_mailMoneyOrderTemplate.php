
<?php use_helper('EPortal');?>
<b>Hello <?php echo ucwords($name); ?>,</b>
<br /><br />Thank you for buying from <?php echo sfConfig::get('app_swglobal_as_merchant');?> using  <b><a href="https://www.swgloballlc.com">SW Global LLC</a>!<br /><br /></b>


<?php $rDetails = unserialize(base64_decode($request_details)); ?>


<?php
$pound = 5;
if($rDetails['currency'] == $pound){
    $moneyOrderAmount = $rDetails['convert_amount'];    
}else{
    $moneyOrderAmount = $rDetails['amount'];    
}
$currencySymbol = CurrencyManager::currencySymbolByCurrencyId($rDetails['currency']);
?>

<!--<br /><br />You have successfully done payment on SW Global LLC , transfers and other services (published terms of use apply).<br /><br/>
-->

<table style="background-color: rgb(255, 255, 255); border: 1px solid rgb(229, 236, 249);" width="100%" border="0" cellpadding="4" cellspacing="0">
  <tr>
    <td>
    <table style="background-color: rgb(229, 236, 249);" width="100%" border="0" cellpadding="2" cellspacing="0"><tbody>

        <tr>
            <td style="font-size: 83%;">&nbsp;Order date: <b><?php  echo $rDetails['paid_date'];?></b><br>&nbsp;SW Global LLC order number: <b><?php  echo $orderNumber;;?></b></td>
            <td style="padding-right: 15px;" nowrap="nowrap" align="right"></td></tr>
        </tbody></table>
    </td>
  </tr>
 <!-- <tr style="color:#00345A;background-color:#E7F0F7;">
    <td  colspan="4">Order Date: <b><?php  //echo $rDetails['paid_date'];?> </b></td>
  </tr>
   <tr style="color:#00345A;background-color:#E7F0F7;">
    <td  colspan="4">SW Global LLC order number:  <b><?php  //echo $orderNumber;;?></b> </td>
  </tr>
  -->
  <tr>
    <td>
        <table style="background-color: rgb(255, 255, 255); border: 1px solid rgb(229, 236, 249);" width="100%" border="0" cellpadding="4" cellspacing="0">
            <tr style="margin-right: 15px; margin-left: 5px;" valign="top">
                <td colspan="10" style="font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); font-weight: bold; color: black; width: 15%; text-align: left; padding-right: 1em;" nowrap="nowrap">&nbsp;<b>Payment Status</b>&nbsp;</td>
                <td colspan="75" style="font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); font-weight: bold; color: black; width: 75%; text-align: left;"><b>Item</b></td>

                <td colspan="5" style="font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); font-weight: bold; color: black; width: 5%; text-align: right; padding-right: 1em;">&nbsp;&nbsp;<b>Price</b></td>
                <td colspan="5" style="font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); font-weight: bold; color: black; width: 5%;">&nbsp;</td>
             </tr>
            <tr style="margin-right: 15px; margin-left: 5px;" valign="top">
                <td colspan="10" style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); width: 15%;" align="left">
            &nbsp;&nbsp;<b>Successful</b></td>
                <td colspan="75" style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); width: 75%;" align="left">
                <b><span style="font-weight: bold; color: black;"><?php  echo $rDetails['merchant_name'];?></span></b>
            &nbsp;-&nbsp;
<?php
            if(is_numeric($rDetails['cardName']))
            {
?>
            <span style="color: rgb(102, 102, 102);">Number of appplications for which payment has been made is <?php echo $rDetails['cardName'];?>.<br> Your applications will be processed further on <?php echo $rDetails['mabbr']; ?> Portal.</span>
<?php
            }
            else
            {
     ?>
            <span style="color: rgb(102, 102, 102);">Payment for Service<?php //echo $rDetails['mdesc']; ?> for customer <?php echo $rDetails['cardName'];?>.<br> Your application will be processed further on <?php echo $rDetails['mabbr']; ?> Portal.</span>
    <?php
            }
?>
            <table><tbody>
                    <tr style="background-color: rgb(229, 236, 249);" width="100%" border="1" cellpadding="2" cellspacing="0">
                        <td>S. No.</td>
                        <td>Application Type</td>
                        <td>Application Id</td>
                        <td>Reference Number</td>
                    </tr>
           <?php
           $i=1;
           $visaArrivalFlag = false;
           foreach ($rDetails['ipayNisAppDetails'] as $ipayNisDetails):
           if(strtolower($ipayNisDetails['app_type']) == 'vap'){ $visaArrivalFlag = true; }
              ?>
              <tr >
                        <td><?php echo $i; ?></td>
                        <td><?php echo (strtolower($ipayNisDetails['app_type']) == 'vap')?sfConfig::get('app_visa_arrival_title'):ucwords($ipayNisDetails['app_type']);?></td>
                        <td><?php echo $ipayNisDetails['id'];?></td>
                        <td><?php echo $ipayNisDetails['ref_no']; ?></td>
              </tr>
              <?php $i++;  endforeach;
              ?>
             </tbody>
            </table>

            </td>
                <td colspan="5" style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); width: 5%; text-align: right; padding-right: 1em;"><?php echo format_amount($moneyOrderAmount, 1, 0, $currencySymbol); ?></td>

                <td colspan="5" style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); width: 5%;" align="right">&nbsp;</td>
            </tr>
            <tr style="background-color: rgb(229, 236, 249);">
                <td rowspan="4" colspan="70" style="width: 70%;"></td>
                <td colspan="20" style="font-weight: bold; font-size: 105%; width: 20%; text-align: right; vertical-align: text-top;">Total:</td>
                <td colspan="5" style="font-weight: bold; font-size: 105%; width: 5%; text-align: right; white-space: nowrap; padding-right: 1em;"><?php echo format_amount($moneyOrderAmount, 1, 0, $currencySymbol); ?></td>
                <td colspan="5" style="font-weight: bold; font-size: 105%; width: 5%;">&nbsp;</td></tr>
        </table>
    </td>
  </tr>
  <!--<tr  style="">
    <td width="25%" ><b>Payment Status</b></td>
    <td width="77%"><b>Item</b></td>
    <td width="23%" align="right"><b>Price</b></td>
  </tr>
  <tr>

    <td ><b>Successful </b></td>
    <td ><b><?php  //echo $rDetails['merchant_name'];?></b> - Payment <?php //echo $rDetails['mdesc']; ?> for customer <?php //echo $rDetails['cardName']; ?>. your application will be processed further on <?php //echo $rDetails['mabbr']; ?> Portal.</td>
    <td align="right" ><?php //echo format_amount($rDetails['amount'],1);?></td>
  </tr>
  <tr style="color:#00345A;background-color:#E7F0F7;font-size:18px;">

    <td >&nbsp;</td>
    <td  align="right" >Total:</td>
    <td align="right"><?php //echo format_amount($rDetails['amount'],1); ?></td>
  </tr>
  -->
  <?php if( ''!= $rDetails['cardType'] || ''!= $rDetails['cardLast'])
        {
  ?>
  <tr class="blTxt">
    <td colspan="3" class="txtBold" align="left"><b>Paid with:</b> <?php echo $rDetails['cardType'];?>&nbsp;<?php echo getNoOfX($rDetails['cardLen'] - 8);?>-<?php  echo $rDetails['cardLast']; ?></td>
  </tr>

 <?php
        }
 ?>
 </table>
<br><br>
<?php echo "You can get your receipt information by " ;?>
<?php  echo link_to1("clicking here ", $rDetails['url']); ?>
<br><br>
<?php echo "For Complete transaction history ";?>
<?php  echo link_to1("click here ", $rDetails['url1']); ?>
<br><br>
<?php 
/** 
 * [WP: 080] => CR: 116
 */
if($visaArrivalFlag){ ?>
<?php
$no_of_months =  (int)sfConfig::get('app_visa_arrival_flight_update_limit')/30;
list($d, $m, $y) = explode('-',$rDetails['paid_date']);
$expiry_date = date('Y-m-d', mktime(0,0,0,$m+$no_of_months,$d-1,$y));
$datetime = date_create($expiry_date);
?>
<strong>Note: Application(s) are valid till <?php echo $no_of_months; ?> <?php echo ($no_of_months > 1)?'months':'month';?>. it will expire automatically after <?php echo date_format($datetime, 'd/F/Y'); ?> .</strong>
<br /><br />
<?php } ?>

<?php echo $signature;?>


