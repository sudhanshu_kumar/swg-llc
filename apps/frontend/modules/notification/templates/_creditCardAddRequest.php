<?php use_helper('EPortal');?>
<b>Hello <?php echo ucwords($name); ?>,</b>

<?php if($action == "add"){ ?>
<br /><br />Thanks for submission of your credit card information to <b>SW Global LLC!<br /><br /></b>

  <?php } else if ($action == "Approved") {?>

<br /><br />Your request is successfully approved.<br /><br />

  <?php }else if($action == "Rejected"){ ?>
<br /><br />Your request has been rejected.<br /><br />
<?php } ?>


<!--<br /><br />You have successfully done payment on SW Global LLC , transfers and other services (published terms of use apply).<br /><br/>
-->

<table style="background-color: rgb(255, 255, 255); border: 1px solid rgb(229, 236, 249);" width="100%" border="0" cellpadding="4" cellspacing="0">
  <?php if ($action == "Rejected" || $action == "Approved") {?>
  <tr>
    <td>
      <table style="background-color: rgb(229, 236, 249);" width="100%" border="0" cellpadding="2" cellspacing="0"><tbody>
          <?php if($action == "Rejected"){ ?>
          <tr><td>Reason</td></tr>
          <?php } else { ?>
          <tr><td>Comments</td></tr>
          <?php } ?>

          <!-- <tr>
  <td style="font-size: 83%;">&nbsp;Order date: <b><?php // echo $rDetails['paid_date'];?></b><br>&nbsp;SW Global LLC order number: <b><?php // echo $orderNumber;;?></b></td>
  <td style="padding-right: 15px;" nowrap="nowrap" align="right"></td></tr>-->
      </tbody></table>
    </td>
  </tr>
  <tr><td><?= nl2br($app_details->getComments()) ; ?></td></tr>
  <?php }?>
  <tr>
    <td>
      <table style="background-color: rgb(229, 236, 249);" width="100%" border="0" cellpadding="2" cellspacing="0"><tbody>

          <tr><td>Your submitted details are mentioned below</td></tr>

          <!-- <tr>
<td style="font-size: 83%;">&nbsp;Order date: <b><?php // echo $rDetails['paid_date'];?></b><br>&nbsp;SW Global LLC order number: <b><?php // echo $orderNumber;;?></b></td>
          <td style="padding-right: 15px;" nowrap="nowrap" align="right"></td></tr>-->
      </tbody></table>
    </td>
  </tr>
  <!-- <tr style="color:#00345A;background-color:#E7F0F7;">
<td  colspan="4">Order Date: <b><?php  //echo $rDetails['paid_date'];?> </b></td>
</tr>
<tr style="color:#00345A;background-color:#E7F0F7;">
<td  colspan="4">SW Global LLC order number:  <b><?php  //echo $orderNumber;;?></b> </td>
</tr>
  -->
  <tr>
    <td>
      <table style="background-color: rgb(255, 255, 255); border: 1px solid rgb(229, 236, 249);" width="100%" border="0" cellpadding="4" cellspacing="0">
        <tr style="margin-right: 15px; margin-left: 5px;" valign="top">
          <td colspan="10" style="font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); font-weight: bold; color: black; width: 15%; text-align: left; padding-right: 1em;" nowrap="nowrap">&nbsp;<b>Name:</b>&nbsp;</td>
          <td colspan="50" style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); width: 15%;" align="left">
          <?php echo $app_details->getFirstName()."  ".$app_details->getLastName();?></td>
        </tr>



        <tr style="margin-right: 15px; margin-left: 5px;" valign="top">
          <td colspan="10" style="font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); font-weight: bold; color: black; width: 15%; text-align: left; padding-right: 1em;" nowrap="nowrap">&nbsp;<b>Name on Card:</b>&nbsp;</td>
          <td colspan="50" style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); width: 15%;" align="left">
          <?php echo $app_details->getCardHolder();?></td>
        </tr>

<?php


$cardLen = $app_details->getCardLen();
$middlenum = '';
$cardDisplay = '';
if($cardLen == 13){
  $middlenum = "-XXXXX-";
}else if($cardLen == 16){
  $middlenum = "-XXXX-XXXX-";
}else if($cardLen == 15){
  $middlenum = "-XXXXXXX-";
}
$cardDisplay = $app_details->getCardFirst().$middlenum.$app_details->getCardLast();
?>
        <tr style="margin-right: 15px; margin-left: 5px;" valign="top">
          <td colspan="10" style="font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); font-weight: bold; color: black; width: 15%; text-align: left; padding-right: 1em;" nowrap="nowrap">&nbsp;<b>Credit Card Number:</b>&nbsp;</td>
          <td colspan="50" style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); width: 15%;" align="left">
          <?php echo $cardDisplay;?></td>
        </tr>
        <?php
        switch ($app_details->getCardType()){
          case "V" :
            $cardtype = "Visa Card";
            break;
          case "A":
            $cardtype = "American Express";
            break;

          case "M":
            $cardtype = "Master Card";
            break;
          default :
            $cardtype = "-- Not Set --";


          }


          ?>
        <tr style="margin-right: 15px; margin-left: 5px;" valign="top">
          <td colspan="10" style="font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); font-weight: bold; color: black; width: 15%; text-align: left; padding-right: 1em;" nowrap="nowrap">&nbsp;<b>Card Type:</b>&nbsp;</td>
          <td colspan="50" style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); width: 15%;" align="left">
          <?php echo $cardtype;?></td>
        </tr>

  <?php $month = array('January','February','March','April','May','June','July','August','September','October','November','December') ; ?>

        <tr style="margin-right: 15px; margin-left: 5px;" valign="top">
          <td colspan="10" style="font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); font-weight: bold; color: black; width: 15%; text-align: left; padding-right: 1em;" nowrap="nowrap">&nbsp;<b>Card Expiry:</b>&nbsp;</td>
          <td colspan="50" style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); width: 15%;" align="left">
          <?php echo $month[$app_details->getExpiryMonth()-1]." ".$app_details->getExpiryYear(); ?></td>
        </tr>

        <tr style="margin-right: 15px; margin-left: 5px;" valign="top">
        <td colspan="10" style="font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); font-weight: bold; color: black; width: 15%; text-align: left; padding-right: 1em;" nowrap="nowrap">&nbsp;<b>Email:</b>&nbsp;</td>
        <td colspan="50" style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); width: 15%;" align="left">
        <?php echo $app_details->getEmail(); ?></td>

        <tr style="margin-right: 15px; margin-left: 5px;" valign="top">
        <td colspan="10" style="font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); font-weight: bold; color: black; width: 15%; text-align: left; padding-right: 1em;" nowrap="nowrap">&nbsp;<b>Phone Number:</b>&nbsp;</td>
        <td colspan="50" style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); width: 15%;" align="left">
        <?php echo $app_details->getPhone(); ?></td>

        <tr style="margin-right: 15px; margin-left: 5px;" valign="top">
          <td colspan="10" style="font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); font-weight: bold; color: black; width: 15%; text-align: left; padding-right: 1em;" nowrap="nowrap">&nbsp;<b>Address:</b>&nbsp;</td>
          <td colspan="50" style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); width: 15%;" align="left">
          <?php echo nl2br($app_details->getAddress1()); ?></td>
        </tr>

        <tr style="margin-right: 15px; margin-left: 5px;" valign="top">
          <td colspan="10" style="font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); font-weight: bold; color: black; width: 15%; text-align: left; padding-right: 1em;" nowrap="nowrap">&nbsp;<b>Town:</b>&nbsp;</td>
          <td colspan="50" style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); width: 15%;" align="left">
          <?php echo nl2br($app_details->getTown()); ?></td>
        </tr>

        <tr style="margin-right: 15px; margin-left: 5px;" valign="top">
          <td colspan="10" style="font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); font-weight: bold; color: black; width: 15%; text-align: left; padding-right: 1em;" nowrap="nowrap">&nbsp;<b>Country:</b>&nbsp;</td>
          <td colspan="50" style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); width: 15%;" align="left">
          <?php echo $app_details->getCountryName(); ?></td>
        </tr>
        <tr style="margin-right: 15px; margin-left: 5px;" valign="top">
          <td colspan="10" style="font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); font-weight: bold; color: black; width: 15%; text-align: left; padding-right: 1em;" nowrap="nowrap">&nbsp;<b>Do you want to do multiple transactions?</b>&nbsp;</td>
          <td colspan="50" style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); width: 15%;" align="left">
          <?php echo $app_details->getTransactionType(); ?></td>
        </tr>
        <?php if($app_details->getTransactionType() == 'Yes'){?>
        <tr style="margin-right: 15px; margin-left: 5px;" valign="top">
          <td colspan="10" style="font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); font-weight: bold; color: black; width: 15%; text-align: left; padding-right: 1em;" nowrap="nowrap">&nbsp;<b>Reason:</b>&nbsp;</td>
          <td colspan="50" style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); width: 15%;" align="left">
          <?php echo $app_details->getReason(); ?></td>
        </tr>
        <?php }?>
      <?php if ($action == "Approved") {
        if ($cart_capacity != "") {?>
        <tr style="margin-right: 15px; margin-left: 5px;" valign="top">
          <td colspan="10" style="font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); font-weight: bold; color: black; width: 15%; text-align: left; padding-right: 1em;" nowrap="nowrap">&nbsp;<b>Cart Capacity:</b>&nbsp;</td>
          <td colspan="50" style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); width: 15%;" align="left">
          <?php echo $cart_capacity; ?></td>
        </tr>

      <?php } if ($cart_amount != "") {?>
        <tr style="margin-right: 15px; margin-left: 5px;" valign="top">
          <td colspan="10" style="font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); font-weight: bold; color: black; width: 15%; text-align: left; padding-right: 1em;" nowrap="nowrap">&nbsp;<b>Cart Amount capacity:</b>&nbsp;</td>
          <td colspan="50" style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); width: 15%;" align="left">
          <?php echo $cart_amount; ?></td>
        </tr>

      <?php } if ($transaction != "") {?>
        <tr style="margin-right: 15px; margin-left: 5px;" valign="top">
          <td colspan="10" style="font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); font-weight: bold; color: black; width: 15%; text-align: left; padding-right: 1em;" nowrap="nowrap">&nbsp;<b>Allowed Number of Transaction(s):</b>&nbsp;</td>
          <td colspan="50" style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); width: 15%;" align="left">
          <?php echo $transaction; ?></td>
        </tr>

      <?php }  }?>

        <!--<tr style="margin-right: 15px; margin-left: 5px;" valign="top">
  <td colspan="10" style="font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); font-weight: bold; color: black; width: 15%; text-align: left; padding-right: 1em;" nowrap="nowrap">&nbsp;<b>Processing Country:</b>&nbsp;</td>
  <td colspan="50" style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); width: 15%;" align="left">
  <?php //echo $app_details->getProcessingCountryId();?></td>
  </tr>-->



      </table>
    </td>
  </tr>
</table>
<br><br>
<?php if($action == "add"){ ?>
You will receive an email upon approval/rejection of your application.
<?php } ?>

<!-- <br>
  Please look out for our newsletter in your registered email with more information about ways to utilize your account.<br /><br />"SW Global LLC.....KEEPING IT SIMPLE" -->
<br /><br />

  <?php echo $signature;?>


