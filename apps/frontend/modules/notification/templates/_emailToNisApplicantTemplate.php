<?php use_helper('EPortal');?>
<b>Dear <?php echo ucwords($name); ?>,</b>
<br /><br />As per our records, you have applied for a Nigerian <?php echo "<b>".ucfirst($appType)."</b> on ".date("F d, Y",strtotime($appliedDate));?>.<br /><br />

<table style="background-color: rgb(255, 255, 255); border: 1px solid rgb(229, 236, 249);" width="100%" border="0" cellpadding="4" cellspacing="0">
  <tr>
    <td>
      <table style="background-color: rgb(229, 236, 249);" width="100%" border="0" cellpadding="2" cellspacing="0">
        <tbody>
          <tr><td>The details of your application are given as under.</td></tr>
        </tbody>
      </table>
    </td>
  </tr>
  <tr>
    <td>
      <table style="background-color: rgb(255, 255, 255); border: 1px solid rgb(229, 236, 249);" width="100%" border="0" cellpadding="4" cellspacing="0">
        <tr style="margin-right: 15px; margin-left: 5px;" valign="top">
          <td colspan="10" style="font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); font-weight: bold; color: black; width: 15%; text-align: left; padding-right: 1em;" nowrap="nowrap">&nbsp;<b>Application Id:</b>&nbsp;</td>
          <td colspan="50" style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); width: 15%;" align="left">
          <?php echo $appId; ?></td>
        </tr>
        <tr style="margin-right: 15px; margin-left: 5px;" valign="top">
          <td colspan="10" style="font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); font-weight: bold; color: black; width: 15%; text-align: left; padding-right: 1em;" nowrap="nowrap">&nbsp;<b>Reference Number:</b>&nbsp;</td>
          <td colspan="50" style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); width: 15%;" align="left">
          <?php echo $refrenceNumber; ?></td>
        </tr>
        <tr style="margin-right: 15px; margin-left: 5px;" valign="top">
          <td colspan="10" style="font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); font-weight: bold; color: black; width: 15%; text-align: left; padding-right: 1em;" nowrap="nowrap">&nbsp;<b>Date of birth of the applicant:</b>&nbsp;</td>
          <td colspan="50" style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); width: 15%;" align="left">
          <?php echo date("F d, Y",strtotime($dob)); ?></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<br><br>
This is to bring into your kind notice that a <?php echo "<b>".$refundType."</b>";?> has been requested on <?php echo date("F d, Y",strtotime($refundDate));?> for the amount paid for the above application.
Please be adviced that above mentioned NIS <?php echo "<b>".ucfirst($appType)."</b>"; ?> application has been cancelled. If a <?php echo "<b>".ucfirst($appType)."</b>"; ?> has already been provided to you, it will be REVOKED and any attempt to use such will be subject to prosecution.

<br /><br />
In case this <?php echo "<b>".$refundType."</b>";?> is not initiated by you, please contact your <?php echo "<b>".ucfirst($appType)."</b>";?> agent who applied on your behalf and ask for a refund of your application.


<br /><br /><br /><br />

<?php echo $signature;?>
