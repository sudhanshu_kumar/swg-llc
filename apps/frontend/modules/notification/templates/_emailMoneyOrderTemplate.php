<?php
if($moneyOrderDetails['currency'] == '1'){ 
    $moneyOrderAmount = $moneyOrderDetails['amount'];    
}else{
    $moneyOrderAmount = $moneyOrderDetails['convert_amount'];
}
$currencySymbol = CurrencyManager::currencySymbolByCurrencyId($moneyOrderDetails['currency']);
?>
<?php use_helper('EPortal');?>
<b>Dear <?php echo ucwords($name); ?>,</b>
<br /><br />Your Money Order is successfully associated with Tracking number(s).<br /><br />

<table style="background-color: rgb(255, 255, 255); border: 1px solid rgb(229, 236, 249);" width="100%" border="0" cellpadding="4" cellspacing="0">
  <tr>
    <td>
      <table style="background-color: rgb(229, 236, 249);" width="100%" border="0" cellpadding="2" cellspacing="0">
        <tbody>
          <tr><td>Money Order Information</td></tr>
        </tbody>
      </table>
    </td>
  </tr>
  <tr>
    <td>
      <table style="background-color: rgb(255, 255, 255); border: 1px solid rgb(229, 236, 249);" width="100%" border="0" cellpadding="4" cellspacing="0">
        <tr style="margin-right: 15px; margin-left: 5px;" valign="top">
          <td colspan="50" style="font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); font-weight: bold; color: black; width: 50%; text-align: left; padding-right: 1em;" nowrap="nowrap">&nbsp;<b>Money Order Number:</b>&nbsp;</td>
          <td colspan="50" style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); width: 50%;" align="left">
          <?php echo $moneyOrderDetails['moneyorder_number']; ?></td>
        </tr>
        <tr style="margin-right: 15px; margin-left: 5px;" valign="top">
          <td colspan="50" style="font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); font-weight: bold; color: black; width: 50%; text-align: left; padding-right: 1em;" nowrap="nowrap">&nbsp;<b>Money Order Amount:</b>&nbsp;</td>
          <td colspan="50" style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); width: 50%;" align="left">
          <?php echo $moneyOrderAmount; ?></td>
        </tr>
        <tr style="margin-right: 15px; margin-left: 5px;" valign="top">
          <td colspan="50" style="font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); font-weight: bold; color: black; width: 50%; text-align: left; padding-right: 1em;" nowrap="nowrap">&nbsp;<b>Money Order Date:</b>&nbsp;</td>
          <td colspan="50" style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); width: 50%;" align="left">
          <?php echo $moneyOrderDetails['moneyorder_date']; ?></td>
        </tr>   
      </table>
    </td>
  </tr>
  <tr>
    <td>
      <table style="background-color: rgb(229, 236, 249);" width="100%" border="0" cellpadding="2" cellspacing="0">
        <tbody>
          <tr><td>Order Numbers associated with money order</td></tr>
        </tbody>
      </table>
     </td>
   </tr>
   <tr>
    <td>
     <table style="background-color: rgb(255, 255, 255); border: 1px solid rgb(229, 236, 249);" width="100%" border="0" cellpadding="4" cellspacing="0">
      <tr style="margin-right: 15px; margin-left: 5px;" valign="top">
        
        <td colspan="40" style="font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); font-weight: bold; color: black; width: 40%; text-align: left; padding-right: 1em;" nowrap="nowrap">Tracking Number</td>
        <td colspan="40" style="font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); font-weight: bold; color: black; width: 40%; text-align: left; padding-right: 1em;" nowrap="nowrap">&nbsp;<b>Order Number</b>&nbsp;</td>
        <td colspan="40" style="font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); font-weight: bold; color: black; width: 40%; text-align: left; padding-right: 1em;" nowrap="nowrap">Application Details</td>
        <td colspan="20" style="font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); font-weight: bold; color: black; width: 20%; text-align: right; padding-right: 1em;" nowrap="nowrap">Amount (<?php echo html_entity_decode($currencySymbol); ?>)</td>
    </tr>
    <?php  foreach ($trackingDetails as $records){ 
        
         
        ?>
      <tr style="margin-right: 15px; margin-left: 5px;" valign="top">
        <td colspan="40" style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); width: 40%;" align="left">&nbsp;<?= $records['tracking_number']?>&nbsp;</td>
        <td colspan="40" style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); width: 40%;" align="left"><?= $records['order_number']?></td>
        <td colspan="40" style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); width: 40%;" align="left">
        <table>
            <tr bgcolor="#eeeeee">
                <td colspan="40" style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); width: 40%;" align="left"><span class="txtBold">Application Id</span></td>
                <td colspan="40" style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); width: 40%;" align="left"><span class="txtBold">Application Type</span></td>
                <td colspan="40" style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); width: 40%;" align="left"><span class="txtBold">Reference Number</span></td>
            </tr>
            <?php foreach($records['app_detail'] as $detail){
            
          // echo '<pre>';print_r($detail);die;
            ?>

            <tr>
                <td colspan="40" style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); width: 40%;" align="left"><a href="http://portal.immigration.gov.ng/visa/OnlineQueryStatus" target="_NEW"><?php echo $detail['app_id']?></a></td>
                <td colspan="40" style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); width: 40%;" align="left"><?php if($detail['app_type']=='vap') { echo sfConfig::get('app_visa_arrival_title');} else { echo $detail['app_type']; }?></td>
                <td colspan="40" style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); width: 40%;" align="left"><?php echo $detail['ref_no']?></td>
            </tr>
            <?php } ?>
        </table>
            
        </td>
        <td colspan="20" style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); width: 20%;" align="right"><?= html_entity_decode($currencySymbol).$records['cart_amount']?></td>
    </tr>
    <?php } ?>
    </table>
   </td>
 </tr>
</table>
<br><br>
You will be updated, once we receive your Money order.


<br /><br />
For accessing your application status, click on application id above or go to "<a href="http://portal.immigration.gov.ng/visa/OnlineQueryStatus" target="_NEW">http://portal.immigration.gov.ng/visa/OnlineQueryStatus</a>".
<br /><br />

<br /><br /><br />

<?php echo $signature;?>
