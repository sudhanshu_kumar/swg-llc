
<!--This template is being displayed after money order serial is being associated with tracking number(s) -->
<div class="clear"></div>
<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">
<?php
use_helper('Form');
include_partial('global/innerHeading',array('heading'=>'Money Order Details'));
?>
        <?php
        $sf = sfContext::getInstance()->getUser();
        if($sf->hasFlash('notice')){ ?>
        <div id="flash_notice" class="alertBox" >
          <?php
          echo nl2br($sf->getFlash('notice'));
          ?>
      </div>
      <br/>
        <?php }?>
          <?php
          $sf = sfContext::getInstance()->getUser();
          if($sf->hasFlash('error')){ ?>
          <div id="flash_error" class="alertBox" >
            <?php
            echo nl2br($sf->getFlash('error'));
            ?>
        </div>
        <br/>
          <?php }?>
        <div class="clear"></div>
        <div class="tmz-spacer"></div>
           <?php include_partial('moneyOrderDetails', array('moneyOrderDetails' => $moneyOrderDetails, 'applicationDetails' => $applicationDetails, 'record' => $record)); ?>
        <table width="100%">
      <tr>
        <td> An email has been sent to you confirming, your order number & tracking number is associated with a money order serial number.<br/>
          <br/>
          <div class="prominent">Please mail the filled in Money Order via a traceable courier service (such as Fedex, UPS and DHL) to following address:<br/>
            <br/>
      <strong>SW Global LLC<br/>
      50 Albany Turnpike, Suite 5032<br/>
            Canton, CT 06019<br/>
            </strong><br/>
Once we receive your money order, we’ll confirm the receipt via another email to you stating that your payment has been received and you can proceed to submit your Visa/Passport application.</div>
          <div class="clear"></div>
          <br/>
Once we confirm the receipt of your payment in an email, you can proceed to check further details (e.g. interview date if applicable) of your application(s) on Nigeria Immigration Services (NIS) Portal by following these steps:<br/>
          <ol class="prominent_listing">
            <li><a href="http://portal.immigration.gov.ng/visa/OnlineQueryStatus" target="new">Click HERE to open "Query Your Application Status" page on NIS</a>. Alternatively you can go to NIS home page (<a href="http://portal.immigration.gov.ng/" target="new">http://portal.immigration.gov.ng/</a>) and click "Query Your Application Status" link there.</li>
<li>Using your APPLICATION ID and REFERENCE NUMBER (sent to you via email), please
      pull up your NIS ACKNOWLEDGMENT SLIP and PAYMENT RECEIPT.</li>
	  <li>if you have made ONE payment for MULTIPLE APPLICATIONS using our CART, you must
      enter each APPLICATION ID/REFERENCE NUMBER at <a href="http://portal.immigration.gov.ng/visa/OnlineQueryStatus" target="new">"Query Your Application Status"</a> page to pull up the ACKNOWLEDGMENT SLIP and PAYMENT RECEIPT
      for each application.</li>
	  <li>PRINT your ACKNOWLEDGMENT SLIP and PAYMENT RECEIPT for each application. You must present the
              ACKNOWLEDGMENT SLIP and PAYMENT RECEIPT to the Embassy/Consulate with other required documents on the interview date assigned to you. </li>
          </ol></td>
      </tr>
    </table>
	<div class="clear">&nbsp;</div>
    <div align="center">
      <input type="button" class="normalbutton noPrint" onclick="javaScript:window.print();" name="Print" value="Print">
        </div>
      </div>
      </div>
<div class="content_wrapper_bottom"></div>
