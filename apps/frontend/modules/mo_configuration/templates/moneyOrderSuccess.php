<!--This templates has interface from which user enters details for Money Order and associate it with a tracking number. -->
<script>

  function validate_form(fmobj){   
    
    var checkBoxCount = $('input:checkbox[name = "chk_fee[]"]:checked').length;
   
    if(checkBoxCount > 0)
    {
      
      var illegalChars = /^[0-9 \.]*$/;      
      
      var amtToPaid = $("#amt_to_paid").val();      
      var moneyorderAmt = $.trim($("#moneyorder_amount").val());
      
      var errFlag = false;
      if(moneyorderAmt == '')
      {
        $("#amount_error").html("Please enter money order amount.");
        errFlag = true;
      }
      //adding only numeric mo amount bug-id 29312 & 33163
      else if(!illegalChars.test(moneyorderAmt)){
        $("#amount_error").html("Money Order amount should be numeric.");
        errFlag = true;
      }
      else if(parseFloat(amtToPaid) != parseFloat(moneyorderAmt)){
        $("#amount_error").html("Amount to be paid and money order amount should be same.");
        errFlag = true;
      }
      else
      {
        $("#amount_error").html("");
      }
      
      var moneyorderNumber = $.trim($("#moneyorder_moneyorder_number").val());
    
      if(moneyorderNumber == '')
      {
        $("#moneyorder_number_error").html("Please enter money order serial number.");
        errFlag = true;
      }
      else
      {
          $("#moneyorder_number_error").html("");
      }
      var remoneyorderNumber = $.trim($("#moneyorder_re_moneyorder_number").val());
      if(remoneyorderNumber == '')
      {
        $("#re_moneyorder_number_error").html("Please enter confirm money order serial number.");
        errFlag = true;
      }else{ if(moneyorderNumber != ''){
          if(remoneyorderNumber != moneyorderNumber)
           {
            $("#re_moneyorder_number_error").html("Money order serial number and Confirm money order serial number do not match.");
            errFlag = true;
           }
           else
           {
            $("#re_moneyorder_number_error").html("");
           }
      }
      }

      if($("#moneyorder_moneyorder_date_day").val() == '')
      {
        $("#moneyorder_date_error").html("Please enter Money Order Issuing Date.");
        errFlag = true;
      }

      if($("#moneyorder_moneyorder_date_month").val() =='')
      {
        $("#moneyorder_date_error").html("Please enter Money Order Issuing Date.");
        errFlag = true;
      }

      if($("#moneyorder_moneyorder_date_year").val() =='')
      {
        $("#moneyorder_date_error").html("Please enter Money Order Issuing Date.");
        errFlag = true;
      }

      if($("#moneyorder_moneyorder_date_year").val() == '' || $("#moneyorder_moneyorder_date_month").val() =='' || $("#moneyorder_moneyorder_date_day").val() == '')
      {
        $("#moneyorder_date_error").html("Please enter Money Order Issuing Date.");
        errFlag = true;
      }
      else
      {
          $("#moneyorder_date_error").html("");
      }

    if($('#moneyorder_courier_flag').attr('checked') == true) {     
       if($('#moneyorder_courier_service').val() == 0){
         $('#courier_service_error').html('Please select Courier Company.');
          errFlag = true;
        }
        else
        {
            $('#courier_service_error').html('');
        }
       if($('#moneyorder_courier_service').val() == 4){
         if($.trim(($('#otherCourerService').val())) == ''){
              $('#other_courier_error').html('Please Enter Courier Company.');
              errFlag = true;
         }
         else
         {
             $('#other_courier_error').html('');
         }
     }
     if($('#waybillCheckbox').attr('checked') == false) {
         if($.trim($('#moneyorder_waybill_trackingno').val()) == ''){
             $('#waybill_error').html('Please Enter Waybill Tracking Number.');
             errFlag = true;
         }
         else
        {
            $('#waybill_error').html('');
        }
     }else{
         $('#waybill_error').html('');
     }
  }else{
      $('#courier_service_error').html('');
      $('#other_courier_error').html('');
      $('#waybill_error').html('');
  }
  if($.trim(($('#moneyorder_address').val())) == ''){
      $('#address_error').html('Please Enter Contact Information.');
      errFlag = true;
  }
  else
      {
          $('#address_error').html('');
      }
  if($.trim($('#moneyorder_phone').val()) == ''){
      $('#phone_error').html('Please Enter Phone Number.');
      errFlag = true;
  }
  else
      {
          $('#phone_error').html('');
      }

      //Round up decimal amount in amount validation
      
      if(errFlag){
        return false;
      }else {
          if(confirm('Your money order serial number is "'+moneyorderNumber+'". Do you want to continue?')){
            return true;
          }else{
            return false;
          }
      }
    }
    else
    {
      var checkBoxCount = $('input:checkbox[name = "chk_fee[]"]').length;
      if(checkBoxCount > 2){
        var amt_to_paid_error = 'Please select at least one tracking number.';
      }else{
        var amt_to_paid_error = 'Please select tracking number.';
      }
      $('#amt_to_paid_error').html(amt_to_paid_error);
      $('#amt_to_paid_error').focus();
      return false;
    }
   
  }



  function uniqueMoneyOrderNumber(moneyorder_number)
  {

    var url = "<?php echo url_for("mo_configuration/uniqueOrderNumber"); ?>";

    $.get(url, { moneyorder_number: moneyorder_number},
    function(data){
      alert(data);


    });
  }
  function checkAll(fmobj, chkAll)
  {
    var amount = 0;
    $('#amt_to_paid_error').html('');
    for (var i=0;i<fmobj.elements.length;i++)
    {
      var e = fmobj.elements[i];
      if ((e.name == chkAll) && (e.type=='checkbox') && (!e.disabled))
      {
        e.checked = fmobj.chk_all.checked;
        if(fmobj.chk_all.checked)
        {
          amount = parseInt(amount)+parseInt($("#"+e.id+"amt").html());
        }
        $("#amt_to_paid").val(amount);
        $("#disp_amt").html(amount+".00");
      }
    }
  }

  function UncheckMain(fmobj, objChkAll, chkElement,obj,amt)
  {
    var boolCheck = true;
    $('#amt_to_paid_error').html('');
    var amount = $("#amt_to_paid").val();
    if(amount ==''){
      amount = 0;
    }
    if($("#"+obj.id).is(':checked')){
      amount = parseInt(amount)+parseInt(amt);
      $("#amt_to_paid").val(amount);
      $("#disp_amt").html(amount+".00");

    }else{
      amount = parseInt(amount)-parseInt(amt);
      $("#amt_to_paid").val(amount);
      $("#disp_amt").html(amount+".00");

    }
    if(objChkAll.checked==false)
    {
      for(var i=0;i<fmobj.elements.length;i++)
      {
        if(fmobj.elements[i].name.indexOf(""+chkElement+"")!="-1" && fmobj.elements[i].checked==false)
        {
          boolCheck = false;
          break;
        }
      }
      if(boolCheck==true)
        objChkAll.checked=true;
    }
    else
    {
      objChkAll.checked=false;
    }
  }
</script>

<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">
    <?php
use_helper('Form');
use_helper('Pagination');
include_partial('global/innerHeading',array('heading'=>'Associate Tracking Number'));
?>
    <!--<div class="global_content4 clearfix">-->
<?php echo form_tag('mo_configuration/saveMoneyorder',array('name'=>'order_search_form','class'=>'', 'method'=>'post','id'=>'searchApplicationForm' ,'onsubmit'=>'return validate_form(\''. count($pendingtrackingNumbers).'\')','name'=>'searchApplicationForm')) ?>
<?php
if(count($pendingtrackingNumbers)>0)
{ ?>
    <div class="clear"></div>
 <div id="flash_notice" class="highlight yellow">
            <!-- DUE TO THE HIGH INCIDENCE OF USE OF STOLEN CREDIT CARDS ON OUR PLATFORM, ALL PAYMENTS FOR APPLICATIONS FOR VISA AND PASSPORT SERVICES SHALL BE MADE ONLY WITH UNITED STATES POSTAL SERVICE MONEY ORDERS EFFECTIVE FROM MARCH 2, 2011.  CREDIT/DEBIT CARD PAYMENTS ARE NO LONGER ACCEPTABLE. WE REGRET ANY INCONVENIENCE THIS MAY CAUSE OUR CUSTOMERS. -->
      <?php echo sfConfig::get('app_mo_based_content');?> </div>
  <?php } ?>
  <div class="wrapForm2">
    <?php
    $sf = sfContext::getInstance()->getUser();
    if($sf->hasFlash('notice')){ ?>
    <div id="flash_notice" class="alertBox" >
      <?php
      echo nl2br($sf->getFlash('notice'));
      ?>
      </div>
      <br/>
    <?php }?>
    <div class="wrapForm2">
      <?php
      $sf = sfContext::getInstance()->getUser();
      if($sf->hasFlash('error')){ ?>
      <div id="flash_error" class="alertBox" >
        <?php
        echo nl2br($sf->getFlash('error'));
        ?>
        </div>
        <br/>
      <?php }?>
    <?php
    $colspan = '4';
    if($isValid) { ?>
      <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
        <tr>
          <td class="blbar" colspan="<?php echo $colspan?>" align="left">Please select Tracking Number(s)</td>
        </tr>
        <tr>
            <td width="12%" valign="top"><span class="txtBold" id="maincheck">
              <input type="checkbox" name="chk_all" id="chk_all" onclick="checkAll(document.searchApplicationForm,'chk_fee[]');" >
              &nbsp;(Check All)</span></td>
          <td width="18%"><span class="txtBold">Tracking Number</span></td>
          <td width="60%" align="center"><span class="txtBold">Application Detail</span></td>
          <td width="10%" align="right"><span class="txtBold">Amount($)</span></td>
        </tr>
        <tbody>
          <?php
          $i=1;
          foreach ($pendingtrackingNumbers as $result): //echo '<pre>';print_r($result);echo '</pre>';
          ?>
          <tr>
              <td valign="top"><span class="txtBold">
                <input type="checkbox" name="chk_fee[]" id="<?= "chk_fee".$i; ?>" value="<?= $result['track']['id']."_".$result['track']['cart_amount'];?>" OnClick="UncheckMain(document.searchApplicationForm,document.searchApplicationForm.chk_all,'chk_fee',<?= "chk_fee".$i; ?>,'<?= $result['track']['cart_amount']?>');" >
                </span> </td>
              <td valign="top"><span><?php echo $result['track']['tracking_number'];?></span> </td>
              <td valign="top"><table cellspacing="0" cellpadding="0" border="1" width="100%" >
                <tr style="background-color: rgb(229, 236, 249);">
                  <td width="15%"><span class="txtBold">App. Id</span></td>
                  <td width="15%"><span class="txtBold">App. Type</span></td>
                  <td width="30%"><span class="txtBold">Reference Number</span></td>
                  <td width="40%"><span class="txtBold">Applicant Name</span></td>
                </tr>
                <?php
                $appDetailsCount = (isset($result['app_details']))?count($result['app_details']):0;
                if($appDetailsCount > 0) {
                  for($j=0;$j<$appDetailsCount;$j++){ ?>
                <tr>
                  <td width="15%"><?php echo $result['app_details'][$j]['id']?></td>
                  <td width="15%"><?php echo ucfirst($result['app_details'][$j]['app_type'])?></td>
                  <td width="15%"><?php echo $result['app_details'][$j]['ref_no']?></td>
                  <td width="15%"><?php echo $result['app_details'][$j]['name']?></td>
                </tr>
                <?php } } else {  ?>
                <tr>
                  <td colspan="4" align="center" >No application detail found.</td>
                </tr>
                <?php } ?>
                </table></td>
            <td align="right" id="<?= "chk_fee".$i."amt"; ?>"><?php echo $result['track']['cart_amount'];?></td>
          </tr>
            <?php
            $i++;
            endforeach; ?>
          <tr>
            <td></td>
            <td colspan="2" align="right"><b>Amount to be Paid($)</b></td>
              <td align="right"><div id="amt_to_paid_div"><span id="disp_amt">0.00</span></div>
                <input type="hidden" name="amt_to_paid" id="amt_to_paid"  class="txt-input" readonly="readonly"></td>
            </tr>
            <tr>
              <td width="100%" colspan="3"><div class="red" id="amt_to_paid_error"></div></td>
            </tr>
        </tbody>
      </table>
      <table width="100%">
        <tr>
          <td class="blbar" colspan="<?php echo $colspan?>" align="left">Enter Money Order Details</td>
        </tr>
        <tr>
            <td width="30%"><?php echo $form['amount']->renderLabel()." ($)"; ?><span class="red">*</span><br>
              <small><i>(Kindly do not enter decimal places or dollar sign inside the box.)</i></small></td>
            <td><?php echo $form['amount']->render(); ?> <br>
              <div class="red" id="amount_error"> <?php echo $form['amount']->renderError(); ?> </div></td>
        </tr>
        <tr>
            <td>Money Order Serial Number<span class="red">*</span><br>
              <small><i>(Kindly use only one Money Order Serial Number at a time.)</i></small></td>
            <td><?php echo $form['moneyorder_number']->render(); ?> <br>
              <div class="red" id="moneyorder_number_error"> <?php echo $form['moneyorder_number']->renderError(); ?> </div></td>
        </tr>
        <tr>
          <td>Confirm Money Order Serial Number<span class="red">*</span></td>
            <td><?php echo $form['re_moneyorder_number']->render(); ?> <br>
              <div class="red" id="re_moneyorder_number_error"> <?php echo $form['re_moneyorder_number']->renderError(); ?> </div></td>
        </tr>
        <tr>
            <td>Money Order Issuing Date<span class="red">*</span><br>
              (dd/mm/yyyy)</td>
            <td><?php echo $form['moneyorder_date']->render(); ?> <br>
              <div class="red" id="moneyorder_date_error"> <?php echo $form['moneyorder_date']->renderError(); ?> </div></td>
        </tr>
        <tr>
        <td><b><span class="red">Are you sending Money Order via Courier Service</span></b>
        <td><?php echo $form['courier_flag']->render(); ?></td>
            <div class="red" > <?php echo $form['courier_flag']->renderError(); ?> </div>
       <!-- <input type ="radio" name = "CourierYes" value="No" id="CourierYes" onclick="hideCourierDetails()"></td> -->
        </tr>       
          <tr id = "courierRow" class="no_display">
          <td><?php echo $form['courier_service']->renderLabel(); ?><span class="red">*</span></td>
          <td id="courierServiceId"><?php echo $form['courier_service']->render(); ?>
              <?php

              

              ?>
            <br>
              <div class="red" id="courier_service_error"> <?php echo $form['courier_service']->renderError(); ?> </div></td>
        </tr>
          <tr id = "otherCourierRow" class="no_display">
          <td>Other Courier Company<span class="red">*</span></td>
          <td id="otherCourierServiceId"><input type="texbox" name="otherCourerService" id ="otherCourerService" class='txt-input' maxlength="100" min_length ="3" value="">
            <br>
              <div class="red" id="other_courier_error"> <?php echo $form['courier_service']->renderError(); ?> </div></td>
        </tr>
          <tr id="nowaybillRow" class="no_display">
            <td>I do not remember/have a Waybill Tracking Number</td>
        <td><input type = "checkbox" name ="waybillCheckbox" id ="waybillCheckbox"></td>
        </tr>
          <tr id ="waybillRow" class="no_display">
            <td><?php echo $form['waybill_trackingno']->renderLabel(); ?><span class="red">*<br/>
              Note : To avoid undue delays Please provide Waybill Tracking Number</span></td>
            <td><?php echo $form['waybill_trackingno']->render(); ?> <br>
              <div class="red" id="waybill_error"> <?php echo $form['waybill_trackingno']->renderError(); ?> </div></td>
        </tr>        
        <tr>
          <td><?php echo $form['address']->renderLabel(); ?><span class="red">*</span></td>
            <td><?php echo $form['address']->render(); ?> <br>
              <div class="red" id="address_error"> <?php echo $form['address']->renderError(); ?> </div></td>
        </tr>
        <tr>
          <td><?php echo $form['phone']->renderLabel(); ?><span class="red">*</span></td>
            <td><?php echo $form['phone']->render(); ?> <br>
              <div class="red" id="phone_error"> <?php echo $form['phone']->renderError(); ?> </div></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
            <td>
                <?php   echo submit_tag('Submit for Payment',array('class' => 'normalbutton')); ?>

              </td>
        </tr>
      </table>
      </form>
      <?php } ?>
    </div>
  </div>
</div>
</div>
<!--</div>-->
<div class="content_wrapper_bottom"></div>
<br>
<?php if(isset($form_val['courier_flag'])){
//    echo "<pre>";print_r($form_val);
   if($form_val['courier_flag'] == 'Yes'){ ?>
        <script>
        $('#moneyorder_courier_flag').attr('checked',true);
        $('#courierRow').show();
        $('#nowaybillRow').show();
        </script>
        <?php if($form_val['courier_service'] != 'UPS' && $form_val['courier_service'] != 'DHL' && $form_val['courier_service'] != 'USPS'){?>
            <script>
                $('#moneyorder_courier_service').val('Others');
                $('#otherCourierRow').show();
                $('#otherCourerService').val('<?php echo $form_val['courier_service'];?>');

            </script>
        <?php  } else { ?>
         <script>
         $('#moneyorder_courier_service').val('<?php echo $form_val['courier_service']; ?>');
         </script>
       <?php }if(isset($form_val['waybill_trackingno']) && $form_val['waybill_trackingno'] != '') {?>
           <script>
           $('#waybillRow').show();
           $('#waybillCheckbox').attr('checked',false);
           </script>
        <?php } else { ?>
        <script>
        $('#waybillRow').hide();
        $('#waybillCheckbox').attr('checked',true);
        $('#waybill_error').html('');
        </script>
   <?php } } } else { ?>
        <script>
            $('#courier_service_error').html('');
            $('#other_courier_error').html('');
            $('#waybill_error').html('');

        </script>
 <?php  } ?>
<script type="text/javascript">

  $(document).ready(function(){
    $('#moneyorder_moneyorder_number').bind("cut copy paste",function(e) {
      e.preventDefault();
    });

    $('#moneyorder_re_moneyorder_number').bind("cut copy paste",function(e) {
      e.preventDefault();
    });

    <?php
    if(isset($form_val['courier_flag']))
    {
        $form_courier_flag = $form_val['courier_flag'];
    } else{
        $form_courier_flag = '';
    }
    ?>
    var formVal = '<?php echo $form_courier_flag; ?>';
   if(formVal == ''){    
    $('#moneyorder_courier_flag').attr('checked',false);
    }else {
        $('#moneyorder_courier_flag').attr('checked',true);
    }
      
   });
   

  function showCourierDetails(){
     if($('#moneyorder_courier_flag').attr('checked') == true) {
        $('#courierRow').show();
        $('#waybillRow').show();
        $('#nowaybillRow').show();
     }else{ 
          $('#courierRow').hide();
          $('#otherCourierRow').hide();
          $('#waybillRow').hide();
          $('#nowaybillRow').hide();
          $('#moneyorder_courier_service').val('');
          $('#waybillCheckbox').val('');
          $('#moneyorder_waybill_trackingno').val('');
          $('#otherCourerService').val('');
          $('#waybillCheckbox').attr('checked',false);

     }
  }
 $('#waybillCheckbox').bind('click',function(){
      if($('#waybillCheckbox').attr('checked') == true) {
          $('#waybillRow').hide();
          $('#moneyorder_waybill_trackingno').val('');
      }else{
      $('#waybillRow').show();
      }
  });

  function otherCourierTexBox(obj){
    var courierName = $("#moneyorder_courier_service [value='"+obj.value+"']").text();      
      if(courierName == 'Others'){
        $('#otherCourierRow').show();          
      }else{
          $('#otherCourierRow').hide(); 
          $('#otherCourerService').val('');
      }
  }
</script>
