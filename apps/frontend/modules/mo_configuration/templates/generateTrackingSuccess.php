<!-- Tracking number is being displayed for the application that is to be paid with MO-->
<?php include_partial('global/innerHeading',array('heading'=>'Tracking Number: '.$tracking_number));?>
<div style="clear:both;"></div>
<?php if($errMsg != '') { ?>
    <div style="background-color:#ffffff;">
 <div id="flash_notice" class="alertBox" >
  <?php
    echo $errMsg;
  ?>
</div>
<div style="height:200px;"></div></div>
<?php } ?>
<div class="wrapForm2">
<?php
if($data)
{
    ?>
    <div style="background-color:#ffffff;">
            <table width="97%" style="background-color:#ffffff; margin:0 0 0 12px;" align="center">
                <tr>
                    <td colspan="2" style="background-color:#f1f1f1;">
                        <b>Your Tracking details is as follows:</b>
                    </td>
                </tr>
                <tr>
                    <td>
                        Tracking Number
                    </td>
                    <td>
                        <?= $trackingDetails['tracking_number']?>
                    </td>
                </tr>
                <tr>
                    <td>
                        Amount
                    </td>
                    <td>
                        <?= "$".$trackingDetails['cart_amount']?>
                    </td>
                </tr>
      </table>
            
             

    </div>

</div>

<div class="global_content4 clearfix">
  <?php
  $action = url_for('mo_configuration/trackingProcess') ;
    /*
     * To change this template, choose Tools | Templates
     * and open the template in the editor.
     */

  ?>


  <form name="genearteForm" id ="genearteForm" action="<?php echo $action; ?>" method="post">
    <input type="hidden" name="request_id" value="<?= $sf_params->get('requestId')?>">
    <p style="text-align: left ! important;" class="body_txtp">
    <?php if($encriptedMoneyorderId == '') { ?>
        <div class="highlight yellow" id="flash_notice"><span style="text-align:left; color:red;"><b style="font-size:13px;">Important!!! Now you have to associate this tracking number with Money order serial number to complete the payment process else your payment will not be activated on the platform and you may encounter undue delays.</b></span></div></p>
    <?php } else { ?>
        <div class="highlight yellow" id="flash_notice"><span style="text-align:left; color:red;"><b style="font-size:13px;">Important!!! You have successfully associate this tracking number with Money order serial number.</b></span></div></p>
    <?php } ?>
    
    <br>
    <table cellspacing="0" cellpadding="0" border="1" width="100%" >              
        <tr style="background-color: rgb(229, 236, 249);">
          <td width="15%"><span class="txtBold">Application Id</span></td>
          <td width="14%"><span class="txtBold">Application Type</span></td>
          <td width="14%"><span class="txtBold">Reference Number</span></td>
          <td width="30%"><span class="txtBold">Applicant Name</span></td>          
          <td width="12%"><span class="txtBold">Application Status</span></td>
          
        </tr>
        <?php
        $appDetailsCount = count($appDetailsArray);

        if($appDetailsCount > 0){
          for($j=0;$j<$appDetailsCount;$j++){ ?>
        <tr>
          <td><?php echo $appDetailsArray[$j]['id']?></td>
          <td><?php echo ucfirst($appDetailsArray[$j]['app_type'])?></td>
          <td><?php echo $appDetailsArray[$j]['ref_no']?></td>
          <td><?php echo $appDetailsArray[$j]['name']?></td>          
          <td><?php echo $appDetailsArray[$j]['status']?></td>
        </tr>
        <?php } ?>

        <?php } else {  ?>
        <tr>
          <td colspan="<?php echo $colspan;?>" align="center" >No application details found.</td>
        </tr>
        <?php } ?>
      </table>

    <table width="100%" cellspacing="0" cellpadding="0">
        <tr><td align="center" style="text-align:center;"><center><p class="page_subheader">Your Tracking Number is <?php echo $trackingDetails['tracking_number']; ?></p></center></td></tr>
        <?php if($encriptedMoneyorderId == '') { ?>
        <tr bgcolor="#eeeeee">
            <td>
                <?php $totalCheckBox = 3; ?>
                <table width="100%" cellspacing="0" cellpadding="0" >
                    <tr>
                        <td>
                            Please ensure to have below information before associating money order serial number with tracking number.
                            <div id="checkBoxError" class="red"></div>
                        </td>
                    </tr>
                    <tr>
                        <td><input type="checkbox" name ="moNumber" id="moNumberId" value="1">&nbsp;Money Order Number<span class="red">*</span></td>
                    </tr>
                    <tr>
                        <td><input type="checkbox" name="courierCompany" value="1">&nbsp;Mode of mailing to SW Global LLC<span class="red">*</span></td>
                    </tr>
                    <tr>
                        <td><input type="checkbox">&nbsp;Waybill Tracking Number</td>
                    </tr>
                    <tr>
                        <td><span class="red">*</span>&nbsp;Note: All above mandatory information should be available</td>
                    </tr>
                    
                </table>
            </td>
        </tr>
        <?php } ?>
        <tr>
            <td align="center">
                <?php if($encriptedMoneyorderId != '') { ?>
                <div class="margin-five">
                    <div class="lblButton" style="padding-left:5px;">
                        <input  class="button noPrint" id="viewMoneyOrderSlip" value='View Money Order Details' type="button" />
                    </div>
                    <div class="lblButtonRight" style="*display:none;">
                        <div class="btnRtCorner"></div>
                    </div>
                </div>
                <?php } else { ?>
                <div class="margin-five">
                    <div class="lblButton" style="padding-left:5px;">
                        <input  class="button noPrint" id="yes" value='Yes, I have my money order to proceed' type="button" onclick="gotoNext()">
                    </div>
                    <div class="lblButtonRight" style="*display:none;">
                        <div class="btnRtCorner"></div>
                    </div>
                </div>
                <?php } ?>                
                <div class="margin-five">
                    <div class="lblButton" style="padding-left:5px;">
                        <input  class="button noPrint" id="print" value='Print Money Order Instructions' type="button" />
                    </div>
                    <div class="lblButtonRight">
                        <div class="btnRtCorner"></div>
                    </div>
                </div>
                <div class="margin-five">
                    <div class="lblButton" style="padding-left:5px;">
                        <input type="button" class="button noPrint" onclick="javaScript:window.print();" name="Print" value="Print">
                    </div>
                    <div class="lblButtonRight">
                        <div class="btnRtCorner"></div>
                    </div>
                </div>
                
            </td>
        </tr>
    </table>
    <br>
	<div style="margin-left:5px;"></div>
  </form>
</div>
<?php } ?>

<script>
  function gotoNext(){
     var moCheckbox = $('input:checkbox[name="moNumber"]:checked').val();
     var courCompCheckbox = $('input:checkbox[name="courierCompany"]:checked').val();     
    
     if(moCheckbox == '1' && courCompCheckbox == '1'){
         $('#checkBoxError').html('');
         window.location = '<?php echo url_for('mo_configuration/saveMoneyorder'); ?>';
     }else{         
         $('#checkBoxError').html('Kindly verify below information by checking the checkboxes.');
     }
  }

  $(document).ready(function(){
    $('#print').click(function(){
        var url = '<?php echo url_for('paymentGateway/printMoInfo?mode=print'); ?>';
        window.open(url, '', "status=1,toolbar=0,width=800px,height=800px,location=0,menubar=0,scrollbars=1");
    });
    
    $('#viewMoneyOrderSlip').click(function(){
        var url = '<?php echo url_for('mo_configuration/done?id='.$encriptedMoneyorderId); ?>';
        window.location = url;
    });
    

  })
  
  
  </script>