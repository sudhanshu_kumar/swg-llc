<?php
if($processingCountry == 'GB'){
   $currencySymbol = '&amp;pound;';
}else{
   $currencySymbol = '$';
}
?>

<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">
<!--This templates has interface from which user enters details for Money Order and associate it with a tracking number. -->
<?php
use_helper('Form');
use_helper('Pagination');
include_partial('global/innerHeading',array('heading'=>'Associate Tracking Number'));
?>
 <div class="clear"></div>
        <div class="tmz-spacer"></div>

<?php echo form_tag('mo_configuration/associateTrackingNumber',array('name'=>'order_search_form','class'=>'', 'method'=>'post','id'=>'searchApplicationForm' ,'onsubmit'=>'return validate_form()','name'=>'searchApplicationForm')) ?>

 <div id="flash_notice" class="highlight yellow">
            <!-- DUE TO THE HIGH INCIDENCE OF USE OF STOLEN CREDIT CARDS ON OUR PLATFORM, ALL PAYMENTS FOR APPLICATIONS FOR VISA AND PASSPORT SERVICES SHALL BE MADE ONLY WITH UNITED STATES POSTAL SERVICE MONEY ORDERS EFFECTIVE FROM MARCH 2, 2011.  CREDIT/DEBIT CARD PAYMENTS ARE NO LONGER ACCEPTABLE. WE REGRET ANY INCONVENIENCE THIS MAY CAUSE OUR CUSTOMERS. -->

            <?php echo sfConfig::get('app_mo_based_content');?>
      </div>


    <?php
    $sf = sfContext::getInstance()->getUser();
    if($sf->hasFlash('notice')){ ?>
    <div id="flash_notice" class="alertBox" >
      <?php
      echo nl2br($sf->getFlash('notice'));
      ?>
    </div>
  <br/>
    <?php }?>

      <?php
      $sf = sfContext::getInstance()->getUser();
      if($sf->hasFlash('error')){ ?>
      <div id="flash_error" class="alertBox" >
        <?php
        echo nl2br($sf->getFlash('error'));
        ?>
      </div>
      <br/>
      <?php }?>
    
    
     <!-- ########### START APPLICATION IN CART ############## -->

  <div class="clear tmz-spacer"></div>

  
  <div class="tmz-info-div">
      <table width="100%" border="0" cellspacing="0" cellpadding="0" class="">
          <tr>
              <td valign="top" class="tmz-tab-heading blbar" style="margin-left: 0px;padding-left:5px;">Application Details</td>
          </tr>
          <tr>
              <td valign="top" class="grey_bg_table">
                  <?php include_partial('cart/cart_details', array('processingCountry' => $processingCountry, 'currencySymbol' => $currencySymbol)); ?>
              </td>
          </tr>
      </table>
  </div>
      
        <div class="clear tmz-spacer"></div>

  <!-- ########### END APPLICATION IN CART ############## -->

  <?php
    $colspan = '4';
    $isValid = true;
    if($isValid) { ?>      
      <table width="100%">
        <tr>
          <td class="blbar" colspan="<?php echo $colspan?>" align="left">Enter Money Order Details</td>
        </tr>
        <tr>
          <td valign="top">Money Order Serial Number<span class="red">*</span><br><small><i>(Kindly use only one Money Order Serial Number at a time.)</i></small></td>
          <td><?php echo $form['moneyorder_number']->render(); ?>
            <br>
            <div class="red" id="moneyorder_number_error">
              <?php echo $form['moneyorder_number']->renderError(); ?>
            </div>

          </td>
        </tr>

        <tr>
          <td valign="top">Confirm Money Order Serial Number<span class="red">*</span></td>
          <td><?php echo $form['re_moneyorder_number']->render(); ?>
            <br>
            <div class="red" id="re_moneyorder_number_error">
              <?php echo $form['re_moneyorder_number']->renderError(); ?>
            </div>
          </td>
        </tr>
        <tr>
          <td width="30%" valign="top"><?php echo $form['amount']->renderLabel()." (".html_entity_decode($currencySymbol).")"; ?><span class="red">*</span><br><small><i>(Kindly do not enter decimal places or <?php echo html_entity_decode($currencySymbol); ?> sign inside the box.)</i></small></td>
          <td><?php echo $form['amount']->render(); ?>
            <br>
            <div class="red" id="amount_error">
              <?php echo $form['amount']->renderError(); ?>
            </div>

          </td>
        </tr>
        <tr>
          <td valign="top">Money Order Issuing Date<span class="red">*</span><br>(dd/mm/yyyy)</td>
          <td><?php echo $form['moneyorder_date']->render(); ?>
            <br>
            <div class="red" id="moneyorder_date_error">
              <?php echo $form['moneyorder_date']->renderError(); ?>
            </div>
          </td>
        </tr>
        <script>
            $(document).ready(function(){
                $('#moneyorder_moneyorder_number').focus();
            });
        </script>

        <tr>
          <td valign="top"><?php echo $form['address']->renderLabel(); ?><span class="red">*</span></td>
          <td><?php echo $form['address']->render(); ?>
            <br>
            <div class="red" id="address_error">
              <?php echo $form['address']->renderError(); ?>
            </div>
          </td>
        </tr>
        <tr>
          <td valign="top"><?php echo $form['phone']->renderLabel(); ?><span class="red">*</span></td>
          <td><?php echo $form['phone']->render(); ?>
            <br>
            <div class="red" id="phone_error">
              <?php echo $form['phone']->renderError(); ?>
            </div>
          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td style="border-left:none;">
              <div>
              <input type="Submit" id="btnSubmit" value="  Proceed  " class="normalbutton" />
            </div>            
            <input type="hidden" name="amt_to_paid" id="amt_to_paid"  class="txt-input" readonly="readonly" value="<?php echo $totalAppsAmount; ?>" />
          </td>

        </tr>
        <tr>
            <td colspan="2">
                <center><i><font color="red">Clicking this button will generate and associate a Tracking Number to this application.</font></i></center>
            </td>
        </tr>
      </table>
      </form>
      <?php } ?>
    </div>
  </div>
  <div class="content_wrapper_bottom"></div>
<script type="text/javascript">
  $(document).ready(function(){
    $('#moneyorder_moneyorder_number').bind("cut copy paste",function(e) {
      e.preventDefault();
    });

    $('#moneyorder_re_moneyorder_number').bind("cut copy paste",function(e) {
      e.preventDefault();
    });
   });
</script>

<script>
    /* Making both div height same...  */
    $('#tmz_cart_info_id').height($('#tmz_info_id').height());
    
</script>
<script>

  function validate_form(){


      var illegalChars = /^[0-9 \.]*$/;

      var amtToPaid = $("#amt_to_paid").val();

      var moneyorderAmt = $.trim($("#moneyorder_amount").val());

      var errFlag = false;
      if(moneyorderAmt == '')
      {
        $("#amount_error").html("Please enter money order amount.");
        errFlag = true;
      }
      //adding only numeric mo amount bug-id 29312 & 33163
      else if(!illegalChars.test(moneyorderAmt)){
        $("#amount_error").html("Money Order amount should be numeric.");
        errFlag = true;
      }
      else if(parseFloat(amtToPaid) != parseFloat(moneyorderAmt)){
        $("#amount_error").html("Amount to be paid and money order amount should be same.");
        errFlag = true;
      }
      else
      {
        $("#amount_error").html("");
      }

      var moneyorderNumber = $.trim($("#moneyorder_moneyorder_number").val());

      if(moneyorderNumber == '')
      {
        $("#moneyorder_number_error").html("Please enter money order serial number.");
        errFlag = true;
      }
      else
      {
          $("#moneyorder_number_error").html("");
      }
      var remoneyorderNumber = $.trim($("#moneyorder_re_moneyorder_number").val());
      if(remoneyorderNumber == '')
      {
        $("#re_moneyorder_number_error").html("Please enter confirm money order serial number.");
        errFlag = true;
      }else{ if(moneyorderNumber != ''){
          if(remoneyorderNumber != moneyorderNumber)
           {
            $("#re_moneyorder_number_error").html("Money order serial number and Confirm money order serial number do not match.");
            errFlag = true;
           }
           else
           {
            $("#re_moneyorder_number_error").html("");
           }
      }
      }

      if($("#moneyorder_moneyorder_date_day").val() == '')
      {
        $("#moneyorder_date_error").html("Please enter Money Order Issuing Date.");
        errFlag = true;
      }

      if($("#moneyorder_moneyorder_date_month").val() =='')
      {
        $("#moneyorder_date_error").html("Please enter Money Order Issuing Date.");
        errFlag = true;
      }

      if($("#moneyorder_moneyorder_date_year").val() =='')
      {
        $("#moneyorder_date_error").html("Please enter Money Order Issuing Date.");
        errFlag = true;
      }

      if($("#moneyorder_moneyorder_date_year").val() == '' || $("#moneyorder_moneyorder_date_month").val() =='' || $("#moneyorder_moneyorder_date_day").val() == '')
      {
        $("#moneyorder_date_error").html("Please enter Money Order Issuing Date.");
        errFlag = true;
      }
      else
      {
          $("#moneyorder_date_error").html("");
      }

      if($.trim(($('#moneyorder_address').val())) == ''){
          $('#address_error').html('Please Enter Contact Information.');
          errFlag = true;
      }
      else
      {
          $('#address_error').html('');
      }
      if($.trim($('#moneyorder_phone').val()) == ''){
          $('#phone_error').html('Please Enter Phone Number.');
          errFlag = true;
      }
      else
      {
          $('#phone_error').html('');
      }

      //Round up decimal amount in amount validation
      if(errFlag){
        return false;
      }else {
          if(confirm('Your money order serial number is "'+moneyorderNumber+'". Do you want to continue?')){
            return true;
          }else{
            return false;
          }
      }
  }
</script>