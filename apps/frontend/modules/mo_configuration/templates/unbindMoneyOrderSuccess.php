<!--Unbind Money order application is being dis associated with tracking and becomes free to be added back in  the cart -->
<?php use_helper('Form'); ?>
<?php
use_javascript('dhtmlwindow.js');
use_stylesheet('dhtmlwindow.css');
use_javascript('modal.js');
use_stylesheet('modal.css');
?>
<script>
  function validateForm()
  {
    var err = false;

    var app_id = jQuery.trim($('#app_id').val());
    var tracking_number = jQuery.trim($('#tracking_number').val());

    if(app_id != '' && tracking_number == ''){
         if($('#app_type').val() == ''){
              $('#app_type_error').html('Please select application type.');
              $('#app_type').focus();
              err = true;
         }
         if(isNaN(app_id)){
              $('#app_id_error').html('Please provide valid applicatio id.');
              err = true;
         }//End of if(isNaN(app_id)){...
         else{
             $('#app_id_error').html('');
         }         
         $('#errorBoth').html('');
    }else if(app_id == '' && tracking_number != ''){
         $('#errorBoth').html('');
         $('#app_type').val('');
    }else if(app_id == '' && tracking_number == ''){
         if($('#app_type').val() != ''){
            $('#app_id_error').html('Please provide application id.');
         }else{
            $('#errorBoth').show();
            $('#errorBoth').html('Please provide either Application Id OR Tracking Number.');
            $('#app_id_error').html('');
            $('#app_type_error').html('');
            $('#tracking_number_error').html('');            
         }
         err = true;
    }else if(app_id != '' && tracking_number != ''){
         $('#errorBoth').show();
         $('#errorBoth').html('Please provide either Application Id OR Tracking Number.');
         $('#app_id_error').html('');
         $('#app_type_error').html('');
         $('#tracking_number_error').html('');
         err = true;
    }else{
         $('#app_id_error').html('');
         $('#app_type_error').html('');
         $('#tracking_number_error').html('');
         $('#errorBoth').html('');
    }
    
    if(err){
        return false;
    }else{
        return true;
    }
  }

  function selectAppType(selVal){
      if(selVal != ''){
        $('#app_type_error').html('');
      }
  }
  
</script>

<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">
    <?php include_partial('global/innerHeading',array('heading'=>'Unbind Money Order Application')); ?>
    <div class="clear"></div>
    <div class="tmz-spacer"></div>
<?php
$sf = sfContext::getInstance()->getUser();
if($sf->hasFlash('notice')){ ?>
    <div id="flash_notice" class="alertBox" >
<?php
    echo nl2br($sf->getFlash('notice'));
    
?>
    </div>
    <br/>
<?php }?>
    <div class="highlight yellow">Note: All applications associated to tracking number will be listed and unbound. </div>
            <?php echo form_tag('mo_configuration/unbindMoneyOrder',array('name'=>'app_search_form','class'=>'', 'method'=>'POST','id'=>'app_search_form','onSubmit'=>'return validateForm()')) ?>
            <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td width="150" class="ltgray" valign="top">Application Type <span class="red">*</span></td>
        <td><?php
                    $app_type = array('' => 'Select Application Type', 'P' => 'Passport', 'V' => 'Visa', 'F' => 'Free Zone','VAP'=>'Visa On Arrival');
                    echo select_tag('app_type', options_for_select($app_type, $appType), array("onchange" => "selectAppType(this.value);")); ?>
          <br/>
          <span id="app_type_error" class="red"></span> </td>
            </tr>
            <tr>
                <td class="ltgray" valign="top">Application Id <span class="red">*</span></td>
        <td><?php echo input_tag('app_id', $app_id, array('size' => 20, 'maxlength' => 20, 'class'=>'txt-input'));?> <br/>
          <span id="app_id_error" class="red"></span> </td>
            </tr>
            <tr>                
        <td colspan="2"><strong>OR</strong></td>
            </tr>
            <tr>
                <td class="ltgray" valign="top">Tracking Number <span class="red">*</span></td>
        <td><?php echo input_tag('tracking_number', $tracking_number, array('size' => 20, 'maxlength' => 20, 'class'=>'txt-input'));?> <br/>
          <span id="tracking_number_error" class="red"></span> </td>
            </tr>       
            
            <tr  ><td colspan="2" id="errorBoth" class="no_display red"></td></tr>
            <tr>
                <td>&nbsp;</td>
        <td><?php echo submit_tag('Search',array('class' => 'normalbutton')); ?></td>
                </tr>
            </table>
            </form>
            <?php if($postData){ ?>
            <?php $colspan = 5; ?>
	<div class="clear">&nbsp;</div>
            <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
                <tr>
                  <td class="blbar" align="left">Application Details:</td>
                </tr>                                
                  <tr>
        <td valign="top"><?php echo form_tag('mo_configuration/unbindMoneyOrderDone',array('name'=>'app_form','class'=>'', 'method'=>'POST','id'=>'app_form', 'onSubmit' => 'return freeFormCheck();')) ?>
                      <table cellspacing="0" cellpadding="0" border="1" width="100%" >
                      <?php if($cartTrackingStatus == 'Associated') { ?>
                        <tr>
              <td colspan="<?php echo $colspan?>"> Money Order Tracking Number: <strong><a href = "javascript:void(0);" onclick="trackingInfoBox('<?php echo $cartTrackingId; ?>','<?php echo $cartTrackingStatus; ?>');return false;"><span><?php echo $cartTrackingNumber; ?></span></a></strong> </td>
                        </tr>
                        <?php } ?>
                        <tr>
              <td colspan="<?php echo $colspan?>"> Tracking Number Status: <strong><span><?php echo $cartTrackingStatus; ?></span></strong> </td>
                        </tr>
                        <tr bgcolor="#eeeeee">
                          <td width="15%"><span class="txtBold">Application Id</span></td>
                          <td width="14%"><span class="txtBold">Application Type</span></td>
                          <td width="14%"><span class="txtBold">Reference Number</span></td>
                          <td width="30%"><span class="txtBold">Applicant Name</span></td>
                          <td width="12%"><span class="txtBold">Application Status</span></td>
                        </tr>
                        <?php
                        $appDetailsCount = count($appDetailsArray);

                        if($appDetailsCount > 0){
                          for($j=0;$j<$appDetailsCount;$j++){ ?>
                        <tr>
                          <td><?php echo $appDetailsArray[$j]['id']?></td>
                          <td><?php if($appDetailsArray[$j]['app_type']=="vap") { echo sfConfig::get('app_visa_arrival_title');  } else { echo ucfirst($appDetailsArray[$j]['app_type']); }?></td>
                          <td><?php echo $appDetailsArray[$j]['ref_no']?></td>
                          <td><?php echo $appDetailsArray[$j]['name']?></td>
                          <td><?php echo $appDetailsArray[$j]['status']?></td>
                        </tr>
                        <?php } ?>
                        <tr>
                          
              <td align="center" colspan="5"><?php echo submit_tag('Unbind Application',array('class' => 'normalbutton')); ?> </td>
                        </tr>
                        <?php } else {  ?>                        
                        <tr>
              <td colspan="<?php echo $colspan;?>" align="center" class="red" >No application details found.</td>
                        </tr>
                        <?php } ?>
                      </table>
                      </form>
                    </td>                    
                  </tr>
              </table>
              <?php }//End of if($postData){... ?>
        </div>
      </div>
<div class="content_wrapper_bottom"></div>
    <script>
        function freeFormCheck(){
            if(!confirm('Are you sure. You really want to unbind these application(s)?')){
                return false;
            }
            return true;
        }

        function trackingInfoBox(id,status){            
            emailwindow = dhtmlmodal.open('EmailBox', 'iframe', "<?php echo url_for('supportTool/trackingInfoBox').'?tracking_id=' ?>"+id+"&status="+status, 'Tracking Number Details', 'width=650px,height=320px,center=1,border=0, scrolling=no');
        }

    </script>
