<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">

<!--This template is being displayed after money order serial is being associated with tracking number(s) -->
<?php
use_helper('Form');
include_partial('global/innerHeading',array('heading'=>'Money Order Details'));
?>
    <div class="clear"></div>
    <div class="tmz-spacer"></div>
   
        <?php
        $sf = sfContext::getInstance()->getUser();
        if($sf->hasFlash('notice')){ ?>
        <div id="flash_notice" class="alertBox" >
          <?php
          echo nl2br($sf->getFlash('notice'));
          ?>
        </div><br/>
        <?php }?>

 
          <?php
          $sf = sfContext::getInstance()->getUser();
          if($sf->hasFlash('error')){ ?>
          <div id="flash_error" class="alertBox" >
            <?php
            echo nl2br($sf->getFlash('error'));
            ?>
          </div><br/>
          <?php }?>
         
           <?php include_partial('moneyOrderDetails', array('moneyOrderDetails' => $moneyOrderDetails, 'applicationDetails' => $applicationDetails, 'record' => $record)); ?>
         

      <br>
      <div  class="wrapForm2">
        <table width="100%">

      <tr>
        <td>
          An email has been sent to you confirming, your order number & tracking number is associated with a money order serial number.<br/><br/>
		  
		  <div class="prominent">Please mail the filled in Money Order via a traceable courier service (such as Fedex, UPS and DHL) to following address:<br/><br/>
      <strong>SW Global LLC<br/>
      50 Albany Turnpike, Suite 5032<br/>
      Canton, CT 06019<br/></strong><br/>

      Once we receive your money order, we’ll confirm the receipt via another email to you stating that your payment has been received and you can proceed to submit your Visa/Passport application.</div>
        </td>

      </tr>


      


    </table>
    <?php echo form_tag('mo_configuration/moneyOrderDetails?id='.$moneyOrderEncpId,array('name'=>'frm', 'method'=>'post', 'id'=>'frm', 'onSubmit'=>'return validate_form();')) ?>
    <table width="100%" cellspacing="0" cellpadding="0" class="noPrint">
        <tr>
            <td class="highlight yellow" valign="top" width="30%"><strong>Please select your shipping service</strong><span class="red">*</span>
            <td class="highlight yellow" valign="top">
            <input type="radio" name="courierFlag" id="courierFlag1" value="Yes" />&nbsp;<strong>Courier</strong>&nbsp;&nbsp;
            <input type="radio" name="courierFlag" id="courierFlag2" value="No" />&nbsp;<strong>Regular Mail</strong>
            <div class="red" id="radio_error" ></div>
            <div style="padding-top:30px;display:none;" id="noteId">
                <span class="red">Note:</span> Processing may be delayed, if you use regular mail as shipping service.
            </div>
            </td>
        </tr>
        <tr>
            <td valign="top" colspan="2">
                <input type="checkbox" name="chk" id="chk" value="1" />&nbsp;I hearby confirm that I have written tracking number on money order. <a href="javascript:void(0);" id="moneyOrderSlipId" ><strong>Click here</strong></a> to see money order slip.
                <div class="red" id="chk_error" ></div>
            </td>
        </tr>
        <tr>
             
            <td colspan="2" align="center">
                <input type="Submit" id="submit" name="btnSubmit" value="Submit" class="normalbutton" />
                <input type="button" onclick="javaScript:window.print();" name="Print" value="Print" class="normalbutton">
            </td>
        </tr>
    </table></form>
        </div>        
      </div>
<script>
    
    function validate_form(){
        var errFlag = false;

        var courierFlag = $('input[name=courierFlag]:checked').val();
        switch(courierFlag){
            case 'Yes':
                $('#radio_error').html('');
                break;
            case 'No':
                $('#radio_error').html('');
                break;
            default:
                $('#radio_error').html('Please select shipping service.');
                errFlag = true;
                break;
        }


        if($('#chk').attr('checked')){
              $('#chk_error').html('');
        }else{
              $('#chk_error').html('Please select confirmation checkbox.');
              errFlag = true;
        }

        if(errFlag){
            return false;
        }else{
            return true;
        }

    }
    
    $(document).ready(function(){

        $('#moneyOrderSlipId').click(function(){
            var url = '<?php echo url_for('paymentGateway/printMoInfo?mode=print&moi='.$moneyOrderId); ?>';
            window.open(url, '', "status=1,toolbar=0,width=800px,height=800px,location=0,menubar=0,scrollbars=1");
        });

        $('#chk').click(function(){
            if($('#chk').attr('checked')){
                  $('#chk_error').html('');
            }else{
                  $('#chk_error').html('Please select confirmation checkbox.');
                  errFlag = true;
            }
        });

        $('#courierFlag1').click(function(){
            if($('#courierFlag1').attr('checked')){
                  $('#radio_error').html('');
                  $('#noteId').hide();
            }
        });
        $('#courierFlag2').click(function(){
            if($('#courierFlag2').attr('checked')){
                  $('#radio_error').html('');
                  $('#noteId').show();
            }
        });
        
    })
</script>
    </div>
<div class="content_wrapper_bottom"></div>