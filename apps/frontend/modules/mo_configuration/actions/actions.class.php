<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of actionsclass
 *
 * @author akumar1
 */
class mo_configurationActions extends sfActions {    

    /**
     * Display tacking number after being generated
     * @param sfWebRequest $request
     */
    public function executeDisplayTracking(sfWebRequest $request) {
        //show tracking number
        $this->data = false;
        if ($request->hasParameter("id")) {
            $trackingId = $request->getParameter("id");
            $encriptedTrackingId = SecureQueryString::DECODE($trackingId);
            $trackingId = SecureQueryString::ENCRYPT_DECRYPT($encriptedTrackingId);
            $trackingDetails = CartTrackingNumberTable::getInstance()->find($trackingId)->toArray();
            if (isset($trackingDetails) && count($trackingDetails) > 0) {
                $this->data = true;
                $this->trackingDetails = $trackingDetails;
            } else {
                $this->getUser()->setFlash("notice", "Invalid Tracking Number Request.");
            }
        }
    }

    /**
     * This function is used to associate tracking number...
     * @param sfWebRequest $request
     * @return <type>
     */
    public function executeSaveMoneyorder(sfWebRequest $request) {
        $this->form = new MoneyorderForm();
        $this->isValid = false;

        if ($request->getParameter('userid')) {
            $userid = Settings::encryptInput($request->getParameter('userid'));
        } else {
            $userid = $this->getUser()->getGuardUser()->getId();
        }

        $this->userid = $userid;

        $pendingtrackingNumbers = CartTrackingNumberTable::getInstance()->getTrackingNumbersByStaus($userid, "New");
        $pendingtrackingNumbersCount = count($pendingtrackingNumbers);

        $trackingArray = array();
        if ($pendingtrackingNumbersCount > 0) {

            for ($i = 0; $i < $pendingtrackingNumbersCount; $i++) {

                foreach ($pendingtrackingNumbers[$i] AS $key => $value) {
                    $trackingArray[$i]['track'][$key] = $value;
                }

                if (isset($pendingtrackingNumbers[$i]['cart_id'])) {
                    $fields = 'item_id AS item_id';
                    $cartTrakingInfo = Doctrine::getTable('CartItemsTransactions')->getCartItemInfoByCartId($pendingtrackingNumbers[$i]['cart_id'], $fields);                    
                    $cartTrakingInfoCount = count($cartTrakingInfo);

                    for ($j = 0; $j < $cartTrakingInfoCount; $j++) {
                        $item_id = $cartTrakingInfo[$j]['item_id'];
                        if ($item_id != '') {
                            $appIdDetails = Doctrine::getTable('IPaymentRequest')->getAppId($item_id);
                            if (empty($appIdDetails))
                                continue;

                            $appDetails = array(); /* Bug id: 29837 */
                            if ($appIdDetails[0]['passport_id'] != '') {
                                $appDetails['passport_id'] = $appIdDetails[0]['passport_id'];
                            } else if ($appIdDetails[0]['visa_id'] != '') {
                                $appDetails['visa_id'] = $appIdDetails[0]['visa_id'];
                            } else if ($appIdDetails[0]['freezone_id'] != '') {
                                $appDetails['freezone_id'] = $appIdDetails[0]['freezone_id'];
                            }

                            ## Getting application details...
                            $applicationDetails = Doctrine::getTable('CartItemsTransactions')->getApplicationDetails($appDetails);
                            $trackingArray[$i]['app_details'][$j] = $applicationDetails;
                        }//End of if($item_id != ''){...
                    }//End of for($j=0;$j<$cartTrakingInfoCount;$j++){...
                }//End of if(isset($pendingtrackingNumbers[$i]['cart_id'])){...
            }//End of for($i=0;$i<$pendingtrackingNumbersCount;$i++){...
        }//End of if($pendingtrackingNumbersCount > 0){...


        $this->pendingtrackingNumbers = $trackingArray;


        if (isset($this->pendingtrackingNumbers) && is_array($this->pendingtrackingNumbers) && count($this->pendingtrackingNumbers) > 0) {
            $this->isValid = true;
        } else {
            $this->getUser()->setFlash("error", "No tracking numbers for associate", false);
        }
        //    echo "<pre>";print_r($pendingtrackingNumbers);die;
        $this->setTemplate("moneyOrder");
        if ($request->isMethod('post')) {
            $form_val = $request->getParameter($this->form->getName());
            
//                  echo "<pre>";print_r($form_val);die;
            $trackingNumberArr = $request->getParameter("chk_fee");
            if (count($trackingNumberArr) > 0) {
                $amount = 0;
                $actualAmt = 0;
                $trackingArr = array();
                $orderReqArr = array();
                $arrPaid = array();
                $paidTrackingNumber = array();
                foreach ($trackingNumberArr as $trackingNumber) {
                    //echo '<pre>';print_r($trackingNumber);die;
                    $tracArr = explode("_", $trackingNumber);
                    $amount = $amount + $tracArr[1];
                    $trackingObj = CartTrackingNumberTable::getInstance()->find($tracArr[0]);
                    //add code for bug-id 29316
                    if ($trackingObj->getStatus() == 'Associated') {
                        $arrPaid[] = $trackingObj->getStatus();
                    }


                    $cartId = $trackingObj->getCartId();                    

                    if (isset($cartId) && $cartId != '') {

                          $fields = 'item_id AS item_id';
                          $cartTrakingInfo = Doctrine::getTable('CartItemsTransactions')->getCartItemInfoByCartId($cartId, $fields);                          
                          $cartTrakingInfoCount = count($cartTrakingInfo);
                          $cartTransArray = array();
                          for ($i = 0; $i < $cartTrakingInfoCount; $i++) {
                                $item_id = $cartTrakingInfo[$i]['item_id'];
                                if ($item_id != '') {
                                    $appIdDetails = Doctrine::getTable('IPaymentRequest')->find($item_id);                                                                        
                                    if(count($appIdDetails) > 0){
                                        $appIdArray = array();
                                        if ($appIdDetails['passport_id'] != '') {
                                            $appIdArray = Doctrine::getTable('IPaymentRequest')->findByPassportId($appIdDetails['passport_id']);
                                        } else if ($appIdDetails['visa_id'] != '') {
                                            $appIdArray = Doctrine::getTable('IPaymentRequest')->findByVisaId($appIdDetails['visa_id']);
                                        } else if ($appIdDetails['freezone_id'] != '') {
                                            $appIdArray = Doctrine::getTable('IPaymentRequest')->findByFreezoneId($appIdDetails['freezone_id']);
                                        }

                                        $appIdArrayCount = count($appIdArray);
                                        if($appIdArrayCount > 0){

                                            $itemIdArray = array();
                                            ## Getting ItemIds...
                                            for($j=0;$j<$appIdArrayCount;$j++){
                                                $itemIdArray[$j] = $appIdArray[$j]['id'];
                                            }

                                            ## Getting cartinfo from itemids...
                                            $cartDetails = Doctrine::getTable('CartItemsTransactions')->getCartItemAllIdInfo($itemIdArray);
                                            $cartDetailsCount = count($cartDetails);

                                            ## Making array of transaction numbers i.e., retry ids...
                                            for($j=0;$j<$cartDetailsCount;$j++){
                                                  $cartTransArray[] = $cartDetails[$j]['transaction_number'];
                                            }

                                      }//End of if($appIdArrayCount > 0){...


                                    }//End of if($appIdDetailsCount > 0){...
                                    
                                    
                                }//End of if($item_id != ''){...
                    
                           }//End of for ($i = 0; $i < $cartTrakingInfoCount; $i++) {...
                           
                           $cartTransArray = array_unique($cartTransArray);                           

                           ## Finding cartid exists in cartTrackingNumber table and if exists then set status 1 for delete field...
                           $cartTransArrayCount = count($cartTransArray);
                           for($i=0;$i<$cartTransArrayCount;$i++){                               
                                $cartObj = Doctrine::getTable('OrderRequestDetails')->findByRetryId($cartTransArray[$i]);
                                if(count($cartObj) > 0){
                                    if($cartObj[0]['payment_status'] == 0){
                                        $paidTrackingNumber[] = $trackingObj->getTrackingNumber();
                                    }                                    
                                }//End of if(count($cartObj) > 0){...
                           }//End of for($i=0;$i<$cartTransArrayCount;$i++){...
                     }//End of if (isset($cartId) && $cartId != '') {...


                    $trackingAmountDB = $trackingObj->getCartAmount();
                    if (isset($trackingAmountDB) && $trackingAmountDB > 0) {
                        $actualAmt = $actualAmt + $trackingAmountDB;
                        $arr = array($tracArr[0], $trackingObj->getOrderRequestDetailId());
                        $trackingArr[] = $arr;
                    } else {
                        $this->getUser()->setFlash("error", "Invalid request found.");
                        return;
                    }
                }


                if(count($paidTrackingNumber) > 0){
                    $this->getUser()->setFlash("notice", "Tracking Number(s) ". implode(",", $paidTrackingNumber). " has been already Paid.");
                    $this->redirect("mo_configuration/saveMoneyorder");
                }

                
                //add code for bug-id 29316
                if (count($arrPaid) > 0) {

                    $this->redirect("mo_configuration/saveMoneyorder");
                }

                if ($actualAmt == $amount && $actualAmt == $form_val['amount']) {                    
                    $courierCompanyList = sfConfig::get('app_courierCompanyList');
                if(!empty($form_val['courier_flag'])){
                    if($form_val['courier_flag'] == 'on'){
                        $form_val['courier_flag'] = 'Yes';
                    } } else {
                        unset($form_val['courier_flag']);
                        unset($form_val['courier_service']);
                        unset($form_val['waybill_trackingno']);

                    }
                if(!empty($form_val['courier_service'])) {
                    if($form_val['courier_service'] == '0'){
                        unset($form_val['courier_service']);
                    }else if($form_val['courier_service'] == '4'){
                        $form_val['courier_service'] = trim($request->getParameter('otherCourerService'));
                    } else {
                        $form_val['courier_service'] = $courierCompanyList[$form_val['courier_service']];
                    }
                 }
                    $this->form->bind($form_val);
                    if ($this->form->isValid()) {
                        $con = Doctrine_Manager::connection();
                        try {
                            $con->beginTransaction();
                            $this->form->save();
                            $moneyorderId = $this->form->getObject()->getId();

                            $paymentManager = new PaymentManager();
                            $isMoneyordered = $paymentManager->moneyorder($trackingArr, $moneyorderId, $form_val);
                            $con->commit();

                            $this->getUser()->setFlash("notice", "Your Money Order is successfully associated with Tracking number(s).");
                            $encriptedMoneyorderId = SecureQueryString::ENCRYPT_DECRYPT($moneyorderId);
                            $encriptedMoneyorderId = SecureQueryString::ENCODE($encriptedMoneyorderId);
                            //send mail to user
                            $mailUrl = "notification/trackingMoneyOrderMail";

                            $mailTaskId = EpjobsContext::getInstance()->addJob('TrackingMoneyOrderMail', $mailUrl, array('money_order_id' => $moneyorderId, 'user_id' => $this->getUser()->getGuardUser()->getId()));

                            $this->redirect("mo_configuration/done?id=" . $encriptedMoneyorderId);
                        } catch (Exception $e) {
                            try {
                                $con->rollback();
                                $this->getUser()->setFlash("notice", "There is some error, please verify Money Order and do again.");
                            } catch (Exception $e) {
                                
                            }
                            $this->redirect("mo_configuration/saveMoneyorder");
                        }
                    }else {
                 $this->form_val = $form_val;
                    }
                } else {
                    $this->getUser()->setFlash("error", "Invalid Amount please check amount.");
                    return;
                }
            } else {
                $this->getUser()->setFlash("error", "Please Select atleast one tracking number.");
                return;
            }
        }
    }

    /**
     * Function is called when tracking number is successfully associated with the money order receipt and is processed further of approval of the payment.
     * @param sfWebRequest $request
     */
    public function executeDone(sfWebRequest $request) {
        $moneyOrderId = $request->getParameter("id");

        $encriptedMoneyorderId = SecureQueryString::DECODE($moneyOrderId);
        $moneyOrderId = SecureQueryString::ENCRYPT_DECRYPT($encriptedMoneyorderId);
        //    $moneyOrderDetails = TrackingMoneyorderTable::getInstance()->findByMoneyorderId($moneyOrderId)->toArray();
        $this->moneyOrderDetails = MoneyorderTable::getInstance()->find($moneyOrderId);
        if (!empty($this->moneyOrderDetails)) {
            $this->moneyOrderDetails = $this->moneyOrderDetails->toArray();
        } else {
            $this->getUser()->setFlash("notice", 'Oops! URL tempering is not allow.');
            $this->redirect('cart/list');
        }

        /**
         * If user try to change monery order id from the address bar then this condition will work...
         */
        if('Paid' == $this->moneyOrderDetails['status'] || $this->getUser()->getGuardUser()->getId() != $this->moneyOrderDetails['created_by']){
            $this->getUser()->setFlash("notice", 'Oops! URL tempering is not allow.');
            $this->redirect('cart/list');
        }

        $this->record = false;
        $trackingDetails = TrackingMoneyorderTable::getInstance()->getMoneyOrderDetails($moneyOrderId);
        // echo '<pre>';print_r($trackingDetails);
        if (isset($trackingDetails) && is_array($trackingDetails) && count($trackingDetails) > 0) {
            $i = 0;
            foreach ($trackingDetails as $orderDetail) {
                //get retry id
                $retryId = OrderRequestDetailsTable::getInstance()->find($orderDetail['order_request_detail_id'])->toArray();

                $this->ipayNisAppDetails = CartItemsTransactionsTable::getInstance()->getRetryIdDetailsOnNis($retryId['retry_id']);
                // echo '<pre>';print_r($this->ipayNisAppDetails);die;
                foreach ($this->ipayNisAppDetails as $key => $app) {

                    $applicationDetails[$i]['tracking_number'] = $orderDetail['tracking_number'];                    
                     if($orderDetail['currency'] == '5'){
                        $applicationDetails[$i]['cart_amount'] = $orderDetail['convert_amount'];
                    }else{
                        $applicationDetails[$i]['cart_amount'] = $orderDetail['cart_amount'];
                    }
                    $applicationDetails[$i]['order_number'] = $orderDetail['order_number'];                    
                    $applicationDetails[$i]['app_detail'][$key]['app_id'] = $app['id'];
                    $applicationDetails[$i]['app_detail'][$key]['app_type'] = $app['app_type'];
                    $applicationDetails[$i]['app_detail'][$key]['ref_no'] = $app['ref_no'];
                }
                $i++;
            }
            //echo '<pre>';print_r($applicationDetails);die;
            $this->applicationDetails = $applicationDetails;
            $this->trackingDetails = $trackingDetails;
            $this->record = true;
        }
    }

    /**
     * Function is called when tracking number is successfully associated with the money order receipt and is processed further of approval of the payment.
     * @param sfWebRequest $request
     */
    public function executeMoneyOrderDetails(sfWebRequest $request) {

        $this->moneyOrderEncpId = $request->getParameter("id");

        $encriptedMoneyorderId = SecureQueryString::DECODE($this->moneyOrderEncpId);
        $this->moneyOrderId = SecureQueryString::ENCRYPT_DECRYPT($encriptedMoneyorderId);

        $moneyOrderDetailsObj = MoneyorderTable::getInstance()->find($this->moneyOrderId);
        if (!empty($moneyOrderDetailsObj)) {
            $this->moneyOrderDetails = $moneyOrderDetailsObj->toArray();
        } else {
            $this->getUser()->setFlash("notice", 'Oops! URL tempering is not allow.');
            $this->redirect('cart/list');
        }

        
        /**
         * If user try to change monery order id from the address bar then this condition will work...
         */
        if('Paid' == $this->moneyOrderDetails['status'] || $this->getUser()->getGuardUser()->getId() != $this->moneyOrderDetails['created_by']){
            $this->getUser()->setFlash("notice", 'Oops! URL tempering is not allow.');
            $this->redirect('cart/list');
        }

        if ($request->isMethod('post')) {

            $courierFlag = $request->getPostParameter('courierFlag');
            $moneyOrderDetailsObj->setCourierFlag($courierFlag);
            $moneyOrderDetailsObj->save();
            $this->getUser()->setFlash("notice", 'You have successfully associated tracking number with money order.');
            $this->redirect('mo_configuration/done?id='.$this->moneyOrderEncpId);
        }
        
        

        $this->record = false;
        $trackingDetails = TrackingMoneyorderTable::getInstance()->getMoneyOrderDetails($this->moneyOrderId);
        // echo '<pre>';print_r($trackingDetails);
        if (isset($trackingDetails) && is_array($trackingDetails) && count($trackingDetails) > 0) {
            $i = 0;
            foreach ($trackingDetails as $orderDetail) {
                //get retry id
                $retryId = OrderRequestDetailsTable::getInstance()->find($orderDetail['order_request_detail_id'])->toArray();

                $this->ipayNisAppDetails = CartItemsTransactionsTable::getInstance()->getRetryIdDetailsOnNis($retryId['retry_id']);
                // echo '<pre>';print_r($this->ipayNisAppDetails);die;
                foreach ($this->ipayNisAppDetails as $key => $app) {

                    $applicationDetails[$i]['tracking_number'] = $orderDetail['tracking_number'];

                    if($orderDetail['currency'] == '5'){
                        $applicationDetails[$i]['cart_amount'] = $orderDetail['convert_amount'];
                    }else{
                        $applicationDetails[$i]['cart_amount'] = $orderDetail['cart_amount'];
                    }
                    $applicationDetails[$i]['order_number'] = $orderDetail['order_number'];
                    $applicationDetails[$i]['app_detail'][$key]['app_id'] = $app['id'];
                    $applicationDetails[$i]['app_detail'][$key]['app_type'] = $app['app_type'];
                    $applicationDetails[$i]['app_detail'][$key]['ref_no'] = $app['ref_no'];
                }
                $i++;
            }
            //echo '<pre>';print_r($applicationDetails);die;
            $this->applicationDetails = $applicationDetails;
            $this->trackingDetails = $trackingDetails;
            $this->record = true;
        }

        /**
         * [WP: 085] => CR:124
         * Removed requestId from session variable...
         */        
        $this->getUser()->getAttributeHolder()->remove('requestId');

    }

    public function executeTracking(sfWebRequest $request) {
        
    }

    //To display sample image of Money Order
    public function executeExampleOfMoneyOrder() {
        $this->setLayout('popupLayout');
    }

    /**
     * This function checks whether the money order number given by user is duplicate or not.
     * @param sfWebRequest $request
     * @return <type>
     */
    public function executeUniqueOrderNumber(sfWebRequest $request) {
        $moneyorder_number = $request->getParameter("moneyorder_number");
        $isMoneyrderNumber = MoneyOrderTable::getInstance()->findByMoneyorderNumber($moneyorder_number)->count();        
        return $this->renderText($isMoneyrderNumber);        
    }

    /**
     * This function is used to search application by their id. Ansd display's the result for the application that are to be associated.
     * @param sfWebRequest $request
     */
    public function executeSearchAppForMO(sfWebRequest $request) {
        $submitFlag = $request->getParameter('submitFlag');
        $appType = $request->getParameter('app_type');
        $appID = $request->getParameter('app_id');
        $appRefNumber = $request->getParameter('ref_number');
        $notice = false;
        if (isset($submitFlag) && $submitFlag != '') {
            $paymentRequestRecord = Doctrine::getTable('CartItemsTransactions')->getPaymentRequestMoneyOrder($appID, $appType);

            $arrRequestRecord = array();
            for ($u = 0; $u < count($paymentRequestRecord); $u++) {
                $arrRequestRecord[] = $paymentRequestRecord[$u]['id'];
            }

            if (!empty($arrRequestRecord)) {

                $cartItemInfo = Doctrine::getTable('CartItemsTransactions')->getCartItemInfo($arrRequestRecord);
                $arrcartItemInfo = array();
                for ($y = 0; $y < count($cartItemInfo); $y++) {
                    $arrcartItemInfo[] = $cartItemInfo[$y]['cart_id'];
                }

                if (isset($cartItemInfo[0]['cart_id'])) {
                    $cartTrakingInfo = Doctrine::getTable('CartTrackingNumber')->getTrackingNumber($arrcartItemInfo, 'New');

                    if (isset($cartTrakingInfo[0]['user_id'])) {
                        //$request->setParameter('userid', $cartTrakingInfo[0]['user_id']);
                        //$this->forward('mo_configuration', 'saveMoneyorder');
                        $this->redirect('mo_configuration/saveMoneyorder?userid=' . Settings::encryptInput($cartTrakingInfo[0]['user_id']));
                    } else {
                        $notice = true;
                    }//End of if(isset($cartTrakingInfo[0]['user_id'])){...
                } else {
                    $notice = true;
                }//End of if(isset($cartItemInfo[0]['id'])){;...
            } else {
                $notice = true;
            }//End of if(isset($paymentRequestRecord[0]['id'])){...

            if ($notice) {
                $this->getUser()->setFlash("notice", "Application not found.");
            }//End of if($notice){...
        }
    }

//End of public function executeSearchAppForMO(...
    /**
     * Unibind's the application associated with tracking and user can again add the application to the cart.
     * @param sfWebRequest $request
     */

    public function executeUnbindMoneyOrder(sfWebRequest $request) {
        $this->appType = $request->getParameter('app_type');
        $this->app_id = trim($request->getParameter('app_id'));
        $this->tracking_number = trim($request->getParameter('tracking_number'));
        $notice = false;
        $this->postData = false;
        $msg = '';
        $this->cartTrackingStatus = '';
        if ($request->isMethod('post')) {
            $cartId = '';

            $arrcartItemInfo = array();
            if ($this->app_id != '') {
                $paymentRequestRecord = Doctrine::getTable('CartItemsTransactions')->getPaymentRequestMoneyOrder($this->app_id, $this->appType);

                $arrRequestRecord = array();
                for ($u = 0; $u < count($paymentRequestRecord); $u++) {
                    $arrRequestRecord[] = $paymentRequestRecord[$u]['id'];
                }

                $cartItemInfo = Doctrine::getTable('CartItemsTransactions')->getCartItemInfo($arrRequestRecord);

                for ($y = 0; $y < count($cartItemInfo); $y++) {
                    $arrcartItemInfo[] = $cartItemInfo[$y]['cart_id'];
                }

                $msg = 'application id.';
            }else{
                $msg = 'tracking number.';
            }


            ## Fetching cart tracking information...
            $trackingInfo = Doctrine::getTable('CartTrackingNumber')->getUnbindTrackingNumber($arrcartItemInfo, $this->tracking_number);

            if (!empty($trackingInfo)) {

                if(1 != $trackingInfo[0]['deleted']){
                    $cartId = $trackingInfo[0]['cart_id'];
                    $tracking_number = $trackingInfo[0]['tracking_number'];
                    $this->cartTrackingStatus = $trackingInfo[0]['status'];
                    $this->cartTrackingNumber = $trackingInfo[0]['tracking_number'];
                    $this->cartTrackingId = $trackingInfo[0]['id'];
                    $this->cartAmount = $trackingInfo[0]['cart_amount'];
                    if ($this->cartTrackingStatus == 'Paid') {
                        $cartId = '';
                        $notice = true;
                        $errMsg = 'Application is alreay paid.';
                    }
                }else{
                     $errMsg = 'Application has been already unbound.';
                     $notice = true;
                }
            }//End of if(isset($cartItemInfo[0]['cart_id'])){...
            else {
                $errMsg = 'Application not found by given '.$msg;
                $notice = true;
            }
            

            $this->appDetailsArray = array();

            /*
             * finally all application details will be fetched from the cart id...
             */
            if (isset($cartId) && $cartId != '') {

                $fields = 'item_id AS item_id';
                $cartTrakingInfo = Doctrine::getTable('CartItemsTransactions')->getCartItemInfoByCartId($cartId, $fields);
                $cartTrakingInfoCount = count($cartTrakingInfo);
                for ($j = 0; $j < $cartTrakingInfoCount; $j++) {
                     $item_id = $cartTrakingInfo[$j]['item_id'];
                    if ($item_id != '') {
                        $appIdDetails = Doctrine::getTable('IPaymentRequest')->getAppId($item_id);
                        $appDetails = array(); /* Bug id: 29837 */
                        if ($appIdDetails[0]['passport_id'] != '') {
                            $appDetails['passport_id'] = $appIdDetails[0]['passport_id'];
                        } else if ($appIdDetails[0]['visa_id'] != '') {
                            $appDetails['visa_id'] = $appIdDetails[0]['visa_id'];
                        } else if ($appIdDetails[0]['freezone_id'] != '') {
                            $appDetails['freezone_id'] = $appIdDetails[0]['freezone_id'];
                        } else if ($appIdDetails[0]['visa_arrival_program_id'] != '') {
                            $appDetails['visa_arrival_program_id'] = $appIdDetails[0]['visa_arrival_program_id'];
                        }

                        ## Getting application details...
                        $applicationDetails = Doctrine::getTable('CartItemsTransactions')->getApplicationDetails($appDetails);
                        $this->appDetailsArray[$j] = $applicationDetails;
                    }//End of if($item_id != ''){...
                }//End of for($j=0;$j<$cartTrakingInfoCount;$j++){...
                ## cart id is being encrypted due to pass with post parameter...
                $encriptedCartId = SecureQueryString::ENCRYPT_DECRYPT($cartId);
                $encriptedCartId = SecureQueryString::ENCODE($encriptedCartId);
                $this->getUser()->setAttribute('encriptedCartId', $encriptedCartId);
            }//End of if($cartId != ''){...

            if ($notice) {
                $this->getUser()->setFlash("notice", $errMsg);
            } else {
                $this->getUser()->setFlash("notice", NULL);
                $this->postData = true;
            }//End of if($notice){...
        }//End of if($request->isMethod('post')){...
    }

//End of public function executeFreeMoneyOrder(sfWebRequest $request)...

    /**
     * Application is unbinded and added back to the cart.
     * @param sfWebRequest $request
     */
    public function executeUnbindMoneyOrderDone(sfWebRequest $request) {
        $cartId = $this->getUser()->getAttribute('encriptedCartId');
        $decriptedCartId = SecureQueryString::DECODE($cartId);
        $cartId = SecureQueryString::ENCRYPT_DECRYPT($decriptedCartId);

        if ($cartId != '') {
            ## fetching cart details from cart id...
            $cartIdExist = Doctrine::getTable('CartTrackingNumber')->findByCartId($cartId)->toArray();
            if (!empty($cartIdExist) && isset($cartIdExist[0])) {

                ## Setting 1 means record is soft delete...
                $cartIdDeleted = Doctrine::getTable('CartTrackingNumber')->setTrackingNumberDeleteStatus($cartId, 1);

                if (!$cartIdDeleted) {
                    $msg = 'Error found while application was being unbind.';
                } else {

                    ## Cart tracking autoincrement id...
                    $cart_track_id = $cartIdExist[0]['id'];

                    if (!empty($cart_track_id)) {
                        ## Fetching information about money order...
                        $trackingMoneyOrder = Doctrine::getTable('TrackingMoneyOrder')->findByCartTrackId($cart_track_id)->toArray();

                        if (!empty($trackingMoneyOrder)) {
                            $moneyorderId = $trackingMoneyOrder[0]['moneyorder_id'];
                            $deleteTrackingMoneyOrder = Doctrine::getTable('TrackingMoneyOrder')->deleteDetailByMoneyOrderId($moneyorderId);
                            $deleteMoneyOrder = Doctrine::getTable('MoneyOrder')->deleteDetailById($moneyorderId);
                        }//End of if(!empty($trackingMoneyOrder)){...
                    }//End of if(!empty($cart_track_id)){...

                    $msg = 'Application(s) has been unbind successfully.';
                }
            } else {
                $msg = 'Application(s) not found.';
            }

            ## removing session variable...
            $this->getUser()->getAttributeHolder()->remove('encriptedCartId');

            $this->getUser()->setFlash("notice", $msg);
            $this->redirect('mo_configuration/unbindMoneyOrder');
        }//End of if($cartId != ''){...
        else{
            $this->getUser()->setFlash("notice", 'Application(s) already has been unbind.');
            $this->redirect('mo_configuration/unbindMoneyOrder');
        }
    }//End of public function executeUnbindMoneyOrder(sfWebRequest $request)...


    /**
     * This function is used to associate tracking number...
     * @param sfWebRequest $request
     * @return <type>
     */
    public function executeAssociateTrackingNumber(sfWebRequest $request) {

        $items = $this->getUser()->getCartItems();
        if(count($items) < 1){
            $this->getUser()->setFlash("notice", 'Application(s) already has been associated with money order.');
            $this->redirect('cart/list#msg');
        }

        /**
        * [WP: 085] => CR:124
        * Fetching requestId from session variable...
        * If requestId does not exist, user redirected to cart page with tempering message...
        */
        $this->requestId = $this->getUser()->getAttribute('requestId'); //base64_decode($request->getParameter('requestId'));
        Functions::checkRequestIdTempering($this->requestId);

        $paymentOptions = new paymentOptions();
        $this->processingCountry = $paymentOptions->arrProcessingCountry[count($paymentOptions->arrProcessingCountry)-1];

        if($this->processingCountry == 'GB'){
            $currency = 'pound';
        }else{
            $currency = 'dollar';
        }               
        
        $orderRequest = Doctrine::getTable('OrderRequestDetails')->find($this->requestId);        
        $fromCurrency = 1;
        $toCurrency = 5;
        $cartTrakingInfo = array();
        ## Get Application on NIS
        $appDetails = CartItemsTransactionsTable::getInstance()->getRetryIdDetailsOnNis($orderRequest->getRetryId());
        $appIdArr = array();
        if(count($appDetails) > 0){
            $itemIds = array();
            $appAmount = 0;
            $additionalAmount = sfConfig::get('app_visa_arrival_additional_charges');
            $additionalVisaAmount = sfConfig::get('app_visa_additional_charges');

            $visa_additional_charges_on = sfConfig::get('app_visa_additional_charges_flag');

            foreach($appDetails AS $appDetail){
                $appId = $appDetail['id'];
                $appType = strtolower($appDetail['app_type']);

                $visa_additional_charges_flag = false;

                if($visa_additional_charges_on){
                    if($appType == 'visa'){
                        $visa_additional_charges_flag = true;
                    }
                    if($appType == 'freezone'){
                        $isVisaEntryFreezone = Functions::isVisaEntryFreezone($appId);
                        if($isVisaEntryFreezone){
                            $visa_additional_charges_flag = true;
                        }
                    }
                }

                /**
                 * [WP: 087] => CR:126
                 * Fetching application amount and application additional charges and converted them into pound currency...
                 */
                if($currency == 'pound'){

                    if($appType == 'vap'){
                        ## Fetching application amount without additional charges in dollar...
                        $amountFee = Functions::getVapApplicationFeesForMO($appId, $appType);
                        ## Converted dollar amount into the pound currency...
                        $amountFee = CurrencyManager::getConvertedAmount($amountFee, $fromCurrency, $toCurrency);
                        ## Converted additional dollar amount into the pound currency...
                        $additionalAmount  = CurrencyManager::getConvertedAmount($additionalAmount,$fromCurrency,$toCurrency);
                        ## Adding application amount and application additional amount...
                        $totalAppAmount = $amountFee + $additionalAmount;
                        ## Sum of total application amount...
                    }else if($visa_additional_charges_flag){
                        ## Converted additional dollar amount into the pound currency...
                        $additionalAmount  = CurrencyManager::getConvertedAmount($additionalVisaAmount,$fromCurrency,$toCurrency);
                        $amountFee = Functions::getVisaApplicationFeesWithoutAdditionalCharges($appId,$appType);
                        $amountFee = CurrencyManager::getConvertedAmount($amountFee, $fromCurrency, $toCurrency);
                        $totalAppAmount = $amountFee + $additionalAmount;
                    }else{
                        ## Fetching application amount without additional charges in dollar...
                        $amountFee = Functions::getApplicationFees($appId, $appType);
                        $totalAppAmount = CurrencyManager::getConvertedAmount($amountFee, $fromCurrency, $toCurrency);
                    }

                    
                    $appAmount = $appAmount + $totalAppAmount;
                }//End of if($currency == 'pound'){...

                $appIdArr[] = $appId;

                switch($appType){
                    case 'passport':
                        $appIdArray = Doctrine::getTable('IPaymentRequest')->findByPassportId($appId);
                        break;
                    case 'visa':
                        $appIdArray = Doctrine::getTable('IPaymentRequest')->findByVisaId($appId);
                        break;
                    case 'freezone':
                        $appIdArray = Doctrine::getTable('IPaymentRequest')->findByFreezoneId($appId);
                        break;
                        break;
                    case 'vap':
                        $appIdArray = Doctrine::getTable('IPaymentRequest')->findByVisaArrivalProgramId($appId);
                        break;
                    default:
                        $appIdArray = array();
                        break;
                }//End of switch(strtolower($appType)){...

                foreach($appIdArray AS $key => $value){
                    $itemIds[] = $value['id'];
                }//End of foreach($appIdArray AS $key => $value){...

            }//End of foreach($appDetails AS $appDetail){...

            $cartIdDetails = Doctrine::getTable('CartItemsTransactions')->getCartItemInfo($itemIds);

            $cartIds = array();
            foreach($cartIdDetails AS $key => $value){
                $cartIds[] = $value['cart_id'];
            }

            if(count($cartIds) > 0){
                $cartIds = array_unique($cartIds);
            }

            $cartTrakingInfo = Doctrine::getTable('CartTrackingNumber')->getTrackingNumber($cartIds);
        }
        
        
        if(count($cartTrakingInfo) > 0){
            $this->getUser()->setFlash("notice", "Tracking number has already been associated with money order.");
            $this->redirect("cart/list");
        }
        


        $this->form = new MoneyorderForm();
        $this->isValid = false;

        

        $requestDetails = OrderRequestDetailsTable::getInstance()->getOrderRequestDetails($this->requestId);
        if($currency == 'pound'){
            $this->totalAppsAmount = $appAmount; //PaymentModeManager::convertRate($requestDetails['OrderRequest']['amount'], $this->processingCountry);
            $currencyId = '5';
        }else{
            $this->totalAppsAmount = $requestDetails['OrderRequest']['amount'];
            $currencyId = '1';
        }

        $this->userid = $this->getUser()->getGuardUser()->getId();
        
        if ($request->isMethod('post')) {
            $form_val = $request->getParameter($this->form->getName());

                if ($this->totalAppsAmount == $form_val['amount']) {
                
                    $this->form->bind($form_val);
                    if ($this->form->isValid()) {
                        $con = Doctrine_Manager::connection();
                        try {
                            $con->beginTransaction();
                            
                            ## Inserting tracking number...
                            $trackingNumberObj = new CartTrackingNumber();
                            $trackingNumberObj->setCartId($requestDetails['OrderRequest']['transaction_number']);
                            $trackingNumberObj->setOrderRequestDetailId($this->requestId);
                            $trackingNumberObj->setCartAmount($requestDetails['OrderRequest']['amount']);
                            $trackingNumberObj->setCurrency($currencyId);
                            if($currency == 'pound'){
                                $trackingNumberObj->setConvertAmount($form_val['amount']);                                
                            }                            
                            $trackingNumberObj->setUserId($this->getUser()->getGuardUser()->getId());
                            $trackingNumberObj->save();

                            ## Getting tracking number...
                            $trackingId = $trackingNumberObj->id;
                            $trackingNumber = PaymentGatewayHelper::generateTrackingNumber($trackingId);

                            $trackingNumberObj->setTrackingNumber($trackingNumber);
                            $trackingNumberObj->save();


                            ## Saving money order details...
                            $this->form->save();
                            $moneyorderId = $this->form->getObject()->getId();
                            $this->form->getObject()->setCurrency($currencyId);
                            $this->form->getObject()->save();
                            if(!empty($orderRequest)){
                                $orderRequest->setCurrency($currencyId);
                                $orderRequest->save();
                            }
                            $orderRequestObj = Doctrine::getTable('OrderRequest')->find($orderRequest->getOrderRequestId());
                            if(!empty($orderRequestObj)){
                                $orderRequestObj->setCurrency($currencyId);
                                $orderRequestObj->save();
                            }
                            $serviceCharges = Doctrine::getTable('TransactionServiceCharges')->findByOrderRequestDetailId($orderRequest->getId());
                            if(count($serviceCharges) > 0){
                                $serviceChargesObj = $serviceCharges->getFirst();
                                foreach($serviceCharges AS $key => $service){
                                    $obj = Doctrine::getTable('TransactionServiceCharges')->find($service['id']);
                                    $obj->setAppCurrency($currencyId);
                                    $obj->save();
                                }
                            }
                            
                            ## If currency is pound...
                            if($currencyId == '5'){
                                $processingCountry = 'GB';
                                $this->form->getObject()->setAmount($requestDetails['OrderRequest']['amount']);
                                $this->form->getObject()->setConvertAmount($form_val['amount']);
                                $this->form->getObject()->save();
                                /**
                                 * Updating amount in pound into OrderRequest and OrderRequestDetails table...
                                 */
                                if(!empty($orderRequest)){
                                    $orderRequest->setConvertAmount($form_val['amount']);
                                    $orderRequest->setCurrency($currencyId);
                                    $orderRequest->save();
                                 
                                    /**
                                     * Updating order_request table...
                                     */
                                    $orderRequestObj = Doctrine::getTable('OrderRequest')->find($orderRequest->getOrderRequestId());
                                    if(!empty($orderRequestObj)){
                                        $orderRequestObj->setConvertAmount($form_val['amount']);
                                        $orderRequestObj->setCurrency($currencyId);
                                        $orderRequestObj->save();
                                    }


                                    ## Updating converted application amount for each application...                                    
                                    $totalRecord = count($serviceCharges);
                                    if(count($serviceCharges) > 0){
                                        $serviceChargesObj = $serviceCharges->getFirst();                                        
                                        foreach($serviceCharges AS $key => $service){
                                            $serviceCharge = '';
                                            $appAmount = '';

                                            ## If application is vap then get amount without additional charges...
                                            if(strtolower($service['app_type']) == 'vap'){
                                                $serviceCharge = sfConfig::get('app_visa_arrival_additional_charges');
                                                $serviceCharge = CurrencyManager::getConvertedAmount($serviceCharge,$fromCurrency,$toCurrency);

                                                ## Fetching application amount without additional charges in dollar...
                                                $amountFee = Functions::getVapApplicationFeesForMO($service['app_id'], $service['app_type']);
                                                ## Converted dollar amount into the pound currency...
                                                $amountFee = CurrencyManager::getConvertedAmount($amountFee, $fromCurrency, $toCurrency);
                                                ## Converted additional dollar amount into the pound currency...
                                                //$additionalAmount  = CurrencyManager::getConvertedAmount($additionalAmount,$fromCurrency,$toCurrency);
                                                ## Adding application amount and application additional amount...
                                                $totalAppAmount = $amountFee + $serviceCharge;                                                    
                                            }else{
                                                ## Fetching application amount without additional charges in dollar...
                                                $amountFee = Functions::getApplicationFees($service['app_id'], $service['app_type']);
                                                ## Converted dollar amount into the pound currency...
                                                $totalAppAmount = CurrencyManager::getConvertedAmount($amountFee, $fromCurrency, $toCurrency);
                                            }

                                            //$amountFee = Functions::getApplicationFees($service['app_id'], $service['app_type']);
                                            $appAmount = $appAmount + $totalAppAmount;

                                            ## Updating transaction service charge table...
                                            Doctrine::getTable('TransactionServiceCharges')->updateConvertedAmount($orderRequest->getId(), $service['app_type'], $service['app_id'], $appAmount, $currencyId, $serviceCharge);

                                        }//End of foreach($serviceCharges AS $key => $service){...

                                    }//End of if(count($serviceCharges) > 0){...

                                }//End of if(!empty($orderRequestDetailsObj)){...
                                
                            }//End of if($currency == 'pound'){...

                            $trackingArr[] = array($trackingId, $this->requestId);

                            ## Saving Tracking Money Order...
                            $paymentManager = new PaymentManager();
                            $isMoneyordered = $paymentManager->moneyorder($trackingArr, $moneyorderId, $form_val);

                            

                            $con->commit();

                            ## Empty cart after successfully association tracking number...
                            $cartItems = $this->getUser()->clearCart();
                            $userDetailObj = $this->getUser()->getGuardUser()->getUserDetail();
                            $userDetailObj->setCartItems(NULL);
                            $userDetailObj->save();

                            //$this->getUser()->setFlash("notice", "Your Money Order is successfully associated with Tracking number.");

                            $encriptedMoneyorderId = SecureQueryString::ENCRYPT_DECRYPT($moneyorderId);
                            $encriptedMoneyorderId = SecureQueryString::ENCODE($encriptedMoneyorderId);
                            
                            ## send mail to user
                            $mailUrl = "notification/trackingMoneyOrderMail";

                            $mailTaskId = EpjobsContext::getInstance()->addJob('TrackingMoneyOrderMail', $mailUrl, array('money_order_id' => $moneyorderId, 'user_id' => $this->getUser()->getGuardUser()->getId()));


                            $this->getUser()->setFlash("notice", "Your have successfully generated Tracking Number: ".$trackingNumber);
                            $this->redirect("mo_configuration/moneyOrderDetails?id=" . $encriptedMoneyorderId);
                            
                        } catch (Exception $e) {
                            try {
                                $con->rollback();
                                $this->getUser()->setFlash("notice", "There is some error, please verify Money Order and do again.");
                            } catch (Exception $e) {

                            }
                            $this->redirect("mo_configuration/associateTrackingNumber");
                        }
                    }else {
                 $this->form_val = $form_val;
                    }
                } else {
                    $this->getUser()->setFlash("error", "Amount is invalid. Please check amount.");
                    return;
                }            
        }//End of if ($request->isMethod('post')) {...
    }
}
