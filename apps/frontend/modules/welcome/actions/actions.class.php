<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */




/**
 * Description of actionsclass
 *
 * @author akumar1
 */
class welcomeActions extends sfActions {

  public function executeIndex(sfWebRequest $request) {
    $userId = sfContext::getInstance()->getUser()->getAttribute('user_id', null, 'sfGuardSecurityUser');

    //$this->getPaymentHistory($userId);
    
    $page = 1;
    if($request->hasParameter('page')) {
      $page = $request->getParameter('page');
    }
    $this->ipayNisDetailsQuery = $this->getPaymentHistory($userId, $page);
    
    //user details
    $this->userdetails = $user_detail = Doctrine::getTable('UserDetail')->findByUserId($userId)->getFirst();
    $this->group = $groups = $this->getUser()->getGroupDescription();

    //check if user have some money order tracking number
    $status = 'New';
    $this->trackingNumberDetails = array(); //Doctrine::getTable('CartTrackingNumber')->getTrackingNumbersByStaus($userId,$status);
    

    ## Fetching wire transfer pending tracking numbers...
    $status = 'New';
    $this->wtTrackingNumberDetails = Doctrine::getTable('WireTrackingNumber')->getTrackingNumbersByStaus($userId,$status);
  }
  public function executeOpenInfoBox()
    {
        $this->setTemplate('openInfoBox');
        $this->setLayout(false);
    }
  public function executeTrackingInfoBox()
    {
        $userId = sfContext::getInstance()->getUser()->getAttribute('user_id', null, 'sfGuardSecurityUser');

        ## Fetching money order pending tracking numbers...
        $status = 'New';
        $this->trackingNumberDetails = Doctrine::getTable('CartTrackingNumber')->getTrackingNumbersByStaus($userId,$status);
        $moTrackingCount = count($this->trackingNumberDetails);


        ## Fetching wire transfer pending tracking numbers...
        $status = 'New';
        $this->wtTrackingNumberDetails = Doctrine::getTable('WireTrackingNumber')->getTrackingNumbersByStaus($userId,$status);
        $wtTrackingCount = count($this->wtTrackingNumberDetails);


        $this->isMo = false;
        $this->isWt = false;
        $this->isBoth = false;
        $this->defaultDiv = '';
        if($moTrackingCount > 0){
            $this->isMo = true;
            $this->defaultDiv = 'money';
        }
        if($wtTrackingCount > 0){
            $this->isWt = true;
            $this->defaultDiv = 'wire';
        }
        if($moTrackingCount > 0 && $wtTrackingCount > 0){
            $this->isBoth = true;

            if($moTrackingCount >= $wtTrackingCount ){
                $this->defaultDiv = 'money';
            }else{
                $this->defaultDiv = 'wire';
            }
        }

        $this->setTemplate('trackingInfoBox');
        $this->setLayout(false);

    }

    public function executePaymentHistoryAjax(sfWebRequest $request){
        $userId = sfContext::getInstance()->getUser()->getAttribute('user_id', null, 'sfGuardSecurityUser');
        $page = 1;
        if($request->hasParameter('page')) {
          $page = $request->getParameter('page');
        }
        $this->ipayNisDetailsQuery = $this->getPaymentHistory($userId, $page);
        $this->setLayout(false);        
    }

    private function getPaymentHistory($userId, $page){
        //get pending application in cart
        $cartItems = $this->getUser()->getCartItems();
        $this->countCartPending = count($cartItems);
        //purchase history
        $paidDate = date("Y-m-d", mktime(date("H"), date("i"), date("s"), date("m")-1, date("d"), date('Y')));
        $purchaseHistoryQuery = Doctrine::getTable('ipay4meOrder')->getLastMonthPurchaseHistory($userId, $paidDate);

        $this->page = $page;        
        $this->pager = new sfDoctrinePager('ipay4meOrder',5);
        $this->pager->setQuery($purchaseHistoryQuery);
        $this->pager->setPage($this->getRequestParameter('page',$this->page));
        $this->pager->init();

        $purchaseHistory = $this->pager->getResults()->toArray();        
        
        $ipayNisDetailsQuery = array();
        foreach($purchaseHistory as $key => $val){
          $ipayNisDetailsQuery[$key]['order_number'] = $val['request_order_number'];
          $ipayNisDetailsQuery[$key]['order_request_detail_id'] = $val['OrderRequestDetails']['id'];
          $ipayNisDetailsQuery[$key]['currency'] = $val['OrderRequestDetails']['currency'];
          $ipayNisDetailsQuery[$key]['app_detail'] = Doctrine::getTable('CartItemsTransactions')->getCartDetailsQuery($val['request_order_number'])->execute()->toArray();
        }        

        return $ipayNisDetailsQuery;
    }


  
}
?>
