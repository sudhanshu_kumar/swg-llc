<?php use_helper('Form');?>
<?php use_helper('Url');?>
<?php use_stylesheet('style.css');
echo include_stylesheets();
echo include_javascripts(); ?>
<style>
    .hand{cursor:pointer;}
    .activeTab{
        background-color: #2eb32c; /*green color*/
        color: #FFFFFF; /* white color */
    }
    .activeContainer{
        background-color: #2eb32c; /* green color */
    }

    .inactiveTab{
        background-color: #CCCCCC; /*gray color*/
        color: #000000; /* black color */
        cursor: pointer;
    }
</style>

<div class="clearfix">
<div id="nxtPage">
<table width="100%" border="0" align="center">

<!--tr>
    <td class="blbar" colspan="3" align="right">
    <div style="float:left">Pending Tracking Number</div>
    </td>
</tr-->

<?php $colspan = ($isBoth)?'2':'1'; ?>
<tr>
    <td colspan="3">
        <table width="100%">
            <tr>
                <?php if($isMo) { ?>
                <td id="td_moneyID" style="border-bottom:none;text-align:center"  <?php if($isBoth){ ?> onclick="showhide('moneyID');" <?php } ?>><b>Money Order</b></td>
                <?php } ?>
                <?php if($isWt) { ?>
                <td id="td_wireID" style="border-bottom:none;text-align:center"<?php if($isBoth){ ?> onclick="showhide('wireID');" <?php } ?>><b>Wire Transfer</b></td>
                <?php } ?>
            </tr>
            <?php if($isMo) { ?>
            <tr id="moneyID">
                <td colspan="<?php echo $colspan; ?>">
                    <table>
                        <tr>
                            <td width="5%"><span class="txtBold">S. No.</span></td>
                            <td width="20%"><span class="txtBold">Tracking Number</span></td>
                            <td width="20%"><span class="txtBold">Amount</span></td>
                        </tr>

                            <?php $j = 1;
                           
                                foreach($trackingNumberDetails as $detail){
                                ?>

                                <tr>
                                    <td width="5%"><?php echo $j;?></td>
                                    <td width="20%"><span><?= $detail['tracking_number'] ?></span></td>
                                    <td width="20%"><span><?= format_amount($detail['cart_amount'],1) ?></span></td>


                                </tr>
                                <?php $j++;} ?>

                         <tr>
                            <td colspan="3">
                                <br />
                                <div class="lblButton" style="text-align:center;padding-left:65px">
                                    <input value='Go To Associate Money Order' type="button" class ='button' onclick="parent.location='<?php echo url_for('mo_configuration/saveMoneyorder');?>'">
                                </div>
                                <div class="lblButtonRight" style="margin-right:5px;">
                                    <div class="btnRtCorner"></div>
                                </div>

                                <div class="divBlock" style="padding-right:65px;">
                                    <input style="" type="button" class="loginbutton"  id="multiFormSubmit" value="Close" onclick="parent.emailwindow.hide();">
                                </div>
                                <br />
                            </td>
                         </tr>
                    </table>
                </td>
            </tr>
            <?php }//End of if($isMo) { ... ?>
            <?php if($isWt) { ?>
             <tr id="wireID" style="display:<?php if($isBoth) { ?>none<?} ?>" >
                <td colspan="<?php echo $colspan; ?>">
                    <table>
                        <tr>
                            <td width="5%"><span class="txtBold">S. No.</span></td>
                            <td width="20%"><span class="txtBold">Tracking Number</span></td>
                            <td width="20%"><span class="txtBold">Amount</span></td>
                        </tr>

                            <?php $j = 1;
                            foreach($wtTrackingNumberDetails as $detail){


                            ?>

                            <tr>
                                <td width="5%"><?php echo $j;?></td>
                                <td width="20%"><span><?= $detail['tracking_number'] ?></span></td>
                                <td width="20%"><span><?= format_amount($detail['cart_amount'],1) ?></span></td>


                            </tr>
                            <?php $j++;}

                         ?>
                          <tr>
                            <td colspan="3"><br />
                                <div class="lblButton" style="text-align:center;padding-left:65px">
                                    <input value='Go To Associate Wire Transfer' type="button" class ='button' onclick="parent.location='<?php echo url_for('wt_configuration/saveWiretransfer');?>'">
                                </div>
                                <div class="lblButtonRight" style="margin-right:5px;">
                                    <div class="btnRtCorner"></div>
                                </div>

                                <div class="divBlock" style="padding-right:65px">
                                    <input style="" type="button" class="loginbutton"  id="multiFormSubmit" value="Close" onclick="parent.emailwindow.hide();">
                                </div><br />
                            </td>
                         </tr>
                    </table>
                </td>
            </tr>
            <?php }//End of if($isWt) { ... ?>
        </table>
    </td>
</tr>
</table>
</div>
</div>
<script>
    function showhide(id){
        if(id == 'moneyID'){
            /* Active tab setting...*/
            $("#td_"+id).removeClass("inactiveTab");
            $("#td_"+id).addClass("activeTab");
            /* Active container box setting...*/
            $("#"+id).addClass("activeContainer");
            $('#'+id).show();
            /* Inactive tab setting...*/
            $("#td_wireID").removeClass("activeTab");
            $("#td_wireID").addClass("inactiveTab");
            $('#wireID').hide();
        }else{
            /* Active tab setting...*/
            $("#td_"+id).removeClass("inactiveTab");
            $("#td_"+id).addClass("activeTab");
            /* Active container box setting...*/
            $("#"+id).addClass("activeContainer");
            $('#'+id).show();
            /* Inactive tab setting...*/
            $("#td_moneyID").removeClass("activeTab");
            $("#td_moneyID").addClass("inactiveTab");
            $('#moneyID').hide();
        }
    }//End of function showhide(id){...

    var defaultDiv = '<?php echo $defaultDiv;?>';
    if(defaultDiv == 'wire'){
        showhide('wireID');
    }else{
        showhide('moneyID');
    }
</script>