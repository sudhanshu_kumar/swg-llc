<?php use_helper('Pagination'); ?>
<table width="100%" cellpadding="0px" cellspacing="0px" class="dshbrd_table">
        <tr>
          <td valign="top" class="tmz-tab-heading"><div class="float_left">Payment History<span class="red">*</span></div>
            <div class="float_right"><span>Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total <b><?php echo $pager->getNbResults(); ?></b> results</span></div></td>
        </tr>
        <tr>
          <td valign="top" class="grey_bg_table"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                <td valign="top" class="no_border"><table width="100%" cellspacing="0" cellpadding="0" class="dsbrd_listing">
                    <tr class="dsbrd_listing_headings">
                      <td width="50" class="border-right"><span class="txtBold">S. No.</span></td>
                      <td width="100" class="border-right"><span class="txtBold">Application Type</span></td>
                      <td width="200" class="border-right" ><span class="txtBold">Applicant Name</span></td>
                      <td width="100" class="border-right"><span class="txtBold">Application Id</span></td>
                      <td width="120" class="border-right" align="right"><span class="txtBold">Price&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></td>
                      <td width="30" class="border-right" align="center"><span class="txtBold">Action</span></td>
                                </tr>
                  </table></td>
                    </tr>
                    <tr>
                <td valign="top" class="padding_margin_zero"><?php
                            $browserName = $_SERVER['HTTP_USER_AGENT'];
                            if(stripos($browserName, 'msie') === false){
                                $lastTdWidth = (count($ipayNisDetailsQuery) > 4)?85:100;
                            }else{
                                $lastTdWidth = 100;
                            }
                            //echo $lastTdWidth;
                            ?>
                  <div class="listing_scroll">
                    <table width="100%" cellspacing="0" cellpadding="0" class="dsbrd_listing">
                                   <?php
                                    $i=$pager->getFirstIndice();
                                    $total_amount = 0;
                                       foreach ($ipayNisDetailsQuery as $ipayNisDetails){
                                           $visa_additional_charges = false;
                                           $temp = '';
                                           foreach ($ipayNisDetails['app_detail'] as $appDetails){
                                              $appObj = CartItemsTransactionsTable::getInstance();
                                              $app['passport_id'] = $appDetails['passport_id'];
                                              $app['visa_id'] = $appDetails['visa_id'];
                                              $app['freezone_id'] = $appDetails['freezone_id'];
                                              $app['visa_arrival_program_id'] = $appDetails['visa_arrival_program_id'];
                                              $appDetails = $appObj->getApplicationDetails($app);
                                              $appAmount = $appDetails['amount'];
                                              $total_amount = $total_amount+$appAmount;

                                              $encryptAapplicationId = SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT($appDetails['id']));
                                              
                                              if($appDetails['app_type']=='visa')
                                              {

                                                $visa_additional_charges = true;

                                                $referenceNumber = Doctrine::getTable('VisaApplication')->findBy('id', $appDetails['id']);
                                                $appType = 'Visa';
                                                /**
                                                 * [WP: 096]
                                                 */
                                                ##$printUrl = url_for("visa/printVisaApp?id=".SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT($appDetails['id'])));
                                                $printUrl = Functions::printApplicationURL('visa', $encryptAapplicationId);
                                              }
                                              if($appDetails['app_type']=='freezone')
                                              {
                                                $referenceNumber = Doctrine::getTable('VisaApplication')->findBy('id', $appDetails['id']);
                                                if($referenceNumber[0]['visacategory_id'] == 102){
                                                  $appType = 'Rentry Freezone';
                                                  /**
                                                   * [WP: 096]
                                                   */
                                                  ##$printUrl = url_for("visa/printFreezone?id=".SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT($appDetails['id'])));
                                                  $printUrl = Functions::printApplicationURL('reentryfreezone', $encryptAapplicationId);
                                                }else{
                                                  $appType = 'Entry Freezone';
                                                  /**
                                                   * [WP: 096]
                                                   */
                                                  ##$printUrl = url_for("visa/printVisaApp?id=".SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT($appDetails['id'])));
                                                  $printUrl = Functions::printApplicationURL('entryfreezone', $encryptAapplicationId);

                                                  $visa_additional_charges = true;
                                                  
                                                }
                                              }
                                              if($appDetails['app_type']=='passport')
                                              {
                                                $referenceNumber = Doctrine::getTable('PassportApplication')->findBy('id', $appDetails['id']);
                                                $appType = 'Passport';
                                                /**
                                                 * [WP: 096]
                                                 */
                                                ##$printUrl = url_for("passport/showPrintSuccess?id=".SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT($appDetails['id'])));
                                                $printUrl = Functions::printApplicationURL('passport', $encryptAapplicationId);
                                              }
                                              if($appDetails['app_type']=='vap')
                                              {
                                                $referenceNumber = Doctrine::getTable('VapApplication')->findBy('id', $appDetails['id']);
                                                $appType = 'Visa on Arrival';
                                                $printUrl = url_for("visaArrival/printVisaApp?id=".SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT($appDetails['id'])));
                                                $appAmount = $appAmount + sfConfig::get('app_visa_arrival_additional_charges');
                                              }

                                              ## Getting application amount...
                                              $appAmountDetails = Doctrine::getTable('TransactionServiceCharges')->getApplicationDetails($appDetails['id'], $appDetails['app_type'], $ipayNisDetails['order_request_detail_id']);                                              

                                              if(count($appAmountDetails) > 0){
                                                  if($appAmountDetails[0]['app_currency'] == '1'){
                                                      $appAmount = $appAmountDetails[0]['app_amount'];
                                                      $additional_charges = $appAmountDetails[0]['service_charge'];
                                                  }else{
                                                      $appAmount = $appAmountDetails[0]['app_convert_amount'];
                                                      $additional_charges = $appAmountDetails[0]['app_convert_service_charge'];
                                                  }
                                              }else{                                                  
                                                  $additional_charges = sfConfig::get('app_visa_arrival_additional_charges');                                                  
                                              }
                                              
                                              $currencySymbol = PaymentModeManager::currencySymbol($ipayNisDetails['currency']);

                                              ?>
                                               <?php if($temp == '' && $temp != $ipayNisDetails['order_number']) { ?>
                      <tr class="border_top">
                        <td width="45" align="left" class="dsbrd_listing border_top"><?php echo $i++; ?></td>
                        <td colspan="5" class="dsbrd_listing border_right">Order Number: <strong><?php echo $ipayNisDetails['order_number'];?></strong></td>
                                                </tr>
                                                <?php } $temp = $ipayNisDetails['order_number']; ?>
                      <tr class="border_top">
                        <td width="45" valign="top" align="left" class="border_right">&nbsp;</td>
                        <td width="100" valign="top" align="left" class="border_right"><?php echo $appType;?></td>
                        <td width="200" valign="top" align="left" class="border_right" ><?php echo $appDetails['name']; ?></td>
                        <td width="100" valign="top" align="left"><?php echo $appDetails['id'];?></td>
                        <td width="120" valign="top" align="right" class="border_right"><?php if($appDetails['app_type'] =='vap') { echo format_amount($appAmount,1,0,$currencySymbol) ;} else { echo format_amount($appAmount,1,0,$currencySymbol); } ?>
                        <?php
                            if($appDetails['app_type'] =='vap' || $visa_additional_charges){
                                if((int)$additional_charges > 0){
                                    //echo '<i><p style="font-size:10px;text-align:right">(Inclusive service charge: '.html_entity_decode($currencySymbol).$additional_charges.')</p></i>';
                                    echo Functions::getAdditionalText($additional_charges, $currencySymbol);
                                }
                            }
                            ?>
                        </td>
                        <td width="30" valign="top" align="center"><span onclick='openPopUp("<?php echo $printUrl?>");' style="text-decoration:underline;color:green;cursor:pointer;"><img src="<?php echo image_path('print_icon.png'); ?>" alt="Print" title="Print" /></span></td>
                                                </tr>
                                              <?php
                                              }
                                        } ?>
                                        <?php if($total_amount > 0){ ?>
                                          <!--tr>
                        <td colspan="4" align="right" class="tmz-strong border_top" >Total:</td>
                        <td  align="right" class="dsbrd_listing border_top"><strong><?php //echo format_amount($total_amount,1);?></strong></td>
                        <td class="border_top" >&nbsp;</td>
                                          </tr-->
                                          <?php }else{ ?>
                                          <tr>
                        <td  colspan="6" align="center" ><div class="red"> No record found</div></td>
                                          </tr>
                                          <?php } ?>
                            </table>
                            </div>
                            <div>
                    <!--div style="float:left"><span>Showing <b><span id ="firstRecord"><?php echo $pager->
                    getFirstIndice() ?></span></b> - <b><span id = "lastRecord"><?php echo $pager->getLastIndice() ?></span></b> of total <b><span id = "totalRecord"><?php echo $pager->getNbResults(); ?></span></b> results</span>
                    </div-->
                    <div class = "paging pagingFoot float_right" id = "footPaging">
                                    <?php  echo ajax_pager_navigation($pager, url_for($sf_context->getModuleName().'/paymentHistoryAjax'), 'paymentHistoryDataDiv') ?>
                               </div>
                  </div></td>
                    </tr>
            </table></td>
        </tr>
    </table>