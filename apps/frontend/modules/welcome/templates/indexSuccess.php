<?php 
use_helper('Pagination');
echo ePortal_pagehead(" "); ?>
<?php
use_javascript('dhtmlwindow.js');
use_stylesheet('dhtmlwindow.css');
use_javascript('modal.js');
use_stylesheet('modal.css');
?>
<script>



function openInformationWindow(){
        emailwindow=dhtmlmodal.open('EmailBox', 'iframe', 'openInfoBox', 'Confirmation Message', 'width=530px,height=335px,center=1,border=0, scrolling=no');

        emailwindow.onclose=function(){
            var theform=this.contentDoc.forms[0]
            var theemail=this.contentDoc.getElementById("info_check")
            if (!theemail.checked){
                alert("Please confirm that you have read the information by seleting the checkbox");
                return false
            }
            else{
                return true;
            }
        }
    }

function trackingInfoBox(H){
        var height = 150 + (25*H);
        emailwindow=dhtmlmodal.open('EmailBox', 'iframe', 'trackingInfoBox', 'Pending Tracking Number', 'width=530px,height='+height+',center=1,border=0, scrolling=no');

        emailwindow.onclose=function(){
            var theform=this.contentDoc.forms[0]
            var theemail=this.contentDoc.getElementById("info_check")

            if (!theemail.checked){
                alert("Please confirm that you have read the information by seleting the checkbox");
                return false
            }
            else{
                return true;
            }
        }
    }


</script>
<?php
$trackingFlag = false;
$heightMO = 0;
$heightWT = 0;
if(!empty($trackingNumberDetails) && count($trackingNumberDetails) > 0){
    $heightMO = count($trackingNumberDetails);
    $trackingFlag = true;
}
if(!empty($wtTrackingNumberDetails) && count($wtTrackingNumberDetails) > 0){
    $heightWT = count($wtTrackingNumberDetails);
    $trackingFlag = true;
}


if($trackingFlag){
    if($heightMO != '' && $heightWT != ''){
        $height = ($heightMO > $heightWT)?$heightMO:$heightWT;
    }else{
        $height = ($heightMO != '')?$heightMO:$heightWT;
    }
    //echo "<script>window.onload = trackingInfoBox($height);</script>";
}




?>
<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">
    <?php
$name = 'Welcome to the Dashboard';
include_partial('global/innerHeading',array('heading'=>$name));
$is_ewallet_active = Settings::isEwalletActive();
if($userdetails['kyc_status'] == '0' && $group[0] == 'payment_group' && $is_ewallet_active)
{
    echo "<script>window.onload = openInformationWindow();</script>";
}

?>
    <?php if($group[0] == 'Payment Group'){ ?>
      <div class="clear"></div>
<div class="tmz-spacer"></div>
  <div class="listingBox" style="display:none;">
    <div class="topCorner_new"></div>
	<div class="inst_container">
    <ul>
	<div class="inst_heading">Instructions</div>
	<div class="clear"></div>
      <li>If you have already filled NIS Application, Please Click "CART" on upper right corner to proceed for payment.</li>
      <li>To start filling your NIS Application go to <a href="<?php echo url_for('nis/newApplication');?>">Application >> NIS Application </a></li>
      <li>To see acknowledgment slip after payment, go to <a href="http://portal.immigration.gov.ng/visa/OnlineQueryStatus" target="NEW">NIS Portal</a> with your Application Id and Reference Number.</li>
        <li>To contact support team, please send email to <a href="mailto:support@swgloballlc.com">support@swgloballlc.com</a> or call +1 877-693-1919 or +1 877-693-1922.</li>
        </ul>
        <div class="inst_img"></div>
      </div>
	<div class="clear"></div>
    <div class="btmCorner_new"></div>
    </div>
    <?php } ?>
<?php if($group[0] == 'Payment Group') { ?>
    <div class="div_box_bg">
      <div class="cart_info_container">
        <div class="div_box_heading">Pending Applications Info in the Cart</div>
        <img src="<?php echo image_path('ico-info.png'); ?>" alt="information" class="info"/>
        <div class="pending_app">
          <div class="cart_info">You have <span class="highlight-red tmz-strong"> <?php echo $countCartPending;?> pending application(s)</span> in the</div>
          <div class="cart_pic"><a class="tmz-link tmz-strong" href="<?php echo url_for('cart/list');?>"><img src="<?php echo image_path('ico-cart.png'); ?>" alt="CART" /></a></div>
          <div class="clear"></div>
          <div>Please <a class="tmz-link tmz-strong" href="<?php echo url_for('cart/list');?>">click here</a> to proceed for payments.</div>
        </div>
      </div>
      <div class="separator">&nbsp;</div>
      <div class="app_info_container">
        <div class="div_box_heading">Steps for filling NIS Application</div>
        <ul class="steps_nis_app">
          <li> To start filling your NIS Application go to <br />
            <a class="tmz-link tmz-strong" href="<?php echo url_for('nis/newApplication');?>">Application >> NIS Application </a> </li>
          <li> To contact support team, please send email to <a class="tmz-link tmz-strong" href="mailto:support@swgloballlc.com">support@swgloballlc.com</a> or call +1 877-693-1919 or +1 877-693-1922. </li>
        </ul>
      </div>
    </div>
  <!-- ########### START APPLICATION IN CART ############## -->
  <div class="clear tmz-spacer"></div>
  <?php 
  $items = $sf_user->getCartItems();
  $colspan = 7;
  $itemCount = count($items);
  if($itemCount > 0){
    $paymentOptionsObj = new paymentOptions();
    $processingCountry = $paymentOptionsObj->getProcessingCountry();
    $currencyId = CurrencyManager::getDestinationCurrency($processingCountry);


    $isVapExist = Functions::isVapApplicationExistsInCart();

    if($currencyId != 1 && !$isVapExist){
        $currencySymbol = CurrencyManager::currencySymbolByCurrencyId($currencyId);
    }else{
        $currencySymbol = CurrencyManager::currencySymbolByCurrencyId(1);
    }
  }else{
      $currencySymbol = '';
  }
  
  ?>
    <div class="tmz-grid-div">
      <table width="100%" cellpadding="0" cellspacing="0" border="0">
        <tr>
          <td valign="top" class="tmz-tab-heading">Application(s) in Cart</td>
        </tr>
        <tr>
          <td valign="top" class="grey_bg_table">



		  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="">
                    <tr>
                <td valign="top" class="no_border"><table width="100%" cellspacing="0" cellpadding="0" class="dsbrd_listing">
                    <tr class="dsbrd_listing_headings">
                      <td width="50" class="border_right"><span class="txtBold">S. No.</span></td>
                      <td width="100" class="border_right"><span class="txtBold">Application Type</span></td>
                      <td width="180" class="border_right" ><span class="txtBold">Applicant Name</span></td>
                      <td width="100" class="border_right"><span class="txtBold">Application Id</span></td>
                      <td width="100" class="border_right"><span class="txtBold">Reference Number</span></td>
                      <td width="130" style="font-weight:bold!important" align="center">Price(<?php echo $currencySymbol; ?>)</td>
                      <td width="50"><span class="txtBold">Actions</span></td>
                                </tr>
                  </table></td>
                    </tr>
                    <tr>
                <td valign="top" class="padding_margin_zero"><?php
                                    $browserName = $_SERVER['HTTP_USER_AGENT'];
                                    if(stripos($browserName, 'msie') === false){
                                        $lastTdWidth = ($itemCount > 4)?85:100;
                                    }else{
                                        $lastTdWidth = 100;
                                    }
                                    //echo $lastTdWidth;
                                    ?>
                  <div class="listing_scroll">
                    <table width="100%" cellspacing="0" cellpadding="0" class="dsbrd_listing">
                                      <?php
                                          $i = 1;
                                          $total_amount = 0;
                                          $visa_additional_charges_flag = sfConfig::get('app_visa_additional_charges_flag');
                                          foreach ($items as $item) {
                                            $visa_add_charges_flag = false;
                                            $encryptAapplicationId = SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT($item->getAppId()));
                                            if($item->getType()=='Visa')
                                            {
                                                ## Additional charges in dollar for visa...
                                                if($visa_additional_charges_flag){
                                                    $additional_charges = sfConfig::get('app_visa_additional_charges');
                                                    $visa_add_charges_flag = true;
                                                }
                                                $referenceNumber = Doctrine::getTable('VisaApplication')->findBy('id', $item->getAppId());
                                                $appType = 'Visa';
                                                /**
                                                 * [WP: 096]
                                                 */
                                                ##$printUrl = url_for("visa/printVisaApp?id=".SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT($item->getAppId())));
                                                $printUrl = Functions::printApplicationURL('visa', $encryptAapplicationId);
                                            }
                                            if($item->getType()=='Freezone')
                                            {
                                                $referenceNumber = Doctrine::getTable('VisaApplication')->findBy('id', $item->getAppId());
                                                if($referenceNumber[0]['visacategory_id'] == 102){
                                                  $appType = 'Rentry Freezone';
                                                  /**
                                                   * [WP: 096]
                                                   */
                                                  ##$printUrl = url_for("visa/printFreezone?id=".SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT($item->getAppId())));
                                                  $printUrl = Functions::printApplicationURL('reentryfreezone', $encryptAapplicationId);
                                                }else{
                                                  $appType = 'Entry Freezone';
                                                  /**
                                                   * [WP: 096]
                                                   */
                                                  ##$printUrl = url_for("visa/printVisaApp?id=".SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT($item->getAppId())));
                                                  $printUrl = Functions::printApplicationURL('entryfreezone', $encryptAapplicationId);

                                                  ## Additional charges in dollar for visa...
                                                  if($visa_additional_charges_flag){
                                                    $additional_charges = sfConfig::get('app_visa_additional_charges');
                                                    $visa_add_charges_flag = true;
                                                  }
                                                }
                                            }
                                            if($item->getType()=='Passport')
                                            {
                                                $referenceNumber = Doctrine::getTable('PassportApplication')->findBy('id', $item->getAppId());
                                                $appType = 'Passport';
                                                /**
                                                 * [WP: 096]
                                                 */
                                                ##$printUrl = url_for("passport/showPrintSuccess?id=".SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT($item->getAppId())));
                                                $printUrl = Functions::printApplicationURL('passport', $encryptAapplicationId);
                                            }
                                            if($item->getType()=='Vap')
                                            {
                                                $referenceNumber = Doctrine::getTable('VapApplication')->findBy('id', $item->getAppId());
                                                $appType = sfConfig::get('app_visa_arrival_title');
                                                $printUrl = url_for("visaArrival/printVisaApp?id=".SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT($item->getAppId())));

                                                $additional_charges = sfConfig::get('app_visa_arrival_additional_charges');
                                            }
                                            $referenceNumber=$referenceNumber[0]['ref_no'];
                                            $processingCountry = $paymentOptionsObj->getProcessingCountry();
                                            $currencyId = CurrencyManager::getDestinationCurrency($processingCountry);
                                            if($currencyId != 1 && strtolower($item->getType()) != 'vap'){
                                                $currencySymbol = CurrencyManager::currencySymbolByCurrencyId($currencyId);
                                            }else{
                                                $currencySymbol = CurrencyManager::currencySymbolByCurrencyId(1);
                                            }

                                          $amount = $item->getPrice();


                                          /**
                                           * [WP: 102] => CR: 144
                                           * Fetching visa convertd amount if additional charges flag true...
                                           */
                                            if($visa_add_charges_flag){
                                                $applicationFees = Functions::getConvertedVisaAndAdditionalFees($item->getAppId(), $item->getType(), $additional_charges);
                                                if(count($applicationFees) > 0){
                                                    $additional_charges = $applicationFees['additional_charges'];
                                                    $amount = $applicationFees['appAmount'] + $additional_charges ;
                                                }
                                            }//End of if($visa_add_charges_flag){...
                                            ?>
                                            <tr>
                                                <td width="45" valign="top" align="left" class="border-right"><?php echo $i; ?></td>
                                                <td width="100" valign="top" align="left" class="border-right"><?php echo $appType; ?></td>
                                                <td width="180" valign="top" align="left" class="border-right" ><?php echo $item->getName();?></td>
                                                <td width="100" valign="top" align="left" class="border-right"><?php echo $item->getAppId();?></td>
                                                <td width="100" valign="top" align="left" class="border-right"><?php echo $referenceNumber;?></td>
                                                <td width="130" valign="top" align="right" class="border-right"><?php echo format_amount($amount); ?>
                                                <?php if(strtolower($item->getType()) == 'vap' || $visa_add_charges_flag){                                                      
                                                      echo Functions::getAdditionalText($additional_charges, $currencySymbol);
                                                } ?>
                                                </td>
                                                <td width="50" class="border-right"><a href="<?php echo url_for('cart/remove?id='.$item->getAppId().'&type='.$item->getType().'&home=home'); ?>"><img src="<?php echo image_path('remove_icon.png'); ?>" alt="Remove" title="Remove" /></a>&nbsp;&nbsp;<span onclick='openPopUp("<?php echo $printUrl?>");' style="text-decoration:underline;color:green;cursor:pointer;"><img src="<?php echo image_path('print_icon.png'); ?>" alt="Print" title="Print" /></span></td>
                                            </tr>
                                            <?php
                                           $total_amount = $total_amount+$amount;
                                            $i++;


                                          } ?>
                                          <?php if($total_amount > 0) { ?>
                                          <tr>
                        <td colspan="5" align="right" class="tmz-strong border_top">Total:</td>
                        <td  align="right" class="dsbrd_listing border_top txtBold"><?php echo format_amount($total_amount,1,0,$currencySymbol);?></td>
                        <td class="border_top">&nbsp;</td>
                                          </tr>
                                          <?php }else{ ?>
                                          <tr>
                        <td colspan="7" class="red border_top" align="center" height="50">No pending application in cart.</td>
                                          </tr>
                                          <?php } ?>                                          
                                    </table>
                  </div></td>
                          </tr>
            </table></td>
                </tr>
            </table>
        </div>
    <!-- ########### END APPLICATION IN CART ############## -->
        <div class="clear tmz-spacer"></div>
        <!-- ########### START PAYMENT HISTORY ############## -->
    <div id="paymentHistoryDataDiv" class="tmz-grid-div">
       <!-- ######## INCLUDING PAYMENT HISTORY PARTIAL ##########  -->
      <?php include_partial('paymentHistory', array('pager' => $pager, 'ipayNisDetailsQuery' => $ipayNisDetailsQuery)); ?>
    </div>
	<div class="clear">&nbsp;</div>
    <div class="highlight yellow">
      <p > <b><font color="red">* </font>Note:</b>&nbsp;Only applications paid in the last month are available in Payment History. </p>
   </div>
<!-- ########### END PAYMENT HISTORY ############## -->
   <div class="clear tmz-spacer"></div>
<!-- ########### START MONEY ORDER TRACKING NUMBER ############## -->
  <?php if(count($trackingNumberDetails) > 0){ ?>
    <div class="tmz-grid-div">
      <table width="100%" cellpadding="0px" cellspacing="0px" class="dsbrd_listing">
        <tr>
          <td valign="top" class="tmz-tab-heading"><div class="float_left">Pending Money Order Tracking Number</div>
            <div class="float_right"><span onclick="window.location.href='<?php echo url_for('mo_configuration/saveMoneyorder');?>';" class="hand red" >
			<img src="<?php echo image_path('associate_money_order.gif'); ?>" alt="Associate with Money Order" title="Associate with Money Order" />
			</span></div></td>
        </tr>
        <tr>
          <td valign="top" class="grey_bg_table"><table width="100%" border="0" cellspacing="0" cellpadding="0" class="table_no_border">
                    <tr>
                <td valign="top" class="no_border"><table width="100%" cellspacing="0" cellpadding="0" class="dsbrd_listing">
                    <tr class="dsbrd_listing_headings">
                      <td width="57" class="border_right"><span class="txtBold">S. No.</span></td>
                      <td class="border_right" width="437"><span class="txtBold">Tracking Number</span></td>
                      <td width="190" class="border_right"><span class="txtBold">Creation Date</span></td>
                      <td width="60" class="border_right"><span class="txtBold">Price($)</span></td>
                      <td width="100" class="border_right"><span class="txtBold">Status</span></td>
                                </tr>
                  </table></td>
                    </tr>
                    <tr>
                <td valign="top" class="padding_margin_zero"><?php
                            $browserName = $_SERVER['HTTP_USER_AGENT'];
                            if(stripos($browserName, 'msie') === false){
                                $lastTdWidth = (count($trackingNumberDetails) > 4)?85:100;
                            }else{
                                $lastTdWidth = 100;
                            }
                            //echo $lastTdWidth;
                            ?>
                  <div class="listing_scroll">
                    <table width="100%" cellspacing="0" cellpadding="0" class="dsbrd_listing" >
                               <?php
                                  $i = 1; $total_amount = 0;
                                  foreach ($trackingNumberDetails as $item) { ?>
                                    <tr>
                        <td width="45px;" valign="top" align="left" class="border_right"><?php echo $i; ?></td>
                        <td valign="top" align="left" class="border_right"><?php echo $item['tracking_number']; ?></td>
                        <td width="190px;" valign="top" align="left" class="border_right"><?php echo $item['created_at'];?></td>
                        <td width="60px;" valign="top" align="left" class="border_right"><?php echo $item['cart_amount'];?></td>
                        <td width="<?php echo $lastTdWidth;?>px;" valign="top" align="left" class="border_right"><?php echo $item['status'];?></td>
                                    </tr>
                                   <?php
                                   $total_amount = $total_amount+$item['cart_amount']; $i++; } ?>
                                   <?php if($total_amount > 0) { ?>
                                      <tr>
                        <td colspan="3" align="right" class="tmz-strong border_top">Total:</td>
                        <td align="left" class="dsbrd_listing border_top"><strong><?php echo format_amount($total_amount,1);?></strong></td>
                        <td class="border_top">&nbsp;</td>
                                      </tr>
                                   <?php } ?>
                            </table>
                  </div></td>
                    </tr>
            </table></td>
        </tr>
    </table>
    </div>
  <div class="clear tmz-spacer"></div>
  <?php }//End of if(count($trackingNumberDetails) > 0){... ?>
<!-- ########### END MONEY ORDER TRACKING NUMBER ############## -->
<!-- ########### START WIRE TRANSFER TRACKING NUMBER ############## -->
<?php if(count($wtTrackingNumberDetails) > 0){ ?>
    <div>
      <table width="100%" cellpadding="0px" cellspacing="0px">
        <tr>
          <td valign="top" class="tmz-tab-heading"><div class="float_left">Pending Wire Transfer Tracking Number</div>
            <div class="float_right"><span onclick="window.location.href='<?php echo url_for('wt_configuration/saveWiretransfer');?>';" class="hand" >Associate with Wire Transfer</span></div></td>
        </tr>
        <tr>
          <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0" class="table_no_border">
                    <tr>
                <td valign="top" class="no_border dsbrd_listing"><table width="100%" cellspacing="0" cellpadding="0">
                    <tr class="dsbrd_listing_headings">
                      <td width="57" class="border_right"><span class="txtBold">S. No.</span></td>
                      <td class="border_right" width="437"><span class="txtBold">Tracking Number</span></td>
                      <td width="190" class="border_right"><span class="txtBold">Creation Date</span></td>
                      <td width="60" class="border_right"><span class="txtBold">Amount</span></td>
                      <td width="100" class="border_right"><span class="txtBold">Status</span></td>
                                </tr>
                  </table></td>
                    </tr>
                    <tr>
                <td valign="top" class="padding_margin_zero"><?php
                            $browserName = $_SERVER['HTTP_USER_AGENT'];
                            if(stripos($browserName, 'msie') === false){
                                $lastTdWidth = (count($wtTrackingNumberDetails) > 4)?85:100;
                            }else{
                                $lastTdWidth = 100;
                            }
                            //echo $lastTdWidth;
                            ?>
                  <div class="listing_scroll">
                    <table width="100%" cellspacing="0" cellpadding="0" class="dsbrd_listing">
                               <?php
                                  $i = 1; $total_amount = 0;
                                  foreach ($wtTrackingNumberDetails as $item) { ?>
                                    <tr>
                        <td width="57" valign="top" align="left" class="border_right"><?php echo $i; ?></td>
                        <td valign="top" align="left" class="border_right" width="437"><?php echo $item['tracking_number']; ?></td>
                        <td width="190" valign="top" align="left" class="border_right"><?php echo $item['created_at'];?></td>
                        <td width="60" valign="top" align="left" class="border_right"><?php echo $item['cart_amount'];?></td>
                        <td width="<?php echo $lastTdWidth;?>px;" valign="top" align="left" class="border_right"><?php echo $item['status'];?></td>
                                    </tr>
                                   <?php
                                   $total_amount = $total_amount+$item['cart_amount']; $i++; } ?>
                                   <?php if($total_amount > 0) { ?>
                                      <tr>
                        <td colspan="3" align="right" class="tmz-strong border_top">Total:</td>
                        <td align="left" class="dshbrd_listing border_top"><strong><?php echo format_amount($total_amount,1);?></strong></td>
                        <td class="border_top">&nbsp;</td>
                                      </tr>
                                   <?php } ?>
                            </table>
                  </div></td>
                    </tr>
            </table></td>
        </tr>
    </table>
 </div>
  <div class="clear tmz-spacer"></div>
  <?php }//End of if(count($trackingNumberDetails) > 0){... ?>
<!-- ########### END WIRE TRANSFER TRACKING NUMBER ############## -->
<?php } ?>
<script>
    /* Making both div height same...  */
    $('#tmz_cart_info_id').height($('#tmz_info_id').height());
    function openPopUp(url){
      url = url + '/mode/cart'
      window.open (url, "mywindow","status=1,toolbar=0,width=1000px,height=800px,location=0,menubar=0,scrollbars=1");
     // alert(url);
    }
</script>
  </div>
</div>
<div class="content_wrapper_bottom"></div>
