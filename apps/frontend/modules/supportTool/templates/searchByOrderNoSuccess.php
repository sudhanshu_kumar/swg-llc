
<script type="text/javascript">
  function disableForm(frm){

    if(validate_frm(frm)){
      $('#submit').attr('disabled',true);
      return true;
    }else{
      return false;
    }
  }

function   findDuplicate(appType, appId){
   var url = "<?= url_for("supportTool/findDuplicateNisApplication"); ?>";
   $("#application_div").hide();
   $('#result_div').html('<?php echo image_tag('../images/ajax-loader.gif'); ?>');
   $("#result_div").show();

   $.get(url, { id: appId, appType: appType},
   function(data){

       //$("#flash_notice").html('<span>Bank '+status+' successfully</span>');
       $("#application_div").html(data);
          $("#application_div").show();
          $("#application_div").focus();
          $("#result_div").hide();
       //window.location.href = "URL";
       //setTimeout( "refresh()", 2*1000 );
   });
  }

  function validate_form(){
    var orderNo = $('#order_no').val();
    if(orderNo == ''){
      alert("Please enter SW Global LLC order number");
      $('#order_no').focus();
      return false;
 }
    if(orderNo != ''){
      if(isNaN(orderNo)){
        alert("Please enter only numeric value.");
        $('#order_no').val('') ;
        $('#order_no').focus();
        return false;
      }
    }
  }
</script>

<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">
  <?php
use_helper('Form');
use_helper('Pagination');
include_partial('global/innerHeading',array('heading'=>'Search NIS Application By SW Global LLC Order Number'));
?>
  <div class="clear"></div>
<div class="tmz-spacer"></div>
  <?php echo form_tag('supportTool/searchOrderNo',array('name'=>'order_search_form','class'=>'', 'method'=>'get','id'=>'order_search_form' ,'onsubmit'=>'return validate_form()')) ?>
    <?php
        $sf = sfContext::getInstance()->getUser();
        if($sf->hasFlash('notice')){ ?>
    <div id="flash_notice" class="alertBox" >
      <?php
          echo nl2br($sf->getFlash('notice'));
          ?>
    </div>
    <br/>
    <?php }?>
    <?php
          $sf = sfContext::getInstance()->getUser();
          if($sf->hasFlash('error')){ ?>
    <div id="flash_error" class="alertBox" >
      <?php
            echo nl2br($sf->getFlash('error'));
            ?>
    </div>
    <br/>

    <?php }?>
    <table width="99%" cellpadding="0" cellspacing="0">
      <?php
            $val = '';
            if($sf_params->has('order_no')){
              $val = $sf_params->get('order_no');
            }
            //                die($val);
            echo formRowComplete('Order Number<span class="red">*</span>',input_tag('order_no', $val, array('class'=>'txt-input'))); ?>
              <tr>
        <td>&nbsp;</td>
        <td>
            <?php   echo submit_tag('Search',array('class' => 'normalbutton')); ?>
         
          
            
          </td>
              </tr>
    </table>
		<div class="clear"></div>
    <?php if($isFound) {
        $i = 1;
        ?>
    <?php if($notRefundMessage != '') {?>
    <div class ="alertBox"> <?php echo "Note: ".$notRefundMessage; ?> </div>
    <?php } ?>
    <br/>
     
        <table width="99%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
              <tr>
            <td class="blbar" colspan="12" align="right"><div class="float_left">NIS Application Details By SW Global LLC Order Number</div>
              <span>Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total <b><?php echo $pager->getNbResults(); ?></b> results</span></td>
              </tr>
          <tr>
            <td width="4%" ><span class="txtBold">S. No.</span></td>
            <td width="9%"><span class="txtBold">Application type</span></td>
            <td width="9%"><span class="txtBold">App Id</span></td>
            <td width="12%"><span class="txtBold">Reference No</span></td>
            <td width="14%"><span class="txtBold">Applicant Name</span></td>
            <td width="9%"><span class="txtBold">Date of Birth</span></td>
            <td width="6%"><span class="txtBold">NIS Payment Status</span></td>
            <td width="6%"><span class="txtBold">Paid On</span></td>
            <td width="10%"><span class="txtBold">Email Id</span></td>
            <td width="10%"><span class="txtBold">Order Number</span></td>
            <td width="10%"><span class="txtBold">Payment Gateway</span></td>
            <td width="6%"><span class="txtBold">Actions</span></td>
          </tr>
          <tbody>

            <?php $showButtons = false;
            if($pager->getNbResults()>0)
                  {
//              $finalStatus = $nisHelper->isAllReadyRefunded($sf_params->get('order_no'));
//              $finalStatus= $finalStatus->getRawValue();
//              $fStatus = null;
//              if(isset ($finalStatus) && is_array($finalStatus) && count($finalStatus)>0){
//                $fStatus = $finalStatus['action'];
//              }
              foreach ($pager->getResults() as $result):
              //            echo "<pre>";print_r(get_class_methods($result));die;
              $app['passport_id'] = $result['passport_id'];
              $app['visa_id'] = $result['visa_id'];
              $app['freezone_id'] = $result['freezone_id'];
              $appDetails = $appObj->getApplicationDetails($app);
              //            echo "<pre>";print_R($appDetails);die;
              $finalStatus = $nisHelper->isAllAppReadyRefunded($sf_params->get('order_no'),$appDetails['id']);
              $finalStatus= $finalStatus->getRawValue();
              $fStatus = null;
              if(isset ($finalStatus) && is_array($finalStatus) && count($finalStatus)>0){
                  $fStatus = $finalStatus['action'];
              }

              ?>
                        <tr>
              <td width="4%"><?php echo $i;?></td>
              <td width="9%"><span><?php echo strtoupper($appDetails['app_type']);?></span></td>
              <td width="10%"><span><?php echo $appDetails['id'];?></span></td>
              <td width="9%"><span><?php echo $appDetails['ref_no'];?></span></td>
              <td width="14%"><span><?php echo $appDetails['name'];?></span></td>
              <td width="9%"><span><?php echo date_format(date_create($appDetails['dob']), 'Y-m-d');?></span></td>
              <?php if(isset ($fStatus) && $fStatus!=null){ ?>
              <td width="9%"><span><?php echo strtoupper($fStatus);?></span></td>
              <?php } else{ $showButtons = true;?>
              <td width="9%"><span><?php echo $appDetails['status'];?></span></td>
              <?php  }  ?>
              <td width="9%"><span>
                <?php if($appDetails['status'] != 'New') echo date_format(date_create($ipay4meOrderDetails[0]['paid_at']), 'Y-m-d'); else echo "--"; ?>
                </span></td>
              <td width="10%"><span><?php echo str_replace('@',' @',$ipay4meOrderDetails[0]['email']);?></span></td>
              <td width="10%"><span><?php echo $sf_params->get('order_no');?></span></td>
              <td width="10%"><span><?php echo $gatewayName;?></span></td>
              <td width="9%"><span><a href="#" onclick="findDuplicate('<?= $appDetails['app_type']; ?>','<?= $appDetails['id']; ?>')" title="Find Duplicate Applications" ><?php echo image_tag("/images/duplicate_app.gif"); ?></a></span></td>
                     </tr>
            <?php
            $i++;
            endforeach; ?>
            <tr>
              <td class="blbar" colspan="12" height="25px" align="right"><div class="paging pagingFoot">
                  <?php
                  echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName()).'?order_no='.$sf_params->get('order_no').'&usePager=true','detail_div');
                  ?>
                </div></td>
            </tr>
            <?php } else { ?>
            <tr>
              <td  align='center' class='red' colspan="11">No Results found</td>
            </tr>
            <tr>
              <td class="blbar" colspan="12" height="25px" align="right"></td>
            </tr>
            <?php } ?>
            </tbody>
        </table>
        <div class="clear">&nbsp;</div>
        <?php if($showButtons)
  {
    if($pager->getNbResults()>0)
    {?>
        <div align="center">
          <?php   echo button_to('Refund',url_for("supportTool/orderAction?pAction=Refund&order_no=".$sf_params->get('order_no')),array('class' => 'normalbutton')); ?>
          <?php   echo button_to('Refund and Block',url_for("supportTool/orderAction?pAction=Refund and Block&order_no=".$sf_params->get('order_no')),array('class' => 'normalbutton')); ?>
          <?php   echo button_to('Chargeback',url_for("supportTool/orderAction?pAction=Chargeback&order_no=".$sf_params->get('order_no')),array('class' => 'normalbutton')); ?>
        </div>
        <?php }
                    } 
                } ?>
<div class="clear">&nbsp;</div>
      <div id="result_div"></div>
      <div id="application_div" class="no_display"></div>
</div>
<div class="content_wrapper_bottom"></div>
