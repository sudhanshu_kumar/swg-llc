
<script>
  function validateForm(){
    var comments = $("#approver_comments").val();
    var cart_cap = $("#cart_capacity").val();
    var cart_amt = $("#cart_amount_capacity").val();
    var cart_txn = $("#number_of_transaction").val();
    if(comments==''){
      alert("Please provide comments");
      $("#approver_comments").focus();
      return false;
    }else if(comments.length>255){
      alert("Comments should not more then 255 characters");
      $("#approver_comments").focus();
      return false;
    }
<?php if($userAuthDetailsObj->getTransactionType() == 'Yes'){?>
    if (isNaN(cart_cap))
    {
      alert("Cart Capacity is not an integer value")
      $("#cart_capacity").focus();
      return false
    }

    if (isNaN(cart_amt))
    {
      alert("Cart Amount capacity is not a number value")
      $("#cart_amount_capacity").focus();
      return false
    }

    if (isNaN(cart_txn))
    {
      alert("Allowed Number of Transaction is not a number value")
      $("#number_of_transaction").focus();
      return false
    }
  <?php }?>

    }
</script>




<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">
  <?php
use_helper('Pagination');
use_helper('Form');
include_partial('global/innerHeading',array('heading'=>"Applicant's Credit Card Details"));
?>
<div class="clear"></div>
<div class="tmz-spacer"></div>
      <?php
      $sf = sfContext::getInstance()->getUser();
      if($sf->hasFlash('notice')){ ?>
      <div id="flash_notice" class="alertBox" >
        <?php
        echo nl2br($sf->getFlash('notice'));
        ?>
      </div><br/>
      <?php }?>

      <div class="wrapForm2">
        <?php
        $sf = sfContext::getInstance()->getUser();
        if($sf->hasFlash('error')){ ?>
        <div id="flash_error" class="alertBox" >
          <?php
          echo nl2br($sf->getFlash('error'));
          ?>
        </div><br/>
        <?php }?>

      </div>

      <div class="wrapForm2">
        <?php
        if($isApprove){ ?>
        <div id="flash_error" class="alertBox" >
          <?php
          echo nl2br($msg);
          ?>
        </div><br/>
        <?php }?>

      </div>


    <br>
    <div class="clearfix">
      <div id="nxtPage">



        <form action="<?= url_for("supportTool/processAction")?>" method="post" onsubmit="return validateForm();">

          <table border="1" style="width:100%; margin-bottom:10px;">

            <tr>
              <td valign="top">
                <table width="100%" style="margin:auto; vertical-align:top;">
                  <tr>
                    <td class="blbar" colspan="2" align="right">      <p>Billing information</p>

                    </td>
                  </tr>


                  <tr>
                    <td> First Name</td>
                    <td><?= $userAuthDetails['first_name'] ?>
                    </td>
                  </tr>
                  <tr>
                    <td>Last Name</td>
                    <td><?php echo $userAuthDetails['last_name'] ?>
                    </td>
                  </tr>

                  <tr>
                    <td>Address 1</td>
                    <td><?php echo nl2br($userAuthDetails['address1']) ?>
                    </td>
                  </tr>

                  <tr>
                    <td>Address 2</td>
                    <td><?php if($userAuthDetails['address2']!='') echo $userAuthDetails['address2']; else echo "--"; ?>
                    </td>
                  </tr>

                  <tr>
                    <td>Town</td>
                    <td><?php echo $userAuthDetails['town'] ?>
                    </td>
                  </tr>

                  <tr>
                    <td>State</td>
                    <td><?php if($userAuthDetails['state']!='') echo $userAuthDetails['state']; else echo "--"; ?>
                    </td>
                  </tr>

                  <tr>
                    <td>Zip</td>
                    <td><?php if($userAuthDetails['zip']!='') echo  $userAuthDetails['zip']; else echo "--"; ?>
                    </td>
                  </tr>

                  <tr>
                    <td>Country</td>
                    <td><?php echo CountryTable::getCountryName($userAuthDetailsObj['country']); ?>
                    </td>
                  </tr>

                  <tr>
                    <td>Email</td>
                    <td><?php echo $userAuthDetails['email'] ?>
                    </td>
                  </tr>

                  <tr>
                    <td>Phone Number</td>
                    <td><?php echo $userAuthDetails['phone'] ?>
                    </td>
                  </tr>

                </table>
                <table width="100%" style="margin:auto; margin-bottom:10px;">
                  <tr>
                    <td class="blbar" colspan="2" align="right">      <p>Card information</p></td>
                  </tr>
                  <?php
                  switch ($userAuthDetails['card_type']){
                    case "V" :
                      $cardtype = "Visa Card";
                      break;
                    case "A":
                      $cardtype = "American Express";
                      break;

                    case "M":
                      $cardtype = "Master Card";
                      break;
                    default :
                      $cardtype = "-- Not Set --";


                    }


                    ?>
                  <tr>
                    <td>Card Type</td>
                    <td><?php echo $cardtype ?>
                    </td>
                  </tr>
                  <?php


                  $cardLen = $userAuthDetails['card_len'];
                  $middlenum = '';
                  $cardDisplay = '';
                  if($cardLen == 13){
                    $middlenum = "-XXXXX-";
                  }else if($cardLen == 16){
                    $middlenum = "-XXXX-XXXX-";
                  }else if($cardLen == 15){
                    $middlenum = "-XXXXXXX-";
                  }
                  $cardDisplay = $userAuthDetails['card_first'].$middlenum.$userAuthDetails['card_last']
                  ?>
                  <tr>
                    <td>Card number</td>
                    <td><?php echo "<b>".$cardDisplay."</b>" ?> <!--<br> <img src="<?php echo image_path('visa_new.gif'); ?>" alt="" class="pd4" />-->
                    </td>
                  </tr>

                  <tr>
                    <td>Card Holder Name</td>
                    <td><?php echo ucwords($userAuthDetails['card_holder']) ?>
                    </td>
                  </tr>
                  <?php $month = array('January','February','March','April','May','June','July','August','September','October','November','December') ; ?>
                  <tr>
                    <td>Expiry</td>
                    <td><?php echo $month[$userAuthDetails['expiry_month']-1]." ".$userAuthDetails['expiry_year'] ?>
                    </td>
                  </tr>

                    <?php
                    if(file_exists(sfConfig::get('sf_upload_dir').'/applicant_profile/'.$userAuthDetails['address_proof']) && !empty($userAuthDetails['address_proof']))
                    { ?>
                  <tr>
                    <td colspan="2"><a href="javascript:;" onclick="javascript:window.open('<?= _compute_public_path($userAuthDetails['address_proof'], 'uploads/applicant_profile', '', true);;?>','MyPage','width=750,height=600,scrollbars=1,resizable=yes');">View uploaded credit card statement</a>
                    </td>
                  </tr>
                  <?php } ?>
                <?php if(file_exists(sfConfig::get('sf_upload_dir').'/applicant_profile/'.$userAuthDetails['user_id_proof']) && $userAuthDetails['user_id_proof']!=''){ ?>
                  <tr>
                    <td colspan="2"><a href="javascript:;" onclick="javascript:window.open('<?= _compute_public_path($userAuthDetails['user_id_proof'], 'uploads/applicant_profile', '', true);?>','MyPage','width=750,height=600,scrollbars=1,resizable=yes');">View uploaded identity proof</a>
                    </td>
                  </tr>
                  <?php } ?>

                </table>

              </td>
              <br/>
              <td style="width:50%; vertical-align:top;">

              <table  style="width:100%;">

                <tr>
                  <td class="blbar" colspan="2" align="right">      <p>Comments</p>

                  </td>
                </tr>
                <tr>
                  <td>Comments<span class ="red">*</span></td>
                  <td>
                    <textarea cols="28" rows="2" id="approver_comments" name="approver_comments"></textarea>
                    <br>
                    <div class="red" id="expiry_date_error">
                      (The text you enter above will be sent to the user in an email)
                    </div>
                    <input type="hidden" name="auth_id" value="<?= $userAuthDetails['id'];?>">
                  </td>
                </tr>
                <tr>
                  <td class="blbar" colspan="2" align="right">      <p>User Limit</p>

                  </td>
                </tr>
                <tr>
                  <td colspan="2">Do you want to do multiple transactions? <span style="text-align:right;font-weight:bold;"><?php echo $userAuthDetailsObj->getTransactionType(); ?></span></td>
                </tr>

                <input type="hidden" name="user_id" id="user_id" value="<?php echo $user_id; ?>" />

                  <?php if($userAuthDetailsObj->getTransactionType() == 'Yes' && $userAuthDetailsObj->getStatus() == 'New'){?>
                <tr>
                  <td width="44%">Reason</td>
                  <td><?php echo $userAuthDetailsObj->getReason();?></td>
                </tr>

                <tr>
                  <td colspan="2">
                    <table width="100%" >
                      <?php
                      $cartCapacity = (isset($cartCapacity))?$cartCapacity:"";
                      $CartAmountCapacity = (isset($CartAmountCapacity))?$CartAmountCapacity:"";
                      $NumberOfTransaction = (isset($NumberOfTransaction))?$NumberOfTransaction:"";
                      //$TransactionPeriod = (isset($TransactionPeriod))?$TransactionPeriod:"";
                      echo formRowCompleteNoCSS('Cart Capacity',input_tag('cart_capacity', $cartCapacity, array('size' => 30, 'maxlength' => 5,'class'=>'txt-input')));
                      echo formRowCompleteNoCSS('Cart Amount Capacity',input_tag('cart_amount_capacity', $CartAmountCapacity, array('size' => 30, 'maxlength' => 8,'class'=>'txt-input')));
                      echo formRowCompleteNoCSS('Allowed Number of Transaction(s)',input_tag('number_of_transaction', $NumberOfTransaction, array('size' => 30, 'maxlength' => 5,'class'=>'txt-input')));
                      // echo formRowCompleteNoCSS('Transaction Period',input_tag('transaction_period', $TransactionPeriod, array('size' => 30, 'maxlength' => 30,'class'=>'txt-input')));

                      ?>
                    </table>
                  </td>
                </tr>
                <?php }?>
              <?php if(file_exists(sfConfig::get('sf_upload_dir').'/applicant_profile/'.$userAuthDetails['agent_proof']) && $userAuthDetails['agent_proof']!=''){ ?>
                <tr>
                  <td colspan="2"><a href="javascript:;" onclick="javascript:window.open('<?= _compute_public_path($userAuthDetails['agent_proof'], 'uploads/applicant_profile', '', true);;?>','MyPage','width=750,height=600,scrollbars=1,resizable=yes');">View uploaded Agent proof</a>
                  </td>
                </tr>
                <?php } ?>
              </table>
            </tr>
          </table>
          <tfoot>


            <tr>

              <td colspan="2" colspan="2">


                <div class="pixbr XY20" style="width:350px; margin:auto;">
                  <center id="multiFormNav">

                      <?php if(!$isApprove){ ?>
                    <input type="submit" id="approve" class="normalbutton" value="Approve" name="approve"  />
                    <?php } ?>


                  <?php if($status=="New"){ ?>
                    <input type="submit" id="reject" value="Reject" class="normalbutton" name="reject"/>
                    <?php if(!$isApprove){ ?>
                   
                    <?php } ?>
                  <?php } ?>
                <?php if($status=="Approved"){ ?>
                    <input type="submit" id="reject" value="Reject" class="normalbutton" name="reject"/>
                    <?php if(!$isApprove){ ?>
                
                    <?php } ?>
                  <?php } ?>
                <?php if($status=="Rejected"){ ?>

                  <?php if(!$isApprove){ ?>
                
                    <?php } ?>
                  <?php } ?>
                    <input type="button" value="Cancel" onclick="gotoBack();" class="normalbutton" id="Cancel" name="cancle"/>
                    <input type="hidden" name="transaction_type" value="<?php echo $userAuthDetailsObj->getTransactionType(); ?>" />
                  </center>
                </div>
              </td>
            </tr>
          </tfoot>
        </form>
        <br>
      </div>
    </div>
    </div>
</div>
<div class="content_wrapper_bottom"></div>

<script>
  function gotoBack(){
      var url = '<?php echo url_for('supportTool/userListAuth');?>';
      location.href=url+'/back/1';
  }
</script>