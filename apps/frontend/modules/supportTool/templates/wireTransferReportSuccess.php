
<script>
  function validateForm()
  {
    $('#detail_div').html('<?php echo image_tag('../images/ajax-loader.gif'); ?>');
    var err  = 0;

    if($('#wt_status').val() =="")
    {
      $('#detail_error').html("<font color='red'>Please select wire transfer status.</font>");

      err = err+1;
    }else
    {
      $('#detail_error').html(" ");
    }


    if(err != 0)
    {
      $('#hdn_datatable').hide();
      return false;
    }else{

    }



  }


  function hideMONumberTr(){
    if($('#wt_status').val() == 'New'){
      $('#wire_transfer_row').hide();
      $('#msg_tr').show();
      $('#msg_tr_mo').hide();
    }else{
      $('#wire_transfer_row').show();
      $('#msg_tr_mo').show();
      $('#msg_tr').hide();
    }
  }
</script>

<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">
<?php
use_helper('Pagination');
use_helper('Form');
include_partial('global/innerHeading',array('heading'=>'Tracking Number Status'));
?>
<?php
use_javascript('dhtmlwindow.js');
use_stylesheet('dhtmlwindow.css');
use_javascript('modal.js');
use_stylesheet('modal.css');
?>
  <div class="clear"></div>
<div class="tmz-spacer"></div>

    <?php echo form_tag('supportTool/wireTransferReport',array('name'=>'order_search_form','class'=>'', 'method'=>'post','id'=>'order_search_form')) ?>

   
      <?php
      $sf = sfContext::getInstance()->getUser();
      if($sf->hasFlash('notice')){ ?>
      <div id="flash_notice" class="alertBox" >
        <?php
        echo nl2br($sf->getFlash('notice'));
        ?>
      </div><br/>
      <?php }?>

        <?php
        $sf = sfContext::getInstance()->getUser();
        if($sf->hasFlash('error')){ ?>
        <div id="flash_error" class="alertBox" >
          <?php
          echo nl2br($sf->getFlash('error'));
          ?>
        </div><br/>
        <?php }?>

        <table width="100%">
          <?php
          $wt_status ='';
          $wireTransfer = (isset($_REQUEST['wire_transfer'])) ? $_REQUEST['wire_transfer'] : $_SESSION['wire_transfer'];

          $wt_status = (isset($_REQUEST['wt_status'])) ? $_REQUEST['wt_status'] : $_SESSION['wt_status'];


          $order_number = (isset($_REQUEST['order_number'])) ? $_REQUEST['order_number'] : $_SESSION['order_number'];
          $tracking_number = (isset($_REQUEST['tracking_number'])) ? $_REQUEST['tracking_number'] : $_SESSION['tracking_number'];
          $statusArray = array(''=>'All','Associated'=>'Associated','Paid'=>'Paid','New'=>'Pending');

          echo formRowComplete('Status',select_tag('wt_status',options_for_select($statusArray,$wt_status),array('onchange'=>'hideMONumberTr();')),'','wt_status','detail_error','wt_status_row');
          echo "</tr>";
          //echo "<div id='mo_number_tr' style=''>";
          echo formRowComplete('Wire Transfer Serial Number',input_tag('wire_transfer', $wireTransfer, array('size' => 27, 'maxlength' => 30,'class'=>'txt-input')),'','wire_transfer','detail_error','wire_transfer_row');
          echo "</tr>";
          echo formRowComplete('Tracking Number',input_tag('tracking_number', $tracking_number, array('size' => 27, 'maxlength' => 20,'class'=>'txt-input')));
          echo "</tr>";
          echo formRowComplete('Order Number',input_tag('order_number', $order_number, array('size' => 27, 'maxlength' => 20,'class'=>'txt-input')));
          echo "</tr>";
          ?>
          <tr id="msg_tr_mo">
            <td colspan="2" height="50px" class="highlight yellow"><b>Note: </b>You can search your tracking number status by entering either Wire Transfer Serial Number or Tracking Number or Order Number OR any other combination available above.</td>
          </tr>
          <tr id="msg_tr"  style="display:none;">
            <td colspan="2" height="50px class="highlight yellow""><b>Note: </b>You can search your tracking number status by entering either Tracking Number or Order Number OR any other combination available above.</td>
          </tr>
          <tr valign="top" >
            <td height="30" valign="top" colspan="2" align="center">
                <?php echo submit_tag('Search',array('class' => 'normalbutton')); ?>
           
              </td>
          </tr>
        </table>

 


  </form>
  <br>





<?php if($isFound){

  ?>


    <div id="nxtPage">

      <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" id="hdn_datatable">
        <tr>
          <td class="blbar" colspan="6" align="right">
            <div style="float:left">Tracking Number</div>
          <span>Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total  <b><?php echo $pager->getNbResults(); ?></b>  results</span></td>
        </tr>
        <tr>
          <td width="5%" ><span class="txtBold">S. No.</span></td>
          <td width="20%"><span class="txtBold">Tracking Number</span></td>
          <td width="20%"><span class="txtBold">Amount</span></td>
          <td width="20%"><span class="txtBold">Association Date</span></td>
          <td width="20%"><span class="txtBold">Order Number</span></td>
          <td width="20%"><span class="txtBold">Status</span></td>

        </tr>
        <tbody>
          <?php
          $j = $pager->getFirstIndice();
          if($pager->getNbResults()>0)
          {
            foreach ($pager->getResults() as $result):

            ?>
          <tr>
            <td width="5%"><?php echo $j;?></td>
            <td width="20%">
              <?php if($result['status'] == 'New') {?>
              <span><?= $result['tracking_number'] ?></span>
              <? } else{?>
              <a href = "#" onclick="trackingInfoBox('<?php echo $result['id'] ?>','<?php echo $result['status'] ?>')"><span><?= $result['tracking_number'] ?></span></a>
              <?php } ?>
            </td>

            <td width="20%"><span><?= format_amount($result['cart_amount'],1) ?></span></td>
            <td width="20%">
              <?php if($result['associated_date'] == '0000-00-00') {?>
              <span>--</span>
              <?php }else{echo $result['associated_date'];} ?>
            </td>
            <td width="20%">
              <?php if($result['order_number'] == '' || $result['order_number']=='0') {?>
              <span>--</span>
              <?php }else{  ?>
              <a href="<?php echo url_for('supportTool/wireTransferDetails?wire_transfer_id='.$result['Wiretransfer']['id'].'&back=1&page='.$page.'&order_number='.$result['order_number']);?>"><?php echo $result['order_number'] ?></a>
              <?php } ?>
            </td>

            <td width="20%">
              <?php if($result['status'] != 'New') {?>
              <span><?= $result['status'] ?></span>
              <?php }else {echo 'Pending';} ?>
            </td>

          </tr>
          <?php $j++;

          endforeach; ?>
          <tr>
            <td class="blbar" colspan="11" height="25px" align="right">
              <div class="paging pagingFoot">
                <?php
                echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName()).'?wt_status='.$_SESSION['wt_status'].'&wire_transfer='.$_SESSION['wire_transfer'].'&tracking_number='.$_SESSION['tracking_number'].'&order_number'.$_SESSION['order_number'].'&usePager=true'.'&back=1','detail_div');
                ?>
              </div>
            </td>
          </tr>
          <?php } else { ?>
          <tr><td  align='center' class='red' colspan="6">No Result Found.</td></tr>
          <?php } ?>
        </tbody>

      </table>
      <br>
    </div>

  <?php }
?>


</div>
</div>
<div class="content_wrapper_bottom"></div>
<script>
  function trackingInfoBox(id,status){

    emailwindow=dhtmlmodal.open('EmailBox', 'iframe', "<?php echo url_for('supportTool/wtTrackingInfoBox').'?tracking_id=' ?>"+id+"&status="+status, 'Tracking Number Details', 'width=650px,height=270px,center=1,border=0, scrolling=no');


  }
  if($('#wt_status').val() == 'New'){
    //alert('dfg');
    $('#wire_transfer_row').hide();
  }else{
    $('#wire_transfer_row').show();
  }
</script>