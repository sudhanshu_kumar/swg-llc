
<script>
  function validateForm()
  {
    $('#detail_div').html('<?php echo image_tag('../images/ajax-loader.gif'); ?>');
    var err  = 0;

    if(document.getElementById('digits').value){
      var creditcardD = $('#digits').val();
      if(isNaN(creditcardD)){
        alert("Credit Card Number should be numeric value.");
        $('#digits').val('');
        $('#digits').focus();
        $('#hdn_datatable').hide();
        return false;
      }
      if(creditcardD.length>16){
        alert("Please Enter Correct Credit Card Number.");
        $('#digits').focus();
        $('#hdn_datatable').hide();
        return false;
      }
      if(creditcardD.length<13){
        alert("Please Enter Correct Credit Card Number.");
        $('#digits').focus();
        $('#hdn_datatable').hide();
        return false;
      }
    }

    if($('#first_digit').val()!="" && $('#digits').val()!="" || $('#last_digit').val()!="" && $('#digits').val()!="" )
    {
      if(matchCard())
      {
        return false;
      }
      else {
        return true;
      }
    }


    if(document.getElementById('first_digit').value) {
      var firstD = $('#first_digit').val();
      if(isNaN(firstD)){
        alert("Card First 4 Digits should be numeric value.");
        $('#first_digit').val('');
        $('#first_digit').focus();
        return false;
      }

      if(firstD.length>4){
        alert("Please Enter Only First 4 Digits of Credit Card.");
        $('#first_digit').focus();
        return false;
      }
    }

    if(document.getElementById('name').value) {
      var nameC = $('#name').val();
      var regExp = /^[A-Za-z]$/;
      if (!nameC.charAt(document.getElementById('name').value).match(regExp))
      {
        alert("Please enter correct Card Holder Name.");
        return false;
      }

      if(nameC.length<1){
        alert("Please Enter Credit Card Holder Name.");
        $('#name').focus();
        return false;
      }

      if(nameC.length>27){
        alert("Please Enter correct Credit Card Holder Name.");
        $('#name').focus();
        return false;
      }
    }



    if(document.getElementById('last_digit').value){
      var lastD = $('#last_digit').val();
      if(isNaN(lastD)){
        alert("Card Last 4 Digits should be numeric value.");
        $('#last_digit').val('');
        $('#last_digit').focus();
        return false;
      }

      if(lastD.length>4){
        alert("Please Enter Only Last 4 Digits of Credit Card.");
        $('#last_digit').focus();
        return false;
      }
    }



    if($('#first_digit').val() ==""&& $('#last_digit').val() =="" && $('#digits').val() =="" &&$('#name').val() =="" )
    {

      document.getElementById('error_td').style.display=  '';
      $('#card_error').html("<span class='red'>Please enter either Credit Card Number / Card First 4 Digits / Card Last 4 Digits / Card Holder Name.</span>");

      err = err+1;
    }else
    {
      $('#card_error').html(" ");
    }

    if(err != 0)
    {
      $('#hdn_datatable').hide();
      return false;
    }else{

    }

  }
  function matchCard()
  {

    var string = $('#digits').val();
    var length= string.length;
    var the_first= string.slice(0,4);
    var the_last = string.slice(length-4,length)
    var first_digit = $('#first_digit').val();
    var last_digit = $('#last_digit').val();
    //        alert(the_first);alert(the_last);alert(first_digit);alert(last_digit);die;

    if(first_digit){
      if(the_first!=first_digit)
      {
        document.getElementById('error_td').style.display=  '';
        $('#card_error').html("<font color='red'>Credit Card first four or last four digits are not same as mentioned in Credit Card number.</font>");
       $('#hdn_datatable').hide();
       return true;
      }
    }
    if(last_digit){
      if (the_last!=last_digit)
      {
        document.getElementById('error_td').style.display=  '';
        $('#card_error').html("<font color='red'>Credit Card first four or last four digits are not same as mentioned in Credit Card number.</font>");
        $('#hdn_datatable').hide();
        return true;
      }
    }
    else {
      return false;
    }


  }
</script>

<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">
  <?php
use_helper('Pagination');
use_helper('Form');
include_partial('global/innerHeading',array('heading'=>'Search by Card Number'));
?>
<div class="clear"></div>
<div class="tmz-spacer"></div>
    <?php echo form_tag('supportTool/searchByCardNumber',array('name'=>'order_search_form','class'=>'', 'method'=>'post','id'=>'order_search_form','onSubmit'=>'return validateForm()')) ?>

      <?php
      $sf = sfContext::getInstance()->getUser();
      if($sf->hasFlash('notice')){ ?>
      <div id="flash_notice" class="alertBox" >
        <?php
        echo nl2br($sf->getFlash('notice'));
        ?>
      </div><br/>
      <?php }?>

        <?php
        $sf = sfContext::getInstance()->getUser();
        if($sf->hasFlash('error')){ ?>
        <div id="flash_error" class="alertBox" >
          <?php
          echo nl2br($sf->getFlash('error'));
          ?>
        </div><br/>
        <?php }?>

        <table width="100%" cellpadding="0" cellspacing="0" border="0">
          <?php

          $cardFirst = (isset($_POST['first_digit']))?$_POST['first_digit']:"";
          $cardLast = (isset($_POST['last_digit']))?$_POST['last_digit']:"";
          $cardDigit = (isset($_POST['digits']))?$_POST['digits']:"";
          $name = (isset($_POST['name']))?$_POST['name']:"";

          //                echo "<tr><td>";
          echo formRowComplete('Card First 4 Digit',input_tag('first_digit', $cardFirst, array('size' => 27, 'maxlength' => 4,'class'=>'txt-input')));
          echo "</tr>";
          echo formRowComplete('Card Last 4 Digits',input_tag('last_digit', $cardLast, array('size' => 27, 'maxlength' => 4,'class'=>'txt-input')));
          echo "</tr>";
          echo formRowComplete('Credit Card Number',input_tag('digits', $cardDigit, array('size' => 27, 'maxlength' => 16,'class'=>'txt-input')));
          echo "</tr>";
          echo formRowComplete('Card Holder Name',input_tag('name', $name, array('size' => 27, 'maxlength' => 27,'class'=>'txt-input')));
          echo "</tr>";?>
          <tr id="error_td" style="display:none">
            <td colspan="2" ><div id="card_error" class="float_left">
            </div></td>
          </tr>
          <tr valign="top">
            <td height="30" valign="top" style="border-right-style:none;">&nbsp;</td>
            <td height="30" valign="top" style="border-left-style:none;">
                <?php   echo submit_tag('Search',array('class' => 'normalbutton')); ?>

              </td>
          </tr>
        </table>
		<div class="clear">&nbsp;</div>
        <div id="card_error1" class="highlight yellow"><strong>NOTE:</strong> Atleast one field must be specified in search criteria.
        </div>
  <br>
  <?php if($isFound): ?>
  <div class="clearfix">
    <div id="nxtPage">
      <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" id="hdn_datatable">
        <tr>
          <td class="blbar" colspan="4" align="right">
            <div class="float_left">Details by Card Number</div>
          <span>Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total  <b><?php echo $pager->getNbResults(); ?></b>  results</span></td>
        </tr>
        <tr>
          <td width="5%" ><span class="txtBold">S. No.</span></td>
          <td width="15%"><span class="txtBold">Credit Card number</span></td>
          <td width="10%"><span class="txtBold">Card Holder Name</span></td>
          <td width="10%"><span class="txtBold">Action</span></td>
        </tr>
        <tbody>
          <?php
          $i = $pager->getFirstIndice();

          // $checkedVal = $sf_request->getParameter('checkedVal');

          if($pager->getNbResults()>0)
          {
            foreach ($pager->getResults() as $details):

            $cardLen = $details['card_len'];
            $middlenum = '';
            $cardDisplay = '';
            if($cardLen == 13){
              $middlenum = "-XXXXX-";
            }else if($cardLen == 16){
              $middlenum = "-XXXX-XXXX-";
            }else if($cardLen == 15){
              $middlenum = "-XXXXXXX-";
            }
            $cardDisplay = $details['card_first'].$middlenum.$details['card_last'];
            ?>
          <tr>
            <td width="10%"><?php echo $i;?></td>
            <td width="35%"><span><?= "<b>".$cardDisplay."</b>"; ?></span></td>
            <td width="35%"><span><?= ucwords($details['card_holder']) ?></span></td>
            <td width="20%">
                <span><?= link_to("Details", url_for("supportTool/authDetailsCredit?id=".Settings::encryptInput($details['id'])))?>
            <span><?= link_to("Edit", url_for("supportTool/editContectDetails?id=".Settings::encryptInput($details['id'])))?></span>
            </span></td>
          </tr>
          <?php
          $i++;
          endforeach; ?>

          <tr>
            <td class="blbar" colspan="10" align="right">
              <div class="paging pagingFoot">
                <?php
                echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName()).'?first_digit='.$sf_params->get('first_digit').'&last_digit='.$sf_params->get('last_digit').'&digits='.$sf_params->get('digits').'&name='.$sf_params->get('name').'&usePager=true','detail_div');
                ?>
              </div>
            </td>
          </tr>
          <?php } else { ?>
          <tr><td  align='center' class='red' colspan="10">No Results found</td></tr>
          <?php } ?>
        </tbody>

      </table>
      <br>
    </div>
  </div>

  <?php endif; ?>
</div>
</div>
<div class="content_wrapper_bottom"></div>
