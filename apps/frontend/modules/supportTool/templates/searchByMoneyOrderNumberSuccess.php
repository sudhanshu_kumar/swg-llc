
<script>
  function validateForm()
  {
    //    if(document.getElementById('card_type').value == '')
    //    {
    //      alert('Please Select Card Type.');
    //      document.getElementById('card_type').focus();
    //      return false;
    //    }
    
    var moneyOrderNumber = $('#money_order_number').val();
    if(document.getElementById('money_order_number').value == '')
    {
      alert('Please Enter Money Order Serial Number.');
      document.getElementById('money_order_number').focus();
       
      return false;
    }else{
      var reg = /^[\-0-9a-zA-Z]+$/;
      var moneyOrderNumber = $('#money_order_number').val();

       if(reg.test(moneyOrderNumber) == false) {
          alert('Wrong Money Order Serial Number');
          $("#money_order_number").val('');
          $("#money_order_number").focus();
          
          return false;
       }

    }

  }
</script>


<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">
  <?php
use_helper('Pagination');
use_helper('Form');
include_partial('global/innerHeading',array('heading'=>'Update Money Order'));
?>
  <div class="clear"></div>
<div class="tmz-spacer"></div>
  <div class="clearfix"/>
    <?php echo form_tag('supportTool/moneyOrderDetailsTrackingNumbers',array('name'=>'order_search_form','class'=>'', 'method'=>'post','id'=>'order_search_form','onSubmit'=>'return validateForm()')) ?>

    <div class="wrapForm2">
      <?php
      $sf = sfContext::getInstance()->getUser();
      if($sf->hasFlash('notice')){ ?>
      <div id="flash_notice" class="alertBox" >
        <?php
        echo nl2br($sf->getFlash('notice'));
        ?>
      </div><br/>
      <?php }?>

      <div class="wrapForm2">
        <?php
        $sf = sfContext::getInstance()->getUser();
        if($sf->hasFlash('error')){ ?>
        <div id="flash_error" class="alertBox" >
          <?php
          echo nl2br($sf->getFlash('error'));
          ?>
        </div><br/>
        <?php }?>

        <table width="100%" align="center">
          <?php

          $moneyOrderNumber = (isset($_POST['money_order_number']))?$_POST['money_order_number']:"";


          //                echo "<tr><td>";
          echo formRowComplete('Enter Money Order Serial Number<span style="color:red">*</span>',input_tag('money_order_number', $moneyOrderNumber, array('size' => 30, 'maxlength' => 30,'class'=>'txt-input')));
          echo "<tr>";?>
          <tr>
            <td>&nbsp;</td>
            <td>
                <?php   echo submit_tag('Search',array('class' => 'normalbutton')); ?>
             
              </td>
          </tr>
        </table>
      </div>
    </div>
  </div>
</div>
</div>
<div class="content_wrapper_bottom"></div>
