<?php
use_helper('Form');
use_helper('Pagination');
use_helper('FpsError');

include_partial('global/innerHeading',array('heading'=>'Detailed Summary'));

?>
<?php $sf = sfContext::getInstance()->getUser();
if($sf->hasFlash('notice')){
  ?><div id="flash_notice" class="alertBox" ><?php
  echo nl2br($sf->getFlash('notice'));

  ?></div><?php }?>
<div class="global_content4 clearfix">

  <div class="clearfix" style="padding-bottom:20px">
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
      <tr>
        <td class="blbar" colspan="9" align="right">
        <div style="float:left">Card Detail's</div>

      </tr>
      <?php
      $cardLen = $summary[0]['card_len'];
      $middlenum = '';
      $cardDisplay = '';
      if($cardLen == 13){
        $middlenum = "-XXXXX-";
      }else if($cardLen == 16){
        $middlenum = "-XXXX-XXXX-";
      }else if($cardLen == 15){
        $middlenum = "-XXXXXXX-";
      }
      $cardDisplay = $summary[0]['card_first'].$middlenum.$summary[0]['card_last'];
      ?>
      <tr>
        <td width="40%" ><span class="txtBold">Card Holder Name</span></td>
        <td width="60%"><span><?php echo ucwords($summary[0]['card_holder']);?></span></td>
      </tr>

      <tr>
        <td width="40%" ><span class="txtBold">Card number</span></td>
        <td width="60%"><span><?php echo $cardDisplay;?></span></td>
      </tr>

      <tr>
        <td width="40%" ><span class="txtBold">Gateway</span></td>
        <td width="60%"><span><?php echo $summary[0]['Gateway']['display_name'];?></span></td>
      </tr>

      <tr>
        <td width="40%" ><span class="txtBold">Is Registered</span></td>
        <td width="60%"><span><?php if($appVault)
            {
              echo "Yes";
            }else{
              echo "No";
            }
            ?></span></td>
      </tr>
    </table>
  </div>
  <div class="clearfix" style="padding-bottom:20px">
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable">
      <tr>
        <td class="blbar" colspan="9" align="right">
        <div style="float:left">User Detail's</div>

      </tr>
      <tr>
        <td width="40%" ><span class="txtBold">Name</span></td>
        <td width="60%"><span><?php echo ucwords($userDetail[0]['first_name']." ".$userDetail[0]['last_name']);?></span></td>
      </tr>

      <tr>
        <td width="40%" ><span class="txtBold">Date of Birth</span></td>
        <td width="60%"><span><?php echo $userDetail[0]['dob'];?></span></td>
      </tr>

      <tr>
        <td width="40%" ><span class="txtBold">Email Address</span></td>
        <td width="60%"><span><?php echo $userDetail[0]['email'];?></span></td>
      </tr>

      <tr>
        <td width="40%" ><span class="txtBold">Mobile Phone Number</span></td>
        <td width="60%"><span><?php echo $userDetail[0]['mobile_phone']?></span></td>
      </tr>
    </table>

  </div>
  <div class="clearfix" style="padding-bottom:20px">
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable">
      <tr>
        <td class="blbar" colspan="9" align="right">
        <div style="float:left">Other Detail's</div>

      </tr>
      <tr>
        <td width="40%" ><span class="txtBold">Transaction Date</span></td>
        <td width="60%"><span><?php echo $summary[0]['OrderRequestDetails']['FpsDetail'][0]['trans_date'];?></span></td>
      </tr>

      <tr>
        <td width="40%" ><span class="txtBold">Payment Date</span></td>
        <td width="60%"><span><?php echo $summary[0]['OrderRequestDetails']['FpsDetail'][0]['updated_at'];?></span></td>
      </tr>

      <tr>
        <td width="40%" ><span class="txtBold">Payment Status</span></td>
        <td width="60%"><span><?php
            $paid = $summary[0]['OrderRequestDetails']['FpsDetail'][0]['payment_status'];
            if($paid=='0')
            {
              echo "Paid" ;
            } else {
              echo "Unpaid";
            }
            ?></span></td>
      </tr>

      <?php
        $fpsId =  $summary[0]['OrderRequestDetails']['FpsDetail'][0]['fps_rule_id'];
        $reason = fps_error($fpsId);
      ?>


      <tr>
        <td width="40%" ><span class="txtBold">Reason</span></td>
        <td width="60%"><span><?php echo $reason;?></span></td>
      </tr>
      <tr>
        <td width="40%" ><span class="txtBold">Is Verified</span></td>
        <td width="60%"><span><?php echo ucwords($summary[0]['OrderRequestDetails']['FpsDetail'][0]['isverified']);?></span></td>
      </tr>
      <tr>
        <td width="40%" ><span class="txtBold">Is Registered</span></td>
        <td width="60%"><span><?php if($appVault)
            {
              echo "Yes";
            }else{
              echo "No";
            }
            ?></span></td>
      </tr>
    </table>




  </div>


  <div class="pixbr XY20">

    <center id="multiFormNav">





      <div align="center">
        <table align="center" class="nobrd"><tr>
            <td align="center">
              <div class="lblButton">
                <input style="*text-align:center;" type="button" class="button"  onclick="javascript:history.go(-1)"  value="Back">
              </div>
              <div class="lblButtonRight">
                <div class="btnRtCorner"></div>
              </div>
            </td>
          </tr>

      </table></div>



    </center>

  </div>

</div>