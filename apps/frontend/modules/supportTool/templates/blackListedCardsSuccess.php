<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">
<?php
use_helper('Pagination');
use_helper('Form');
include_partial('global/innerHeading',array('heading'=>'Search SW Global LLC black listed cards'));
?>
<?php
use_javascript('dhtmlwindow.js');
use_stylesheet('dhtmlwindow.css');
use_javascript('modal.js');
use_stylesheet('modal.css');
?>
    <div class="clear"></div>
    <div class="tmz-spacer"></div>
    <?php
      $sf = sfContext::getInstance()->getUser();
      if($sf->hasFlash('notice')){ ?>
      <div id="flash_notice" class="alertBox" >
        <?php
        echo nl2br($sf->getFlash('notice'));
        ?>
    </div>
    <br/>
      <?php }?>
    <?php
    $sf = sfContext::getInstance()->getUser();
    if($sf->hasFlash('error')){ ?>
      <div id="flash_error" class="alertBox" >
        <?php
        echo nl2br($sf->getFlash('error'));
        ?>
    </div>
    <br/>
      <?php }?>

    <?php echo form_tag('supportTool/blackListedCards',array('name'=>'card_search_form','class'=>'', 'method'=>'post','id'=>'cardr_search_form','onSubmit'=>'return validateForm()')) ?>
    <table width="99%" cellpadding="0" cellspacing="0" border="0">
        <?php
        $email = isset($email)?$email:'';
        $cardFirst= isset($card_first)?$card_first:'';
        $cardLast = isset($card_last)?$card_last:'';

        echo formRowComplete('Email Id<span class="red">*</span>',input_tag('email', $email, array('size' => 40, 'maxlength' => 50,'class'=>'txt-input')));
        ?><tr>
            <td align="center" colspan="2">OR</td>
        </tr>
        <tr><?php echo formRowComplete('Card First<span class="red">*</span>',input_tag('card_first', $cardFirst, array('size' => 25, 'maxlength' => 4,'class'=>'txt-input')));?>
        </tr>
        <tr><?php echo formRowComplete('Card Last<span class="red">*</span>',input_tag('card_last', $cardLast, array('size' => 25, 'maxlength' => 4,'class'=>'txt-input')));?>
        </tr>
        <?php
        echo "<tr>";?>
        <tr>
          <td>&nbsp;</td>
        <td><?php   echo submit_tag('Search',array('class' => 'normalbutton')); ?>
        </td>
        </tr>
      </table>
    <br>
      
    <?php if($showDataFlag){?>
      <div class="clearfix">
      <div id="nxtPage">
        <table width="99%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
          <tr>
            <td class="blbar" colspan="5" align="right"><div class="float_left">Black Listed Cards Details</div>
              <span>Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total <b><?php echo $pager->getNbResults(); ?></b> results</span></td>
          </tr>
          <tr>
            <td width="7%" ><span class="txtBold">S. No.</span></td>
            <td width="23%"><span class="txtBold">Name</span></td>
            <td width="40%"><span class="txtBold">Email Id</span></td>
            <td width="15%"><span class="txtBold">Card Number</span></td>
            <td width="15%"><span class="txtBold">Action</span></td>
          </tr>
          <tbody>
            <?php
            $i = $pager->getFirstIndice();

            if($pager->getNbResults()>0)
            {
              foreach ($pager->getResults() as $details):

              $username = ucfirst($details['fname']).' '.ucfirst($details['lname']);
              $cardFirst = $details['card_first'];
              $cardLast = $details['card_last'];
              $cardlen = $details['card_len'];
              $count = $cardlen -8;
              $cardMid = '';
              for($j = 0; $j < $count; $j++){
                $cardMid .= 'X';
              }
              $cardnumber = $cardFirst . $cardMid . $cardLast;
              $encryptedOrder = Settings::encryptInput($details['fpsorder']);              
              ?>
            <tr>
              <td width="7%"><?php echo $i;?></td>
              <td width="23%"><span><?php echo $username ?></span></td>
              <td width="40%"><span><?php echo $details['email'] ?></span></td>
              <td width="15%"><span><?php echo ($cardnumber != '')?$cardnumber:'N/A'; ?></span></td>
              <td width="15%">
                <span class="txtBold">

                <?php if($cardnumber != ''){ ?>
                <?php echo ($details['rule_id'] == 5) ? "<span class='bluelink hand' onclick=\"popupInf('".$encryptedOrder."');\">View Comments</span>"
                  : '<a href ="javascript:void(0)" onclick = \'unblockCard("'.url_for($sf_context->getModuleName().'/unblockCards').'?order='.$details['fpsorder'].'&card='.$cardnumber.'","'.$cardnumber.'");\' >Unblock</a>' ;  ?>
                <?php } else { ?>
                    <a href="javascript:void(0);" onclick="unblockUndefinedCard('<?php echo $details['fps_id']; ?>');" >Unblock</a>
                <?php } ?>                  
                </span>
              </td>
            </tr>
            <?php
            $i++;
            endforeach; ?>
            <tr>
              <td class="blbar" colspan="5" height="25px" align="right"><div class="paging pagingFoot">
                  <?php
                  echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName()).'?email='.$sf_params->get('email'),'detail_div');
                  ?>
                </div></td>
            </tr>
            <?php } else { ?>
            <tr>
              <td  align='center' class='red' colspan="5">No card is blocked by this details</td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
        <br>
      </div>
    </div>
    <?php } ?>
  </div>
</div>
<div class="content_wrapper_bottom"></div>
<script>

  function popupInf(order){
    commentwindow=dhtmlmodal.open('view', 'iframe', '<?php echo url_for('supportTool/viewComments').'?order='?>'+order, 'View Comments', 'width=620px,height=250px,center=1,border=0, scrolling=no')
    commentwindow.onclose=function(){
      return true ;
    }
  }

  function unblockCard(url,cardnumber){
      if(confirm('Are you sure you want to unblock card : '+cardnumber+' ?')){
          window.location = url;
      }

  }


  function unblockUndefinedCard(fps_id){
      if(confirm('Are you sure you want to unblock card?')){
          window.location = '<?php echo url_for('supportTool/unblockUndefinedCards'); ?>/fps_id/'+fps_id;          
      }
  }
  
  function validateForm(){
       var email = $('#email').val();       
       var card_first = $('#card_first').val();
       var card_last = $('#card_last').val();
       if(email != '' && (card_first != '' || card_last != '')){
          alert('Please enter either email or Card details.');
          return false;
       }else if(email == '' && card_first == '' && card_last == ''){
                alert('Please enter Email or Card details.');
                return false;
       }else if(email != ''){
               var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
               if(reg.test(email) == false) {
                    alert('Please provide a valid email id');
                    email.focus;
                    return false;
                }
          }else if(card_first == ''){
              alert('Please enter Card First.');
              return false;
          }else if(card_last == ''){
              alert('Please enter Card Last.');
              return false;
          }else if(isNaN(card_first) || isNaN(card_last)){
              alert('Card Number should be numeric only.');
              return false;
          }else {
                    if(card_first.length > 4 || card_last.length > 4){
                        alert('Please Enter only first four card digits.');
                        return false;
                    }

          }
  }
</script>
