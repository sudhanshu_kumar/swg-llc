<?php  echo ePortal_pagehead(" ",array('class'=>'_form')); ?>
<tr>
    <td>
        <table width="100%">
            <tr>
                <td class="blbar"  colspan="6" align="left"><div style="float:left">Order Details</div></td>
            </tr>
            <tr>
                <td width="10%" class="txtBold">Order Number</td>
                <td width="10%" class="txtBold">Item Number</td>
                <td width="15%" class="txtBold">Gateway</td>
                <td width="15%" class="txtBold">Amount</td>
                <td width="15%" class="txtBold">User Name</td>
                <td width="10%" class="txtBold">Action</td>
            </tr>

            <tr>
                <td width="50px"><span ><?php echo $orderNumber; ?></span></td>

                <td width="50px"><span ><?php echo $orderDetails->getTransactionNumber(); ?></span></td>
                <td width="50px"><span ><?php echo $gatewayDetails->getName(); ?></span></td>
                <td width="50px"><span ><?php echo format_amount($orderDetails->getAmount(),1); ?></span></td>
                <td width="50px"><span ><?php echo $userName; ?></span></td>
                <td><a href="<?php echo url_for('supportTool/makePayment?orderNumber='.$orderNumber) ?>">Pay</a></td>
            </tr>
         </table>
    </td>
</tr>

