
<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">
<?php include_partial('global/innerHeading',array('heading'=>"Applicant's Details"));?>
    <div class="clear"></div>
    <div class="tmz-spacer"></div>

<?php if($sf_request->getParameter('p') !='i') {?>
  <?php  echo ePortal_popup("Please Keep This Safe","<p>You will need it later</p><h5>Application Id: ".$passport_application[0]['id']."</h5><h5> Reference No: ".$passport_application[0]['ref_no']."</h5>");  ?>
  <?php if(isset($chk)){ echo "<script>pop();</script>"; } ?>
  <?php }

?>

<!--div id="waitPayment" style="display:none;position:fixed;_position:absolute;top:20%;left:45%;padding:50px 40px;background:#fff;border:1px solid #999;z-index:99;line-height:30px;"> <?php echo image_tag('/images/ajax-loader.gif', array('alt' => 'Checking your Payment Attempts status.','border'=>0 )); ?><br/> Loading payment status...</div-->
</center>

<?php
$sf = sfContext::getInstance()->getUser();
if($sf->hasFlash('notice')){ ?>
<div id="flash_notice" class="alertBox" >
  <?php
  echo nl2br($sf->getFlash('notice'));
  ?>
</div><br/>
<?php }?>


<?php
$sf = sfContext::getInstance()->getUser();
if($sf->hasFlash('error')){ ?>
<div id="flash_error" class="alertBox" >
  <?php
  echo nl2br($sf->getFlash('error'));
  ?>
</div><br/>
<?php }?>

<form action="<?php echo url_for('cart/list');// echo secure_url_for('payments/ApplicationPayment') ?>" method="POST" class="dlForm multiForm">
<div  id="Reciept" >

<fieldset class="bdr">
  <?php echo ePortal_legend("Profile"); ?>
  <dl>
    <dt><label >Full name:</label ></dt><dd style="width:450px;">
      <?php if($type=='passport'){ ?>
        <?php echo ePortal_displayName($passport_application[0]['title_id'],$passport_application[0]['first_name'],@$passport_application[0]['mid_name'],$passport_application[0]['last_name']); ?>
        <?php } else if($type=='visa'){?>
        <?php echo ePortal_displayName($passport_application[0]['title'],$passport_application[0]['other_name'],@$passport_application[0]['middle_name'],$passport_application[0]['surname']); ?>
        <?php } else if($type=='freezone'){ ?>
        <?php echo ePortal_displayName($passport_application[0]['title'],$passport_application[0]['other_name'],@$passport_application[0]['middle_name'],$passport_application[0]['surname']);  ?>
        <?php } ?>
    </dd>
   

  </dl>
  <dl>
    <dt><label >Gender:</label ></dt>
    <dd>
      <?php if($type=='passport'){ ?>
        <?php echo $passport_application[0]['gender_id']; ?>
        <?php } else if($type=='visa'){ ?>
        <?php echo $passport_application[0]['gender']; ?>
        <?php } else if($type=='freezone') { ?>
        <?php echo $passport_application[0]['gender']; ?>
        <?php } ?>
    </dd>
  </dl>
  <dl>
    <dt><label >Date of birth:</label ></dt>
    <dd style="width:453px; "><?php $datetime = date_create($passport_application[0]['date_of_birth']); echo date_format($datetime, 'd/F/Y'); ?>&nbsp;</dd>

  </dl>
  <dl>
    <dt><label >Email:</label ></dt>
    <dd><?php if($passport_application[0]['email']!='') echo $passport_application[0]['email']; else echo "--"?>&nbsp;</dd>
  </dl>
  <?php if($type=='passport'){?>
  <dl>
    <dt><label >Country Of Origin:</label ></dt>
    <dd><?php echo $passport_application[0]['cName']; ?>&nbsp;</dd>
  </dl>
  <?php if($passport_application[0]['sName']!='') { ?>
  <dl>
    <dt><label >State of origin:</label ></dt>
    <dd><?php echo $passport_application[0]['sName']; ?>&nbsp;</dd>
  </dl>
  <?php } ?>
  <dl>
    <dt><label >Occupation:</label ></dt>
    <dd><?php if($passport_application[0]['occupation']!=""){ echo $passport_application[0]['occupation'];}else{ echo '--';} ?>&nbsp;</dd>
  </dl>
  <?php  } ?>
</fieldset>

<fieldset class="bdr">
  <?php echo ePortal_legend("Application Information"); ?>
  <?php if($type=='passport'){?>
  <dl>
    <dt><label >Passport type:</label ></dt>
    <dd><?php echo $passport_application[0]['passportType']; ?>&nbsp;</dd>
  </dl>
  <?php  } ?>
<?php if(isset($passport_application[0]['PassportApplicationDetails'])) { ?>
  <dl>
    <dt><label >Request Type:</label ></dt>
    <dd><?php echo $passport_application[0]['PassportApplicationDetails']['request_type_id']; ?>&nbsp;</dd>
  </dl>
  <?php } ?>
  <dl>
    <dt><label >Date:</label ></dt>
    <dd><?php $applicationDatetime = date_create($passport_application[0]['created_at']); echo date_format($applicationDatetime, 'd/F/Y');?>&nbsp;</dd>
  </dl>

  <dl>
    <dt><label >Application Id:</label ></dt>
    <dd><?php echo $passport_application[0]['id']; ?>&nbsp;</dd>
  </dl>
  <dl>
    <dt><label >Reference No:</label ></dt>
    <dd><?php echo $passport_application[0]['ref_no']; ?>&nbsp;</dd>
  </dl>
</fieldset>
<fieldset class="bdr">
  <?php echo ePortal_legend("Processing Information"); ?>

  <dl>
    <dt><label> <?php if($type =='passport'){ echo 'Country:'; } else{ echo 'Applying Country:';} ?></label ></dt>
    <dd><?php if($type=='passport'){echo $passport_application[0]['passportPCountry']; }else {echo
    $passport_application[0]['CurrentCountry']['country_name']; }?>&nbsp;</dd>
  </dl><?php if($type=='passport'){?>
  <dl>
    <dt><label >State:</label ></dt>
    <dd><?php if($passport_application[0]['passportPState']!='')echo $passport_application[0]['passportPState']; else echo 'Not Applicable'; ?>&nbsp;</dd>

  </dl><?php }?>


  <dl>
    <dt><label ><?php if($type =='passport'){ echo 'Embassy:'; } else if($type == "visa") { echo 'Applying Embassy:';}else {echo "Center:" ;} ?></label ></dt>
    <dd style="width:400px;"><?php if($type=='passport'){if($passport_application[0]['passportPEmbassy']=='') echo 'Not Applicable'; else echo $passport_application[0]['passportPEmbassy']; }else if($type == 'freezone'){ if($passport_application[0]['center'] == ''){echo 'Not Applicable'; }else{echo $passport_application[0]['center'];}}else{
    echo $passport_application[0]['VisaApplicantInfo']['EmbassyMaster']['embassy_name']; }?>&nbsp;</dd>
     </dl>
  <?php if($type=='passport'){?>
  <dl>
    <dt><label >Passport Office:</label ></dt>
    <dd><?php if($passport_application[0]['passportPOffice']!='') echo $passport_application[0]['passportPOffice']; else echo 'Not Applicable'; ?>&nbsp;</dd>
  </dl>
  <?php }?>


  <dl>
    <dt><label >Interview Date:</label ></dt>
    <dd>
      <?php
      if($passport_application[0]['ispaid'] == 1)
      {
          if($type =='passport'){
              if(($passport_application[0]['paid_local_currency_amount'] == 0) && ($passport_application[0]['paid_dollar_amount'] == 0))
              {
                  echo "Check the Passport Office / Embassy / High Commission";
              }else
              {
                  $interviewDatetime = date_create($passport_application[0]['interview_date']); echo date_format($interviewDatetime, 'd/F/Y');
              }
          }
          else{
              if(($passport_application[0]['paid_naira_amount'] == 0) && ($passport_application[0]['paid_dollar_amount'] == 0))
              {
                  echo "Check the Passport Office / Embassy / High Commission";
              }else
              {
                  $interviewDatetime = date_create($passport_application[0]['interview_date']); echo date_format($interviewDatetime, 'd/F/Y');
              }
          }
      }else{
        echo "Available after Payment";
      }
      ?></dd>
  </dl>

</fieldset>
<?php
$isPaid = $passport_application[0]['ispaid'];
?>





<div class="pixbr XY20">

<center id="multiFormNav">





<div align="center">
  <table align="center" class="nobrd"><tr>
      <td align="center">

          <input style="*text-align:center;" type="button" class="normalbutton"  onclick="javascript:history.go(-1)"  value="Back">
        
        
      </td>
    </tr>

</table></div>



</center>

</div>

</div>


<input type ="hidden" value="<?php echo $getAppIdToPaymentProcess;?>" name="appDetails" id="appDetails"/>
</form>
<br>


  </div>
      </div>
<div class="content_wrapper_bottom"></div>