<!--
New Template added to edit user transaction details
-->
<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
<div class="content_container">
<?php include_partial('global/innerHeading',array('heading'=>'Edit User Transaction Limit'));?>
<div class="clear"></div>
<div class="tmz-spacer"></div>
<?php include_partial('edit', array('form' => $form,'user_id' => $user_id, 'page' => $page)) ?>
