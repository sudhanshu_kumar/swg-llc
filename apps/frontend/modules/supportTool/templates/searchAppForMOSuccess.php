<?php use_helper('Form');
include_partial('global/innerHeading',array('heading'=>'Search Application For Money Order'));
?>
<script>
  function validateForm()
  {
    if($('#app_type').val() == '')
    {
      alert('Please select application type.');
      $('#app_type').focus();
      return false;
    }
    else if($('#app_id').val() == '')
    {
      alert('Please insert application id.');
      $('#app_id').focus();
      return false;
    }
   
    
    if($('#app_id').val() != "")
    {
      if(isNaN(document.getElementById('app_id').value))
      {
        alert('Please insert only numeric value.');
        document.getElementById('app_id').value = "";
        document.getElementById('app_id').focus();
        return false;
      }

    }
  }
</script>

<div class="global_content4 clearfix">
<div class="brdBox">
 <div class="clearfix"/>
<?php echo form_tag('supportTool/searchAppForMO',array('name'=>'app_search_form','class'=>'', 'method'=>'get','id'=>'app_search_form','onSubmit'=>'return validateForm()')) ?>

<div class="wrapForm2">
<?php
$sf = sfContext::getInstance()->getUser();
if($sf->hasFlash('notice')){ ?>
    <div id="flash_notice" class="alertBox" >
<?php
    echo nl2br($sf->getFlash('notice'));
?>
</div><br/>
<?php }?>
            <table width="50%">
		<?php   
                $app_type = array('' => 'Select Application Type', 'P' => 'Passport', 'V' => 'Visa', 'F' => 'Free Zone');
                echo formRowComplete('Application Type<span style="color:red">*</span>',select_tag('app_type', options_for_select($app_type), 0)); ?>
		
                <?php               
                $app_id = '';
                echo formRowComplete('Application Id<span style="color:red">*</span>',input_tag('app_id', $app_id, array('size' => 20, 'maxlength' => 20, 'class'=>'txt-input'))); ?>

		
                <tr>
                    <td>&nbsp;</td>
                    <td><div class="lblButton">
                            <input type="hidden" name="submitFlag" id="submitFlag" value="Submit" />
                            <?php   echo submit_tag('Search',array('class' => 'button')); ?>
                        </div>
                        <div class="lblButtonRight">
                            <div class="btnRtCorner"></div>
                    </div></td>
                </tr>
            </table>
        </div>
      </div>
    </div>
