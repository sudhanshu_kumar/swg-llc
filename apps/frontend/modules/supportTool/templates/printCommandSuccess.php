<?php use_javascript('jquery.printElement.js'); ?>
<?php //include_partial('global/innerHeading',array('heading'=>"Applicant's Details"));?>
<link rel="stylesheet" type="text/css" media="print" href="<?php  echo stylesheet_path('printPopUpMessage');?>" />
<script>
/* Start Ashwani This function is using jquery print  */
   function printSelection(){
     printElem({});
   }
   function printElem(options){
     document.getElementById("main_payment_slip").style.display='';
     $('#main_payment_slip').printElement(options);
     document.getElementById("main_payment_slip").style.display='none';
     document.getElementById("paymentbutton_close").style.display='';
   }

  
  function showDiv(div) 
  {
        $.post("<?php echo url_for('supportTool/printNISPaymentReceipt') ?>", {orderId: <?php echo $orderId; ?>},
          function(data){
            $('#main_payment_slip').html(data);
            printSelection();
            }
        );
  }
</script>
<div id="main_payment_slip" style="display:none"></div>
<div class="comBox" width="70%">
  <div id="div1" align="center"><br>
    <h2>PLEASE NOTE: PRINT NIS APPLICATION PAYMENT SLIP TO PRESENT IN SELECTED PASSPORT OFFICE FOR FURTHER PROCESSING.</h2>
  </div>
  <div  align="center" id="div4">
    <input type="button" name="paymentbutton" id ="paymentbutton" class="paymentbutton" value="Print & Continue" onclick="showDiv();">
    <input type="button" name="paymentbutton_close" id ="paymentbutton_close" style="display:none" class="paymentbutton" value="Close" onclick="parent.commentwindow.hide();">
  </div>
</div>
<script>
  $(document).ready(function()
  {
    $('.global_header').removeClass();
  });

  function closeWindow()
  {
      alert('close');
      hidePop();
  }
</script>