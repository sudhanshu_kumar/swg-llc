<script type="text/javascript">
  function disableForm(frm){

    if(validate_frm(frm)){
      $('#submit').attr('disabled',true);
      return true;
    }else{
      return false;
    }
  }

  function   findDuplicate(appType, appId){
    var url = "<?= url_for("supportTool/findDuplicateNisApplication"); ?>";
    $("#application_div").hide();
    $('#result_div').html('<?php echo image_tag('../images/ajax-loader.gif'); ?>');
    $("#result_div").show();

    $.get(url, { id: appId, appType: appType},
    function(data){

      //$("#flash_notice").html('<span>Bank '+status+' successfully</span>');
      $("#application_div").html(data);
      $("#application_div").show();
      $("#application_div").focus();
      $("#result_div").hide();
      //window.location.href = "URL";
      //setTimeout( "refresh()", 2*1000 );
    });
  }

  function validate_form(){
    var orderNo = $('#order_no').val();
    if(orderNo == ''){
      alert("Please enter SW Global LLC order number");
      $('#order_no').focus();
      return false;
    }
    if(orderNo != ''){
      if(isNaN(orderNo)){
        alert("Please enter only numeric value.");
        $('#order_no').val('') ;
        $('#order_no').focus();
        return false;
      }
    }
  }
</script>
<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">
<?php
use_helper('Form');
use_helper('Pagination');
include_partial('global/innerHeading',array('heading'=>'Search NIS Application By SW Global LLC Order Number'));
?>
  <div class="clear"></div>
<div class="tmz-spacer"></div>


      <?php echo form_tag('supportTool/searchOrderNo',array('name'=>'order_search_form','class'=>'', 'method'=>'post','id'=>'order_search_form' ,'onsubmit'=>'return validate_form()')) ?>

   
        <?php
        $sf = sfContext::getInstance()->getUser();
        if($sf->hasFlash('notice')){ ?>
        <div id="flash_notice" class="alertBox" >
          <?php
          echo nl2br($sf->getFlash('notice'));
          ?>
        </div><br/>
        <?php }?>

     
          <?php
          $sf = sfContext::getInstance()->getUser();
          if($sf->hasFlash('error')){ ?>
          <div id="flash_error" class="alertBox" >
            <?php
            echo nl2br($sf->getFlash('error'));
            ?>
          </div><br/>
          <?php }?>

<div class="clear"></div>
          <table width="100%" cellpadding="0" cellspacing="0" border="0">
            <?php
            $val = '';
//            if($sf_params->has('order_no')){
//              $orderNumber = Settings::decryptInput($sf_params->get('order_no'));
//            }
            //                die($val);
            echo formRowComplete('Order Number<span style="color:red">*</span>',input_tag('order_no', $ipayOrderNo, array('class'=>'txt-input'))); ?>

            <tr>
              <td>&nbsp;</td>
              <td><div class="lblButton">
                  <?php   echo submit_tag('Search',array('class' => 'normalbutton')); ?>
                </div>
                <div class="lblButtonRight">
                  <div class="btnRtCorner"></div>
              </div></td>
            </tr>
          </table>
 

      <br>


      <?php if($isFound) {
        $i = 1;
        ?>
    
    <?php if($notRefundMessage != '') {?>
    <div class ="alertBox">
    <?php echo "Note: ".$notRefundMessage; ?>
    </div>
    <?php } ?>
    <br/>
  <div class="clear"></div>
 
        <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
          <tr>
            <td class="blbar" colspan="13" align="right">
            <div style="float:left">NIS Application Details By SW Global LLC Order Number</div>
            <span>Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total  <b><?php echo $pager->getNbResults(); ?></b>  results</span></td>
          </tr>
          <tr>
            <td width="4%" ><span class="txtBold">S. No.</span></td>
            <td width="9%"><span class="txtBold">Application type</span></td>
            <td width="9%"><span class="txtBold">App Id</span></td>
            <td width="12%"><span class="txtBold">Reference No</span></td>
            <td width="14%"><span class="txtBold">Applicant Name</span></td>
            <td width="9%"><span class="txtBold">Date of Birth</span></td>
            <td width="6%"><span class="txtBold">NIS Payment Status</span></td>
            <td width="6%"><span class="txtBold">Paid On</span></td>
            <td width="10%"><span class="txtBold">Email Id</span></td>
            <td width="10%"><span class="txtBold">Order Number</span></td>
            <td width="10%"><span class="txtBold">Payment Gateway</span></td>
            <td width="10%"><span class="txtBold">Country</span></td>
            <td width="6%"><span class="txtBold">Actions</span></td>
          </tr>
          <tbody>
            <?php $showButtons = false;
            $isVettedFlag = false;
            if($pager->getNbResults()>0)
            {
//              $finalStatus = $nisHelper->isAllReadyRefunded($sf_params->get('order_no'));
//              $finalStatus= $finalStatus->getRawValue();
//              $fStatus = null;
//              if(isset ($finalStatus) && is_array($finalStatus) && count($finalStatus)>0){
//                $fStatus = $finalStatus['action'];
//              }
              foreach ($pager->getResults() as $result):
              //            echo "<pre>";print_r(get_class_methods($result));die;
              $app['passport_id'] = $result['passport_id'];
              $app['visa_id'] = $result['visa_id'];
              $app['freezone_id'] = $result['freezone_id'];
              $app['visa_arrival_program_id'] = $result['visa_arrival_program_id'];

              ## Fetching application details...
              $appDetails = $appObj->getApplicationDetails($app);

              ## Fetching application processing country...
              $processingCountry = Functions::getProcessingCountryByAppIdAndType($appDetails['id'], $appDetails['app_type']);
              
              $finalStatus = $nisHelper->isAllAppReadyRefunded($ipayOrderNo,$appDetails['id']);
              $finalStatus= $finalStatus->getRawValue();
              $fStatus = null;
              if(isset ($finalStatus) && is_array($finalStatus) && count($finalStatus)>0){
                  $fStatus = $finalStatus['action'];
              }

              ?>
            <tr>
              <td width="4%"><?php echo $i;?></td>
              <td width="9%"><span><?php if($appDetails['app_type'] == 'vap'){ echo sfConfig::get('app_visa_arrival_title');} else { echo strtoupper($appDetails['app_type']);} ?></span></td>
              <td width="10%"><span><?php echo $appDetails['id'];?></span></td>
              <td width="9%"><span><?php echo $appDetails['ref_no'];?></span></td>
              <td width="14%"><span><?php echo $appDetails['name'];?></span></td>
              <td width="9%"><span><?php echo date_format(date_create($appDetails['dob']), 'Y-m-d');?></span></td>
              <?php if(isset ($fStatus) && $fStatus!=null){ ?>

              <td width="9%"><span><?php echo strtoupper($fStatus);?></span></td>
              <?php } else{ $showButtons = true;?>

              <td width="9%"><span><?php echo $appDetails['status'];?></span></td>

                    <?php  }  ?>

              <td width="9%"><span><?php if($appDetails['status'] != 'New') echo date_format(date_create($ipay4meOrderDetails[0]['paid_at']), 'Y-m-d'); else echo "--"; ?></span></td>
              <td width="10%"><span><?php echo str_replace('@',' @',$ipay4meOrderDetails[0]['email']);?></span></td>
              <td width="10%"><span><?php echo $ipayOrderNo;?></span></td>
              <td width="10%"><span><?php echo $gatewayName;?></span></td>
              <td width="10%"><span><?php echo ($processingCountry != '')?$processingCountry:'--';?></span></td>
              <td width="9%"><span><a href="javascript:void(0);" onclick="findDuplicate('<?= $appDetails['app_type']; ?>','<?= $appDetails['id']; ?>')" title="Find Duplicate Applications" ><?php echo image_tag("/images/duplicate_app.gif"); ?></a></span></td>



            </tr>
            <?php
            $i++;
            endforeach; ?>

            <tr>
              <td class="blbar" colspan="13" height="25px" align="right">
                <div class="paging pagingFoot">
                  <?php
                  echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName()).'?order_no='.$ipayOrderNo.'&usePager=true','detail_div');
                  ?>
                </div>
              </td>
            </tr>
            <?php } else { ?>
            <tr><td  align='center' class='red' colspan="13">No Results found</td></tr>
            <tr><td class="blbar" colspan="13" height="25px" align="right"></td></tr>
            <?php } ?>
          </tbody>

        </table>
        <br>

  <?php if($showButtons)
  {
    if($pager->getNbResults()>0)
    {?>
        <div align="center">
            <?php   echo button_to('Refund',url_for("supportTool/orderAction?pAction=".Settings::encryptInput('Refund')."&order_no=".Settings::encryptInput($ipayOrderNo)),array('class' => 'normalbutton')); ?>

            <?php   echo button_to('Refund and Block',url_for("supportTool/orderAction?pAction=".Settings::encryptInput('Refund and Block')."&order_no=".Settings::encryptInput($ipayOrderNo)),array('class' => 'normalbutton')); ?>

            <?php   //echo button_to('Chargeback',url_for("supportTool/orderAction?pAction=".Settings::encryptInput('Chargeback')."&order_no=".Settings::encryptInput($ipayOrderNo)),array('class' => 'normalbutton')); ?>
             <input style="" type="button" class="normalbutton" id="multiFormSubmit" value="Chargeback" onclick="checkVettedApp()";
        </div>
        <?php }
    }
   } ?>
    
      <br>
      <br>
      <br>

      <div id="result_div" style="width:500px;" align="center"></div>
      <div id="application_div" style="display:none;"></div>
</div>
</div>
<div class="content_wrapper_bottom"></div>


<script>
    // Function for getting confirmation for Vetted Applications
  function checkVettedApp(){
      var isVettedFlag = '<?php echo (isset($isVettedFlag))?$isVettedFlag:''; ?>';
      var app_status = '<?php echo (isset($appDetails['status']))?$appDetails['status']:''; ?>';
      var chargeBackUrl = '<?php echo url_for("supportTool/orderAction?pAction=".Settings::encryptInput('Chargeback')."&order_no=".Settings::encryptInput($ipayOrderNo)); ?>';
      if(app_status == "Vetted"){          
          if(confirm("Application(s) are vetted in this order. Do you really want to Chargeback?") == true){            
            window.location = chargeBackUrl;
          }
          else{
              return false;
          }
      }else{
         window.location = chargeBackUrl;
      }
  }
</script>