
<script>
  function validateForm()
  {
    if(document.getElementById('acoount_email').value == '')
    {
      alert('Please Insert Email Address.');
      document.getElementById('acoount_email').focus();
      return false;
    }

    if(document.getElementById('acoount_email').value != "")
    {

       var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
       var address = $("#acoount_email").val();
       if(reg.test(address) == false) {
          alert('Wrong Email Address');
          $("#acoount_email").focus();
          return false;
       }
    }
  }
</script>
<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">
  <?php
use_helper('Pagination');
use_helper('Form');
include_partial('global/innerHeading',array('heading'=>'Search Application By SW Global LLC Account Email Address'));
?>
  <div class="clear"></div>
<div class="tmz-spacer"></div>
<?php echo form_tag('supportTool/searchBySwLlcAccount',array('name'=>'order_search_form','class'=>'', 'method'=>'post','id'=>'order_search_form','onSubmit'=>'return validateForm()')) ?>


<?php
$sf = sfContext::getInstance()->getUser();
if($sf->hasFlash('notice')){ ?>
    <div id="flash_notice" class="alertBox" >
<?php
    echo nl2br($sf->getFlash('notice'));
?>
</div><br/>
<?php }?>
<?php
$sf = sfContext::getInstance()->getUser();
if($sf->hasFlash('error')){ ?>
    <div id="flash_error" class="alertBox" >
<?php
    echo nl2br($sf->getFlash('error'));
?>
</div><br/>
<?php }?>

            <table width="99%" cellpadding="0" cellspacing="0" border="0">
                <?php

                $app_id = (isset($_GET['acoount_email']))?$_GET['acoount_email']:"";
                echo formRowComplete('Email address<span class="red">*</span>',input_tag('acoount_email', $app_id, array('size' => 40, 'maxlength' => 50,'class'=>'txt-input'))); ?>

                <tr>
                    <td>&nbsp;</td>
                    <td>
                            <?php   echo submit_tag('Search',array('class' => 'normalbutton')); ?>

              </td>
                </tr>
            </table>
   <br>
   <?php if($isFound): ?>
   <table width="75%">
   <tr>
   <td width="25%">
   Name
   </td>
   <td>
   <?= $name; ?>
   </td>
   </tr>
   <tr>
   <td width="25%">
   Phone Number
   </td>
   <td>
   <?= $phone; ?>
   </td>
   </tr>
   <tr>
   <td width="25%">
   Email
   </td>
   <td>
   <?= $email; ?>
   </td>
   </tr>

   <tr>
   <td width="25%">
   Address
   </td>
   <td>
   <?= $address; ?>
   </td>
   </tr>
   </table>
<br>
<br>
    <div id="nxtPage">
      <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
            <tr>
                <td class="blbar" colspan="10" align="right">
                <div class="float_left">Application Details By SW Global LLC Account Email Address</div>
                <span>Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total  <b><?php echo $pager->getNbResults(); ?></b>  results</span></td>
            </tr>
            <tr>
                <td width="4%" ><span class="txtBold">S. No.</span></td>
                <td width="9%"><span class="txtBold">Order Number</span></td>
                <td width="9%"><span class="txtBold">Payment Gateway</span></td>
                <td width="9%"><span class="txtBold">Date Of Transaction</span></td>
                <td width="12%"><span class="txtBold">Transaction Amount</span></td>
                <td width="14%"><span class="txtBold">Transaction Status</span></td>
            </tr>
            <tbody>
            <?php
            $i = $pager->getFirstIndice();
            $url = url_for("supportTool/searchOrderNo");
            if($pager->getNbResults()>0)
            {
            foreach ($pager->getResults() as $appDetails):

            
            ?>
            <tr>
                <td width="4%"><?php echo $i;?></td>
                <td width="9%"><span><a href="<?= $url."?order_no=".(Settings::encryptInput($appDetails['order_no'])) ;?>" ><?= $appDetails['order_no'] ?></a></span></td>
               <?php
               $gatewayId = $appDetails['gatewayId'];
               $gatewayName = Doctrine_Core::getTable('Gateway')->getDisplayName($gatewayId);
               ?>
               <td width="9%"><span><? echo $gatewayName; ?></span></td>
               <td width="9%"><span><?php echo date_format(date_create($appDetails['trans_date']), 'Y-m-d');?></span></td>

               <?php 
               $currencyId =  $appDetails['currency'];
               if($currencyId == '1'){
                   $amount = $appDetails['amount'];
               }else{
                   $amount = $appDetails['convert_amount'];
               }
               $currencySymbol = CurrencyManager::currencySymbolByCurrencyId($currencyId);

               ?>
                <td width="10%"><span><?php echo format_amount($amount,1,0,$currencySymbol);?></span></td>
                <?php
                $status = '';
                if(isset ($appDetails['pay_status']) && $appDetails['pay_status']!='')
                {
                  if($appDetails['pay_status'] == 0){
                      $status = "Paid";
                  }else if($appDetails['pay_status'] == 1){
                      $status = "Notpaid";
                  }else if($appDetails['pay_status'] == 2){
                      $status = "Refunded";
                  }else if($appDetails['pay_status'] == 3){
                      $status = "Charge-back";
                  }
                }

                ?>

                <td width="9%"><span><?php echo $status;?></span></td>
                
            </tr>
            <?php
            $i++;
            endforeach; ?>
            
            <tr>
                <td class="blbar" colspan="10" height="25px" align="right">
                <div class="paging pagingFoot">
                    <?php
                    echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName()).'?acoount_email='.$sf_params->get('acoount_email').'&usePager=true','detail_div');
                    ?>
                    </div>
                </td>
            </tr>
            <?php } else { ?>
                    <tr><td  align='center' class='red' colspan="10">No Results found</td></tr>
            <?php } ?>
          </tbody>

        </table>
        <br>
        </div>

<?php endif; ?>

</div></div>
<div class="content_wrapper_bottom"></div>
