
<script>

 function validateForm(){

     if(jQuery.trim($('#money_order').val()) ==''){
         alert('Please enter Money Order Number.');
         $('#money_order').focus();
         return false;
     }
//     if(jQuery.trim($('#amount_currency').val()) ==''){
//         alert('Please select Amount Currency.');
//         $('#amount_currency').focus();
//         return false;
//     }
     
     if(jQuery.trim($('#received_amount').val()) ==''){
        alert('Please enter amount which you have recieved for this money order.');
        $('#received_amount').focus();
        return false;
     }

     if(isNaN($('#received_amount').val())){
        alert('Please enter amount in numeric only.');
        $('#received_amount').focus();
        return false;
     }

 }
  function validate_form(fmobj)
  {
    var sel= false;
    for (var i=0;i<fmobj.elements.length;i++)
    {
      var e = fmobj.elements[i];
      if ((e.type=='checkbox') && (!e.disabled))
      {
        if(e.checked == true)
        {
          sel= true;
          break;
        }
      }else if((e.type=='checkbox') && (e.disabled))
      {
        sel= true;
        break;
      }
    }

    if(sel)
    {

      return true;
    }
    else
    {
      $('#check_box_error').html("Please select tracking number");

      return false;
    }
    return false;
  }

    function checkAll(fmobj, chkAll)
  {
    var amount = 0;
    $('#amt_to_paid_error').html('');
    for (var i=0;i<fmobj.elements.length;i++)
    {
      var e = fmobj.elements[i];
      if ((e.name == chkAll) && (e.type=='checkbox') && (!e.disabled))
      {
        e.checked = fmobj.chk_all.checked;
        if(fmobj.chk_all.checked)
        {
          amount = parseInt(amount)+parseInt($("#"+e.id+"amt").html());
        }
        $("#amt_to_paid").val(amount);
        $("#disp_amt").html(amount+".00");
      }
    }
  }


  function UncheckMain(fmobj, objChkAll, chkElement,obj)
  {
    var boolCheck = true;


    if(objChkAll.checked==false)
    {
      for(var i=0;i<fmobj.elements.length;i++)
      {
        if(fmobj.elements[i].name.indexOf(""+chkElement+"")!="-1" && fmobj.elements[i].checked==false)
        {
          boolCheck = false;
          break;
        }
      }
      if(boolCheck==true)
        objChkAll.checked=true;
    }
    else
    {
      objChkAll.checked=false;
    }
  }

</script>


<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">
  <?php
use_helper('Pagination');
use_helper('Form');
include_partial('global/innerHeading',array('heading'=>'Money Order Info'));
?>
    <div class="clear"></div>
    <div class="tmz-spacer"></div>

    <?php echo form_tag('supportTool/searchByMoneyOrder',array('name'=>'order_search_form','class'=>'', 'method'=>'post','id'=>'order_search_form', 'onSubmit'=>'return validateForm()')) ?>


      <?php
      $sf = sfContext::getInstance()->getUser();
      if($sf->hasFlash('notice')){ ?>
      <div id="flash_notice" class="alertBox" >
        <?php
        echo nl2br($sf->getFlash('notice'));
        ?>
      </div><br/>
      <?php }?>


        <?php
        $sf = sfContext::getInstance()->getUser();
        if($sf->hasFlash('error')){ ?>
        <div id="flash_error" class="alertBox" >
          <?php
          echo nl2br($sf->getFlash('error'));
          ?>
        </div><br/>
        <?php }?>

        <table width="100%">
          <?php

          $moneyOrder = (isset($_REQUEST['money_order']))?$_REQUEST['money_order']:"";
          $receivedAmount = (isset($_REQUEST['received_amount']))?$_REQUEST['received_amount']:'';
          //$amountCurrency = (isset($_REQUEST['amount_currency']))?$_REQUEST['amount_currency']:'';
          echo formRowComplete('Money Order Number <span style="color:red">*</span>',input_tag('money_order', $moneyOrder, array('size' => 27, 'maxlength' => 30,'class'=>'txt-input','required'=>'true')),'','money_order','detail_error','money_order_row');
          //echo formRowComplete('Amout Currency <span style="color:red">*</span>',select_tag('amount_currency', $option_tags = array('' => 'Please Select','dollar' => 'Dollar', 'pound' => 'GBP'), $options = array()),'','','','');
          echo formRowComplete('Received Money Order Amount <span style="color:red">*</span>',input_tag('received_amount', $receivedAmount, array('size' => 27, 'maxlength' => 30,'class'=>'txt-input')),'','received_amount','detail_error','received_amount_row');          
          echo "</tr>";
          ?>

          <tr valign="top">
            <td height="30" valign="top">&nbsp;</td>
            <td height="30" valign="top">
                <?php   echo submit_tag('Search',array('class' => 'normalbutton')); ?>
        
              </td>
          </tr>
        </table>




  </form>
  <br>





  <?php if($isFound){?>

  
        <?php echo form_tag('supportTool/updateMoneyOrder',array('name'=>'MOUpdateFrm','class'=>'', 'method'=>'post','id'=>'MOUpdateFrm','onsubmit'=>'return validate_form(document.MOUpdateFrm)')) ?>

      <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" id="hdn_datatable">

      <?php if($isSearch) {  ?>

        <tr>
          <td class="blbar" colspan="10" align="right">
            <div style="float:left">Tracking Number</div>
            <!-- <span>Showing <b><?php //echo $pager->getFirstIndice() ?></b> - <b><?php //echo $pager->getLastIndice() ?></b> of total  <b><?php// echo $pager->getNbResults(); ?></b>  results</span> -->
          </td>
        </tr>
         <?php $j = $pager->getFirstIndice();
         $pound = 5;
          if($pager->getNbResults()>0){
            foreach ($pager->getResults() as $details):

                if($details['currency'] == $pound){
                    $amount = $details['convert_amount'];
                    $color = ($details['convert_amount']==trim($receivedAmount))?'':'RED';                    
                }else{
                    $amount = $details['amount'];
                    $color = ($details['amount']==trim($receivedAmount))?'':'RED';                    
                }
                $currencySymbol =CurrencyManager::currencySymbolByCurrencyId($details['currency']);
                
                $txtColor ='';
                if($color !=''){
                    $txtColor ='white';
                }
           ?>
        <tr bgcolor="#eeeeee">

          <td width="5%"><span class="txtBold">S.No.</span></td>
          <td width="12%"><span class="txtBold">Association Date</span></td>
          <td width="12%"><span class="txtBold">Money Order Serial Number</span></td>          
          <?php if($details['courier_flag']=='Yes') { ?>
          <td width="12%"><span class="txtBold">Courier Service</span></td>
          <?php } else { ?>
          <td width="12%"><span class="txtBold">Regular Mail</span></td>
          <?php } ?>
          <td width="10%"><span class="txtBold">Phone</span></td>
          <td width="12%"><span class="txtBold">Address</span></td>
          <td width="10%"><span class="txtBold">Amount</span></td>
          <td width="12%" ><span class="txtBold" id="maincheck">Payment received <br> <input type="checkbox" name="chk_all" id="chk_all" onclick="checkAll(document.MOUpdateFrm,'hdn_mo_id[]');" >&nbsp;&nbsp;(Check All)</span></td>

        </tr>
        <tbody>
         
          <tr bgcolor="<?php echo $color; ?>">
            <td width="5%" style="color:<?php echo $txtColor; ?>;"><span><?= $j ?></span></td>
            <td width="12%" style="color:<?php echo $txtColor; ?>;"><span><?= $details['associated_date'] ?></span></td>
            <td width="12%" style="color:<?php echo $txtColor; ?>;"><span><a href="<?php echo url_for('supportTool/moneyOrderDetails?money_order_id='.(Settings::encryptInput($details['id'])));?>"><font color="<?php echo $txtColor; ?>"><?= $details['moneyorder_number'] ?></font></a></span></td>
            <td width="12%" style="color:<?php echo $txtColor; ?>;"><span><?php echo 'Yes';?></span></td>
            <td width="10%" style="color:<?php echo $txtColor; ?>;"><span><?php  if($details['phone']!=''){ echo $details['phone']; } else { echo '- -'; } ?></span></td>
            <td width="12%" style="color:<?php echo $txtColor; ?>;"><span><?php if($details['address']!=''){ echo $details['address']; } else {echo '- -'; } ?></span></td>
            <td width="10%" style="color:<?php echo $txtColor; ?>;"><span><?= format_amount($amount,1,0,$currencySymbol) ?></span></td>
            <td width="12%" style="color:<?php echo $txtColor; ?>;"><input type="checkbox" id="hdn_mo_id_<?php echo $details['id']; ?>" name="hdn_mo_id[]" value="<?php echo $details['id']; ?>"  OnClick="UncheckMain(document.MOUpdateFrm,document.MOUpdateFrm.chk_all,'hdn_mo_id',<?php echo $details['id']; ?>);">
            </td>
          </tr>
      <?php $j++;
          endforeach; ?>
         <?php if($amount == trim($receivedAmount)) { ?>
         <tr>

              <td colspan="10" align="center">
              <div id="check_box_error" style="text-align:left;color:red;">&nbsp;</div>
                <?php echo submit_tag('Update',array('class' => 'loginbutton','width'=>'60px')); ?>
              </td>
          </tr>
          <?php } else { ?>
          <tr>
              <td colspan="10" align="center" class ="txtBold">
              <div id="check_box_error" style="text-align:left;color:red;">&nbsp;</div>
                <?php echo 'Amount of money order do not match with amount entered by you.'; ?>
              </td>
          </tr>
          <?php } ?>

          <?php } else { ?>
          <tr>
            <td align="center" class="red" colspan="10">No record Found</td>
          </tr>
          <?php } ?>
      <tr>
              <td class="blbar" colspan="50" height="25px" align="right">
                <div class="paging pagingFoot">
                  <?php
                  echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName()).'?email='.$sf_params->get('email'),'detail_div');
                  ?>
                </div>
              </td>
            </tr>
            <?php } else { ?>

            <tr>
                <td align="center" class ="red">There are <?php echo $totalMORequest;?> Pending Money Order Request(s).</td>
            </tr>

           <?php } ?>
        </tbody>

      </table>
      </form>



  <?php }
  ?>


   </div>
    </div>
<div class="content_wrapper_bottom"></div>