<?php
if($moneyOrderDetail[0]['currency'] == '5'){
    $totalAmount = $moneyOrderDetail[0]['convert_amount'];    
}else{
    $totalAmount = $moneyOrderDetail[0]['amount'];    
}
$currencySymbol = CurrencyManager::currencySymbolByCurrencyId($moneyOrderDetail[0]['currency']);
?>

<script>
  function validate_form(fmobj){
    var sel= false;
    for (var i=0;i<fmobj.elements.length;i++)
    {
      var e = fmobj.elements[i];
      if ((e.type=='checkbox') && (!e.disabled))
      {
        if(e.checked == true)
        {
          sel= true;
          break;
        }
      }else if((e.type=='checkbox') && (e.disabled))
      {
        sel= true;
        break;
      }
    }

    if(sel)
    {
      var illegalChars = /\W/;
      var reg = /^[\-0-9a-zA-Z]+$/;
      var amtToPaid = $("#amt_to_paid").val();
      var moneyorderAmt = $("#moneyorder_amount").val();
      if(moneyorderAmt == '')
      {
        $("#amount_error").html("Please enter money order amount");
        return false;
      }
      //adding only numeric mo amount bug-id 29312
      else if(illegalChars.test(moneyorderAmt)){
        $("#amount_error").html("money order amount should be numeric");
        return false;
      }
      var moneyorderNumber = $("#moneyorder_moneyorder_number").val();
      if(moneyorderNumber == '')
      {
        $("#moneyorder_number_error").html("Please enter money order serial number");
        return false;
      }

      if(reg.test(moneyorderNumber) == false) {
        $("#moneyorder_number_error").html("White spaces and special characters are not allowed");
        $("#moneyorderNumber").val('');
        $("#moneyorderNumber").focus();
        return false;
      }

      //Round up decimal amount in amount validation
      if(Math.round(amtToPaid)!=Math.round(moneyorderAmt)){
        $("#amount_error").html("Amount to paid and money order amount should be same");
        return false;
      }
      return true;
    }
    else
    {
      $('#amt_to_paid_error').html("Please select at least one tracking number");
      $('#amt_to_paid_error').focus();
      return false;
    }
    return false;
  }

  function uniqueMoneyOrderNumber(moneyorder_number)
  {

    var url = "<?php echo url_for("mo_configuration/uniqueOrderNumber"); ?>";

    $.get(url, { moneyorder_number: moneyorder_number},
    function(data){
      alert(data);


    });
  }
  function checkAll(fmobj, chkAll)
  {
    $("#chk_all").attr('checked', 'checked');
    return false;

    var amount = 0;
    $('#amt_to_paid_error').html('');
    for (var i=0;i<fmobj.elements.length;i++)
    {
      var e = fmobj.elements[i];
      if ((e.name == chkAll) && (e.type=='checkbox') && (!e.disabled))
      {
        e.checked = fmobj.chk_all.checked;
        if(fmobj.chk_all.checked)
        {
          amount = parseInt(amount)+parseInt($("#"+e.id+"amt").html());
        }
        $("#amt_to_paid").val(amount);
        $("#disp_amt").html(amount+".00");
      }
    }
  }

  function UncheckMain(fmobj, objChkAll, chkElement,obj,amt)
  {

    $("#"+obj.id).attr('checked', 'checked');
    return false;

    var boolCheck = true;
    $('#amt_to_paid_error').html('');
    var amount = $("#amt_to_paid").val();
    if(amount ==''){
      amount = 0;
    }
    if($("#"+obj.id).is(':checked')){
      amount = parseInt(amount)+parseInt(amt);
      $("#amt_to_paid").val(amount);
      $("#disp_amt").html(amount+".00");

    }else{
      amount = parseInt(amount)-parseInt(amt);
      $("#amt_to_paid").val(amount);
      $("#disp_amt").html(amount+".00");

    }
    if(objChkAll.checked==false)
    {
      for(var i=0;i<fmobj.elements.length;i++)
      {
        if(fmobj.elements[i].name.indexOf(""+chkElement+"")!="-1" && fmobj.elements[i].checked==false)
        {
          boolCheck = false;
          break;
        }
      }
      if(boolCheck==true)
        objChkAll.checked=true;
    }
    else
    {
      objChkAll.checked=false;
    }
  }
  function disallocate(moneyorderId)
  {
    var answer = confirm('Do you want to Dis-Associate whole Momey Order?')
    var moneyorderId = moneyorderId
    if(answer)
    {
      var url = "<?php echo url_for('supportTool/disAllocate?moneyorderId='.$moneyorderId) ;?>";
      window.location.href = url;

    }

  }
</script>

<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">
<?php
use_helper('Form');
use_helper('Pagination');
include_partial('global/innerHeading',array('heading'=>'Update Money Order'));
?>
 <div class="clear"></div>
<div class="tmz-spacer"></div>
<?php echo form_tag('supportTool/updateMoneyOrderDetails',array('name'=>'order_search_form','class'=>'', 'method'=>'post','id'=>'searchApplicationForm' ,'onsubmit'=>'return validate_form(document.searchApplicationForm)','name'=>'searchApplicationForm')) ?>
<input type="hidden" name="userid" id="userid" value="<?php echo $userid; ?>" />
<input type="hidden" name="moneyOrderNumber1" id="moneyOrderNumber1" value="<?php echo $moneyOrderNumber; ?>" />
<input type="hidden" name="moneyOrderDetailId" id="moneyOrderDetailId" value="<?php echo $moneyOrderDetail[0]['id']; ?>" />
<input value="<?php echo $totalAmount;?>" type="hidden" name="amt_to_paid" id="amt_to_paid"  class="txt-input" readonly="readonly">

 
    <?php
    echo $form->renderGlobalErrors();
    $sf = sfContext::getInstance()->getUser();
    if($sf->hasFlash('notice')){ ?>
    <div id="flash_notice" class="alertBox" >
      <?php
      echo nl2br($sf->getFlash('notice'));
      ?>
    </div><br/>
    <?php }?>

    <div class="wrapForm2">
      <?php
      $sf = sfContext::getInstance()->getUser();
      if($sf->hasFlash('error')){ ?>
      <div id="flash_error" class="alertBox" >
        <?php
        echo nl2br($sf->getFlash('error'));
        ?>
      </div><br/>
      <?php }?>
    <?php if($isValid) { ?>
      <!--span><b>NOTE:</b><font color='red'> Please UnCheck the check box to Dis- Associate individually by clicking Update button OR Click below link to Dis-Associate Whole Money Order.</font> </span-->
      <!--div class="dis_allocate" width="10%" align="right"><a href="#" onclick="disallocate(<?php echo $moneyorderId; ?>)"><font color="#008900">Click Here to Dis-Associate Whole Money Order</font></a></div-->

      <table style="padding-top:150px" width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
        <tr>
          <td class="blbar" colspan="3" align="left">Tracking Number Details</td>
        </tr>
        <tr>
          <td width="10%" valign="top"><span class="txtBold" id="maincheck"> <input checked type="checkbox" name="chk_all" id="chk_all" onclick="checkAll(document.searchApplicationForm,'chk_fee[]');" >&nbsp;(Check All)</span></td>
          <td width="40%"><span class="txtBold">Tracking Number</span></td>
          <td width="40%" align="right"><span class="txtBold">Amount( <?php echo html_entity_decode($currencySymbol); ?> )</span></td>
        </tr>
        <tbody>
          <?php
          $i=1;
          foreach ($pendingtrackingNumbers as $result):
            if($result['currency'] == '5'){
                $trackAmount = $result['convert_amount'];
            }else{
                $trackAmount = $result['cart_amount'];
            }
          
          ?>
          <tr>
            <td width="10%"><span class="txtBold"><input checked type="checkbox" name="chk_fee[]" id="<?= "chk_fee".$i; ?>" value="<?= $result['id']."_".$trackAmount;?>" OnClick="UncheckMain(document.searchApplicationForm,document.searchApplicationForm.chk_all,'chk_fee',<?= "chk_fee".$i; ?>,'<?php echo $trackAmount;?>');" ></span></td>
            <td width="40%"><span><?php echo $result['tracking_number'];?></span></td>
            <td width="40%" align="right" id="<?= "chk_fee".$i."amt"; ?>"><?php echo $trackAmount;?></td>
          </tr>

            <?php
            $i++;
            endforeach; ?>
          <tr><td></td><td align="right"><b>Total (<?php echo html_entity_decode($currencySymbol); ?>)</b></td><td align="right"><div id="amt_to_paid_div"><span id="disp_amt"><?php echo $totalAmount;?></span></div></td></tr>
          <tr><td width="100%" colspan="3"><div class="red" id="amt_to_paid_error"></div></td></tr>

        </tbody>
      </table>
      <table width="100%">
        <tr>
          <td class="blbar" colspan="3" align="left">Enter Money Order Details</td>
        </tr>

        <tr>
          <td width="30%"><?php echo $form['amount']->renderLabel()." ( ".html_entity_decode($currencySymbol)." )"; ?><span class="red">*</span><br><small><i>(Kindly do not enter decimal places or <?php echo html_entity_decode($currencySymbol); ?> sign inside the box.)</i></small></td>
          <td><input type="text" id="moneyorder_amount" name="moneyorder[amount]" min_length="6" maxlength="32" class="txt-input" value="<?php
                       $amount = $totalAmount;
                       $amount = explode('.',$amount);
                       echo $amount[0];
                       ?>"/>
            <br>
            <div class="red" id="amount_error">
              <?php echo $form['amount']->renderError(); ?>
            </div>

          </td>
        </tr>


        <tr>
          <td>Money Order Serial Number<span class="red">*</span><br><small><i>(Kindly use only one Money Order Serial Number at a time.)</i></small></td>
          <td>
            <input type="text" id="moneyorder_moneyorder_number" name="moneyorder[moneyorder_number]" autocomplete="off" min_length="3" maxlength="30" class="txt-input"  value="<?php echo $moneyOrderDetail[0]['moneyorder_number'];?>"/>


            <br>
            <div class="red" id="moneyorder_number_error">
              <?php echo $form['moneyorder_number']->renderError(); ?>
            </div>

          </td>
        </tr>
        <tr>
          <td>Money Order Issuing Date<span class="red">*</span><br>(dd/mm/yyyy)</td>
          <td><?php
            if(!empty($moneyOrderDetail[0]['moneyorder_date']))
            {
              $date = $moneyOrderDetail[0]['moneyorder_date'];
              $date = explode('-',$date);
              $year = $date[0];
              $month = $date[1];
              $day = $date[2];

              echo $day."-".$month."-".$year;
            }
            ?>
            <br>
            <div class="red" id="moneyorder_date_error">
              <?php echo $form['moneyorder_date']->renderError(); ?>
            </div>

          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td><div class="lblButton">
              <?php   echo submit_tag('Update',array('class' => 'normalbutton')); ?>
            </div>
            <div class="lblButtonRight">
              <div class="btnRtCorner"></div>
          </div></td>
        </tr>
      </table>


      </form>
      <?php }else{?>

      <?php }?>
    </div>

<br>

</div>
</div>
<div class="content_wrapper_bottom"></div>