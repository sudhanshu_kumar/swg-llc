      <?php
      $sf = sfContext::getInstance()->getUser();
      if($sf->hasFlash('notice')){ ?>
      <div id="flash_notice" class="alertBox" >
        <?php
        echo nl2br($sf->getFlash('notice'));
        ?>
          </div>
          <br/>
      <?php }?>
        <?php
        $sf = sfContext::getInstance()->getUser();
        if($sf->hasFlash('error')){ ?>
        <div id="flash_error" class="alertBox" >
          <?php
          echo nl2br($sf->getFlash('error'));
          ?>
            </div>
            <br/>
        <?php }?>
        <div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">
  <?php use_helper('Form');
use_javascript('common');
include_partial('global/innerHeading',array('heading'=>'Application Detail and Order List by Application Id'));
?>
<div class="clear"></div>
<div class="tmz-spacer"></div>
 <div class="clear"></div>
          <table width="100%" border="0" cellpadding="0" cellspacing="0">
              <tr>
                <td class="blbar" colspan="10" align="left">NIS Application
              </tr>
              <tr>
                <td><span class="txtBold">Application Type</span></td>
                <td><span class="txtBold">App Id</span></td>
                <td><span class="txtBold">Reference No</span></td>
                <td><span class="txtBold">Applicant Name</span></td>
                <td><span class="txtBold">Date Of Birth</span></td>
                <td><span class="txtBold">NIS Payment Status</span></td>
                <td><span class="txtBold">Paid On</span></td>
                <td><span class="txtBold">Order Number</span></td>
                <td><span class="txtBold">Payment Gateway</span></td>
                <td><span class="txtBold">Email Id</span></td>
              </tr>
            <tbody>
              <tr>
                <td><?php if($detailinfo['app_type'] == 'vap'){echo sfConfig::get('app_visa_arrival_title');}else{echo strtoupper($detailinfo['app_type']);}?></td>
                <td><?php echo $detailinfo['appid'];?></td>
                <td><?php echo $detailinfo['refno'];?></td>
                <td><?php echo $detailinfo['name'];?></td>
                <td><?php echo date_format(date_create($detailinfo['date_of_birth']),'Y-m-d');?></td>
                <td><?php echo $detailinfo['status'];?></td>
                <td><?php if($detailinfo['paid_at']!='') echo date_format(date_create($detailinfo['paid_at']),'Y-m-d'); else echo "--"; ?></td>
                <?php if($detailinfo['order_no'] != "Not Paid on NIS"){ ?>
                <td><a href="<?= url_for("supportTool/searchOrderNo?order_no=".Settings::encryptInput($detailinfo['order_no'])); ?>" id="order_detail" title="Show Order Details">
                    <?php  echo $detailinfo['order_no'];?>
                    </a></td>
                <?php } else {?>
                <td><?php echo $detailinfo['order_no'] ?></td>
                <?php } ?>
                <td><?php echo $detailinfo['gatewayName']; ?></td>
                <td><?php echo $detailinfo['email']; ?></td>
                </tr>
            </tbody>
              <tfoot>
                
              </tfoot>
          </table>
        <br>
    <div id="result_div" class="no_display" align="center"></div>
        <div id="application_div"></div>
        <div>&nbsp;</div>
                <div class="clear">
                <div class="scroll_div">
       <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable">
              <tr>
                <td class="blbar" colspan="16" align="left">Failed Order List attempted by this Application
              </tr>
              <tr>
                <td><span class="txtBold">S.No.</span></td>
                <td><span class="txtBold">Order Number</span></td>
                <td><span class="txtBold">Amount</span></td>
                <td><span class="txtBold">Request Id</span></td>
                <td><span class="txtBold">Payment Gateway</span></td>
                <td><span class="txtBold">Payment Status</span></td>
                <td><span class="txtBold">Card First</span></td>
                <td><span class="txtBold">Card Last</span></td>
                <td><span class="txtBold">Payer Name</span></td>
                <td><span class="txtBold">Is Card Register with this User Id</span></td>
                <td><span class="txtBold">FPS Block</span></td>
                 <td><span class="txtBold">Failure Reason</span></td>
                <td><span class="txtBold">Response Code</span></td>
                <td><span class="txtBold">User Email</span></td>
                <td><span class="txtBold">User Id</span></td>
                <td><span class="txtBold">Transaction Date</span></td>
              </tr>

              <tbody>
              <?php
             // echo '<pre>';print_r($orderDetailArray);echo '</pre>';
             $srNo = 1;
              if(count($orderDetailArray) > 0){
              for($i=0;$i<count($orderDetailArray);$i++){
                 ?>
                          <tr>
                            <td><?php echo $srNo; ?></td>
                            <td><?php echo ($orderDetailArray[$i]['order_number']); ?></td>
                            <td><?php echo format_amount($orderDetailArray[$i]['amount'],1,0,$orderDetailArray[$i]['currency_symbol']); ?></td>
                            <td><?php echo ($orderDetailArray[$i]['request_id']); ?></td>
                            <td><?php echo ucwords($orderDetailArray[$i]['gateway_name']); ?></td>
                            <td><?php echo ($orderDetailArray[$i]['payment_status']); ?></td>
                            <td><?php echo ($orderDetailArray[$i]['card_first']); ?></td>
                            <td><?php echo ($orderDetailArray[$i]['card_last']); ?></td>
                            <td><?php echo ($orderDetailArray[$i]['payor_name']); ?></td>
                            <td><?php echo ($orderDetailArray[$i]['is_register']); ?></td>
                            <td><?php echo ($orderDetailArray[$i]['fps_block']); ?></td>
                            <td><?php echo ($orderDetailArray[$i]['failure_reason']); ?></td>
                            <td><?php echo ($orderDetailArray[$i]['response_code']); ?></td>
                            <td><?php echo ($orderDetailArray[$i]['user_email']); ?></td>
                            <td><?php echo ($orderDetailArray[$i]['user_id']); ?></td>
                            <td><?php echo date_format(date_create($orderDetailArray[$i]['trans_date']),'Y-m-d'); ?></td>
                          </tr>
              <?php $srNo++; }
              }else{ ?>
              <tr><td colspan="16" align="center">No Order Number is failed during payment of this application.</td></tr>

              <?php }

              ?>
              

            </tbody>
            <tfoot><tr><td colspan="16"></td></tr></tfoot>
          </table>
          </div>
          <div class="clear">&nbsp;</div>
          <div align="center"> 
          <?php   echo button_to('Back',url_for("supportTool/searchByAppId"),array('class' => 'normalbutton')); ?>
          </div>
<script>
    function getBackendStatus(orderNo,rowId,updateId,paymentStatus){

        var url = '<?php echo url_for('supportTool/showStatusFromBackEnd'); ?>';
         $('#result_div').html('<?php echo image_tag('../images/ajax-loader.gif'); ?>');
         $("#result_div").show();
        $.post(url, { orderNo: orderNo},
        function(data){
            $('#'+rowId).html(data);
            if(data == "Not Paid at Back-End"  && $('#'+paymentStatus).val() == 'Not Paid')
                {
                    $('#'+updateId).html('-------');
                }
            else if(data == "Paid at Back-End"  && $('#'+paymentStatus).val() == 'Paid'){
                
                    $('#'+updateId).html('-------');
                }
            else if($('#'+paymentStatus).val() == 'Not Paid' && data == "Paid at Back-End" )
                    {
                        $('#default_'+updateId).hide();
                        $('#'+updateId).css('display','block');
                        
                    }

                  $("#result_div").hide();
        });
         
    }

    function updatePaymentAtBackEnd(orderNo,rowId, notpaidid)
    {
        var url = '<?php echo url_for('supportTool/updatePaymentAtBackEnd'); ?>';
        
         $('#result_div').html('<?php echo image_tag('../images/ajax-loader.gif'); ?>');
         $("#result_div").show();

        $.post(url, { orderNo: orderNo},
        function(data){
               $('#'+rowId).show();
               $('#'+rowId).html(data);
               $('#'+notpaidid).html('Paid');               
               $("#result_div").hide();
               $('#default_'+rowId).hide();
       });
       

    }


</script>
</div></div>
</div>
<div class="content_wrapper_bottom"></div>