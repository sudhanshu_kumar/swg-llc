
<script>
  function validateForm()
  {
    if(document.getElementById('email').value == '')
    {
      alert('Please Insert Email Address.');
      document.getElementById('email').focus();
      return false;
    }

    if(document.getElementById('email').value != "")
    {

       var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
       var address = $("#email").val();
       if(reg.test(address) == false) {
          alert('Wrong Email Address');
          $("#email").focus();
          return false;
       }
    }
  }
</script>

<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">
<?php
use_helper('Pagination');
use_helper('Form');
include_partial('global/innerHeading',array('heading'=>'User Transaction Status'));
?>
  <div class="clear"></div>
<div class="tmz-spacer"></div>

<?php echo form_tag('supportTool/userTransactionLimitStatus',array('name'=>'order_search_form','class'=>'', 'method'=>'post','id'=>'order_search_form','onSubmit'=>'return validateForm()')) ?>


<?php
$sf = sfContext::getInstance()->getUser();
if($sf->hasFlash('notice')){ ?>
    <div id="flash_notice" class="alertBox" >
<?php
    echo nl2br($sf->getFlash('notice'));
?>
</div><br/>
<?php }?>

<?php
$sf = sfContext::getInstance()->getUser();
if($sf->hasFlash('error')){ ?>
    <div id="flash_error" class="alertBox" >
<?php
    echo nl2br($sf->getFlash('error'));
?>
</div><br/>
<?php }?>

            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                <?php

                //$app_id = (isset($_GET['email']))?$_GET['email']:"";
                echo formRowComplete('Email address<span style="color:red">*</span>',input_tag('email', $emailId, array('size' => 40, 'maxlength' => 50,'class'=>'txt-input'))); ?>

                <tr>
                    <td>&nbsp;</td>
                    <td>
                            <?php   echo submit_tag('Search',array('class' => 'normalbutton')); ?>
                     
              </td>
                </tr>
            </table>
  


   <br>
   <?php if($isFound): ?>

   <table cellpadding="0" cellspacing="0" border="0" width="100%">
        <tr>
            <td class="blbar" colspan="2">Basic Details</td>
       </tr>
       <tr>
           <td class="label_name">Name</td>
           <td class="label_name"><?= $name; ?></td>
       </tr>
       <tr>
           <td class="label_name">Phone Number</td>
           <td><?= $phone; ?></td>
       </tr>
       <tr>
           <td class="label_name">Email</td>
           <td><?= $email; ?></td>
       </tr>
       <tr>
            <td class="label_name">Cart Amount Capacity: </td>
            <td><?php echo $cartAmountCapacity; ?></td>
       </tr>
      <tr>
          <td class="label_name">Number of Transaction Allowed:</td>
          <td class="label_name"><?php echo $numberOfTransaction; ?></td>
      </tr>
      <tr>
        <td class="label_name">Transaction Period :</td>
        <td class="label_name">
        <?php
           switch(strtolower($transactionPeriod)){
               case 'a minute.':
                   echo '1 Minute';
                   break;
               case 'a month.':
                   echo '1 Month';
                   break;
               case 'an hour.':
                   echo '1 Hour';
                   break;
               case 'a day.':
                   echo '1 Day';
                   break;
               default:
                   echo $transactionPeriod;
                   break;
           }
        ?>
       </td>
      </tr>
            </table>
<table cellpadding="0" cellspacing="0" border="0" width="100%">
    <tbody>
    <?php
        $i =0;
    foreach ($fpsDetails as $cardDetails):   
            ?>
      <?php      
      $distinctCardSuccessTrans = Doctrine_Core::getTable('FpsDetail')->getDistinctCardSuccessTrans($fpsDetails[$i]['DISTINCT'],$timePeriod,$currentDate);
      $distinctCardSuccessTrans = $distinctCardSuccessTrans->execute(); //->toArray();

      if(count($distinctCardSuccessTrans) >0 ){
        $nextAllowedDate = $distinctCardSuccessTrans->getFirst()->getUpdatedAt();
      }else{
        $nextAllowedDate = '';
      }
      if($transactionPeriod=="a Month."){
          $transactionPeriod = '1 Months.';
      }
      if($nextAllowedDate!='' && !empty($nextAllowedDate)){
        $dateOneMonthAdded = strtotime(date("Y-m-d", strtotime($nextAllowedDate)) . "+$transactionPeriod");
      } else {
          $dateOneMonthAdded = '';
      }
//      echo "After adding one month: ".date('l dS \o\f F Y', $dateOneMonthAdded)."<br>";

      $distinctCardFailTrans = Doctrine_Core::getTable('FpsDetail')->getDistinctCardFailTrans($fpsDetails[$i]['DISTINCT'],$timePeriod,$currentDate);
      $cardNumber = Doctrine_Core::getTable('ipay4meOrder')->findByCardHash($fpsDetails[$i]['DISTINCT']);

      $cardBlock = Doctrine_Core::getTable('FpsDetail')->getDistinctCardBlock($fpsDetails[$i]['DISTINCT'],$timePeriod,$currentDate);

      ?>
      <tr>
        <td width="100%" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0">
          <tr>
            <td class="blbar" colspan="2">Card Details <?php echo $i+1;?></td>
          </tr>
          <tr>
            <td class="label_name">Card Number:</td>
            <td><?php if(isset($cardNumber[0]['card_first']) && $cardNumber[0]['card_first']!=''){ echo $cardNumber[0]['card_first'].'xxxx-xxxx-'.$cardNumber[0]['card_last']; } else { echo "N/A" ; } ?></td>
          </tr>
          <tr>
            <td class="label_name">Success Transaction(s) :</td>
            <td><?php echo count($distinctCardSuccessTrans); ?></td>
          </tr>
          <tr>
            <td class="label_name">Failed Transaction(s) :</td>
            <td><?php echo $distinctCardFailTrans; ?></td>
          </tr>
          <tr>
            <td class="label_name">Number of Attempt(s) left for the Card:</td>
            <td><?php  $a = $numberOfTransaction - count($distinctCardSuccessTrans); if($a>0){ echo $a; }else{ echo '0';}?></td>
          </tr>
          <?php
            $a = $numberOfTransaction - count($distinctCardSuccessTrans);
            if($a<=0){?>
          <tr>
              <td class="label_name"> <b>Next Allowed Transaction Date:</b></td>
              <td class="label_name"><?php  echo ($dateOneMonthAdded!='')?date('l dS \o\f F Y', $dateOneMonthAdded):'N/A'; ?></td>
          </tr>
          <?php } ?>
          <tr>
                <td class="label_name">Card Status:</td>
                <td class="label_name"><?php  if($cardBlock){ echo "Blocked";}else { echo "Not Blocked"; } ?></td>
          </tr>
        </table></td>
    </tr>
        <?php
            $i++;
            endforeach;
      ?>
</tbody>
</table>

<?php endif; ?>

</div>
</div>
<div class="content_wrapper_bottom"></div>