<script>
function   findDuplicate(appType, appId){
   var url = "<?= url_for("supportTool/findDuplicateNisApplication"); ?>";
   
   $("#application_div").hide();
   $('#result_div').html('<?php echo image_tag('../images/ajax-loader.gif'); ?>');
   $("#result_div").show();

   $.get(url, { id: appId, appType: appType},
   function(data){

       //$("#flash_notice").html('<span>Bank '+status+' successfully</span>');
       $("#application_div").html(data);
          $("#application_div").show();
          $("#application_div").focus();
          $("#result_div").hide();
       //window.location.href = "URL";
       //setTimeout( "refresh()", 2*1000 );
   });

 }
</script>

<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">
    <?php use_helper('Form');
use_javascript('common');
include_partial('global/innerHeading',array('heading'=>'Search Application by Application Id'));
?>
    <div class="clear"></div>
    <div class="tmz-spacer"></div>
    <div id="nxtPage">
          <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable">
              <tr>
                <td class="blbar" colspan="12" align="left">NIS Applications
              </tr>
              <tr>
                <td><span class="txtBold">S.No.</span></td>
                <td><span class="txtBold">Application Type</span></td>
                <td><span class="txtBold">App Id</span></td>
                <td><span class="txtBold">Reference No</span></td>
                <td><span class="txtBold">Applicant Name</span></td>
                <td><span class="txtBold">Date Of Birth</span></td>
                <td><span class="txtBold">NIS Payment Status</span></td>
                <td><span class="txtBold">Paid On</span></td>
                <td><span class="txtBold">Order Number</span></td>
                <td><span class="txtBold">Payment Gateway</span></td>
                <td><span class="txtBold">Email Id</span></td>
                <td><span class="txtBold">Action</span></td>
              </tr>
            <tbody>
              <?php
              $i=0;
              foreach ($detailinfo as $k=>$v):
              $i++;
              ?>
              <tr>
                <td><?php echo $i;//$embassyList->getid() ?></td>
                <td><?php
                echo ($k == 'vap')?sfConfig::get('app_visa_arrival_title'):strtoupper($k) ;//$embassyList->getid() ?></td>
                <td><?php echo $v['appid'] ?></td>
                <td><?php echo $v['refno'] ?></td>
                <td><?php echo $v['name'] ?></td>
                <td><?php echo date_format(date_create($v['date_of_birth']),'Y-m-d'); ?></td>
                <td><?php echo $v['status'] ?></td>
                <td><?php if($v['paid_at']!='') echo date_format(date_create($v['paid_at']),'Y-m-d'); else echo "--"; ?></td>
                <?php if($v['order_no'] != "Not Applicable"){ ?>
                <td><a href="searchOrderNo?order_no=<?php echo Settings::encryptInput($v['order_no']); ?>" id="order_detail" title="Show Order Details">
              <?php  echo $v['order_no'];?>
              </a></td>
                <?php } else {?>                
                <td><?php echo $v['order_no'] ?></td>
                <?php } ?>
                <td><?php echo $v['gatewayName'] ?></td>
                <td><?php echo str_replace('@',' @',$v['email']); ?></td>
                <td width="9%"><span><a href="#" onclick="findDuplicate('<?= strtolower($k); ?>','<?= $v['appid']; ?>')" title="Find Duplicate Applications" ><?php echo image_tag("/images/duplicate_app.gif"); ?></a></span></td>
              </tr>
              <?php
              endforeach;
              if($i==0):
              ?>
          <tr>
            <td colspan="12" align="center" class="red">No Records Found.</td>
          </tr>
              <?php endif; ?>
            </tbody>
        <tfoot>
        </tfoot>
          </table>
        </div>
        <br>
    <div id="result_div" align="center" class="no_display"></div>
        <div id="application_div"></div>
</div>
</div>
<div class="content_wrapper_bottom"></div>