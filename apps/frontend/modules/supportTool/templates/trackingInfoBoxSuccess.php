<?php $currencySymbol = CurrencyManager::currencySymbolByCurrencyId($arrMODetail['currency']); ?>
<?php use_helper('Form');?>
<?php use_stylesheet('style.css');
echo include_stylesheets();
echo include_javascripts(); ?>

<div class="tmz-spacer"></div>
<div id="nxtPage">
  <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
<tr>
      <td bgcolor="#eeeeee"><span >Money Order Serial Number:</span></td>
      <td><span ><?php echo $arrMODetail['moneyorder_number']?></span></td>
</tr>
<tr>
      <td width="30%" bgcolor="#eeeeee"><span >Money Order Amount:</span></td>
      <td width="70%"><span ><?php echo format_amount($arrMODetail['mo_amount'],1,0,$currencySymbol)?></span></td>
</tr>
<tr>
      <td width="30%" bgcolor="#eeeeee"><span >Money Order Paid Date:</span></td>
      <td width="70%"><span >
        <?php if($arrMODetail['paid_date'] !='') {echo $arrMODetail['paid_date'];}else{ echo 'N/A';}?>
        </span></td>
</tr>
<tr>
      <td width="30%" bgcolor="#eeeeee"><span >Money Order Status:</span></td>
      <td width="70%"><span >
        <?php if($arrMODetail['status'] == 'Paid') {echo $arrMODetail['status'];}else{ echo 'Not Paid';} ?>
        </span></td>
</tr>
<tr>
      <td width="30%" bgcolor="#eeeeee"><span >Tracking Number:</span></td>
      <td width="70%"><span ><?php echo $arrMODetail['tracking_number']?></span></td>
</tr>
<tr>
      <td width="30%" bgcolor="#eeeeee"><span >Tracking Amount:</span></td>
      <td width="70%" colspan=""><span ><?php echo format_amount($arrMODetail['tn_amount'],1,0,$currencySymbol)?></span></td>
    </tr>
<tr>
      <td width="30%" bgcolor="#eeeeee"><span >Order Number:</span></td>
      <td width="70%" colspan=""><span ><?php echo $arrMODetail['order_number']?></span></td>
</tr>
<!--<tr>
      <td width="30%" bgcolor="#eeeeee"><span >Waybill Tracking No.:</span></td>
      <td width="70%" colspan=""><span >
        <?php //if(isset ($arrMODetail['waybill_trackingno']) && $arrMODetail['waybill_trackingno']!=''){ echo $arrMODetail['waybill_trackingno']; } else {echo '- -';}?>
        </span></td>
</tr>
<tr>
      <td width="30%" bgcolor="#eeeeee"><span >Courier Service:</span></td>
      <td width="70%" colspan=""><span >
        <?php // if(isset ($arrMODetail['courier_service']) && $arrMODetail['courier_service']!=''){ echo $arrMODetail['courier_service']; } else {echo '- -';}?>
        </span></td>
</tr> -->
<tr>
      <td width="30%" bgcolor="#eeeeee"><span >Address:</span></td>
      <td width="70%" colspan=""><span >
        <?php if(isset ($arrMODetail['address']) && $arrMODetail['address']!=''){ echo $arrMODetail['address']; } else {echo '- -';}?>
        </span></td>
</tr>
<tr>
      <td width="30%" bgcolor="#eeeeee"><span >Phone Number:</span></td>
      <td width="70%" colspan=""><span >
        <?php if(isset ($arrMODetail['phone']) && $arrMODetail['phone']!=''){ echo $arrMODetail['phone']; } else {echo '- -';}?>
        </span></td>
</tr>
</table>
<br />
<center>
    <div align="center">
    <input style="" type="button" class="loginbutton"  id="multiFormSubmit" value="Close" onclick="parent.emailwindow.hide();">
</div>
</center>
</div>
