<table width="100%" align="center" style="border:4px solid #CCCCC" cellpadding="0" cellspacing="0" >
    <tr>
        <td class="blbar" colspan="12" align="right"><div class="float_left">Applicant Details</div></td>
    </tr>
    <tr>
        <td><strong>First Name</strong></td>
        <td><strong>Middle Name</strong></td>
        <td><strong>Last Name</strong></td>
        <td><strong>Date of Birth</strong></td>
        <td><strong>Card First</strong></td>
        <td><strong>Card Last</strong></td>
        <td><strong>Card Holder Name</strong></td>
        <td><strong>Email Address</strong></td>
        <td><strong>Blocked</strong></td>
    </tr>
    <?php    
    if(count($appDetailsObj)){

        foreach($appDetailsObj AS $apps){ ?>
           <tr>
            <td><?php echo $apps->getFirstName(); ?></td>
            <td><?php echo $apps->getMiddleName(); ?></td>
            <td><?php echo $apps->getLastName(); ?></td>
            <td><?php echo date_format(date_create($apps->getDob()), 'Y-m-d'); ?></td>
            <td><?php echo $apps->getCardFirst(); ?></td>
            <td><?php echo $apps->getCardLast(); ?></td>
            <td><?php echo $apps->getCardHolderName(); ?></td>
            <td><?php echo $apps->getEmail(); ?></td>
            <td><?php echo $apps->getBlocked(); ?></td>            
          </tr>
        <?php }

        
    }else{ ?>

    <tr><td colspan="12" align="center" class="red">No Record Found.</td></tr>
        
    <?php } 
    ?>

    <tr><td colspan="12" align="center"><input type="button" name="Hide" id="Hide" value="Hide" class="normalbutton" onclick="hideDiv('<?php echo $divId; ?>');" /></td></tr>
    
</table>

<script>
    var divId = '<?php echo $divId; ?>';
    //function hideDiv(){
        //document.getElementById('tr_'+divId).style.display='none';
    //}
</script>