
<script>
  function validateForm()
  {
    if(document.getElementById('app_id').value == '' && document.getElementById('ref_no').value == '')
    {
      alert('Please insert Application Id and Ref No');
      document.getElementById('app_id').focus();
      return false;
    }
     if(document.getElementById('app_id').value =='')
        {
            alert("Please insert Application Id ");
            document.getElementById('app_id').focus();
            return false;
        }
     if(document.getElementById('ref_no').value =='')
         {
            alert("Please insert Ref No ");
            document.getElementById('ref_no').focus();
            return false;
        }

    if(document.getElementById('app_id').value != "" || document.getElementById('app_id').value != "")
    {
      if(isNaN(document.getElementById('app_id').value))
      {
        alert('Please insert only numeric value.');
        document.getElementById('app_id').value = "";
        document.getElementById('app_id').focus();
        return false;
      }
      else if(isNaN(document.getElementById('ref_no').value))
          {
              alert('Please insert only numeric value.');
              document.getElementById('ref_no').value = "";
              document.getElementById('ref_no').focus();
          }



    }
  }
</script>

<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">
  <?php use_helper('Form');
include_partial('global/innerHeading',array('heading'=>'Search Order List by Application Id'));
?>
<div class="clear"></div>
<div class="tmz-spacer"></div>
<?php echo form_tag('supportTool/searchByAppId',array('name'=>'order_search_form_a','class'=>'', 'method'=>'post','id'=>'order_search_form','onSubmit'=>'return validateForm()')) ?>
<?php
$sf = sfContext::getInstance()->getUser();
if($sf->hasFlash('notice')){ ?>
    <div id="flash_notice" class="alertBox" >
<?php
    echo nl2br($sf->getFlash('notice'));
?>
</div><br/>
<?php }?>


            <table  width="99%" cellpadding="0" cellspacing="0" border="0">
                <?php

                $app_id = (isset($_POST['app_id']))?$_POST['app_id']:"";
                $ref_no = (isset($_POST['ref_no']))?$_POST['ref_no']:"";
            ?>
            <?php echo formRowComplete('NIS Application Id<span style="color:red">*</span>',input_tag('app_id', $app_id, array('size' => 20, 'maxlength' => 20, 'class'=>'txt-input'))) ;
            echo '</tr>';
            echo formRowComplete('NIS Ref No<span style="color:red">*</span>',input_tag('ref_no', $ref_no, array('size' => 20, 'maxlength' => 20, 'class'=>'txt-input')));
            echo '</tr>';

            ?>
            <tr><td></td><td><?php   echo submit_tag('Search',array('class' => 'normalbutton')); ?></td></tr>
            </table><div class="clear">&nbsp;</div>
               
                  
            
            

</div></div>
<div class="content_wrapper_bottom"></div>
