<script>
    function validateForm()
    {
        $('#detail_div').html('<?php echo image_tag('../images/ajax-loader.gif'); ?>');
        var err  = 0;



        if($('#mo_status').val() =="")
        {

            // document.getElementById('error_td').style.display=  '';
            $('#detail_error').html("<font color='red'>Please select money order status.</font>");

            err = err+1;
        }else
        {
            $('#detail_error').html(" ");
        }


        if(err != 0)
        {
            $('#hdn_datatable').hide();
            return false;
        }else{

        }



    }


    function hideMONumberTr(){
        if($('#mo_status').val() == 'New'){
            $('#money_order_row').hide();
            $('#msg_tr').show();
            $('#msg_tr_mo').hide();
        }else{
            $('#money_order_row').show();
            $('#msg_tr_mo').show();
            $('#msg_tr').hide();
        }
    }
</script>
<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
    <div class="content_container">
        <?php
        use_helper('Pagination');
        use_helper('Form');
        include_partial('global/innerHeading',array('heading'=>'Money Order Report'));
        ?>
        <?php
        use_javascript('dhtmlwindow.js');
        use_stylesheet('dhtmlwindow.css');
        use_javascript('modal.js');
        use_stylesheet('modal.css');
        if($sf_user->getGuardUser()->hasGroup("payment_group")){
            $paymentUser = true;
        }else{
            $paymentUser = false;
        }
        ?>
        <div class="brdBox">
            <?php echo form_tag('supportTool/moneyOrderReport',array('name'=>'order_search_form','class'=>'', 'method'=>'post','id'=>'order_search_form')) ?>
            <?php
            $sf = sfContext::getInstance()->getUser();
            if($sf->hasFlash('notice')){ ?>
            <div id="flash_notice" class="alertBox" >
                <?php
                echo nl2br($sf->getFlash('notice'));
                ?>
            </div>
            <br/>
            <?php }?>
        <?php
        $sf = sfContext::getInstance()->getUser();
        if($sf->hasFlash('error')){ ?>
            <div id="flash_error" class="alertBox" >
                <?php
                echo nl2br($sf->getFlash('error'));
                ?>
            </div>
            <br/>
            <?php }?>
            <div class="clear"></div>
            <div class="tmz-spacer"></div>
            <table width="99%" cellpadding="0" cellspacing="0" border="0">
                <?php
                $mo_status ='';
                $currency = '';
                $moneyOrder = (isset($_REQUEST['money_order'])) ? $_REQUEST['money_order'] : $_SESSION['money_order'];

                $mo_status = (isset($_REQUEST['mo_status'])) ? $_REQUEST['mo_status'] : $_SESSION['mo_status'];
                $currency = (isset($_REQUEST['currency'])) ? $_REQUEST['currency'] : $_SESSION['currency'];

                $order_number = (isset($_REQUEST['order_number'])) ? $_REQUEST['order_number'] : $_SESSION['order_number'];
                $tracking_number = (isset($_REQUEST['tracking_number'])) ? $_REQUEST['tracking_number'] : $_SESSION['tracking_number'];

                $fdate = (isset($_REQUEST['fdate'])) ? $_REQUEST['fdate'] : $_SESSION['fdate'];
                $tdate = (isset($_REQUEST['tdate'])) ? $_REQUEST['tdate'] : $_SESSION['tdate'];

                $statusArray = array(''=>'All','Associated'=>'Associated','Paid'=>'Paid','New'=>'Pending');
                $currencyArray = array(''=>'Please Select','dollar'=>'Dollar','pound'=>'Pound');

                echo formRowComplete('Status',select_tag('mo_status',options_for_select($statusArray,$mo_status),array('onchange'=>'hideMONumberTr();')),'','mo_status','detail_error','mo_status_row');
                echo "</tr>";
                //echo "<div id='mo_number_tr' style=''>";
                echo formRowComplete('Money Order Serial Number',input_tag('money_order', $moneyOrder, array('size' => 27, 'maxlength' => 30,'class'=>'txt-input')),'','money_order','detail_error','money_order_row');
                echo "</tr>";
                echo formRowComplete('Tracking Number',input_tag('tracking_number', $tracking_number, array('size' => 27, 'maxlength' => 20,'class'=>'txt-input')));
                echo "</tr>";
                echo formRowComplete('Order Number',input_tag('order_number', $order_number, array('size' => 27, 'maxlength' => 20,'class'=>'txt-input')));
                echo "</tr>";

                if(!$paymentUser){
                    echo formRowComplete('Currency Type',select_tag('currency',options_for_select($currencyArray,$currency),array('onchange'=>'hideMONumberTr();')),'','currency','detail_error','currency_row');
                    echo "</tr>"; ?>
                <tr>
                    <td width="20%">From date</td>
                    <td width="79%" ><?php
                    echo input_tag('fdate', $fdate, array('size' => 20, 'maxlength' => 10,'class'=>'txt-input','onfocus'=>'showCalendarControl(fdate)','readonly'=>'true')); ?></td>
                </tr>
                <tr>
                    <td width="20%">To date</td>
                    <td width="79%" ><?php
                        echo input_tag('tdate', $tdate, array('size' => 20, 'maxlength' => 10,'class'=>'txt-input','onfocus'=>'showCalendarControl(tdate)','readonly'=>'true'));
                        ?></td>
                </tr>
                <tr>
                    <td colspan="2"><strong><span class="red">Note: </span></strong>If you want downloadable excel sheet for this search, you need to give date range. You can search only 31 days record.</td>
                </tr>
                <?php } ?>
                <tr id="msg_tr_mo">
                    <td colspan="2" height="50px" class="highlight yellow"><b>Note: </b>You can search your tracking number status by entering either Money Order Serial Number or Tracking Number or Order Number OR any other combination available above.</td>
                </tr>
                <tr id="msg_tr"  style="display:none;">
                    <td colspan="2" height="50px" class="highlight yellow"><b>Note: </b>You can search your tracking number status by entering either Tracking Number or Order Number OR any other combination available above.</td>
                </tr>
                <tr valign="top" >

                    <td height="30" valign="top" colspan="2" align="center"><?php echo submit_tag('Search',array('class' => 'normalbutton','onclick'=>' return validateFormDate()')); ?>
                    </td>
                </tr>
            </table>
            </form>
            <br>
            <?php if($isFound){
                $colspan='8';
                ?>
            <div id="nxtPage">
                <table width="99%" border="0" cellpadding="0" cellspacing="0" class="dataTable" id="hdn_datatable">
                    <tr>
                        <td class="blbar" colspan="<?php echo $colspan; ?>" align="right"><div style="float:left">Tracking Number</div>
                        <span>Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total <b><?php echo $pager->getNbResults(); ?></b> results</span></td>
                    </tr>
                    <tr>
                        <td width="5%" ><span class="txtBold">S. No.</span></td>
                        <td width="20%"><span class="txtBold">Money Order Serial #</span></td>
                        <td width="20%"><span class="txtBold">Tracking #</span></td>
                        <td width="20%"><span class="txtBold">Order #</span></td>
                        <td width="20%"><span class="txtBold">Association Date</span></td>
                        <td width="20%"><span class="txtBold">Paid Date</span></td>
                        <td width="20%"><span class="txtBold">Amount</span></td>
                        <td width="20%"><span class="txtBold">Status</span></td>
                    </tr>
                    <tbody>
                        <?php
                        $j = $pager->getFirstIndice();
                        if($pager->getNbResults()>0)
                        {

                            foreach ($pager->getResults() as $result):

                            if($result['currency'] == '5'){
                                $orderAmount = $result['convert_amount'];
                            }else{
                                $orderAmount = $result['cart_amount'];
                            }
                            $currencySymbol = CurrencyManager::currencySymbolByCurrencyId($result['currency']);

                            ?>
                        <tr>
                            <td width="5%"><?php echo $j;?></td>
                            <td width="20%"><?php echo $result['TrackingMoneyorder'][0]['Moneyorder']['moneyorder_number'];?></td>
                            <td width="20%"><?php if($result['status'] == 'New') {?>
                                <span>
                                    <?= $result['tracking_number'] ?>
                                </span>
                                <? } else{?>
                                <a href = "#" onclick="trackingInfoBox('<?php echo $result['id'] ?>','<?php echo $result['status'] ?>')"><span>
                                        <?= $result['tracking_number'] ?>
                                </span></a>
                                <?php } ?>
                            </td>
                            <td width="20%"><?php if($result['order_number'] == '' || $result['order_number']=='0') {?>
                                <span>--</span>
                                <?php }else{ ?>
                                <a href="<?php echo url_for('supportTool/moneyOrderDetails?money_order_id='.Settings::encryptInput($result['TrackingMoneyorder'][0]['moneyorder_id']).'&back=1&page='.$page.'&order_number='.Settings::encryptInput($result['order_number']));?>"><?php echo $result['order_number'] ?></a>
                                <?php } ?>
                            </td>
                            <td width="20%"><?php if($result['associated_date'] == '0000-00-00') {?>
                                <span>--</span>
                                <?php }else{echo $result['associated_date'];} ?>
                            </td>
                            <td width="20%"><?php if($result['status'] == 'Paid') {
                                echo $result['updated_at'];
                            }else{echo '--';} ?>
                            </td>
                            <td width="20%"><span><?php echo format_amount($orderAmount,1,0,$currencySymbol); ?></span></td>
                            <td width="20%"><?php if($result['status'] != 'New') {?>
                                <span>
                                    <?= $result['status'] ?>
                                </span>
                                <?php }else {echo 'Pending';} ?>
                            </td>
                        </tr>
                        <?php $j++;

                        endforeach; ?>

                        <tr>
                            <td class="blbar" colspan="<?php echo $colspan; ?>" height="25px" align="right"><div class="paging pagingFoot">
                                    <?php
                                    echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName()).'?mo_status='.$_SESSION['mo_status'].'&money_order='.$_SESSION['money_order'].'&tracking_number='.$_SESSION['tracking_number'].'&order_number'.$_SESSION['order_number'].'&usePager=true'.'&back=1','detail_div');
                                    ?>
                            </div></td>
                        </tr>
                        <?php if($isExcelShow) {?>

                        <tr>
                            <td colspan="<?php echo $colspan; ?>" align="center">
                                <!--input type="button" value="Export to Excel" class="normalbutton" onclick="window.open('<?php //echo _compute_public_path($filename, 'uploads/excel', '', true); ?>');return false;" /-->
                                <input type="button" id="exportExcel" value="Export to Excel" class="normalbutton" onclick="exportExcel();"  />
                                <div id="loading" class="no_display"><?php echo image_tag('/images/ajax-loader_1.gif',array('alt'=>'Loading ...', 'align' => 'absmiddle'));?><br/>&nbsp;&nbsp;Please wait...</div>
                                <div id="exportExcel_error" class="red"></div>
                            </td>
                        </tr>


                                    <?php } } else { ?>
                        <tr>
                            <td  align='center' class='red' colspan="<?php echo $colspan; ?>">No Result Found.</td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
                <br>
            </div>
        </div>
    </div>
    <div class="content_wrapper_bottom"></div>
    <?php }
?>
</div>
<script>
    function trackingInfoBox(id,status){

        emailwindow=dhtmlmodal.open('EmailBox', 'iframe', "<?php echo url_for('supportTool/trackingInfoBox').'?tracking_id=' ?>"+id+"&status="+status, 'Tracking Number Details', 'width=650px,height=350px,center=1,border=0, scrolling=no');


    }
    if($('#mo_status').val() == 'New'){
        //alert('dfg');
        $('#money_order_row').hide();
    }else{
        $('#money_order_row').show();
    }

    function validateFormDate(){
        var fdate = $('#fdate').val();
        var tdate = $('#tdate').val();
        var start_date = fdate;
        var end_date = tdate;
        var current_date = new Date();
        var start_month = start_date.split('-')[0];
        var end_month = end_date.split('-')[1];

        if(fdate != ''){

            start_date = new Date(start_date.split('-')[0],start_date.split('-')[1]-1,start_date.split('-')[2]);
            if(start_date.getTime()>current_date.getTime()) {
                alert('From date cannot be future date.');
                return false
            }

            if(tdate == ''){
                alert('Please enter To date.');
                return false
            }
        }
        if(tdate != ''){

            end_date = new Date(end_date.split('-')[0],end_date.split('-')[1]-1,end_date.split('-')[2]);

            if(end_date.getTime()>current_date.getTime()) {
                alert('To date cannot be future date.');
                return false;
            }
            if(fdate == ''){
                alert('Please enter From date.');
                return false
            }
        }

        if((fdate != '') && (tdate != '')){
            if(start_date.getTime()>end_date.getTime()) {
                alert('From date cannot be greater than To date.');
                return false;
            }
        }

       endTime =  end_date.getTime();
       startTime = start_date.getTime();
       startMonth= start_date.getMonth();
       var difference = endTime - startTime ;
       days = Math.round(difference/(1000*60*60*24));
       var daysAllowed = (start_month == end_month)?31:30;
       if(days > daysAllowed){
           alert('Date Range can not be greater than 31 days.');
           return false;
       }
    }

    function exportExcel(){
        $('#exportExcel').hide();
        $('#loading').show();
        var url = "<?php echo url_for('supportTool/downloadMoneyOrderReportInExcel'); ?>";
        $.ajax({
            type: "POST",
            url: url,
            data: $('#order_search_form').serialize(),
            success: function (data) {
                data = jQuery.trim(data);
                if(data == 'yes'){
                    $('#exportExcel_error').html('');
                    window.open('<?php echo _compute_public_path($filename, 'uploads/excel', '', true); ?>');
                }else{
                    $('#exportExcel_error').html('Error found while creating excel sheet. Please try again.');
                }
                $('#exportExcel').show();
                $('#loading').hide();

            }//End of success: function (data) {...
        });
    }//End of function exportExcel(){...
</script>