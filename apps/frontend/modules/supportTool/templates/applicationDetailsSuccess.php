
<?php $sf = sfContext::getInstance()->getUser();
if($sf->hasFlash('notice')){
  ?><div id="flash_notice" class="alertBox" ><?php
  echo nl2br($sf->getFlash('notice'));

  ?></div><?php }?>

<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">
  <?php
/**
 * Template to display Application Listing for the order number selected.
 */

use_helper('Form');
use_helper('Pagination');
use_helper('FpsError');

include_partial('global/innerHeading',array('heading'=>'Application Listing'));

?>
<div class="clear"></div>
<div class="tmz-spacer"></div>
    <table width="99%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
      <tr>
        <td class="blbar" colspan="9" align="right">
        <div style="float:left">Summary</div>

      </tr>

      <tr>
        <td width="4%" ><span class="txtBold">Last Transaction</span></td>
        <td width="9%"><span><?php echo $arrTransactionDetail['last_transaction'];?></span></td>
      </tr>

      <tr>
        <td width="4%" ><span class="txtBold">Next Allowed Transaction</span></td>
        <?php
        if($date_flg == 0)
        {
          $date = $arrTransactionDetail['last_transaction'];
          $dd = date('d',strtotime($date));
          $mm = date('m',strtotime($date));
          $yyyy = date('Y',strtotime($date));
          $hh = date('h',strtotime($date));
          $min = date('i',strtotime($date));
          $ss = date('s',strtotime($date));
          $nextDate = date('Y-m-d h:i:s',mktime($hh,$min+$time,$ss,$mm,$dd,$yyyy));
        }
        else
        {
          $nextDate = $arrTransactionDetail['last_transaction'];
        }
        ?>
        <td width="9%"><span><?php echo $nextDate;?></span></td>
      </tr>

      <tr>
        <td width="4%" ><span class="txtBold">Gateway</span></td>
        <td width="9%"><span><?php echo $arrTransactionDetail['gateway_name']?></span></td>
      </tr>

      <tr>
        <td width="4%" ><span class="txtBold">Is Registered</span></td>
        <td width="9%"><span>
            <?php if($date_flg == 0)
            {
              echo "No";
            }else{
              echo "Yes";
            }
            ?>
        </span></td>
      </tr>
      <?php

      $fpsId =  $arrTransactionDetail['fps_rule_id'];
      $reason = fps_error($fpsId);
      ?>
      <tr>
        <td width="4%" ><span class="txtBold">Reason</span></td>
        <td width="9%"><span><?php echo $reason;?></span></td>
      </tr>

      <?php if($fpsId=='4' || $fpsId=='5')
      { ?>
      <tr>
        <td width="4%" ><span class="txtBold">Is Blocked</span></td>
        <td width="9%"><span><?php echo "<font color='red'>Yes</font>";?></span></td>
      </tr>
      <tr>
        <td width="4%" ><span class="txtBold">Is allowed for transaction</span></td>
        <td width="9%"><span><?php echo "No";?></span></td>
      </tr>
      <?php }
    else
    {
      ?>
      <tr>
        <td width="4%" ><span class="txtBold">Is Blocked</span></td>
        <td width="9%"><span><?php echo "<font color='green'>No</font>";?></span></td>
      </tr>
      <tr>
        <td width="4%" ><span class="txtBold">Is allowed for transaction</span></td>
        <td width="9%"><span><?php echo "Yes";?></span></td>
      </tr>
      <?php } ?>
      <tr>
        <td width="4%" ><span class="txtBold">User Error Message</span></td>
        <td width="9%"><span><?php

            $errorMsgObj =  new ErrorMsg();
            $this->errorMsg = $errorMsgObj->getErrorMgs("E".$errorMessageId);

            echo $this->errorMsg;  ?></span></td>
      </tr>
      <tr>
        <td width="4%" ><span class="txtBold">Actual Error Message</span></td>
        <td width="9%"><span><?php

            $errorMsgObj =  new ErrorMsg();
            $this->errorMsg = $errorMsgObj->getErrorDescription("E".$errorMessageId);

            echo $this->errorMsg;  ?></span></td>
      </tr>

    </table>




  <?php if($isFound) {
    $i = 1;
    ?>
<div class="clear">&nbsp;</div>
    <div id="nxtPage">
      <table width="99%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
        <tr>
          <td class="blbar" colspan="9" align="right">
            <div style="float:left">Application Details</div>
          <span>Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total  <b><?php echo $pager->getNbResults(); ?></b>  results</span></td>
        </tr>

        <tr>
          <td width="4%" ><span class="txtBold">S. No.</span></td>
          <td width="9%"><span class="txtBold">Application type</span></td>
          <td width="9%"><span class="txtBold">App Id</span></td>
          <td width="12%"><span class="txtBold">Reference No</span></td>
          <td width="14%"><span class="txtBold">Applicant Name</span></td>
          <td width="9%"><span class="txtBold">Date of Birth</span></td>
          <td width="6%"><span class="txtBold">NIS Payment Status</span></td>
          <td width="6%"><span class="txtBold">Paid On</span></td>
          <td width="10%"><span class="txtBold">Email Id</span></td>
        </tr>
        <tbody>
          <?php

          if($pager->getNbResults()>0)
          {
            //              $finalStatus = $nisHelper->isAllReadyRefunded($sf_params->get('order_no'));
            //              $finalStatus= $finalStatus->getRawValue();
            //              $fStatus = null;
            //              if(isset ($finalStatus) && is_array($finalStatus) && count($finalStatus)>0){
            //                $fStatus = $finalStatus['action'];
            //              }

            foreach ($pager->getResults() as $result):
            //                        echo "<pre>";print_r($result);die;
            $app['passport_id'] = $result['passport_id'];
            $app['visa_id'] = $result['visa_id'];
            $app['freezone_id'] = $result['freezone_id'];
            $appDetails = $appObj->getApplicationDetails($app);
            //            echo "<pre>";print_R($appDetails);die;
            $finalStatus = $nisHelper->isAllAppReadyRefunded($orderNumber,$appDetails['id']);
            $finalStatus= $finalStatus->getRawValue();
            $fStatus = null;
            if(isset ($finalStatus) && is_array($finalStatus) && count($finalStatus)>0){
              $fStatus = $finalStatus['action'];
            }


            ?>
          <tr>
            <td width="4%"><?php echo $i;?></td>
            <td width="9%"><span><?php echo strtoupper($appDetails['app_type']);?></span></td>
            <td width="10%"><span><a href="<?php echo url_for('supportTool/applicationDetailsInfo?appId='.Settings::encryptInput($appDetails['id']).'&type='.Settings::encryptInput($appDetails['app_type']).'&orderNumber='.Settings::encryptInput($orderNumber)) ?>"><?php echo $appDetails['id'];?></a></span></td>
            <td width="9%"><span><?php echo $appDetails['ref_no'];?></span></td>
            <td width="14%"><span><?php echo $appDetails['name'];?></span></td>
            <td width="9%"><span><?php echo date_format(date_create($appDetails['dob']), 'd-m-Y');?></span></td>
            <?php if(isset ($fStatus) && $fStatus!=null){ ?>

            <td width="9%"><span><?php echo strtoupper($fStatus);?></span></td>
            <?php } else{ ?>

            <td width="9%"><span><?php echo $appDetails['status'];?></span></td>

                  <?php  }  ?>

            <td width="9%"><span><?php if($appDetails['status'] != 'New') echo date_format(date_create($ipay4meOrderDetails[0]['paid_at']), 'd-m-Y'); else echo "--"; ?></span></td>
            <td width="10%"><span><?php if($ipay4meOrderDetails[0]['email'] != ''){ echo $ipay4meOrderDetails[0]['email'];}else { echo '--'; }?></span></td>



          </tr>
          <?php
          $i++;
          endforeach; ?>
          <tr>
            <td class="blbar" colspan="11" height="25px" align="right">
              <div class="paging pagingFoot">
                <?php
                echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName()).'?orderNumber='.$orderNumber);
                ?>
              </div>
            </td>
          </tr>
          <?php } else { ?>
          <tr><td  align='center' class='error' colspan="11">No Results found</td></tr>
          <?php } ?>
        </tbody>
      </table>




    </div>
    <?php

  } ?>



  <div class="pixbr XY20">

    <center id="multiFormNav">





      <div align="center">
        <table align="center" class="nobrd"><tr>
            <td align="center">
              <div class="lblButton">
                <input style="*text-align:center;" type="button" class="normalbutton"  onclick="javascript:history.go(-1)"  value="Back">
              </div>
              <div class="lblButtonRight">
                <div class="btnRtCorner"></div>
              </div>
            </td>
          </tr>

      </table></div>



    </center>

  </div>
  </div>
</div>
<div class="content_wrapper_bottom"></div>

