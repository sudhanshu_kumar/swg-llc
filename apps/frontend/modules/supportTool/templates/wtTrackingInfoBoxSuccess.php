<?php use_helper('Form');?>

<?php use_stylesheet('style.css');
echo include_stylesheets();
echo include_javascripts(); ?>


<div class="clearfix">
<div id="nxtPage">
<br />
<table width="100%" border="0" align="center">
<tr>
    <td class="blbar" colspan="4" align="right">
    <div style="float:left">Tracking Number Details</div></td>
</tr>
<tr>
    <td width="25%" bgcolor="#eeeeee"><span >Wire Transfer Serial Number:</span></td>
    <td width="25%"><span ><?php echo $arrMODetail['wire_transfer_number']?></span></td>
</tr>
<tr>
    <td width="25%" bgcolor="#eeeeee"><span >Wire Transfer Amount:</span></td>
    <td width="25%"><span ><?php echo format_amount($arrMODetail['wt_amount'],1)?></span></td>
</tr>
<tr>
    <td width="25%" bgcolor="#eeeeee"><span >Wire Transfer Paid Date:</span></td>
    <td width="20%"><span ><?php if($arrMODetail['paid_date'] !='') {echo $arrMODetail['paid_date'];}else{ echo 'N/A';}?></span></td>
</tr>
<tr>
    <td width="25%" bgcolor="#eeeeee"><span >Wire Transfer Status:</span></td>
    <td width="20%"><span ><?php if($arrMODetail['status'] == 'Paid') {echo $arrMODetail['status'];}else{ echo 'Not Paid';} ?></span></td>
</tr>
<tr>
    <td width="25%" bgcolor="#eeeeee"><span >Tracking Number:</span></td>
    <td width="20%"><span ><?php echo $arrMODetail['tracking_number']?></span></td>
    </tr>
<tr>
    <td width="25%" bgcolor="#eeeeee"><span >Tracking Amount:</span></td>
    <td width="20%" colspan=""><span ><?php echo format_amount($arrMODetail['tn_amount'],1)?></span></td>
</tr>
<tr>
    <td width="25%" bgcolor="#eeeeee"><span >Order Number:</span></td>
    <td width="20%" colspan=""><span ><?php echo $arrMODetail['order_number']?></span></td>
</tr>

    


</table>
<br />
<center>

<div class="divBlock" style="padding-right:65px">
    <input style="" type="button" class="loginbutton"  id="multiFormSubmit" value="Close" onclick="parent.emailwindow.hide();">
</div>

</center>
</div>
</div>