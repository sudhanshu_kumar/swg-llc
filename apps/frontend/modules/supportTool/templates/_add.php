<!--
New partial added to add user transaction details
-->
<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>
<form action="<?php echo url_for('supportTool/addUserTransactionLimitDetail?page='.$page) ?>" method="post" onsubmit="return validateForm()">

<?php if (!$form->getObject()->isNew()): ?>
<input type="hidden" name="sf_method" value="put" />
<?php endif; ?>

   <input type="hidden" name="user_id" id="user_id" value="<?php echo $user_id;?>" />

    <table width="100%" cellpadding="0" cellspacing="0" border="0">


        <tr>
            <td width="300"><b><?php echo $form['cart_capacity']->renderLabel(); ?></b></td>
            <td><?php echo $form['cart_capacity']->render();?>
                <br>
                <div class="red" id="cart_capacity_error">
                    <?php echo $form['cart_capacity']->renderError(); ?>
                </div>

            </td>
        </tr>
        <tr>
            <td><b><?php echo $form['cart_amount_capacity']->renderLabel(); ?></b></td>
            <td><?php echo $form['cart_amount_capacity']->render();?>
                <br>
                <div class="red" id="cart_amount_capacity_error">
                    <?php echo $form['cart_amount_capacity']->renderError(); ?>
                </div>

            </td>
        </tr>
        <tr>
            <td><b><?php echo $form['number_of_transaction']->renderLabel(); ?></b></td>
            <td><?php echo $form['number_of_transaction']->render();?>
                <br>
                <div class="red" id="number_of_transaction_error">
                    <?php echo $form['number_of_transaction']->renderError(); ?>
                </div>

            </td>
        </tr>
        <tr>
            <td><b><?php echo $form['transaction_period']->renderLabel(); ?></b></td>
            <td><?php echo $form['transaction_period']->render();?>
                <br>
                <div class="red" id="transaction_period_error">
                    <?php echo $form['transaction_period']->renderError(); ?>
                </div>
<?php echo $form->renderGlobalErrors(); ?>
            </td>
        </tr>
    <tfoot>
      <tr>
        <td colspan="2" align="center">

          &nbsp;
          <input type="button" value="Back" id="Back" name="Back" class="normalbutton" onclick="window.location.href='<?php echo url_for('supportTool/userTransactionLimit')?>/back/1/page/<?php echo $page;?>'" />
        <input type="submit" value="Save" id="update" name="Save" class="normalbutton" />
        </td>
      </tr>
    </tfoot>
  </table>
</form>
</div>
</div><div class="content_wrapper_bottom"></div>
<script>
    function validateForm(){

        var err  = 0;
        var cart_capacity = jQuery.trim($('#cart_capacity').val());
        var cart_amount_capacity = jQuery.trim($('#cart_amount_capacity').val());
        var number_of_transaction = jQuery.trim($('#number_of_transaction').val());
        var transaction_period = jQuery.trim($('#transaction_period').val());
        var reg = /^[\-0-9]+$/;
        if(cart_capacity != '')
        {

            if(reg.test(cart_capacity) == false) {
                $('#cart_capacity_error').html("<font color='red'>Invalid cart capacity</font>");
                $('#cart_capacity').focus();
                err = err + 1;
            }
        }
        else
        {
            $('#cart_capacity_error').html("");
        }
        if(cart_amount_capacity != '')
        {
            if(isNaN(cart_amount_capacity)){
                $('#cart_amount_capacity_error').html("<font color='red'>Invalid cart amount capacity</font>");
                $('#cart_amount_capacity').focus();
                err = err + 1;
            }
        }
        else
        {
            $('#cart_amount_capacity_error').html("");
        }

        if(number_of_transaction != '')
        {
            if(reg.test(number_of_transaction) == false){
                $('#number_of_transaction_error').html("<font color='red'>Invalid number of transaction(s)</font>");
                $('#number_of_transaction').focus();
                err = err + 1;
            }
        }
        else
        {
            $('#number_of_transaction_error').html("");
        }

        if(transaction_period !='')
        {
            if(reg.test(transaction_period) == false){
                $('#transaction_period_error').html("<font color='red'>Invalid transaction period </font>");
                $('#transaction_period').focus();
                err = err + 1;
            }
        }
        else
        {
            $('#transaction_period_error').html("");
        }
        if(err>0)
            {
                return false;
            }
        return true;

    }
</script>