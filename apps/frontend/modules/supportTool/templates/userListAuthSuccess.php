<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">
<?php

use_helper('Form');
use_helper('Pagination');

include_partial('global/innerHeading',array('heading'=>'Credit Card Pending List'));?>
<div class="clear"></div>
<div class="tmz-spacer"></div>
    <?php
    $sf = sfContext::getInstance()->getUser();
    if($sf->hasFlash('notice') || $sf->hasFlash('error')){ ?>
    <div id="flash_notice" class="alertBox" >
      <?php
      echo nl2br($sf->getFlash('notice'));
      ?>
    </div>
    <?php }?>


  <form action='#' method="post" name="frm_tra_detail" id="frm_tra_detail" onsubmit="return false;">
    <table width="99%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >

      <tbody>
        <tr>
          <td width="99%" valign="top" colspan="2">
            <?php //echo radiobutton_tag('selectBy', 'filtered', true) ?> <!--<strong> View Latest</strong>
            &nbsp;(Listing of Credit Card Pending List will be shown according to the latest Credit Card.)
            <br><br> -->
            <?php //echo radiobutton_tag('selectBy', 'normal', false) ?> <!-- <strong>View All</strong> -->
            Listing of Credit Card Pending List will be shown with all pending Credit Card.

          </td>
        </tr>
        <tr>
          <td width="20%">Email address</td>
          <td width="79%" ><?php $email = (isset($email))?$email:"";
          echo input_tag('email', $email, array('size' => 40, 'maxlength' => 50,'class'=>'txt-input')); ?></td>
        </tr>
        <tr>
          <td width="20%">From date</td>
          <td width="79%" ><?php $fdate = (isset($fdate))?$fdate:"";
          echo input_tag('fdate', $fdate, array('size' => 20, 'maxlength' => 10,'class'=>'txt-input','onfocus'=>'showCalendarControl(fdate)','readonly'=>'true')); ?></td>
        </tr>
        <tr>
          <td width="20%">To date</td>
          <td width="79%" ><?php $tdate = (isset($tdate))?$tdate:"";
          echo input_tag('tdate', $tdate, array('size' => 20, 'maxlength' => 10,'class'=>'txt-input','onfocus'=>'showCalendarControl(tdate)','readonly'=>'true')); ?></td>
        </tr>
        <tr>
          <?php if(isset($mode) && !empty($mode)){?>
          <input type="hidden" name="hdn_action" id="hdn_action" value="<?php echo $mode;?>">
          <?php }?>
		  <td></td>
          <td>
            <input type="button" name="btn_search" id="btn_search" value="Search" class="normalbutton" onclick="searchReportDone();">
            </td>
        </tr>

      </tbody>

    </table>
    <!--  This hidden page variable is using to maintain paging ... -->
    <input type="hidden" name="page" id="page" value="" />
  </form>
  <br />
  <div id="result_div"  align="center"></div>
  <div id="entry">


  </div>
</div></div>
<div class="content_wrapper_bottom"></div>

<script type="text/javascript">
  var checkedVal = '<?php echo $checkedVal; ?>';
  var page = '<?php echo $page; ?>';
  if(true){
    if(checkedVal != '' && checkedVal == 'normal'){
        $('#selectBy_filtered').attr('checked','');
        $('#selectBy_normal').attr('checked','checked');
    }else{
        $('#selectBy_filtered').attr('checked','checked');
        $('#selectBy_normal').attr('checked','');        
    }
    $('#page').val(page);
    
    searchReport();
  }


  function searchReportDone(){
      $('#page').val(0);
      <?php
      ## removing paging...
      sfContext::getInstance()->getUser()->getAttributeHolder()->remove('page');
      ?>
      searchReport();
  }


  function searchReport(){
    var err = 0;
    var fdate = $('#fdate').val()
    var tdate = $('#tdate').val()
    var email = $('#email').val()
    var page = $('#page').val()    
    var start_date = fdate;
    var end_date = tdate;
    var current_date;
    current_date = new Date();

    if(fdate != ''){
      start_date = new Date(start_date.split('-')[0],start_date.split('-')[1]-1,start_date.split('-')[2]);

      if(start_date.getTime()>current_date.getTime()) {
        alert('From date cannot be future date');
        err += 1;
      }
    }

    if(tdate != ''){
      end_date = new Date(end_date.split('-')[0],end_date.split('-')[1]-1,end_date.split('-')[2]);

      if(end_date.getTime()>current_date.getTime()) {
        alert('To date cannot be future date');
        err += 1;
      }
    }

    if((fdate != '') && (tdate != '')){
      if(start_date.getTime()>end_date.getTime()) {
        alert('From date cannot be greater than To date');
        err += 1;
      }
    }

    if(email != "")
    {
      var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
      if(reg.test(email) == false) {
        alert('Wrong Email Address');
        err += 1;
      }
    }

    if(err == 0){
      var checkedVal = $('input[name=selectBy]:checked').val()

      var url = "<?php echo url_for('supportTool/pendingUserAuthList'); ?>";
      $('#result_div').html('<?php echo image_tag('../images/ajax-loader.gif'); ?>');
      $("#result_div").show();
      $('#entry').html('');      
      $.post(url, {checkedVal: checkedVal,email:email,fdate:fdate,tdate:tdate,page:page},function(data){
        $('#entry').html(data);
        $("#result_div").hide();
      });
    }else{
      return false;
    }

  }


</script>