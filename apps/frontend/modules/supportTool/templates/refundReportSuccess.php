<?php
use_helper('Pagination');
use_helper('Form');
include_partial('global/innerHeading',array('heading'=>'Search SW Global LLC Refund Report'));
?>

<div class="global_content4 clearfix">
<div class="brdBox">
  <div class="clearfix"/>
    <div class="wrapForm2">
      <?php
      $sf = sfContext::getInstance()->getUser();
      if($sf->hasFlash('notice')){ ?>
      <div id="flash_notice" class="alertBox" >
        <?php
        echo nl2br($sf->getFlash('notice'));
        ?>
      </div><br/>
      <?php }?>

      <div class="wrapForm2">
        <?php
        $sf = sfContext::getInstance()->getUser();
        if($sf->hasFlash('error')){ ?>
        <div id="flash_error" class="alertBox" >
          <?php
          echo nl2br($sf->getFlash('error'));
          ?>
        </div><br/>
        <?php }?>

      </div>
    </div>

    <br>
    <h2 style="font-size:18px;"> Refund Report </h2>
    <div class="clearfix">
      <div id="nxtPage">
        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
          <tr>
            <td class="blbar" colspan="5" align="right">
            <span>Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total  <b><?php echo $pager->getNbResults(); ?></b>  results</span></td>
          </tr>
          <tr>
            <td width="5%" ><span class="txtBold">S. No.</span></td>
            <td width="15%"><span class="txtBold">Order Number</span></td>
            <td width="10%"><span class="txtBold">Amount</span></td>
            <td width="24%"><span class="txtBold">Refund Type</span></td>
            <td width="46%"><span class="txtBold">Message</span></td>
          </tr>
          <tbody>
            <?php
            $i = $pager->getFirstIndice();

            if($pager->getNbResults()>0)
            {
              foreach ($pager->getResults() as $details):

              $orderNumber = $details['order_number'];
              $amount = $details['amount'];
              $refundType = $details['refund_type'];
              $message = $details['message'];
              $url = url_for("supportTool/viewReportDetail").'?order='.$orderNumber;
              $j = $i;

              if($details['is_read'] =='no')
              {
                $orderNumber = '<b>'.$orderNumber.'</b>';
                $amount = '<b>'.$amount.'</b>';
                $refundType = '<b>'.$refundType.'</b>';
                $message = '<b>'.$message.'</b>';
                $j = '<b>'.$j.'</b>';
              }
              ?>
            <tr>
              <td width="5%"><?php echo $j;?></td>
              <td width="15%"><span><?php echo link_to($orderNumber, $url, array( 'popup' => array('View details', 'width=760,height=400,left=320,top=130,'))); ?></span></td>
              <td width="10%"><span><?php echo $amount; ?></span></td>
              <td width="24%"><span><?php echo $refundType; ?></span></td>
              <td width="46%"><span><?php echo $message; ?></span></td>
            </tr>
            <?php
            $i++;
            endforeach; ?>
            <tr>
              <td class="blbar" colspan="5" height="25px" align="right">
                <div class="paging pagingFoot">
                  <?php
                  echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName()),'detail_div');
                  ?>
                </div>
              </td>
            </tr>
            <?php } else { ?>
            <tr><td  align='center' class='error' colspan="5">No Results found</td></tr>
            <?php } ?>
          </tbody>

        </table>
        <br>
      </div>
    </div>
  </div>
</div>
<script>

  function refreshParent()
  {
    window.location.reload( true );
  }
</script>