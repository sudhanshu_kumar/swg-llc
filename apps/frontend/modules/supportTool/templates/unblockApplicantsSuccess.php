<script type="text/javascript">    

    function validateForm(fmobj, chkAll){
        $("#err_comment").html('');
        $('#err_chkbx').html("");
        var orderNo = $('#order_no').val();
        if(orderNo == ''){
            alert("Please enter SW Global LLC order number");
            $('#order_no').focus();
            return false;
        }
        if(orderNo != ''){
            if(isNaN(orderNo)){
                alert("Please enter only numeric value.");
                $('#order_no').val('') ;
                $('#order_no').focus();
                return false;
            }
        }
    }

    function checkAll(fmobj, chkAll)
    {
        for (var i=0;i<fmobj.elements.length;i++)
        {
            var e = fmobj.elements[i];            
            if ((e.name == chkAll) && (e.type=='checkbox') && (!e.disabled))
            {
                e.checked = fmobj.chk_all.checked;
                
            }
        }
    }

    function UncheckMain(fmobj, objChkAll, chkElement, obj)
    {
        var boolCheck = true;        
        
        if(objChkAll.checked==false)
        {
            for(var i=0;i<fmobj.elements.length;i++)
            {
                if(fmobj.elements[i].name.indexOf(""+chkElement+"")!="-1" && fmobj.elements[i].checked==false)
                {
                    boolCheck = false;
                    break;
                }
            }
            if(boolCheck==true)
                objChkAll.checked=true;
        }
        else
        {
            objChkAll.checked=false;
        }
    }


</script>

<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
<div class="content_container">
<?php
use_helper('Form');
include_partial('global/innerHeading',array('heading'=>'Unblock Applicants'));
?>
<div class="clear"></div>
<div class="tmz-spacer"></div>


<?php
$sf = sfContext::getInstance()->getUser();
if($sf->hasFlash('error')){ ?>
<div id="flash_error" class="alertBox" >
    <?php
    echo nl2br($sf->getFlash('error'));
    ?>
</div><br/>
<?php }?>

<div class="clear"></div>
<form name='order_search_form' id='order_search_form' action='<?php echo url_for('supportTool/unblockApplicants');?>' method='post' class="dlForm" onsubmit="return validateForm(document.order_search_form,'chk_fee[]')">
<table width="100%" cellpadding="0" cellspacing="0" border="0">
    <tr>
        <td>&nbsp;</td>
        <td>
            <?php
            echo formRowComplete('Order Number<span style="color:red">*</span>',input_tag('order_no', $ipayOrderNo, array('class'=>'txt-input'))); ?></td>
    </tr>    

    <td>&nbsp;</td>
    <td><div class="lblButton">
            <?php   echo submit_tag('Search',array('class' => 'normalbutton')); ?>
        </div>
        <div class="lblButtonRight">
            <div class="btnRtCorner"></div>
    </div></td>
    </tr>
</table>
</form>
<br>


<?php if($isFound) {
    $rollbackPaymentId = $rollbackDetails[0]['id'];

    $rollbackPaymentDetails = Doctrine::getTable('RollbackPaymentDetails')->findByRollbackPaymentId($rollbackDetails[0]['id']);

    



    $i = 1;
    ?>
<form name='applicant_form' id='applicant_form' action='<?php echo url_for('supportTool/markUnblockApplicants');?>' method='post' class="dlForm" onsubmit="return validateUnblockForm()">
<table width="100%" border="0" cellpadding="1" cellspacing="1" align="center">
    <?php if(count($rollbackPaymentDetails)){
        
     if($userPaymentRights != ''){
       $paymentRights = ($userPaymentRights == 0)?'Un-Blocked':'Blocked';
       $cardStatus = ($fpsRuleId == 4 || $fpsRuleId == 5)?'Blocked':'Un-Blocked';
    ?>
    <tr>
        <td colspan="10"><strong>User Payment Rights:</strong> <?php echo $paymentRights; ?> <?php if($fpsRuleId != ''){ ?>  <strong>| Card Status:</strong> <?php echo $cardStatus; ?> <?php } ?></td>
    </tr>
    
    <?php } }  ?>

    <tr>
        <td class="blbar" colspan="14" align="right">
            <div style="float:left">Unblock Applicants</div>
        </td>
    </tr>
    <tr>
        <td width="2%" ><span class="txtBold" id="maincheck"> <input type="checkbox" name="chk_all" id="chk_all" onclick="checkAll(document.applicant_form,'chk_fee[]');"></span></td>
        <td width="5%" ><span class="txtBold">S. No.</span></td>
        <td width="15%"><span class="txtBold">Application type</span></td>
        <td width="17%"><span class="txtBold">Application Id</span></td>
        <td width="4%"><span class="txtBold">Amount</span></td>
        <td width="16%"><span class="txtBold">Processing Country</span></td>
        <td width="10%"><span class="txtBold">Status</span></td>
        <td width="10%"><span class="txtBold">Date On</span></td>
        <td width="20%"><span class="txtBold">Applicant Details</span></td>
    </tr>
    <tbody>
        <?php $showButtons = false;
        $i=1;
        $appCount = 0;
        if(count($rollbackPaymentDetails))
        { ?>

            


        <?php
            
            foreach ($rollbackPaymentDetails as $result):
            $result = $result->toArray();
            

            ## Fetching application details...
            //$appDetails = $appObj->getApplicationDetails($app);

            ## Fetching application processing country...
            $processingCountry = Functions::getProcessingCountryByAppIdAndType($result['app_id'], $result['app_type']);

            //$finalStatus = $nisHelper->isAllAppReadyRefunded($ipayOrderNo,$appDetails['id']);
            //$finalStatus= $finalStatus->getRawValue();
            //$fStatus = null;
            //if(isset ($finalStatus) && is_array($finalStatus) && count($finalStatus)>0){
                //$fStatus = $finalStatus['action'];
            //}

            //if($appDetails['status']!="New"){
                //$appCount = $appCount + 1;
            //}

            ?>
        <tr id="data_tr_<?php echo $i; ?>">
            <td><?php if($result['action'] == 'chargeback'){ ?><span><input type="checkbox" name="chk_fee[]" id="<?= "chk_fee".$i; ?>" value="<?= $ipayOrderNo."_".$result['app_id']."_".$result['app_type'];?>" OnClick="UncheckMain(document.applicant_form,document.applicant_form.chk_all,'chk_fee',<?= "chk_fee".$i; ?>);" ></span><?php }else{ echo '&nbsp;'; } ?></td>
            <td><?php echo $i;?></td>
            <td><span><?php if($result['app_type'] == 'vap'){ echo sfConfig::get('app_visa_arrival_title');} else { echo strtoupper($result['app_type']);} ?></span></td>
            <td><span><?php echo $result['app_id'];?></span></td>
            <td><span><?php echo $result['amount'];?></span></td>
            <td><span><?php echo ($processingCountry != '')?$processingCountry:'--';?></span></td>
            <td><span><?php echo $result['action'];?></span></td>
            <td><span><?php echo date_format(date_create($result['created_at']), 'Y-m-d');?></span></td>
            <td><span><a href="javascript:void(0);" onclick="showApplicantDetails('<?php echo $i; ?>', '<?php echo $result['app_id']; ?>', '<?php echo $result['app_type']; ?>'); return false;">View Details</a></span></td>
        </tr>
        <p></p>
        <tr id="tr_<?php echo $i; ?>" style="display:none;" >
            <td colspan="9" id="divId_<?php echo $i; ?>" bgcolor="#CCCCCC"></td>
        </tr>
        
        <?php
        $showButtons = true;
        $i++;
        endforeach; ?>
        
        <?php } else { ?>
        <tr><td  align='center' class='red' colspan="13">No Results found</td></tr>
        <tr><td class="blbar" colspan="13" height="25px" align="right"></td></tr>
        <?php } ?>
    </tbody>

</table>
<br>

    <?php if($showButtons)
    {
        
        ?>
<div align="center">
    <input type="submit" class="normalbutton" id="unblockApplicants" name="unblockApplicants" value="Unblock Applicants"/>
</div>
<?php }
} ?>

<div id="result_div" style="width:500px;" align="center"></div>
<div id="application_div" style="display:none;"></div>
</form>
</div>
</div>
<div class="content_wrapper_bottom"></div>
<div id="loading" class="no_display"><?php echo image_tag('/images/ajax-loader_1.gif',array('alt'=>'Loading ...', 'align' => 'absmiddle'));?><br/>&nbsp;&nbsp;Please wait...</div>
<script>
    function showApplicantDetails(divId, appId, appType){
      var i = '<?php echo (isset($i))?$i:0; ?>';
      for(var k = 1; k<= i; k++){
          if(k != divId){
              $('#tr_'+k).hide();
              $('#data_tr_'+k).children('td').css('background-color','#FFFFFF');
          }else{
              $('#data_tr_'+k).children('td').css('background-color','#CCCCCC');
          }
      }
      $('#tr_'+divId).toggle();
      if($('#tr_'+divId).is(":visible")){
          
      }else{
          $('#data_tr_'+divId).children('td').css('background-color','#FFFFFF');
          return false;
      }
      $('#divId_'+divId).html($('#loading').html());
      url = '<?php echo url_for('supportTool/blockApplicantDetails'); ?>';      
      $.ajax({
            type: "POST",
            url: url,
            data: 'divId='+divId+'&appId='+appId+'&appType='+appType,
            success: function (data) {

                $('#data_tr_'+divId).children('td').css('background-color','#CCCCCC');
                
                data = jQuery.trim(data);
                $('#divId_'+divId).html(data);
            }//End of success: function (data) {...
        });
  }

  function hideDiv(divId){
    $('#data_tr_'+divId).children('td').css('background-color','#FFFFFF');
    $('#tr_'+divId).hide();
  }

  function validateUnblockForm(){
      var appCheckbox = document.getElementsByName('chk_fee[]');
        var appLength = appCheckbox.length;
        var count = 0;
        for(var i=0;i< appLength;i++){
            if(appCheckbox[i].checked){
                count = count + 1;
            }
        }
        if(count == 0){
            alert("Please Select atleast one applicant.");
            return false;
        }else{
            return true;
        }
      
  }

 $(document).ready(function(){
     if($('#order_no').length > 0){
         $('#order_no').focus();
     }
 })

</script>


