<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">
<?php
use_helper('Form');
use_helper('Pagination');
include_partial('global/innerHeading',array('heading'=>'Search NIS Application By SW Global LLC Order Number'));
?>
    <?php echo form_tag('supportTool/searchOrderNo',array('name'=>'order_search_form','class'=>'', 'method'=>'post','id'=>'order_search_form' ,'onsubmit'=>'return validate_form()')) ?>
    <div class="clear">&nbsp;</div>
    <div id="nxtPage">
      <table width="99%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
        <tr>
          <td class="blbar" colspan="11" align="right"><div style="float:left">NIS Application Details By SW Global LLC Order Number</div>
        </tr>
        <tr>
          <td width="4%" ><span class="txtBold">S. No.</span></td>
          <td width="9%"><span class="txtBold">Application type</span></td>
          <td width="9%"><span class="txtBold">App Id</span></td>
          <td width="12%"><span class="txtBold">Reference No</span></td>
          <td width="14%"><span class="txtBold">Applicant Name</span></td>
          <td width="9%"><span class="txtBold">Date of Birth</span></td>
          <td width="6%"><span class="txtBold">NIS Payment Status</span></td>
          <td width="6%"><span class="txtBold">Paid On</span></td>
          <td width="10%"><span class="txtBold">Email Id</span></td>
          <td width="10%"><span class="txtBold">Order Number</span></td>
        </tr>
        <tbody>
          <?php if(count($ipayNisAppDetails) > 0)
          {
            $i = 1; foreach ($ipayNisAppDetails as $details) {?>
          <tr>
            <td width="4%"><?php echo $i;?></td>
            <td width="9%"><span><?php if($details['app_type']=='vap'){ echo sfConfig::get('app_visa_arrival_title');} else { echo strtoupper($details['app_type']) ; }?></span></td>
            <td width="10%"><span><?php echo $details['id'];?></span></td>
            <td width="9%"><span><?php echo $details['ref_no'];?></span></td>
            <td width="14%"><span><?php echo $details['name'];?></span></td>
            <td width="9%"><span><?php echo date_format(date_create($details['dob']), 'd-m-Y');?></span></td>
            <td width="9%"><span><?php echo strtoupper($details['status']);?></span></td>
            <td width="9%"><span>--</span></td>
            <td width="10%"><span><?php echo $details['email'];?></span></td>
            <td width="10%"><span><?php echo Settings::decryptInput($sf_params->get('order_no'));?></span></td>
          </tr>
          <?php $i++; } }else{ ?>
          <tr>
            <td  align='center' class='error' colspan="11">No Results found</td>
          </tr>
          <?php  } ?>
          <tr>
            <td class="blbar" colspan="11" height="25px" align="right"></td>
          </tr>
        </tbody>
      </table>
    </div>
    <br />
    <div class="pixbr XY20" style="width:350px; margin:auto;">
      <center id="multiFormNav">
        <input type="button" value="Back" onclick="javascript:history.go(-1)" class="normalbutton" id="Cancel" name="cancle"/>
      </center>
    </div>
  </div>
</div>
<div class="content_wrapper_bottom"></div>
