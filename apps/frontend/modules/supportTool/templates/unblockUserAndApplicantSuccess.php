<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">
<?php
use_helper('Pagination');
use_helper('Form');
include_partial('global/innerHeading',array('heading'=>'Search Black Listed User'));
?>
<?php
use_javascript('dhtmlwindow.js');
use_stylesheet('dhtmlwindow.css');
use_javascript('modal.js');
use_stylesheet('modal.css');
?>
    <div class="clear"></div>
    <div class="tmz-spacer"></div>

      <br>
      <?php
      $sf = sfContext::getInstance()->getUser();
      if($sf->hasFlash('notice')){ ?>
      <div id="flash_notice" class="alertBox" >
        <?php
        echo nl2br($sf->getFlash('notice'));
        ?>
    </div>
    <br/>
      <?php }?>
    <?php
    $sf = sfContext::getInstance()->getUser();
    if($sf->hasFlash('error')){ ?>
      <div id="flash_error" class="alertBox" >
        <?php
        echo nl2br($sf->getFlash('error'));
        ?>
    </div>
    <br/>
      <?php }?>
    <?php echo form_tag('supportTool/unblockUserAndApplicant',array('name'=>'card_search_form','class'=>'', 'method'=>'get','id'=>'cardr_search_form','onsubmit'=>'return validateForm()')) ?>
    <table width="99%" cellpadding="0" cellspacing="0" border="0">
        <?php

        echo formRowComplete('Email Id',input_tag('email', '', array('size' => 40, 'maxlength' => 50,'class'=>'txt-input')));
        echo "<tr>";?>
        <tr>
          <td>&nbsp;</td>
        <td><?php   echo submit_tag('Search',array('class' => 'normalbutton')); ?>
        </td>
        </tr>
      </table>
<?php if(isset($pager)) {?>
      <div class="clearfix">
      <div id="nxtPage">
        <table width="99%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
          <tr>
            <td class="blbar" colspan="8" align="right"><div class="float_left">Black Listed Order Details by this User</div>
              <span>Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total <b><?php echo $pager->getNbResults(); ?></b> results</span></td>
          </tr>
          <tr>
            <td width="7%" ><span class="txtBold">S. No.</span></td>
            <td width="10%"><span class="txtBold">Order Number</span></td>
            <td width="10%"><span class="txtBold">Payment Status</span></td>
            <td width="10%"><span class="txtBold">Card First</span></td>
            <td width="10%"><span class="txtBold">Card Last</span></td>
            <td width="10%"><span class="txtBold">Black Listed Date</span></td>
            <td width="15%"><span class="txtBold">Comment</span></td>
            <td width="15%"><span class="txtBold">Action</span></td>
          </tr>
          <tbody>
            <?php
            $i = $pager->getFirstIndice();

            if($pager->getNbResults()>0)
            {
              foreach ($pager->getResults() as $details):

                $status= $details['payment_status'];
                if($status == 3){
                    $paymentStatus = 'Charge-back';
                }else{
                    $paymentStatus = 'Refund and Block';
                }
             
              ?>
            <tr>
            
              <td width="7%"><?php echo $i;?></td>
              <td width="10%"><span><a href ="#" id="order_no" onclick="getApplicationId('<?php echo $details['order_number']; ?>');"><?php echo $details['order_number'] ?></a></span></td>
              <td width="10%"><span><?php echo $paymentStatus; ?></span></td>
              <td width="10%"><span><?php echo $details['card_first'] ?></span></td>
              <td width="10%"><span><?php echo $details['card_last'] ?></span></td>
              <td width="10%"><span><?php echo date_format(date_create($details['updated_at']),'Y-m-d'); ?></span></td>
              <td width="23%"><span><?php echo $details['comments']; ?></span></td>
              <td width="10%"><span><a href ="<?php echo url_for('supportTool/doUnblockUserAndApplicant?email='.$email.'&order='.$details['order_number'])?>" id="order_no">Unblock</a></span></td>
            </tr>
            <?php
            $i++;
            endforeach; ?>
            <tr>
              <td class="blbar" colspan="8" height="25px" align="right"><div class="paging pagingFoot">
                  <?php
                  echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName()).'?email='.$sf_params->get('email'),'detail_div');
                  ?>
                </div></td>
            </tr>
            <?php } else { ?>
            <tr>
              <td  align='center' class='red' colspan="8">No Results found</td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
        <br>
      </div>
    </div>
    <?php } ?>
  
     
  </div>
</div>
<div class="content_wrapper_bottom"></div>
<script>
    
 function validateForm(){
     var email = $.trim($('#email').val());
     if(email == ''){
         alert('Please enter email id');
         return false;
     }
     var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
     if(reg.test(email) == false){
         alert('Please enter valid email address');
         return false;
     }
 }




  function getApplicationId(orderNo){
    commentwindow=dhtmlmodal.open('searchBlackListedApplications', 'iframe', '<?php echo url_for('supportTool/searchBlackListedApplications').'?order_no='?>'+orderNo, 'Black Listed Applications', 'width=840px,height=3   50px,center=1,border=0, scrolling=no')
    commentwindow.onclose=function(){
      return true ;
     }

  }

</script>