<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">
<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>
<?php use_helper('Form');
include_partial('global/innerHeading',array('heading'=>'Update Credit Card'));?>
<div class="clear"></div>
<div class="tmz-spacer"></div>
<form  id="frm_billing_info"  name="frm_billing_info" action="<?= url_for("supportTool/editContectDetails") ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?> onsubmit="return checkRequestedCardType();" >
  <?php if (!$form->getObject()->isNew()): ?>
  <input type="hidden" name="sf_method" value="post" />
  <?php endif; ?>  
  <table border="0" width="100%" cellpadding="0" cellspacing="0">
  <!--  <tr><td colspan="2">
    <table border="0" style="width:100%; margin-bottom:10px;">
          <strong>Guidelines:</strong>
          <ol style="padding-left:25px;"><li>Fill out the request form below. You are required to produce proof of your name and billing address as associated with your credit card. A scanned copy of your latest credit card statement shall suffice for this purpose. In case your credit card statement displays the full digits of your card number on it, please ensure to hide (by overwriting it with dark black pen) the 8 middle digits of your credit card leaving only the first four and last four digits visible.<br><b>For example, if your credit card statement shows your credit card number 1234-5678-5678-9876, please mask  5678-5678 so that it appears as 1234-XXX-XXX-9876 in the scanned copy that you upload for our review.</b></li>
            <li>Submit it for approval. You'll also be sent a notification email confirming your submission.</li>
            <li>Once your request is processed, a notification will be sent to you for confirmation and next steps..</li>
            <li>On successful approval you will be able to use your card for multiple payments on SWGloballlc platform.</li>
          <li>Please ensure that the phone number you have provided is a valid phone number on which you are reachable, we may contact you on this phone number should we need any more information before approving your account. Failure to provide a valid phone number will result into rejection of your request.</li></ol>
          <br/>
    </table></td></tr>-->
    <tr>
      <td>
      <?php $form->renderGlobalErrors();?>
        <table width="100%" cellpadding="0" cellspacing="0" border="0">
          <tr>
            <td class="blbar" colspan="2" align="right">      <p>Billing information</p>

            </td>
          </tr>
          <tr>
            <td colspan="2" align="right">      <p class="red">*Required Information </p></td>
          </tr>
          <tr>
            <td colspan="2" align="right">      <p class="red">Please enter the exact billing address which is printed on your Credit Card statement</p>

            </td>
          </tr>

          <tr>
            <td><?php echo $form['first_name']->renderLabel(); ?><span class="red">*</span></td>
            <td><?php echo $form['first_name']->render(); ?>
              <br>
              <div class="red" id="first_name_error">
                <?php echo $form['first_name']->renderError(); ?>
              </div>
              <input type="hidden" name="hdn_mode" id="hdn_mode" value="<?php echo $mode; ?>">
            </td>
          </tr>
          <tr>
            <td><?php echo $form['last_name']->renderLabel(); ?><span class="red">*</span></td>
            <td><?php echo $form['last_name']->render(); ?>
              <br>
              <div class="red" id="last_name_error">
                <?php echo $form['last_name']->renderError(); ?>
              </div>

            </td>
          </tr>

          <tr>
            <td><?php echo $form['address1']->renderLabel(); ?><span class="red">*</span></td>
            <td><?php echo $form['address1']->render(); ?>
              <br>
              <div class="red" id="address1_error">
                <?php echo $form['address1']->renderError(); ?>
              </div>

            </td>
          </tr>

          <tr>
            <td><?php echo $form['address2']->renderLabel(); ?></td>
            <td><?php echo $form['address2']->render(); ?>
              <br>
              <div class="red" id="address2_error">
                <?php echo $form['address2']->renderError(); ?>
              </div>

            </td>
          </tr>

          <tr>
            <td><?php echo $form['town']->renderLabel(); ?><span class="red">*</span></td>
            <td><?php echo $form['town']->render(); ?>
              <br>
              <div class="red" id="town_error">
                <?php echo $form['town']->renderError(); ?>
              </div>

            </td>
          </tr>

          <tr>
            <td><?php echo $form['state']->renderLabel(); ?></td>
            <td><?php echo $form['state']->render(); ?>
              <br>
              <div class="red" id="state_error">
                <?php echo $form['state']->renderError(); ?>
              </div>
            </td>
          </tr>

          <tr>
            <td><?php echo $form['zip']->renderLabel(); ?></td>
            <td><?php echo $form['zip']->render(); ?>
              <br>
              <div class="red" id="zip_error">
                <?php echo $form['zip']->renderError(); ?>
              </div>

            </td>
          </tr>

          <tr>
            <td><?php echo $form['country']->renderLabel(); ?><span class="red">*</span></td>
            <td>

              <select id="applicant_vault_country" name="applicant_vault[country]">

                <option ringto="" value="">Please Select Country</option>
                <option ringto="+93-" value="AF">Afghanistan</option>

                <option ringto="+355-" value="AL">Albania</option>

                <option ringto="+213-" value="DZ">Algeria</option>

                <option ringto="+1684-" value="AS">American Samoa</option>

                <option ringto="+376-" value="AD">Andorra</option>

                <option ringto="+244-" value="AO">Angola</option>

                <option ringto="+1264-" value="AI">Anguilla</option>

                <option ringto="+672-" value="AQ">Antarctica</option>

                <option ringto="+1268-" value="AG">Antigua &amp; Barbuda</option>

                <option ringto="+54-" value="AR">Argentina</option>

                <option ringto="+374-" value="AM">Armenia</option>

                <option ringto="+297-" value="AW">Aruba</option>

                <option ringto="+61-" value="AU">Australia</option>

                <option ringto="+43-" value="AT">Austria</option>

                <option ringto="+994-" value="AZ">Azerbaijan</option>

                <option ringto="+1242-" value="BS">Bahamas</option>

                <option ringto="+973-" value="BH">Bahrain</option>

                <option ringto="+880-" value="BD">Bangladesh</option>

                <option ringto="+1246-" value="BB">Barbados</option>

                <option ringto="+375-" value="BY">Belarus</option>

                <option ringto="+32-" value="BE">Belgium</option>

                <option ringto="+501-" value="BZ">Belize</option>

                <option ringto="+229-" value="BJ">Benin</option>

                <option ringto="+1441-" value="BM">Bermuda</option>

                <option ringto="+975-" value="BT">Bhutan</option>

                <option ringto="+591-" value="BO">Bolivia</option>

                <option ringto="+387-" value="BA">Bosnia/Herzegovina</option>

                <option ringto="+267-" value="BW">Botswana</option>

                <option ringto="+55-" value="BR">Brazil</option>

                <option ringto="+1284-" value="VG">British Virgin Islands</option>

                <option ringto="+673-" value="BN">Brunei</option>

                <option ringto="+359-" value="BG">Bulgaria</option>

                <option ringto="+226-" value="BF">Burkina Faso</option>

                <option ringto="+257-" value="BI">Burundi</option>

                <option ringto="+855-" value="KH">Cambodia</option>

                <option ringto="+237-" value="CM">Cameroon</option>

                <option ringto="1-" value="CA">Canada</option>

                <option ringto="+238-" value="CV">Cape Verde Islands</option>

                <option ringto="+1345-" value="KY">Cayman Islands</option>

                <option ringto="+236-" value="CF">Central African Republic</option>

                <option ringto="+235-" value="TD">Chad Republic</option>

                <option ringto="+56-" value="CL">Chile</option>

                <option ringto="+86-" value="CN">China</option>

                <option ringto="+6724-" value="CX">Christmas Island</option>

                <option ringto="+6722-" value="CC">Cocos Keeling Island</option>

                <option ringto="+57-" value="CO">Colombia</option>

                <option ringto="+269-" value="KM">Comoros</option>

                <option ringto="+243-" value="CD">Congo Democratic Republic</option>

                <option ringto="+242-" value="CG">Congo, Republic of</option>

                <option ringto="+682-" value="CK">Cook Islands</option>

                <option ringto="+506-" value="CR">Costa Rica</option>

                <option ringto="+225-" value="CI">Cote D'Ivoire</option>

                <option ringto="+385-" value="HR">Croatia</option>

                <option ringto="+53-" value="CU">Cuba</option>

                <option ringto="+357-" value="CY">Cyprus</option>

                <option ringto="+420-" value="CZ">Czech Republic</option>

                <option ringto="+45-" value="DK">Denmark</option>

                <option ringto="+253-" value="DJ">Djibouti</option>

                <option ringto="+1767-" value="DM">Dominica</option>

                <option ringto="+1809, 1829-" value="DO">Dominican Republic</option>

                <option ringto="+593-" value="EC">Ecuador</option>

                <option ringto="+20-" value="EG">Egypt</option>

                <option ringto="+503-" value="SV">El Salvador</option>

                <option ringto="+240-" value="GQ">Equatorial Guinea</option>

                <option ringto="+291-" value="ER">Eritrea</option>

                <option ringto="+372-" value="EE">Estonia</option>

                <option ringto="+251-" value="ET">Ethiopia</option>

                <option ringto="+500-" value="FK">Falkland Islands</option>

                <option ringto="+298-" value="FO">Faroe Island</option>

                <option ringto="+679-" value="FJ">Fiji Islands</option>

                <option ringto="+358-" value="FI">Finland</option>

                <option ringto="+33-" value="FR">France</option>

                <option ringto="+596-" value="TF">French Antilles/Martinique</option>

                <option ringto="+594-" value="GF">French Guiana</option>

                <option ringto="+689-" value="PF">French Polynesia</option>

                <option ringto="+241-" value="GA">Gabon Republic</option>

                <option ringto="+220-" value="GM">Gambia</option>

                <option ringto="+995-" value="GE">Georgia</option>

                <option ringto="+49-" value="DE">Germany</option>

                <option ringto="+233-" value="GH">Ghana</option>

                <option ringto="+350-" value="GI">Gibraltar</option>

                <option ringto="+30-" value="GR">Greece</option>

                <option ringto="+299-" value="GL">Greenland</option>

                <option ringto="+1473-" value="GD">Grenada</option>

                <option ringto="+590-" value="GP">Guadeloupe</option>

                <option ringto="+1671-" value="GU">Guam</option>

                <option ringto="+502-" value="GT">Guatemala</option>

                <option ringto="+224-" value="GN">Guinea Republic</option>

                <option ringto="+245-" value="GW">Guinea-Bissau</option>

                <option ringto="+592-" value="GY">Guyana</option>

                <option ringto="+509-" value="HT">Haiti</option>

                <option ringto="+504-" value="HN">Honduras</option>

                <option ringto="+852-" value="HK">Hong Kong</option>

                <option ringto="+36-" value="HU">Hungary</option>

                <option ringto="+354-" value="IS">Iceland</option>

                <option ringto="+91-" value="IN">India</option>

                <option ringto="+62-" value="ID">Indonesia</option>

                <option ringto="+964-" value="IQ">Iraq</option>

                <option ringto="+353-" value="IE">Ireland</option>

                <option ringto="+972-" value="IL">Israel</option>

                <option ringto="+39-" value="IT">Italy</option>

                <option ringto="+1876-" value="JM">Jamaica</option>

                <option ringto="+81-" value="JP">Japan</option>

                <option ringto="+962-" value="JO">Jordan</option>

                <option ringto="+254-" value="KE">Kenya</option>

                <option ringto="+686-" value="KI">Kiribati</option>

                <option ringto="+3774-" value="XK">Kosovo</option>

                <option ringto="+965-" value="KW">Kuwait</option>

                <option ringto="+996-" value="KG">Kyrgyzstan</option>

                <option ringto="+856-" value="LA">Laos</option>

                <option ringto="+371-" value="LV">Latvia</option>

                <option ringto="+961-" value="LB">Lebanon</option>

                <option ringto="+266-" value="LS">Lesotho</option>

                <option ringto="+231-" value="LR">Liberia</option>

                <option ringto="+218-" value="LY">Libya</option>

                <option ringto="+423-" value="LI">Liechtenstein</option>

                <option ringto="+370-" value="LT">Lithuania</option>

                <option ringto="+352-" value="LU">Luxembourg</option>

                <option ringto="+853-" value="MO">Macau</option>

                <option ringto="+389-" value="MK">Macedonia</option>

                <option ringto="+261-" value="MG">Madagascar</option>

                <option ringto="+265-" value="MW">Malawi</option>

                <option ringto="+60-" value="MY">Malaysia</option>

                <option ringto="+960-" value="MV">Maldives</option>

                <option ringto="+223-" value="ML">Mali Republic</option>

                <option ringto="+356-" value="MT">Malta</option>

                <option ringto="+692-" value="MH">Marshall Islands</option>

                <option ringto="+222-" value="MR">Mauritania</option>

                <option ringto="+230-" value="MU">Mauritius</option>

                <option ringto="+52-" value="MX">Mexico</option>

                <option ringto="+691-" value="FM">Micronesia</option>

                <option ringto="+373-" value="MD">Moldova</option>

                <option ringto="+377-" value="MC">Monaco</option>

                <option ringto="+976-" value="MN">Mongolia</option>

                <option ringto="+382-" value="ME">Montenegro</option>

                <option ringto="+1664-" value="MS">Montserrat</option>

                <option ringto="+212-" value="MA">Morocco</option>

                <option ringto="+258-" value="MZ">Mozambique</option>

                <option ringto="+95-" value="MM">Myanmar (Burma)</option>

                <option ringto="+264-" value="NA">Namibia</option>

                <option ringto="+674-" value="NR">Nauru</option>

                <option ringto="+977-" value="NP">Nepal</option>

                <option ringto="+31-" value="NL">Netherlands</option>

                <option ringto="+687-" value="NC">New Caledonia</option>

                <option ringto="+64-" value="NZ">New Zealand</option>

                <option ringto="+505-" value="NI">Nicaragua</option>

                <option ringto="+227-" value="NE">Niger Republic</option>

                <option ringto="+234-" value="NG">Nigeria</option>

                <option ringto="+683-" value="NU">Niue Island</option>

                <option ringto="+6723-" value="NF">Norfolk</option>

                <option ringto="+850-" value="KP">North Korea</option>

                <option ringto="+47-" value="NO">Norway</option>

                <option ringto="+968-" value="OM">Oman Dem Republic</option>

                <option ringto="+92-" value="PK">Pakistan</option>

                <option ringto="+680-" value="PW">Palau Republic</option>

                <option ringto="+970-" value="PS">Palestine</option>

                <option ringto="+507-" value="PA">Panama</option>

                <option ringto="+675-" value="PG">Papua New Guinea</option>

                <option ringto="+595-" value="PY">Paraguay</option>

                <option ringto="+51-" value="PE">Peru</option>

                <option ringto="+63-" value="PH">Philippines</option>

                <option ringto="+48-" value="PL">Poland</option>

                <option ringto="+351-" value="PT">Portugal</option>

                <option ringto="+1787-" value="PR">Puerto Rico</option>

                <option ringto="+974-" value="QA">Qatar</option>

                <option ringto="+262-" value="RE">Reunion Island</option>

                <option ringto="+40-" value="RO">Romania</option>

                <option ringto="+7-" value="RU">Russia</option>

                <option ringto="+250-" value="RW">Rwanda Republic</option>

                <option ringto="+1670-" value="MP">Saipan/Mariannas</option>

                <option ringto="+378-" value="SM">San Marino</option>

                <option ringto="+239-" value="ST">Sao Tome/Principe</option>

                <option ringto="+966-" value="SA">Saudi Arabia</option>

                <option ringto="+221-" value="SN">Senegal</option>

                <option ringto="+381-" value="RS">Serbia</option>

                <option ringto="+248-" value="SC">Seychelles Island</option>

                <option ringto="+232-" value="SL">Sierra Leone</option>

                <option ringto="+65-" value="SG">Singapore</option>

                <option ringto="+421-" value="SK">Slovakia</option>

                <option ringto="+386-" value="SI">Slovenia</option>

                <option ringto="+677-" value="SB">Solomon Islands</option>

                <option ringto="+252-" value="SO">Somalia Republic</option>

                <option ringto="+685-" value="WS">Somoa</option>

                <option ringto="+27-" value="ZA">South Africa</option>

                <option ringto="+82-" value="KR">South Korea</option>

                <option ringto="+34-" value="ES">Spain</option>

                <option ringto="+94-" value="LK">Sri Lanka</option>

                <option ringto="+290-" value="SH">St. Helena</option>

                <option ringto="+1869-" value="KN">St. Kitts</option>

                <option ringto="+1758-" value="LC">St. Lucia</option>

                <option ringto="+508-" value="PM">St. Pierre</option>

                <option ringto="+1784-" value="VC">St. Vincent</option>

                <option ringto="+249-" value="SD">Sudan</option>

                <option ringto="+597-" value="SR">Suriname</option>

                <option ringto="+268-" value="SZ">Swaziland</option>

                <option ringto="+46-" value="SE">Sweden</option>

                <option ringto="+41-" value="CH">Switzerland</option>

                <option ringto="+963-" value="SY">Syria</option>

                <option ringto="+886-" value="TW">Taiwan</option>

                <option ringto="+992-" value="TJ">Tajikistan</option>

                <option ringto="+255-" value="TZ">Tanzania</option>

                <option ringto="+66-" value="TH">Thailand</option>

                <option ringto="+228-" value="TG">Togo Republic</option>

                <option ringto="+690-" value="TK">Tokelau</option>

                <option ringto="+676-" value="TO">Tonga Islands</option>

                <option ringto="+1868-" value="TT">Trinidad &amp; Tobago</option>

                <option ringto="+216-" value="TN">Tunisia</option>

                <option ringto="+90-" value="TR">Turkey</option>

                <option ringto="+993-" value="TM">Turkmenistan</option>

                <option ringto="+1649-" value="TC">Turks &amp; Caicos Island</option>

                <option ringto="+688-" value="TV">Tuvalu</option>

                <option ringto="+256-" value="UG">Uganda</option>

                <option ringto="+380-" value="UA">Ukraine</option>

                <option ringto="+971-" value="AE">United Arab Emirates</option>

                <option ringto="+44-" value="GB">United Kingdom</option>

                <option ringto="+598-" value="UY">Uruguay</option>

                <option ringto="1-" value="US">USA/Canada</option>

                <option ringto="+998-" value="UZ">Uzbekistan</option>

                <option ringto="+678-" value="VU">Vanuatu</option>

                <option ringto="+3966-" value="VA">Vatican City</option>

                <option ringto="+58-" value="VE">Venezuela</option>

                <option ringto="+84-" value="VN">Vietnam</option>

                <option ringto="+1340-" value="VI">Virgin Islands (US)</option>

                <option ringto="+681-" value="WF">Wallis/Futuna Islands</option>

                <option ringto="+967-" value="YE">Yemen Arab Republic</option>

                <option ringto="+260-" value="ZM">Zambia</option>

                <option ringto="+263-" value="ZW">Zimbabwe</option>

              </select>
              <br>
              <div class="red" id="country_error">
              </div>

            </td>
          </tr>

          <tr>
            <td><?php echo $form['email']->renderLabel(); ?><span class="red">*</span></td>
            <td><?php echo $form['email']->render(); ?>
              <br>
              <div class="red" id="email_error">
                <?php echo $form['email']->renderError(); ?>
              </div>

            </td>
          </tr>

          <tr>
            <td><?php echo $form['phone']->renderLabel(); ?><span class="red">*</span></td>
            <td> <input type="text" name="mobile_frefix" id="mobile_frefix" style="width:38px;border:1px solid #A3ABB8;padding:2px;" readonly> <?php echo $form['phone']->render(); ?>
              <br>
              <div class="red" id="phone_error">
                <?php echo $form['phone']->renderError(); ?>
              </div>

            </td>
          </tr>

        </table>
        <br/>
      </td>
      <td style="width:50%; vertical-align:top;">
        <table width="100%" style="margin:auto; margin-bottom:10px;">
          <tr>
            <td class="blbar" colspan="2" align="right">      <p>Card information</p></td>
          </tr>
          <?php
          switch ($arrCardDetail['card_type']){
            case "V" :
                $cardtype = "Visa Card";
                break;
            case "A":
                $cardtype = "American Express";
                break;

            case "M":
                $cardtype = "Master Card";
                break;
            default :
                $cardtype = "-- Not Set --";
            }

          $cardLen = $arrCardDetail['card_len'];
          $middlenum = '';
          $cardDisplay = '';
          if($cardLen == 13){
              $middlenum = "-XXXXX-";
          }else if($cardLen == 16){
              $middlenum = "-XXXX-XXXX-";
          }else if($cardLen == 15){
              $middlenum = "-XXXXXXX-";
          }
          $cardDisplay = $arrCardDetail['card_first'].$middlenum.$arrCardDetail['card_last']
            ?>
          <tr>
            <td>Card Type</td>
            <td><?php echo $cardtype?></td>
          </tr>

          <tr>
            <td>Crad Number</td>
            <td><?php echo $cardDisplay?></td>
          </tr>

          <tr>
            <td><?php echo $form['card_holder']->renderLabel(); ?><span class="red">*</span></td>
            <td><?php echo $form['card_holder']->render(); ?>
              <br>
              <div class="red" id="card_holder_error">
                <?php echo $form['card_holder']->renderError(); ?>
              </div>

            </td>
          </tr>

          <tr>
            <td><?php echo $form['expiry_date']->renderLabel(); ?><span class="red">*</span></td>
            <td><?php echo $form['expiry_date']->render(); ?>
              <br>
              <div class="red" id="expiry_date_error">
                <?php echo $form['expiry_date']->renderError(); ?>
              </div>

            </td>
          </tr>
<?php /*
          <tr>
            <td><?php echo $form['user_id_proof']->renderLabel(); ?></td>
            <td><?php echo $form['user_id_proof']->render(); ?>
              <br>
              -Please upload JPG/GIF/PNG images only.
              <br>-File size upto 200 KB.
              <div class="red" id="user_id_proof_error">
                <?php echo $form['user_id_proof']->renderError(); ?>
              </div>

            </td>
          </tr>

          <tr>
            <td><?php echo $form['address_proof']->renderLabel(); ?></td>
            <td><?php echo $form['address_proof']->render(); ?>
              <br>
              -Please upload JPG/GIF/PNG images only.
              <br>-File size upto 200 KB.
              <div class="red" id="address_proof_error">
                <?php echo $form['address_proof']->renderError(); ?>
              </div>

            </td>
          </tr>
*/?>
          <table width="100%" style="margin:auto; margin-bottom:10px;">
            <ol style="padding-left:25px;">The credit card statement must clearly show proof of your name and billing
              address as associated with the credit card. Please mask your credit card number
              middle 8 digits as <a href='javascript: void(0)' onclick='window.open("<?php echo url_for('userAuth/exampleOfCard');?>",
                "windowname1","width=400, height=150")'>described here</a>.</ol>
          </table>

        </table>
      </td>
    </tr>
  </table>
  <br/>
  <tfoot>


    <tr>

      <td colspan="2">

        <?php if (!$form->getObject()->isNew()): ?>
        &nbsp;<?php //echo link_to('Delete', 'payeasy/delete?id='.$form->getObject()->getId(), array('method' => 'delete', 'confirm' => 'Are you sure?')) ?>

        <?php endif; ?>
        <input type="hidden" name="requestId" id="requestId" value="<?php echo $requestId; ?>">
        
        <div id="payButDiv" align="center">
          <?= $form->renderHiddenFields(); ?>
          <input type="submit" value="Submit" id="payBut" name="payBut" class="normalbutton" />
          <?= $form->renderGlobalErrors(); ?>
          <input type="button" value="Cancel" onclick="gotoBack();" class="normalbutton" id="Cancel" name="cancle"/>
        </div>
      </td>
    </tr>
  </tfoot>

</form>
<br/>
<script>
  function gotoBack(){
      <?php if($mode == 1) { ?>
          var url = '<?php echo url_for('supportTool/userListAuth');?>';
          location.href=url+'/back/1';
      <?php } else { ?>
          history.go(-1);
      <?php } ?>      
  }
  
  function checkRequestedCardType(){

    if($('#applicant_vault_country').val() == "")
    {
      $('#country_error').html("Please select Country");
      return false;
    }
    else
    {
      $('#country_error').html("");
    }
    return true;
  }

  $(document).ready(function() {
    $("#applicant_vault_country").change(function() {
      var phone_prefix = $('#applicant_vault_country option:selected').attr('ringto');
      $('#mobile_frefix').val(phone_prefix);

    });


<?php if(isset ($arrCardDetail['country']) && $arrCardDetail['country'] !=''){ ?>

    $('#applicant_vault_country option[value="<?= $arrCardDetail['country'];?>"]').attr('selected', 'selected');

    var phone_prefix = $('#applicant_vault_country option:selected').attr('ringto');
    $('#mobile_frefix').val(phone_prefix);


  <?php  }

?>
<?php if(isset ($phone) && $phone !=''){ ?>
        var phone_number = '<?= $phone;?>';
        $('#applicant_vault_phone').val(phone_number);
  <?php } ?>

<?php if((isset ($arrCardDetail['year']) && isset ($arrCardDetail['month'])) && ($arrCardDetail['year'] !='' && $arrCardDetail['month'] !='')){ ?>
        var YEAR = '<?= $arrCardDetail['year'];?>';
        var MONTH = '<?= $arrCardDetail['month'];?>';
        $('#applicant_vault_expiry_date_year').val(YEAR);
        $('#applicant_vault_expiry_date_month').val(MONTH);
  <?php } ?>


 });

</script>
</div></div>
<div class="content_wrapper_bottom"></div>