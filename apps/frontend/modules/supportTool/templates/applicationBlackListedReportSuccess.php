<?php use_helper('Form');?>
<div class="brdBox">
<div class="clearfix">
      <div id="nxtPage">
        <table width="99%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
          <tr>
            <td class="blbar" colspan="12" align="right">
            <div style="float:left">NIS Application Details By SW Global LLC Order Number</div>
          </tr>
          <tr>
            <td width="4%" ><span class="txtBold">S. No.</span></td>
            <td width="9%"><span class="txtBold">Application type</span></td>
            <td width="9%"><span class="txtBold">App Id</span></td>
            <td width="12%"><span class="txtBold">Reference No</span></td>
            <td width="14%"><span class="txtBold">Applicant Name</span></td>
            <td width="9%"><span class="txtBold">Date of Birth</span></td>
            <td width="6%"><span class="txtBold">NIS Payment Status</span></td>
            <td width="6%"><span class="txtBold">Paid On</span></td>
            <td width="10%"><span class="txtBold">Email Id</span></td>
            <td width="10%"><span class="txtBold">Order Number</span></td>
            <td width="10%"><span class="txtBold">Payment Gateway</span></td>
          </tr>
          <tbody>
            <?php 
            if($isFound)
            { $i = 1;
              foreach ($appDetailArray as $result):
              
              $app['passport_id'] = $result['passport_id'];
              $app['visa_id'] = $result['visa_id'];
              $app['freezone_id'] = $result['freezone_id'];
              $appDetails = $appObj->getApplicationDetails($app);
              //            echo "<pre>";print_R($appDetails);die;
              $finalStatus = $nisHelper->isAllAppReadyRefunded($sf_params->get('order_no'),$appDetails['id']);
              $finalStatus= $finalStatus->getRawValue();
              $fStatus = null;
              if(isset ($finalStatus) && is_array($finalStatus) && count($finalStatus)>0){
                  $fStatus = $finalStatus['action'];
              }

              ?>
            <tr>
              <td width="4%"><?php echo $i;?></td>
              <td width="9%"><span><?php echo strtoupper($appDetails['app_type']);?></span></td>
              <td width="10%"><span><?php echo $appDetails['id'];?></span></td>
              <td width="9%"><span><?php echo $appDetails['ref_no'];?></span></td>
              <td width="14%"><span><?php echo $appDetails['name'];?></span></td>
              <td width="9%"><span><?php echo date_format(date_create($appDetails['dob']), 'd-m-Y');?></span></td>
              <?php if(isset ($fStatus) && $fStatus!=null){ ?>

              <td width="9%"><span><?php echo strtoupper($fStatus);?></span></td>
              <?php } else{ ?>

              <td width="9%"><span><?php echo $appDetails['status'];?> </span></td>

                    <?php  }  ?>

              <td width="9%"><span><?php if($appDetails['status'] != 'New') echo date_format(date_create($ipay4meOrderDetails[0]['paid_at']), 'd-m-Y'); else echo "--"; ?></span></td>
              <td width="10%"><span><?php echo str_replace('@',' @',$ipay4meOrderDetails[0]['email']);?></span></td>
              <td width="10%"><span><?php echo $sf_params->get('order_no');?></span></td>
              <td width="10%"><span><?php echo $gatewayName;?></span></td>

            </tr>
            <?php
            $i++;
            endforeach; ?>

            <tr>
              <td class="blbar" colspan="12" height="25px" align="right">
              </td>
            </tr>
            <?php } else { ?>
            <tr><td  align='center' class='error' colspan="11">No Results found</td></tr>
            <tr><td class="blbar" colspan="12" height="25px" align="right"></td></tr>
            <?php } ?>
          </tbody>
        </table>   
      </div>
    </div>
    </div>
