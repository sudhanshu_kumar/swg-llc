<?php use_helper('Form');
use_helper('Pagination');
?>
<script>
  $(document).ready(function()
  {
    var fmobj = document.searchApplicationForm;
    var count = 0;
    for (var i=0;i<fmobj.elements.length;i++)
    {
      var e = fmobj.elements[i];
      if ((e.type=='checkbox'))
      {
        count = parseInt(count) +1;
      }
    }
    if(count < 2)
    {
      $('#maincheck').hide();
      $('#hidbtn').html('<span class="red txtBold">All applications has been <?php echo $action.'ed'?>. You have no further permission.</span>');
    }
  });

  function validateForm(fmobj, chkAll)
  {
    $("#err_comment").html('');
    $('#err_chkbx').html("");
    if(document.getElementById('comment').value=='')
    {
      $("#err_comment").html('Please enter Reason for Refund.');
      document.getElementById('comment').focus();
      return false;
    }
    if($('#Refund_and_Block').val() == 'Refund and Block' || $('#Chargeback').val() == 'Chargeback')
    {
      for (var i=0;i<fmobj.elements.length;i++)
      {
        var e = fmobj.elements[i];
        if ((e.type=='checkbox') && (!e.disabled))
        {
          if(e.checked == false)
          {
            $('#err_chkbx').html("This operation is not allowed for partial amount.Please select all applications and proceed.");
            return false;
          }
        }
      }
      return true;
    }
    if($('#Refund').val() == 'Refund')
    {

      var sel= false;
      for (var i=0;i<fmobj.elements.length;i++)
      {
        var e = fmobj.elements[i];
        if ((e.type=='checkbox') && (!e.disabled))
        {
          if(e.checked == true)
          {
            sel= true;
            break;
          }
        }else if((e.type=='checkbox') && (e.disabled))
        {
          sel= true;
          break;
        }
      }

      if(sel)
      {
        return true;
      }
      else
      {
        $('#err_chkbx').html("Please select at least one application.");
        return false;
      }
      return false;
    }
  }


  function checkAll(fmobj, chkAll)
  {
    var amount = 0;
    var appCount = 0;
    for (var i=0;i<fmobj.elements.length;i++)
    {
      var e = fmobj.elements[i];
      if ((e.name == chkAll) && (e.type=='checkbox') && (!e.disabled))
      {
        e.checked = fmobj.chk_all.checked;
        if(fmobj.chk_all.checked)
        {
          amount = parseInt(amount)+parseInt($("#"+e.id+"amt").html());
          appCount = appCount + 1;
        }
        $("#amount").val(amount);
        $("#app_count").val(appCount);
      }
    }
  }

  function UncheckMain(fmobj, objChkAll, chkElement,obj,amt)
  {
    var boolCheck = true;
    var appCount = $("#app_count").val();
    var amount = $("#amount").val();
    if(amount ==''){
      amount = 0;
    }
    if($("#"+obj.id).is(':checked')){
      amount = parseInt(amount)+parseInt(amt);
      appCount = parseInt(appCount) + 1;
      $("#amount").val(amount);
      $("#app_count").val(appCount);
    }else{
      amount = parseInt(amount)-parseInt(amt);
      appCount = parseInt(appCount) - 1;
      $("#amount").val(amount);
      $("#app_count").val(appCount);
    }
    if(objChkAll.checked==false)
    {
      for(var i=0;i<fmobj.elements.length;i++)
      {
        if(fmobj.elements[i].name.indexOf(""+chkElement+"")!="-1" && fmobj.elements[i].checked==false)
        {
          boolCheck = false;
          break;
        }
      }
      if(boolCheck==true)
        objChkAll.checked=true;
    }
    else
    {
      objChkAll.checked=false;
    }
  }
</script>
<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">
<?php
include_partial('global/innerHeading',array('heading'=>$action." SW Global LLC Order Number"));?>
    <div class="clear"></div>
    <div class="tmz-spacer"></div>
<?php
$sf = sfContext::getInstance()->getUser();
if($sf->hasFlash('notice')){ ?>
<div id="flash_notice" class="alertBox" >
  <?php
  echo nl2br($sf->getFlash('notice'));
  ?>
</div><br/>

  <?php }?>

<?php
$sf = sfContext::getInstance()->getUser();
if($sf->hasFlash('error')){ ?>
<div id="flash_error" class="alertBox" >
  <?php
  echo nl2br($sf->getFlash('error'));
  ?>
</div><br/>

  <?php }?>
<?php
if(isset($nisAppRecords) && $nisAppRecords!='')
{?> <div align="center"><h3><?php echo $msg;?></h3></div>
  <?php } ?>

<div class="global_content4 clearfix">
<div class="brdBox">
  <div class="clearfix"/>
    <form name='searchApplicationForm' id='searchApplicationForm' action='<?php echo url_for('supportTool/orderAction');?>' method='post' class="dlForm" onsubmit="return validateForm(document.searchApplicationForm,'chk_fee[]')">


      <div class="wrapForm2">
        <table width="100%" cellpadding="0" cellspacing="0" border="0">
          <?php
//          $val = '';
//          if($$valsf_params->get('order_no'))
//          $val = $sf_params->get('order_no');
          echo "";
          echo formRowComplete('Order Number', $orderNumber);
          echo "";
          echo "";
          echo formRowComplete('Order Amount', format_amount($totAmount,1,0,$currencySymbol));
          echo "";
          echo "";
          echo formRowComplete('Total Applications', $pager->getNbResults());
          echo "";
          switch ($action){
            case "Refund":
              $label = "Reason For Refund"; ?>
              <?php
              break;
            case "Refund and Block":
              $label = "Reason For Refund";
              break;
            case "Chargeback":
              $label = "Reason For Chargeback";
              break;
          }
          echo "";
          echo formRowComplete($label.'<span style="color:red">*</span>',textarea_tag('comment', '', array('class=FieldInput')),'','','err_comment');
          echo "";          
          if($action == "Refund")
          {
            $amount = 0;
            $app_count = 0;
            $selected = '';
            $readonly = '';
          }else{
            $amount = $totAmount;
            $app_count = $pager->getNbResults();
            $selected = 'checked';
            $readonly = 'readonly';
          }
          echo "";
          echo formRowComplete(" Refund Amount".'<span style="color:red">*</span>',input_tag('amount', $amount, array('class=FieldInput','readonly'=>true)));
          echo "";
          echo "";
          echo formRowComplete(" Total Application for Refund".'<span style="color:red">*</span>',input_tag('app_count', $app_count, array('class=FieldInput','readonly'=>true)));
          echo "";
          echo "";
          echo "<tr><td colspan = '2'><div id='err_chkbx' class='red''/></td></tr>";
          echo "";
          echo "";

          ?>
          
          <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
            <tr>
              <td class="blbar" colspan="11" align="right">
              <span>Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total  <b><?php echo $pager->getNbResults(); ?></b>  results</span></td>
            </tr>
            <tr>
              <td width="2%" ><span class="txtBold" id="maincheck"> <input type="checkbox" name="chk_all" id="chk_all" onclick="checkAll(document.searchApplicationForm,'chk_fee[]');" <?php echo $selected.' '.$readonly?>></span></td>
              <td width="15%"><span class="txtBold">Application type</span></td>
              <td width="10%"><span class="txtBold">App Id</span></td>
              <td width="15%"><span class="txtBold">Reference No</span></td>
              <td width="23%"><span class="txtBold">Applicant Name</span></td>
              <td width="10%"><span class="txtBold">Status</span></td>
              <td width="10%"><span class="txtBold">Amount</span></td>
              <td width="10%"><span class="txtBold">Date of Birth</span></td>
              <td width="15%"><span class="txtBold">Email Id</span></td>
            </tr>
            <tbody>
              <?php
              if($pager->getNbResults()>0)
              {
                $i = 1;


                foreach ($pager->getResults() as $result):
                //            echo "<pre>";print_r(get_class_methods($result));die;
                $app['passport_id'] = $result['passport_id'];
                $app['visa_id'] = $result['visa_id'];
                $app['freezone_id'] = $result['freezone_id'];
                $app['visa_arrival_program_id'] = $result['visa_arrival_program_id'];
                $appDetails = $appObj->getApplicationDetails($app);
                //            echo "<pre>";print_R($appDetails);die;
                $finalStatus = $nisHelper->isAllAppReadyRefunded($orderNumber,$appDetails['id']);
                $finalStatus= $finalStatus->getRawValue();
                $fStatus = null;
                if(isset ($finalStatus) && is_array($finalStatus) && count($finalStatus)>0){
                  $fStatus = $finalStatus['action'];
                }

                ?>

                <?php
                ## Fetching visa addtional charges flag...
                $visa_additional_charges_flag = sfConfig::get('app_visa_additional_charges_flag');

                ## Getting transaction service charge object for application data...
                ## $orderRequestDetailObj = Doctrine::getTable('TransactionServiceCharges')->getApplicationDetailsByORDId($ipay4meOrderDetails[0]['order_request_detail_id']);
                $orderRequestDetailObj = Doctrine::getTable('TransactionServiceCharges')->getApplicationDetails($appDetails['id'], $appDetails['app_type'], $ipay4meOrderDetails[0]['order_request_detail_id']);             
                $serviceCharges = '';
                $processingCountry = Functions::getProcessingCountryByAppIdAndType($appDetails['id'], $appDetails['app_type']);                
                if(strtolower($appDetails['app_type']) != 'vap' && $processingCountry == 'CN'){
                    $currency_id = CurrencyManager::getCurrency($processingCountry);
                    $currencySymbol = PaymentModeManager::currencySymbol($currency_id);
                    if(isset($appDetails['convert_amount'])){
                        $appAmount = $appDetails['convert_amount'];
                        if($appDetails['app_type'] == 'visa'){
                            if(count($orderRequestDetailObj) > 0){
                              $appAmount = $orderRequestDetailObj->getFirst()->getAppConvertAmount();
                              $serviceCharges = $orderRequestDetailObj->getFirst()->getAppConvertServiceCharge();
                              if((int)$serviceCharges > 0){
                                $serviceCharges = '<i><p style="font-size:10px;text-align:">('.sfConfig::get('app_visa_additional_charges_text').': '.html_entity_decode($currencySymbol).$serviceCharges.')</p></i>'; //($'.(int)$orderRequestDetailObj->getFirst()->getServiceCharge().')
                              }else{
                                $serviceCharges = '';
                              }
                            }
                        }
                        
                    }else{
                        $appAmount = $appDetails['amount'];
                    }
                }else{
                    $appAmount = $appDetails['amount'];
                    $currency_id = 1; // means dollar...                    
                    /**
                     * [WP: 102] => CR: 144
                     * Fetching amount with addtional charges if applicable...
                     */
                    $additional_charges_flag = false;
                    if($appDetails['app_type'] == 'visa'){
                        $additional_charges_flag = true;
                    }else if ($appDetails['app_type'] == 'freezone'){
                        $isVisaEntryFreezone = Functions::isVisaEntryFreezone($appDetails['id']);
                        if($isVisaEntryFreezone){
                            $additional_charges_flag = true;
                        }
                    }
                    if($additional_charges_flag){
                      if(count($orderRequestDetailObj) > 0){                          
                          $appAmount = $orderRequestDetailObj->getFirst()->getAppAmount();
                          $serviceCharges = $orderRequestDetailObj->getFirst()->getServiceCharge();
                          if((int)$serviceCharges > 0){
                            $serviceCharges = '<i><p style="font-size:10px;text-align:">('.sfConfig::get('app_visa_additional_charges_text').': $'.$serviceCharges.')</p></i>';
                          }else{
                            $serviceCharges = '';
                          }
                      }
                    }
                }                
                ?>
              <tr>
                <td width="2%"><span><?php if($appDetails['status']!="New") { ?> <input type="checkbox" name="chk_fee[]" id="<?= "chk_fee".$i; ?>" value="<?= $appDetails['id']."_".$appDetails['app_type']."_".$appAmount."_".$currency_id;?>" OnClick="UncheckMain(document.searchApplicationForm,document.searchApplicationForm.chk_all,'chk_fee',<?= "chk_fee".$i; ?>,'<?= $appAmount;?>','<?php echo $currency_id; ?>');" <?php echo $selected.' '.$readonly?>> <?php } ?></span></td>
                <td width="12%"><span><?php if($appDetails['app_type'] == 'vap'){echo sfConfig::get('app_visa_arrival_title');}else{echo strtoupper($appDetails['app_type']); }?></span></td>
                <td width="10%"><span><?php echo $appDetails['id'];?></span></td>
                <td width="12%"><span><?php echo $appDetails['ref_no'];?></span></td>
                <td width="23%"><span><?php echo $appDetails['name'];?></span></td>

                  <?php if(isset ($fStatus) && $fStatus!=null){ ?>

                <td width="10%"><span><?php echo strtoupper($fStatus);?></span></td>
                <?php } else{ ?>

                <td width="10%"><span><?php echo $appDetails['status'];?></span></td>

                    <?php  } ?>

                <td width="14%" id="<?= "chk_fee".$i."amt"; ?>">
                    <?php echo $appAmount;?>
                    <?php echo $serviceCharges; ?>
                </td>
                <td width="10%"><span><?php echo date_format(date_create($appDetails['dob']), 'd-m-Y');?></span></td>
                <td width="15%"><span><?php echo $ipay4meOrderDetails[0]['email'];?></span></td>


              </tr>
              <?php
              $i++;
              endforeach; ?>

              <tr>
                <td class="blbar" colspan="11" height="25px" align="right">
                  <div class="paging pagingFoot">
                    <?php
                    echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName()).'?order_no='.Settings::encryptInput($orderNumber).'&pAction='.Settings::encryptInput($action).'&usePager=true','detail_div');
                    ?>
                  </div>
                </td>
              </tr>
              <?php } else { ?>
              <tr><td  align='center' class='error' colspan="11">No Results found</td></tr>
              <?php } ?>
            </tbody>

          </table>
          <?php //} ?>
          <tr>
            <td>&nbsp;</td>
            <td><div id="hidbtn" align="center">
                  <input type="hidden" name="pAction" value="<?= Settings::encryptInput($action); ?>">
                  <input type="hidden" name="order_no" value="<?= Settings::encryptInput($orderNumber); ?>">
                  <?php   $btnId = str_replace(' ', '_', $action);                  
                  echo submit_tag($action,array('class' => 'normalbutton','id' =>"$btnId"));
                  ?>
               
                </div></td>
          </tr>
        </table>
      </div>
    </form>
  </div></div>
</div>
  </div>
      </div>
<div class="content_wrapper_bottom"></div>
<!--<INPUT TYPE=CHECKBOX NAME=" <?= "application_".$appDetails['id']."_".strtoupper($appDetails['app_type']); ?>" ALIGN="center">-->