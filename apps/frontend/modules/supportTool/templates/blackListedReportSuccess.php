<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
<div class="content_container">
<?php
use_helper('Pagination');
use_helper('Form');
include_partial('global/innerHeading',array('heading'=>'Black Listed Users Report')); 
?>
<div class="clear"></div>
<div class="tmz-spacer"></div>
<?php //echo form_tag('supportTool/blackListedReport',array('name'=>'order_search_form','class'=>'', 'method'=>'get','id'=>'order_search_form','onSubmit'=>'return validateForm()')) ?>
<form action="#" method="post" name="frm_blacklist_report" id="frm_blacklist_report" onsubmit="return validateForm()" > 
      <?php
      $sf = sfContext::getInstance()->getUser();
      if($sf->hasFlash('notice')){ ?>
      <div id="flash_notice" class="alertBox" >
        <?php
        echo nl2br($sf->getFlash('notice'));
        ?>
      </div><br/>
      <?php }?>


        <?php
        $sf = sfContext::getInstance()->getUser();
        if($sf->hasFlash('error')){ ?>
        <div id="flash_error" class="alertBox" >
          <?php
          echo nl2br($sf->getFlash('error'));
          ?>
        </div><br/>
        <?php }?>
    <table width="99%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
    <tr>
    <td width="16%"> From Date<span class="red"></span> : </td>
    <?php $fdate = (isset($fdate))?$fdate:'' ?>
    <td width="84%"> <?php echo input_tag('fdate',$fdate,array('id' => 'fdate','size'=>'20','maxlenght'=>'10','class'=>'txt-input','onfocus'=>'showCalendarControl(fdate)','readonly'=>'true')); ?>    </tr>
    <tr>
    <td> To Date<span class="red"></span>  : </td>
    <?php $tdate = (isset($tdate))?$tdate:'' ?>
    <td> <?php echo input_tag('tdate',$tdate,array('id' => 'tdate','size'=>'20','maxlenght'=>'10','class'=>'txt-input','onfocus'=>'showCalendarControl(tdate)','readonly'=>'true')); ?>
    </tr>
    <tr><td colspan = "2" align="left"> <strong>OR </strong></td>
    </tr>
    <tr>
    <td> Card First Four Digit<span class="red"></span>  : </td>
    <?php $cardFirst = (isset($cardFirst))?$cardFirst:'' ?>
    <td> <?php echo input_tag('card_first',$cardFirst,array('id'=>'cardFirst','size'=>'20','maxlenght' => 4,'class'=>'txt-input')); ?>
    </tr>
    <tr>
    <td> Card Last Four Digit<span class="red"></span>  : </td>
    <?php $cardLast = (isset($cardLast))?$cardLast:'' ?>
    <td> <?php echo input_tag('card_last',$cardLast,array('id'=>'cardLast','size'=>'20','maxlenght'=> 4,'class'=>'txt-input')); ?>
    </tr>
    <tr> <td align="center" colspan="2"><span class="red">Note: Please enter any combination from above</span></td></tr>
   <tr><td colspan ="2" align="center"><input type = "submit" value="Search" class="loginbutton"></td></tr>
   </table>
</form>
<div class="clear">&nbsp;</div>
<?php if($isReport) {?>
<table width="99%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
   <tr>
      <td class="blbar" colspan="7" align="right">
      <div class="float_left">Black Listed Users</div>
      <span>Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total  <b><?php echo $pager->getNbResults(); ?></b>  results</span></td>
   </tr>
   <tr>
       <td class="txtBold">Sr No.</td>
       <!--td class="txtBold">Customer Name</td-->
       <!--td class="txtBold">Email</td-->
       <td class="txtBold">Card Number</td>
       <td class="txtBold">Blocked Date</td>
       <td class="txtBold">Card Holder Name</td>
       <td class="txtBold">Order Number</td>

   </tr>

       <?php if($pager->getNbResults() >0) {
           $i = $pager->getFirstIndice();
         foreach($pager->getResults() as $details):
//         echo "<pre>";print_r($details);die;
              $cardLen = $details['card_len'];
              $middlenum = '';
              $cardDisplay = '';
              if($cardLen == 13){
                $middlenum = "-XXXXX-";
              }else if($cardLen == 16){
                $middlenum = "-XXXX-XXXX-";
              }else if($cardLen == 15){
                $middlenum = "-XXXXXXX-";
              }
              $cardDisplay = $details['card_first'].$middlenum.$details['card_last'];

         ?>
         <tr>
         <td> <?php echo $i; ?> </td>
         <!--td> <?php //echo strtoupper($details['fname']).' '.strtoupper($details['lname']); ?></td-->
         <!--td> <?php //echo $details['email']; ?> </td-->
         <td> <?php echo $cardDisplay; ?> </td>
         <td> <?php echo date_format(date_create($details['blocked_date']),'d-m-Y'); ?> </td>
         <td> <?php echo ucfirst($details['card_holder']);  ?></td>
         <td> <a href ="javascript:void(0);"  onclick="findApplications('<?php echo Settings::encryptInput($details['order_number']); ?>','blackListedReport')"> <?php echo $details['order_number']; ?></a></td>

         </tr>
   <?php $i++; endforeach; } else { ?>
     <tr><td  align='center' class='red' colspan="7">No Record Found </td></tr>
   <?php } ?>
        <tr>
            <td class="blbar" colspan="7" height="25px" align="right">
             <div class="paging pagingFoot"><?php
             $url = '';
             if(!empty ($fdate) && !empty ($tdate)){
               $url .= '?fdate='.$fdate.'&tdate='.$tdate;
             }else if(!empty ($cardFirst) && !empty ($cardLast))
             {
               $url .= '?card_first='.$cardFirst.'&card_last='.$cardLast;
             }
             echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName().$url), 'nxtPage') ?>
             </div>
            </td>
        </tr>
   </table>
   <br/>
    <center class='multiFormNav'>
   <?php if($pager->getNbResults()>0) {?>
    <input type='button' value='Export to Excel' class="normalbutton" onclick="window.open('<?php echo _compute_public_path($filename, 'uploads/excel', '', true); ?>');return false;">&nbsp;
   <?php } ?>
    <input type = "button" value = "Back" name ="back" class="normalbutton" onclick = "javascript:history.go(-1)">
   </center>
<br/>
<?php }?>

 <div id="result_div" align="center"></div>
  <div id="application_div"></div>
</div>
</div><div class="content_wrapper_bottom"></div>
<!--</div>-->
<script type="text/javascript">
   function validateForm(){
     var start_date = jQuery.trim($('#fdate').val());
      var end_date = jQuery.trim($('#tdate').val());
      var cardFirst = jQuery.trim($('#cardFirst').val());
      var cardLast = jQuery.trim($('#cardLast').val());
      if(cardFirst != ''){
          if(cardLast == '' || isNaN(cardLast)){
              alert('Please enter valid Card Last Four Digit');
              return false;
              $('#cardLast').focus('');
          }
      }
      if(cardLast != ''){
          if(cardFirst == ''|| isNaN(cardFirst)){
              alert('Please enter valid Card First Four Digit');
              return false;
              $('#cardFirst').focus('');
          }
      }
 if(cardFirst != ''){
      if(cardLast.length > 4)
          {
              alert('Please enter only 4 digits for Card Last Four Digit');
              return false;
              $('#cardLast').focus('');
          }
       }
  if(cardLast != ''){
      if(cardFirst.length > 4)
          {
              alert('Please enter only 4 digits for Card First Four Digit');
              return false;
              $('#cardFirst').focus('');
          }
        }
  if(cardFirst != ''){
      if(cardLast.length < 4)
          {
              alert('Please enter minimum 4 digits for Card Last Four Digit');
              return false;
              $('#cardLast').focus('');
          }
         }
  if(cardLast != ''){
      if(cardFirst.length < 1)
          {
              alert('Please enter minimum 1 digit for Card First Four Digit');
              return false;
              $('#cardFirst').focus('');
          }
          }
      var current_date  = new Date();
      
      if(start_date != ''){
          if(end_date == ''){
              alert('Please enter To Date');
              return false;
              $('#tdate').focus('');
          }
      }
      if(end_date != ''){
          if(start_date == ''){
              alert('Please enter From Date');
              return false;
              $('#fdate').focus('');
             }
      }
     if(start_date != ''){
      var sdate = start_date.split('-');
      start_date = new Date(sdate[0],sdate[1]-1,sdate[2]);

      if(start_date.getTime()>current_date.getTime()) {
        alert('From date cannot be future date');
        return false;
      }
    }

    if(end_date != ''){
      var edate = end_date.split('-');
      end_date = new Date(edate[0],edate[1]-1,edate[2]);

      if(end_date.getTime()>current_date.getTime()) {
        alert('To date cannot be future date');
        return false;
      }
    }

    if((start_date != '') && (end_date != '')){
      if(start_date.getTime()>end_date.getTime()) {
        alert('From date cannot be greater than To date');
        return false;
      }
    }

    if(start_date == '' && end_date == '' && cardFirst == '' && cardLast == ''){
        alert('Please Enter any combination from both');
        return false;
        $('#fdate').focus('');
    }
}
  function findApplications(orderNumber,blackListedReport){
      var url = "<?php echo url_for('supportTool/searchOrderNo'); ?>";
      $("#application_div").hide();
      $('#result_div').html('<?php echo image_tag('../images/ajax-loader.gif'); ?>');
      $("#result_div").show();
      
      $.get(url, {order_no: orderNumber,template : blackListedReport }, function(data) { 
          $("#application_div").html(data);
          $("#application_div").show();
          $("#application_div").focus();
          $("#result_div").hide();
      })      
  }

</script>


