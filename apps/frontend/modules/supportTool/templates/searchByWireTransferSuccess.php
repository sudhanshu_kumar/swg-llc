<script>
  function validate_form(fmobj)
  {
    var sel= false;
    for (var i=0;i<fmobj.elements.length;i++)
    {
      var e = fmobj.elements[i];
      if ((e.type=='checkbox') && (!e.disabled))
      {
        if(e.checked == true)
        {
          sel= true;
          break;
        }
      }else if((e.type=='checkbox') && (e.disabled))
      {
        sel= true;
        break;
      }
    }

    if(sel)
    {
      
      return true;
    }
    else
    {
      $('#check_box_error').html("Please select at least one tracking number");
      
      return false;
    }
    return false;
  }

    function checkAll(fmobj, chkAll)
  {
    var amount = 0;
    $('#amt_to_paid_error').html('');
    for (var i=0;i<fmobj.elements.length;i++)
    {
      var e = fmobj.elements[i];
      if ((e.name == chkAll) && (e.type=='checkbox') && (!e.disabled))
      {
        e.checked = fmobj.chk_all.checked;
        if(fmobj.chk_all.checked)
        {
          amount = parseInt(amount)+parseInt($("#"+e.id+"amt").html());
        }
        $("#amt_to_paid").val(amount);
        $("#disp_amt").html(amount+".00");
      }
    }
  }


  function UncheckMain(fmobj, objChkAll, chkElement,obj)
  {
    var boolCheck = true;
    
    
    if(objChkAll.checked==false)
    {
      for(var i=0;i<fmobj.elements.length;i++)
      {
        if(fmobj.elements[i].name.indexOf(""+chkElement+"")!="-1" && fmobj.elements[i].checked==false)
        {
          boolCheck = false;
          break;
        }
      }
      if(boolCheck==true)
        objChkAll.checked=true;
    }
    else
    {
      objChkAll.checked=false;
    }
  }

</script>
<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">
    <div class="clear"></div>
<div class="tmz-spacer"></div>
<?php
use_helper('Pagination');
use_helper('Form');
include_partial('global/innerHeading',array('heading'=>'Wire Transfer Info'));
?>


    <?php echo form_tag('supportTool/searchByWireTransfer',array('name'=>'order_search_form','class'=>'', 'method'=>'post','id'=>'order_search_form')) ?>

 
      <?php
      $sf = sfContext::getInstance()->getUser();
      if($sf->hasFlash('notice')){ ?>
      <div id="flash_notice" class="alertBox" >
        <?php
        echo nl2br($sf->getFlash('notice'));
        ?>
      </div><br/>
      <?php }?>


        <?php
        $sf = sfContext::getInstance()->getUser();
        if($sf->hasFlash('error')){ ?>
        <div id="flash_error" class="alertBox" >
          <?php
          echo nl2br($sf->getFlash('error'));
          ?>
        </div>
        <?php }?>
<div class="clear"></div>
        <table width="100%">
          <?php

          $wireTransfer = (isset($_REQUEST['wire_transfer']))?$_REQUEST['wire_transfer']:"";
          echo formRowComplete('Wire Transfer Number',input_tag('wire_transfer', $wireTransfer, array('size' => 27, 'maxlength' => 30,'class'=>'txt-input')),'','wire_transfer','detail_error','wire_transfer_row');
          echo "</tr>";
          ?>
         
          <tr valign="top">
            <td height="30" valign="top" >&nbsp;</td>
            <td height="30" valign="top">
                <?php   echo submit_tag('Search',array('class' => 'normalbutton')); ?>
       
              </td>
          </tr>
        </table>
     
    


  </form>
  <br>
  

  

  
  <?php if($isFound){?>
  <div class="clearfix">
    <div id="nxtPage">
        <?php echo form_tag('supportTool/updateWireTransfer',array('name'=>'WTUpdateFrm','class'=>'', 'method'=>'post','id'=>'WTUpdateFrm','onsubmit'=>'return validate_form(document.WTUpdateFrm)')) ?>
        
      <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" id="hdn_datatable">
        <tr>
          <td class="blbar" colspan="6" align="right">
            <div style="float:left">Tracking Number</div>
          <span>Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total  <b><?php echo $pager->getNbResults(); ?></b>  results</span>
          </td>
        </tr>
        <tr bgcolor="#eeeeee">
          
          <td width="5%"><span class="txtBold">S.No.</span></td>
          <td width="15%"><span class="txtBold">Association Date</span></td>
          <td width="20%"><span class="txtBold">Wire Transfer Serial Number</span></td>
          <td width="20%"><span class="txtBold">Amount</span></td>
          <td width="20%"><span class="txtBold">Wire Transfer Receipt</span></td>
          <td width="15%" ><span class="txtBold" id="maincheck">Payment received <br> <input type="checkbox" name="chk_all" id="chk_all" onclick="checkAll(document.WTUpdateFrm,'hdn_wt_id[]');" >&nbsp;&nbsp;(Check All)</span></td>

        </tr>
        <tbody>
          <?php $j = $pager->getFirstIndice();
          if($pager->getNbResults()>0){
            foreach ($pager->getResults() as $details):
           ?>
          <tr>
            <td width="5%"><span><?= $j ?></span></td>
            <td width="15%"><span><?= $details['associated_date'] ?></span></td>
            <td width="20%"><span><a href="<?php echo url_for('supportTool/wireTransferDetails?wire_transfer_id='.$details['id']);?>"><?= $details['wire_transfer_number'] ?></a></span></td>
            <td width="20%"><span><?= format_amount($details['amount'],1) ?></span></td>
            <td width="20%">
              <?php if($details['wire_transfer_proof'] != ''){ ?>
              <a href="#" onclick="javascript:window.open('<?= _compute_public_path($details['wire_transfer_proof'], 'uploads/wire_transfer_receipt/'.$details['user_id'], '', true);;?>','MyPage','width=750,height=700,scrollbars=1,resizable=yes');">View Wire Transfer Receipt</a>
              <?php } ?>
            </td>

            <td width="15%"><input type="checkbox" id="hdn_wt_id_<?php echo $details['id']; ?>" name="hdn_wt_id[]" value="<?php echo $details['id']; ?>"  OnClick="UncheckMain(document.WTUpdateFrm,document.WTUpdateFrm.chk_all,'hdn_wt_id',<?php echo $details['id']; ?>);">
            </td>
          </tr>
      <?php $j++;
          endforeach; ?>
          <tr>
            
              <td colspan="6" align="center">
              <div id="check_box_error" style="text-align:left;color:red;">&nbsp;</div>
                <?php echo submit_tag('Update',array('class' => 'loginbutton','width'=>'60px')); ?>
              </td>
          </tr>
          <?php } else { ?>
          <tr>
            <td  align='center' class='error' colspan="9"><font color="red">No record Found</font></td>
          </tr>
          <?php } ?>
      <tr>
              <td class="blbar" colspan="50" height="25px" align="right">
                <div class="paging pagingFoot">
                  <?php
                  echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName()).'?email='.$sf_params->get('email'),'detail_div');
                  ?>
                </div>
              </td>
            </tr>
        </tbody>

      </table>
      </form>
      <br>
    </div>
  </div>
  <?php } 
  ?>
  

  </div>
</div>
<div class="content_wrapper_bottom"></div>