<?php

/**
 *  order actions.
 *
 * @package    IP4M
 * @subpackage  privilageUser
 * @author     KSingh
 * @version    SVN: $Id: actions.class.php 12479 2011-04-29 10:54:40Z fabien $
 */
class privilageUserActions extends sfActions {

    public function executeIndex(sfWebRequest $request) {

        /**
         * [WP: 107] => CR: 152
         * Changes to incarease performance of privilege tool...
         * Now by default privilege user list will be displayed.
         * User can search from non - privilege user and then convert them to privilege user...
         */

        $fname = $request->getParameter("fname");
        $lname = $request->getParameter("lname");
        $email = $request->getParameter("email");
        $status = $request->getParameter("status");
        $userList = $request->getParameter("userList");

        //creating session
        if ($request->isMethod('post')) {
            
            $this->getUser()->setAttribute('fname', $fname);
            $this->getUser()->setAttribute('lname', $lname);
            $this->getUser()->setAttribute('email', $email);
            $this->getUser()->setAttribute('status', $status);
            $this->getUser()->setAttribute('userList', $userList);

        } else if ($request->hasParameter('back')) {
            
            $fname = $this->getUser()->getAttribute('fname');
            $lname = $this->getUser()->getAttribute('lname');
            $email = $this->getUser()->getAttribute('email');
            $status = $this->getUser()->getAttribute('status');
            $userList = $this->getUser()->getAttribute('userList');



        } else {           

            $this->getUser()->setAttribute('fname', NULL);
            $this->getUser()->setAttribute('lname', NULL);
            $this->getUser()->setAttribute('email', NULL);
            $this->getUser()->setAttribute('status', 'all');
            $this->getUser()->setAttribute('userList', 'pu');


            $fname = $this->getUser()->getAttribute('fname');
            $lname = $this->getUser()->getAttribute('lname');
            $email = $this->getUser()->getAttribute('email');
            $status = $this->getUser()->getAttribute('status');
            $userList = $this->getUser()->getAttribute('userList');

        }
        $this->mode = $request->getParameter('mode');

        if($this->mode == "edit"){
            $this->endLimit = $request->getParameter('page');
        }
        $this->status = $status ;
        if($status == 'all'){
            $status = '';
        }

        
        $userStatus = ($userList == 'pu')?'':'temp';

        if($userList == 'pu'){
            $arrTotalUser = Doctrine::getTable('PaymentPrivilageUser')->getAllActivePaymentUserQuery($fname, $lname, $email, $status, $userStatus);
            $tableText = 'PaymentPrivilageUser';
        }else{
            $arrTotalUser = Doctrine::getTable('UserDetail')->getAllActivePaymentUserQuery($fname, $lname, $email);
            $tableText = 'UserDetail';
        }

        $this->page = 1;
        if ($request->hasParameter('page')) {
            $this->page = $request->getParameter('page');
        }
        $this->pager = new sfDoctrinePager($tableText, sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($arrTotalUser);
        $this->pager->setPage($this->page);
        $this->pager->init();

    }

    /*     * function executeSetStatus()
     * @purpose :set user status
     * @param : user_id
     * @return :  boolean
     * @author : KSingh
     * @date : 02-05-2011
     */

    public function executeSetStatus(sfWebRequest $request) {
        $this->setTemplate(null);
        $id = $request->getParameter('id');
        $status = $request->getParameter('status');
        $page = $request->getParameter('page');
        if ($status != '') {
            //get user detail
            $user = Doctrine::getTable('PaymentPrivilageUser')->getUserPreviousDetail($id);
            if (!empty($user)) {
                //update user status
                if ($status == "active") {
                    $deleted = '0';
                } else {
                    $deleted = '1';
                }
                $isUpdated = Doctrine::getTable('PaymentPrivilageUser')->updateStatus($id, $status, $deleted);
            } else {
                $saveUser = new PaymentPrivilageUser();
                $saveUser->setUserId($id);
                $saveUser->setStatus($status);
                $saveUser->save();
            }

            if ($status == "active") {

                $this->getUser()->setFlash('notice', 'User activated successfully.');
            }
            if ($status == "Inactive") {

                $this->getUser()->setFlash('notice', 'User inactivated successfully.');
            }
        }
        $this->redirect('privilageUser/index?page=' . $page . '&back=1');
    }

    public function executeAddPriviligeUserValue(sfWebRequest $request){

        $totalRecords = $request->getParameter('totalRecords');

        if($request->getParameter('id')!=''){
            $this->id = $request->getParameter('id');
        } else{
            $this->id = '';
        }
        $this->mode = $request->getParameter('mode');
        //if($this->mode == "edit"){
        $this->endLimit = $request->getParameter('page');
        //}
        $this->setLayout(false);
        $this->form = new NewPriviligeUserValueForm();

        if($this->mode == 'edit'){
            //set default
            $arrPrivilageUser = Doctrine_Core::getTable('PaymentPrivilageUser')->find($this->id);
            $midServiceArr=explode(',',$arrPrivilageUser->getMidService());
            $cardtypeArr=explode(',',$arrPrivilageUser->getCardType());

            $this->id = $arrPrivilageUser->getId();
            $this->userId = $arrPrivilageUser->getUserId();
            $this->form->setDefault('mid_service', $midServiceArr);
            $this->form->setDefault('status', $arrPrivilageUser->getStatus());
            $this->form->setDefault('card_type', $cardtypeArr);

        }
        if($this->mode == 'add'){
            //set default
            $this->id = '';
            $this->userId = $request->getParameter('id');

        }

    }
    public function executeSavePriviligeUserValue(sfWebRequest $request){
        $this->setLayout(false);
        $this->setTemplate(NULL) ;
        $postArray = $request->getPostParameters();
        $serviceArr = $postArray['mid_service'];
        $cardTypeArr = $postArray['card_type'];
        $postArray['mid_service'] = implode(',',$serviceArr);
        $postArray['card_type'] = implode(',',$cardTypeArr);
        $data = Doctrine::getTable("PaymentPrivilageUser")->find($postArray['hdn_id']);


        $flagError = false;
        if($data != ''){
            if($data['user_id']==$postArray['hdn_user_id']){
                if(!empty ($postArray['status'])){
                    $flagError = true;
                    $this->saveData($postArray);
                }
            }
        }
        if($request->getParameter('hdn_mode')=='add')
        {
            $flagError=true;
            $this->saveData($postArray);
        }
        if($flagError)
        {
            sfContext::getInstance()->getUser()->setAttribute('sessMsg', 'Value(s) successfully Assigned for Privilege User.');
            return $this->renderText('success');
        }
        else
        {
            sfContext::getInstance()->getUser()->setAttribute('sessMsg', 'Value(s) not Assigned due to some technical problem.');
            return $this->renderText('failed');
        }

    }

    private function saveData($postArray)
    {
        if(!empty($postArray['status'])){
            if($postArray['hdn_mode']=='edit')
            {
                $paymentPrivilageUserAdd = Doctrine::getTable("PaymentPrivilageUser")->find($postArray['hdn_id']);
                $paymentPrivilageUserAdd->setStatus($postArray['status']);
                $paymentPrivilageUserAdd->setMidService($postArray['mid_service']);
                $paymentPrivilageUserAdd->setCardType($postArray['card_type']);
                $paymentPrivilageUserAdd->save();
            }
            if($postArray['hdn_mode']=='add')
            {
                $paymentPrivilageUserAdd = new PaymentPrivilageUser();
                $paymentPrivilageUserAdd->setStatus($postArray['status']);
                $paymentPrivilageUserAdd->setMidService($postArray['mid_service']);
                $paymentPrivilageUserAdd->setCardType($postArray['card_type']);
                $paymentPrivilageUserAdd->setUserId($postArray['hdn_user_id']);
                $paymentPrivilageUserAdd->save();
            }
        }
    }


}
