<?php use_stylesheet('style.css');
use_javascript('calendar.js');
echo include_stylesheets();
echo include_javascripts(); ?>
<div id="flash_notice" class="alertBox" style="display:none"></div>
<div class="clearfix">
    <div id="nxtPage">
  <form action='#' method="post" name="frm_tra_detail" id="frm_tra_detail" onsubmit="return false;">
      <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
      
      <tr>
          <td ><?php echo $form['status']->renderLabel(); ?><span class="red"></span></td>
          <td colspan="2"><?php echo $form['status']->render(); ?>
              <br>
              <div class="red" id="status_error">
                  <?php echo $form['status']->renderError(); ?>
              </div>

          </td>
      </tr>
      <tr>
          <td ><?php echo $form['mid_service']->renderLabel(); ?><span class="red"></span></td>
          <td colspan="2"><?php echo $form['mid_service']->render(); ?>
              <br>
              <div class="red" id="mid_service_error">
                  <?php echo $form['mid_service']->renderError(); ?>
              </div>

          </td>
      </tr>
      <tr>
          <td ><?php echo $form['card_type']->renderLabel(); ?><span class="red"></span></td>
          <td colspan="2"><?php echo $form['card_type']->render(); ?>
              <br>
              <div class="red" id="card_type_error">
                  <?php echo $form['card_type']->renderError(); ?>
              </div>
          </td>
      </tr>
      <input type="hidden" name="hdn_id" id="hdn_id" value="<?php echo $id;?>">
      <input type="hidden" name="hdn_user_id" id="hdn_user_id" value="<?php echo $userId;?>">
      <input type="hidden" name="hdn_mode" id="hdn_user_id" value="<?php echo $mode;?>">
      <tr>
        <td align="center" colspan="3">
          <div class="divBlock">
            <center><input style="" type="button" class="loginbutton"  id="multiFormSubmit" value="Save" onclick="validateForm();">
            &nbsp;&nbsp;
            <input style="" type="button" class="loginbutton"  id="multiFormSubmit" value="Close" onclick="parent.emailwindow.hide();">

            </center>
          </div>
        </td>
      </tr>
    </table>
    </form>
    <br>
  </div>
</div>

<script type="text/javascript">


  function validateForm(){ 
     var err  = 0;
     var reg = /^[\-0-9a-zA-Z\,_]+$/;
     var reg1 = /^[\a-zA-Z\ ]+$/;
  
      if($('#status').val() == "" || reg1.test($('#status').val()) == false)
      {
        $('#status_error').html("Please enter valid Status");
        err = err+1;
      }else
      {
        $('#status_error').html(" ");
      }

      if($('#card_type').val() == null)
    {
      $('#card_type_error').html("Please select Card Type");
      err = err+1;
    }else
    {
      $('#card_type_error').html(" ");
    }

      if($('#mid_service').val() == null)
      {
        $('#mid_service_error').html("Please enter valid Mid Service");
        err = err+1;
      }else
      {
        $('#mid_service_error').html(" ");
      }


      if(err == 0)
      {
        var posturl = "<?php echo url_for('privilageUser/savePriviligeUserValue'); ?>";
          $.ajax({
               type: "POST",
               url: posturl,
               data: $('#frm_tra_detail').serialize(),
               success: function(msg){
                 if(jQuery.trim(msg) == 'success'){
                    parent.emailwindow.hide();
                    var url = "<?php echo url_for("privilageUser/index?page=".$endLimit);?>";
                    window.parent.location =  url;
                 }
                else if(jQuery.trim(msg) == 'failed'){
                    parent.emailwindow.hide();
                    window.parent.location.reload();
                 }
               }
             });

    }else{
          return false;
    }
  }
  </script>