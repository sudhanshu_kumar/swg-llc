  
<?php if ($sf_user->hasFlash('notice')): ?>
  <div class="flash_notice" style="font-size: 12pt;color: red;"><?php echo $sf_user->getFlash('notice') ?></div>
<?php endif ?>
  
<p>Cart Items:</p>
<table>
  <thead>
  <th>Type</th>
  <th>Name</th>
  <th>Application Id</th>
  <th>Actions</th>
  </thead>
<?php
$items = $sf_user->getCartItems();
if (!$items) {
  // emptry set, no point in continuing
  return;
}

foreach ($items as $item) {
  echo '<tr>';
  echo '<td>'.$item->getType().'</td>';
  echo '<td>'.$item->getName().'</td>';
  echo '<td>'.$item->getAppId().'</td>';
  echo '<td><a href="'
          .url_for('cart/remove?id='.$item->getAppId().'&type='.$item->getType())
          .'">Delete</a></td>';
  echo '</tr>';
}
?>
</table>
