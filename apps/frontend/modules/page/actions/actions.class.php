<?php

/**
 * Pages actions.
 *
 * @package    symfony
 * @subpackage Pages
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12474 2008-10-31 10:41:27Z fabien $
 */
class PageActions extends sfActions {

    public function executePrintMe(sfWebRequest $request) {
        $this->setLayout('ipay4me_print');


        $this->setTemplate('printMe');
    }

    public function executeAdd(sfWebRequest $request) {
        $id = $request->getParameter('id');
        $type = $request->getParameter('type');
        $this->logMessage("Adding with id $id and type $type .");
        if ($type == 'p') {
            $table = "PassportApplication";
        } elseif ($type == 'v') {
            $table = "VisaApplication";
        } else {
            return;
        }
        $app = Doctrine::getTable($table)->find($id);
        $item = $app->getCartItem();
        $this->getUser()->addCartItem($item);
        $this->getUser()->setFlash('notice', "Application Added", false);
    }

    public function executeList(sfWebRequest $request) {
        
    }

    public function executeClear(sfWebRequest $request) {
        $this->getUser()->clearCart();
        $this->setTemplate('add');
    }

    public function executeRemove(sfWebRequest $request) {
        $id = $request->getParameter('id');
        $type = $request->getParameter('type');
        $this->getUser()->removeCartItemById($id, $type);
        $this->getUser()->setFlash('info', "Application Removed", false);
        $this->setTemplate('list');
    }

    public function executePrintPassport(sfWebRequest $request) {
        $this->setTemplate('printPassport');
    }

    public function executePrintInstruction()
    {
        $this->setLayout(false);
        $this->screenMsgType = 'app_other_based_country_content';
    }
}
