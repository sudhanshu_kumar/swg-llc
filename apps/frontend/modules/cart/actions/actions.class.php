<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class CartActions extends sfActions {

    public function executeList(sfWebRequest $request) {

        ## This varibale is coming from nmi...
        ## Removing this variable...
        $this->getUser()->getAttributeHolder()->remove('ses_order_number');
        $cartValidationObj = new CartValidation();
        $arrCartItems = $this->getUser()->getCartItems();
        $arrProcessingCountry = array();
        foreach ($arrCartItems as $items) {
            $application_type = $items->getType();
            if ($items->getType() == 'Passport') {
                //get Application details
                $arrProcessingCountry[] = Doctrine::getTable('PassportApplication')->getProcessingCountry($items->getAppId());
            } elseif ($items->getType() == 'Freezone') {
                $arrProcessingCountryId = Doctrine::getTable('ReEntryVisaApplication')->getProcessingCountry($items->getAppId());
                $arrProcessingCountry[] = Doctrine::getTable('VisaProcessingCentre')->getProcessingCentre($arrProcessingCountryId);
            } elseif ($items->getType() == 'Visa') {
                $arrProcessingCountry[] = Doctrine::getTable('VisaApplicantInfo')->getProcessingCountry($items->getAppId());
            } elseif ($items->getType() == 'Vap') {
                $arrProcessingCountry[] = Doctrine::getTable('VapApplication')->getProcessingCountry($items->getAppId());
            }
        }


        $this->form = new NisApplicationSearchForm();
        $this->appDetails = $request->getParameter('appDetails');
        $this->isPrivilegeUser = $this->getUser()->isPrivilegeUser();

        $this->formValid = "";
        if ($request->isMethod('post')) {
            if ($request->getParameter('search')) {
                $this->form->bind($request->getParameter('search'));
                if ($this->form->isValid()) {
                    $errorMsgObj = new ErrorMsg();
                    $this->formValid = "valid";
                    $parameters = $request->getParameter('search');
                    $application_type = $parameters['application_type'];
                    $application_id = $parameters['application_id'];

                    if ($application_type == 'p') {
                        $table = "PassportApplication";
                        $displayApplicationType = 'Passport';
                    } else if ($application_type == 'v') {
                        $table = "VisaApplication";
                        $displayApplicationType = 'Visa/Freezone';
                    } else if ($application_type == 'vap') {
                        $table = "VapApplication";
                        $displayApplicationType = 'Visa on Arrival';
                    }

                    $app = Doctrine::getTable($table)->find($application_id)->toArray();

                    if (!empty($app)) {

                        if ($application_type == 'p') {
                            $arrForVerification = array();
                            $arrForVerification['fname'] = $app['first_name'];
                            $arrForVerification['mname'] = $app['mid_name'];
                            $arrForVerification['lname'] = $app['last_name'];
                            $arrForVerification['dob'] = $app['date_of_birth'];
                        } else if ($application_type == 'v') {
                            $arrForVerification = array();
                            $arrForVerification['fname'] = $app['other_name'];
                            $arrForVerification['mname'] = $app['middle_name'];
                            $arrForVerification['lname'] = $app['surname'];
                            $arrForVerification['dob'] = $app['date_of_birth'];
                        }else if ($application_type == 'vap') {
                            $arrForVerification = array();
                            $arrForVerification['fname'] = $app['first_name'];
                            $arrForVerification['mname'] = $app['middle_name'];
                            $arrForVerification['lname'] = $app['surname'];
                            $arrForVerification['dob'] = $app['date_of_birth'];
                        }

                        if (TblBlockApplicantTable::getInstance()->isApplicantBlocked($arrForVerification)) {
//                  $this->getUser()->setFlash('error',"This applicant is blocked on the protal.");
                            $errorMsgObj = new ErrorMsg();
                            $errorMsg = $errorMsgObj->displayErrorMessage("E046", '001000');
                            $this->getUser()->setFlash('notice', $errorMsg);
                            $this->redirect('cart/list');
                        }
                    }

                    ## this is updated by ashwani kumar bug ID :34864
                    ## $isRefunded = RollbackPaymentDetailsTable::getInstance()->findByAppId($application_id);
                    $isRefunded = RollbackPaymentDetailsTable::getInstance()->isRefunded($application_id, $application_type);
                    if (count($isRefunded) < 1) {
                        $cartObj = new cartManager();

                        $addToCart = $cartObj->addToCart($application_type, $application_id);
                        $arrCartCapacityLimit = $cartValidationObj->checkUserCartCapacity();
                        if (!empty($arrCartCapacityLimit) && count($arrCartCapacityLimit)) {
                            $arrCartCapacity = $arrCartCapacityLimit;
                        }
                        //$arrCartCapacity = $this->getUser()->checkCartCapacityByProcessingCountry();

                        if ($addToCart == "duplicate") {
                            $this->getUser()->setFlash('notice', sprintf('Application already exists in cart.'));
                        }else if($addToCart == "only_vap_allowed"){
                            if($application_type != 'vap'){
                                $msg = $displayApplicationType.' application can not be added into the cart with Visa on Arrival application.';
                            }else{
                                $msg = 'Visa on Arrival application can not be added into the cart with another type of application such as passport, visa, .. etc.';
                            }
                            $this->getUser()->setFlash('notice', sprintf($msg));

                        }else if ($addToCart == "capacity_full") {
                            if ($arrCartCapacity['cart_capacity'] > 1) {
                                $msg = $arrCartCapacity['cart_capacity'] . " applications";
                            } else {
                                $msg = $arrCartCapacity['cart_capacity'] . " application";
                            }
                            $errorMsg = $errorMsgObj->displayErrorMessage("E047", '001000', array("msg" => $msg));
                            $this->getUser()->setFlash('notice', $errorMsg);
                        }else if ($addToCart == "amount_capacity_full") {
                            $confVal = $arrCartCapacity['cart_amount_capacity'];
                            $msg = ($confVal > $cartObj->getCartCurrValue()) ? $confVal : $cartObj->getCartCurrValue();
                            $errorMsg = $errorMsgObj->displayErrorMessage("E048", '001000', array("msg" => $msg));
                            $this->getUser()->setFlash("notice", $errorMsg);
                        }else if ($addToCart == "common_opt_not_available") {
                            $this->getUser()->setFlash("notice", sfConfig::get("app_cart_err_message_add"), true);
                        }else if ($addToCart == "application_edit") {
                            $errorMsg = $errorMsgObj->displayErrorMessage("E058", '001000', array("msg" => ''));
                            $this->getUser()->setFlash("notice", $errorMsg);
                        }else if ($addToCart == "application_rejected") {
                            $errorMsg = $errorMsgObj->displayErrorMessage("E060", '001000', array("msg" => ''));
                            $this->getUser()->setFlash("notice", $errorMsg);
                        }else if ($addToCart == "gratis") {
                            $this->getUser()->setFlash("notice", "Application can not be added into the cart. There is no fees associated with this application.");
                        }else {
                            $arrCartItems = $this->getUser()->getCartItems();
                            $arrProcessingCountry = array();
                            foreach ($arrCartItems as $items) {
                                if ($items->getType() == 'Passport') {
                                    //get Application details
                                    $arrProcessingCountry[] = Doctrine::getTable('PassportApplication')->getProcessingCountry($items->getAppId());
                                } elseif ($items->getType() == 'Freezone') {
                                    $arrProcessingCountryId = Doctrine::getTable('ReEntryVisaApplication')->getProcessingCountry($items->getAppId());
                                    $arrProcessingCountry[] = Doctrine::getTable('VisaProcessingCentre')->getProcessingCentre($arrProcessingCountryId);
                                } elseif ($items->getType() == 'Visa') {
                                    $arrProcessingCountry[] = Doctrine::getTable('VisaApplicantInfo')->getProcessingCountry($items->getAppId());
                                } elseif ($items->getType() == 'Vap') {
                                    $arrProcessingCountry[] = Doctrine::getTable('VapApplication')->getProcessingCountry($items->getAppId());
                                }
                            }
                            //              $this->isUSFlg = false;
                            //              if(in_array('US',$arrProcessingCountry)){
                            //                  $this->isUSFlg = true;
                            //              }
                            $this->getUser()->setFlash('notice', sprintf('Application added to cart successfully'), false); //Bug id : 29742
                        }
                        //                $this->form->clearValues();
                    } else {
                        //            $this->getUser()->setFlash('notice', sprintf('This application can not be added as '.$isRefunded[0]['action'].' is already initiated for this application'));
                        $errorMsg = $errorMsgObj->displayErrorMessage("E049", '001000', array('isRefunded' => $isRefunded[0]['action']));
                        $this->getUser()->setFlash('notice', $errorMsg);
                    }
                }
            }
        }

        $paymentmanager = new PaymentModeManager();
        $arrPaymentOptions = $paymentmanager->getPaymentOptionArray($arrProcessingCountry);
        $this->arrProcessingCountry = $arrProcessingCountry;

        ## Getting processing country...
        if(count($this->arrProcessingCountry) > 0){
            $paymentOptionsObj = new paymentOptions();
            $this->processingCountry = $paymentOptionsObj->getProcessingCountry();
            $currencyId = CurrencyManager::getCurrency($this->processingCountry);
            $isVapExist = Functions::isVapApplicationExistsInCart();
            if($currencyId != 1 && !$isVapExist){
                $this->currencySymbol = CurrencyManager::currencySymbolByCurrencyId($currencyId);
            }else{
                $this->currencySymbol = CurrencyManager::currencySymbolByCurrencyId(1);
            }
        }else{
            $this->processingCountry = '';
            $this->currencySymbol = '';
        }

        ## Resettingn key values...
        sort($arrPaymentOptions);

        ## disable MO based country notice...
        $this->isUSFlg = false;

        $isWt = false;
        $isMo = false;
        $isNmi = false;
        $isOther = false;
        $isJna = false;
        $arrPaymentOptionsCount = count($arrPaymentOptions);
        for ($i = 0; $i < count($arrPaymentOptions); $i++) {
            if ($arrPaymentOptions[$i] == 'Wt') {
                $isWt = true;
            } else if ($arrPaymentOptions[$i] == 'Mo') {
                $isMo = true;
            } else if ($arrPaymentOptions[$i] == 'nmi_mcs' || $arrPaymentOptions[$i] == 'nmi_vbv' || $arrPaymentOptions[$i] == 'nmi_amx' || $arrPaymentOptions[$i] == 'A' || $arrPaymentOptions[$i] == 'M' || $arrPaymentOptions[$i] == 'V') {
                $isNmi = true;
                $isOther = true;
            }
            /**
             * [WP: 111] => CR: 157
             * Added condition of MasterCard SecureCode for JNA service...
             */
            else if ($arrPaymentOptions[$i] == 'jna_nmi_vbv' || $arrPaymentOptions[$i] == 'jna_nmi_mcs') {
                $isJna = true;
            } else {
                $isOther = true;
            }
        }//End of for($i=0;$i<$arrPaymentOptionsCount;$i++){...
        /**
         * Condition(s) changed to show message on cart while application are added in the cart
         * Condition based on MO and Other payment options present in the cart
         */
        $screenMsgType = '';
//      if($isMo && !$isWt && !$isOther){
//        $screenMsgType = 'app_mo_based_country_content';
//        $this->isUSFlg = true;
//      }
//      else if(!$isMo && $isWt && !$isOther){
//        $screenMsgType = 'app_wt_based_country_content';
//        $this->isUSFlg = true;
//      }else
//      if(!$isMo && !$isWt && $isOther){
        if ($isNmi && !$isMo) {
            $screenMsgType = 'app_other_based_country_content';
        }


//      else if($isMo && $isWt && !$isOther){
//        $screenMsgType = 'app_mo_wt_based_country_content';
//        $this->isUSFlg = true;
//      }else
        else if ($isMo && !$isWt && $isOther) {
            $screenMsgType = 'app_mo_other_based_country_content';
        } else if ($isJna) {
            if($arrProcessingCountry[count($arrProcessingCountry)-1] == 'ZA'){
                $screenMsgType = 'app_register_card_content_za';
            }else{
                $screenMsgType = 'app_jna_based_country_content';
            }
        }
//      else if(!$isMo && $isWt && $isOther){
//        $screenMsgType = 'app_wt_other_based_country_content';
//      }else if($isMo && $isWt && $isOther){
//        $screenMsgType = 'app_mo_wt_other_based_country_content';
//      }
        else {
            $screenMsgType = '';
        }
        if (!$this->isPrivilegeUser) {
            $this->screenMsgType = $screenMsgType;
        } else {
            $this->screenMsgType = '';
        }


        //start: kuldeep code for cart limit masseges
        $arrCartCapacityLimit = $cartValidationObj->checkUserCartCapacity();
        if (!empty($arrCartCapacityLimit) && count($arrCartCapacityLimit)) {
            $arrCartCapacity = $arrCartCapacityLimit;
        }

        //echo '<pre>';print_r($arrCartCapacity);die;
        $this->arrCartCapacity = $arrCartCapacity;
        if (!$this->isPrivilegeUser) {
            $cartObj = new cartManager();
            $CapacityFlg = false;
            $amtCapacityFlg = false;

            if (count($arrCartItems) > $this->arrCartCapacity['cart_capacity']) {
                $CapacityFlg = true;
            }

            if (count($arrCartItems) > 1) {
                if ($cartObj->getCartCurrValue() > $this->arrCartCapacity['cart_amount_capacity']) {
                    $amtCapacityFlg = true;
                }
            }
            if ($CapacityFlg && $amtCapacityFlg) {
                $this->getUser()->setFlash('notice', sprintf('Your CART limit has been changed, You can process ' . $this->arrCartCapacity['cart_capacity'] . ' application(s) of amount $' . $this->arrCartCapacity['cart_amount_capacity'] . ' at a time.', false));
            } else if ($CapacityFlg) {
                $this->getUser()->setFlash('notice', sprintf('Your CART limit has been changed, You can process ' . $this->arrCartCapacity['cart_capacity'] . ' application(s) at a time.', false));
            } else if ($amtCapacityFlg) {
                $this->getUser()->setFlash('notice', sprintf('Your CART limit has been changed, You can process amount $' . $this->arrCartCapacity['cart_amount_capacity'] . ' at a time.', false));
            }
        }
        //end code :kuldeep
    }

    public function executeRemove(sfWebRequest $request) {
        $id = $request->getParameter('id');
        $type = $request->getParameter('type');
        $this->getUser()->removeCartItemById($id, $type);
        $this->getUser()->setFlash('notice', "Application Removed", false);
        if($request->getParameter('home'))
        {
          $this->redirect('welcome/index');
        }
        else
        {
        $this->redirect('cart/list');
        }
        //$this->setTemplate('list');
    }

    public function executeOpenInfoBox(sfWebRequest $request) {
        ## Start here... ## Adding by ashwani...
        $arrSameCardLimit = array();
        $cartValidationObj = new CartValidation();
        $arrCartCapacityLimit = $cartValidationObj->checkUserCartCapacity();
        if (!empty($arrCartCapacityLimit) && count($arrCartCapacityLimit)) {
            $arrSameCardLimit = $arrCartCapacityLimit;
        } ## End here...
        $this->hour = Settings::maxcard_time($arrSameCardLimit['transaction_period']);
        $this->setTemplate('popupInfo');
        $this->setLayout('popupLayout');
    }
}
?>