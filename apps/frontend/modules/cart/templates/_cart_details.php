<?php
//inlcude_once('functions.php');
  $items = $sf_user->getCartItems();
  $colspan = 7;
  $itemCount = count($items);
  //echo $currencySymbol = ($currencySymbol == '&amp;pound')?'&amp;pound;':$currencySymbol;
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="background-color:#f0f6ff;" class="">
    <tr>
        <td valign="top" class="no_border">
            <table width="100%" cellspacing="0" cellpadding="0" class="dsbrd_listing">
                <tr style="background-color:#f0f6ff;">
                    <td width="50" class="border_right"><span class="txtBold">S. No.</span></td>
                    <td width="100" class="border_right"><span class="txtBold">Application Type</span></td>
                    <td class="border_right" width="100"><span class="txtBold">Applicant Name</span></td>
                    <td width="100" class="border_right"><span class="txtBold">Application Id</span></td>
                    <td width="190" class="border_right"><span class="txtBold">Reference Number</span></td>
                    <td width="60" align="right" class="border_right"><span class="txtBold">Price(<?php echo html_entity_decode($currencySymbol); ?>)</span></td>
                    <td width="100" align="center"><span class="txtBold">Actions</span></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td valign="top" style="margin:0px; padding-top:0px; padding-bottom:0px;">
            <?php
            $browserName = $_SERVER['HTTP_USER_AGENT'];
            if(stripos($browserName, 'msie') === false){
                $lastTdWidth = ($itemCount > 4)?85:100;
            }else{
                $lastTdWidth = 100;
            }
            ?>
            <div style="overflow-x:hidden;overflow-y:auto;height:100px; margin:0px;">
                <table width="100%" cellspacing="0" cellpadding="0" class="dsbrd_listing" >
                    <?php
                    $i = 1;
                    $total_amount = 0;
                    $visa_additional_charges_on = sfConfig::get('app_visa_additional_charges_flag');
                    foreach ($items as $item) {
                        $visa_additional_charges_flag = false;
                        $additional_charges = 0;
                        $encryptAapplicationId = SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT($item->getAppId()));
                        if($item->getType()=='Visa')
                        {
                            if($visa_additional_charges_on){
                                ## Additional charges in dollar for visa...
                                $additional_charges = sfConfig::get('app_visa_additional_charges');
                                $visa_additional_charges_flag = true;
                            }
                            
                            $referenceNumber = Doctrine::getTable('VisaApplication')->findBy('id', $item->getAppId());
                            $appType = 'Visa';
                            /**
                             * [WP: 096]
                             */
                            ##$printUrl = url_for("visa/printVisaApp?id=".SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT($item->getAppId())));
                            $printUrl = Functions::printApplicationURL('visa', $encryptAapplicationId);
                        }
                        if($item->getType()=='Freezone')
                        {
                            $referenceNumber = Doctrine::getTable('VisaApplication')->findBy('id', $item->getAppId());
                            if($referenceNumber[0]['visacategory_id'] == 102){
                                $appType = 'Rentry Freezone';
                                /**
                                 * [WP: 096]
                                 */
                                ##$printUrl = url_for("visa/printFreezone?id=".SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT($item->getAppId())));
                                $printUrl = Functions::printApplicationURL('reentryfreezone', $encryptAapplicationId);
                            }else{
                                $appType = 'Entry Freezone';
                                /**
                                 * [WP: 096]
                                 */
                                ##$printUrl = url_for("visa/printVisaApp?id=".SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT($item->getAppId())));
                                $printUrl = Functions::printApplicationURL('entryfreezone', $encryptAapplicationId);
                                if($visa_additional_charges_on){
                                    ## Additional charges in dollar for visa...
                                    $additional_charges = sfConfig::get('app_visa_additional_charges');
                                    $visa_additional_charges_flag = true;
                                }
                            }
                        }
                        if($item->getType()=='Passport')
                        {
                            $referenceNumber = Doctrine::getTable('PassportApplication')->findBy('id', $item->getAppId());
                            $appType = 'Passport';
                            /**
                             * [WP: 096]
                             */
                            ##$printUrl = url_for("passport/showPrintSuccess?id=".SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT($item->getAppId())));
                            $printUrl = Functions::printApplicationURL('passport', $encryptAapplicationId);
                        }
                        if($item->getType()=='Vap')
                        {
                            ## Additional charges in dollar for vap...
                            $additional_charges = sfConfig::get('app_visa_arrival_additional_charges');
                        
                            $referenceNumber = Doctrine::getTable('VapApplication')->findBy('id', $item->getAppId());
                            $appType = sfConfig::get('app_visa_arrival_title');
                            $printUrl = url_for("visaArrival/printVisaApp?id=".SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT($item->getAppId())));
                        }
                        $referenceNumber=$referenceNumber[0]['ref_no'];
                        $amount = Functions::getApplicationFees($item->getAppId(),$item->getType());
                        
                        if($processingCountry == 'GB'){
                            // currency id 1 mean dolloar and 5 means pound..
                            $fromCurrency = 1;
                            $toCurrency = 5;

                            ## Converted Additional charges for vap...
                            $additional_charges = CurrencyManager::getConvertedAmount($additional_charges,$fromCurrency,$toCurrency);
                            if($item->getType()=='Vap'){
                                $appAmount = Functions::getVapApplicationFeesForMO($item->getAppId(),$item->getType());
                                $appAmount = CurrencyManager::getConvertedAmount($appAmount, $fromCurrency, $toCurrency);                                
                                $amount = $appAmount + $additional_charges ;
                            }else if($visa_additional_charges_flag){
                                $appAmount = Functions::getVisaApplicationFeesWithoutAdditionalCharges($item->getAppId(),$item->getType());
                                $appAmount = CurrencyManager::getConvertedAmount($appAmount, $fromCurrency, $toCurrency);
                                $amount = $appAmount + $additional_charges ;
                            }else{
                                $amount = CurrencyManager::getConvertedAmount($amount,$fromCurrency,$toCurrency);                            
                            }                            
                        }

                        ?>
                    <tr>
                        <td width="45" valign="top" align="left" class="border-right"><?php echo $i; ?></td>
                        <td width="100" valign="top" align="left" class="border-right"><?php echo $appType; ?></td>
                        <td valign="top" align="left" class="border-right" width="100"><?php echo $item->getName();?></td>
                        <td width="100" valign="top" align="left" class="border-right"><?php echo $item->getAppId();?></td>
                        <td width="90" valign="top" align="left" class="border-right"><?php echo $referenceNumber;?></td>
                        <td width="160" valign="top" align="right" class="border-right"><?php echo $amount; ?>
                            <?php if($item->getType() == 'Vap' || $visa_additional_charges_flag){
                                //$additional_charges = ($processingCountry == 'GB')?sfConfig::get('app_visa_arrival_additional_pound_charges'):sfConfig::get('app_visa_arrival_additional_charges');
                                //echo '<i><p style="font-size:10px;text-align:right">(Inclusive service charge: '.html_entity_decode($currencySymbol).$additional_charges.')</p></i>';
                                echo Functions::getAdditionalText($additional_charges, $currencySymbol);
                            }?>

                            </td>
                        <td width="100" class="border-right" align="center"><span onclick='openPopUp("<?php echo $printUrl?>");'><img class="hand" src="<?php echo image_path('print_icon.png'); ?>" alt="Print" title="Print" /></span></td>
                    </tr>
                    <?php
                    $total_amount = $total_amount+$amount;
                    $i++;


                } ?>

                <?php if($total_amount > 0) { ?>
                    <tr>
                        <td colspan="5" align="right" class="tmz-strong" style="border-top:1px solid #CCCCCC;">Total:</td>
                        <td  align="right" style="border:1px solid #CCCCCC;"><strong><?php echo format_amount($total_amount,1,0,$currencySymbol);?></strong></td>
                        <td style="border-top:1px solid #CCCCCC;" >&nbsp;</td>
                    </tr>
                    <?php }else{ ?>
                    <tr>
                        <td colspan="7" class="tmz-strong red" style="border-top:1px solid #CCCCCC;text-align:center" height="50">No pending application in cart.</td>
                    </tr>
                    <?php } ?>
                </table>
            </div>
        </td>
    </tr>
</table>
<script>
    function openPopUp(url){
      url = url + '/mode/cart'
      window.open (url, "mywindow","status=1,toolbar=0,width=1000px,height=800px,location=0,menubar=0,scrollbars=1");
    }
</script>