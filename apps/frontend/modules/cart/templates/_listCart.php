 <?php
use_javascript('dhtmlwindow.js');
use_stylesheet('dhtmlwindow.css');
use_javascript('modal.js');
use_stylesheet('modal.css');
?>

<form action="<?php echo url_for('passport/applicationPayment');?>" method="POST"  name="cartform"  class="dlForm multiForm">
<a name="msg"></a>
<?php if ($sf_user->hasFlash('notice')): ?>
  <div id="flash_notice" class="alertBox" > <?php echo nl2br($sf_user->getFlash('notice'));?></div>
<?php  endif  ?>
<br>
<table width="100%">
  <tr>
  <td width="5%"><span class="txtBold">S. No.</span></td>
  <td width="15%"><span class="txtBold">Type</span></td>
  <td width="25%"><span class="txtBold">Name</span></td>
  <td width="11%"><span class="txtBold">Application Id</span></td>
  <td width="11%"><span class="txtBold">Reference Number</span></td>
   <td width="15%" class="txtRight txtBold">Price(<?php echo $currencySymbol; ?>)</td>
  <td width="7%"><span class="txtBold">Actions</span></td>
  </tr>
<?php
$items = $sf_user->getCartItems();
if (!count($items)) {
  // emptry set, no point in continuing
     echo '<tr><td  colspan="7" align="center" ><div class="red"> No record found</div></td></tr></table>';

}
else {
$i = 1;
$total_amount = 0;
$visa_additional_charges_flag = sfConfig::get('app_visa_additional_charges_flag');
foreach ($items as $item) {
  $visa_add_charges_flag = false;


  $encryptAapplicationId = SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT($item->getAppId()));
  if($item->getType()=='Visa')
  {
      ## Additional charges in dollar for visa...
      if($visa_additional_charges_flag){
          $additional_charges = sfConfig::get('app_visa_additional_charges');
          $visa_add_charges_flag = true;
      }

      $referenceNumber = Doctrine::getTable('VisaApplication')->findBy('id', $item->getAppId());
      $appType = 'Visa';
      
      /**
       * [WP: 096]
       */
      ## $printUrl = url_for("visa/printVisaApp?id=".SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT($item->getAppId())));            
      $printUrl = Functions::printApplicationURL('visa', $encryptAapplicationId);
  }
  if($item->getType()=='Freezone')
  {
      $referenceNumber = Doctrine::getTable('VisaApplication')->findBy('id', $item->getAppId());
      if($referenceNumber[0]['visacategory_id'] == 102){
        $appType = 'Rentry Freezone';
        /**
         * [WP: 096]
         */
        ##$printUrl = url_for("visa/printFreezone?id=".SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT($item->getAppId())));
        $printUrl = Functions::printApplicationURL('reentryfreezone', $encryptAapplicationId);
      }else{
        $appType = 'Entry Freezone';
        /**
         * [WP: 096]
         */
        ##$printUrl = url_for("visa/printVisaApp?id=".SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT($item->getAppId())));
        $printUrl = Functions::printApplicationURL('entryfreezone', $encryptAapplicationId);

        ## Additional charges in dollar for visa...
        if($visa_additional_charges_flag){
            $additional_charges = sfConfig::get('app_visa_additional_charges');
            $visa_add_charges_flag = true;
        }
      }
  }
  if($item->getType()=='Passport')
  {
      $referenceNumber = Doctrine::getTable('PassportApplication')->findBy('id', $item->getAppId());
      $appType = 'Passport';
      /**
       * [WP: 096]
       */
      ##$printUrl = url_for("passport/showPrintSuccess?id=".SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT($item->getAppId())));
      $printUrl = Functions::printApplicationURL('passport', $encryptAapplicationId);
  }
  if($item->getType()=='Vap')
  {
      $referenceNumber = Doctrine::getTable('VapApplication')->findBy('id', $item->getAppId());
      $appType = sfConfig::get('app_visa_arrival_title');
      $printUrl = url_for("visaArrival/printVisaApp?id=".SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT($item->getAppId())));
  }
  $referenceNumber=$referenceNumber[0]['ref_no'];
  $amount = $item->getPrice();

  /**
   * [WP: 102] => CR: 144
   * Fetching visa convertd amount if additional charges flag true...
   */
    if($visa_add_charges_flag){
        $applicationFees = Functions::getConvertedVisaAndAdditionalFees($item->getAppId(), $item->getType(), $additional_charges);
        if(count($applicationFees) > 0){
            $additional_charges = $applicationFees['additional_charges'];
            $amount = $applicationFees['appAmount'] + $additional_charges ;
        }
    }//End of if($visa_add_charges_flag){...
  
  echo '<tr>';
  echo '<td valign="top">'.$i.'</td>';
  echo '<td valign="top">'.$appType.'</td>';
  echo '<td valign="top">'.$item->getName().'</td>';
  echo '<td valign="top">'.$item->getAppId().'</td>';//format_amount($amount, $currency="",$convert_to_cents=0)
  echo '<td valign="top">'.$referenceNumber.'</td>';
  echo '<td align="right" valign="top">'.format_amount($amount);
  if($item->getType() == 'Vap'){       
      echo '<i><p style="font-size:10px;text-align:right">(Inclusive service charge: $'.sfConfig::get('app_visa_arrival_additional_charges').')</p></i>';
  }

  if($visa_add_charges_flag){
      //echo '<i><p style="font-size:10px;text-align:right;">(Inclusive service charge: '.html_entity_decode($currencySymbol).$additional_charges.')</p></i>';
      echo Functions::getAdditionalText($additional_charges, $currencySymbol);
  }

  echo '</td>';
  echo '<td valign="top"><a href="javascript:void(0)" onclick = \'removeFromCart("'.url_for('cart/remove?id='.$item->getAppId().'&type='.$item->getType()).'")\'><img src="'.image_path('remove_icon.png').'" alt="Remove" title="Remove" /></a>
      <span onclick=\'openPopUp("'.$printUrl.'");\' style="text-decoration:underline;color:green;cursor:pointer;"> <img src="'.image_path('print_icon.png').'" alt="Print" title="Print" /></span></td>';
  echo '</tr>';
  $total_amount = $total_amount+$amount;
  ?>
    <input type ="hidden" value="<?php echo $item->getAppData();?>" name="appDetails[<?php echo $i-1; ?>]" />
  <?php
  $i++;
}
echo '<tr> <td colspan="5" height="25px" align="right"><span class="txtBold">Total:</span></td><td align="right">'.format_amount($total_amount,1,0,$currencySymbol).'</td><td>&nbsp;</td></tr>';
?>
</table>
  <table align="center" class="nobrd" style="margin:auto;">
    <tr>
        <?php
        $cartCapacity = $arrCartCapacity['cart_capacity'];
        $popupDisplay = sfConfig::get('app_popup_display');
        
        $cartAmountCapacity = $arrCartCapacity['cart_amount_capacity'];

        
        if(($total_amount < $cartAmountCapacity) && (count($items) < $cartCapacity) && !$isPrivilegeUser){
        ?>
      <td align="right"><input type="button" name="add" class='normalbutton' value="Start Another Application" onclick="window.location.href='<?php echo url_for('nis/newApplication'); ?>'" >
      </td>
        <?php }else if($isPrivilegeUser){?>
      <td align="right"><input type="button" name="add" class='normalbutton' value="Start Another Application" onclick="window.location.href='<?php echo url_for('nis/newApplication'); ?>'" >
      </td>
        <?php }?>
      <td align="left"><?php
      //if popup off for all - vonfigured from app.yml
      if($popupDisplay != 1)
      {
          echo '<input type="button" class="normalbutton"  id="multiFormSubmit" onclick="document.cartform.submit();"  value="Proceed To Online Payment">';
      }
      else
      {
          //if pop up off for money order (US)
          if($isUSFlg == '1')
          {
            echo formatButton('<input type="button" class="normalbutton"  id="multiFormSubmit" onclick="document.cartform.submit();"  value="Proceed To Online Payment">');
          }
          else
          {
              //popup on for all -- only conditional off
              if(($total_amount >= $cartAmountCapacity) || (count($items) >= $cartCapacity)){
                  echo formatButton('<input type="button" class="normalbutton"  id="multiFormSubmit" onclick="document.cartform.submit();"  value="Proceed To Online Payment">');
              }else{
                  echo formatButton('<input type="button" class="normalbutton"  id="multiFormSubmit" onclick="confirmSubmission();"  value="Proceed To Online Payment">');
              }
          }
      }
       //echo formatButton('<input type="button" class="button"  id="multiFormSubmit" onclick="confirmSubmission();"  value="Proceed To Online Payment">');
      ?>
        <!-- <input type ="hidden" value="<?php //echo $appDetails;?>" name="appDetails" id="appDetails"/> -->
            </td>
	  </tr>
  </table>
  <?php } ?>
</form>
<?php //echo $uid = $sf_user->getGuardUser()->getId(); ?>
    <script>    
   
     function confirmSubmission(){
        emailwindow=dhtmlmodal.open('EmailBox', 'iframe', 'openInfoBox', 'Confirm Submission', 'width=620px,height=250px,center=1,border=0, scrolling=no')

        emailwindow.onclose=function(){
            var theform=this.contentDoc.forms[0];
            var theemail=this.contentDoc.getElementById("emailfield")
            if (theemail.value.indexOf("@")==-1){
                alert("Please enter a valid email address")
                return false
            }
            else{
                document.getElementById("youremail").innerHTML=theemail.value //Assign the email to a span on the page
                return true ;
            }
        }
    }

    function openPopUp(url){
      url = url + '/mode/cart'
      window.open (url, "mywindow","status=1,toolbar=0,width=1000px,height=800px,location=0,menubar=0,scrollbars=1");
     // alert(url);
    }

    function removeFromCart(url){ 
        window.location = url;
    }
 </script>