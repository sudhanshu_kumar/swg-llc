
<div class="comBox" width="50%">
  <div id="div1" align="center">
    <br><br>
    <h2>Will you be submitting more applications within the next <?php echo str_replace('.', '', $hour)?> ?</h2>

  </div>

  <div id="div2"  style="display:none">
    <h2>Due to regulations, we do not support multiple payments (repetitive payments) with same card within <?php echo str_replace('.', '', $hour)?> (one use of same card is permitted every <?php echo str_replace('.', '', $hour);?>). <br/><br/>
      You are hereby advised to START A NEW APPLICATION which will be added to your CART on submission.<br/><br/>
    You should proceed to payment only after adding all applications you intend to process in the next 2 hours into your CART.</h2>

    <table align="center" class="nobrd" style="margin:auto;"><tr>
        <td align="center"> <div class="lblButton">
            <input type="button" name="add" class='button' value="NO, Please start New Application" onclick="newapplication();" >
          </div>
          <div class="lblButtonRight">
            <div class="btnRtCorner"></div>
          </div>
        </td>
      </tr>

    </table>
  </div>

  <div id="div3"  style="display:none">
    <h2> Due to regulations, we do not support multiple payments (repetitive payments) with same card within <?php echo str_replace('.', '', $hour)?> (one use of same card is permitted every <?php echo str_replace('.', '', $hour)?>). <br/><br/>
      Card used to make payment cannot be used again for the next <?php echo str_replace('.', '', $hour)?> after this payment.<br/><br/>
    Are you sure you want to proceed to payment now?</h2>
    <table align="center" class="nobrd"><tr>
        <td align="center"> <div class="lblButton">
            <input type="button" name="add" class='button' value="NO, Please start New Application" onclick="newapplication();" >
          </div>
          <div class="lblButtonRight">
            <div class="btnRtCorner"></div>
        </div></td>
        <td align="center">
          <div class="lblButton">
            <input style="*text-align:center;" type="button" class="button"  id="multiFormSubmit" onclick="parent.document.cartform.submit(); window.close();"  value="YES, Please start payment process">
          </div>
          <div class="lblButtonRight">
            <div class="btnRtCorner"></div>
          </div>
        </td>
      </tr>

    </table>
  </div>
  <div  align="center" id="div4">
    <input type="button" name="yes" class="paymentbutton" value="Yes" onclick="showDiv('div2');">
    <input type="button" name="no" class="paymentbutton" value="No" onclick="showDiv('div3');">
  </div>
</div>
<script>
  $(document).ready(function()
  {
    $('.global_header').removeClass();
  });
  function showDiv(div) {
    document.getElementById(div).style.display = "block";
    document.getElementById('div4').style.display = "none";
    document.getElementById('div1').style.display = "none";
  }
  function newapplication(){
    parent.window.location.href='<?php echo url_for('nis/newApplication'); ?>';
    window.close();
  }
</script>