<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">
<?php
include_partial('global/innerHeading',array('heading'=>"Cart Items")); ?>
    <div class="clear"></div>
    <div class="tmz-spacer"></div>
  <div id="nxtPage">
  <?php if($screenMsgType != '') { ?>
      <div id="flash_notice" class="highlight yellow"> <?php echo sfConfig::get($screenMsgType);?> </div>
      <?php } ?>


  <?php
if(count($arrProcessingCountry) > 0){
    $arrProcessingCountry = $arrProcessingCountry[count($arrProcessingCountry)-1];
}else{
    $arrProcessingCountry = '';
}
$messageCountryArray = array('NZ');
if(in_array($arrProcessingCountry, $messageCountryArray)){ ?>
  <div id="flash_notice" class="highlight red" >
    <style>
        .highlight.red {
            background-color:RED;
            border-color:RED;
            color:#FFF;
            }

        .highlight {
            border:1px dotted;
            margin:10px 0;
            padding:5px;
            position:relative;
        }
    </style>
    <strong><i>Attention:</i> We are experiencing technical problems with our payments processor. Due to this AMEX AND MASTER cards are not allowed with our temporary payment processor. You are required to payment through VISA card only. </strong>
</div>
<?php } ?>
  <?php if(!$isUSFlg){
      if(!$isPrivilegeUser){?>
      <div align="center">
        <table width="100%" cellpadding="0" cellspacing="0" border="0">
              <tr>
            <td width="35%"><b>Cart Total Application Capacity:</b> </td>
            <td><b> <?php echo $arrCartCapacity['cart_capacity'];?> </b> </td>
              </tr>
              <tr>
            <td><b>Cart Total Amount Capacity:</b> </td>
            <td><b><?php echo $arrCartCapacity['cart_amount_capacity'];?> </b> </td>
              </tr>
          </table>
      </div>
      <?php }}?>
<?php
include_partial('searchApplication', array('form' => $form));?>
<?php
include_partial('listCart',array('isUSFlg'=>$isUSFlg,'isPrivilegeUser'=>$isPrivilegeUser,'arrCartCapacity'=>$arrCartCapacity,'arrProcessingCountry'=>$arrProcessingCountry, 'currencySymbol' => $currencySymbol));
?>
</div>
  </div>
</div>
<div class="content_wrapper_bottom"></div>