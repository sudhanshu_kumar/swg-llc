<?php

/**
 *  Web Service actions.
 *
 * @package    mysfp
 * @subpackage  order
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12479 2008-10-31 10:54:40Z fabien $
 */
class servicesActions extends sfActions {
    /**
     *
     * @param <type> $request
     * @return <type> [WP: 098] => CR:140
     * Calling web service...
     */
    public function executeApplicationDetailsInfo(sfWebRequest $request){

        if($request->isMethod('POST')){

            $service_url = settings::getHTTPpath().'/services/app/q/appSearch';
            $curl = curl_init($service_url);
            $curl_post_data = array(
                "username" => 'demo',
                "password" => 'demo',
                "applicationId" => $request->getParameter('applicationId'),
                "referenceNumber" => $request->getParameter('referenceNumber'),
            );
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
            $curl_response = curl_exec($curl);
            curl_close($curl);
            echo "<pre>";print_r($curl_response);echo "</pre>";exit;
        }

    }

    /**
     *
     * @param <type> $request
     * @return <type> [WP: 098] => CR:140
     * Call web serives for result...
     */
    public function executeCallWebSerivecs(sfWebRequest $request){
        $q = $request->getParameter('q');
        $applicationId = $request->getParameter('applicationId');
        $referenceNumber = $request->getParameter('referenceNumber');

        $username = $request->getParameter('username');
        $password = $request->getParameter('password'); 

        if($username == sfConfig::get('app_webService_query_username') && $password == sfConfig::get('app_webService_query_password')){
            if($q != ''){
                switch(strtolower($q)){
                    case 'appsearch':

                        ## Adding below 2 lines for blocking api...
                        ## Remove below 2 lines to run api...
                        $xml = $this->generateXml('methodnotallowed');
                        return $this->renderText($xml);
                        
                        $request->setParameter('applicationId', $applicationId);
                        $request->setParameter('referenceNumber', $referenceNumber);
                        $this->forward('services', 'fetchApplicationData');
                        exit;
                        break;
                    case 'apppaid':

                        ## Adding below 2 lines for blocking api...
                        ## Remove below 2 lines to run api...
                        $xml = $this->generateXml('methodnotallowed');
                        return $this->renderText($xml);

                        
                        $request->setParameter('applicationId', $applicationId);
                        $request->setParameter('referenceNumber', $referenceNumber);
                        $request->setParameter('transactionNumber', $request->getParameter('transactionNumber'));
                        $request->setParameter('appAmount', $request->getParameter('appAmount'));

                        /*
                         * Creating request log...
                         */
                        $requestString = 'username='.$username.'&password='.$password.'&applicationId='.$applicationId.'&referenceNumber='.$referenceNumber.'&transactionNumber='.$request->getParameter('transactionNumber').'&appAmount='.$request->getParameter('appAmount');
                        Functions::createLogData($requestString,'OIS_Request-'.$applicationId, 'OIS');

                        $this->forward('services', 'markApplicationPaid');
                        exit;
                        break;
                    default:
                        $xml = $this->generateXml('methodnotallowed');
                        return $this->renderText($xml);
                        break;
                }
            }else{
                $xml = $this->generateXml('notacceptable');
                return $this->renderText($xml);
            }
        }else{
            $xml = $this->generateXml('unauthorized');
            return $this->renderText($xml);
        }        
    }
    /**
     *
     * @param <type> $request
     * @return <type> [WP: 098] => CR:140
     * Fetching application details...
     */
    public function executeFetchApplicationData(sfWebRequest $request){

        $applicationId = (int)$request->getParameter('applicationId');
        $referenceNumber = (int)$request->getParameter('referenceNumber');

        if($applicationId !='' && $referenceNumber != ''){

           $visaData = Doctrine::getTable('VisaApplication')->find($applicationId);
           if(!empty($visaData)){

               ## If visacategoryId is 29 or 101 i.e., Visa and reference number in table matches with reference number provided by user then true...
               ## 29 means Entry Visa and 101 means Entry Freezone
               if(($visaData->getVisacategoryId() == 29 || $visaData->getVisacategoryId() == 101) && $visaData->getRefNo() == $referenceNumber){
                    $appData = Doctrine::getTable('VisaApplication')->getVisaFreshRecordSummery($applicationId);
                    if(count($appData) > 0){                       
                        
                        ## If processing country is United Kingdom i.e., GB then only code execute...
                        if($appData[0]['VisaApplicantInfo']['applying_country_id'] == 'GB'){

                            $xml = $this->generateApplicationXml($appData);
                        }else{
                            $xml = $this->generateXml('countryspecific');
                        }                        

                    }else{
                        $xml = $this->generateXml('invalid');
                    }
               }else{
                   $xml = $this->generateXml('invalid');
               }
                
            }else{
                $xml = $this->generateXml('invalid');
            }
           }else{
               $xml = $this->generateXml('invalid');
           }



            
            
        
        return $this->renderText($xml);
        exit;

    }
    /**
     *
     * @param <type> $request
     * @return <type> [WP: 098] => CR:140
     * Generate error xml...
     */

    private function generateXml($errorType){
        $xml = '<?xml version="1.0"?>';
        switch(strtolower($errorType)){
            case 'invalid':
                $xml .= '<message>Invalid Application Data</message>';
                break;
            case 'countryspecific':
                $xml .= '<message>Application processing country does not match</message>';
                break;
            case 'methodnotallowed':
                $xml .= '<message>Method not allowed</message>';
                break;
            case 'notacceptable':
                $xml .= '<message>Not Acceptable</message>';
                break;
            case 'unauthorized':
                $xml .= '<message>Unauthorized Request</message>';
                break;
            default:
                $xml .= '<message>Not Supported</message>';
                break;
        }

        return $xml;
        
    }
    /**
     *
     * @param <type> $request
     * @return <type> [WP: 098] => CR:140
     * Generate application xml...
     */
    private function generateApplicationXml($visa_application){
          $applicantPreviousNigeriaHistory = Doctrine::getTable('VisaApplicantPreviousHistory')->getPreviousHistory($visa_application[0]['id']);
          $applicantTravelHistory = Doctrine::getTable('VisaApplicantTravelHistory')->getTravelHistory($visa_application[0]['id']);
          
          $visaObj = Doctrine::getTable('VisaApplication')->find($visa_application[0]['id']);          
          $applicationFees = $visaObj->getDollarFee();
          
          //header('Content-type: text/plain');
          $doc = new DomDocument('1.0');
          $doc->formatOutput = true;

          $root = $doc->appendChild($doc->createElement('Application-Details'));
          $visaDetail=$root->appendChild($doc->createElement("VisaDetails"));
          $visaDetail->appendChild($doc->createElement("AppId",$visa_application[0]['id']));
          //$visaDetail->appendChild($doc->createElement("VisaCategory",$visa_application[0]['visacategory_id']));
          //$visaDetail->appendChild($doc->createElement("Zone",$visa_application[0]['zone_type_id']));
          $visaDetail->appendChild($doc->createElement("ReferenceNo",$visa_application[0]['ref_no']));
          

          $visaDetail->appendChild($doc->createElement("Title",$visa_application[0]['title']));
          $visaDetail->appendChild($doc->createElement("Surname",$visa_application[0]['surname']));
          $visaDetail->appendChild($doc->createElement("MiddleName",$visa_application[0]['middle_name']));
          $visaDetail->appendChild($doc->createElement("OtherName",$visa_application[0]['other_name']));
          $visaDetail->appendChild($doc->createElement("Gender",$visa_application[0]['gender']));
          $visaDetail->appendChild($doc->createElement("MaritalStatus",$visa_application[0]['marital_status']));
          $visaDetail->appendChild($doc->createElement("Email",$visa_application[0]['email']));
          $visaDetail->appendChild($doc->createElement("DateofBirth",$visa_application[0]['date_of_birth']));
          $visaDetail->appendChild($doc->createElement("PlaceofBirth",$visa_application[0]['place_of_birth']));
          $visaDetail->appendChild($doc->createElement("HairColor",$visa_application[0]['hair_color']));
          $visaDetail->appendChild($doc->createElement("EyesColor",$visa_application[0]['eyes_color']));
          $visaDetail->appendChild($doc->createElement("IdMarks",$visa_application[0]['id_marks']));
          $visaDetail->appendChild($doc->createElement("Height",$visa_application[0]['height']));
          $visaDetail->appendChild($doc->createElement("PresentNationality",$visa_application[0]['CurrentCountry']['country_name']));
          $visaDetail->appendChild($doc->createElement("PreviousNationality",$visa_application[0]['previous_country']));
          $visaDetail->appendChild($doc->createElement("ApplicationCreationDate",$visa_application[0]['created_at']));
          $visaDetail->appendChild($doc->createElement("ApplicationUpdationDate",$visa_application[0]['updated_at']));
          $permanentAddress = $visaDetail->appendChild($doc->createElement("PermanentAddress"));
          $address1=$visa_application[0]['VisaPermanentAddress']['address_1'];
          $permanentAddress->appendChild($doc->createElement("Address1",$address1));
          $address2=$visa_application[0]['VisaPermanentAddress']['address_2'];
          $permanentAddress->appendChild($doc->createElement("Address2",$address2));
          $permanentAddress->appendChild($doc->createElement("City",$visa_application[0]['VisaPermanentAddress']['city']));
          $permanentAddress->appendChild($doc->createElement("State",$visa_application[0]['VisaPermanentAddress']['state']));
          $permanentAddress->appendChild($doc->createElement("PostCode",$visa_application[0]['VisaPermanentAddress']['postcode']));
          //$permanentAddress->appendChild($doc->createElement("VarType",$visa_application[0]['VisaPermanentAddress']['var_type']));
          //$country = $permanentAddress->appendChild($doc->createElement("Country"));
          $permanentAddress->appendChild($doc->createElement("CountryName",$visa_application[0]['VisaPermanentAddress']['Country']['country_name']));
          //$country->appendChild($doc->createElement("IsEcowas",$visa_application[0]['VisaPermanentAddress']['Country']['is_ecowas']));
          //$country->appendChild($doc->createElement("IsVisaApplicabele",$visa_application[0]['VisaPermanentAddress']['Country']['is_visa_fee_applicable']));
          //$country->appendChild($doc->createElement("IsAfrican",$visa_application[0]['VisaPermanentAddress']['Country']['is_african']));
          //$country->appendChild($doc->createElement("Currency",$visa_application[0]['VisaPermanentAddress']['Country']['currency_id']));
          $visaDetail->appendChild($doc->createElement("PermanentPhoneNo",$visa_application[0]['perm_phone_no']));
          $visaDetail->appendChild($doc->createElement("Profession",$visa_application[0]['profession']));
          $officeAddress = $visaDetail->appendChild($doc->createElement("OfficeAddress"));
          $officeAddress->appendChild($doc->createElement("Address1",$visa_application[0]['VisaOfficeAddress']['address_1']));
          $officeAddress->appendChild($doc->createElement("Address2",$visa_application[0]['VisaOfficeAddress']['address_2']));
          $officeAddress->appendChild($doc->createElement("Country",$visa_application[0]['VisaOfficeAddress']['Country']['country_name']));
          $officeAddress->appendChild($doc->createElement("City",$visa_application[0]['VisaOfficeAddress']['city']));
          $officeAddress->appendChild($doc->createElement("State",$visa_application[0]['VisaOfficeAddress']['state']));
          $officeAddress->appendChild($doc->createElement("PostCode",$visa_application[0]['VisaOfficeAddress']['postcode']));
          $visaDetail->appendChild($doc->createElement("OfficePhoneNo",$visa_application[0]['office_phone_no']));
          $visaDetail->appendChild($doc->createElement("MilitaryIn",$visa_application[0]['milltary_in']));
          $visaDetail->appendChild($doc->createElement("From",$visa_application[0]['military_dt_from']));
          $visaDetail->appendChild($doc->createElement("To",$visa_application[0]['military_dt_to']));

          
          //$visaDetail->appendChild($doc->createElement("PaymentTransId",$visa_application[0]['payment_trans_id']));
          //$visaDetail->appendChild($doc->createElement("CreatedAt",$visa_application[0]['created_at']));
          //$visaDetail->appendChild($doc->createElement("UpdatedAt",$visa_application[0]['updated_at']));
          $visaMoreInfo=$visaDetail->appendChild($doc->createElement("VisaMoreInfo"));
          $visaMoreInfo->appendChild($doc->createElement("IssusingGovt",$visa_application[0]['VisaApplicantInfo']['issusing_govt']));
          $visaMoreInfo->appendChild($doc->createElement("PassportNumber",$visa_application[0]['VisaApplicantInfo']['passport_number']));
          $visaMoreInfo->appendChild($doc->createElement("DateofIssue",$visa_application[0]['VisaApplicantInfo']['date_of_issue']));
          $visaMoreInfo->appendChild($doc->createElement("DateofExp",$visa_application[0]['VisaApplicantInfo']['date_of_exp']));
          $visaMoreInfo->appendChild($doc->createElement("PlaceofIssue",$visa_application[0]['VisaApplicantInfo']['place_of_issue']));
          //$visaMoreInfo->appendChild($doc->createElement("IssusingGovt",$visa_application[0]['VisaApplicantInfo']['issusing_govt']));
          //$visaType=$visaMoreInfo->appendChild($doc->createElement("VisaType"));
          $visaMoreInfo->appendChild($doc->createElement("VisaType",$visa_application[0]['VisaApplicantInfo']['VisaTypeId']['var_value']));
          //$applyingCountry=$visaMoreInfo->appendChild($doc->createElement("ApplyingCountry"));
          $visaMoreInfo->appendChild($doc->createElement("ApplyingCountry",$visa_application[0]['applying_country']));
          if($visa_application[0]['VisaApplicantInfo']['authority_id'] != ''){
              $visaMoreInfo->appendChild($doc->createElement("Authority",$visa_application[0]['VisaApplicantInfo']['VisaProcessingCentre']['centre_name']));
          }
          //$EmbassyDetail=$visaMoreInfo->appendChild($doc->createElement("Embassy"));
          //$embassyName=$visa_application[0]['VisaApplicantInfo']['EmbassyMaster']['embassy_name'];
          $visaMoreInfo->appendChild($doc->createElement("EmbassyName",$visa_application[0]['VisaApplicantInfo']['EmbassyMaster']['embassy_name']));
          $visaMoreInfo->appendChild($doc->createElement("PurposeofJourney",$visa_application[0]['VisaApplicantInfo']['purpose_of_journey']));
          //$EntryType=$visaMoreInfo->appendChild($doc->createElement("EntryType"));
          $visaMoreInfo->appendChild($doc->createElement("EntryType",$visa_application[0]['VisaApplicantInfo']['EntryType']['var_value']));
          //$EntryType->appendChild($doc->createElement("VarType",$visa_application[0]['VisaApplicantInfo']['EntryType']['var_type']));
          $visaMoreInfo->appendChild($doc->createElement("NoofReEntryType",$visa_application[0]['VisaApplicantInfo']['no_of_re_entry_type']));
          $visaMoreInfo->appendChild($doc->createElement("StayDurationDays",$visa_application[0]['VisaApplicantInfo']['stay_duration_days']));
          $visaMoreInfo->appendChild($doc->createElement("ProposedDateOfTravel",$visa_application[0]['VisaApplicantInfo']['proposed_date_of_travel']));
          $visaMoreInfo->appendChild($doc->createElement("ModeofTravel",$visa_application[0]['VisaApplicantInfo']['mode_of_travel']));
          $visaMoreInfo->appendChild($doc->createElement("MoneyInHand",$visa_application[0]['VisaApplicantInfo']['money_in_hand']));
          $visaMoreInfo->appendChild($doc->createElement("AppliedNigeriaVisa",$visa_application[0]['VisaApplicantInfo']['applied_nigeria_visa']));
          $visaMoreInfo->appendChild($doc->createElement("NigeriaVisaAppliedPlace",$visa_application[0]['VisaApplicantInfo']['nigeria_visa_applied_place']));
          $visaMoreInfo->appendChild($doc->createElement("AppliedNigeriaVisaStatus",$visa_application[0]['VisaApplicantInfo']['applied_nigeria_visa_status']));
          $visaMoreInfo->appendChild($doc->createElement("AppliedNigeriaVisaRejectReason",$visa_application[0]['VisaApplicantInfo']['applied_nigeria_visa_reject_reason']));
          $visaMoreInfo->appendChild($doc->createElement("HaveVisitedNigeria",$visa_application[0]['VisaApplicantInfo']['have_visited_nigeria']));
          if(empty($visa_application[0]['VisaApplicantInfo']['VisaTypeReason']['var_value'])){
          $visaMoreInfo->appendChild($doc->createElement("HaveVisitedNigeria", ''));
          }else{
             $visaMoreInfo->appendChild($doc->createElement("HaveVisitedNigeria",$visa_application[0]['VisaApplicantInfo']['VisaTypeReason']['var_value']));
          }
          $visaMoreInfo->appendChild($doc->createElement("ApplyingCountryDuration",$visa_application[0]['VisaApplicantInfo']['applying_country_duration']));
          $visaMoreInfo->appendChild($doc->createElement("ContagiousDisease",$visa_application[0]['VisaApplicantInfo']['contagious_disease']));
          $visaMoreInfo->appendChild($doc->createElement("PoliceCase",$visa_application[0]['VisaApplicantInfo']['police_case']));
          $visaMoreInfo->appendChild($doc->createElement("NarcoticInvolvement",$visa_application[0]['VisaApplicantInfo']['narcotic_involvement']));
          $visaMoreInfo->appendChild($doc->createElement("DeportedStatus",$visa_application[0]['VisaApplicantInfo']['deported_status']));
          $visaMoreInfo->appendChild($doc->createElement("VisaFraudStatus",$visa_application[0]['VisaApplicantInfo']['visa_fraud_status']));
          //$visaCategory = $visaDetail->appendChild($doc->createElement("VisaCategory"));
          $visaMoreInfo->appendChild($doc->createElement("VisaCategory",$visa_application[0]['VisaCategory']['var_value']));
          $visaAddedDetails = $visaDetail->appendChild($doc->createElement("VisaAddedDetails"));
          $visaAddedDetails->appendChild($doc->createElement("EmployerName",$visa_application[0]['VisaApplicationDetails']['employer_name']));
          $visaAddedDetails->appendChild($doc->createElement("PositionOccupied",$visa_application[0]['VisaApplicationDetails']['position_occupied']));
          $visaAddedDetails->appendChild($doc->createElement("JobDescription",$visa_application[0]['VisaApplicationDetails']['job_description']));
          $visaAddedDetails->appendChild($doc->createElement("RelativeRmployerName",$visa_application[0]['VisaApplicationDetails']['relative_employer_name']));
          $visaAddedDetails->appendChild($doc->createElement("RelativeRmployerPhone",$visa_application[0]['VisaApplicationDetails']['relative_employer_phone']));
          $visaAddedDetails->appendChild($doc->createElement("RelativeNigeriaLeavingMth",$visa_application[0]['VisaApplicationDetails']['relative_nigeria_leaving_mth']));

          $visaARelatedEmpAdd = $visaAddedDetails->appendChild($doc->createElement("VisaRelativeEmployerAddress"));
          $visaARelatedEmpAdd->appendChild($doc->createElement("Address1",$visa_application[0]['VisaApplicationDetails']['VisaRelativeEmployerAddress']['address_1']));
          $visaARelatedEmpAdd->appendChild($doc->createElement("Address2",$visa_application[0]['VisaApplicationDetails']['VisaRelativeEmployerAddress']['address_2']));
          $visaARelatedEmpAdd->appendChild($doc->createElement("City",$visa_application[0]['VisaApplicationDetails']['VisaRelativeEmployerAddress']['city']));
          if(empty($visa_application[0]['VisaApplicationDetails']['VisaRelativeEmployerAddress']['Country']['country_name'])){
          $visaARelatedEmpAdd->appendChild($doc->createElement("Country", ''));
          }else{
             $visaARelatedEmpAdd->appendChild($doc->createElement("Country",$visa_application[0]['VisaApplicationDetails']['VisaRelativeEmployerAddress']['Country']['country_name']));
          }
          $visaARelatedEmpAdd->appendChild($doc->createElement("State",$visa_application[0]['VisaApplicationDetails']['VisaRelativeEmployerAddress']['state']));
          $visaARelatedEmpAdd->appendChild($doc->createElement("Postcode",$visa_application[0]['VisaApplicationDetails']['VisaRelativeEmployerAddress']['postcode']));
          $visaARelatedEmpAdd->appendChild($doc->createElement("RelativeNigeriaLivingMonth",$visa_application[0]['VisaApplicationDetails']['relative_nigeria_leaving_mth']));
          $VisaIntendedAddress = $visaARelatedEmpAdd->appendChild($doc->createElement("VisaIntendedAddress"));
          $VisaIntendedAddress->appendChild($doc->createElement("Address1",$visa_application[0]['VisaApplicationDetails']['VisaIntendedAddressNigeriaAddress']['address_1']));
          $VisaIntendedAddress->appendChild($doc->createElement("Address2",$visa_application[0]['VisaApplicationDetails']['VisaIntendedAddressNigeriaAddress']['address_2']));
          $VisaIntendedAddress->appendChild($doc->createElement("City",$visa_application[0]['VisaApplicationDetails']['VisaIntendedAddressNigeriaAddress']['city']));
          $VisaIntendedAddress->appendChild($doc->createElement("Country","Nigeria"));
          $VisaIntendedAddress->appendChild($doc->createElement("State",$visa_application[0]['VisaApplicationDetails']['VisaIntendedAddressNigeriaAddress']['State']['state_name']));
          if(empty($visa_application[0]['VisaApplicationDetails']['VisaIntendedAddressNigeriaAddress']['LGA']['lga'])){
          $VisaIntendedAddress->appendChild($doc->createElement("LGA",''));
          }
          else{
          $VisaIntendedAddress->appendChild($doc->createElement("LGA",$visa_application[0]['VisaApplicationDetails']['VisaIntendedAddressNigeriaAddress']['LGA']['lga']));
          }
          $VisaIntendedAddress->appendChild($doc->createElement("District",$visa_application[0]['VisaApplicationDetails']['VisaIntendedAddressNigeriaAddress']['district']));
          $VisaIntendedAddress->appendChild($doc->createElement("Postcode",$visa_application[0]['VisaApplicationDetails']['VisaIntendedAddressNigeriaAddress']['postcode']));
          $Period1 = $visaDetail->appendChild($doc->createElement("Period1"));
          $Period1->appendChild($doc->createElement("From",$applicantPreviousNigeriaHistory[0]['startdate']));
          $Period1->appendChild($doc->createElement("To",$applicantPreviousNigeriaHistory[0]['endate']));
          $Period1->appendChild($doc->createElement("Address1",$applicantPreviousNigeriaHistory[0]['VisaApplicantPreviousHistoryAddress']['address_1']));
          $Period1->appendChild($doc->createElement("Address2",$applicantPreviousNigeriaHistory[0]['VisaApplicantPreviousHistoryAddress']['address_2']));
          $Period1->appendChild($doc->createElement("City",$applicantPreviousNigeriaHistory[0]['VisaApplicantPreviousHistoryAddress']['city']));
          $Period1->appendChild($doc->createElement("Country","Nigeria"));
          if(empty($applicantPreviousNigeriaHistory[0]['VisaApplicantPreviousHistoryAddress']['State']['state_name'])){
          $Period1->appendChild($doc->createElement("State",''));
          }
          else{
          $Period1->appendChild($doc->createElement("State",$applicantPreviousNigeriaHistory[0]['VisaApplicantPreviousHistoryAddress']['State']['state_name']));
          }
          if(empty($applicantPreviousNigeriaHistory[0]['VisaApplicantPreviousHistoryAddress']['LGA']['lga'])){
          $Period1->appendChild($doc->createElement("LGA",''));
          }
          else{
          $Period1->appendChild($doc->createElement("LGA",$applicantPreviousNigeriaHistory[0]['VisaApplicantPreviousHistoryAddress']['LGA']['lga']));
          }
          $Period1->appendChild($doc->createElement("District",$applicantPreviousNigeriaHistory[0]['VisaApplicantPreviousHistoryAddress']['district']));
          $Period1->appendChild($doc->createElement("Postcode",$applicantPreviousNigeriaHistory[0]['VisaApplicantPreviousHistoryAddress']['postcode']));
          $Period2 = $visaDetail->appendChild($doc->createElement("Period2"));
          $Period2->appendChild($doc->createElement("From",$applicantPreviousNigeriaHistory[1]['startdate']));
          $Period2->appendChild($doc->createElement("To",$applicantPreviousNigeriaHistory[1]['endate']));
          $Period2->appendChild($doc->createElement("Address1",$applicantPreviousNigeriaHistory[1]['VisaApplicantPreviousHistoryAddress']['address_1']));
          $Period2->appendChild($doc->createElement("Address2",$applicantPreviousNigeriaHistory[1]['VisaApplicantPreviousHistoryAddress']['address_2']));
          $Period2->appendChild($doc->createElement("City",$applicantPreviousNigeriaHistory[1]['VisaApplicantPreviousHistoryAddress']['city']));
          $Period2->appendChild($doc->createElement("Country","Nigeria"));
          if(empty($applicantPreviousNigeriaHistory[1]['VisaApplicantPreviousHistoryAddress']['State']['state_name'])){
          $Period2->appendChild($doc->createElement("State",''));
          }
          else{
          $Period2->appendChild($doc->createElement("State",$applicantPreviousNigeriaHistory[1]['VisaApplicantPreviousHistoryAddress']['State']['state_name']));
          }
          if(empty($applicantPreviousNigeriaHistory[1]['VisaApplicantPreviousHistoryAddress']['State']['state_name'])){
          $Period2->appendChild($doc->createElement("LGA",''));
          }
          else{
          $Period2->appendChild($doc->createElement("LGA",$applicantPreviousNigeriaHistory[1]['VisaApplicantPreviousHistoryAddress']['LGA']['lga']));
          }
          $Period2->appendChild($doc->createElement("District",$applicantPreviousNigeriaHistory[1]['VisaApplicantPreviousHistoryAddress']['district']));
          $Period2->appendChild($doc->createElement("Postcode",$applicantPreviousNigeriaHistory[1]['VisaApplicantPreviousHistoryAddress']['postcode']));
          $Period3 = $visaDetail->appendChild($doc->createElement("Period3"));
          $Period3->appendChild($doc->createElement("From",$applicantPreviousNigeriaHistory[2]['startdate']));
          $Period3->appendChild($doc->createElement("To",$applicantPreviousNigeriaHistory[2]['startdate']));
          $Period3->appendChild($doc->createElement("Address1",$applicantPreviousNigeriaHistory[2]['VisaApplicantPreviousHistoryAddress']['address_1']));
          $Period3->appendChild($doc->createElement("Address2",$applicantPreviousNigeriaHistory[2]['VisaApplicantPreviousHistoryAddress']['address_2']));
          $Period3->appendChild($doc->createElement("City",$applicantPreviousNigeriaHistory[2]['VisaApplicantPreviousHistoryAddress']['city']));
          $Period3->appendChild($doc->createElement("City","Nigeria"));
          if(empty($applicantPreviousNigeriaHistory[2]['VisaApplicantPreviousHistoryAddress']['State']['state_name'])){
          $Period3->appendChild($doc->createElement("State",''));
          }
          else{
          $Period3->appendChild($doc->createElement("State",$applicantPreviousNigeriaHistory[2]['VisaApplicantPreviousHistoryAddress']['State']['state_name']));
          }
          if(empty($applicantPreviousNigeriaHistory[2]['VisaApplicantPreviousHistoryAddress']['State']['state_name'])){
          $Period3->appendChild($doc->createElement("LGA",''));
          }
          else{
          $Period3->appendChild($doc->createElement("LGA",$applicantPreviousNigeriaHistory[2]['VisaApplicantPreviousHistoryAddress']['LGA']['lga']));
          }
          $Period3->appendChild($doc->createElement("District",$applicantPreviousNigeriaHistory[2]['VisaApplicantPreviousHistoryAddress']['district']));
          $Period3->appendChild($doc->createElement("Postcode",$applicantPreviousNigeriaHistory[2]['VisaApplicantPreviousHistoryAddress']['postcode']));
          $travelHistory = $visaDetail->appendChild($doc->createElement("TravelHistory"));
          //$travelHistory->appendChild($doc->createElement("ApplyingCountryDuration",$visa_application[0]['VisaApplicantInfo']['applying_country_duration']));
          //$travelHistory->appendChild($doc->createElement("ContagiousDisease",$visa_application[0]['VisaApplicantInfo']['contagious_disease']));
          //$travelHistory->appendChild($doc->createElement("PoliceCase",$visa_application[0]['VisaApplicantInfo']['police_case']));
          //$travelHistory->appendChild($doc->createElement("NarcoticInvolvement",$visa_application[0]['VisaApplicantInfo']['narcotic_involvement']));
          //$travelHistory->appendChild($doc->createElement("DeportedStatus",$visa_application[0]['VisaApplicantInfo']['deported_status']));
          //$travelHistory->appendChild($doc->createElement("VisaFraudStatus",$visa_application[0]['VisaApplicantInfo']['visa_fraud_status']));
          $Period1 = $travelHistory->appendChild($doc->createElement("Period1"));
          if(empty($applicantTravelHistory[0]['Country']['country_name'])){
           $Period1->appendChild($doc->createElement("Country", ''));
          }else{
          $Period1->appendChild($doc->createElement("Country",$applicantTravelHistory[0]['Country']['country_name']));
          }
          $Period1->appendChild($doc->createElement("City",$applicantTravelHistory[0]['city']));
          $Period1->appendChild($doc->createElement("DateofDeparture",$applicantTravelHistory[0]['date_of_departure']));
          $Period2 = $travelHistory->appendChild($doc->createElement("Period2"));
          if(empty($applicantTravelHistory[1]['Country']['country_name'])){
          $Period2->appendChild($doc->createElement("Country",''));
          }
          else{
           $Period2->appendChild($doc->createElement("Country",$applicantTravelHistory[1]['Country']['country_name']));
          }

          $Period2->appendChild($doc->createElement("City",$applicantTravelHistory[1]['city']));
          $Period2->appendChild($doc->createElement("DateofDeparture",$applicantTravelHistory[1]['date_of_departure']));
          $Period3 = $travelHistory->appendChild($doc->createElement("Period3"));
          if(empty($applicantTravelHistory[2]['Country']['country_name'])){
            $Period3->appendChild($doc->createElement("Country",''));
          }else{
            $Period3->appendChild($doc->createElement("Country",$applicantTravelHistory[2]['Country']['country_name']));
          }
          $Period3->appendChild($doc->createElement("City",$applicantTravelHistory[2]['city']));
          $Period3->appendChild($doc->createElement("DateofDeparture",$applicantTravelHistory[2]['date_of_departure']));
          $Period4 = $travelHistory->appendChild($doc->createElement("Period1"));
          if(empty($applicantTravelHistory[3]['Country']['country_name'])){
            $Period4->appendChild($doc->createElement("Country",''));
          }else{
            $Period4->appendChild($doc->createElement("Country",$applicantTravelHistory[3]['Country']['country_name']));
          }
          $Period4->appendChild($doc->createElement("City",$applicantTravelHistory[3]['city']));
          $Period4->appendChild($doc->createElement("DateofDeparture",$applicantTravelHistory[3]['date_of_departure']));
          $Period5 = $travelHistory->appendChild($doc->createElement("Period2"));
          if(empty($applicantTravelHistory[4]['Country']['country_name'])){
            $Period5->appendChild($doc->createElement("Country",''));
          }else{
            $Period5->appendChild($doc->createElement("Country",$applicantTravelHistory[4]['Country']['country_name']));
          }
          $Period5->appendChild($doc->createElement("City",$applicantTravelHistory[4]['city']));
          $Period5->appendChild($doc->createElement("DateofDeparture",$applicantTravelHistory[4]['date_of_departure']));
          $Period6 = $travelHistory->appendChild($doc->createElement("Period3"));
          if(empty($applicantTravelHistory[5]['Country']['country_name'])){
            $Period6->appendChild($doc->createElement("Country",''));
          }else{
            $Period6->appendChild($doc->createElement("Country",$applicantTravelHistory[5]['Country']['country_name']));
          }
          $Period6->appendChild($doc->createElement("City",$applicantTravelHistory[5]['city']));
          $Period6->appendChild($doc->createElement("DateofDeparture",$applicantTravelHistory[5]['date_of_departure']));

          /**
           * Payment Inforamtion start here...
           */
          $paymentInformation = $visaDetail->appendChild($doc->createElement("PaymentInforamtion"));
          $paymentInformation->appendChild($doc->createElement("ApplicationFees",$applicationFees));
          $paymentInformation->appendChild($doc->createElement("Currency", 'Dollar'));
          $paymentInformation->appendChild($doc->createElement("Status",$visa_application[0]['status']));
          $paymentInformation->appendChild($doc->createElement("PaidOn",$visa_application[0]['paid_at']));
          $paidAmount = ($visa_application[0]['status'] == 'New')?'':$applicationFees;
          $paymentInformation->appendChild($doc->createElement("PaidAmount",$paidAmount));

          
          $xmlDate = $doc->saveXML();

          return $xmlDate;
    }

    /**
     * [WP: 099] => CR: 141
     * @param <type> $request
     * This function is being used to get paid an application...
     */
    public function executeApplicationPaid(sfWebRequest $request){

        if($request->isMethod('POST')){

            $this->setLayout(false);
        
            $service_url = settings::getHTTPpath().'/services/app/q/appPaid';
            $curl = curl_init();
            $curl_post_data = array(
                "username" => sfConfig::get('app_webService_query_username'),
                "password" => sfConfig::get('app_webService_query_password'),
                "applicationId" => $request->getParameter('applicationId'),
                "referenceNumber" => $request->getParameter('referenceNumber'),
                "transactionNumber" => $request->getParameter('transactionNumber'),
                "appAmount" => $request->getParameter('applicationAmount'),
            );
           
            curl_setopt($curl, CURLOPT_URL, $service_url);
            curl_setopt($curl, CURLOPT_HEADER, false);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
            curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
            
            $curl_response = curl_exec($curl);            
            curl_close($curl);

            echo "<pre>";print_r($curl_response);echo "</pre>";
                 
            exit;
        }

    }

    /**
    * [WP: 099] => CR: 141
    * @param <type> $request
    */
    
    public function executeMarkApplicationPaid(sfWebRequest $request){

        $this->setLayout(false);

        $applicationId = (int)$request->getParameter('applicationId');
        $referenceNumber = (int)$request->getParameter('referenceNumber');
        $transactionNumber = $request->getParameter('transactionNumber');
        $appAmount = $request->getParameter('appAmount');

        if($applicationId !='' && $referenceNumber != '' && $transactionNumber != '' && $appAmount != '' ){

           $oisRequestData = Doctrine::getTable('EpOisRequest')->findByTransactionId($transactionNumber);
           if(count($oisRequestData) < 1){
               $visaData = Doctrine::getTable('VisaApplication')->find($applicationId);

               if(!empty($visaData)){
                   ## If visacategoryId is 29 or 101 i.e., Visa and reference number in table matches with reference number provided by user then true...
                   ## 29 means Entry Visa and 101 means Entry Freezone
                   if(($visaData->getVisacategoryId() == 29 || $visaData->getVisacategoryId() == 101) && $visaData->getRefNo() == $referenceNumber){

                      $visaApplicantInfo = Doctrine::getTable('VisaApplicantInfo')->findByApplicationId($applicationId);

                      if(count($visaApplicantInfo) > 0){

                          if(strtolower($visaData->getStatus()) == 'new'){
                              $applicationFees = $visaData->getDollarFee();
                              if($applicationFees == $appAmount ){

                                  if($visaApplicantInfo[0]['applying_country_id'] == 'GB'){
                                      $appData = $applicationId.'~visa';
                                      $errorType = $this->applicationPaidProcess($appData, $transactionNumber);
                                  }else{                                      
                                      $errorType = 'invalidCountry';
                                  }
                              }else{                                   
                                   $errorType = 'invalidAmount';
                              }
                          }else{
                              $errorType = 'already_paid';
                          }

                      }else{
                            $errorType = 'invalidData';
                      }

                   }else{
                       $errorType = 'invalidApplication';
                   }
               }else{                  
                  $errorType = 'invalidData';
               }
           }else{               
               $errorType = 'invalidTrasactionNumber';
           }
        }else{
            $errorType = 'invalidData';
        }


        $xml = $this->generateXmlForPaidApplication($errorType, $applicationId, $referenceNumber, $transactionNumber, $appAmount);

        ## Creating Response log...
        Functions::createLogData($xml,'OIS_Response-'.$applicationId, 'OIS');

        return $this->renderText($xml);
        exit;

        
        
    }

    /**
    * [WP: 099] => CR: 141
    * @param <type> $request
    */
    private function applicationPaidProcess($appVar, $transactionNumber){
        
        $appDetails[0] = SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT($appVar));
        $result = $this->applicationOISPayment($appDetails, $transactionNumber);
        return $result;
    }
    
    /**
    * [WP: 099] => CR: 141
    * @param <type> $request
    */
    private function applicationOISPayment($appDetails, $transactionNumber) {

        /**
         * [WP: 099] => CR: 141
         * Getting varibale for OIS payment system...
         */        
        $paformeLibObj = new iPayformeLib();
        $returnValueByCurl = $paformeLibObj->postMultipleItems($appDetails);

        if ($returnValueByCurl == "already_paid") {
            return $returnValueByCurl;
        }

        $returnValueByCurl = explode('?', $returnValueByCurl);
        if(count($returnValueByCurl) > 1){
            $order = explode('=', $returnValueByCurl[1]);

            $sfRequest = sfContext::getInstance()->getRequest();
            $sfRequest->setParameter('order', $order[1]);
            $sfRequest->setParameter('oisPayment', true);
            $sfRequest->setParameter('transactionNumber', $transactionNumber);

            $sfController = sfContext::getInstance()->getController();
            $sfController->forward('order', 'proceed');
            
            //echo $returnValueByCurl = $returnValueByCurl.'&oisPayment=true&transactionNumber='.$transactionNumber;
            //$this->redirect($returnValueByCurl);
        }else{
          return 'Invalid_Request';
        }        
        exit;
    }

    /**
    * [WP: 099] => CR: 141
    * @param <type> $request
    */
    public function executeSendOISPaymentNotify(sfWebRequest $request){

        $this->setLayout(false);
        //return $this->renderText('invalid');
        //exit;
        
        $requestId = $request->getParameter('requestId');
        $transactionNumber = $request->getParameter('transactionNumber');
        $this->request_details = Doctrine::getTable('OrderRequestDetails')->find($requestId);
        $appDetails = CartItemsTransactionsTable::getInstance()->getRetryIdDetailsOnNis($this->request_details->getRetryId());
        if (count($appDetails) == 1) {
            $appId = $appDetails[0]['id'];
            $application_type = strtolower($appDetails[0]['app_type']);
            if ($application_type == 'visa') {

                /**
                 * Inserting application data into transaction service charge table...
                 */

                ## Checking duplicate record for the same request id...
                $serviceObj = Doctrine::getTable('TransactionServiceCharges')->getApplicationDetails($appDetails[0]['id'], $appDetails[0]['app_type'], $requestId);
                if(count($serviceObj) < 1){
                    $serviceChargeObj = new TransactionServiceCharges();
                    $serviceChargeObj->setAppId($appDetails[0]['id']);
                    $serviceChargeObj->setRefNo($appDetails[0]['ref_no']);
                    $serviceChargeObj->setAppType($appDetails[0]['app_type']);
                    
                    /**
                     * [WP: 102] => CR: 144
                     * Adding addtional charges for visa if applicable...
                     */
                    if(sfConfig::get('app_visa_additional_charges_flag')){
                        $serviceChargeObj->setServiceCharge(sfConfig::get('app_visa_additional_charges'));
                    }else{
                        $serviceChargeObj->setServiceCharge(0);
                    }

                    ## Fetching Application fees...
                    $appObj = Doctrine::getTable('VisaApplication')->find($appDetails[0]['id']);
                    $dollarFees = $appObj->getDollarFee();

                    ## Getting processing country...
                    $processingCountry = 'GB'; //$appObj->getApplyingCountryId();

                    $serviceChargeObj->setAppAmount($dollarFees);

                    $serviceChargeObj->setOrderRequestDetailId($requestId);
                    /**
                     * [WP: 112] => CR: 158
                     * Updated payment status...
                     * Setting 0 means payemnt is done.
                     */
                    $serviceChargeObj->setStatus(0);
                    $serviceChargeObj->save();
                }

                /**
                 * Creating Order...
                 */
                $params = NonAuthPaymentManager::createOrder($request, $processingCountry);
                $order_number = $params['orderNumber'];

                $paymentResponseArray = array();
                $paymentResponseArray['response_code'] = 1;
                $paymentResponseArray['response_text'] = 'Approved';
                $paymentResponseArray['card_Num'] = '5555555555555555'; //$params['cardNumber'];
                $paymentResponseArray['card_type'] = 'M';
                $paymentResponseArray['status'] = 1;


                /**
                 * Sending Payment Notification...
                 */
                $payManagerObj = new PaymentManager();
                $orderObj = $payManagerObj->updatePaymentRequestNmi($params['order_id'], $paymentResponseArray);
                $updateOrder = $payManagerObj->updateOrderRequest($params['order_id'], $paymentResponseArray);
                $gateway_id = 11;
                $user_id = 1; //Admin
                $updateOrder = NonAuthPaymentManager::updateSplitAmount($requestId, $gateway_id, $user_id);
                $result = NonAuthPaymentManager::paymentSuccess($order_number, $user_id);
                $status = "success";

                if($result['status'] == 'success'){
                    $appObj->setTermChkFlg(1);
                    $appObj->save();
                }             

                

                /**
                 * Inserting Request...
                 */
                $oisRequestId = $this->saveOisRequest($transactionNumber, $params['orderNumber'], $params['amount']);

                /**
                 * Insering response...
                 */

                $transactionResponse = array();
                $transactionResponse['result'] = 1;
                $transactionResponse['result'] = 1;
                $transactionResponse['result-text'] = 'Success';
                $transactionResponse['transaction-id'] = $transactionNumber;
                $transactionResponse['result-code'] = 100;
                $transactionResponse['authorization-code'] = NULL;
                $transactionResponse['avs-result'] = NULL;
                $transactionResponse['action-type'] = 'OIS';
                $transactionResponse['amount'] = $params['amount'];
                $transactionResponse['ip-address'] = $params['ip_address'];
                $transactionResponse['industry'] = NULL;
                $transactionResponse['processor-id'] = 0;
                $transactionResponse['currency'] = 'USD';
                $transactionResponse['tax-amount'] = 0;
                $transactionResponse['shipping-amount'] = 0;
                $transactionResponse['billing'][0]['first-name'] = $params['fname'];
                $transactionResponse['billing'][0]['last-name'] = $params['lname'];
                $transactionResponse['billing'][0]['address1'] = NULL;
                $transactionResponse['billing'][0]['city'] = NULL;
                $transactionResponse['billing'][0]['country'] = $processingCountry;
                $transactionResponse['billing'][0]['phone'] = $params['phone'];
                $transactionResponse['billing'][0]['email'] = $params['billing-email'];
                $transactionResponse['billing'][0]['cc-number'] = '555555******5555';
                $transactionResponse['billing'][0]['cc-exp'] = NULL;

                $saveResponse = Doctrine::getTable('EpOisResponse')->setResponse($transactionResponse, $oisRequestId, $params['orderNumber'], '');


               // WP[094]=> setting data to NIS table for POS report purpose..

                    $serviceChargeObj = new TransactionServiceChargesNIS();
                    $serviceChargeObj->setAppId($appDetails[0]['id']);
                    $serviceChargeObj->setRefNo($appDetails[0]['ref_no']);
                    $serviceChargeObj->setAppType($appDetails[0]['app_type']);

                    /**
                     * [WP: 102] => CR: 144
                     * Adding addtional charges for visa if applicable...
                     */
                    if(sfConfig::get('app_visa_additional_charges_flag')){
                        $serviceChargeObj->setServiceCharge(sfConfig::get('app_visa_additional_charges'));
                    }else{
                        $serviceChargeObj->setServiceCharge(0);
                    }

                    ## Fetching Application fees...
                    $appObj = Doctrine::getTable('VisaApplication')->find($appDetails[0]['id']);
                    $dollarFees = $appObj->getDollarFee();
                    $serviceChargeObj->setAppAmount($dollarFees);
                    $serviceChargeObj->setAppConvertAmount('0');
                    $serviceChargeObj->setAppCurrency('1');
                    $serviceChargeObj->setAppConvertServiceCharge('0');
                    $serviceChargeObj->save();

               // end of WP[094]

            }//End of if ($this->application_type == 'visa') {...            
           
            $xml = $this->generateXmlForPaidApplication('success',$appObj->getId(), $appObj->getRefNo(), $transactionNumber, $dollarFees, $order_number);

        }//End of if (count($appDetails) == 1) {...
        else{            
            $xml = $this->generateXmlForPaidApplication('processProblem','', '', $transactionNumber, '');
        }

        return $this->renderText($xml);
        exit;
    }

   
    /**
    * [WP: 099] => CR: 141
    * @param <type> $request
    */
    function saveOisRequest($transactionId, $orderNumber, $amount)
    {
        $epOisRequest = new EpOisRequest();
        $epOisRequest->setGatewayId(11);
        $epOisRequest->setOrderId($orderNumber);
        $epOisRequest->setTransactionId($transactionId);
        $epOisRequest->setAmount($amount);
        $epOisRequest->save();

        return $epOisRequest->getId();
    }
    
    /**
    * [WP: 099] => CR: 141
    * @param <type> $request
    */
    private function generateXmlForPaidApplication($errorType, $applicationId='', $referenceNumber='', $transactionNumber='', $appAmount='', $order_number=''){
        $xml = '<?xml version="1.0"?><message><ApplicationDetails>';
        if($applicationId != ''){
            $xml .= '<ApplicationId>'.$applicationId.'</ApplicationId>';
        }
        if($referenceNumber != ''){
            $xml .= '<ReferenceNumber>'.$referenceNumber.'</ReferenceNumber>';
        }
        if($appAmount != ''){
            $xml .= '<ApplicationAmount>'.$appAmount.'</ApplicationAmount>';
        }
        if($transactionNumber != ''){
            $xml .= '<TransactionNumber>'.$transactionNumber.'</TransactionNumber>';
        }
        $xml .= '<Currency>Dollar</Currency>';
        if($order_number != ''){
            $xml .= '<OrderNumber>'.$order_number.'</OrderNumber>';
            $xml .= '<TransactionDate>'.date('Y-m-d H:i:s').'</TransactionDate>';
        }        
        $xml .= '</ApplicationDetails><PaymentResponse>';
        switch(strtolower($errorType)){
            case 'success':
                $xml .= '<ResponseCode>100</ResponseCode><ResponseText>Success</ResponseText>';
                break;
            case 'invalidtrasactionnumber':
                $xml .= '<ResponseCode>001</ResponseCode><ResponseText>Duplicate Transaction Id</ResponseText>';
                break;
            case 'invaliddata':
                $xml .= '<ResponseCode>002</ResponseCode><ResponseText>Invalid Data</ResponseText>';
                break;
            case 'invalidamount':
                $xml .= '<ResponseCode>003</ResponseCode><ResponseText>Invalid Application Amount</ResponseText>';
                break;
            case 'invalidapplication':
                $xml .= '<ResponseCode>004</ResponseCode><ResponseText>Invalid Application Type</ResponseText>';
                break;
            case 'invalidcountry':
                $xml .= '<ResponseCode>005</ResponseCode><ResponseText>Invalid Application Country</ResponseText>';
                break;
            case 'already_paid':
                $xml .= '<ResponseCode>006</ResponseCode><ResponseText>Application Already Paid</ResponseText>';
                break;
            case 'invalid_request':
                $xml .= '<ResponseCode>007</ResponseCode><ResponseText>Invalid Request</ResponseText>';
                break;
            case 'processproblem':
                $xml .= '<ResponseCode>102</ResponseCode><ResponseText>Problem found during process.</ResponseText>';
                break;
            default:
                $xml .= '<ResponseCode>101</ResponseCode><ResponseText>Payment not supported</ResponseText>';
                break;
        }
        
        $xml .= '</PaymentResponse></message>';
        return $xml;

    }

    
}
