<?php

/**
 *  order actions.
 *
 * @package    mysfp
 * @subpackage  nis
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12479 2008-10-31 10:54:40Z fabien $
 */
class nisActions extends sfActions {

    /**
     * Executes index actionvalidated params
     *
     * @param sfRequest $request A request object
     */
    public function executeIndex(sfWebRequest $request) {
        $appVars = $request->getParameter('appVars');

        $appvar = explode('###', base64_decode($appVars));
        $this->heading = $appvar[0];
        $this->type = $appvar[0];
        if ($this->type == "passport" || $this->type == "visa" || $this->type == 'vap') {
            if (isset($appvar[1])){
                $this->country = $appvar[1];

                /**
                 * [WP: 104] => CR: 149
                 * Setting flag true for jna service to change address defined in bottom of the page...
                 */
                $sess_jna_processing = Functions::setFooterAddressFlag($this->country);
                $this->getUser()->setAttribute('sess_jna_processing', $sess_jna_processing);
            }
        }
        else {
            $this->country = '';
            $this->type = base64_decode($appVars);
        }
        if ($this->getUser()->isAuthenticated()) {

            $redirectObj = new NisManager();
            $redirectObj->redirectoNisPage($appVars);
        } else {
            $this->setTemplate('nis');
        }
    }

    public function executeIframe() {
        $this->setLayout(false);
    }

    public function executeEditReEntry(sfWebRequest $request) {
        $getAppId = SecureQueryString::DECODE($request->getParameter('id'));
        $getAppId = SecureQueryString::ENCRYPT_DECRYPT($getAppId);
        $this->redirect('visa/editReEntry?id=' . $getAppId);
    }

    public function executeAjaxCall(sfWebRequest $request) {
        $act = $request->getParameter('act');
        switch ($act) {
            case 'countryPaymentMode':
                $processingCountry = $request->getParameter('processingCountry');
                $cardType = Doctrine::getTable('CountryPaymentMode')->getPaymentOptions($processingCountry);
                if ($cardType === 0) {
                    $cardType = Doctrine::getTable('CountryPaymentMode')->getPaymentOptions('ALL');
                }
                echo $cardType;
                break;
            default:
                break;
        }

        $this->setLayout(false);
        exit;
    }

    public function executeNewApplication(sfWebRequest $request) {
        $this->AppArray = array('p' => 'Passport', 'f' => 'Reentry FreeZone', 'v' => 'Visa/Free Zone');
        if(sfConfig::get('app_visa_arrival_enable')=='true')
        {
            $this->AppArray['vap'] = 'Visa on Arrival Program';
        }
        $this->isPrivilegeUser = $this->getUser()->isPrivilegeUser();
        ## Fetching all card type from CountryPaymentMode table...
        $cardTypeArray = Doctrine::getTable('CountryPaymentMode')->findAll()->toArray();
        $this->cardTypeArrayCount = count($cardTypeArray);
        $this->cardType = array();
        for ($i = 0; $i < $this->cardTypeArrayCount; $i++) {
            $countryCode = $cardTypeArray[$i]['country_code'];
            $this->cardType[$countryCode] = $cardTypeArray[$i]['card_type'];
        }
    }

    public function executeNewVisa(sfWebRequest $request) {
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
        $country = $request->getParameter('country');
        $appvars = base64_encode("visa###" . $country);
        $this->redirect(url_for('nis/index?appVars=' . $appvars));
    }

    public function executeNewPassport(sfWebRequest $request) {
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
        $country = $request->getParameter('country');
        $appvars = base64_encode("passport###" . $country);
        $this->redirect(url_for('nis/index?appVars=' . $appvars));
    }

    public function executeGetApplicationStatus(sfWebRequest $request) {
        $appId = $request->getParameter("appId");
        $appType = $request->getParameter("appType");
        $orderNumber = $request->getParameter("orderNumber");
        $this->setTemplate(false);
        if (isset($appId) && $appId != '' && isset($appType) && $appType != '' && isset($orderNumber) && $orderNumber != '') {
            $nisAppStatus = RollbackPaymentTable::getInstance()->getNisApplicationStatus($appId, $appType, $orderNumber);
            echo $nisAppStatus;
            die;
        } else {
            return $this->renderText("invaild parameters");
        }
    }

    //Start Code for country drop down
    public function executeGetCountry(sfWebRequest $request) {
        $appType = $request->getParameter("app_type");

        $countryArray = Doctrine::getTable('Country')->getCountryListOption($appType);
        $str = '<option value="">-- Please Select --</option>';
        if (count($countryArray) > 0) {
            foreach ($countryArray as $key => $value) {
                $str .= "<option value='$key'> $value </option>";
            }
        }
        return $this->renderText($str);
    }

    public function executeGetEmbassy(sfWebRequest $request) {
        sfContext::getInstance()->getLogger()->info("+++" . $request->getParameter('app_country'));
        $embassyArray = Doctrine::getTable('EmbassyMaster')->createQuery('st')->
                        addWhere('embassy_country_id = ?', $request->getPostParameter('app_country'))->execute()->toArray();
        //get
        $str ='';
       
        if(count($embassyArray) == 0){
            $str .= "<span>Unfortunately, the country you selected do not have any processing center, please select some other country to continue.</span>";
            return $this->renderText($str);
        }else{
            return $this->renderText(false);
        }
        
    }

    public function executeGeneratePrint(sfWebRequest $request) {
      // $this->isFound = false;
      $AppId = $request->getParameter("app_id");
      $referenceNo = $request->getParameter("reference_no");

      if ($request->isMethod('post')) {
        unset($_SESSION['app_id']);
        unset($_SESSION['reference_no']);

        $_SESSION['app_id'] = $AppId;
        $_SESSION['reference_no'] = $referenceNo;
      } else if ($request->hasParameter('back')) {
        $AppId = $_SESSION['app_id'];
        $referenceNo = $_SESSION['reference_no'];
      } else {
        $_SESSION['app_id'] = NULL;
        $_SESSION['reference_no'] = NULL;
      }

    }
    public function executeValidatePrint(sfWebRequest $request) {
           
        $appType = $request->getParameter('app_type');
        $appID = $request->getParameter('app_id');
        $refNo = $request->getParameter('reference_no');        
        
        $returnData = array();        

        $returnData['flag'] = 'notfound';
        $dataExistFlag = false;
        if (isset($appID) && $appID != '' && isset($refNo) && $refNo != 0) {
            $appDeatils = array();
            switch($appType){
                case 'passport':
                    $appDeatils['passport_id'] = $appID;
                    $returnData['appId'] = SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT($appID));
                    $dataExist = Doctrine::getTable('PassportApplication')->getPassportInfoByAppId($appID, $refNo);
                    if($dataExist)
                    {
                            $firstProcessingCountry[] = Doctrine::getTable('PassportApplication')->getProcessingCountry($appID);
                            if($firstProcessingCountry[0]!='NG')
                            {
                                $dataExistFlag = true;
                            }
                    }
                    break;
                case 'visa':
                    $appDeatils['visa_id'] = $appID;
                    $returnData['appId'] = SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT($appID));
                    $dataExist = Doctrine::getTable('VisaApplication')->getVisaInfoById($appID);
                    if(count($dataExist) > 0){                        
                        if($dataExist[0]['visacategory_id'] != 102){
                            $firstProcessingCountry[] = Doctrine::getTable('VisaApplicantInfo')->getProcessingCountry($appID);
                            if($firstProcessingCountry[0]!='NG')
                            {
                                $dataExistFlag = true;
                            }
                        }
                    }                    
                    break;
                case 'freezone':
                    //102
                    $appDeatils['freezone_id'] = $appID;
                    $returnData['appId'] = SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT($appID));
                    $dataExist = Doctrine::getTable('VisaApplication')->getVisaInfoById($appID);
                    if(count($dataExist) > 0){
                        if($dataExist[0]['visacategory_id'] == 102){
                            $dataExistFlag = true;
                        }
                    }
                    break;
                 case 'vap':
                    $appDeatils['visa_arrival_program_id'] = $appID;
                    $returnData['appId'] = SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT($appID));
                    $dataExist = Doctrine::getTable('VapApplication')->getVisaInfoById($appID);
                    if(count($dataExist) > 0){                        
                        $dataExistFlag = true;
                    }
                    break;
                default:
                    break;
            }

            if($dataExistFlag){
                $appData = Doctrine::getTable('CartItemsTransactions')->getApplicationDetails($appDeatils); 
                if(count($appData) > 0){
                    if($refNo == $appData['ref_no']){
                        $returnData['flag'] = 'found';
                        $status = $appData['status'];
                    }
                   if($appData['app_type']=='visa' && isset($appData['visacategory_id']))
                   {
                        $returnData['flag'] = 'notfound';
                   }
                }
                else
                {
                    $returnData['flag'] = 'notfound';
                }
            }
            
        }

        if($returnData['flag'] == 'found' && (strtolower($status) == 'paid' || strtolower($status) == 'approved') ){
            $orderRequestDetails = Doctrine::getTable('TransactionServiceCharges')->getOrderRequestDetailId($appID, $appType);
            if(count($orderRequestDetails) > 0){                
                foreach($orderRequestDetails AS $orderDetail){
                    $ipay4meOrderDetails = Doctrine::getTable('ipay4meOrder')->findSuccessfulPaymentDetail($orderDetail->getOrderRequestDetailId());
                    if(!empty($ipay4meOrderDetails)){
                        if($ipay4meOrderDetails['gateway_id'] != '6'){
                          $userId = $this->getUser()->getGuardUser()->getId();
                          if($userId != $ipay4meOrderDetails['created_by']){
                              $userDetail = Doctrine_Core::getTable('UserDetail')->findByUserId($ipay4meOrderDetails['created_by']);
                              $returnData['email'] = $userDetail[0]['email'];
                              $returnData['flag'] = 'paidfound';
                          }
                        }
                      break;
                    }//End of if(!empty($ipay4meOrderDetails)){...                    
                }//End of foreach($orderRequestDetails AS $orderDetail){...
            }//End of if(count($orderRequestDetails) > 0){...
        }       
        $text = json_encode($returnData);
        return $this->renderText($text);
    }
    
    public function executeNewVisaWaiver(sfWebRequest $request){
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
        $country = $request->getParameter('country');
        $appvars = base64_encode("vap###" . $country);
        $this->redirect(url_for('nis/index?appVars=' . $appvars));
    }



}
