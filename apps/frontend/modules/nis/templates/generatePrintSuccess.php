
<script>
  function openPopUp(url){
      url = url + '/mode/cart'
      window.open (url, "mywindow","status=1,toolbar=0,width=800px,height=800px,location=0,menubar=0,scrollbars=1");
     // alert(url);
  }

  function validateForm()
  {
    $('#detail_div').html('<?php echo image_tag('../images/ajax-loader.gif'); ?>');
    var err  = 0;


    var app_type = jQuery.trim($('#app_type').val());
    var app_id = jQuery.trim($('#app_id').val());
    var reference_no = jQuery.trim($('#reference_no').val());

    if(app_type =="")
    {
      // document.getElementById('error_td').style.display=  '';
      $('#app_type_detail_error').html("<font color='red'>Please select Application Type</font>");
      $('#result_div').hide();

      err = err+1;
    }else
    {
      $('#app_type_detail_error').html(" ");
    }
    if(app_id == "")
    {
      // document.getElementById('error_td').style.display=  '';
      $('#detail_error').html("<font color='red'>Please enter  Application Id.</font>");
      $('#result_div').hide();

      err = err+1;
    }else if(isNaN(app_id)){
        $('#detail_error').html("<font color='red'>Please enter valid Application Id.</font>");
        $('#result_div').hide();
        err = err+1;        
    }
    else
    {
      $('#detail_error').html(" ");
    }
    if(reference_no == "")
    {
      // document.getElementById('error_td').style.display=  '';
      $('#reference_detail_error').html("<font color='red'>Please enter Reference Number.</font>");
      $('#result_div').hide();
      err = err+1;      
    }else if(isNaN(reference_no)){
        $('#reference_detail_error').html("<font color='red'>Please enter valid Reference Number.</font>");
        $('#result_div').hide();
        err = err+1;
    }
    else
    {
      $('#reference_detail_error').html(" ");
    }


    if(err != 0)
    {
      return false;
    }else{
            $('#result_div').show();
            $('#result_div').html('Please wait...');
            $('#result_div1').hide();
            var url = "<?php echo url_for('nis/validatePrint'); ?>";
            $.ajax({
            type: "POST",
            url: url,
            data: "app_type="+app_type+"&app_id="+app_id+"&reference_no="+reference_no,
            dataType: "json",
            success: function(data) {                
                if(data.flag == 'found'){
                    $('#result_div').html('');
                    if(app_type=='passport')
                    {                       
                        var printUrl = '<?php echo sfConfig::get('app_nis_portal_url').'passport/summary';?>/id/'+data.appId+'/read/1/popup/true';
                        openPopUp(printUrl); 
                    }
                    if(app_type=='visa')
                    {
                        var printUrl = '<?php echo sfConfig::get('app_nis_portal_url').'visa/printForm';?>/id/'+data.appId+'/read/1/popup/true';
                        openPopUp(printUrl); 
                    }
                    if(app_type=='freezone')
                    {       
                        var printUrl = '<?php echo sfConfig::get('app_nis_portal_url').'visa/printReEntryForm';?>/id/'+data.appId+'/read/1/popup/true';
                        openPopUp(printUrl); 
                    }
                    if(app_type=='vap')
                    {
                        var printUrl = "<?php echo url_for("visaArrival/printVisaApp"); ?>/id/"+data.appId;
                        openPopUp(printUrl);
                    }

                }else{
                    if(data.flag == 'paidfound')
                        {
                          if(data.email !=''){
                            $('#result_div').html('<font color="red">The Application Id and Reference Number entered are paid from <u>'+data.email+'</u> , please log-in with mentioned email address and take print out. </font>');
                            $('#result_div1').show();
                          }else{
                            $('#result_div').html('<font color="red">The Application Id and Reference Number entered are paid from another user , you can not take print out of this application. </font>');
                            $('#result_div1').show();
                          }
                            
                        }else {
                            //alert(data.email);
                            $('#result_div').html('<font color="red"> Application not Found</font>');
                            $('#result_div1').show();
                        }
                }
            }
        });

    }

  }
</script>


 <div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">
      <?php
use_helper('Pagination');
use_helper('Form');
include_partial('global/innerHeading',array('heading'=>'Print Application'));
?>
<?php
use_javascript('dhtmlwindow.js');
use_stylesheet('dhtmlwindow.css');
use_javascript('modal.js');
use_stylesheet('modal.css');
?>
       <div class="clear"></div>
    <div class="tmz-spacer"></div>
  <div class="clearfix"/>
    <?php echo form_tag('nis/generatePrint',array('name'=>'generate_print_form','class'=>'','onsubmit'=>'return validateForm()', 'method'=>'post','id'=>'generate_print_form')) ?>

   
      <?php
      $sf = sfContext::getInstance()->getUser();
      if($sf->hasFlash('notice')){ ?>
      <div id="flash_notice" class="alertBox" >
        <?php
        echo nl2br($sf->getFlash('notice'));
        ?>
      </div><br/>
      <?php }?>

    
        <?php
        $sf = sfContext::getInstance()->getUser();
        if($sf->hasFlash('error')){ ?>
        <div id="flash_error" class="alertBox" >
          <?php
          echo nl2br($sf->getFlash('error'));
          ?>
        </div><br/>
        <?php }?>

        <table width="100%">
          <?php
          $appId = (isset($_REQUEST['app_id'])) ? $_REQUEST['app_id'] : $_SESSION['app_id'];

          $referenceNo = (isset($_REQUEST['reference_no'])) ? $_REQUEST['reference_no'] : $_SESSION['reference_no'];
          if(sfConfig::get('app_visa_arrival_enable')=='true')
        {
            $appArray = array(''=>'Please Select Application Type','passport'=>'Passport','visa'=>'Visa','freezone'=>'Freezone', 'vap' => sfConfig::get('app_visa_arrival_title'));
        } else {
            $appArray = array(''=>'Please Select Application Type','passport'=>'Passport','visa'=>'Visa','freezone'=>'Freezone');
        }

          echo formRowComplete('Application Type<font color="red">*</font>',select_tag('app_type',options_for_select($appArray)),'','app_type','app_type_detail_error','app_type_row');
          echo "</tr>";
          //echo "<div id='mo_number_tr' style=''>";
          echo formRowComplete('Application Id<font color="red">*</font>',input_tag('app_id', $appId, array('size' => 15, 'maxlength' => 10,'class'=>'txt-input')),'','app_id','detail_error','app_id_row');
          echo "</tr>";
          echo formRowComplete('Reference Number<font color="red">*</font>',input_tag('reference_no', $referenceNo, array('size' => 15, 'maxlength' => 15,'class'=>'txt-input')), '','reference_no','reference_detail_error','reference_no_row');
          echo "</tr>";
          ?>
         <tr id="result_div1" style="display: none">
             <td colspan="2" height="30" valign="top" style="font-size: 12px;">
                    <div id="result_div" style="display: none;width:500px" align="center">Please Wait...</div></td>
            </tr>
          <tr valign="top" >
            <td height="30" valign="top">&nbsp;</td>
            <td height="30" valign="top">
                <input type="button" name="btnSearch" value="Search" onclick="validateForm(); " class="normalbutton" />
  
              </td>
          </tr>
        </table>

      
   
    
  </form>
  <br>
</div>
</div></div>
<div class="content_wrapper_bottom"></div>

