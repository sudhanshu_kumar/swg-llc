<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
    <div class="content_container">
        <?php
        use_helper('Form')
        /*
         * To change this template, choose Tools | Templates
         * and open the template in the editor.
         */
        ?>
        <?php include_partial('global/innerHeading', array('heading' => 'New NIS Application')); ?>
        <div class="clear"></div>
        <div class="tmz-spacer"></div>
        <div class="brdBox">
            <form action="" method="post" class="dlForm">
                <div id="show_error" class="error_list" align="center"></div>
                <table cellspacing="0" cellpadding="0" border="0" width="99%" id="new_table">
                    <tbody>
                        <tr>
                            <td class="blbar" colspan="2">New NIS Application</td>
                        </tr>
                        <tr>
                            <td width="30%" valign="top" id="audit_startDate_row"><label for="audit_startDate">Application Type <sup><font color="red">* </font></sup></label></td>
                            <td width="70%" valign="top"><?php echo select_tag('app_type', options_for_select($AppArray, null, array('include_custom' => '-- Select Application --'))); ?><br/>
                            <span class="red" id="err_app_type"></span> </td>
                        </tr>
                        <tr id="country_row" class="no_display">
                            <td width="30%" valign="top" id="audit_startDate_row"><label for="audit_endDate">Processing Country<sup><font color="red"><span id="interview_symbol_id">^</span>* </font></sup></label></td>
                            <td width="70%" valign="top"><?php echo select_tag('app_country', options_for_select()); ?><br/>
                                <span class="red" id="err_country_option"></span>
                            <div class="red" id="interview_text_id">^ Please select the country where you wish to attend the interview for your application processing.</div></td>
                        </tr>
                    </tbody>
                </table>

                <table cellspacing="0" cellpadding="0" border="0" width="99%" id="">
                    <tr>
                        <td class="" colspan="2" style="border-bottom:none;border-top:none;">
                            <div id="flash_notice" class="no_display"><br>
                                <div id="flash_notice_content"  class="highlight_new red">
                                    <!-- DUE TO THE HIGH INCIDENCE OF USE OF STOLEN CREDIT CARDS ON OUR PLATFORM, ALL PAYMENTS FOR APPLICATIONS FOR VISA AND PASSPORT SERVICES SHALL BE MADE ONLY WITH UNITED STATES POSTAL SERVICE MONEY ORDERS EFFECTIVE FROM MARCH 2, 2011. CREDIT/DEBIT CARD PAYMENTS ARE NO LONGER ACCEPTABLE. WE REGRET ANY INCONVENIENCE THIS MAY CAUSE OUR CUSTOMERS. -->
                                <?php echo sfConfig::get('app_mo_based_country_content'); ?> </div>
                            </div>

                        </td>
                    </tr>
                    <tr>
                        <td width="30%" style="border-right:none;border-top:none;"></td>
                        <td width="70%" style="border-left:none;border-top:none;">
                            <input type="hidden" name="isPrivilegeUser" id="isPrivilegeUser" value="<?php echo $isPrivilegeUser;?>" />
                            <input name="commit" value="Start Application" class="normalbutton" type="button" onclick="validateRequestForm()" id="btn_submit">
                            <div id="loading" class="no_display"><?php echo image_tag('/images/ajax-loader_1.gif',array('alt'=>'Loading ...', 'align' => 'absmiddle'));?><br/>&nbsp;&nbsp;Please wait...</div>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</div>
<div class="content_wrapper_bottom"></div>
<script>
    function inArray(first,arrayField)
    {
        var arrlength = arrayField.length;
        for(i=0;i<arrlength;i++)
        {
            if(first == arrayField[i])
            {
                return true;
            }
        }
        return false;
    }
</script>
<script>
    var cardTypeArray = new Object();
</script>
<?php foreach($cardType AS $key => $value){ ?>
<script>cardTypeArray["<?php echo $key; ?>"] = "<?php echo $value; ?>";</script>
<?php } ?>
<script>
    var nis_visa_url = '<?php echo url_for("nis/newVisa"); ?>';
    var nis_passport_url = '<?php echo url_for("nis/newPassport"); ?>';
    var freezone_passport_url = '<?php echo url_for("visa/newReEntryVisa"); ?>';
    var nis_vwp_url = '<?php echo url_for("nis/newVisaWaiver"); ?>';
    function validateRequestForm(){
        $("#err_country_option").html("");
        if($('#app_type').val() == '')
        {
            $("#err_app_type").html("Please Select Application");
            return false;
        }
        else
            $("#err_app_type").html("");
        if((($('#app_type').val() == 'p') || ($('#app_type').val() == 'v') || ($('#app_type').val() == 'vap')) && $('#app_country').val() == '')
        {
            $("#err_country_option").html("Please Select Country");
            return false;
        }
        // alert($('#app_type').val());
        if($('#app_type').val() == 'p')
            openPassportForm();
        if($('#app_type').val() == 'v')
            openVisaForm();
        if($('#app_type').val() == 'f')
            openfreeZoneForm();
        if($('#app_type').val() == 'vap')
            openVAPForm();
        return true;
    }

    function ShowCountry(){
        var screenMsgType;
        if(($('#app_type').val() == 'p') || ($('#app_type').val() == 'v') || $('#app_type').val() == 'vap'){
            $('#country_row').show();

            if($('#app_type').val() == 'vap'){
                $('#interview_symbol_id').hide();
                $('#interview_text_id').hide();
            }else{
                $('#interview_symbol_id').show();
                $('#interview_text_id').show();
            }


            if($('#flash_notice').is(':visible')){
                if($('#app_country').val() =='')
                {
                    $('#flash_notice').hide();
                }
                else
                {
                    $('#flash_notice').show();
                }
            }else{
                $('#flash_notice').hide();
            }
        }
        else if($('#app_type').val() == 'f'){
            $('#country_row').hide();
            $('#app_country').val(0);
            screenMsgType = '<?php echo sfConfig::get('app_jna_based_country_content'); ?>';
            $('#flash_notice_content').html(screenMsgType);
            if(!($('#isPrivilegeUser').val()))
            {
                $('#flash_notice').show();
            }
        }

        else{
            $('#country_row').hide();
            $('#app_country').val(0);
            $('#flash_notice').hide();
        }
    }

    // Start of change state
    //function getCountryDropDown(){
    $("#app_type").change(function(){
        var appType =  $(this).val();
        var url = "<?php echo url_for('nis/getCountry/'); ?>";
        //$("#app_country").load(url, {app_type: appType});
        $('#btn_submit').hide();
        $('#loading').show();
        $.ajax({
            type: "POST",
            url: url,
            data: {app_type: appType},
            success: function (data) {
                $("#app_country").html(data);                
                ShowCountry();
                $('#btn_submit').show();
                $('#loading').hide();
                $("#show_error").hide();
                $('#err_app_type').html('');
                $('#err_country_option').html('');
            }
        });
    });


    //}



    function openVisaForm() {

        // go to visa page
        location.href=nis_visa_url+'/country/'+$('#app_country').val();//+'/country/'+$('#app_country').val();

    }

    function openPassportForm() {

        // go to Passport page
        location.href=nis_passport_url+'/country/'+$('#app_country').val();

    }
    function openfreeZoneForm() {
        // go to Passport page
        location.href=freezone_passport_url
    }
    function openVAPForm() {

        // go to Visa Waiver page
        location.href= nis_vwp_url+'/country/'+$('#app_country').val();

    }


    /* This function remove the blank space...*/
    function trim(inputString)
    {
        inputString=inputString.replace(/^\s+/g,"");
        inputString=inputString.replace(/\s+$/g,"");
        return inputString;
    }//End of the trim function...

    /**
     * This function show hide the special notice for MO only...
     */
    $("#app_country").change(function(){
        var app_country = $("#app_country").val();
        var app_type =  $("#app_type").val();
        if(app_country != ''){
            if(app_type != 'vap'){
                var url = "<?php echo url_for('nis/getEmbassy/'); ?>";
                $.post(url,{app_country: app_country},function(data){
                    if(data){
                        $("#show_error").show();
                        $("#show_error").html(data);
                        $('#flash_notice').hide();
                        $('#btn_submit').hide();
                    }else{
                        showMsg(app_country);
                        $("#show_error").hide();
                        $('#btn_submit').show();
                    }
                });
            }else{
                showMsg(app_country);
                $("#show_error").hide();
                $('#btn_submit').show();
            }
        }else{
            $("#show_error").hide();
            $('#btn_submit').show();
            $('#flash_notice').hide();
        }
    });

    function showMsg(app_country){
        var isOther = false;
        var isMo = false;
        var isNmi = false;
        var isJna = false;
        var isWt = false;

        var data = '';
        if(cardTypeArray[app_country] == undefined){
            data = cardTypeArray['ALL'];
        }else{
            data = cardTypeArray[app_country];
        }

        if(data != undefined){
            var result = trim(data);
            if(result != ''){
                var cardType = result.split(",");
                var len = cardType.length;
                for(i=0;i<len;i++){
                    if(cardType[i] == 'Wt'){
                        isWt = true;
                    }else if(cardType[i] == 'Mo'){
                        isMo = true;
                    }
                    else if(cardType[i] == 'nmi_mcs' || cardType[i] == 'nmi_vbv' || cardType[i] == 'nmi_amx' || cardType[i] == 'A' || cardType[i] == 'M' || cardType[i] == 'V'){
                        isNmi = true;
                        isOther = true;
                    }
                    /**
                     * [WP: 111] => CR: 157
                     * Added condition of MasterCard SecureCode for JNA service...
                     */
                    else if(cardType[i] == 'jna_nmi_vbv' || cardType[i] == 'jna_nmi_mcs'){
                        isJna = true;
                    }
                    else{
                        isOther = true;
                    }
                }//End of for(i=0;i<len;i++){...

                /**
                 * Condition(s) changed to show message while application are filled
                 * Condition based on MO and Other payment options
                 */
                var screenMsgType = '';
                //if(isMo == true && isWt == false && isOther == false){
                //  screenMsgType = '<?php //echo sfConfig::get('app_mo_based_country_content'); ?>';
                //}else if(!isMo && isWt && !isOther){
                //  screenMsgType = '<?php //echo sfConfig::get('app_wt_based_country_content'); ?>';
                //}else
                //            if(!isMo && !isWt && isOther){
                //               screenMsgType = '<?php //echo sfConfig::get('app_other_based_country_content'); ?>';
                //            }

                if(isNmi && !isMo){
                    screenMsgType = '<?php echo sfConfig::get('app_other_based_country_content'); ?>';
                }
                //else if(isMo && isWt && !isOther){
                // screenMsgType = '<?php //echo sfConfig::get('app_mo_wt_based_country_content'); ?>';
                //}else
                else if(isMo && !isWt && isOther){
                    screenMsgType = '<?php echo sfConfig::get('app_mo_other_based_country_content'); ?>';
                }
                else if(isJna){
                    screenMsgType = '<?php echo sfConfig::get('app_jna_based_country_content'); ?>';
                }
                //else if(!isMo && isWt && isOther){
                //  screenMsgType = '<?php //echo sfConfig::get('app_wt_other_based_country_content'); ?>';
                //}else if(isMo && isWt && isOther){
                //  screenMsgType = '<?php //echo sfConfig::get('app_mo_wt_other_based_country_content'); ?>';
                //}
                else{
                    screenMsgType = '';
                }

                if(screenMsgType != ''){
                    $('#flash_notice_content').html(screenMsgType);
                    if(!($('#isPrivilegeUser').val()))
                    {
                        $('#flash_notice').show();
                    }
                }else{
                    $('#flash_notice').hide();
                }

            }//End of if(result != ''){...
        }
    }
</script>