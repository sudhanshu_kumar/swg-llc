<?php include_stylesheets_for_form($form) ?>
<?php include_javascripts_for_form($form) ?>
<?php //use_helper('Javascript');?>
<?php use_stylesheet('/css/sfPasswordStrengthStyle.css'); ?>
<script>
  function call()
  {
    document.forms[0].title.disabled = false;
    return false;
  }
</script>
<script>

  $(document).ready(function()
  {
    $('#sf_guard_user_password').keypress(function(){

      url ='<?php echo url_for('userAdmin/calculateStrength');?>';
      password=$('#sf_guard_user_password').val();
      $("#strength").load(url, {password: password,username:'',byPass:1 },function (data){
        if(data=='logout'){
          $("#strength").html('');
          location.reload();
        } });
    });
  });

</script>
<?php if(isset($firstLogin)): ?>
<?php include_partial('global/innerHeading',array('heading'=>'Update Password'));?>
<div class="global_content4 clearfix">
<?php echo ePortal_pagehead('',array()); ?>
<div class="clearfix"/>
  <form method="post" class="dlForm" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>


        <? else : ?>
        <form action="<?php echo url_for('userAdmin/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getid() : '')) ?>" method="post" class="dlForm" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
    <?php endif; ?>
    
    
    <fieldset class="multiform wd90">
           
           <legend>Change Password</legend>
                <div class="wrapForm2">
                    <?php if (!$form->getObject()->isNew()): ?>
                    <input type="hidden" name="sf_method" value="put" />
                    <?php endif; ?>
                    <?php echo $form ?>
                   
                </div>
                <div style="clear:both;"></div>
      
            
            <div class="mgauto">
                <input type="submit" onclick="" value="Change Password" class="normalbutton">
            </div>
            
            
        </form>
        <div style="clear:both;"></div>
    <table width="90%" style="margin:auto;">
      <tbody>
        
        <tr><td colspan="2"><?php include_partial('global/passwordPolicy') ?></td></tr>
      </tbody>

    </table>
    <?php
    //        echo observe_field('sf_guard_user_password', array(
    //                  'update'   => 'strength',
    //                  'url'      => 'userAdmin/calculateStrength',
    //                  'with'     => "$('sf_guard_user_password').serialize() ",
    //                  'script'   => true,
    //                  'frequency'=> 0.5,
    //        ));
    ?>
    <?php echo $form->renderGlobalErrors(); ?>
  

</div>
