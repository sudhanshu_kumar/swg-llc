<?php include_stylesheets_for_form($form) ?>
<?php include_javascripts_for_form($form) ?>
<script>
 function call()
 {
  document.forms[0].title.disabled = false;
  return false;
 }
</script>

<?php
$sf = sfContext::getInstance()->getUser();
if($sf->hasFlash('notice')){ ?>
    <div id="flash_notice" class="alertBox" >
<?php
    echo nl2br($sf->getFlash('notice'));
?>
</div><br/>
<?php }?>
<?php
if($sf->hasFlash('error')){ ?>
    <div id="flash_error" class="alertBox" >
<?php
    echo nl2br($sf->getFlash('error'));
?>
</div><br/>
<?php }?>
<?php if(isset($firstLogin)): ?>

<!--<form method="post" class="" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>> -->
<?php //echo ePortal_highlight('In order to continue please change your password and update your profile.','Welcome',array('class'=>'green'));?>

<? else : ?>

<form action="<?php echo url_for('userAdmin/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getid() : '')) ?>" method="post" class="dlForm" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
<?php endif; ?>
    <?php if (!$form->getObject()->isNew()): ?>
        <input type="hidden" name="sf_method" value="put" />
    <?php endif; ?>
     
    <?php //echo $form ?>
        <table cellpadding="0" cellspacing="0" border="0" width="99%">
            <tr>
              <td width="30%"><?php echo $form['details']['first_name']->renderLabel(); ?><span class="red">*</span></td>
              <td><?php echo $form['details']['first_name']->render(); ?> <br>
              <div class="red" id="first_name_error"> <?php echo $form['details']['first_name']->renderError(); ?> </div></td>
            </tr>
            <tr>
              <td width="30%"><?php echo $form['details']['last_name']->renderLabel(); ?></td>
              <td><?php echo $form['details']['last_name']->render(); ?> <br>
              <div class="red" id="first_name_error"> <?php echo $form['details']['last_name']->renderError(); ?> </div></td>
            </tr>
            <tr>
              <td width="30%"><?php echo $form['details']['email']->renderLabel(); ?><span class="red">*</span></td>
              <td><?php echo $form['details']['email']->render(); ?> <br>
              <div class="red" id="first_name_error"> <?php echo $form['details']['email']->renderError(); ?> </div></td>
            </tr>
            <tr>
              <td width="30%"><?php echo $form['username']->renderLabel(); ?><span class="red">*</span></td>
              <td><?php echo $form['username']->render(); ?> <br>
              <div class="red" id="first_name_error"> <?php echo $form['username']->renderError(); ?> </div></td>
            </tr>
            <tr>
              <td width="30%"><?php echo $form['password']->renderLabel(); ?><span class="red">*</span></td>
              <td><?php echo $form['password']->render(); ?> <br>
              <div class="red" id="first_name_error"> <?php echo $form['password']->renderError(); ?> </div></td>
            </tr>
            <tr>
              <td width="30%"><?php echo $form['confirm_password']->renderLabel(); ?><span class="red">*</span></td>
              <td><?php echo $form['confirm_password']->render(); ?> <br>
              <div class="red" id="first_name_error"> <?php echo $form['confirm_password']->renderError(); ?> </div></td>
            </tr>
            <tr>
              <td width="30%"><?php echo $form['is_active']->renderLabel(); ?><span class="red">*</span></td>
              <td><?php echo $form['is_active']->render(); ?> <br>
              <div class="red" id="first_name_error"> <?php echo $form['is_active']->renderError(); ?> </div></td>
            </tr>
            <tr>
              <td width="30%"><?php echo $form['groups_list']->renderLabel(); ?><span class="red">*</span></td>
              <td><?php echo $form['groups_list']->render(); ?> <br>
              <div class="red" id="first_name_error"> <?php echo $form['groups_list']->renderError(); ?> </div></td>
            </tr>
            <tr><td></td><td> <?php //echo button_to('Cancel','userAdmin/index') ?>
              <input type="submit" value="<?php echo ($form->getObject()->isNew() ? 'Create New User' : 'Update User') ?>" onclick="call();" class="normalbutton" /></td></tr>
        </table>
        <div class="clear">&nbsp;</div>
      <table width="99%" cellpadding="0" cellspacing="0" border="0">
            <tbody>
                <!--tr><td colspan="2">      <p class="red">*Required Information</p></td></tr-->
          <tr>
            <td colspan="2"><?php include_partial('global/smallPasswordPolicy') ?></td>
          </tr>
            </tbody>
        </table>
   
             
     

   <?php echo $form->renderGlobalErrors(); ?>
   <?php //echo $form['details']->renderError(); ?>
</form>
  </div></div><div class="content_wrapper_bottom"></div>
