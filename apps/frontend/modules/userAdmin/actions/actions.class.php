<?php

/**
 * userAdmin actions.
 *
 * @package    symfony
 * @subpackage userAdmin
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12474 2008-10-31 10:41:27Z fabien $
 */
class userAdminActions extends sfActions {
  public function executeIndex(sfWebRequest $request) {
    if($this->getUser()->isAuthenticated()) {
            return $this->redirect('@home');
        }
    $this->form = new LoginForm();
  }


  public function executeChangePassword(sfWebRequest $request) {
    if($this->getUser()->isFirstLogin($this->getUser()->getGuardUser())) {
      $this->firstLoginUser='yes';
    }else {
      $this->firstLoginUser='no';
    }

    $this->form = new ChangeUserForm($this->getUser()->getGuardUser());

    $this->setTemplate('change');
  }
  public function executeCalculateStrength()
  {
    $strength = PasswordStrength::calculate(
      $this->getRequestParameter('password'),
      $this->getRequestParameter('username'),
      $this->getRequestParameter('byPass')
    );
    return $this->renderText(PasswordStrength::getBar($strength));
  }
  //save the new password
  public function executeSavechange(sfWebRequest $request) {
    $this->forward404Unless($request->isMethod('post') || $request->isMethod('put'));
    $this->form = new ChangeUserForm($this->getUser()->getGuardUser());
    #set last login time
    //$this->form->setDefault('last_login',date('Y-m-d h:i:s'));

    if($this->getUser()->isFirstLogin($this->getUser()->getGuardUser())) {
      $this->firstLoginUser='yes';
      $event_description = EpAuditEvent::$MSG_SUBCATEGORY_SECURITY_FIRSTTIME_PASSWORD_CHANGE;
    }else {
      $this->firstLoginUser='no';
      $event_description = EpAuditEvent::$MSG_SUBCATEGORY_SECURITY_PASSWORD_CHANGE;
    }

    if ($this->processForm($request, $this->form, false)) {

      $userDetailsObj = $this->getUser()->getGuardUser();
      $applicationArr = array(new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_USERINFO,$userDetailsObj->getUsername(),$userDetailsObj->getId()));

      $eventHolder = new pay4meAuditEventHolder(
        EpAuditEvent::$CATEGORY_SECURITY,
        EpAuditEvent::$SUBCATEGORY_SECURITY_PASSWORD_CHANGE,
        EpAuditEvent::getFomattedMessage($event_description, array('username'=>$userDetailsObj->getUsername())),
        $applicationArr);

      $this->dispatcher->notify(new sfEvent($eventHolder, 'epAuditEvent'));

      $this->getUser()->setFlash('notice',"Password Changed Successfully",false);

      $this->forward($this->getModuleName(), 'changePassword');
    }

    $this->setTemplate('change');
  }

  protected function processForm(sfWebRequest $request, sfForm $form, $redirect=true) {
    $form->bind($request->getParameter($form->getName()));
    if ($form->isValid()) {
      $postArr = $request->getPostParameters();
      $sf_guard_user = $this->getUser()->getGuardUser();
      if(EpPasswordPolicyManager::checkPasswordComplexity($postArr['sf_guard_user']['password'])){
        $resVal =  EpPasswordPolicyManager::checkPreviousPasswords($sf_guard_user->getId(),$postArr['sf_guard_user']['password'],$sf_guard_user->getSalt(), $sf_guard_user->getAlgorithm());
        if($resVal){
          $oldPasswordLimit = EpPasswordPolicyManager::getOldPasswordLimit();
          $this->getUser()->setFlash('error', 'New password can not be same as last '.$oldPasswordLimit.' password.',true);
          $this->redirect('@change_password');
        }else{
          $form->save();
          $sf_guard_user = $this->getUser()->getGuardUser();

          EpPasswordPolicyManager::doChangePassword($sf_guard_user->getId(),$sf_guard_user->getPassword());
        }
      }
      else{
        $this->getUser()->setFlash('error', ' Password should have atleast 1 special character, 1 numeric character and 1 alphabetic character.',true);
        $this->redirect('@change_password');
      }
      if ($redirect)
      $this->forward('userAdmin','edit');
      return true;
    }
    //sfContext::getInstance()->getLogger()->info("Form invalid");
    return false;
  }

  public function executeChangeFirstPassword(sfWebRequest $request) {
    //die('First Change Password');

    $user_id = sfContext::getInstance()->getUser()->getGuardUser()->getId();
    $this->forward404Unless($sf_guard_user = Doctrine::getTable('sfGuardUser')->find(array($user_id)), sprintf('Object sf_guard_user does not exist (%s).', array($user_id)));
    $this->form = new EditFirstProfileForm($sf_guard_user);
    $this->group_description = $this->getUser()->getGroups()->getFirst()->getDescription();
    if($request->isMethod('put')) {
      $this->form->bind($request->getParameter($this->form->getName()));
      if ($this->form->isValid()) {
        $postArr = $request->getPostParameters();
        if(EpPasswordPolicyManager::checkPasswordComplexity($postArr['sf_guard_user']['password'])){
          $resVal =  EpPasswordPolicyManager::checkPreviousPasswords($sf_guard_user->getId(),$postArr['sf_guard_user']['password'],$sf_guard_user->getSalt(), $sf_guard_user->getAlgorithm());
          if($resVal){
            $oldPasswordLimit = EpPasswordPolicyManager::getOldPasswordLimit();
            $this->getUser()->setFlash('error', 'New password can not be same as last '.$oldPasswordLimit.' password.');
            $this->redirect('@change_first_password');
          }else{
            $sf_guard_user = $this->form->save();

            EpPasswordPolicyManager::doChangePassword($sf_guard_user->getId(),$sf_guard_user->getPassword());

            $event_description = EpAuditEvent::$MSG_SUBCATEGORY_SECURITY_FIRSTTIME_PASSWORD_CHANGE;
            $applicationArr = array(new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_USERINFO,$sf_guard_user->getUsername(),$sf_guard_user->getId()));

            $eventHolder = new pay4meAuditEventHolder(
              EpAuditEvent::$CATEGORY_SECURITY,
              EpAuditEvent::$SUBCATEGORY_SECURITY_PASSWORD_CHANGE,
              EpAuditEvent::getFomattedMessage($event_description, array('username'=>$sf_guard_user->getUsername())),
              $applicationArr);

            $this->dispatcher->notify(new sfEvent($eventHolder, 'epAuditEvent'));

            # should go to signout and ask user to relogin.
            $this->getUser()->setFlash('notice',"<b><font color='green'>Profile Updated Successfully! you need to login again.</font></b>",true);
            $this->forward('sfGuardAuth','signout');
            return true;
          }
        } else{
          $this->getUser()->setFlash('error', ' Password should have atleast 1 special character, 1 numeric character and 1 alphabetic character.',true);
          $this->redirect('@change_first_password');
        }
      }

    }
    $this->form->setDefault('last_login', date('Y-m-d h:i:s'));
    $this->setVar('firstLogin','true');
    $this->setTemplate('edit');
  }

  protected function chk_username($username) {
    return $username_count = Doctrine::getTable('sfGuardUser')->createQuery('a')->where('username=?',$username)->count();

  }

      public function executeNewSupportUser(sfWebRequest $request)
      {
          $this->form = new NewSupportUserForm();
      }

      public function executeCreate(sfWebRequest $request)
      {

          $this->forward404Unless($request->isMethod('post') || $request->isMethod('put'));

          $this->form = new NewSupportUserForm();

          $this->form->bind($request->getParameter($this->form->getName()));
          $arrPost = $request->getPostParameters();
          if($request->isMethod('post')){
              $this->username = $arrPost['sf_guard_user']['username'];
              $username_count = Doctrine::getTable('sfGuardUser')->chk_username($this->username);
              if($username_count != 0){
                  $this->getUser()->setFlash('error',"User name already exist!!");
                  $this->redirect('userAdmin/newSupportUser');
              }
              if ($this->form->isValid())
              {
                $postArr = $request->getPostParameters();
                $sf_guard_user = $this->getUser()->getGuardUser();
                if (EpPasswordPolicyManager::checkPasswordComplexity($postArr['sf_guard_user']['password'])) {
                          $sf_guard_user = $this->form->save();
                          $this->getUser()->setFlash('notice',"User Successfully created!!");
                          $this->redirect('userAdmin/newSupportUser');

                } else {
                    $this->getUser()->setFlash('error', ' Password should have atleast 1 special character, 1 numeric character and 1 alphabetic character.', true);                    
                    $this->redirect('userAdmin/newSupportUser');
                }


              }else{
                  
              }
          }
          $this->setTemplate('newSupportUser');
      }

//      public function executeEditSupportUser(sfWebRequest $request)
//      {
//          $userObj = Doctrine::getTable("sfGuardUser")->find($userId);
//          $this->form = new NewSupportUserForm($userObj);
//
//      }
}
