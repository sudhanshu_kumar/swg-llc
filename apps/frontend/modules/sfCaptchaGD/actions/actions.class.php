<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
require_once(sfConfig::get('sf_plugins_dir').'/sfCaptchaGDPlugin/lib/sfCaptchaGD.class.php');
class sfCaptchaGDActions extends sfActions      
{
  /**
   * Output captcha image
   *
   */
  public function executeGetimage()
  {
    $captcha = new sfCaptchaGD();
    $captcha->generateImage($this->getUser()->getAttribute('captcha', NULL));
    return sfView::NONE;
  }
}

?>
