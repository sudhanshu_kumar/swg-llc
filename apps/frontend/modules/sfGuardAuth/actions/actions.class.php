<?php

/*
 * This file is part of the symfony package.
 * (c) 2004-2006 Fabien Potencier <fabien.potencier@symfony-project.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

require_once(sfConfig::get('sf_plugins_dir') . '/sfDoctrineGuardPlugin/modules/sfGuardAuth/lib/BasesfGuardAuthActions.class.php');

/**
 *
 * @package    symfony
 * @subpackage plugin
 * @author     Fabien Potencier <fabien.potencier@symfony-project.com>
 * @version    SVN: $Id: actions.class.php 23319 2009-10-25 12:22:23Z Kris.Wallsmith $
 */
class sfGuardAuthActions extends BasesfGuardAuthActions {

    public function executeSignout($request) {
        $cartObj = new cartManager();
        $cartObj->emptyCart();
        if ($this->getUser() && $this->getUser()->isAuthenticated()) {
            $uname = sfContext::getInstance()->getUser()->getGuardUser()->getUsername();
        } else {
            return sfContext::getInstance()->getController()->redirect('@homepage');
        }

        sfContext::getInstance()->getUser()->getAttributeHolder()->remove('menu');
        /* Added by uday to logout from google and yahoo */
//        $loginType = 'pay4me';
//        $idenUrl = $this->getUser()->getGuardUser()->getUsername();
//        $chkGoogleType = strpos($idenUrl, 'www.google.com');
//        if ($chkGoogleType != "") {
//            $loginType = 'google';
//        }
//        $chkYahooType = strpos($idenUrl, 'me.yahoo.com');
//        if ($chkYahooType != "") {
//            $loginType = 'yahoo';
//        }
//        $chkAolType = strpos($idenUrl, 'www.aol.com');
//        if ($chkAolType != "") {
//            $loginType = 'aol';
//        }
//        $chkFacebookType = strpos($idenUrl, 'facebook');
//        if ($chkFacebookType != "") {
//            $loginType = 'facebook';
//            $facebookAccessToken = $this->getUser()->getAttribute('facebookAccessToken');
//        }

        $idenUrl = $this->getUser()->getGuardUser()->getUsername();
        ## Fetching login type...
        $loginType = strtolower(Settings::getLoginType($idenUrl));

        if($loginType == 'facebook'){
            $facebookAccessToken = $this->getUser()->getAttribute('facebookAccessToken');
        }

        /* End of code Added by uday to logout from google and yahoo */
        $this->getUser()->getAttributeHolder()->remove('loginOnOrderRequest');
        $eventHolder = new pay4meAuditEventHolder(
                        EpAuditEvent::$CATEGORY_SECURITY,
                        EpAuditEvent::$SUBCATEGORY_SECURITY_LOGOUT,
                        EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_SECURITY_LOGOUT, array('username' => $uname)),
                        null);
        $this->dispatcher->notify(new sfEvent($eventHolder, 'epAuditEvent'));
        $EpDBSessionDetailObj = new EpDBSessionDetail();
        $EpDBSessionDetailObj->doCleanUpSessionByUser();
        $groupArr = $this->getUser()->getGroupNames();
        $this->getUser()->signOut();
        $signout_url = '';
        $this->getUser()->setAttribute('loginType', $loginType);

        if($loginType == 'facebook'){
            $this->getUser()->setAttribute('facebookAccessToken', $facebookAccessToken);
        }
        
        //    $this->redirect('' != $signout_url ? $signout_url : '@super_admin_signin');
        if (in_array(sfConfig::get('app_ipfm_role_payment_user'), $groupArr)) {            
                $this->redirect('' != $signout_url ? $signout_url : '@homepage');            
        } else {
            $this->redirect('' != $signout_url ? $signout_url : '@adminhome');
        }
    }

    public function executeSignin($request) {
        $user = $this->getUser();

        if ($user->isAuthenticated()) {
            $this->getUser()->signOut();
            return $this->renderText("<script>window.parent.location = '" . $this->generateUrl('default', array('module' => 'pages',
                        'action' => 'index')) . "'</script>");
        }
        $class = sfConfig::get('app_sf_guard_plugin_signin_form', 'sfGuardFormSignin');

        $this->form = new LoginForm();
        if ($request->isXmlHttpRequest()) {
            $this->getResponse()->setHeaderOnly(true);
            $this->getResponse()->setStatusCode(401);

            return sfView::NONE;
        }
        if ($request->isMethod('post')) {
            $postData = $request->getParameter('signin');

            $uname = $postData['username'];
            /* if($this->redirectOnFailedAttempt($request->getReferer(),$uname)==false){
              return $this->redirect('@adminhome');
              } */
            if ($this->getUserSessionStatus($uname) == false) {
                $this->redirect('@adminhome');
            }

            $this->form->bind($request->getParameter('signin'));

            $user = Doctrine::getTable('sfGuardUser')->findByUsername('admin');
            // $this->context->getUser()->signin($user->getFirst());



            if ($this->form->isValid()) {
                $values = $this->form->getValues();
                $user = Doctrine::getTable('sfGuardUser')->findByUsername($values['username']);
                $remember = isset($values['remember']) ? $values['remember'] : false;
                $this->getUser()->signin($user->getFirst(), $remember);
                if ($request->hasParameter('order') && ($request->getParameter('order') != "")) {
                    if ($this->getUser()->getAttribute('loginOnOrderRequest')) {
                        $this->getUser()->getAttributeHolder()->remove('loginOnOrderRequest');
                        return $this->renderText("<script>window.parent.location = '" . $this->generateUrl('default', array('module' => 'paymentProcess',
                                    'action' => 'orderDetails', 'order' => $request->getParameter('order'))) . "'</script>");
                    } else {
                        $this->getUser()->signOut();
                        return $this->renderText("<script>window.parent.location = '" . $this->generateUrl('default', array('module' => 'pages',
                                    'action' => 'index')) . "'</script>");
                    }
                } else {
                    // $this->auditAdminSignin($this->getUser()->getGuardUser()->getUsername(),$this->getUser()->getGuardUser()->getId());
                    return $this->redirect('@home');
                }
            } else {
                if (!$request->hasParameter('order')) {
                    if ($this->form->hasErrors()) {
                        $errors = "";
                        $errors .= $this->updateInformationForInvalidLogin($uname, $request) . "</br>";

                        $this->getUser()->setFlash('error', $errors, true);
                    }
                    return $this->redirect('@adminhome');
                }
            }
        }
    }

    public function executeChangePassword(sfWebRequest $request) {
        $this->form = new ChangeUserForm($this->getUser()->getGuardUser());

        $this->setTemplate('change');
    }

    //change the password
    public function executeChange(sfWebRequest $request) {
        $this->forward404Unless($sf_guard_user = Doctrine::getTable('sfGuardUser')->find(array($request->getParameter('id'))), sprintf('Object sf_guard_user does not exist (%s).', array($request->getParameter('id'))));
        $this->form = new ChangeUserForm($sf_guard_user);
    }

    //save the new password
    public function executeSavechange(sfWebRequest $request) {
        $this->forward404Unless($request->isMethod('post') || $request->isMethod('put'));
        $this->form = new ChangeUserForm($this->getUser()->getGuardUser());

        if ($this->processForm($request, $this->form, false)) {
            $this->getUser()->setFlash('notice', "Password Changed Successfully", false);
            $this->forward($this->getModuleName(), 'changePassword');
        }
        $this->setTemplate('change');
    }

    protected function processForm(sfWebRequest $request, sfForm $form, $redirect=true) {
        $arr = $request->getParameter($form->getName());
        $form->bind($arr);
        if ($form->isValid()) {
            $sf_guard_user = $form->save();
            return true;
        }
        //sfContext::getInstance()->getLogger()->info("Form invalid");
        return false;
    }

    //redirect user to login page if blocked
    function redirectOnFailedAttempt($requestReferer, $uname) {
        if ($uname == '') {
            $errors = "<ul class='error_list'><li>Username can't be empty.</li>";
            $this->getUser()->setFlash('error', $errors);
            $this->redirect($requestReferer);
            return false;
        }

        $numberOfContinuousAttemptObj = Doctrine::getTable('UserDetail')->getFailedAttempts($uname);
        if ($numberOfContinuousAttemptObj != false) {
            $countFailedAttempt = $numberOfContinuousAttemptObj->getFailedAttempt();
            // TODO - have max failure attempt in app.yml
            if ($countFailedAttempt == sfConfig::get('app_number_of_failed_attempts_for_blocked_user')) {
                $errors = "<ul class='error_list'><li>This Username is blocked. Please contact to Admin.</li>";
                $this->getUser()->setFlash('error', $errors);
                // $this->redirect($requestReferer);
                return false;
            }
            return true;
        } else {
            $errors = "<ul class='error_list'><li>Insufficient User detail.</li>";
            $this->getUser()->setFlash('error', $errors);
            //$this->redirect($requestReferer);
            return false;
        }
    }

    private function updateInformationForInvalidLogin($uname, $request) {
        //$this->logInvalidPasswordAndSECAnswerAuditDetails($uname,$bname);
        //set password and answer to null as it was a failed attempt to login
        //    $data = $request->getParameter($this->form->getName());
        //
        //    $data['password'] = '';
        //    $this->form->bind($data);
        ##start block user functionality
        //if username is exist in database but password mismatched or ip mismatched then block user
        //if user not able to login with in max_failure_attempts attempt(continuously)
        $errors = "";
        $returnVal = $this->updateFailedAttempt($uname);
        if ($returnVal == 'user_blocked') {
            $errors = "<ul class='error_list'><li>This Username is blocked. please contact to Admin.</li>";
            //only send in case of bank teller(bank user) to bank admin.
            // mail send only if user is a bank user(bank teller)
            // $this->mailingNotification('user_blocked',array('bankname'=>$bname,'username'=>$uname,'subject'=>'Pay4Me - User Block Notification'));
        } else if ($returnVal != 'continue') {
            $errors = "<ul class='error_list'><li>you did $returnVal failed attempts out of " . sfConfig::get('app_number_of_failed_attempts_for_blocked_user') . ".</li>";
        }

        foreach ($this->form as $elm) {
            $errors .= $elm->renderError();
        }
        return $errors;
    }

    //check no of attempt for the day, if >max_failure_attempts redirect to signin page with proper message
    function updateFailedAttempt($uname) {
        if (trim($uname) != "") {
            $numberOfContinuousAttemptObj = Doctrine::getTable('UserDetail')->getFailedAttempts($uname);
            $countFailedAttempt = 0;
            if ($numberOfContinuousAttemptObj != false) {
                $countFailedAttempt = $numberOfContinuousAttemptObj->getFailedAttempt();
                if ($countFailedAttempt == sfConfig::get('app_number_of_failed_attempts_for_blocked_user')) {
                    //return to logout message
                    //return false if number of attempts are =max_failure_attempts, false represent the user as block user for 24 hours
                    return 'user_blocked';
                } else {
                    if ($numberOfContinuousAttemptObj) {
                        //update failed attempts for the day
                        $numberOfContinuousAttemptObj->setFailedAttempt(($countFailedAttempt + 1));
                        $numberOfContinuousAttemptObj->save();
                        if ($numberOfContinuousAttemptObj->getFailedAttempt() == sfConfig::get('app_number_of_failed_attempts_for_blocked_user')) {
                            // Audit trail code
                            //Log audit Details
                            $this->logUserBlockAuditDetails($uname, $numberOfContinuousAttemptObj->getUserId());
                            return 'user_blocked';
                        }
                        return $numberOfContinuousAttemptObj->getFailedAttempt();
                    } else {
                        return 'continue';
                    }
                }
            }
            else
                return 'continue';
        }else {
            return 'continue';
        }
    }

    public function logUserBlockAuditDetails($uname, $id) {
        //Log audit Details
        $eventHolder = new pay4meAuditEventHolder(
                        EpAuditEvent::$CATEGORY_TRANSACTION,
                        EpAuditEvent::$SUBCATEGORY_TRANSACTION_USER_BLOCKED,
                        EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_TRANSACTION_USER_BLOCKED, array('username' => $uname)),
                        null,
                        $id, $uname
        );
        $this->dispatcher->notify(new sfEvent($eventHolder, 'epAuditEvent'));
    }

    //  public function  auditAdminSignin($username,$id) {
    //    //Log audit Details
    //    $eventHolder = new pay4meAuditEventHolder(
    //      EpAuditEvent::$CATEGORY_SECURITY,
    //      EpAuditEvent::$SUBCATEGORY_SECURITY_LOGIN,
    //      EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_SECURITY_LOGIN, array('username' => $username)),
    //      null,
    //      $id, $username
    //      );
    //
    //    sfContext::getInstance()->getEventDispatcher()->notify(new sfEvent($eventHolder, 'epAuditEvent'));
    //  }
    private function getUserSessionStatus($userName) {
        $EpDBSessionDetailObj = new EpDBSessionDetail();
        $status = $EpDBSessionDetailObj->isSessionActiveByUsername($userName);
        if ($status == false) {
            $errors = "<ul class='error_list'><li> " . ucwords($userName) . " is already logged in from another terminal.</li>";
            $this->getUser()->setFlash('error', $errors);
            $this->redirect('@adminhome');
            return false;
        }else
            return true;
    }
}
