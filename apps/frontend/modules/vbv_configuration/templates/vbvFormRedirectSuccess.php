<?php //echo ePortal_pagehead($pageTitle); ?>

<!--<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
<META HTTP-EQUIV="Expires" CONTENT="-1">
<title>Requesting for transaction </title>
</head>
<body topmargin="0" leftmargin="0" >
-->
<?php include_partial('global/innerHeading',array('heading'=>'Order Confirmation'));?>
<?php include_partial('orderDetail', array('request_details'=>$request_details)) ?>
<?php

$sf = sfContext::getInstance()->getUser();
if($sf->hasFlash('notice')){
  ?><div id="flash_notice" class="alertBox" ><?php
  echo nl2br($sf->getFlash('notice'));
  ?></div><?php
}
if($sf->hasFlash('error')){
  ?><div id="flash_notice" class="alertBox" ><?php
  echo nl2br($sf->getFlash('error'));
  ?></div><?php
}
?>
<iframe frameborder="0" width="100%" name="vbv" target="_parent" height="760px" src="<?php echo url_for('vbv_configuration/vbvRedirect');?>"></iframe>

<div id="vbvBgDiv">
  <p><font size="3" color="red">In case of any query related to card please contact to given visa contacts.</font></p>
  <div id="vbvContactDiv">
    <div id="addVbVDiv">
      <font size="2"><b>Address:</b> 3, Idowu Taylor Street,P.O. Box 70767,Victoria Island,Lagos,Nigeria.</font>
    </div>
    <div id="contVbVDiv">
      <font size="2"><b>Telephone:</b> +234 1 2703010 - 14 <br />
        <b>Fax:</b> +234 1 2703011<br />
      <b>EMail:</b> info@valucardnigeria.com<br /></font>
    </div>
</div></div>

<form name ="mF" id="mF" action='<?php echo $retObj[0]['post_url']; ?>' method="post" target="vbv">
  <input type="hidden" name="merid" value='<?php echo $retObj[0]['mer_id']; ?>'>
  <input type="hidden" name="acqBIN" value='<?php echo $retObj[0]['acqbin']; ?>'>
  <input type="hidden" name="OrderID" value='<?php echo $retObj[0]['order_id']; ?>'>
  <input type="hidden" name="PurchaseAmount" value='<?php echo $retObj[0]['purchase_amount']; ?>'>
  <input type="hidden" name="VisualAmount" value='<?php echo $retObj[0]['visual_amount'];?>'>
  <input type="hidden" name="Currency" value='<?php echo $retObj[0]['currency']; ?>'>
  <input type="hidden" name="ReturnURLApprove" value='<?php echo $retObj[0]['ret_url_approve']; ?>'>
  <input type="hidden" name="ReturnURLDecline" value='<?php echo $retObj[0]['ret_url_decline']; ?>'>
</form>
<script type='text/javascript'>document.forms[0].submit();document.forms[0].style.display='none';</script>
<!--</body>
</html>
-->
