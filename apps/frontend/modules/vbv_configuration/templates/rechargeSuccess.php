<?php
use_javascript('dhtmlwindow.js');
use_stylesheet('dhtmlwindow.css');
use_javascript('modal.js');
use_stylesheet('modal.css');

?>

 
<?php 

include_partial('global/innerHeading',array('heading'=>'Recharge Confirmation'));?>
 
    <?php
   
//echo "<br/>";
$sf = sfContext::getInstance()->getUser();
if($sf->hasFlash('notice')){
  ?><div id="flash_notice" class="alertBox" ><?php
  echo nl2br($sf->getFlash('notice'));

  ?></div><?php }?>

<div class="wrap870">

<?php include_partial('paymentGateway/rechargeDetail', array('request_details'=>$request_details)) ?>
<?php //include_partial('orderDetail', array('request_details'=>$request_details)) ?>

   
</div>

<div align="center">


        <input type="hidden" name="rechargeRequestId" id="rechargeRequestId" value="<?php echo $requestId ?>">
        <input type="hidden" name="amount" id="amount" value="<?php echo $request_details->getAmount() ?>">

        


</div>

<div id="payment_form">

</div>
<input type="hidden" id="refreshed" value="no">

<script>
    
      $(document).ready(function()
        {
        $('#btn_continue').hide();
        var url = "<?php echo url_for('vbv_configuration/vbvRecharge');?>";
        var rechargeRequestId = $('#rechargeRequestId').val();
        $("#payment_form").load(url, {rechargeRequestId:rechargeRequestId}, function (data){

        });

    });

</script>
