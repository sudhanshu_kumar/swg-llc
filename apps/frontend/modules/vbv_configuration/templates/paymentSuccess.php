<?php
use_javascript('dhtmlwindow.js');
use_stylesheet('dhtmlwindow.css');
use_javascript('modal.js');
use_stylesheet('modal.css');
use_javascript('countdown.js');
?>


<?php include_partial('global/innerHeading', array('heading' => 'Payment Confirmation')); ?>

<?php
//echo "<br/>";
$sf = sfContext::getInstance()->getUser();
if ($sf->hasFlash('notice')) {
?><div id="flash_notice" class="alertBox" ><?php
    echo nl2br($sf->getFlash('notice'));
?></div><?php } ?>

<?php if ($sf_request->hasParameter('display_msg')) {
    echo "<div id='flash_notice' class='alertBox' >Only Visa Card Accepted</div>";
} ?>

<div class="wrap870">

<?php
include_partial('paymentGateway/orderDetail', array('request_details' => $request_details, 'application_type' => $application_type))
?>
    <?php //include_partial('orderDetail', array('request_details'=>$request_details)) ?>

    <div align="center" class="redBigMessage">Only Verified by VISA cards are accepted. <a style="font-size: 15px; font-weight: bold;" target="_blank" href="http://usa.visa.com/personal/security/index.html">Click here </a>for more details.</div>
    <?php if ($sf->hasFlash('notice')) { ?>
    <div align="left" class="redBigMessage"><a style="font-size: 15px; font-weight: bold;"  href="<?php echo url_for('vbv_configuration/lastTransactionStatus') ?>">Click here </a> to check your last transaction status.</div>
    <?php } ?>
</div>
<div align="center">
    <input type="hidden" name="request_id" id="request_id" value="<?php echo $requestId ?>">
    <input type="hidden" name="amount" id="amount" value="<?php echo $request_details->getAmount() ?>">
<?php if (isset($payStatus) && 0 != $payStatus) { ?>
        <input type="button" name="btn_continue" id="btn_continue" value="Continue" class="paymentbutton" onclick='continuePayment()'>
        <div id="loader" align="center"></div>
    <?php
    }
    ?>

</div>
<br/>

<div id="payment_form">

</div>
<input type="hidden" id="refreshed" value="no">

<script>

    function continuePayment(){
        $('#btn_continue').hide();
        $('#loader').html('<?php echo image_tag('../images/ajax-loader.gif'); ?>');
        var url = "<?php echo url_for('vbv_configuration/vbvForm'); ?>";
        var requestId = $('#request_id').val();
        $("#payment_form").load(url, {requestId:requestId}, function (data){
            var redirectTime = document.getElementById('hdn_time').value;
            setTimeout('goToElseWhere()',redirectTime);
            $('#loader').html('');
        });
    }

    function goToElseWhere( )
    {
        window.location ='<?php echo url_for('vbv_configuration/payment') . '/requestId/' . $requestId . '/failure/1' ?>';
    }


</script>
