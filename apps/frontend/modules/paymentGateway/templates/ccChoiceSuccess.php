<?php
//use_javascript('dhtmlwindow.js');
//use_stylesheet('dhtmlwindow.css');
//use_javascript('modal.js');
//use_stylesheet('modal.css');

?>
<script>function openInformationWindow(){
        var url = '<?php echo url_for('paymentGateway/openInfoBox') ?>';
        emailwindow=dhtmlmodal.open('EmailBox', 'iframe', url, 'Confirmation Message', 'width=990px,height=235px,center=1,border=0, scrolling=auto');

        // emailwindow=dhtmlmodal.open('EmailBox', 'iframe', 'abc', 'Confirmation Message', 'width=350px,height=200px,center=1,resize=0,scrolling=1')

        //        emailwindow.onclose=function(){
        //            var theform=this.contentDoc.forms[0]
        //            var theemail=this.contentDoc.getElementById("info_check")
        //            if (!theemail.checked){
        //                alert("Please confirm that you have read the information by seleting the checkbox");
        //                return false
        //            }
        //            else{
        //                //         var url = "createSession";
        //                //         $('#pageHead').load(url,
        //                //          {
        //                //            check: 1
        //                //          },null
        //                //         );
        //                return true;
        //            }
        //        }
    }
    function openPaymentOptionWindow(){
        var url = '<?php echo url_for($this->getModuleName().'/openPaymentOptionBox?requestId='.$requestId) ?>';
        emailwindow=dhtmlmodal.open('EmailBox', 'iframe', url, 'Confirmation Message', 'width=530px,height=335px,center=1,border=0, scrolling=no');

    }

  function imposeMaxLength(Object, MaxLen)
  {
    return (Object.value.length <= MaxLen);
  }



    
</script>
<?php
$sf = sfContext::getInstance()->getUser();
if('fail' != $sf_request->getParameter('paystatus') &&  'failure' != $sf_request->getParameter('paystatus'))
        {
            $is_ewallet_active = Settings::isEwalletActive();
            if($this->getActionName() == "ccChoice" && !$sf->hasFlash('notice') && $is_ewallet_active) {
                if($userdetails['kyc_status'] == '0' && $group[0] == 'payment_group') {
                    echo "<script>window.onload = openInformationWindow();</script>";
                }
                if($userdetails['kyc_status'] == '1') {
                    echo "<script>window.onload = openPaymentOptionWindow();</script>";
                }
            }
        }
?>
<?php use_helper('Form');?>
<?php // use_helper('DateForm');?>
<?php //include_javascripts('jquery-1.2.6.js') ?>
<?php //include_javascripts('jquery-ui.min.js') ?>
<?php $vbv_active  = settings::isVbvActive();
      $nmi_active  = settings::isNmiActive();
?>

<!--<div class="global_content4 clearfix">-->
<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">
<?php include_partial('global/innerHeading',array('heading'=>'Payment Options'));?>
  <div class="clear"></div>
  <div class="tmz-spacer"></div>
   
    <!--p class="flRight">*Required Information</p><br/-->
<?php
//echo "<br/>";
$sf = sfContext::getInstance()->getUser();
if($sf->hasFlash('notice')){
?><div id="flash_notice" class="alertBox" ><?php
echo nl2br($sf->getFlash('notice'));

?></div><?php }?>
    
    <h2 class="highlight yellow">You are authenticated for SW Global LLC Account.</h2>
    <br/>
    <div class="clearfix"></div>
        <table width="100%" cellpadding="0" cellspacing="0" border="0">
  
        <tr>
          <td class="blbar"><b>Please choose payment option you want to pay with:</b></td>
        </tr>
 
    </table>
    <br/>
    <?php echo form_tag(url_for('paymentGateway/selectMode'),array('name'=>'openid','id'=>'openid','onsubmit'=>"return radio_button_checker(openid)")); ?>
    <table width="100%" cellpadding="0" cellspacing="0" border="0">
 
        <?php
        $flgVbv = false;
        $flgMcs = false;
        foreach($cardTypeArray as $cardTypeService) {
            if(count($cardTypeService) > 0){
                for($i=0;$i<count($cardTypeService);$i++){
                    $arrCardTypeService[] = $cardTypeService[$i]['service'];
                }
                
            }
        }
       
        $MoFlag = false;
        foreach ($paymentModsArr as $key =>$val) {
          $forMoTextFlag = false;
          for($i= 0; $i < count($val['card_detail']); $i++){ ?>

        <span>

          <?php
                if('Mo' == $val['card_detail'][$i]['card_type']) { 
                    $MoFlag = true;
                    $forMoTextFlag = true;
                }                
                
                if(!empty($arrCardTypeService) && in_array($val['card_detail'][$i]['card_type'],$arrCardTypeService) && $val['card_detail'][$i]['card_type'] == 'nmi_vbv')
                {
                    $flgVbv = true;
                    echo formRowComplete('Verified by VISA enabled cards',radiobutton_tag('paymentMode', $val['card_detail'][$i]['gateway_id'].','.$val['card_detail'][$i]['card_type'],1),'','aExpress','err_aExpress','aExpress_row');
                }
                elseif(!empty($arrCardTypeService) && in_array($val['card_detail'][$i]['card_type'],$arrCardTypeService) && $val['card_detail'][$i]['card_type'] == 'nmi_mcs')
                {
                    
                    $flgMcs = true;
                    echo formRowComplete('Master Card secure code enabled cards',radiobutton_tag('paymentMode', $val['card_detail'][$i]['gateway_id'].','.$val['card_detail'][$i]['card_type'],1),'','aExpress','err_aExpress','aExpress_row');
                }
                else
                {                    
                    if($processingCountry == 'GB' &&  $forMoTextFlag){
                        $currencyText = '<i>Application fees will be charged in &pound; only.</i>';
                    }else{
                        $currencyText = '';
                    }
                    echo formRowComplete($val['card_detail'][$i]['display_name'],radiobutton_tag('paymentMode', $val['card_detail'][$i]['gateway_id'].','.$val['card_detail'][$i]['card_type'],1), $currencyText,'aExpress','err_aExpress','aExpress_row');
                    
                }

//                if(!empty($cardTypeArray[$i]) && ($cardTypeArray[$i]['service'] == $val['card_detail'][$i]['card_type']) && $cardTypeArray[$i]['service'] == 'nmi_vbv' ){
//                    echo formRowComplete('VISA (Only verified by VISA enabled cards are supported)',radiobutton_tag('paymentMode', $val['card_detail'][$i]['gateway_id'].','.$val['card_detail'][$i]['card_type'],1),'','aExpress','err_aExpress','aExpress_row');
//                }else if(!empty($cardTypeArray[$i]) &&  ($cardTypeArray[$i]['service'] == $val['card_detail'][$i]['card_type']) && $cardTypeArray[$i]['service'] == 'nmi_mcs'){
//                    echo formRowComplete('MasterCard (Only Master card secure code enabled cards are supported)',radiobutton_tag('paymentMode', $val['card_detail'][$i]['gateway_id'].','.$val['card_detail'][$i]['card_type'],1),'','aExpress','err_aExpress','aExpress_row');
//                }else{
//                    echo formRowComplete($val['card_detail'][$i]['display_name'],radiobutton_tag('paymentMode', $val['card_detail'][$i]['gateway_id'].','.$val['card_detail'][$i]['card_type'],1),'','aExpress','err_aExpress','aExpress_row');
//                }

          ?>
        </span>
        <?php }}
        ## Setting MoFlag for error page...
        ## If MoFlag is true then only Mo link will be enabled for generating money order incase of failure...
        if($MoFlag){
            sfContext::getInstance()->getUser()->setAttribute('MoFlag', true);
        }else{
            sfContext::getInstance()->getUser()->getAttributeHolder()->remove('MoFlag');
        }
        ?>
        <?php if(!empty($arrCardTypeService) && $flgVbv && $flgMcs){?>
        <tr>
        <td colspan="2">
            <b>NOTE:</b> <br /><b>Verified by VISA enabled cards :</b> Only Credit cards which are enrolled for Verified by Visa will be accepted. <a href ="http://www.visa-asia.com/ap/sea/cardholders/security/vbv.shtml" target="_new">More Info </a> <br />
            <b>Master Card secure code enabled cards :</b> Only Credit cards which are enrolled for Master Card 3D Secure will be accepted. <a href ="http://www.mastercard.com/us/personal/en/cardholderservices/securecode/index.html" target="_new">More Info </a>
        </td>
        </tr>
        <?php }else if(!empty($arrCardTypeService) && $flgVbv){?>
        <tr>
        <td colspan="2">
            <b>NOTE:</b> <br /><b>Verified by VISA enabled cards :</b> Only Credit cards which are enrolled for Verified by Visa will be accepted. <a href ="http://www.visa-asia.com/ap/sea/cardholders/security/vbv.shtml" target="_new">More Info </a> <br />
            
        </td>
        </tr>
        <?php  ?>
        <?php }else if(!empty($arrCardTypeService) && $flgMcs){?>
        <tr>
        <td colspan="2">
            <b>NOTE:</b> <br />
            <b>Master Card secure code enabled cards :</b> Only Credit cards which are enrolled for Master Card 3D Secure will be accepted. <a href ="http://www.mastercard.com/us/personal/en/cardholderservices/securecode/index.html" target="_new">More Info </a>
        </td>
        </tr>
        <?php } ?>
        <tr>
        <td></td>
        <td>
            <input type="submit" onclick="" value="Next" class="loginbutton">
          </td>
        </tr>
    
    </table>    
<!--</div>-->

<script language="JavaScript">

  function radio_button_checker(vault)
  {//alert('dgfdg');
    // set var radio_choice to false
    var radio_choice = false;

    // Loop from zero to the one minus the number of radio button selections
    for (counter = 0; counter < vault.paymentMode.length; counter++)
    {
      // If a radio button has been selected it will return true
      // (If not it will return false)
      if (vault.paymentMode[counter].checked)
        radio_choice = true;
    }

    if (!radio_choice)
    {
      // If there were no selections made display an alert box
      alert("Please select at least one option.")
      return (false);
    }
    return (true);
  }
</script>
  </div>
</div>
<div class="content_wrapper_bottom"></div>
