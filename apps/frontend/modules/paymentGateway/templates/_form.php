<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>
<?php use_helper('Form'); ?>
<?php
if (isset($workType) && 'recharge' == $workType)
    $action = url_for($this->getModuleName() . '/rechargeProcess');
else
    $action = url_for($this->getModuleName() . '/create');

## Start here... ## Adding by ashwani...
$cartValidationObj = new CartValidation();
$arrSameCardLimit = array();
$arrCartCapacityLimit = $cartValidationObj->checkUserCartCapacity();
if(!empty($arrCartCapacityLimit) && count($arrCartCapacityLimit)){
      $arrSameCardLimit = $arrCartCapacityLimit;
} ## End here...
$hour = Settings::maxcard_time($arrSameCardLimit['transaction_period']);
$hour = str_replace(".","",$hour);
?>
<!-- Note Added for payment user -->
<div style="padding-right:240px;padding-bottom:15px;padding-top:5px" class="redBigMessage">
    Note: This payment will be reflected as Passport Services on your credit card statement.
</div>
<div style="padding-right:118px;" class="redBigMessage">Due to regulations, we do not allow the use of same card more than once in <?php echo $hour; ?>, unless registered.</div>
<form  id="frm_billing_info"  name="frm_billing_info" action="<?php echo $action ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?> onsubmit="return payeasycheckCardType();" >
    <?php if (!$form->getObject()->isNew()): ?>
        <input type="hidden" name="sf_method" value="put" />
    <?php endif; ?>
        <table border="1" style="width:100%; margin-bottom:10px;">

            <tr>
                <td>
                    <table width="100%" style="margin:auto; vertical-align:top;">
                        <tr>
                            <td class="blbar" colspan="2" align="right">      <p>Billing information</p></td>
                        </tr>
                        <tr>
                            <td colspan="2" align="right">      <p class="red">*Required Information </p></td>
                        </tr>
                        <tr>
                            <td colspan="2" align="right">      <p class="red">Please enter the exact billing address which is printed on your Credit Card statement</p>

                            </td>
                        </tr>

                    <?php
                    if (isset($allProfile) && count($allProfile) > 0):

                        echo "<tr><td>Profile</td><td>";
                        foreach ($allProfile as $key => $value) {
                            echo str_ireplace('P', 'Profile ', $value['profile']);
                            if ($radioChecked != $value['id'])
                                echo radiobutton_tag("Profile", $value['id'], '', array('onclick' => 'getProfileActive(this)'));
                            else
                                echo radiobutton_tag("Profile", $value['id'], 1, array('onclick' => 'getProfileActive(this)'));
                            echo '&nbsp;&nbsp;&nbsp;';
                        }


                        echo "</td></tr>";
                    endif ?>



                    <tr>
                        <td width="40%"><?php echo $form['first_name']->renderLabel(); ?><span class="red">*</span></td>
                        <td><?php echo $form['first_name']->render(); ?>
                            <br>
                            <div class="red" id="first_name_error">
                                <?php echo $form['first_name']->renderError(); ?>
                            </div>

                        </td>
                    </tr>
                    <tr>
                        <td><?php echo $form['last_name']->renderLabel(); ?><span class="red">*</span></td>
                        <td><?php echo $form['last_name']->render(); ?>
                            <br>
                            <div class="red" id="last_name_error">
                                <?php echo $form['last_name']->renderError(); ?>
                            </div>

                        </td>
                    </tr>

                    <tr>
                        <td><?php echo $form['address1']->renderLabel(); ?><span class="red">*</span></td>
                        <td><?php echo $form['address1']->render(); ?>
                            <br>
                            <div class="red" id="address1_error">
                                <?php echo $form['address1']->renderError(); ?>
                            </div>

                        </td>
                    </tr>

                    <tr>
                        <td><?php echo $form['address2']->renderLabel(); ?></td>
                        <td><?php echo $form['address2']->render(); ?>
                            <br>
                            <div class="red" id="address2_error">
                                <?php echo $form['address2']->renderError(); ?>
                            </div>

                        </td>
                    </tr>

                    <tr>
                        <td><?php echo $form['town']->renderLabel(); ?><span class="red">*</span></td>
                        <td><?php echo $form['town']->render(); ?>
                            <br>
                            <div class="red" id="town_error">
                                <?php echo $form['town']->renderError(); ?>
                            </div>

                        </td>
                    </tr>

                    <tr>
                        <td><?php echo $form['state']->renderLabel(); ?></td>
                        <td><?php echo $form['state']->render(); ?>
                            <br>
                            <div class="red" id="state_error">
                                <?php echo $form['state']->renderError(); ?>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td><?php echo $form['zip']->renderLabel(); ?><span class="red" id="mandatoryZip" style="display:none;">*</span></td>
                        <td><?php echo $form['zip']->render(); ?>
                            <br>
                            <div class="red" id="zip_error">
                                <?php echo $form['zip']->renderError(); ?>
                            </div>

                        </td>
                    </tr>

                    <tr>
                        <td><?php echo $form['country']->renderLabel(); ?><span class="red">*</span></td>
                        <td><?php echo $form['country']->render(); ?>
                            <br>
                            <div class="red" id="country_error">
                                <?php echo $form['country']->renderError(); ?>
                            </div>

                        </td>
                    </tr>

                    <tr>
                        <td><?php echo $form['email']->renderLabel(); ?><span class="red">*</span></td>
                        <td><?php echo $form['email']->render(); ?>
                            <br>
                            <div class="red" id="email_error">
                                <?php echo $form['email']->renderError(); ?>
                            </div>

                        </td>
                    </tr>

                    <tr>
                        <td><?php echo $form['phone']->renderLabel(); ?><span class="red">*</span></td>
                        <td><?php echo $form['phone']->render(); ?>
                            <br>
                            <div class="red" id="phone_error">
                                <?php echo $form['phone']->renderError(); ?>
                            </div>

                        </td>
                    </tr>

                </table>
                <br/>
            </td>
            <td style="width:50%; vertical-align:top;">
                <table width="100%" style="margin:auto; margin-bottom:10px;">
                    <tr>
                        <td class="blbar" colspan="2" align="right">      <p>Card information</p></td>
                    </tr>

                    <tr>
                        <td><?php echo $form['card_type']->renderLabel(); ?><span class="red">*</span></td>
                        <td><?php echo $form['card_type']->render(); ?>
                            <br>
                            <div class="red" id="card_type_error">
                                <?php echo $form['card_type']->renderError(); ?>
                            </div>

                        </td>
                    </tr>

                    <tr>
                        <td><?php echo $form['card_Num']->renderLabel(); ?><span class="red">*</span></td>
                        <td><?php echo $form['card_Num']->render(); ?> <br> <img src="<?php echo image_path('visa_new.gif'); ?>" alt="" class="pd4" />

                            <br>
                            <div class="red" id="card_Num_error">
                                <?php echo $form['card_Num']->renderError(); ?>
                            </div>

                        </td>
                    </tr>

                    <tr>
                        <td><?php echo $form['card_holder']->renderLabel(); ?><span class="red">*</span></td>
                        <td><?php echo $form['card_holder']->render(); ?>
                            <br>
                            <div class="red" id="card_holder_error">
                                <?php echo $form['card_holder']->renderError(); ?>
                            </div>

                        </td>
                    </tr>

                    <tr>
                        <td><?php echo $form['expiry_date']->renderLabel(); ?><span class="red">*</span></td>
                        <td><?php echo $form['expiry_date']->render(); ?>
                            <br>
                            <div class="red" id="expiry_date_error">
                                <?php echo $form['expiry_date']->renderError(); ?>
                            </div>

                        </td>
                    </tr>

                    <tr>
                        <td><?php echo $form['cvv']->renderLabel(); ?><span class="red">*</span>
                            <?php
                                $cvv_help = public_path('/images/', true) . 'question_mark.jpeg';
                                echo link_to(image_tag($cvv_help, array('title' => 'What is CVV?')), $this->getModuleName() . '/cvvHelp', array('popup' => array('Agent Type Window', 'width=620,height=320,left=320,top=200,resizable=no,toolbar=no,location=no'), 'target' => '_blank'))
                            ?>
                            </td>
                            <td><?php echo $form['cvv']->render(); ?>
                                <br>
                                <div class="red" id="cvv_error">
                                <?php echo $form['cvv']->renderError(); ?>
                            </div>
                            <?php echo $form['request_ip']->render(); ?>
                            </td>
                        </tr>

                </td>
            </tr>
        </table>
        <tfoot>
            <tr>
                <td colspan="2" colspan="2">
                    <div style="margin:auto; width:100%;" >
                    <?php echo $form['agreed']->render(); ?>
                                &nbsp;  I agree to <?php
                                echo link_to('Terms and Conditions', 'cms/termsAndConditions', array(
                                    'popup' => array('Window title', 'width=933,height=600,left=320,top=0,scrollbars=yes')
                                )) ?>
                            </div>

                            <div class="red" id="error_agreedterm">
                    <?php echo $form['agreed']->renderError(); ?>
                            </div>
                        </td>
                    </tr>

                    <tr>

                        <td colspan="2" colspan="2">

                <?php if (!$form->getObject()->isNew()): ?>
                                    &nbsp;<?php //echo link_to('Delete', 'payeasy/delete?id='.$form->getObject()->getId(), array('method' => 'delete', 'confirm' => 'Are you sure?'))  ?>

                <?php endif; ?>
                                    <input type="hidden" name="requestId" id="requestId" value="<?php echo $requestId; ?>">
                                    <div style="margin:auto; width:90px;" id="payButDiv">
                                        <input type="submit" value="Make Payment" id="payBut" name="payBut" class="paymentbutton" onclick="noDisp()" />
                                    </div>
                                </td>
                            </tr>
                        </tfoot>

                    </table>
                    </form>
                    <!-- start - change by vineet for making ZIP mandatory-->
                    <script>
                        var found = new Array();
                    </script>
<?php
                                    for ($i = 0; $i < count($arrCountry); $i++) {
?>
                                        <script>
                                            found[<?php echo $i; ?>] = '<?php echo $arrCountry[$i]; ?>';
                                        </script>
<?php
                                    }
?>

                                    <script>
                                        function inArray(first,arrSecond)
                                        {
                                            for(i=0;i<arrSecond.length;i++)
                                            {
                                                if(first == arrSecond[i])
                                                {
                                                    return true;
                                                }
                                            }
                                            return false;
                                        }


                                        $(document).ready(function()
                                        {
                                            if(inArray($("#ep_pay_easy_request_country").val(),found))
                                            {
                                                document.getElementById("mandatoryZip").style.display = '';
                                            }
                                            $("#ep_pay_easy_request_country").change(function(){
                                                if(inArray($("#ep_pay_easy_request_country").val(),found))
                                                {
                                                    document.getElementById("mandatoryZip").style.display = '';
                                                }
                                                else
                                                {
                                                    document.getElementById("mandatoryZip").style.display = 'none';
                                                }
                                            });
                                        });
                                        // end - change by vineet for making ZIP mandatory

                                        function noDisp()
                                        {

                                            var chkCard = payeasycheckCardType();

                                            if(chkCard)
                                                document.getElementById("payButDiv").style.display="none";

                                        }
                                        function getProfileActive(val){


                                            var url = "<?php echo url_for('paymentGateway/getProfile'); ?>";
        var requestId = $(val).val();

        $.ajax({
            type: "POST",
            url: url,
            data: "requestId="+requestId,
            success: function(data) {

                var vars = data.split('##');
                var data_length = vars.length;
                var form_values = new Array();
                for(var i=0;i<data_length;i++) {
                    var d = vars[i];
                    var v = d.split('=>');
                    var key = v[0];
                    form_values[key] = v[1];
                }
         
         
                $("#ep_pay_easy_request_first_name").val(form_vblockalues['first_name']);
                $("#ep_pay_easy_request_last_name").val(form_values['last_name']);
                $("#ep_pay_easy_request_address1").val(form_values['address1']);
                $("#ep_pay_easy_request_address2").val(form_values['address2']);
                $("#ep_pay_easy_request_address2").val(form_values['address2']);
                $("#ep_pay_easy_request_town").val(form_values['town']);
                $("#ep_pay_easy_request_state").val(form_values['state']);
                $("#ep_pay_easy_request_zip").val(form_values['zip']);
                $("#ep_pay_easy_request_country").val(form_values['country']);
                $("#ep_pay_easy_request_email").val(form_values['email']);
                $("#ep_pay_easy_request_phone").val(form_values['phone']);
            }
        });
        
     
    }
</script>