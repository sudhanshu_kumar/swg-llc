<?php $currencySymbol = PaymentModeManager::currencySymbol($currency); ?>
<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">
<?php include_partial('global/innerHeading',array('heading'=>'MONEY ORDER PAYMENT INSTRUCTIONS')); ?>
<?php
$purchaseMoButtonName = 'YES, but I have to purchase a money order';
$backButtonName = 'No, Please take me back to payment options';
$proceedMoButtonName = 'Yes, I have my money order to proceed';
$registeredButtonName = 'Continue for Card Registeration';
$printMoButtonName = 'Print Money Order Instructions';
$closeMoButtonName = 'Close Window';
$backOption = ($paymentModsCount > 1)?true:false;
?>
    <div class="payment_info_area">
        <?php if(trim($requestId) != ''){ ?>
      <table width="100%" cellpadding="0" cellspacing="0" border="0" class="no_border_new">
            <tr>
          <td class="noPrint"><p class="body_txtp"> <span class="red"> <b>IMPORTANT:  READ AND UNDERSTAND THE FOLLOWING INSTRUCTIONS BEFORE PROCEEDING.  PLEASE MAKE SURE YOU READ ALL OF THE INSTRUCTIONS BELOW BEFORE CLICKING THE "Yes, I have my money order to proceed" BUTTON BELOW.
                                <?php if($backOption){ ?>
              <br />
              <br />
                                IF YOU DO NOT WISH TO PROCEED WITH MONEY ORDER PAYMENT, PLEASE CLICK THE "No, Please take me back to the payment options" BUTTON BELOW TO RETURN TO THE PAYMENT OPTIONS PAGE.
                                <?php } ?>
              </b> </span> </p></td>
            </tr>
            <tr>
          <td> Please obtain a United States Postal Services Money Order for the amount payable for your application. The money order should be payable to "SW Global LLC/FGN". You are required to fill in the details on the money order as follows:<br/>
            <ol class="ol_listing">
              <li>Fill in <strong>"SW Global LLC/FGN"</strong> in the "Pay To" section.</li>
              <li>Fill in following address <strong>"50 Albany Turnpike ,Suite 5032 Canton, CT 06019"</strong> in the "Address" section under the "Pay To" section.</li>
              <li>Please fill in your name under "From" section.</li>
              <li>Please fill in your address (in single line) under "Address" just below "From" section.</li>
              <li>Please fill in your <strong>tracking number "(you will obtain the Tracking number once you click the proceed button below)"</strong> in the "Memo" section. </li>
            </ol>
            <br/>
            <b>Please see below a sample postal money order filled in for candidate "John Doe" with Address "64 Lincoln Street, Spencerville, CT 06000": is show in the image below:</b><br/>
            <br/>
            <div align="center">
            <?php if($currency == 'pound'){ ?>
                <img src="<?php echo image_path('money_order_uk.jpg'); ?>" alt="" id="img" class="pd4_new" width="642px;" />
            <?php }else{ ?>
                <img src="<?php echo image_path('money_order2.jpg'); ?>" alt="" id="img" class="pd4_new" width="642px;" />
            <?php } ?>
            </div>
            <br/>
                    Once you have filled in the postal money order as per instructions above, please follow these steps:<br/>
            <ol class="ol_listing">
              <li>Note down the <strong>"Serial Number"</strong> (as shown by red circle in the sample image above) and provide that to us with other requested information after you hit the proceed button below. We’ll send you a confirmation email stating that your order number & tracking number is associated with a money order serial number. This email doesn’t mean that your application is paid yet.</li>
              <li>Mail the filled in Money Order via a traceable courier service (such as Fedex, UPS and DHL):<br/>
                <strong>SW Global LLC</strong><br/>
                            <strong>50 Albany Turnpike ,Suite 5032</strong> <br/>
                        <strong>Canton, CT 06019</strong></li>
            </ol>
            <br/>
            Once we receive your money order, we’ll confirm the receipt via another email to you stating that your payment has been received and you can proceed to submit your Visa/Passport application.<br/>
                </td>
            </tr>
            <?php if($mode != 'print'){ ?>
            <tr>
          <td class="noPrint"><p class="body_txtp"> <span class="red"><b>YOU MUST HAVE YOUR MONEY ORDER NUMBER BEFORE PROCEEDING TO PAYMENT.  IF YOU DO NOT, PLEASE CLICK THE “Yes, but I have to purchase a money order” BUTTON BELOW AND EXIT THE SITE.  ONCE YOU HAVE YOUR MONEY ORDER, PLEASE LOG INTO THE PORTAL AND PROCEED TO PAYMENT.</b></span> </p></td>
            </tr>
            <?php }else{ ?>
            <tr>
          <td class="noPrint"><p class="body_txtp"> <span class="red"><b>YOU MUST HAVE YOUR MONEY ORDER NUMBER BEFORE PROCEEDING TO PAYMENT.</b></span> </p></td>
            </tr>
            <?php } ?>
        </table>
<div class="clear border_top"></div>
      <?php if($mode != 'print'){ ?>
      <div class="tmz-info-div">
          <table width="100%" border="0" cellspacing="0" cellpadding="0" class="">
              <tr>
                  <td valign="top" class="tmz-tab-heading blbar" style="margin-left: 0px;padding-left:5px;">Application Details</td>
              </tr>
              <tr>
                  <td valign="top" class="grey_bg_table">
                      <?php include_partial('cart/cart_details', array('processingCountry' => $processingCountry, 'currencySymbol' => $currencySymbol)); ?>
                  </td>
              </tr>
          </table>
      </div>
      <?php } ?>
      <table width="100%" class="no_border_padding">

      

            <?php if($mode == 'print'){ ?>
            <tr>
                <td>
              <div id="printDiv" align="center">
                <input type="button" class="normalbutton noPrint" value="<?php echo $printMoButtonName; ?>" onclick="window.print();" />
				<input type="button" class="normalbutton noPrint" value="<?php echo $closeMoButtonName; ?>" onclick="window.close();" />
                            </div>
                </td>
            </tr>
            <?php } else { ?>
        <tr class="noPrint">
          <!-- noPrint class is using for print only -->
          <td align="center"><b>DO YOU WANT TO CONTINUE PAYMENT WITH MONEY ORDER?</b> </td>
            </tr>
        <tr class="noPrint">
          <!-- noPrint class is using for print only -->
          <td><table width="100%" border="0" align="center" cellpadding="0"  cellspacing="0" class="no_border_padding">
                        <tr>
                <td align="right"  ><input type="button" class="normalbutton noPrint" id="proceed" value="<?php echo $proceedMoButtonName; ?>" /></td>
                <td align="left"  ><input type="button" class="normalbutton noPrint" id="purchase" value="<?php echo $purchaseMoButtonName; ?>" /></td>
                        </tr>
                        <tr>
                <td align="right"  ><input type="button" class="normalbutton noPrint" id="print" value="<?php echo $printMoButtonName; ?>" onclick="window.print();" /></td>
                <td align="left"  ><?php if($backOption && $comeFrom == ''){ ?>
                  <input type="button" class="normalbutton noPrint" id="no" value="<?php echo $backButtonName; ?>" />
                                <?php }else if($comeFrom == 'po'){ ?>
                  <input type="button" class="normalbutton noPrint" id="registered" value="<?php echo $registeredButtonName; ?>" />
                          <?php } ?></td>
                        </tr>
            </table></td>
            </tr>
            <?php } ?>
        </table>
        <?php } else { ?>
        <p class="redBigMessage" >Oops! You have tempored url from somewhere.</p>
        <br />
        <?php } ?>
    </div>
    
<div id="popupContent" class="noPrint">    
    <div id="purchaseMessage" style="display:none;" class="NwPopUp">
        <div align="right"><strong>press [Esc] to close</strong></div>
        <div class="NwHeading noPrint"><?php echo image_tag('/images/info.png',array('alt'=>'SWGlobal LLC', 'align' => 'absmiddle'));?> Money Order Information</div>
        <div class="NwMessage noPrint"> In order to make payment via money order, please obtain a <strong><?php echo $moneyOrderTitle; ?></strong> for the amount <strong><?php echo html_entity_decode($currencySymbol); ?><span id="amount"><?php echo $totalAppsAmount; ?></span></strong> payable for your application. The money order should be payable to <strong>"SW Global LLC/FGN"</strong>. For more information kindly press <strong>[Esc]</strong> to close it and see the instructions by clicking on <strong>"Print Money Order Instructions"</strong>. <br />
        <br />
          <br />
          <strong>Do you wish to exit from the system?</strong> </div>
        <br />
        
        <div class="noPrint">
          <center>
            <input type="button" id="purchaseMessageYes" value="Yes, Exit from the system for now" class="normalbutton noPrint" />
            <input type="button" id="purchaseMessageNo" value="No, close this message box" class="normalbutton noPrint" />
          </center>
        </div>
    </div>
</div>
<div id="backgroundPopup" class="noPrint"></div>
  </div>
</div>
<div class="content_wrapper_bottom"></div>
<script>
    $(document).ready(function(){

        $('#purchase').click(function(){
            var elementPos = findElementPosition('img');
            elementPos = elementPos.split(",");

            //alert("Left: "+elementPos[0]+ " Top: "+elementPos[1]); //219 1568

            $('#moneyOrderMessage').hide();
            $('#purchaseMessage').show();



            $("#popupContent").css({
                "position": "absolute",
                "width": '600px',
                "left": (parseInt(elementPos[0])-100)+'px',
                "top": (parseInt(elementPos[1]) + 400)+'px'
            });
            //centerPopup();
            loadPopup();
        });

        $('#proceed').click(function(){


            var moURL = "<?php echo url_for('mo_configuration/associateTrackingNumber');?>";
            window.location = moURL;

            /*
            var elementPos = findElementPosition('img');
            elementPos = elementPos.split(",");

            //alert("Left: "+elementPos[0]+ " Top: "+elementPos[1]); //219 1568

            $('#purchaseMessage').hide();
            $('#moneyOrderMessage').show();

            $('#txtMoneyOrderNumber').val('');
            $('#errorMessage').html('');

            $("#popupContent").css({
                "position": "absolute",
                "width": '600px',
                "left": (parseInt(elementPos[0])-100)+'px',
                "top": (parseInt(elementPos[1]) + 400)+'px'
            });
            loadPopup();
            */

        });

        $('#no').click(function(){
            var url = "<?php echo url_for('paymentGateway/gotoApplicantVault');?>";
            window.location = url;
        });


        $('#registered').click(function(){
            var url = "<?php echo url_for('userAuth/registerCreditCard');?>";
            window.location = url;
        });


        /**
         * following javascript works for popup buttons...
         */
        $('#purchaseMessageNo').click(function(){
            disablePopup();
        });

        $('#purchaseMessageYes').click(function(){
            var url = "<?php echo url_for('@sf_guard_signout');?>";
            window.location = url;
        });

        $('#moneyOrderMessageContinue').click(function(){

            //$('#moneyOrderMessageContinue').attr('disabled','disabled');


            var txtMoneyOrderNumber = jQuery.trim($('#txtMoneyOrderNumber').val());
            if(txtMoneyOrderNumber == ''){
                $('#errorMessage').html('Please enter money order serial number.');
                //$('#moneyOrderMessageContinue').removeAttr('disabled');
                return false;
            }

            var url = "<?php echo url_for("mo_configuration/uniqueOrderNumber"); ?>";
            $.post(url, { moneyorder_number: encodeURIComponent(txtMoneyOrderNumber)},
            function(data){
                 data = jQuery.trim(data);
                 //$('#moneyOrderMessageContinue').removeAttr('disabled');
                 if(data == 0){
                    var moURL = "<?php echo url_for('mo_configuration/associateTrackingNumber?p=y');?>"+"/mo/"+encodeURIComponent(txtMoneyOrderNumber);
                    window.location = moURL;
                 }else if(data == 1){
                    $('#errorMessage').html('Money order serial number already exist.');
                 }else{
                    $('#errorMessage').html('Connection problem found. Please try again after few seconds.');
                 }
            });
        });

        $('#moneyOrderMessageClose').click(function(){
            disablePopup();
        });

    });
</script>