<?php
include_partial('global/innerHeading',array('heading'=>'Order Confirmation'));?>

<div class="wrap870">
<?php
//echo "<br/>";
$sf = sfContext::getInstance()->getUser();
if($sf->hasFlash('notice')){
    ?><div id="flash_notice" class="alertBox" ><?php
    echo nl2br($sf->getFlash('notice'));

    ?></div><br/><?php }?>
    <?php include_partial('orderDetail', array('request_details'=>$request_details)) ?>
    <?php
    echo "<br/>";

    if($sf->hasFlash('error')){
        ?><div id="flash_notice" class="alertBox" ><?php
        echo nl2br($sf->getFlash('error'));
        ?></div><?php
    }
    ?>

    <?php
    if(!$pStatus)
    {
        $action = url_for($this->getModuleName().'/createViaEwallet') ;
        ?>
    <form action="<?php echo $action ?>" method="post"  onsubmit="return payeasycheckCardType();">

        <input type="hidden" name="requestId" id="requestId" value="<?php echo $requestId; ?>">
        <div style="margin:auto; width:90px;" id="payButDiv">
            <input type="submit" value="Make Payment" id="payBut" name="payBut" class="paymentbutton" onclick="noDisp();" />
        </div>
    </form>
    <?php
}
?>
    <script>
        function noDisp()
        {
                document.getElementById("payButDiv").style.display="none";

        }
    </script>


</div>
