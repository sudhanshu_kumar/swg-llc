<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>
<?php use_helper('Form');?>
<?php include_partial('global/innerHeading',array('heading'=>'Payment Details'));?>
<div class="global_content4 clearfix">

<div class="">
  <?php include_partial('orderDetail', array('request_details'=>$request_details)) ?>
</div>
<!-- <div align="center" class="redBigMessage">Due to regulations, we do not allow use of same card more than once in <?php echo $hour = Settings::maxcard_time();?>. [Not applicable to registered cards]</div><br> -->
<?php if($cardType !='A'){?>
<div align="center" class="redMediumMessage" >Note: Your card will be tested for eligibility of Verified by Visa or MasterCard SecureCode on submission .  <a href='javascript: void(0)' onclick='window.open("<?php echo url_for('nmi/visaMasterInfo');?>",
  "windowname1","width=500, height=500")'>Know more</a></div><?php } ?>
<form  id="frm_billing_info"  name="frm_billing_info" action="#" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
  <?php if (!$form->getObject()->isNew()): ?>
  <input type="hidden" name="sf_method" value="put" />
  <?php endif; ?>
  <table border="1" style="width:100%; margin-bottom:10px;">

    <tr>
      <td>
        <table width="100%" style="margin:auto; vertical-align:top;">
          <tr>
            <td class="blbar" colspan="2" align="right">      <p>Billing information</p></td>
          </tr>
          <tr>
            <td colspan="2" align="right">      <p class="red">*Required Information </p></td>
          </tr>
          <tr>
            <td colspan="2" align="right">      <p class="red">Please enter the exact billing address which is printed on your Credit Card statement</p>

            </td>
          </tr>

          <tr>
            <td><?php echo $form['billing-first-name']->renderLabel(); ?><span class="red">*</span></td>
            <td><?php echo $form['billing-first-name']->render(); ?>
              <br>
              <div class="red" id="billing-first-name_error">
                <?php echo $form['billing-first-name']->renderError(); ?>
              </div>

            </td>
          </tr>
          <tr>
            <td><?php echo $form['billing-last-name']->renderLabel(); ?><span class="red">*</span></td>
            <td><?php echo $form['billing-last-name']->render(); ?>
              <br>
              <div class="red" id="billing-last-name_error">
                <?php echo $form['billing-last-name']->renderError(); ?>
              </div>

            </td>
          </tr>

          <tr>
            <td><?php echo $form['billing-address1']->renderLabel(); ?><span class="red">*</span></td>
            <td><?php echo $form['billing-address1']->render(); ?>
              <br>
              <div class="red" id="billing-address1_error">
                <?php echo $form['billing-address1']->renderError(); ?>
              </div>

            </td>
          </tr>

          <tr>
            <td><?php echo $form['billing-address2']->renderLabel(); ?></td>
            <td><?php echo $form['billing-address2']->render(); ?>
              <br>
              <div class="red" id="billing-address2_error">
                <?php echo $form['billing-address2']->renderError(); ?>
              </div>

            </td>
          </tr>

          <tr>
            <td><?php echo $form['billing-city']->renderLabel(); ?><span class="red">*</span></td>
            <td><?php echo $form['billing-city']->render(); ?>
              <br>
              <div class="red" id="billing-city_error">
                <?php echo $form['billing-city']->renderError(); ?>
              </div>

            </td>
          </tr>

          <tr>
            <td><?php echo $form['billing-state']->renderLabel(); ?></td>
            <td><?php echo $form['billing-state']->render(); ?>
              <br>
              <div class="red" id="billing-state_error">
                <?php echo $form['billing-state']->renderError(); ?>
              </div>
            </td>
          </tr>

          <tr>
            <td><?php echo $form['billing-postal']->renderLabel(); ?><span class="red" id="mandatoryZip" style="display:none;">*</span></td>
            <td><?php echo $form['billing-postal']->render(); ?>
              <br>
              <div class="red" id="billing-postal_error">
                <?php echo $form['billing-postal']->renderError(); ?>
              </div>

            </td>
          </tr>

          <tr>
            <td><?php echo $form['billing-country']->renderLabel(); ?><span class="red">*</span></td>
            <td><?php echo $form['billing-country']->render(); ?>
              <br>
              <div class="red" id="billing-country_error">
                <?php echo $form['billing-country']->renderError(); ?>
              </div>

            </td>
          </tr>

          <tr>
            <td><?php echo $form['billing-email']->renderLabel(); ?><span class="red">*</span></td>
            <td><?php echo $form['billing-email']->render(); ?>
              <br>
              <div class="red" id="billing-email_error">
                <?php echo $form['billing-email']->renderError(); ?>
              </div>

            </td>
          </tr>

          <tr>
            <td><?php echo $form['billing-phone']->renderLabel(); ?><span class="red">*</span></td>
            <td><?php echo $form['billing-phone']->render(); ?>
              <br>
              <div class="red" id="billing-phone_error">
                <?php echo $form['billing-phone']->renderError(); ?>
              </div>

            </td>
          </tr>

        </table>
        <br/>
      </td>
      <td style="width:50%; vertical-align:top;">
        <table width="100%" style="margin:auto; margin-bottom:10px;">
        <tr>
          <td class="blbar" colspan="2" align="right">      <p>Card information</p></td>
        </tr>

        <tr>
          <td><?php echo $form['card_type']->renderLabel(); ?><span class="red">*</span></td>
          <td><?php echo $paymentMode ; ?> <input type="hidden" name="cardType" id="cardType" value="<?php echo $cardType ; ?>" />
            <br>
            <div class="red" id="card_type_error">
              <?php echo $form['card_type']->renderError(); ?>
            </div>

          </td>
        </tr>

        <tr>
          <td><?php echo $form['billing-cc-number']->renderLabel(); ?><span class="red">*</span></td>
          <td><?php //echo $form['billing-cc-number']->render(); ?> <!-- <br> <img src="<?php // echo image_path('visa_new.gif'); ?>" alt="" class="pd4" /> -->
            <input id="billing-cc-number" name="billing-cc-number" type="hidden" size="16" maxlength="16">
            <input id="cc_first" name="cc_first" type="text" size="5" maxlength="4" class="txt-input" readonly="readonly" value="<?php echo $cardFirst;?>"> -
            <input id="cc_mid" name="cc_mid" type="text" size="10" maxlength="<?php echo $midlen;?>" class="txt-input" autocomplete="off"> -
            <input id="cc_last" name="cc_last" type="text" size="5" maxlength="4" class="txt-input" readonly="readonly" value="<?php echo $cardLast;?>">


            <br>
            <div class="red" id="billing-cc-number_error">
              <?php echo $form['billing-cc-number']->renderError(); ?>
            </div>

          </td>
        </tr>

        <tr>
          <td><?php echo $form['card_holder']->renderLabel(); ?><span class="red">*</span></td>
          <td><?php echo $form['card_holder']->render(); ?>
            <br>
            <div class="red" id="card_holder_error">
              <?php echo $form['card_holder']->renderError(); ?>
            </div>

          </td>
        </tr>

        <tr>
          <td><?php echo $form['billing-cc-exp']->renderLabel(); ?><span class="red">*</span></td>
          <td><?php echo $form['billing-cc-exp']->render(); ?>
            <br>
            <div class="red" id="billing-cc-exp_error">
              <?php echo $form['billing-cc-exp']->renderError(); ?>
            </div>

          </td>
        </tr>

        <tr>
          <td><?php echo $form['billing-cvv']->renderLabel(); ?><span class="red">*</span>
            <?php
            $cvv_help = public_path('/images/',true).'question_mark.jpeg';
            echo link_to(image_tag($cvv_help, array( 'title'=>'What is CVV?')), $this->getModuleName().'/cvvHelp', array('popup' => array('Agent Type Window', 'width=620,height=320,left=320,top=200,resizable=no,toolbar=no,location=no'), 'target' => '_blank'))
            ?>
          </td>
          <td><?php echo $form['billing-cvv']->render(); ?>
            <br>
            <div class="red" id="billing-cvv_error">
              <?php echo $form['billing-cvv']->renderError(); ?>
            </div>
            <?php echo $form['request_ip']->render(); ?>
          </td>
        </tr>

      </td>
    </tr>
  </table>
  <tfoot>
    <tr>
      <td colspan="2" colspan="2">
        <div style="margin:auto; width:100%;" >
          <?php echo $form['agreed']->render(); ?>
          &nbsp;  I agree to <?php echo link_to('Terms and Conditions', 'cms/termsAndConditions', array(
          'popup' => array('Window title', 'width=933,height=600,left=320,top=0,scrollbars=yes'))) ?>
        </div>
        <?php echo $form['agreed']->renderError(); ?>
        <div class="red" id="error_agreedterm">

        </div>
      </td>
    </tr>

    <tr>
      <td colspan="2" colspan="2">
        <input type="hidden" name="requestId" id="requestId" value="<?php echo $requestId ?>">
        <div style="margin:auto; width:90px;" id="payButDiv">
          <input type="button" value="Make Payment" id="payBut" name="payBut" class="paymentbutton" onclick="validateForm('1')" />
        </div>
        <div id="loader" align="center"></div>
      </td>
    </tr>
  </tfoot>
  <input type="hidden" name="billing-cc-exp" id="billing-cc-exp" value="">  
  </table>
</form>
</div>
<script>
  var appCountry = '<?php echo $appCountry; ?>';
  var appExpMonth = '<?php echo $expMonth; ?>';
  var appExpYear = '<?php echo $expYear; ?>';
  function removeAllOptions(selectbox, fieldName)
  {    
    var selectbox = document.getElementById(selectbox);
    var i;
    var selectBoxLength = selectbox.options.length-1;
    var val = '';
    for(i=selectBoxLength;i>=0;i--)
    {
      if(fieldName == 'country'){
          val = appCountry;
      }

      switch(fieldName){
          case 'country':
              val = appCountry;
              break;
          case 'exp_month':
              val = appExpMonth;
              break;
          case 'exp_year':
              val = appExpYear;
              break;
          default:
              val = '';
              break;
      }
      
      if(selectbox.options[i].value == val){
          
      }else{
          selectbox.remove(i);
      }
//      if(selectbox.options[i].selected){
//      }else{
//        selectbox.remove(i);
//      }
    }
  }
  

  function onDateChange()
  {
    var month = document.getElementById('billing-cc-exp_month').value;
    var year = document.getElementById('billing-cc-exp_year').value;
    var year1 = year.substring(2, 4);
    if(month <= 9)
    {
      var date = '0' + month + year1;
    }
    else
    {
      var date = month + year1;
    }
    document.getElementById('billing-cc-exp').value = date;

  }
function validateZip(zip) {
    var reg = /^([A-Za-z0-9])+$/;
    if(reg.test(zip) == false) {
      return 1;
    }

    return 0;
  }
  function validateForm(form)
  {
    $('#payBut').hide();
    $('#loader').html('<?php echo image_tag('../images/ajax-loader.gif'); ?>');

    var err  = 0;
    if($('#billing-first-name').val() == "")
    {
      $('#billing-first-name_error').html("Please enter first name");
      err = err+1;
    }
    else
    {
      $('#billing-first-name_error').html("");
    }
    if($('#billing-last-name').val() == "")
    {
      $('#billing-last-name_error').html("Please enter last name");
      err = err+1;
    }
    else
    {
      $('#billing-last-name_error').html("");
    }

    if($('#billing-address1').val() == "")
    {
      $('#billing-address1_error').html("Please enter address1");
      err = err+1;
    }
    else
    {
      $('#billing-address1_error').html("");
    }

    if($('#billing-city').val() == "")
    {
      $('#billing-city_error').html("Please enter city");
      err = err+1;
    }
    else
    {
      $('#billing-city_error').html("");
    }

    if($('#billing-postal').val() == "")
      {
          if(inArray($("#billing-country").val(),found))
          {
              $('#billing-postal_error').html("ZIP(Postal Code) is compulsory.");
              err = err+1;
          }
          else
          {
              $('#billing-postal_error').html("");
          }
      }
      else if(validateZip($('#billing-postal').val()) !=0)
      {
          $('#billing-postal_error').html("Please enter Valid ZIP(Postal Code)");
          err = err+1;
      }
      else
      {
          $('#billing-postal_error').html("");
      }

    if($('#billing-country').val() == "")
    {
      $('#billing-country_error').html("Please enter country");
      err = err+1;
    }
    else
    {
      $('#billing-country_error').html("");
    }


    if($('#billing-email').val() == "")
    {
      $('#billing-email_error').html("Please enter Email");
      err = err+1;
    }
    else if(validateEmail($('#billing-email').val()) !=0)
    {
      $('#billing-email_error').html("Please enter Valid Email");
      err = err+1;
    }
    else
    {
      $('#billing-email_error').html("");
    }


    if($('#billing-phone').val() == "")
    {
      $('#billing-phone_error').html("Please enter Mobile number");
      err = err+1;
    }else if(validatePhone($('#billing-phone').val()) !=0)
    {
      $('#billing-phone_error').html("Please enter Valid Mobile number");
      err = err+1;
    }
    else
    {
      $('#billing-phone_error').html("");
    }

    var ccnum = $('#cc_first').val() + $('#cc_mid').val() + $('#cc_last').val();
    $('#billing-cc-number').val(ccnum);
    if($('#cc_mid').val() == "")
    {
      $('#billing-cc-number_error').html("Please enter card  number");
      err = err+1;
    }else if(nmiCheckCardType($('#billing-cc-number').val()) !=0){
      $("#billing-cc-number_error").html('Please enter correct Card Number')
      err = err+1;
    }
    else
    {
      $('#billing-cc-number_error').html("");
    }


    if($('#card_holder').val() == "")
    {
      $('#card_holder_error').html("Please enter card  number");
      err = err+1;
    }
    else
    {
      $('#card_holder_error').html("");
    }


    if($('#card_holder').val() == "")
    {
      $('#card_holder_error').html("Please enter card name");
      err = err+1;
    }
    else
    {
      $('#card_holder_error').html("");
    }


    if($('#billing-cvv').val() == "")
    {
      $('#billing-cvv_error').html("Please enter cvv");
      err = err+1;
    }
    else
    {
      $('#billing-cvv_error').html("");
    }



    if(!ValidateExpDate($('#billing-cc-exp_year').val(),$('#billing-cc-exp_month').val()))
    {
      $('#billing-cc-exp_error').html("Invalid expiry date ");
      err = err+1;
    }
    else
    {
      $('#billing-cc-exp_error').html("");
    }


    if($('#agreed:checked').val() == null)
    {
      $('#error_agreedterm').html("Please agree to Terms & Conditions");
      err = err+1;
    }
    else
    {
      $('#error_agreedterm').html("");
    }

    if(err == 0)
    {
      //      return true;
      var validateUrl = "<?php echo url_for('nmi/ValidateInCardVault') ?>";

      $.post(validateUrl, $('#frm_billing_info').serialize(), function(data){
        if(data == "logout"){
          location.reload();
        }else if(data == 'error'){
          $("#billing-cc-number_error").html('Please enter correct Card Number');
          $('#payBut').show();
          $('#loader').html('');
          return false;
        }else{
          var exp = data.split('#');

          var type = exp[1].split('=');
          if(type[1] == 'success')
          {
            document.forms[0].action = exp[0];
            document.forms[0].submit();
          }else{
            window.location = exp[0];
          }

        }
      });
    }else{
      $('#payBut').show();
      $('#loader').html('');
      return false;
    }



  }

  function validateEmail(email) {

    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    if(reg.test(email) == false) {

      return 1;
    }

    return 0;
  }

  function validatePhone(phoneNumber) {
    //      var reg = /^((\+)?(\d{2}[-]))?(\d{10}){1}?$/;
    //var reg = /^((\+)?(\d{2}))?(\d{10-15}){1}?$/;
    var reg = /^[0-9 [\]+()-]{8,20}?$/;
    if(reg.test(phoneNumber) == false) {
      return 1;
    }

    return 0;
  }
</script>

 <script>
    var found = new Array();
  </script>
<?php
for($i=0;$i<count($arrCountry);$i++)
{
  ?>
  <script>
    found[<?php echo $i; ?>] = '<?php echo $arrCountry[$i]; ?>';
  </script>
  <?php
}
?>



<script>

  // start - change by vineet for making ZIP mandatory
  function inArray(first,arrSecond)
    {
      for(i=0;i<arrSecond.length;i++)
      {
        if(first == arrSecond[i])
        {
          return true;
        }
      }
      return false;
    }
    $(document).ready(function()
    {
        if(inArray($("#billing-country").val(),found))
        {
            document.getElementById("mandatoryZip").style.display = '';
        }
        $("#billing-country").change(function(){
            if(inArray($("#billing-country").val(),found))
            {
                document.getElementById("mandatoryZip").style.display = '';
            }
            else
            {
                document.getElementById("mandatoryZip").style.display = 'none';
            }
        });
    });
        // end - change by vineet for making ZIP mandatory

         $(document).ready(function() {
            //var t=setTimeout("removeAllOptions('billing-country')",3000)
            removeAllOptions('billing-country','country');
            onDateChange();
            removeAllOptions('billing-cc-exp_month','exp_month');
            removeAllOptions('billing-cc-exp_year', 'exp_year');
          });

</script>