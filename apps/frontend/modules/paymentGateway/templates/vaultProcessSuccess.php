<?php
use_helper('Form');
include_partial('global/innerHeading',array('heading'=>'Vault Status'));

?>
<div class="global_content4 clearfix">
    <div class="brdBox">
        <div class="clearfix"></div>
        <br/>
        <?php
        $sf = sfContext::getInstance()->getUser();
        if($sf->hasFlash('notice')){ ?>
        <div id="flash_notice" class="alertBox" >
            <?php
            echo nl2br($sf->getFlash('notice'));
            ?>
        </div><br/>
        <?php }?>
    <?php echo form_tag('paymentGateway/processVaultAmount',array('name'=>'proccess_vault_form','class'=>'', 'method'=>'post','id'=>'order_search_form' ,'onsubmit'=>'return validateFrom(this)')) ?>
        <table style="width: 100%;">
            <tbody>

                <tr>
                    <td width="30%" class="ltgray"> Total Unprocessed Amount
                    </td>
                    <td> <?php echo format_amount($totalUnproccessAmount,1); ?>
                    </td>
                </tr>
                <tr>
                    <td width="30%" class="ltgray"> Daily Processing Limit
                    </td>
                    <td> <input type="hidden"  name="hdn_limit_amount" id="hdn_limit_amount" value="<?php echo sfConfig::get('app_daily_process_limit');?>">
                    <?php echo format_amount(sfConfig::get('app_daily_process_limit'),1); ?>
                        
                    </td>
                </tr>
                <tr>
                    <?php echo formRowComplete('Processing Amount<span style="color:red">*</span>',input_tag('process_amount', '', array('class'=>'txt-input')),'','process_amount','process_amount_error','process_amountrow'); ?>
                    
                </tr>
                <tr><td colspan="2" align="center">
                        <input type="submit" onclick="" value="Proccess" class="loginbutton">
                    </td>
                </tr>
            </tbody>
        </table>
        </form>
    </div>
</div>

<script type="text/javascript">
     function validateFrom(){
         var err  = 0;
            if($('#process_amount').val() == "")
            {
                
              $('#process_amount_error').html("Please enter processing amount");
              err = err+1;
            }else if(parseInt($('#process_amount').val()) > parseInt($('#hdn_limit_amount').val()))
            {
              $('#process_amount_error').html("Process amount can not be greater then daily limit");
              err = err+1;
            }else
            {
              $('#process_amount_error').html("");
            }
            if(err == 0)
            {
                return true;
               
            }else{
                return false;
            }
     }
    
</script>
