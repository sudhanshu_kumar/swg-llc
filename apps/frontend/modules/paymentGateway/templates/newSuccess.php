<?php
use_javascript('dhtmlwindow.js');
use_stylesheet('dhtmlwindow.css');
use_javascript('modal.js');
use_stylesheet('modal.css');

?>
<?php include_partial('global/innerHeading',array('heading'=>'Payment Details'));?>
<?php
$sf = sfContext::getInstance()->getUser();
?>
<div class="wrap870">

  <?php include_partial('orderDetail', array('request_details'=>$request_details)) ?>


  <?php

  if($sf->hasFlash('error')){
    ?><div id="flash_notice" class="alertBox" ><?php
    echo nl2br($sf->getFlash('error'));
    ?></div>
    
  <div  align="left">
      <?php if($flag){?>
    <?php echo link_to('Reason For Unsuccessful', $this->getModuleName().'/reason', array('popup' => array('Agent Type Window', 'width=620,height=320,left=320,top=200,resizable=no,toolbar=no,location=no'), 'target' => '_blank'));?>
      <?php }?>
  <?php
    ## If card type contains Mo then only this link will enable...
    $MoFlag = sfContext::getInstance()->getUser()->getAttribute('MoFlag');
    if($MoFlag){ ?>
    <!--<div class="highlight yellow" id="flash_notice_content" style="margin-right: 0px;margin-left: 0px;"><b><span style="font-size: 15px;"><a href="<?php //echo url_for("mo_configuration/trackingProcess?requestId=".$requestId);?>">CLICK HERE</a> if you would like to attempt this transaction via Money Order.</span></b></div>-->
    <?php } ?>

      
  </div>
  
  <?php } ?>




<?php
//change by vineet for passing argument in _form file
if(!$pStatus)
include_partial('form', array('form' => $form,'requestId'=>$requestId,'arrCountry'=>$arrCountry))
?>

</div>
