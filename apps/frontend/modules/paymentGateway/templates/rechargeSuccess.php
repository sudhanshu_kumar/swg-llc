<?php
use_javascript('dhtmlwindow.js');
use_stylesheet('dhtmlwindow.css');
use_javascript('modal.js');
use_stylesheet('modal.css');
?>
<?php

include_partial('global/innerHeading',array('heading'=>'Recharge Process'));?>
    <?php
//echo "<br/>";
$sf = sfContext::getInstance()->getUser();
if($sf->hasFlash('notice')){
  ?><div id="flash_notice" class="alertBox" ><?php
  echo nl2br($sf->getFlash('notice'));

  ?></div><?php }?>
<div class="wrap870">

<?php include_partial('rechargeDetail', array('request_details'=>$request_details)) ?>
<?php
echo "<br/>";

if($sf->hasFlash('error')){
  ?><div id="flash_notice" class="alertBox" ><?php
  echo nl2br($sf->getFlash('error'));
  ?></div><?php
}
?>

<?php
   if(!$pStatus)
        include_partial('form', array('form' => $form,'requestId'=>$requestId,'workType'=>$workType))
?>

</div>
