<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>
<?php use_helper('Form');?>
<?php include_partial('global/innerHeading',array('heading'=>'Payment Details'));?>
<div class="global_content4 clearfix">

<div class="">
  <?php include_partial('orderDetail', array('request_details'=>$request_details)) ?>
</div>
<!--<div align="center" class="redBigMessage">Due to regulations, we do not allow use of same card more than once in <?php //echo $hour = Settings::maxcard_time();?></div> -->
<form  id="frm_billing_info"  name="frm_billing_info" action="#" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?> >
  <?php if (!$form->getObject()->isNew()): ?>
  <input type="hidden" name="sf_method" value="put" />
  <?php endif; ?>
  <table border="1" style="width:100%; margin-bottom:10px;">

    <tr>
      <td>
        <table width="100%" style="margin:auto; vertical-align:top;">
          <tr>
            <td class="blbar" colspan="2" align="right">      <p>Billing information</p></td>
          </tr>
          <tr>
            <td colspan="2" align="right">      <p class="red">*Required Information </p></td>
          </tr>
          <tr>
            <td colspan="2" align="right">      <p class="red">Please enter the exact billing address which is printed on your Credit Card statement</p>

            </td>
          </tr>
          <tr>
            <td><?php echo $form['first_name']->renderLabel(); ?><span class="red">*</span></td>
            <td><?php echo $form['first_name']->render(); ?>
              <br>
              <div class="red" id="first_name_error">
                <?php echo $form['first_name']->renderError(); ?>
              </div>

            </td>
          </tr>
          <tr>
            <td width="40%"><?php echo $form['last_name']->renderLabel(); ?><span class="red">*</span></td>
            <td><?php echo $form['last_name']->render(); ?>
              <br>
              <div class="red" id="last_name_error">
                <?php echo $form['last_name']->renderError(); ?>
              </div>

            </td>
          </tr>

          <tr>
            <td><?php echo $form['address1']->renderLabel(); ?><span class="red">*</span></td>
            <td><?php echo $form['address1']->render(); ?>
              <br>
              <div class="red" id="address1_error">
                <?php echo $form['address1']->renderError(); ?>
              </div>

            </td>
          </tr>

          <tr>
            <td><?php echo $form['address2']->renderLabel(); ?></td>
            <td><?php echo $form['address2']->render(); ?>
              <br>
              <div class="red" id="address2_error">
                <?php echo $form['address2']->renderError(); ?>
              </div>

            </td>
          </tr>

          <tr>
            <td><?php echo $form['town']->renderLabel(); ?><span class="red">*</span></td>
            <td><?php echo $form['town']->render(); ?>
              <br>
              <div class="red" id="town_error">
                <?php echo $form['town']->renderError(); ?>
              </div>

            </td>
          </tr>

          <tr>
            <td><?php echo $form['state']->renderLabel(); ?></td>
            <td><?php echo $form['state']->render(); ?>
              <br>
              <div class="red" id="state_error">
                <?php echo $form['state']->renderError(); ?>
              </div>
            </td>
          </tr>

          <tr>
            <td><?php echo $form['zip']->renderLabel(); ?><span class="red" id="mandatoryZip" style="display:none;">*</span></td>
            <td><?php echo $form['zip']->render(); ?>
              <br>
              <div class="red" id="zip_error">
                <?php echo $form['zip']->renderError(); ?>
              </div>

            </td>
          </tr>

          <tr>
            <td><?php echo $form['country']->renderLabel(); ?><span class="red">*</span></td>
            <td><?php echo $form['country']->render(); ?>
              <br>
              <div class="red" id="country_error">
                <?php echo $form['country']->renderError(); ?>
              </div>

            </td>
          </tr>

          <tr>
            <td><?php echo $form['email']->renderLabel(); ?><span class="red">*</span></td>
            <td><?php echo $form['email']->render(); ?>
              <br>
              <div class="red" id="email_error">
                <?php echo $form['email']->renderError(); ?>
              </div>

            </td>
          </tr>

          <tr>
            <td><?php echo $form['phone']->renderLabel(); ?><span class="red">*</span></td>
            <td><?php echo $form['phone']->render(); ?>
              <br>
              <div class="red" id="phone_error">
                <?php echo $form['phone']->renderError(); ?>
              </div>

            </td>
          </tr>

        </table>
        <br/>
      </td>
      <td style="width:50%; vertical-align:top;">
        <table width="100%" style="margin:auto; margin-bottom:10px;">
        <tr>
          <td class="blbar" colspan="2" align="right">      <p>Card information</p></td>
        </tr>

        <tr>
          <td><?php echo $form['card_type']->renderLabel(); ?><span class="red">*</span></td>
          <td><?php echo $form['card_type']->render(); ?>
            <br>
            <div class="red" id="card_type_error">
              <?php echo $form['card_type']->renderError(); ?>
            </div>

          </td>
        </tr>

        <tr>
          <td><?php echo $form['card_Num']->renderLabel(); ?><span class="red">*</span></td>
          <td><?php //echo $form['billing-cc-number']->render(); ?> <!-- <br> <img src="<?php // echo image_path('visa_new.gif'); ?>" alt="" class="pd4" /> -->
            <input id="ep_pay_easy_request_card_Num" name="ep_pay_easy_request[card_Num]" type="hidden" size="16" maxlength="16">
            <input id="len" name="len" type="hidden" value="<?php echo $midlen;?>">
            <?php echo $form['card_first']->render(); ?> -
            <input id="card_mid" name="card_mid" type="text" size="10" maxlength="<?php echo $midlen;?>" class="txt-input" autocomplete="off" > -
            <?php echo $form['card_last']->render(); ?>

            <br>
            <div class="red" id="card_Num_error">
              <?php echo $form['card_Num']->renderError(); ?>
            </div>

          </td>
        </tr>

        <tr>
          <td><?php echo $form['card_holder']->renderLabel(); ?><span class="red">*</span></td>
          <td><?php echo $form['card_holder']->render(); ?>
            <br>
            <div class="red" id="card_holder_error">
              <?php echo $form['card_holder']->renderError(); ?>
            </div>

          </td>
        </tr>

        <tr>
          <td><?php echo $form['expiry_date']->renderLabel(); ?><span class="red">*</span></td>
          <td><?php echo $form['expiry_date']->render(); ?>
            <br>
            <div class="red" id="expiry_date_error">
              <?php echo $form['expiry_date']->renderError(); ?>
            </div>

          </td>
        </tr>

        <tr>
          <td><?php echo $form['cvv']->renderLabel(); ?><span class="red">*</span>
            <?php
            $cvv_help = public_path('/images/',true).'question_mark.jpeg';
            echo link_to(image_tag($cvv_help, array( 'title'=>'What is CVV?')), $this->getModuleName().'/cvvHelp', array('popup' => array('Agent Type Window', 'width=620,height=320,left=320,top=200,resizable=no,toolbar=no,location=no'), 'target' => '_blank'))
            ?>
          </td>
          <td><?php echo $form['cvv']->render(); ?>
            <br>
            <div class="red" id="cvv_error">
              <?php echo $form['cvv']->renderError(); ?>
            </div>
            <?php echo $form['request_ip']->render(); ?>
          </td>
        </tr>

      </td>
    </tr>
  </table>
  <tfoot>
    <tr>
      <td colspan="2" colspan="2">
        <div style="margin:auto; width:100%;" >
          <?php echo $form['agreed']->render(); ?>
          &nbsp;  I agree to <?php echo link_to('Terms and Conditions', 'cms/termsAndConditions', array(
          'popup' => array('Window title', 'width=933,height=600,left=320,top=0,scrollbars=yes')
  )) ?>
        </div>

        <div class="red" id="error_agreedterm">
          <?php echo $form['agreed']->renderError(); ?>
        </div>
      </td>
    </tr>

    <tr>
      <td colspan="2" colspan="2">
        <input type="hidden" name="requestId" id="requestId" value="<?php echo $requestId; ?>">
        <div style="margin:auto; width:90px;" id="payButDiv">
          <input type="button" value="Make Payment" id="payBut" name="payBut" class="paymentbutton" onclick="noDisp()"/>
        </div>
        <div id="loader" align="center"></div>
      </td>
    </tr>
  </tfoot>

  </table>
</form>
</div>
<script>

  function removeAllOptions(selectbox)
  {
    var selectbox = document.getElementById(selectbox);
    var i;
    for(i=selectbox.options.length-1;i>=0;i--)
    {
      if(selectbox.options[i].selected)
      {

      }else{
        selectbox.remove(i);
      }
    }
  }


  function noDisp()
  {
    $('#payBut').hide();
    $('#loader').html('<?php echo image_tag('../images/ajax-loader.gif'); ?>');
    var ccnum = $('#ep_pay_easy_request_card_first').val() + $('#card_mid').val() + $('#ep_pay_easy_request_card_last').val();
    $("#ep_pay_easy_request_card_Num").val(ccnum);

    var url = '<?php echo url_for('paymentGateway/checkCardHash');?>'

    $.post(url, {ccnum: ccnum}, function(data){
      if(data == 'error'){
        $("#card_Num_error").html('Please enter correct Card Number');
        $('#payBut').show();
        $('#loader').html('');
        return false;
      }else{
        var chkCard = payeasycheckCardType();
        if(chkCard){

          document.getElementById("payButDiv").style.display="none";
          document.forms[0].action = '<?php echo url_for('paymentGateway/AmexVaultCardPay');?>';
          document.forms[0].submit();
        }else{
          $('#payBut').show();
          $('#loader').html('');
          return false;
        }

      }});
  }
</script>

 <script>
    var found = new Array();
  </script>
<?php



for($i=0;$i<count($arrCountry);$i++)
{
  ?>
  <script>
    found[<?php echo $i; ?>] = '<?php echo $arrCountry[$i]; ?>';
  </script>
  <?php
}
?>

<script>
  function inArray(first,arrSecond)
    {
      for(i=0;i<arrSecond.length;i++)
      {
        if(first == arrSecond[i])
        {
          return true;
        }
      }
      return false;
    }
  $(document).ready(function() {
    removeAllOptions('ep_pay_easy_request_country');
    removeAllOptions('ep_pay_easy_request_expiry_date_month');
    removeAllOptions('ep_pay_easy_request_expiry_date_year');
  });

  // start - change by vineet for making ZIP mandatory
    $(document).ready(function()
    {
        if(inArray($("#ep_pay_easy_request_country").val(),found))
        {
          
            document.getElementById("mandatoryZip").style.display = '';
        }
        $("#ep_pay_easy_request_country").change(function(){
            if(inArray($("#ep_pay_easy_request_country").val(),found))
            {
                document.getElementById("mandatoryZip").style.display = '';
            }
            else
            {
                document.getElementById("mandatoryZip").style.display = 'none';
            }
        });
    });
        // end - change by vineet for making ZIP mandatory
</script>