

<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
<div class="content_container">
    <?php use_helper('Form');?>
    <?php include_partial('global/innerHeading',array('heading'=>'Payment Options'));?>
    <div class="clear"></div>
    <div class="tmz-spacer"></div>
    <?php

    //echo "<br/>";
    $sf = sfContext::getInstance()->getUser();
    if($sf->hasFlash('notice')){
        ?><div id="flash_notice" class="alertBox" ><?php
        echo nl2br($sf->getFlash('notice'));

        ?></div><?php }?>

    <!--p class="flRight">*Required Information</p><br/-->
    <h2 class="highlight yellow">You are authenticated for SW Global LLC Account.</h2>
    <div class="clear">&nbsp;</div>
    <br/>
    <div class="clearfix"></div>
    <table style="width: 100%;">
        <b>Below is the list of your registered cards. Please select a card from the list to continue with the transaction. You must select a registered card if you want to use the card for multiple transactions.</b>
    </table>
    <br/>


    <?php echo form_tag(url_for('paymentGateway/continueChoice'),array('name'=>'vault','id'=>'vault','onsubmit'=>"return radio_button_checker(vault)")); ?>
    <table style="width: 100%;" class="card_details">
        <tbody>
            <?php for($i=0;$i<count($applicantVault);$i++) {

                $cardFirst = $applicantVault[$i]['card_first'];
                $cardLast =  $applicantVault[$i]['card_last'];
                $cardLen =  $applicantVault[$i]['card_len'];
                $cardType =  $applicantVault[$i]['card_type'];
                $count = $cardLen -(count($cardFirst) + count($cardLast));
                $display = $cardFirst. '-XXXX-'. $cardLast;
                if(isset($applicantVault[$i+1]) && $applicantVault[$i+1] !=''){
                    $cardFirstNext = $applicantVault[$i+1]['card_first'];
                    $cardType1 =  $applicantVault[$i+1]['card_type'];
                    $cardLastNext =  $applicantVault[$i+1]['card_last'];
                    $cardLenNext =  $applicantVault[$i+1]['card_len'];
                    $count = $cardLenNext -(count($cardFirstNext) + count($cardLastNext));
                    $displayNext = $cardFirstNext. '-XXXX-'. $cardLastNext;

                    ?>
            <tr>
                <td width="50%" valign="top" ><table style="width: 100%;" class="card_details border_table">
                        <tr>
                            <td colspan="2" class="blbar">Card Details <?php echo $i+1;?></td>
                        </tr>
                        <tr>
                            <td class="label_name">Card Number:</td>
                            <td><?php echo $display;?></td>
                        </tr>
                        <tr>
                            <td class="label_name">Card Type:</td>
                            <?php if($cardType=='A'){ ?>
                            <td><?php echo "American Express";?></td>
                            <?php } ?>
                        <?php if($cardType=='V'){ ?>
                            <td><?php echo "Visa Card";?></td>
                            <?php } ?>
                        <?php if($cardType=='M'){ ?>
                            <td><?php echo "Master Express";?></td>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td class="label_name">Card Holder Name:</td>
                            <td><?php echo $applicantVault[$i]['card_holder']?></td>
                        </tr>
                        <tr>
                            <td class="label_name">Card Expiry:</td>
                            <?php $month = array('January','February','March','April','May','June','July','August','September','October','November','December') ; ?>
                            <td><?php echo  $month[$applicantVault[$i]['expiry_month']-1]." ".$applicantVault[$i]['expiry_year'] ?></td>
                        </tr>
                        <tr>
                            <td class="label_name"> <b>Select Card</b></td>
                            <td class="label_name"><?php echo radiobutton_tag('paymentMode', base64_encode($applicantVault[$i]['id']),1) ?></td>
                        </tr>
                </table></td>

                <td width="50%" valign="top" ><table style="width: 100%;" class="card_details border_table">
                        <tr>
                            <td colspan="2" class="blbar">Card Details <?php echo $i+1+1;?></td>
                        </tr>
                        <tr>
                            <td class="label_name">Card Number:</td>
                            <td><?php echo $displayNext;?></td>
                        </tr>
                        <tr>
                            <td class="label_name">Card Type:</td>
                            <?php if($cardType1=='A'){ ?>
                            <td><?php echo "American Express";?></td>
                            <?php } ?>
                        <?php if($cardType1=='V'){ ?>
                            <td><?php echo "Visa Card";?></td>
                            <?php } ?>
                        <?php if($cardType1=='M'){ ?>
                            <td><?php echo "Master Express";?></td>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td class="label_name">Card Holder Name:</td>
                            <td><?php echo $applicantVault[$i+1]['card_holder']?></td>
                        </tr>
                        <tr>
                            <td class="label_name">Card Expiry:</td>
                            <?php $month = array('January','February','March','April','May','June','July','August','September','October','November','December') ; ?>
                            <td><?php echo  $month[$applicantVault[$i+1]['expiry_month']-1]." ".$applicantVault[$i+1]['expiry_year'] ?></td>
                        </tr>
                        <tr>
                            <td class="label_name"> <b>Select Card</b></td>
                            <td class="label_name"><?php echo radiobutton_tag('paymentMode', base64_encode($applicantVault[$i+1]['id']),1) ?></td>
                        </tr>
                </table></td>
                <?php $i++;?>
            </tr>
            <?php  }else{ ?>
            <tr>
            <td  width="50%" valign="top"><table style="width: 100%;" class="card_details border_table">
                    <tr>
                        <td colspan="2" class="blbar">Card Details <?php echo $i+1;?></td>
                    </tr>
                    <tr>
                        <td class="label_name">Card Number:</td>
                        <td><?php echo $display;?></td>
                    </tr>
                    <tr>
                        <td class="label_name">Card Type:</td>
                        <?php if($cardType=='A'){ ?>
                        <td><?php echo "American Express";?></td>
                        <?php } ?>
                    <?php if($cardType=='V'){ ?>
                        <td><?php echo "Visa Card";?></td>
                        <?php } ?>
                    <?php if($cardType=='M'){ ?>
                        <td><?php echo "Master Express";?></td>
                        <?php } ?>
                    </tr>
                    <tr>
                        <td class="label_name">Card Holder Name:</td>
                        <td><?php echo $applicantVault[$i]['card_holder']?></td>
                    </tr>
                    <tr>
                        <td class="label_name">Card Expiry:</td>
                        <?php $month = array('January','February','March','April','May','June','July','August','September','October','November','December') ; ?>
                        <td><?php echo  $month[$applicantVault[$i]['expiry_month']-1]." ".$applicantVault[$i]['expiry_year'] ?></td>
                    </tr>
                    <tr>
                        <td class="label_name"> <b>Select Card</b></td>
                        <td class="label_name"><?php echo radiobutton_tag('paymentMode', base64_encode($applicantVault[$i]['id']),1) ?></td>
                    </tr>
            </table></td>
            <tr>
                <td width="50%" valign="top"><table style="width: 100%;" class="card_details border_table">




                        <div>
                            <table>
                                <b><?php if (!$isRegisteredCardCountry || $isPrivilegeUser) { ?> <br>Please select 'Use Another Payment Option' if you want to use a different payment option for this transaction. <?php } ?> </b>
                            </table>
                        </div>


                                <?php if(!$isRegisteredCardCountry || $isPrivilegeUser){?>
                        <tr>

                            <td width="50%" valign="top">
                                <table style="width: 100%;" class="card_details border_table">
                                    <tr><td colspan="2" class="blbar"> Other Payment Options</td>

                                    </tr>
                                    <tr>
                                        <td width="50%" valign="top" class="label_name"> <b>Use another Payment Option</b></td>
                                        <td width="50%" valign="top" class="label_name"><?php echo radiobutton_tag('paymentMode', base64_encode('againcc'),1) ?> </td>
                                    </tr>
                                </table>



                            </td>
                        </tr>

                                    <?php } ?>





                </table></td>
            </tr>



                    <?php }?>
                <?php //echo formRowComplete($display,radiobutton_tag('paymentMode', base64_encode($val['id']),1),'','aExpress','err_aExpress','aExpress_row'); ?>

                <?php }

            if(!empty($applicantVault) && count($applicantVault)%2 == 0){
                if(!$isRegisteredCardCountry || $isPrivilegeUser){

                    ?>
            <tr>
                <td width="50%" valign="top"><table style="width: 100%;" class="card_details border_table">
                        <div>
                            <table>
                                <b><?php if (!$isRegisteredCardCountry || $isPrivilegeUser) { ?> <br>Please select 'Use Another Payment Option' if you want to use a different payment option for this transaction. <?php } ?> </b>
                            </table>
                        </div>
                        <tr>
                            <td  width="50%" valign="top">
                                <table width="100%" class="card_details border_table">
                                    <tr>
                                        <td colspan="2" class="blbar">Other Payment Options  </td>
                                    </tr>

                                    <tr>
                                        <td width="50%" valign="top" class="label_name">Use another Payment Option</td>
                                        <td width="50%" valign="top"><?php echo radiobutton_tag('paymentMode', base64_encode('againcc'),1) ?> </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                </table><?php } } ?></td>
            </tr>
            
        </tbody>
    </table>




<div class="clear">&nbsp;</div>
<div align="center"><input type="submit" onclick="" value="Next" class="normalbutton">
</div>
</div>
</div>
<div class="content_wrapper_bottom"></div>


<script language="JavaScript">

    function radio_button_checker(vault)
    {
        // set var radio_choice to false
        var radio_choice = false;

        var len = document.vault.elements.length;

        // Loop from zero to the one minus the number of radio button selections
        for (counter = 0; counter < len; counter++)
        {
            if(document.vault.elements[counter].type == 'radio'){
                if (document.vault.elements[counter].checked){
                    radio_choice = true;
                }
            }
        }
        if (!radio_choice)
        {
            // If there were no selections made display an alert box
            alert("Please select at least one option.")
            return (false);
        }

        return true;
    }
</script>

