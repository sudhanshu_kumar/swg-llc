<?php
use_javascript('dhtmlwindow.js');
use_stylesheet('dhtmlwindow.css');
use_javascript('modal.js');
use_stylesheet('modal.css');

?>
<?php include_partial('global/innerHeading',array('heading'=>'Payment status'));?>
<div class="global_content4 clearfix">
<?php
$sf = sfContext::getInstance()->getUser();
if($sf->hasFlash('notice')){
  ?><div id="flash_notice" class="alertBox" ><?php
  echo nl2br($sf->getFlash('notice'));

  ?></div><?php }?>
<br />
<div class="">

<?php //include_partial('orderDetail', array('request_details'=>$request_details)) ?>
<?php include_partial('paymentGateway/orderDetail', array('request_details'=>$request_details, 'application_type' => $application_type)) ?>
  
</div>
</div>