<table width="100%">
    <tr>
        <td class="blbar" colspan="3">Recharge Details - <?php //echo $request_details->getMerchant()->getAddress();?> </td>
    </tr>
    <tr>
        <td width="77%" class="txtBold"><?php echo _('Recharge Amount');?></td>
        <td width="23%" class="txtBold txtRight"><?php  echo format_amount($request_details->getItemFee(),1);?></td>
    </tr>

<?php
if($request_details->getServiceCharge()!=0)
{
?>
    <tr>
        <td class="txtBold"><?php echo _('Service Charges ');?> </td>
        <td class="txtRight"><?php echo format_amount($request_details->getServiceCharge(),1);?></td>
    </tr>
<?php
}
$smsCharge = $request_details->getSfGuardUser()->getUserDetail()->getSmsCharge();
if($smsCharge!=0)
{
?>
    <tr>
        <td class="txtBold"><?php echo _('SMS Charges ');?> </td>
        <td class="txtRight"><?php echo format_amount($smsCharge,1);?></td>
    </tr>
<?php
}
?>
    <tr class="blTxt">

        <td class="txtBold">&nbsp;</td>
        <td class="txtRight blTxt"><span class="bigTxt">Total: <?php echo  format_amount($request_details->getAmount(),1);?></span></td>
    </tr>
</table>
