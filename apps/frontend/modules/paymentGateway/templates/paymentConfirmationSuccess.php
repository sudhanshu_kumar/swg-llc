<?php
use_javascript('dhtmlwindow.js');
use_stylesheet('dhtmlwindow.css');
use_javascript('modal.js');
use_stylesheet('modal.css');
use_javascript('countdown.js');

$sec = sfConfig::get('app_payment_success_time_out_limit');
$currentTime = date('i',mktime(0,0,$sec,0,0,0));
?>
<style>
.NwPopUp	{ width:500px; height:350px; background:#fff; margin:10px auto;}
.NwHeading	{ color:#CC9900; border-bottom:solid 1px #CC9900; font-size:19px; padding:10px 5px; font-weight:bold}
.NwMessage	{ padding:10px 5px; background:#fdf8c3; color:#996600;}
.NsPd5		{ padding:10px 5px}
#clockwrapper	{ margin:10px; text-align:center}
#countdown { font-size:19px; display:block}
</style>

<?php
$content = '<div class="NwPopUp"><div class="NwHeading">'.image_tag('/images/favicon.ico',array('alt'=>'SWGlobal LLC')).' Payment Authorization</div>';
$content .= '<div class="NwMessage">'.image_tag('/images/success.png',array('align'=>'absmiddle', 'alt' => 'success')).' Your Payment has been authorized from bank.</div>';
$content .= '<div class="NsPd5"><br />If you still want to continue with payment, please wait for <b><u>'.$currentTime.' Minute(s)</u></b> while we are updating your Application(s) status at Nigeria Immigration Service end. <br /><br />';
$content .= '<br /><br /><p>Otherwise click on <b>Cancel Transaction</b> button given below, to cancel this transaction.</p>';
$content .= '<div align="center" style="padding-top:25px;"><a href="javascript:void(0);" border="0">'.image_tag('/images/cancel.jpg',array('alt'=>'Cancel Transaction', 'id' => 'cancel')).'</a></div>';
$content .= '<div class="NwMessage" id="confirmationDiv" style="width:300px;display:none; margin:5px auto;border:solid 1px #CC9900;"><div>'.image_tag('/images/question_mark.jpeg',array('align'=>'absmiddle', 'alt'=>'Success')).' <strong>Do you really want to cancel this transaction?</strong></div>';
$content .= '<div align="right" style="padding-right:35px;padding-top:10px;"><span id="okcancel"><a href="javascript:void(0);" id="okConf" >OK</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" id="cancelConf">Cancel</a></span><span id="waitid" style="display:none;">Please Wait...</span></div>';
$content .= '</div>';
$content .= '</div></div>';
?>

<?php echo ePortal_PaymentSuccess_popup($content,array('width' => 600, 'height' => 400)); ?>
<?php echo "<script>pop();</script>"; ?>

<?php include_partial('global/innerHeading',array('heading'=>'Payment Authorization'));?>
<div class="global_content4 clearfix">
    <div style="height:400px;">
        <?php echo $content; ?>
    </div>    
    <div class="NwMessage" style="width:300px;padding-left:50px;">
        <div><?php echo image_tag('/images/question_mark.jpeg',array('alt'=>'Success')); ?> Are you really want to cancel this transaction?</div>
        <div align="right" style="padding-right:35px;padding-top:10px;"><a href="javascript:void(0);" id="okConf" >OK</a>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" id="cancelConf">Cancel</a></div>
    </div>
</div>
<script>
       if($('#mainDiv').length > 0){
           $('#clock').html('done');
           var currentTime = '<?php echo $currentTime;?>';
           var cd1 = new countdown('cd1');
           cd1.Div	= "clock1";
           cd1.TargetDate	= "12/31/2020 5:"+ currentTime +":00 AM" ;//currentTime;
           cd1.DisplayFormat = "<span id='countdown_message'>You will be redirected after <b><span id='countdown'> %%M%% mins, %%S%% secs </span></b></span><br/><br /><i><b><font color='RED'>Important Notes:</font></b> Do not press refresh and back button.</i>";
           cd1.Setup();
       }

       function goToElseWhere( ){
           window.location.href ='<?php echo url_for('paymentGateway/applicationUpdate') . '/valid/' . $valid  ?>';
       }

       $(document).ready(function(){
           $('#cancel').click(function (){
               $('#cancel').fadeOut('fast');
               $('#confirmationDiv').fadeIn('fast');
           });
       });

       $(document).ready(function(){
           $('#okConf').click(function (){
               $('#okcancel').hide();
               $('#waitid').show();
               cd1.CountActive = false;
               window.location.href ='<?php echo url_for('paymentGateway/refund') . '/valid/' . $valid  ?>';
           });
       });

       $(document).ready(function(){
           $('#cancelConf').click(function (){
               $('#cancel').fadeIn();
               $('#confirmationDiv').fadeOut();               
           });
       });
</script>