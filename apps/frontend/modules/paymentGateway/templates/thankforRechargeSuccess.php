<?php include_partial('global/innerHeading',array('heading'=>'Recharge Status'));?>

<div class="global_content4 clearfix">
  <div class="thanksArea">
    <p> <h2><span class="approveInfo"></span>&nbsp;Thanks <?php echo ucfirst($showPaymentDetails['fname'])." ". ucfirst($showPaymentDetails['lname']);?>, you're done!</h2>
    <span class="lspace"></span>Your account has been recharged with  <b><?php echo format_amount($showPaymentDetails['item_fee'],1);?></b></p>

    <p><h3><span class="lspace"></span>How do I track my recharge?</h3>
    <span class="lspace"></span><?php echo link_to('Get up-to-date recharge progress', url_for('ewallet/acctStmtSearch'));?> on swgloballlc.com</p>
  </div>
</div>