<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">
 <?php if($MoFlag){
      include_partial('global/innerHeading',array('heading'=>'Payment Information'));
    }  else {
      include_partial('global/innerHeading',array('heading'=>'Card Registration Information'));
        }
     ?>
<?php 
$backButton = 'Continue for Card Registeration';
$moButton = 'Continue for Money Order Payment';
?>
    <div class="clear"></div>
    <div class="tmz-spacer"></div>
<div class="payment_info_area">
<?php if(trim($requestId) != ''){ ?>
<?php if($MoFlag){?>
Your Tracking number will be generated for doing payment through Money order, click "<b><?php echo $moButton; ?></b>" button to proceed further.
<?php } ?>
We do allow credit card payments for immediate family members. In such cases your surname must match with that of ALL the applicant's in the cart. This transaction can be done once in six months.

        In case, you'd like to pay using your credit card, follow the instructions given below:
      <ol class="card_reg_ol">
<li> Your surname must match with that of all the applicant's surname in the cart. </li>
<li>If your surname doesn't matches with your family member(s) surname, you can still use your credit card for payment however you'd have to register your card with us
  <?php if($isRegistered){ ?>
  or <a href="<?php echo url_for('cart/list') ?>"><b>CLICK HERE</b></a> to make payment using your registered cards
  <?php } ?>
  . Register card process will help us to skip various verifications against the card owner against each transaction. Following are the steps to register your card with SWGlobal LLC: <br/>
  - Click on "<b><?php echo $backButton; ?></b>" button below. <br/>
  - Fill out the request form.
  <div style="padding-left:40px;">
    <ul type="a">
      <li> You are required to produce proof of your name and billing address as associated with your credit card. A scanned copy of your latest credit card statement shall suffice for this purpose. In case your credit card statement displays the full digits of your card number on it, please ensure to hide (by overwriting it with dark black pen) the 8 middle digits of your credit card leaving only the first four and last four digits visible. For example, if your credit card statement shows your credit card number 1234-5678-5678-9876, please mask 5678-5678 so that it appears as 1234-XXX-XXX-9876 in the scanned copy that you upload for our review.</li>
      <li> Please ensure that the phone number you have provided is a valid phone number on which you are reachable, we may contact you on this phone number should we need any more information before approving your account. Failure to provide a valid phone number will result into rejection of your request.</li>
    </ul>
  </div>
  - Submit it for approval. You'll be sent a notification email confirming your submission. <br/>
  - Once your request is processed, a notification email will be sent to you for confirmation and next steps. <br/>
  - On successful approval you will be able to use your card for multiple payments on SWGlobal LLC platform. </li>
<li>After getting confirmation for card, please Click on <b>CART</b> button at the top right on SWGlobalLLC portal and proceed for payment through registered card.</li>
<?php
$paymentCardTypeArrayCount = count($paymentCardTypeArray);
if($paymentCardTypeArrayCount > 0){
    $str = '';
    foreach($paymentCardTypeArray AS $val){
        if('m' == strtolower($val)){
            $str .= '<li> MASTER</li>';
        }else if('v' == strtolower($val)){
            $str .= '<li> VISA</li>';
        }else if('a' == strtolower($val)){
            $str .= '<li> AMEX</li>';
        }
    }
}
?>
<?php if($paymentCardTypeArrayCount > 0){ ?>
        <li> <strong>You can register either of following card type(s) for making payment:</strong>
          <div class="card_reg_ol_40">
        <ul type="a">
          <?php echo $str; ?>
        </ul>
      </div>
    </li>
<?php } ?>
</ol>
      <div align="center">
        <input type="button" class="normalbutton" value="<?php echo $backButton; ?>" onclick="javascript:window.location.href='<?php echo url_for('userAuth/registerCreditCard');?>';" />
        <?php if($MoFlag){?>  
        <input type="button" class="normalbutton" value="<?php echo $moButton; ?>" onclick="javascript:window.location.href='<?php echo url_for('paymentGateway/moInfo?cf=po');?>';" />
        <?php } ?>
    </div>
<!--div class="payment_info_wrapper">
  <div>
    <input type="button" onclick="javascript:window.location.href='<?php //echo url_for('userAuth/registerCreditCard');?>';" value="Register Card" class="button">
  </div>
  <div class="lblButtonRight">
    <div class="btnRtCorner"></div> 
  </div>
  <?php //if($MoFlag){?>
  <div class="continue_payment_btn">
  <div>
    <input type="button" onclick="javascript:window.location.href='<?php //echo url_for('mo_configuration/trackingProcess?requestId='.$requestId);?>';" value="<?php echo $continue; ?>" class="loginbutton">
  </div>
  </div>
  <?php //} ?>
</div-->
<div class="clear"></div>
<?php } else { ?>
<p class="redBigMessage" >Oops! You have tempored url from somewhere.</p>
<br />
<?php } ?>
</div>
</div>
</div>
<div class="content_wrapper_bottom"></div>
