<?php include_partial('global/innerHeading',array('heading'=>'eWallet Authentication'));
$sf = sfContext::getInstance()->getUser();
if($sf->hasFlash('notice')){
  ?><div id="flash_notice" class="alertBox" ><?php
  echo nl2br($sf->getFlash('notice'));

  ?></div><?php }?>
<div class="wrap">

<?php include_partial('form', array('form' => $form,'message'=>$message,'kycStatus'=>$kycStatus)) ?>

</div>