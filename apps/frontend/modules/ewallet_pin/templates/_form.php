<?php use_helper('Form');
use_helper('Pagination');?>


<?php
use_javascript('dhtmlwindow.js');
use_stylesheet('dhtmlwindow.css');
use_javascript('modal.js');
use_stylesheet('modal.css');
?>
<script>
    function openInformationWindow(){
        var url = '<?php echo url_for('ewallet_pin/openInfoBox') ?>';
        emailwindow=dhtmlmodal.open('EmailBox', 'iframe', url, 'Confirmation Message', 'width=530px,height=335px,center=1,border=0, scrolling=no');


        emailwindow.onclose=function(){ 
               window.location.reload();
        }

  
    }
</script>


<div class="global_content4 clearfix"> 
   

    <div class="brdBox"> <?php //echo ePortal_pagehead('Change Password'); ?>
        <div class="clearfix"/>
        <?php echo form_tag('ewallet_pin/create',array('name'=>'frm_ewallet_pin','class'=>'dlForm', 'method'=>'post','id'=>'frm_ewallet_pin')) ?>
        <div class="wrapForm2">
<?php
if($kycStatus==1)
{
    ?>


<h2 class="successBox">If you wish to pay your unpaid bills, you can do it through Bill -> Unpaid Bills after recharging your eWallet account for multiple payments or a credit card for a single payment.</h2>

<div>
<div style="margin-left:250px;">
<div style="float:left;"><?php echo image_tag('/images/certificateicon.jpg',array('alt'=>'SW Global LLC')); ?></div>
<div style="float:left; width:300px; font-size:14px; padding:5px;">Congratulations! Your application for SW Global LLC eWallet Secure Certificate has been approved. You can now use your eWallet Payment facility.</div>
</div>
<?php
}else{
?>

 <h2 class="successBox">It might take 15 to 20 minutes for the PIN to get delivered on your mobile.
   <br>To activate your account go to PIN Management->Enter PIN.
   <br>If you have not received your PIN  in  15 to 20  minutes, please go to PIN Management->Generate PIN.
   </h2>

             <fieldset class="multiform wd90">
                <legend>eWallet Authentication</legend>
                <div class="wrapForm2">

                    <?php echo $form ?>
                    <div class="mgauto"><?php   echo submit_tag('Submit',array('class' => 'paymentbutton')); ?>
                        

                    </div>
                </div>
            </fieldset>
             <table width="90%"  style="margin:15px auto">
               
                <?php
                if($message=='0')
                {
                ?>
                <tr>
                  <td colspan='2'>
                     <?php //echo link_to('Click here to change your mobile number.', '', array('onClick' => 'openInformationWindow();')) ?>
                     <a href="Javascript:void(0);" onclick="openInformationWindow();">Click here to change your mobile number</a>


                  </td>
                </tr>
                <?php
                }
                ?>
            </table>
            <br />
            <?php
            if($message=='1')
            {
                ?>
            <div class="lblButton">
                <input type="button" class= "button" name="regeneratePin" value="Regenerate Pin" onclick="window.location='<?php echo url_for('ewallet_pin/pinUpdation') ?>'" >
            </div>
            <div class="lblButtonRight">
                <div class="btnRtCorner"></div>
            </div>
            <?php
        }
}
        ?>
            
    </div>

        </form>
        </div>
    </div>
</div>

