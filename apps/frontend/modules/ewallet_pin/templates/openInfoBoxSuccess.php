<?php use_helper('Form');?>
<?php use_helper('Url');?>
<?php include_javascripts() ?>
<style type="text/css">

.popupBody
{background-color:#ccc;margin:0px auto;padding:15px;font-family:arial;}
.popupContent {height:auto;margin:0px auto;width:423px; *width:100%; font-size:12px;color:#333333;background-color:#fff;padding:10px;font-family:arial;z-index:1000;}
.flLft
{float:left;font-family:arial;}
.flRt
{float:right;font-family:arial; margin-top:15px;}
.greenButton {
background:#6998b0 url(../../images/buttonBg.gif) repeat-x scroll left top;border:1px solid #3e7693;color:#FFFFFF;cursor:pointer;font-family:arial,verdana,helvetica,sans-serif;font-size:12px;height:22px;width:130px;padding:0 6px;text-align:center; font-weight:bold;}
ul.pBold
{
font:bold 11pxArial, Helvetica, sans-serif;list-style-type:none;padding:0px;margin:10px 0 0 0px;
}
ul.fontNor, ul.fontNor li
{font:normal 12px Arial, Helvetica, sans-serif!important;}
ul.pBold li
{
font:bold 11px Arial, Helvetica, sans-serif;list-style-type:none;background:transparent url(../../images/bulletArrow.gif) no-repeat left -1px;padding-left:15px;text-align:left;padding-bottom:10px;
}
H3 {
	color: #3e7693;
	font-weight: bold;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 15px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 3px;
	margin-left: 0px;
	text-transform: capitalize;
	padding: 0px;
}
.clear {
	clear: both;
}
iframe {overflow:hidden;}
</style>

<body id='popupContent'>
<div class="popupBody">
  <div  class="popupContent">
    <h3>Change Your Mobile Number</h3>

    <table>
     <tr>
      <td style="font-family:arial;font-size:11px;">Mobile Number</td>
      <td><?php echo input_tag('mobile_no', '', 'class=txt-input maxlength=30'); ?>
      <br>      
     <div style="font-family:arial;font-size:11px;color:red;" id="err_mobile_no"></div>
      </td>
     </tr>
     <tr>
      <td align="right" colspan="2" ><div class="flRt"><?php echo button_to('Proceed', '', array('class' => 'greenButton', 'onClick' => 'changeMobileNumber();')) ?> &nbsp;&nbsp; <input name="" type="button" value="Close" class="greenButton" onClick="parent.emailwindow.onclose();" /></div></td>
     </tr>
    </table>

    <div class="pBold">
        <?php echo htmlspecialchars_decode('Please enter valid mobile number, it may be used for verification.');?>

    </div>
    <!--<div class="flLft"><input name="" type="checkbox" value="" id="info_check"/> I have read the Broadcast message</div> -->
   
    <div class="clear"></div>
  </div>
</div>
</body>
</html>
<script>
  //console.log(document.getElementById('popupContent'));
nW = document.getElementById('popupContent').offsetWidth;
nH = document.getElementById('popupContent').offsetHeight;
// console.log(nW+' : '+nH);
window.parent.emailwindow.setSize(nW+20,nH+20);



//function to change mobile number

function changeMobileNumber(){
   var mobile = $("#mobile_no").val();
   var err = 0;


   if(jQuery.trim(mobile) == "")
        {
            if(validatePhone(mobile))
            {
                $('#err_mobile_no').html("Please enter Mobile number");
                err = err+1;
            }
            else
            {
                $('#err_mobile_no').html("");
            }
        }

        if(mobile != "")
        {
            if(validatePhone(mobile))
            {
                $('#err_mobile_no').html("Please enter Valid Mobile number <br/>Mobile Number should be between 10-14 digits, eg: +1234567891");
                err = err+1;
            }
            else
            {
                $('#err_mobile_no').html("");
            }
        }


  if(err == 0){
   var url = '<?php echo url_for('ewallet_pin/updatePhoneNumber') ?>';
   $.post(url, { mobile_number: mobile },
   function(data){
     if(data == 'duplicate_entry'){
         $('#err_mobile_no').html("This Mobile Number already exists.");
     }else{
        $('#err_mobile_no').html("");
       parent.emailwindow.onclose();
     }
   });

   }else{
       return false;
   }

}





</script>