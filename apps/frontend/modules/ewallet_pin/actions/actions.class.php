<?php

/**
 * ewallet_pin actions.
 *
 * @package    ama
 * @subpackage ewallet_pin
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ewallet_pinActions extends sfActions
{
    

    public function executeNew(sfWebRequest $request)
    {
        $userId = sfContext::getInstance()->getUser()->getAttribute('user_id', null, 'sfGuardSecurityUser');
        $user_detail = Doctrine::getTable('UserDetail')->findByUserId($userId);
        $this->kycStatus = $user_detail->getFirst()->getKycStatus();
        if($request->getParameter('response')!='')
        {
            $this->message=$request->getParameter('response');
        }
        else
        {
            $this->message='';
        }      

        $this->form = new EwalletPinForm();
    }



      public function executeOpenInfoBox(sfWebRequest $request)
      {  
        $this->setTemplate('openInfoBox');
        $this->setLayout(false);
    }


    public function executeUpdatePhoneNumber(sfWebRequest $request){
       $this->setLayout(false);
       if($this->getUser()->isAuthenticated() && $request->getPostParameter('mobile_number') != ''){

        $userId = $this->getUser()->getGuardUser()->getUserDetail()->getId();

        $countMobile = Doctrine::getTable('UserDetail')->checkUniqueMobileNumber($userId,$request->getPostParameter('mobile_number'));
        if($countMobile == 0){
            $mobileNumber = $request->getPostParameter('mobile_number');
            $userDetaills = Doctrine::getTable('userDetail')->find($userId);
            $userDetaills->setMobilePhone($mobileNumber);
            $userDetaills->save();
            $this->getUser()->setFlash('notice', 'Please re-generate your pin.');
            //$this->redirect('ewallet_pin/new');
        }else{
            echo "duplicate_entry";
        }
       }
       die;
    }



    public function executeCreate(sfWebRequest $request)
    {
        $userId = sfContext::getInstance()->getUser()->getAttribute('user_id', null, 'sfGuardSecurityUser');
        $user_detail = Doctrine::getTable('UserDetail')->findByUserId($userId);
        $this->kycStatus = $user_detail->getFirst()->getKycStatus();
        $this->message='';
        $this->forward404Unless($request->isMethod(sfRequest::POST));

        $this->form = new EwalletPinForm();

        $this->processForm($request, $this->form);
        $this->setTemplate('new');
    }

   

    protected function processForm(sfWebRequest $request, sfForm $form)
    {
        $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
        if ($form->isValid())
        {
            $pinObj=new pin();
            $ewalletDetail=$pinObj->getEwalletDetail();
            $activatedTill=$ewalletDetail->getActivatedTill();
            if($activatedTill < date('Y-m-d H:i:s'))
            {
                $this->getUser()->setFlash('notice', sprintf('Your pin has been expired.Please regenerate the pin'));
                $this->redirect('ewallet_pin/new?response=1');
            }else if($ewalletDetail->getStatus()=='blocked')
            {
                $this->getUser()->setFlash('notice', sprintf('Your pin has been blocked.Please regenerate the pin'));
                $this->redirect('ewallet_pin/new?response=1');
            }
            else
            {
                $this->getUser()->setFlash('notice', sprintf('User Authenticated Successfully'));
                //            $ewallet_pin = $form->save();
                $userId = sfContext::getInstance()->getUser()->getAttribute('user_id', null, 'sfGuardSecurityUser');
                $pinObj->setKycStatus('2',$userId);
                $this->forward('ewallet_pin', 'new');
            }
        }
    }
    public function executePinGeneration()
    {
        $userId = sfContext::getInstance()->getUser()->getAttribute('user_id', null, 'sfGuardSecurityUser');
        $pinObj=new pin();
        $smsDetail=$pinObj->getPin($userId);
        switch($smsDetail)
        {
            case 'NO_RECIPIENTS':
                $this->getUser()->setFlash('notice', sprintf('SMS could not be sent. Please enter a valid mobile number'));
                $this->redirect('ewallet_pin/new?response=0');
                break;
            case 'SEND_OK':
                $this->getUser()->setFlash('notice', sprintf('Activate account via pin that is send to your mobile'));
                $this->redirect('ewallet_pin/new?response=1');
                break;
        }

    }
    public function executePinUpdation()
    {
        $userid1 = $this->getUser()->getGuardUser()->getId();
        $pinobj = Doctrine::getTable('EwalletPin')->findByUserId($userid1);
        $pinGetObj  = $pinobj->getfirst();
        if($pinGetObj)
        {
            $pinObj=new pin();
            $ewalletDetail=$pinObj->getEwalletDetail();
            $noOfRetries=$ewalletDetail->getNoOfRetries();
            if($noOfRetries==Settings::getMaxNoOfRetries())
            {
                $this->getUser()->setFlash('notice', sprintf('You have attempted maximum number of retries. Now your account can be activated by admin'));
                $this->redirect('ewallet_pin/new');
            }
            else
            {
                $activatedTill=$pinObj->getActivatedTill();
                $pin = substr(rand(),0,4);
                //$pin=1111;
                $ewalletDetail->setPinNumber($pinObj->getEncryptedPin($pin));
                $ewalletDetail->setNoOfRetries($noOfRetries+1);
                $ewalletDetail->setActivatedTill($activatedTill);
                $ewalletDetail->setStatus('active');
                $ewalletDetail->setPinRetries(0);

                $ewalletDetail->save();
                $userId = sfContext::getInstance()->getUser()->getAttribute('user_id', null, 'sfGuardSecurityUser');
                $user_detail = Doctrine::getTable('UserDetail')->findByUserId($userId)->getFirst();
                $mobileNumber=$user_detail->getMobilePhone();
                $smsDetail=$pinObj->sendSms($mobileNumber, $pin);
                switch($smsDetail)
                {
                    case 'NO_RECIPIENTS':
                        $this->getUser()->setFlash('notice', sprintf('SMS could not be sent. Please enter a valid mobile number'));
                        $this->redirect('ewallet_pin/new?response=0');
                        break;
                    case 'SEND_OK':
                        $this->getUser()->setFlash('notice', sprintf('Activate account via pin that is send to your mobile'));
                        $this->redirect('ewallet_pin/new?response=1');
                        break;
                }

            }
        }
        else{
            $this->forward($this->getModuleName(), 'pinGeneration');
        }

    }
}
