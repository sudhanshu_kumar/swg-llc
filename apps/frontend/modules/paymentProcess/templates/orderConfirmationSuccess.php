<?php
include_partial('global/innerHeading',array('heading'=>'Order Confirmation'));?>
<div class="global_content4 clearfix">
<?php if(!$err) {?>
<?php include_partial('orderDetail', array('request_details'=>$request_details)) ?>
  <br />
  <div class="importantArea"><div class="importantNotice">(No card information is captured/stored by SW Global LLC)</div></div>
<br />



<?php
include_partial('orderConfirmation', array('form'=>$form,'requestId'=>$requestId));

}
else {
echo "<tr><td class='error'>".$err."</td></tr>";
 }?>
</div>

