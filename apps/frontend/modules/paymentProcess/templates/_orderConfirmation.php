<form name="payment" id="payment" action="<?php echo url_for('paymentProcess/paymentRequest');?>" method="post" autocomplete="off" onsubmit="return CheckCardNumber();" >


  <table width="50%">
    <tbody>
     <tr>
        <td class="blbar" colspan="2">Enter a payment method</td>
     </tr>

     <tr>
       <td colspan="2" align="right">      <p class="red">*Required Information</p></td>
     </tr>

  <div style="display:none;" id="cardTypeTd">
    <tr><td colspan="2"><b><p id="cardName"></p></b></td></tr>
    </div>

<tr>
<td><?php echo $form['card_num']->renderLabel(); ?><span class="red">*</span></td>
<td><?php echo $form['card_num']->render(); ?> <br> <img src="<?php echo image_path('visa.gif'); ?>" alt="" class="pd4" /> 

<br>
  <div class="red" id="card_num_error">
   <?php echo $form['card_num']->renderError(); ?>
  </div>

</td>
</tr>

<tr>
<td><?php echo $form['month']->renderLabel(); ?><span class="red">*</span></td>
<td><?php echo $form['month']->render(); ?> &nbsp; <?php echo $form['year']->render(); ?>

<br>
  <div class="red" id="month_year_error">
   <?php echo $form['month']->renderError(); ?>
   <?php echo $form['year']->renderError(); ?>
  </div>
  
</td>
</tr>

<tr>
<td><?php echo $form['cvv_number']->renderLabel(); ?><span class="red">*</span></td>
<td><?php echo $form['cvv_number']->render(); ?>

<br>
  <div class="red" id="cvv_number_error">
   <?php echo $form['cvv_number']->renderError(); ?>   
  </div>
</td>
</tr>


       <tr>
          <td>&nbsp;</td>
          <td><div class="lblButton">
          <?php echo $form[$form->getCSRFFieldName()]->render(); ?>
             <input type="hidden" name="requestId" id="requestId" value="<?php echo $requestId; ?>">
             <input type="hidden" name="card_type" id="card_type" >
            <input type="submit"  value="Submit" class="button">
            </div>
            <div class="lblButtonRight">
              <div class="btnRtCorner"></div>
            </div></td>
        </tr>
        </tbody>

      </table>

</form>