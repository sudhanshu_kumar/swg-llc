<?php use_helper('Form');?>
<?php use_helper('DateForm');?>

<?php //echo ePortal_pagehead('Change Password'); ?>
        <form action="<?php echo url_for('epActionAuditEventReports/displayReport'); ?>" method="post" class="dlForm"  >
          <table cellpadding="0" cellspacing="0" border="0" width="100%">
            <tr>
              <td colspan="2" class="blbar">Audit Trail</td>
              </tr>
			
             <?php //echo $form;?>
         
           <tr>
              <td><?php echo $form['startDate']->renderLabel(); ?><span class="red">*</span></td>
              <td><?php echo $form['startDate']->render(); ?>
              <div class="red" id="startDate_error"> <?php echo $form['startDate']->renderError(); ?> </div></td>
          </tr>
           <tr>
              <td><?php echo $form['endDate']->renderLabel(); ?><span class="red">*</span></td>
              <td><?php echo $form['endDate']->render(); ?>
              <div class="red" id="endDate_error"> <?php echo $form['endDate']->renderError(); ?> </div></td>
          </tr>
           <tr>
              <td><?php echo $form['category']->renderLabel(); ?><span class="red">*</span></td>
              <td><?php echo $form['category']->render(); ?>
              <div class="red" id="category_error"> <?php echo $form['category']->renderError(); ?> </div></td>
          </tr>
           <tr>
              <td><?php echo $form['subCategory']->renderLabel(); ?><span class="red">*</span></td>
              <td><?php echo $form['subCategory']->render(); ?>
              <div class="red" id="subCategory_error"> <?php echo $form['subCategory']->renderError(); ?> </div></td>
          </tr>
          <tr><td></td><td>
              <input type="submit" onclick="" value="Search" class="normalbutton"></td></tr>

          </table>


              

             
        </form>
          

            </div>
</div><div class="content_wrapper_bottom"></div>
<script>

    $("#audit_category").select(function (){
        $("#audit_category").change()
    });
    $("#audit_category").change(function()
    {
        var category = $(this).val();
        //bank = bank.toString();
        var url = "<?php echo url_for("epActionAuditEventReports/getSubCategory");?>";
        //$('#loader').show();
        $("#audit_subCategory").load(url, {
            category: category
        },function (data){
            if(data=='logout'){
                location.reload();
            }
            $('#loader').hide();
        });
    });

</script>
