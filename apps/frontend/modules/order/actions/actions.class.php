<?php

/**
 *  order actions.
 *
 * @package    mysfp
 * @subpackage  order
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12479 2008-10-31 10:54:40Z fabien $
 */
class orderActions extends sfActions {
/**
 * Executes index actionvalidated params
 *
 * @param sfRequest $request A request object
 */
    public function executePayment(sfWebRequest $request) {
        $this->setLayout(NULL);
        $version = $request->getParameter('payprocess') ;
        $pay4MeIntObj = Pay4MeIntServiceFactory::getService($version) ;
        $returnVal = $pay4MeIntObj->processOrder($request) ;
        return $this->renderText($returnVal) ;
    }

    public function executeProceed(sfWebRequest $request) {
        $order = $request->getParameter('order');
        $request->setParameter('order', $order) ;
        $this->forward('paymentProcess', 'orderDetails');

    }

    
}
