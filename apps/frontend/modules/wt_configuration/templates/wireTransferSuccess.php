<!--User selects the application that is to be associated with the wire transfer number and,enters the amount , date, uploads a file as its proof.  -->


<script>

  function validate_form(fmobj){
    var sel= false;
    var checkBoxCount = 0;
    for (var i=0;i<fmobj.elements.length;i++)
    {
      var e = fmobj.elements[i];
      if ((e.type=='checkbox') && (!e.disabled))
      {
        checkBoxCount = checkBoxCount+1;
        if(e.checked == true)
        {
          sel= true;
          break;
        }
      }else if((e.type=='checkbox') && (e.disabled))
      {
        checkBoxCount = checkBoxCount+1;
        sel= true;
        break;
      }
    }

    if(sel)
    {
      var illegalChars = /\W/;
      var amtToPaid = $("#amt_to_paid").val();
      var wiretransferAmt = $("#wiretransfer_amount").val();
      var errFlag = false;
      if(wiretransferAmt == '')
      {
        $("#amount_error").html("Please enter wire transfer amount.");
        errFlag = true;
      }
      //adding only numeric mo amount bug-id 29312
      else if(illegalChars.test(wiretransferAmt)){
          $("#amount_error").html("Wire Transfer amount should be numeric.");
          errFlag = true;
      }
      var wiretransferNumber = $("#wiretransfer_wire_transfer_number").val();
      if(wiretransferNumber == '')
      {
        $("#wiretransfer_number_error").html("Please enter wire transfer serial number.");
        errFlag = true;
      }
      var rewiretransferNumber = $("#wiretransfer_re_wire_transfer_number").val();
      if(rewiretransferNumber != wiretransferNumber)
      {
        $("#re_wiretransfer_number_error").html("Wire transfer serial number and Confirm wire transfer serial number do not match.");
        errFlag = true;
      }

      if($("#wiretransfer_wire_transfer_date_day").val() == '')
      {
        $("#wiretransfer_date_error").html("Please enter Wire Transfer Issuing Date.");
        errFlag = true;
      }

      if($("#wiretransfer_wire_transfer_date_month").val() =='')
      {
        $("#wiretransfer_date_error").html("Please enter Wire Transfer Issuing Date.");
        errFlag = true;
      }

      if($("#wiretransfer_wire_transfer_date_year").val() =='')
      {
        $("#wiretransfer_date_error").html("Please enter Wire Transfer Issuing Date.");
        errFlag = true;
      }
      
      //Round up decimal amount in amount validation
      if(Math.round(amtToPaid)!=Math.round(wiretransferAmt)){
        $("#amount_error").html("Amount to paid and wire transfer amount should be same.");
        errFlag = true;
      }

      if($('#wiretransfer_wire_transfer_proof').val()==''){
          $("#wire_transfer_proof_error").html("Please upload wire transfer receipt.");
          errFlag = true;
      }

      if(errFlag){
          return false;
      }

      if(confirm('Your wire transfer serial number is "'+wiretransferNumber+'". Do you want to continue?')){
        return true;
      }else{
        return false;
      }
      
    }
    else
    {
      if(checkBoxCount > 2){
        var amt_to_paid_error = 'Please select at least one tracking number.';
      }else{
        var amt_to_paid_error = 'Please select tracking number.';
      }
      $('#amt_to_paid_error').html(amt_to_paid_error);
      $('#amt_to_paid_error').focus();
      return false;
    }
    return false;
  }

  function uniqueWireTransferNumber(wiretransfer_number)
  {

    var url = "<?php echo url_for("wt_configuration/uniqueWireTransferNumber"); ?>";

    $.get(url, { moneyorder_number: wiretransfer_number},
    function(data){
      alert(data);


    });
  }
  function checkAll(fmobj, chkAll)
  {
    var amount = 0;
    $('#amt_to_paid_error').html('');
    for (var i=0;i<fmobj.elements.length;i++)
    {
      var e = fmobj.elements[i];
      if ((e.name == chkAll) && (e.type=='checkbox') && (!e.disabled))
      {
        e.checked = fmobj.chk_all.checked;
        if(fmobj.chk_all.checked)
        {
          amount = parseInt(amount)+parseInt($("#"+e.id+"amt").html());
        }
        $("#amt_to_paid").val(amount);
        $("#disp_amt").html(amount+".00");
      }
    }
  }

  function UncheckMain(fmobj, objChkAll, chkElement,obj,amt)
  {
    var boolCheck = true;
    $('#amt_to_paid_error').html('');
    var amount = $("#amt_to_paid").val();
    if(amount ==''){
      amount = 0;
    }
    if($("#"+obj.id).is(':checked')){
      amount = parseInt(amount)+parseInt(amt);
      $("#amt_to_paid").val(amount);
      $("#disp_amt").html(amount+".00");

    }else{
      amount = parseInt(amount)-parseInt(amt);
      $("#amt_to_paid").val(amount);
      $("#disp_amt").html(amount+".00");

    }
    if(objChkAll.checked==false)
    {
      for(var i=0;i<fmobj.elements.length;i++)
      {
        if(fmobj.elements[i].name.indexOf(""+chkElement+"")!="-1" && fmobj.elements[i].checked==false)
        {
          boolCheck = false;
          break;
        }
      }
      if(boolCheck==true)
        objChkAll.checked=true;
    }
    else
    {
      objChkAll.checked=false;
    }
  }
</script>


<?php echo form_tag('wt_configuration/saveWiretransfer',array('name'=>'order_search_form','class'=>'', 'method'=>'post','id'=>'searchApplicationForm' , 'enctype' => 'Multipart/form-data', 'onsubmit'=>'return validate_form(document.searchApplicationForm)','name'=>'searchApplicationForm')) ?>
<input type="hidden" name="userid" id="userid" value="<?php echo $userid; ?>" />
<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">

<?php
use_helper('Form');
use_helper('Pagination');
include_partial('global/innerHeading',array('heading'=>'Wire Transfer Details'));
?>
       <div class="clear"></div>
        <div class="tmz-spacer"></div>
    <?php
    $sf = sfContext::getInstance()->getUser();
    if($sf->hasFlash('notice')){ ?>
    <div id="flash_notice" class="alertBox" >
      <?php
      echo nl2br($sf->getFlash('notice'));
      ?>
    </div><br/>
    <?php }?>


      <?php
      $sf = sfContext::getInstance()->getUser();
      if($sf->hasFlash('error')){ ?>
      <div id="flash_error" class="alertBox" >
        <?php
        echo nl2br($sf->getFlash('error'));
        ?>
      </div><br/>
      <?php }?>
    <?php 
    $colspan = '4';
    if($isValid) { ?>
      <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
        <tr>
          <td class="blbar" colspan="<?php echo $colspan?>" align="left">Please select Tracking Number(s)</td>
        </tr>
        <tr>
          <td width="12%" valign="top"><span class="txtBold" id="maincheck"> <input type="checkbox" name="chk_all" id="chk_all" onclick="checkAll(document.searchApplicationForm,'chk_fee[]');" >&nbsp;(Check All)</span></td>
          <td width="18%"><span class="txtBold">Tracking Number</span></td>
          <td width="60%" align="center"><span class="txtBold">Application Detail</span></td>
          <td width="10%" align="right"><span class="txtBold">Amount( $ )</span></td>
        </tr>
        <tbody>
          <?php
          $i=1;
          foreach ($pendingtrackingNumbers as $result): //echo '<pre>';print_r($result);echo '</pre>';
          ?>
          <tr>
            <td valign="top">
                <span class="txtBold"><input type="checkbox" name="chk_fee[]" id="<?= "chk_fee".$i; ?>" value="<?= $result['track']['id']."_".$result['track']['cart_amount'];?>" OnClick="UncheckMain(document.searchApplicationForm,document.searchApplicationForm.chk_all,'chk_fee',<?= "chk_fee".$i; ?>,'<?= $result['track']['cart_amount']?>');" ></span>
            </td>
            <td valign="top">
                <span><?php echo $result['track']['tracking_number'];?></span>
            </td>
            <td valign="top">
                 <table cellspacing="0" cellpadding="0" border="1" width="100%" >
                    <tr bgcolor="#eeeeee">
                        <td width="15%"><span class="txtBold">App. Id</span></td>
                        <td width="15%"><span class="txtBold">App. Type</span></td>
                        <td width="30%"><span class="txtBold">Reference Number</span></td>
                        <td width="40%"><span class="txtBold">Applicant Name</span></td>
                    </tr>
                    <?php
                    $appDetailsCount = count($result['app_details']);
                    if($appDetailsCount > 0) {
                    for($j=0;$j<$appDetailsCount;$j++){ ?>
                        <tr>
                            <td width="15%"><?php echo $result['app_details'][$j]['id']?></td>
                            <td width="15%"><?php echo ucfirst($result['app_details'][$j]['app_type'])?></td>
                            <td width="15%"><?php echo $result['app_details'][$j]['ref_no']?></td>
                            <td width="15%"><?php echo $result['app_details'][$j]['name']?></td>                            
                        </tr>
                    <?php } } else {  ?>
                        <tr>
                            <td colspan="3" align="center" >No application detail found.</td>
                        </tr>
                    <?php } ?>
                </table>
            </td>
            <td valign="top" align="right" id="<?= "chk_fee".$i."amt"; ?>"><?php echo $result['track']['cart_amount'];?></td>
          </tr>

            <?php
            $i++;
            endforeach; ?>
          <tr>
            <td></td>
            <td colspan="2" align="right"><b>Total ($)</b></td>
            <td align="right"><div id="amt_to_paid_div"><span id="disp_amt">0.00</span></div><input type="hidden" name="amt_to_paid" id="amt_to_paid"  class="txt-input" readonly="readonly"></td></tr>
          <tr><td width="100%" colspan="3"><div class="red" id="amt_to_paid_error"></div></td></tr>

        </tbody>
      </table>
      <table width="100%">
        <tr>
          <td class="blbar" colspan="<?php echo $colspan?>" align="left">Enter Wire Transfer Details</td>
        </tr>

         <tr>
  <td width="30%"><?php echo $form['amount']->renderLabel()." ($)"; ?><span class="red">*</span><br><small><i>(Kindly do not enter decimal places or dollar sign inside the box.)</i></small></td>
          <td><?php echo $form['amount']->render(); ?>
            <br>
            <div class="red" id="amount_error">
              <?php echo $form['amount']->renderError(); ?>
            </div>

          </td>
        </tr>


        <tr>
          <td>Wire Transfer Serial Number<span class="red">*</span><br><small><i>(Kindly use only one Wire Transfer Serial Number at a time.)</i></small></td>
          <td><?php echo $form['wire_transfer_number']->render(); ?>
            <br>
            <div class="red" id="wiretransfer_number_error">
              <?php echo $form['wire_transfer_number']->renderError(); ?>
            </div>

          </td>
        </tr>

        <tr>
          <td>Confirm Wire Transfer Serial Number<span class="red">*</span></td>
          <td><?php echo $form['re_wire_transfer_number']->render(); ?>
          <br>
            <div class="red" id="re_wiretransfer_number_error">
              <?php echo $form['re_wire_transfer_number']->renderError(); ?>
            </div>
          </td>
        </tr>
        
        <tr>
          <td>Wire Transfer Issuing Date<span class="red">*</span><br>(dd/mm/yyyy)</td>
          <td><?php echo $form['wire_transfer_date']->render(); ?>
            <br>
            <div class="red" id="wiretransfer_date_error">
              <?php echo $form['wire_transfer_date']->renderError(); ?>
            </div>

          </td>
        </tr>

         <tr>
          <td>Wire Transfer Receipt<span class="red">*</span></td>
          <td><?php echo $form['wire_transfer_proof']->render(); ?>
            <br>            
            <div class="red" id="wire_transfer_proof_error">
              <?php echo $form['wire_transfer_proof']->renderError(); ?>
            </div>

          </td>
        </tr>

        <tr>
          <td>&nbsp;</td>
          <td>
              <?php   echo submit_tag('Submit for Payment',array('class' => 'normalbutton')); ?>
       
           </td>
        </tr>
      </table>


      </form>
      <?php } ?>

  </div>
  </div>
  <div class="content_wrapper_bottom"></div>

<br>
<script type="text/javascript">
   $(document).ready(function(){
        $('#wiretransfer_wire_transfer_number').bind("cut copy paste",function(e) {
                e.preventDefault();
            });

            $('#wiretransfer_re_wire_transfer_number').bind("cut copy paste",function(e) {
                e.preventDefault();
            });
        
        });     

  </script>