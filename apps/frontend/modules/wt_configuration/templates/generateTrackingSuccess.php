<!-- Tracking is being generated and user can proceed to for the same.-->



<?php if($errMsg != '') { ?>
    <div style="background-color:#ffffff;">
 <div id="flash_notice" class="alertBox" >
  <?php
    echo $errMsg;
  ?>
</div>
</div>
<?php } ?>

<?php
if($data)
{
    ?>
<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">
<?php
include_partial('global/innerHeading',array('heading'=>'Tracking Number: '.$tracking_number));?>
       <div class="clear"></div>
        <div class="tmz-spacer"></div>
            <table width="100%" align="center" class="show_table">
                <tr>
                    <td colspan="2" class="blbar">
                        <b>Your Tracking details is as follows:</b>
                    </td>
                </tr>
                <tr>
                    <td>
                        Tracking Number
                    </td>
                    <td>
                        <?= $trackingDetails['tracking_number']?>
                    </td>
                </tr>
                <tr>
                    <td>
                        Amount
                    </td>
                    <td>
                        <?= "$".$trackingDetails['cart_amount']?>
                    </td>
                </tr>
      </table>
            
             






  <?php
  $action = url_for('mo_configuration/trackingProcess') ;
    /*
     * To change this template, choose Tools | Templates
     * and open the template in the editor.
     */

  ?>


  <form name="genearteForm" id ="genearteForm" action="<?php echo $action; ?>" method="post">
    <input type="hidden" name="request_id" value="<?= $sf_params->get('requestId')?>">
    <p style="text-align: left ! important;" class="body_txtp"><div class="highlight yellow" id="flash_notice"><span style="text-align:left; color:red;"><b style="font-size:13px;">Important!!! Now you have to associate this tracking number with Wire Transfer Serial Number to complete the payment process else your payment will not be activated on the platform and you may encounter undue delays.</b></span></div>
    <p class="body_txtp" style="text-align:left!important;"></p>
      <span style="text-align:left;"><span style="text-align:left; color:red;"><center><b style="font-size:14px;">PLEASE PRINT THIS PAGE</b><br>
<b style="font-size:14px;">IMPORTANT:  READ AND UNDERSTAND THE FOLLOWING INSTRUCTIONS BEFORE PROCEEDING.</b></center></span></span>
    </p>
    
    <div>
      <b>INSTRUCTIONS</b>
    </div>
    <table width="100%" class="table_new">
      
      <tr>
        <td>
          All Fees are NONREFUNDABLE, even if a visa or passport is denied or a visa is issued for a shorter period of time or otherwise issued or returned at a time or on terms and conditions that vary from those sought by the applicant.<br/><br/>
		  Please visit the nearest Bank to transfer money for the amount payable for your application(s). The amount should be payable to "SW Global LLC" account (details will be sent you soon on your e-mail address). You are required to upload wire transfer certificate while associating this tracking number with it.<br/><br/>

      Once you have transfer the money to SW Global LLC/FGN account as per instructions above, please follow these steps:<br/>
		  <ol style="padding:10px 0 0 25px;"><li>Please note down the wire transfer <strong>"Serial Number"</strong> provided on the receipt given by your Bank and provide that to us by associating it your tracking number by clicking <?php echo link_to('here',url_for('wt_configuration/saveWiretransfer'),array('title'=>'Wire Transfer > Associate Tracking Number'));?>. You can also follow this step by logging back in to swgloballlc.com and pulling up your pending tracking number from the home screen after logging in. We’ll send you a confirmation email stating that your order number & tracking number is associated with a wire transfer serial number. This email doesn’t mean that your application is paid yet. </li>
</ol><br/>
Once we receive your money, we’ll confirm the receipt via another email to you stating that your payment has been received and you can proceed to submit your Visa/Passport application.
For faster processing of your application, the following guidelines must be strictly followed:<br/>
<ol style="padding:10px 0 0 25px;"><li>Amount of the wire transfer MUST match exactly with amount of your order. Your current order amount is USD <?= "$".$trackingDetails['cart_amount']?>.</li><li>Always remember to associate your swglobal order with the wire transfer number. You can do this by pulling up from pending orders after logging into your swgloballlc.com account.</li></ol><br/>
You can also pay for multiple swgloballlc orders for using a single wire transfer by following these steps:-<br/>
<ol style="padding:10px 0 0 25px;"><li>1.Amount of the wire transfer MUST match exactly with the sum of all your swgloballlc orders that you want to pay for (from the CART).</li>
<li>1.While providing your wire transfer number on swgloballlc.com, please select all the tracking number from the list (you can select one or as many orders as you want). Confirmation email will be sent to you for association of your wire transfer serial number.</li>
</ol>
        </td>
		
      </tr>

    </table>
    <br>
    <table>

                <tr>
                    <td>
                        If you have wire transfer number ready with you, click below button to associate tracking number with wire transfer number. Or, login again when you are ready with wire transfer number and start by clicking on <?php echo link_to('Wire Transfer > Associate Tracking Number',url_for('wt_configuration/saveWiretransfer'),array('title'=>'Associate Tracking Number'));?>
                   </td>
                </tr>
                <tr><td align="center" style="text-align:center;"><center><p class="page_subheader">Your Tracking Number is <?php echo $trackingDetails['tracking_number']; ?></p></center></td></tr>
                <tr>
                    <td align="center">
                    
                        
                   
                           
                        <input  class="normalbutton noPrint" value=' I have wire transfer, let me enter the details' type="button" class ='button' onclick="gotoNext()">
                   
                      

                        <input type="button" class="normalbutton noPrint" onclick="javaScript:window.print();" name="Print" value="Print">
                  
                    </td>
                </tr>
    </table>
<br>
    
<!--    <div style="margin:10px 0 0 315px;">
      <input type="submit" name="generate" id="generate" value="Generate Tracking Number" class="button noPrint">
      <div class="lblButtonRight">
        <div class="btnRtCorner"></div>
      </div>
    </div>-->
	<div style="margin-left:5px;">
      
    </div>
  </form>
    </div>
  </div>
  <div class="content_wrapper_bottom"></div>
<?php } ?>

<script>
  function gotoNext(){
    window.location = '<?php echo url_for('wt_configuration/saveWiretransfer'); ?>';
  }
  </script>