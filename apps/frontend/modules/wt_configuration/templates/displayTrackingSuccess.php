<?php include_partial('global/innerHeading',array('heading'=>'Tracking number information'));?>

<div class="global_content4 clearfix">
<div class="wrapForm2">
<?php
$sf = sfContext::getInstance()->getUser();
if($sf->hasFlash('notice')){ ?>
<div id="flash_notice" class="alertBox" >
    <?php
    echo nl2br($sf->getFlash('notice'));
    ?>
</div><br/>
<?php }?>

<div class="wrapForm2">
<?php
$sf = sfContext::getInstance()->getUser();
if($sf->hasFlash('error')){ ?>
<div id="flash_error" class="alertBox" >
    <?php
    echo nl2br($sf->getFlash('error'));
    ?>
</div><br/>
<?php }?>
<?php
if($data)
{
    ?>
    <div>
            <table width="100%">
                <tr>
                    <td colspan="2" style="background-color:#f1f1f1;">
                        <b>Your Tracking details is as follows:</b>
                    </td>
                </tr>
                <tr>
                    <td>
                        Tracking Number
                    </td>
                    <td>
                        <?= $trackingDetails['tracking_number']?>
                    </td>
                </tr>
                <tr>
                    <td>
                        Amount
                    </td>
                    <td>
                        <?= "$".$trackingDetails['cart_amount']?>
                    </td>
                </tr>
      </table>
            <br><br>
             <table>

                <tr>
                    <td>
                        If you have wire transfer number ready with you, click below button to associate tracking number with wire transfer number. Or, Login again when you are ready with wire transfer number and start by clicking on <?php echo link_to('Wire Transfer > Associate Tracking Number',url_for('wt_configuration/saveWiretransfer'),array('title'=>'Wire Transfer > Associate Tracking Number'));?>
                    </td>
                </tr>
                <tr>
                    <td align="center" style="padding:0 0 0 300px; *padding:0 0 0 270px;">
                        <div class="lblButton">
            <input value=' I have wire transfer, let me enter the details' type="button" class ='button' onclick="gotoNext()">
          </div>
          <div class="lblButtonRight" style="*display:none;">
            <div class="btnRtCorner"></div>
        </div>
                    </td>
                </tr>
            </table>

    </div>
    
</div></div>
</div>
<?php }?>
<script>
  function gotoNext(){
    window.location = '<?php echo url_for('wt_configuration/saveWiretransfer'); ?>';
  }
  </script>
