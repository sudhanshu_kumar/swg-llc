

<!-- This template displays Tracking Information after tracking number is being successfully associated with a wire transfer -->

<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">

  <?php
use_helper('Form');
include_partial('global/innerHeading',array('heading'=>'Wire Transfer Details'));
?>
      <div class="clear"></div>
        <div class="tmz-spacer"></div>
        <?php
        $sf = sfContext::getInstance()->getUser();
        if($sf->hasFlash('notice')){ ?>
        <div id="flash_notice" class="alertBox" >
          <?php
          echo nl2br($sf->getFlash('notice'));
          ?>
        </div><br/>
        <?php }?>

   
          <?php
          $sf = sfContext::getInstance()->getUser();
          if($sf->hasFlash('error')){ ?>
          <div id="flash_error" class="alertBox" >
            <?php
            echo nl2br($sf->getFlash('error'));
            ?>
          </div><br/>
          <?php }?>
         
           <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable show_table" >
           <tr>
              <td class="blbar" colspan="11" align="left">Wire Transfer information</td>
            </tr>
            <tr>
              <td width="50%">
                <table style="margin:auto;" width="100%">
                  <tr>
                    <td width="30%">
                      Wire Transfer Number
                    </td>
                    <td width="30%">
                      <?= $wireTransferDetails['wire_transfer_number']?>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      Wire Transfer Date
                    </td>
                    <td>
                      <?= $wireTransferDetails['wire_transfer_date']?>
                    </td>
                  </tr>
                </table>
              </td>
              <td width="50%">
                <table  style="margin:auto;" width="100%">
                  <tr>
                    <td width="30%"v>
                      Amount
                    </td>
                    <td width="30%">
                      <?= "$".$wireTransferDetails['amount']?>
                    </td>
                  </tr>
                  <tr>
                    <td colspan="2">
                      &nbsp;
                    </td>
                    
                  </tr>
                </table>
              </td>
            </tr>
           </table>
		   <br/>
           <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable show_table" >
            <tr>
              <td class="blbar" colspan="11" align="left">Tracking Information</td>
            </tr>
            <tr>
              <td width="15%"><span class="txtBold">Tracking Number</span></td>
              <td width="15%"><span class="txtBold">Order Number</span></td>
              <td width="15%"><span class="txtBold">Application Detail</span></td>
              <td width="10%"><span class="txtBold">Amount</span></td>
            </tr>
            <tbody>
            <?php if($record) { ?>
              <?php  foreach ($applicationDetails as $records){ ?>
              <tr>
                <td valign="top">
                  <?= $records['tracking_number']?>
                </td>
                <td valign="top">
                  <?= $records['order_number']?>
                </td>
                <td>
                    <table>
                        <tr bgcolor="#eeeeee">
                            <td width="15%"><span class="txtBold">Application Id</span></td>
                            <td width="15%"><span class="txtBold">Application Type</span></td>
                            <td width="15%"><span class="txtBold">Reference Number</span></td>
                        </tr>
                        <?php foreach($records['app_detail'] as $detail){?>
                            <tr>
                                <td width="15%"><?= $detail['app_id']?></td>
                                <td width="15%"><?= $detail['app_type']?></td>
                                <td width="15%"><?= $detail['ref_no']?></td>
                            </tr>
                        <?php } ?>
                    </table>
                  
                </td>
               <td valign="top">
                  <?= "$".$records['cart_amount']?>
                </td>
              </tr>
              <?php } ?>
              <?php } else{ ?>
              <tr>
                <td colspan="3">
                  No tracking number found.
                </td>
              </tr>
              <?php } ?>
            </tbody>
           </table>


        

      <br>
     
        <table width="100%">

      <tr>
        <td>
          An email has been sent to you confirming, your order number & tracking number is associated with a wire transfer transaction number.<br/><br/>
		  
		  <div class="prominent">

Once we receive confirmation from bank, we’ll confirm the receipt via another email to you stating that your payment has been received and you can proceed to submit your Visa/Passport application.
</div>
<div style="clear:both;"></div><br/>
Once we confirm the receipt of your payment in an email, you can proceed to check further details (e.g. interview date if applicable) of your application(s) on Nigeria Immigration Services (NIS) Portal by following these steps:<br/>
<ol class="prominent_listing"><li><a href="http://portal.immigration.gov.ng/visa/OnlineQueryStatus" target="new">Click HERE to open "Query Your Application Status" page on NIS</a>. Alternatively you can go to NIS home page (<a href="http://portal.immigration.gov.ng/" target="new">http://portal.immigration.gov.ng/</a>) and click "Query Your Application Status" link there.</li>
<li>Using your APPLICATION ID and REFERENCE NUMBER (sent to you via email), please pull up your NIS ACKNOWLEDGMENT SLIP and PAYMENT RECEIPT. </li>
	  <li>If you have made ONE payment for MULTIPLE APPLICATIONS using our CART, you must enter each APPLICATION ID/REFERENCE NUMBER at <a href="http://portal.immigration.gov.ng/visa/OnlineQueryStatus" target="new"> "Query Your Application Status"</a> page to pull up the ACKNOWLEDGMENT SLIP and PAYMENT RECEIPT for each application.
    </li>
	  <li>PRINT your ACKNOWLEDGMENT SLIP and PAYMENT RECEIPT for each application. You must present the ACKNOWLEDGMENT SLIP and PAYMENT RECEIPT to the Embassy/Consulate with other required documents on the interview date assigned to you. 
</li>
</ol>
        </td>

      </tr>

    </table>
    
      <div class="clear">&nbsp;</div>
      <div align="center" >
                  <input type="button" class="normalbutton noPrint" onclick="javaScript:window.print();" name="Print" value="Print">
                </div>
             
   

  </div>
  </div>
  <div class="content_wrapper_bottom"></div>
