<!-- Application can be searched by the help of Application type and application Id-->

<script>
  function validateForm()
  {
    if($('#app_type').val() == '')
    {
      alert('Please select application type.');
      $('#app_type').focus();
      return false;
    }
    else if($('#app_id').val() == '')
    {
      alert('Please insert application id.');
      $('#app_id').focus();
      return false;
    }
   
    
    if($('#app_id').val() != "")
    {
      if(isNaN(document.getElementById('app_id').value))
      {
        alert('Please insert only numeric value.');
        document.getElementById('app_id').value = "";
        document.getElementById('app_id').focus();
        return false;
      }

    }
  }
</script>
<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">

<?php use_helper('Form');
include_partial('global/innerHeading',array('heading'=>'Search Application For Wire Transfer'));
?>
      <div class="clear"></div>
        <div class="tmz-spacer"></div>
 <div class="clearfix"/>
<?php echo form_tag('wt_configuration/searchAppForWT',array('name'=>'app_search_form','class'=>'', 'method'=>'get','id'=>'app_search_form','onSubmit'=>'return validateForm()')) ?>


<?php
$sf = sfContext::getInstance()->getUser();
if($sf->hasFlash('notice')){ ?>
    <div id="flash_notice" class="alertBox" >
<?php
    echo nl2br($sf->getFlash('notice'));
?>
</div><br/>
<?php }?>
            <table width="100%">
		<?php   
                $app_type = array('' => 'Select Application Type', 'P' => 'Passport', 'V' => 'Visa', 'F' => 'Free Zone');
                echo formRowComplete('Application Type<span style="color:red">*</span>',select_tag('app_type', options_for_select($app_type), 0)); ?>
		
                <?php               
                $app_id = '';
                echo formRowComplete('Application Id<span style="color:red">*</span>',input_tag('app_id', $app_id, array('size' => 20, 'maxlength' => 20, 'class'=>'txt-input'))); ?>

		
                <tr>
                    <td>&nbsp;</td>
                    <td>
                            
                            <input type="hidden" name="submitFlag" id="submitFlag" value="Submit" />
                            <?php   echo submit_tag('Search',array('class' => 'normalbutton')); ?>
                       
                        </td>
                </tr>
            </table>
     
 
  </div>
  </div>
  </div>
  <div class="content_wrapper_bottom"></div>