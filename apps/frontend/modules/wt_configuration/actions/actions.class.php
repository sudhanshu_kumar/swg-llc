<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of actionsclass
 *
 * @author ashwanik
 */
class wt_configurationActions extends sfActions {

    /**
     * Executes index actionvalidated params
     *
     * @param sfRequest $request A request object
     */
    public function executeGenerateTracking(sfWebRequest $request) {
        if ($request->hasParameter('paymentMode')) {
            $paymentMode = $request->getParameter('paymentMode');
            $this->requestId = $request->getParameter('requestId');
        }

        $this->data = false;
        $this->errMsg = '';
        $this->tracking_number = 'N/A';
        if ($request->hasParameter("id")) {
            $trackingId = $request->getParameter("id");
            $encriptedTrackingId = SecureQueryString::DECODE($trackingId);
            $trackingId = SecureQueryString::ENCRYPT_DECRYPT($encriptedTrackingId);
            $trackingDetails = WireTrackingNumberTable::getInstance()->findById($trackingId)->toArray();

            if (!empty($trackingDetails)) {

                ## Getting user logged in id...
                $userId = $this->getUser()->getGuardUser()->getId();
                ## If tracking id does not belongs to logged in user then error message will be generated...
                if ($userId != $trackingDetails[0]['user_id']) {
                    $this->errMsg = 'Invalid Tracking Number Request.';
                } else {
                    $this->data = true;
                    $this->trackingDetails = $trackingDetails[0];
                    $this->tracking_number = $trackingDetails[0]['tracking_number'];
                }
            } else {
                $this->errMsg = 'Invalid Tracking Number Request.';
            }
        } else {
            $this->errMsg = 'Invalid Tracking Number Request.';
        }
    }

    /**
     * Gathers information regarding application in the cart.And checks whether the application is already processed or not
     * Geneartes tracking number
     * @param sfWebRequest $request
     */
    public function executeTrackingProcess(sfWebRequest $request) {
        //logic to genearete order number....
        //logic to genearte tracking number
        //      $this->forward('wt_configuration', 'displayTracking');
        //check if application allready processed if coming from NIS
        $requestId = $request->getParameter('requestId');
        $orderRequest = Doctrine::getTable('OrderRequestDetails')->find($requestId);
        //get Application on NIS
        $appDetails = CartItemsTransactionsTable::getInstance()->getRetryIdDetailsOnNis($orderRequest->getRetryId());
        $appType = $appDetails[0]['app_type'];
        $appID = $appDetails[0]['id'];
        switch ($appType) {
            case 'passport': $appType = 'P';
                break;
            case 'visa': $appType = 'V';
                break;
            case 'freezone': $appType = 'F';
                break;
        }
        $paymentRequestRecord = Doctrine::getTable('CartItemsTransactions')->getPaymentRequestMoneyOrder($appID, $appType);
        foreach ($paymentRequestRecord as $key => $items) {
            $arrItemId[$key] = $items['id'];
        }
        if (isset($arrItemId)) {

            $cartItemInfo = Doctrine::getTable('CartItemsTransactions')->getCartItemAllIdInfo($arrItemId);

            foreach ($cartItemInfo as $key => $item) {
                $arrCartId[$key] = $item['cart_id'];
            }
            if (isset($arrCartId)) {
                $cartTrakingInfo = Doctrine::getTable('WireTrackingNumber')->getTrackingNumber($arrCartId);
                if (empty($cartTrakingInfo)) {
                    $cartTrakingInfo = Doctrine::getTable('CartTrackingNumber')->getTrackingNumber($arrCartId);
                }
            }
        }

        if (!empty($cartTrakingInfo) && count($cartTrakingInfo) > 0) {
            $this->getUser()->setFlash('notice', 'Applications has been already processed. ');
            $this->redirect('cart/list');
        }

        $arrCartItems = $this->getUser()->getCartItems();
        if (count($arrCartItems) == 0) {
            $this->getUser()->setFlash('notice', 'Applications has been already processed. ');
            $this->redirect('cart/list');
        }


        $isNotVaildRequest = WireTrackingNumberTable::getInstance()->findByOrderRequestDetailId($requestId)->count();
        if (!$isNotVaildRequest) {
            $requestDetails = OrderRequestDetailsTable::getInstance()->getOrderRequestDetails($requestId);
            $trackingNumberObj = new WireTrackingNumber();
            $trackingNumberObj->setCartId($requestDetails['OrderRequest']['transaction_number']);
            $trackingNumberObj->setOrderRequestDetailId($requestId);
            $trackingNumberObj->setCartAmount($requestDetails['OrderRequest']['amount']);
            $trackingNumberObj->setUserId($this->getUser()->getGuardUser()->getId());
            $trackingNumberObj->save();
            $trackingId = $trackingNumberObj->id;
            $trackingNumber = PaymentGatewayHelper::generateWtTrackingNumber($trackingId);
            $trackingNumberObj->setTrackingNumber($trackingNumber);
            $userDetailObj = $this->getUser()->getGuardUser()->getUserDetail();
            $trackingNumberObj->save();
            $cartItems = $this->getUser()->clearCart();
            $userDetailObj->setCartItems(NULL);
            $userDetailObj->save();
            $this->getUser()->setFlash("notice", "Your Tracking Number: " . $trackingNumber);
            $encriptedTrackingId = SecureQueryString::ENCRYPT_DECRYPT($trackingId);
            $encriptedTrackingId = SecureQueryString::ENCODE($encriptedTrackingId);
            $this->redirect("wt_configuration/generateTracking?id=" . $encriptedTrackingId);
        } else {
            $this->getUser()->setFlash("notice", "Invalid Tracking Number Request.");
            $this->redirect("wt_configuration/generateTracking");
        }
    }

    /**
     * Tracking number id displayed
     * @param sfWebRequest $request
     */
    public function executeDisplayTracking(sfWebRequest $request) {
        //show tracking number
        $this->data = false;
        if ($request->hasParameter("id")) {
            $trackingId = $request->getParameter("id");
            $encriptedTrackingId = SecureQueryString::DECODE($trackingId);
            $trackingId = SecureQueryString::ENCRYPT_DECRYPT($encriptedTrackingId);
            $trackingDetails = WireTrackingNumberTable::getInstance()->find($trackingId)->toArray();
            if (isset($trackingDetails) && count($trackingDetails) > 0) {
                $this->data = true;
                $this->trackingDetails = $trackingDetails;
            } else {
                $this->getUser()->setFlash("notice", "Invalid Tracking Number Request.");
            }
        }
    }

    /**
     * @param <type> $request
     * @return <type>
     * This function is used to associate tracking number...
     */
    public function executeSaveWiretransfer(sfWebRequest $request) {

        if ($request->getParameter('userid')) {
            $userid = $request->getParameter('userid');
        } else {
            $userid = $this->getUser()->getGuardUser()->getId();
        }

        $this->userid = $userid;

        $this->form = new WiretransferForm(NULL, array('userId' => $userid));
        $this->isValid = false;

        $pendingtrackingNumbers = WireTrackingNumberTable::getInstance()->getTrackingNumbersByStaus($userid, "New");
        $pendingtrackingNumbersCount = count($pendingtrackingNumbers);

        $trackingArray = array();
        if ($pendingtrackingNumbersCount > 0) {

            for ($i = 0; $i < $pendingtrackingNumbersCount; $i++) {

                foreach ($pendingtrackingNumbers[$i] AS $key => $value) {
                    $trackingArray[$i]['track'][$key] = $value;
                }

                if (isset($pendingtrackingNumbers[$i]['cart_id'])) {
                    $fields = 'item_id AS item_id';
                    $cartTrakingInfo = Doctrine::getTable('CartItemsTransactions')->getCartItemInfoByCartId($pendingtrackingNumbers[$i]['cart_id'], $fields);
                    $cartTrakingInfoCount = count($cartTrakingInfo);

                    for ($j = 0; $j < $cartTrakingInfoCount; $j++) {
                        $item_id = $cartTrakingInfo[$j]['item_id'];
                        if ($item_id != '') {
                            $appIdDetails = Doctrine::getTable('IPaymentRequest')->getAppId($item_id);

                            $appDetails = array(); /* Bug id: 29837 */
                            if ($appIdDetails[0]['passport_id'] != '') {
                                $appDetails['passport_id'] = $appIdDetails[0]['passport_id'];
                            } else if ($appIdDetails[0]['visa_id'] != '') {
                                $appDetails['visa_id'] = $appIdDetails[0]['visa_id'];
                            } else if ($appIdDetails[0]['freezone_id'] != '') {
                                $appDetails['freezone_id'] = $appIdDetails[0]['freezone_id'];
                            }

                            ## Getting application details...
                            $applicationDetails = Doctrine::getTable('CartItemsTransactions')->getApplicationDetails($appDetails);

                            $trackingArray[$i]['app_details'][$j] = $applicationDetails;
                        }//End of if($item_id != ''){...
                    }//End of for($j=0;$j<$cartTrakingInfoCount;$j++){...
                }//End of if(isset($pendingtrackingNumbers[$i]['cart_id'])){...
            }//End of for($i=0;$i<$pendingtrackingNumbersCount;$i++){...
        }//End of if($pendingtrackingNumbersCount > 0){...


        $this->pendingtrackingNumbers = $trackingArray;


        if (isset($this->pendingtrackingNumbers) && is_array($this->pendingtrackingNumbers) && count($this->pendingtrackingNumbers) > 0) {
            $this->isValid = true;
        } else {
            $this->getUser()->setFlash("error", "No tracking numbers for associate", false);
        }
        $this->setTemplate("wireTransfer");
        if ($request->isMethod('post')) {
            $form_val = $request->getParameter($this->form->getName());
            $trackingNumberArr = $request->getParameter("chk_fee");
            if (count($trackingNumberArr) > 0) {
                $amount = 0;
                $actualAmt = 0;
                $trackingArr = array();
                $orderReqArr = array();
                $arrPaid = array();
                foreach ($trackingNumberArr as $trackingNumber) {
                    //echo '<pre>';print_r($trackingNumber);die;
                    $tracArr = explode("_", $trackingNumber);
                    $amount = $amount + $tracArr[1];
                    $trackingObj = WireTrackingNumberTable::getInstance()->find($tracArr[0]);
                    //add code for bug-id 29316
                    if ($trackingObj->getStatus() == 'Associated') {
                        $arrPaid[] = $trackingObj->getStatus();
                    }
                    $trackingAmountDB = $trackingObj->getCartAmount();
                    if (isset($trackingAmountDB) && $trackingAmountDB > 0) {
                        $actualAmt = $actualAmt + $trackingAmountDB;
                        $arr = array($tracArr[0], $trackingObj->getOrderRequestDetailId());
                        $trackingArr[] = $arr;
                    } else {
                        $this->getUser()->setFlash("error", "Invalid request found.");
                        return;
                    }
                }
                //add code for bug-id 29316
                if (count($arrPaid) > 0) {

                    $this->redirect("wt_configuration/saveWiretransfer");
                }

                if ($actualAmt == $amount && $actualAmt == $form_val['amount']) {

                    $this->form->bind($form_val, $request->getFiles($this->form->getName()));
                    if ($this->form->isValid()) {
                        $con = Doctrine_Manager::connection();
                        try {
                            $con->beginTransaction();
                            $this->form->save();
                            $wiretransferId = $this->form->getObject()->getId();

                            $paymentManager = new PaymentManager();
                            $isWiretransfered = $paymentManager->wiretransfer($trackingArr, $wiretransferId, $form_val);
                            $con->commit();

                            /**
                             * Updating image size...
                             */
                            if ($wiretransferId != '') {

                                $fileName = $this->form->getObject()->getWireTransferProof();
                                ## finding file extension...
                                $ext = substr(strrchr($fileName, '.'), 1);

//                      $userID = sfContext::getInstance()->getUser()->getGuarduser()->getId();
                                $wire_transfer_receipt = sfConfig::get('sf_upload_dir') . '/wire_transfer_receipt/' . $userid . '/' . $fileName;


                                $formValues = $request->getFiles($this->form->getName());
                                $formValues = $formValues['wire_transfer_proof'];

                                ## finding image width and height...
                                ## $image_info[0] is width and $image_info[1] is height...
                                $image_info = getimagesize($formValues['tmp_name']);

                                $sizeInKb = (int) ($formValues['size'] / 1024);

                                ## If size is more than 100 KB then convert it into small size...
                                if ($sizeInKb > 100) {
                                    if ($image_info[0] >= 700 && $image_info[1] >= 700) {
                                        $image_info[0] = 700;
                                        $image_info[1] = 700;
                                    }
                                }//End of if($sizeInKb > 100){...

                                $img = new sfImage($formValues['tmp_name'], 'image/' . $ext);
                                $img->thumbnail($image_info[0], $image_info[1]);
                                $img->setQuality(50);
                                $img->saveAs($wire_transfer_receipt);
                            }//End of if($wiretransferId != ''){...


                            $this->getUser()->setFlash("notice", "Your Wire Transfer is successfully associated with Tracking number(s).");
                            $encriptedWiretransferId = SecureQueryString::ENCRYPT_DECRYPT($wiretransferId);
                            $encriptedWiretransferId = SecureQueryString::ENCODE($encriptedWiretransferId);
                            //send mail to user
                            $mailUrl = "notification/trackingWireTransferMail";

                            $mailTaskId = EpjobsContext::getInstance()->addJob('TrackingWireTransferMail', $mailUrl, array('wire_transfer_id' => $wiretransferId, 'user_id' => $this->getUser()->getGuardUser()->getId()));

                            $this->redirect("wt_configuration/done?id=" . $encriptedWiretransferId);
                        } catch (Exception $e) {
                            $con->rollback();
                            $this->getUser()->setFlash("notice", "There is some error, please verify Wire Transfer and do again.");
                            $this->redirect("wt_configuration/saveWiretransfer");
                        }
                    }
                } else {
                    $this->getUser()->setFlash("error", "Invalid Amount please check amount.");
                    return;
                }
            } else {
                $this->getUser()->setFlash("error", "Please Select atleast one tracking number.");
                return;
            }
        }
    }

    /**
     * Function is called when tracking number is successfully associated with the wire transfer receipt and is processed further of approval of the payment.
     * @param sfWebRequest $request
     */
    public function executeDone(sfWebRequest $request) {
        $wiretransferId = $request->getParameter("id");

        $encriptedWiretransferId = SecureQueryString::DECODE($wiretransferId);
        $wiretransferId = SecureQueryString::ENCRYPT_DECRYPT($encriptedWiretransferId);

        $this->wireTransferDetails = WiretransferTable::getInstance()->find($wiretransferId);
        if (!empty($this->wireTransferDetails)) {
            $this->wireTransferDetails = $this->wireTransferDetails->toArray();
        } else {
            $this->redirect('wt_configuration/saveWiretransfer');
        }
        $this->record = false;
        $trackingDetails = WireTrackingNumberTable::getInstance()->getWireTransferDetails($wiretransferId);

        if (isset($trackingDetails) && is_array($trackingDetails) && count($trackingDetails) > 0) {
            $i = 0;
            foreach ($trackingDetails as $orderDetail) {
                //get retry id
                $retryId = OrderRequestDetailsTable::getInstance()->find($orderDetail['order_request_detail_id'])->toArray();

                $this->ipayNisAppDetails = CartItemsTransactionsTable::getInstance()->getRetryIdDetailsOnNis($retryId['retry_id']);
                // echo '<pre>';print_r($this->ipayNisAppDetails);die;
                foreach ($this->ipayNisAppDetails as $key => $app) {

                    $applicationDetails[$i]['tracking_number'] = $orderDetail['tracking_number'];
                    $applicationDetails[$i]['cart_amount'] = $orderDetail['cart_amount'];
                    $applicationDetails[$i]['order_number'] = $orderDetail['order_number'];
                    $applicationDetails[$i]['app_detail'][$key]['app_id'] = $app['id'];
                    $applicationDetails[$i]['app_detail'][$key]['app_type'] = $app['app_type'];
                    $applicationDetails[$i]['app_detail'][$key]['ref_no'] = $app['ref_no'];
                }
                $i++;
            }
            //echo '<pre>';print_r($applicationDetails);die;
            $this->applicationDetails = $applicationDetails;
            $this->trackingDetails = $trackingDetails;
            $this->record = true;
        }
    }

    public function executeTracking(sfWebRequest $request) {

    }

    /**
     * This function checks whether the Wire trasnfer number given by user is duplicate or not.
     * @param sfWebRequest $request
     * @return <type>
     */
    public function executeUniqueWireTransferNumber(sfWebRequest $request) {
        $wire_transfer_number = $request->getParameter("wire_transfer_number");
        $isWireTransferNumber = WireTransferTable::getInstance()->findByWireTransferNumber($wire_transfer_number)->count();
        return $isWireTransferNumber;
    }

    /**
     * This function is used to search application by their id. Ansd display's the result for the application that are to be associated.
     * @param sfWebRequest $request
     */
    public function executeSearchAppForWT(sfWebRequest $request) {
        $submitFlag = $request->getParameter('submitFlag');
        $appType = $request->getParameter('app_type');
        $appID = $request->getParameter('app_id');
        $appRefNumber = $request->getParameter('ref_number');
        $notice = false;
        if (isset($submitFlag) && $submitFlag != '') {
            $paymentRequestRecord = Doctrine::getTable('CartItemsTransactions')->getPaymentRequestMoneyOrder($appID, $appType);
            $arrRequestRecord = array();
            for ($u = 0; $u < count($paymentRequestRecord); $u++) {
                $arrRequestRecord[] = $paymentRequestRecord[$u]['id'];
            }
            if (!empty($arrRequestRecord)) {
                $cartItemInfo = Doctrine::getTable('CartItemsTransactions')->getCartItemInfo($arrRequestRecord);
                $arrcartItemInfo = array();
                for ($y = 0; $y < count($cartItemInfo); $y++) {
                    $arrcartItemInfo[] = $cartItemInfo[$y]['cart_id'];
                }
                if (isset($cartItemInfo[0]['cart_id'])) {
                    $cartTrakingInfo = Doctrine::getTable('WireTrackingNumber')->getTrackingNumber($arrcartItemInfo, 'New');
                    if (isset($cartTrakingInfo[0]['user_id'])) {
                        $this->redirect('wt_configuration/saveWiretransfer?userid=' . $cartTrakingInfo[0]['user_id']);
                    } else {
                        $notice = true;
                    }//End of if(isset($cartTrakingInfo[0]['user_id'])){...
                } else {
                    $notice = true;
                }//End of if(isset($cartItemInfo[0]['id'])){;...
            } else {
                $notice = true;
            }//End of if(isset($paymentRequestRecord[0]['id'])){...

            if ($notice) {
                $this->getUser()->setFlash("notice", "Application does not found.");
            }//End of if($notice){...
        }//End of if(isset ($submitFlag) && $submitFlag != '')...
    }

}
