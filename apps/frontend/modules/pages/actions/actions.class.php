<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of actionsclass
 *
 * @author akumar1
 */
class pagesActions extends sfActions {

  public function executeIndex(sfWebRequest $request) {
    if($this->getUser()->isAuthenticated()) {//if user is already logged in send him to respective index page
      $group_name = $this->getUser()->getGroupNames();

      if(in_array(sfConfig::get('app_ipfm_role_portal_admin'),$group_name) || in_array(sfConfig::get('app_ipfm_role_admin'),$group_name)) {
        $this->logMessage("Found a valid user");
        return $this->redirect('@home');
      }
      else {
        return $this->redirect('@home');
      }

    }
  }

  public function executePaymentSummary(){
            $gatewayId = '4';

      $arrSuccessPayment = Doctrine::getTable('ipay4meOrder')->getSuccefullPayment($gatewayId,'0');
      $arrDeclinePayment = Doctrine::getTable('ipay4meOrder')->getSuccefullPayment($gatewayId,'1');
      $this->paymentSuccess = $arrSuccessPayment;
      $this->paymentFail = $arrDeclinePayment;


      //echo '<pre>';print_r($arrSuccessPayment);die;
  }


  public function executeTestCountry($arrCountry){

      $arrCountry = array('0'=>'SA','1'=>'SW');
      for($i=0;$i<count($arrCountry);$i++){
          $arrCardType = Doctrine::getTable('CountryPaymentMode')->getCardType($arrCountry[$i]);
          if(!empty($arrCardType)){
              break;
          }else{
              $arrCardType = Doctrine::getTable('CountryPaymentMode')->getCardType('ALL');
          }
      }
      $splitArr = explode(',',$arrCardType[0]['card_type']);
      $arrGroup = Doctrine::getTable('PaymentMode')->getCardGroup($splitArr);

      $k=0;$j=0;
      $arrFinalArray= array();
      for($i=0;$i<count($arrGroup);$i++){
          $arrFinalArray[$arrGroup[$i]['group_id']][$k]['gateway_id'] = $arrGroup[$i]['gateway_id'];
          $arrFinalArray[$arrGroup[$i]['group_id']][$k]['card_type'] = $arrGroup[$i]['card_type'];
          $arrFinalArray[$arrGroup[$i]['group_id']][$k]['display_name'] = $arrGroup[$i]['display_name'];
          $k++;
      }
      $j=0;
      $returnCardType= array();
      foreach($arrFinalArray as $key=>$values){
          $returnCardType[$j]['group_id'] = $key;
          $i=0;
          foreach($values as $activity){
              $returnCardType[$j]['card_detail'][$i]['gateway_id'] = $activity['gateway_id'];
              $returnCardType[$j]['card_detail'][$i]['card_type'] = $activity['card_type'];
              $returnCardType[$j]['card_detail'][$i]['display_name'] = $activity['display_name'];
              $i++;
          }
          $j++;
      }
      return $returnCardType;

  }
}
?>
