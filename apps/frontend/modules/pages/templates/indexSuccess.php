<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
    <div class="content_container">
        <?php
        $sf = sfContext::getInstance()->getUser();
        if($sf->hasFlash('session_expired_message')){ ?>
        <div id="flash_notice" class="alertBox" style="margin-bottom:5px;" >
            <?php
            echo nl2br($sf->getFlash('session_expired_message'));
            ?>
        </div>
        <?php }
    if($sf->hasFlash('notice')){ ?>
        <div id="flash_notice" class="alertBox" >
            <?php
            echo nl2br($sf->getFlash('notice'));
            ?>
        </div>
        <br/>
        <?php }?>
        <div class="banner_container">
            <div class="banner_img">
                <div id="tmz-slider" class="nivoSlider">
                    <?php echo image_tag('/images/slideshow_02.jpg',array('alt'=>'Online Passport Applications')); ?>
                    <?php echo image_tag('/images/slideshow_03.jpg',array('alt'=>'Online Passport Applications')); ?>
                    <?php echo image_tag('/images/slideshow_05.jpg',array('alt'=>'Online Passport Applications')); ?>
                </div>
            </div>
        </div>
        <div class="login_new_section">
          <?php 
          /**
           * [WP: 069] => CR: 102
           * Login button html calls...
           */
          include_partial('global/allLoginMedia');
          ?>
        </div>
        <div class="banner_right" style="display:none;" >
            <div class="referenzen_topbg"><span>SW Global LLC Services</span><a
                title="Agentur Referenzen" href=""></a></div>
            <div class="scroll">
                <div id="sdiv1" class="selected" onclick="move_down(23, 1)"><span>Apply and Pay For ePassport</span><br>
                Apply for a Nigerian passport online </div>
                <div id="sdiv2" class="scrollDiv" onclick="move_down(86, 2)"><span>Apply and Pay For Visa</span><br>
                Online Nigerian visa application</div>
                <div id="sdiv3" class="scrollDiv"
                     onclick="move_down(149, 3)"><span>Apply and Pay For MRP</span><br>
                Machine readable passport </div>
                <div id="sdiv4" class="refenzen_last" onclick="move_down(212,
                    4)"><span>Apply and Pay For Travel Certificate</span><br>
                ECOWAS travel certificate</div>
            </div>
        </div>
        <div class="clear"></div>
        <div class="page_title">
            <div class="page_title_left"></div>
            <div class="page_title_bg">
                <div class="page_title_text">Welcome to SW GLOBAL LLC where we make obtaining a Passport or Visa easier than ever!!</div>
            </div>
            <div class="page_title_right"></div>
        </div>
        <div class="welcome_left">
            <div class="countries_fees"> Please <a target="_blank" href="javascript:void(0);" onclick="openFeeDetails();return false;">Click Here</a> for list of Countries and Processing Fee</div>
            <div class="queries_refund">
                <div class="img_mail_phone"><?php echo image_tag('mail_img.gif', array('alt' => 'Mail', 'title' => 'Mail' )) ?></div>
            <span class="welcome_info">For any queries/concerns, please contact us at <a href="mailto:support@swgloballlc.com">support@swgloballlc.com</a> </span></div>
            <div class="clear"></div>
            <div class="queries_refund">
                <div class="img_mail_phone"><?php echo image_tag('phone_img.gif', array('alt' => 'Phone', 'title' => 'Phone' )) ?></div>
                <span class="welcome_info_queries">For refund queries, please contact us at <strong>1-877-693-1919,1922</strong> or <a href="mailto:refund@swgloballlc.com">refund@swgloballlc.com</a></span>
            </div>
            <div class="clear"></div>
            <div class="queries_refund">
                <div class="img_mail_phone"><?php echo image_tag('phone_img.gif', array('alt' => 'Phone', 'title' => 'Phone' )) ?></div>
                <span class="welcome_info_queries">For UK call center, please contact us at <strong>+44 20 783 20000</strong> or <strong>+44 20 783 20001</strong></span>
            </div>
            
        </div>
        <div class="travel_img"><?php echo image_tag('travel_img.gif', array('alt' => 'Travel', 'title' => 'Travel' )) ?></div>
    </div>
</div>
<?php //echo link_to(image_tag('/images/ipay4me.gif',array('alt'=>'Know More')), 'cms/knowMore', array(
//'popup' => array('Window title', 'width=610,height=600,left=320,top=0,scrollbars=yes')
//)) ?>
<script>
    function openFeeDetails() {
        window.open('<?php echo url_for('misc/nisFeeDetails') ?>', 'visaFeeDetails',
        'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, copyhistory=no, width=1000, height=800');
    }
</script>

<div class="content_wrapper_bottom"></div>
<?php
/**
 * [WP:081] => CR:117
 * Added facebook logout...
 */
## Add code for logout from gmail, yahoo, openID and Facebook...
$loginType = sfContext::getInstance()->getUser()->getAttribute('loginType');
## Getting application http path...
$sitePath = Settings::getHTTPpath();

if($loginType == "facebook"){
    sfContext::getInstance()->getUser()->getAttributeHolder()->remove('loginType');
    $facebookAccessToken = sfContext::getInstance()->getUser()->getAttribute('facebookAccessToken');
    sfContext::getInstance()->getUser()->getAttributeHolder()->remove('facebookAccessToken');    
    $facebookLogoutUrl = 'https://www.facebook.com/logout.php?next='.$sitePath.'&access_token='.$facebookAccessToken;
    echo "<iframe style='display:none;' src ='".$facebookLogoutUrl."' width='1' height='1'></iframe>";
    //$facebook->destroySession();      
    ?>
    <script>
        //$(document).ready(function(){
            window.location.reload();
        //})
    </script>
    <?php
}