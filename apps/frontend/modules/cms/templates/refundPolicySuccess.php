<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">
<?php include_partial('global/innerHeading',array('heading'=>'Refund Policy'));?>
    <div class="clear"></div>
    <p class="body_txtp"> A refund will be issued, upon request, and with supporting proof and/or evidence from the customer, for the reasons listed below under Acceptable Reasons for a Refund. Notwithstanding the above, SW Global LLC reserves the right to grant or deny refunds to anyone for any reason. </p>
    <br>
      <ol class="contentul">
     <li><b>Acceptable Reasons for a Refund:</b><br>
        <ol type="A">
        <li>Someone has used your credit/Debit card fraudulently.</li>
        <li>If there is a mistake made, as solely determined by SW Global LLC, with respect to two or more payment transactions being processed for ONE Application.</li>
        <li>Any other illegal or non-approved use of your credit/debit card or billing information.</li>
        </ol>
        </li>
      <li><b>Requesting A Refund:</b> <br>
        To request a refund, please email <a href="mailto:refund@swgloballlc.com" class="bluelink">refund@swgloballlc.com</a> with details of your request and supporting documents if any. Refund requests are processed in the order they were received, and you will have the opportunity to provide SW Global LLC with all the necessary details needed to process any refunds that fall within the Acceptable Reasons for a Refund.  <br>
        Refund may be requested at anytime until the service you paid for is received.&nbsp; </li>
      <li><span class="redTxt"><b>Abuse of Refund Policy: <br>
        Where SW Global LLC, in its sole discretion, believes a customer has or is abusing this Refund Policy through, among others, fraudulent requests for refunds or initiation of chargebacks after submitting applications using the SW Global LLC platform, SW Global LLC shall have the right to: </b>
        <ol type="A">
          <li>In the case of visas or passports, request such visa or passport to be immediately revoked by the issuing authority. </li>
          <li> Immediately publish to relevant Immigration Authorities at Immigration Borders, a “fraud watch list” of the names and/or passport or visa numbers of suspected customers. </li>
            <li>Block a Customer’s name and/or method of payment from being used on the SW Global LLC and relevant immigration authorities site in any manner.  This shall include baring the customer from applying for a visa or passport on SW Global LLC unless specifically approved by SW Global LLC.</li>
        </ol>
        </span> </li>
        <li><b>Refund Updates and Modifications</b><br>
        SW Global LLC reserves the right to modify this Refund Policy at its discretion, or against any customer it believes is abusing this Refund Policy and in some cases reserves the right to refuse refunds if refund policy abuse is detected. Any such revision or change will be binding and effective immediately after posting of the revised Refund Policy on swgloballlc.com. You agree to periodically review our Refund Policy, including the current version of our Refund Policy as made available at <a href="https://www.swgloballlc.com/cms/refundPolicy" class="bluelink" target="_blank">https://www.swgloballlc.com/cms/refundPolicy.</a> </li>
        <li><b>Disclaimer</b><br>
        SW Global LLC accepts no responsibility for the products or services provided by any participating Passport Agency or Embassy/Consulate in connection with the actual granting of passports or visas, nor for any delays, mistakes on passports, loss of passports or other materials occasioned by such services or by any delivery services such as FedEx, UPS, or the US or other Postal Service. Damage compensation is not available from SW Global LLC
    </ol>
    <br>
    <br>
</div>
</div>
<div class="content_wrapper_bottom"></div>