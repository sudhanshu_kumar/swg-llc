<?php include_partial('global/innerHeading',array('heading'=>'Features'));?>

<div class="global_content4 clearfix">
<?php echo image_tag('/images/secureBanner.png',array('alt'=>'')); ?><br />
<br />
  <div class="brdBox">
    <p class="global_left_headP">Accepts all major international credit cards - Visa Card, MasterCard and AMEX.</p>
    <p class="body_txtp"> SW Global LLC allows NIS users to transact through any major credit card. Today, most online merchants working directly with a bank miss out on some business opportunities since they offer only MasterCard and/or Visa Card transaction. However, SW Global LLC gives you the facility to accept all major international credit cards such as Visa, MasterCard and American Express Cards. Hence SW Global LLC minimizes any loss of transactions. </p>
    <p class="global_left_headP">Go global with US $ transaction</p>    
    <p class="global_left_headP">Secure your payment</p>
    <p class="body_txtp"> The fear of fraud is one of the most common reasons why customers are reluctant to share their sensitive card information online. This fear often forces them to abandon their shopping carts mid-way. </p>
    <p class="body_txtp"> Online fraud can damage both your overall profitability and reputation. To prevent fear from damaging your payment potential and success, you need to reassure potential customers about their card information safety by providing them with the highest level of security including a robust system that protect both of you from the risks of transacting online. SW Global LLC payment gateway offers just the perfect solution that can keep all such fears away from you and your customers. </p>
  </div>
</div>
