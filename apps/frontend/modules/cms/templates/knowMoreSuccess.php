    <link rel="stylesheet" type="text/css" media="screen" href="/ipay4me_new/web/css/style.css" />

<?php include_partial('global/innerHeading',array('heading'=>'SW Global LLC eWallet'));?>
<div class="global_content4 clearfix">
<div class="brdBox">
<?php

echo htmlspecialchars_decode('<b>To our valued customers:</b><br><br>');

echo htmlspecialchars_decode('Please note that we will be introducing the SW Global LLC ewallet for customers who process large<br> numbers of transactions daily (commercial customers).  <br>This service will be available effective MIDNIGHT GMT Sunday October 24th.<br><br>

The ewallet will have the following features:<br><br>
<ul>
<li>Customers can open an ewallet account on SW Global LLC;</li>
<li>Ability to load up to $15,000 per month on your ewallet account;</li>
<li>Ability to process multiple (unlimited) applications at the same time (up to the amount loaded);</li>
<li>Dedicated customer service;</li>
</ul>
To use the SW Global LLC ewallet, users must:<br><br>
<ul>
<li>Register to use the ewallet (this will include the collection of basic KYC information and ID).  <br>SMS verification is required.  A verification email or phone call may also be required.</li>
<li>Ensure that credit cards used for loading the ewallet ALWAYS matches the name and address<br> of the registered (same applies if a corporate card is used).</li>
</ul>
Fee:  A service charge of 4% per loading or funding of the ewallet (minimum $10 and maximum $50)<br><br>

<b>NOTE:  Due to anti money laundering laws, there are no refunds for deposits into ewallets.<br>  Such funds MUST BE used on applications on the NIS platform. <br> Customers are urged to be aware of this fact and regulate funding of ewallet accounts accordingly.</b><br><br>

THIS SERVICE IS PROVIDED ONLY TO FACILITATE FASTER PROCESSING OF LARGE NUMBER <br> OF APPLICATIONS. <br> IT IS NOT MANDATORY TO USE THIS SERVICE IN ORDER TO PROCESS VISA OR PASSPORT APPLICATIONS.
</pre>');
?>
<!--  <p class="body_txtp">
 In SW Global LLC, the user cannot make multiple payments using a credit card. However, <br> if the user wants to make multiple payments, the user has the option of eWallet.

  <br><br>

 
  Know your customer (KYC) PIN Generation initiative is the due diligence and bank regulation <br> that financial institutions and other regulated companies must perform to identify their clients<br> and ascertain relevant information pertinent to doing financial business with them.<br> In this process customer is asked to upload certain documents to ascertain the identity <br>of the customer and thereafter the system generates a PIN for transaction safety <br>and authentication.
  
  <br> On authenticating the Pin, an eWallet account is generated for the customer which can be<br>online recharged. Payment via eWallet is then enabled. 
  <br><br>The PIN is sent via SMS and charges for the SMS are deducted from the  <br>customer's eWallet account.
  </p>
  -->
</div>
</div>