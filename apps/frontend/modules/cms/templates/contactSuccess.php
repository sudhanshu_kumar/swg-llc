<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">
<?php include_partial('global/innerHeading',array('heading'=>'Contact Us'));?>
    <div class="clear"></div>
    <div class="wd400_new">
<div class="flLeft"><?php echo image_tag('/images/contact.png',array('alt'=>'Contact Us')); ?></div>
<div class="brdBox flRight pd100">
 <p class="global_left_headP">Address:</p>
        <p class="body_txtp"> SW Global LLC <br />
50 Albany Turnpike<br />
Suite 5032 <br />
Canton, CT 06019<br />
<br>
          <strong>Email:</strong> <a class="bluelink" href="mailto:<?php echo sfConfig::get('app_contact_us_email');?>"><?php echo sfConfig::get('app_contact_us_email');?></a> <br />
          <strong>Support:</strong> <a class="bluelink" href="mailto:<?php echo sfConfig::get('app_support_email');?>"><?php echo sfConfig::get('app_support_email');?></a> </p>
</div>
</div>
  </div>
  <div class="content_wrapper_bottom"></div>
</div>
