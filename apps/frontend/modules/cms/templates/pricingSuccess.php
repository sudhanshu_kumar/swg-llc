<?php include_partial('global/innerHeading',array('heading'=>'Pricing'));?>
<div class="global_content4 clearfix">
<div class="brdBox">
  <p class="body_txtp">
  Our fees are now the same for all accounts - Personal, Premier, and Business. Please note that the Purchase rate applies to all payments using Request Money.</p>
<table width="100%" class="nobrd">
  <tbody><tr>
  <td width="50%"><?php echo image_tag('/images/shopping.jpg',array('alt'=>'')); ?></td>
  <td width="50%"><?php echo image_tag('/images/perTransfer.jpg',array('alt'=>'')); ?></td>
  </tr>
 <!-- <tr>
    <td valign="top"><table width="100%">
      <tbody><tr>
        <td colspan="2" class="blbar">Purchase</td>
      </tr>
      <tr>
        <td width="17%" class="txtBold">Pay</td>
        <td width="83%" class="txtBold"><strong>Get paid</strong></td>
      </tr>
      <tr>
        <td class="txtBold"><strong>Free</strong></td>
        <td><a href="">1.9% to 2.9% + $0.30 USD</a></td>
      </tr>
    </tbody></table></td>
    <td width="50%" valign="top"><table width="100%">
      <tbody><tr>
        <td colspan="2" class="blbar">Personal Transfer</td>
      </tr>
      <tr>
        <td width="23%" class="txtBold"><strong>Send money</strong></td>
        <td width="77%" class="txtBold"><strong>Receive money</strong></td>
      </tr>
      <tr>
        <td colspan="2">
        Free when the money comes from ipay4me balance or bank account.
          <br/>
          <br/>
          <a href="#">2.9% + $0.30 USD</a><br/>


          <br/>
          when the money comes from a debit or credit card or ipay4me Credit<br/>


(the sender decides who pays this fee).
        </td>
      </tr>-->
    </tbody></table></td>
  </tr>
</tbody></table>
 <p class="body_txtp">
SW Global LLC offers an unbeatable package with a complete bouquet of credit card processing options, global quality service and state-of-the-art verification and security technologies, all at very competitive rates. With all the major payment options, all the tools, all in one place, all for one price and all tied together in a straightforward, integrated step-by-step process that works efficiently. Nothing else is even remotely close. By using SW Global LLC payment gateway services, you get a globally scalable platform to enter the digital economy in style.
 </p>
 <p class="body_txtp">
 To know more please drop us an email: <a href="mailto:contact@swgloballlc.com">contact@swgloballlc.com</a>.</p>

<!--
<br/> <br/>
            <div class="hlBox clear">
              <p class="hlarrow"><?php echo image_tag('/images/blue_arrow.jpg',array('alt'=>'')); ?></p>
              <p class="blhlTxt grayTxt"><span><strong>Additional fees  for</strong> </span><br/>
                <a href="">cross border payments</a> and currency conversion.<br/>
A cross border fee of <a href="#">1.0% </a> applies to payments received from senders in another country.

Payments requiring a currency conversion include a 2.5% fee. <br/>

              <a href="#PayDo">What does Pay4Me International do?</a></p>
            </div>
           <p> </p><p> </p>

           -->
  </div>
</div>