<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">
<?php include_partial('global/innerHeading',array('heading'=>'Privacy Policy'));?>
    <div class="clear"></div>
<div class="brdBox">
      <p class="body_txtp"> <b>This Privacy Notice was last modified: June 3, 2010</b><br/>
        <br/>
        The swgloballlc.com Privacy policy explains the ways we manage your personal information. It must be read in conjunction with the swgloballlc.com TERMS OF USE and You automatically agree to this policy when you enroll in the SW Global LLC Service. </p>
 <p class="global_left_headP">The Privacy Policy</p>
      <p class="body_txtp"> This Privacy Policy (“Privacy Policy”) applies to SW GLOBAL LLC, its parents and related entities, subsidiaries, whether or not wholly owned (“SW Global LLC”) and specifically, it outlines the types of information that SW Global LLC gathers about you (“User”) while you are using the <a href="https://www.swgloballlc.com" class="bluelink" target="_blank">https://www.swgloballlc.com</a> website or any other website that links to or uses the SW Global LLC Service now or in the future ("SW Global LLC Website(s)"). This Privacy Policy is incorporated into and is subject to theSW Global LLC Terms of Use. Your use of the SW Global LLC Website and any personal information you provide on the SW Global LLC Website remains subject to the terms of this Privacy Policy and our Terms of Use. </p>
 <p class="global_left_headP">Enrolment Information</p>
      <p class="body_txtp"> During enrolment to SW Global LLC, we require you to enter your personal information. These may include your name, debit or credit card number, card verification number, card expiration number, address, telephone number and email addresses. For Providers, we require that you provide your bank account number and may require your personal address, business category, social security number and other information concerning your Payment Transaction (‘Information”). This Information is stored with your SW Global LLC account. </p>
 <p class="global_left_headP">Personal Information & Information Use </p>
      <p class="body_txtp"> Personally identifiable Information submitted via the SW Global LLC Website is used only for limited purposes.  For example, when you register to request future communications from SW Global LLC, the information you submit may be used to respond to you, to create a personal profile to facilitate further requests or inquiries, or to personalize your web site experience.  In addition, the webmasters may, where necessary, use your information for various site related tasks.

Such Information will not otherwise be shared with third parties unless so disclosed at the point of collection. Personally identifiable Information may be transmitted internationally to third parties for the purposes identified above. This may include transfer to countries without data protection rules similar to those in effect in your country of residence. By providing information to the SW Global LLC Website, you are consenting to such transfers.

        Where appropriate, personally identifiable Information may be disclosed to law enforcement, regulatory or other government agencies, or third parties where necessary or desirable to comply with legal or regulatory obligations or requests or for the purposes identified above. SW Global LLC also reserves the right to disclose personally identifiable Information and/or non-personally-identifiable information that SW Global LLC believes, in good faith, is appropriate or necessary to enforce the Terms of Use, take precautions against liability, to investigate and defend itself against any third-party claims or allegations, to assist government enforcement agencies, to protect the security or integrity of the SW Global LLC Website, and to protect the rights, property, or personal safety of SW Global LLC, our Users or others. In all cases we will treat requests to access information or change information in accordance with applicable legal requirements. </p>
 <p class="global_left_headP">Third Party Information</p>
      <p class="body_txtp"> We sometimes obtain information about you from third parties to save you from fraud, financial misconducts and also to verify the information which you have already provided. We may obtain such information about Providers from various Business Information Services. </p>
 <p class="global_left_headP">Transaction Information</p>
      <p class="body_txtp"> We collate information about every Payment Transaction carried out by you using the SW Global LLC Service. These include but are not limited to transaction amount, description of the goods or services being provided by the Provider, names of the Provider and the Customer and the modes of payment employed. </p>
 <p class="global_left_headP">SW Global LLC Usage</p>
      <p class="body_txtp"> We may require information about your Service usage to enable us validate your identity and thereby stall any upcoming fraudulent conduct. Such information will only be used to prevent fraud except with your permission to use for other reasons. </p>
 <p class="global_left_headP">Cookies</p>
      <p class="body_txtp"> We send cookies to your computer for browser identification every time you access a SW Global LLC web page. This enables us to improve the quality of our service by saving your preferences and remarking your trends. If you choose, you can set your browser to reject all cookies or alert you when a cookie is being sent; however if your cookies are deactivated, you may not be able to accessSW Global LLC. </p>
 <p class="global_left_headP">Information on Logs</p>
      <p class="body_txtp"> Our servers record information sent by your browser each time you use SW Global LLC. These logs may include your IP address, the browser type and language, the date and time of your request, your web requests and cookies that help identify your browser.</p>
 <p class="global_left_headP">User Communication</p>
      <p class="body_txtp"> We may save emails and other communication sent by you to enable us process your queries and respond to your request and also, improve our services. </p>
 <p class="global_left_headP">Interface Links</p>
      <p class="body_txtp"> We may present links in a format that will help us identify if they were followed or not. Using this information, we can improve the quality of our search feature. Additionally, we use the information to provide our services to you; carry out auditing, research and analysis for improving the quality of our services; facilitate the technical functionality of our network and also, build new services.<br/>
        <br/>
        Note that we may process your personal information on a Server which is located outside your own country. Sometimes also, we may process personal information on behalf of a third party. </p>
 <p class="global_left_headP">Information Sharing</p>
      <p class="body_txtp"> We shall not use your personal information for commercial purposes or share with individuals outside SW Global LLC. If you make a purchase using SW Global LLC, we may share some information about your debit/credit card with the Provider/Seller to recognize the transaction but we will not share your full debit card number with Provider/Seller without your written consent.<br/>
        <br/>
However, we may share your personal information with individuals outside SW Global LLC on the following situations:<br/>
      </p>
      <ul class="contentul">
<li>Using a Payment Instrument for payment on <span class="body_txtp">SW Global LLC</span> necessitates that we share your name and debit card number with the banks and other entities that process the Payment Instrument. Providers may at times require telephone numbers of their Customers to process a Payment Transaction.</li>
<li> To prevent or detect fraud or technical issues.</li>
<li> Where we have your consent and User has elected to opt-in for the sharing of any personal information. </li>
<li>We provide such information to our subsidiaries, affiliated companies or other trusted businesses or persons for the purpose of processing personal information on our behalf. We require that these parties agree to process such information based on our instructions and in compliance with this Privacy Policy and any other appropriate confidentiality and security measures.</li>
<li>We have a good faith belief that access, use, preservation or disclosure of such information is reasonably necessary to (a) satisfy any applicable law, regulation, legal process or enforceable governmental request, (b) enforce applicable Terms of Service, including investigation of potential violations thereof, (c) detect, prevent, or otherwise address fraud, security or technical issues, or (d) protect against imminent harm to the rights, property or safety of Google, its users or the public as required or permitted by law. </li>
<li>We may share aggregated non-personally identifiable information with third parties. For example, we may disclose that a certain percentage of our users have a billing address in a particular geographic area, or that a certain percentage of users use a particular type of credit or debit card. However, if we do share this aggregated information, we do not include personally identifiable Information without your explicit opt-in consent or in the limited circumstances described above. </li>
<li> Should <span class="body_txtp">SW Global LLC</span> become involved in a merger, acquisition, or any form of sale of some or all of their assets, a noticed will be mailed to you before your personal information is transferred and becomes subject to a different privacy policy.</li>
</ul>
 <p class="global_left_headP">Information security</p>
      <p class="body_txtp"> Note that you must keep your User/Private ID and Password confidential and never share with anyone. If you do share these with a third party, they can have access to your account and all personal information. </p>
<p class="global_left_headP">Updating Your Personal Information</p>
      <p class="body_txtp"> You will be able to update your payment information by signing into your SW Global LLC account and going to the appropriate page. In this page, you can change your address and mode of payment. You can also see or print your previous transaction history.<br/>
        <br/>
        Contact us if you do not want your payment information or transaction history viewable using SW Global LLC. However, to facilitate our auditing and reporting functions, and also to guard against fraud, the information will be saved to our systems but will not be shared with anybody.<br/>
        <br/>
        Also, note that disabling your SW Global LLC Transaction History functionality does not close your SW Global LLC account.<br/>
        <br/>
        In the Event of Merger, Sale, or Bankruptcy.<br/>
        <br/>
        In the event that SW Global LLC is acquired by or merged with a third party entity, we reserve the right, in any of these circumstances, to transfer or assign the information we have collected from our Users as part of such merger, acquisition, sale, or other change of control. In the unlikely event of our bankruptcy, insolvency, reorganization, receivership, or assignment for the benefit of creditors, or the application of laws or equitable principles affecting creditors' rights generally, we may not be able to control how your personal information is treated, transferred, or used.<br/>
        <br/>
</p>
 <p class="global_left_headP">Changes to Privacy Notice</p>
      <p class="body_txtp"> This Privacy Notice may be revised periodically and this will be reflected by the "effective date" detailed above. Please revisit this page to stay aware of any changes. In general,SW Global LLC may only use your personal information in the manner described in the Privacy Policy in effect when we received the personal information you provided. Your continued use of the SW Global LLC Websites constitutes your agreement to this Privacy Policy and any future revisions. <br/>
        <br/>
        Please feel free to direct any questions or concerns regarding this Policy or SW Global LLC treatment of personal information by contacting us through this web site at privacy@swgloballlc.com . When we receive formal written complaints at this address, it is SW Global LLC policy to contact the complaining User regarding his or her concerns. We will cooperate with the appropriate regulatory authorities, including local data protection authorities, to resolve any complaints regarding the transfer of personal data that cannot be resolved between SW Global LLC and an individual. </p>
 <p class="global_left_headP">Transfer of User Data outside the User Jurisdiction</p>
      <p class="body_txtp"> The Internet is a global environment. In order to provide this website we may need to transfer User Data to locations outside the jurisdiction in which you are viewing this website (the <strong>User Jurisdiction)</strong> and process User Data outside the User Jurisdiction therefore it must be noted that such transfers and processing of User Data may be in locations outside the User’s Jurisdiction and any data sent or uploaded by you may be accessible in jurisdictions outside the User’s Jurisdiction and that the level of data protection offered in such jurisdictions may be more or less than that offered within the User Jurisdiction.<br/>
        <br/>
        User Data may be controlled and processed by any of the SW Global LLC offices some of which may be outside a User Jurisdiction.  The location of our offices may change from time to time and SW Global LLC may acquire offices in any number of countries or territories at any time, any one or more of which may act as controllers of and/or process User Data.<br/>
        <br/>
        By continuing to use this website and by providing any personal data (including sensitive personal data) to us via this website or e-mail addresses provided on this website, you are consenting to such transfers, provided that they are in accordance with the purposes set out above and elsewhere in the Terms of Use. Please do not send us any personal data if you do not consent to the transfer of this information to locations outside the User Jurisdiction.<br/>
        <br/>
  </p>
</div>
</div>
</div>
<div class="content_wrapper_bottom"></div>