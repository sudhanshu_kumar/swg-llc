    <link rel="stylesheet" type="text/css" media="screen" href="/ipay4me_new/web/css/style.css" />

<link rel="stylesheet" type="text/css" media="screen" href="/ipay4me_new/web/css/CalendarControl.css" />
     <link rel="stylesheet" type="text/css" media="print" href="/ipay4me_new/web/css/print.css" />
<?php //include_partial('global/innerHeading',array('heading'=>'Terms of Use'));?>

     
    <div class="blbar_new">Terms and Conditions</div>


</div>
<div class="global_content4 clearfix">
<div class="brdBox">
 <p class="global_left_headP">About The Website</p>
  <p class="body_txtp">
 This Website (<a href="https://www.swgloballlc.com">https://www.swgloballlc.com</a>) is owned and operated by SW Global LLC and, from time to time, its subsidiary and affiliate corporations, successors, and assigns (collectively referred to as “SW Global LLC”).    <br/>
<br/>


The website <a href="https://www.swgloballlc.com">https://www.swgloballlc.com</a> (“SW Global LLC Website” or ‘Website”), is owned and licensed for use by SW Global LLC (“SW Global LLC”) and your use of the payment and other modules, information, graphics, text and other materials (“SW Global LLC Service” or “Service”) is governed by these Terms of Use and the associated Privacy Policy. By using this Website’s Services, Users agree to comply with all the terms and conditions hereof. <br/><br/>


This agreement sets forth the terms and conditions that apply to use of this Program and services by a person, organization or other entity (“Users” or “you”). By using this Program and/or services, Operators agree to comply with all the terms and conditions hereof.

  </p>
  <p class="global_left_headP">NIS Application Procedure</p>

    <ul class="contentul">
        <li>For Visa application, after successful payment of your application, you are required to visit Nigeria Immigration Portal and check your application again for the given date for interview. On the specified date, you must visit specified Nigerian Embassy on your application. If the application is accepted, a Visa is affixed to your passport and the passport is sent back to them in the prepaid envelop, in many cases you can collect your passport yourself from embassy as well. Please be advised that fee once paid, will not be refunded even if the application is rejected at embassy.
        </li>
        <li>For Passport application, after successful payment of your application, you are required to visit Nigeria Immigration Portal and check your application again for the given date for interview. On the specified date, you must visit specified Nigerian Immigration office on your application along with all necessary documents. If the application is accepted, your passport will be processed further (as per nature of your passport application) and it will be sent back in the prepaid envelop. Please be advised that fee once paid, will not be refunded even if the application is rejected at Nigeria Immigration.
        </li>
    </ul>
 <p class="global_left_headP">Terms of Use</p>
  <p class="body_txtp">
By using this Service the Operator agrees to be bound by these terms and conditions (“Terms of Use”) AND THE CONDITIONS OF THE PRIVACY POLICY.
<br/><br/>
SW Global LLC hereby grants you permission to use this Service solely in the manner set forth in the SW Global LLC User Guide, which is incorporated herein by reference; provided that you will not copy, distribute, alter or modify any part of this Program in any medium without SW Global LLC express written permission.
<br/><br/>

THE FOLLOWING CONDITIONS OF SERVICE BETWEEN USER AND SW Global LLC SERVE AS A BINDING LEGAL DOCUMENT. IN ADDITION TO THE TERMS OF USE CONTAINED IN THIS DOCUMENT OTHER TERMS AND CONDITIONS MAY APPLY TO YOR USE OF THIS PROGRAM OR TO PRODUCTS AND SERVICES OFFERED VIA THE SW Global LLC SERVICE AND YOU WILL BE BOUND BY THESE ADDITIONAL TERMS WHERE APPLICABLE.
</p>

   <p class="global_left_headP"> Definition of Terms</p>
  <p class="body_txtp">
  <strong>Commercial Transaction:</strong> shall mean the commencement and/or completion of any Payment Transaction between a SW Global LLC Customer and a Provider for an authorised transfer of goods or services using the SW Global LLC Service.
  <br/><br/>

<strong>Customer/You: </strong>shall mean any individual or corporation which presents a request for a Commercial Transaction at an Operator’s Location using the SW Global LLC Service.
<br/><br/>

<strong>Operator/You:</strong> shall specifically mean any financial institution that initiates and validates online payment transactions using the SW Global LLC Service.
<br/><br/>

<strong>Operator’s Location:</strong> shall mean the business premises or other designated location of registered Operators where the SW Global LLC Service has been licensed to be operated by the Company.
<br/><br/>

<strong>SW Global LLC or Company:</strong> shall mean SW Global LLC, and any subsidiaries and affiliates.
<br/><br/>

<strong>SW Global LLC Service or Service:</strong> shall mean the  SW Global LLC Service accessible via the SW Global LLC Website.
<br/><br/>

<strong>Payment Transaction:</strong> shall mean a payment process initiated and validated for an authorised transfer of goods or service using the SW Global LLC Service through the Website after payment is made.
<br/><br/>

<strong>Provider:</strong> shall mean any individual or corporation that provides goods or services for which payment shall be made using the SW Global LLC Service.
<br/><br/>

<strong>User:</strong> shall mean any individual or entity that initiates a Commercial Transaction to be completed by an Operator using the SW Global LLC Service.
<br/><br/>


<strong>User ID and Password:</strong> shall mean a unique user identifier and password selected by Operators to access the SW Global LLC Service via the Website.
<br/><br/>


<strong>User Jurisdiction:</strong> shall mean the territory from which a User or Operator is physically located upon initiating payment using the SW Global LLC Service or on accessing the SW Global LLC Website.

   <p class="global_left_headP">Registration Requirements </p>
  <p class="body_txtp">
  In order to access and use the SW Global LLC Program, the Operator must, first obtain access to the World Wide Web and pay any service, telecommunication or other fees associated with or incurred by such access and must provide all equipment necessary to connect to the World Wide Web.

Operators acknowledge and agree that the SW Global LLC program is offered on an "as is" and "as available" basis only. SW Global LLC may impose limits or restrictions on the use of the Service with or without notice to and in certain situations may withdraw the SW Global LLC Service for any length of time at any time.  <br/>

  </p>

   <p class="global_left_headP">Ability to Accept Terms of Use</p>
  <p class="body_txtp">
  You affirm that you are either more than 18 years of age, or an emancipated minor, or possess legal parental or guardian consent, and are fully able and competent to enter into the terms, conditions, obligations, affirmations, representations, and warranties set forth in these Terms of Use, and to abide by and comply with these Terms of Use.

<br/><br/>

Operators must have permission to carry out business in their country of operation and Operators acknowledge that they shall be fully responsible for any person accessing and conduct while accessing the SW Global LLC Website at an Operator’s Location.
</p>

 <p class="global_left_headP">Processing of Payment Transactions</p>
  <p class="body_txtp">
The SW Global LLC Service facilitates the processing of Payment Transactions to complete a payment for a purchase between a Customer and a Provider. The Service will store information from Customers and will process Payment Transactions on behalf of Providers.
<br/><br/>
Due to the nature of the Service, SW Global LLC process of payments for commercial transactions between a Customer and a Provider through the SW Global LLC Service means that SW Global LLC shall have access to details of a Customer’s Payment Instrument. SW Global LLC reserves the right to store details from Customers including their Payment Instruments; and will automate Payment Transactions on behalf of Providers through the appropriate Debit Card Network and/or financial institutions. SW Global LLC Further reserves the right to delay or stop the payment processing of transactions that may be dubious, fraudulent, or is a violation of the SW Global LLC Terms of Use or Privacy Policy or a violation of existing laws.
</p>
   <p class="global_left_headP">Acceptable Payment Transactions</p>
  <p class="body_txtp">
  Users and Operators warrant that the processing of Payment Transactions using SW Global LLC for services and products procured from an authorised Provider shall be genuine and legal transactions only.  SW Global LLC shall not process any transfer of money between a Customer and a Provider that is not an authorised Payment Transaction.
<br/><br/>
In addition all Users of the SW Global LLC Service accept and acknowledge that use of the SW Global LLC Service for Payment Transactions is a Commercial transactions which exists solely between Provider and Customer and SW Global LLC as a facilitator of such Payment Transactions through it’s Service is not and shall not be deemed a party to a Commercial transaction.
</p>
   <p class="global_left_headP">Restriction on SW Global LLC Usage</p>
  <p class="body_txtp">
  At any time, SW Global LLC may define, change or modify acceptable practices and restrictions related to the use of the SW Global LLC Service; including restrictions to access for Customers, Users, Operators as well as modifications or changes to Payment Transactions. SW Global LLC reserves the right to cancel, suspend or modify any aspect of the SW Global LLC Service without notice or liability with respect to SW Global LLC schedule of operations; availability or any other feature of the SW Global LLC Service.
<br/><br/>
SW Global LLC may also restrict some features of the Service or limit access to some or all the features of the Service without notice or liability and may also restrict or suspend your usage of the Service entirely. Any such suspensions or restrictions shall be communicated to you via electronic mail. Any suspension or restriction of your use of the Service will not affect your rights and obligations pursuant to these Terms of Use that arises before or after such suspension.
</p>
   <p class="global_left_headP">User ID and Password Information</p>
  <p class="body_txtp">

 Users and Operators warrant and acknowledge that they are responsible for:<br/>
</p><ul class="contentul">
<li> Keeping their User ID and Password confidential;  </li>

<li> any Payment Transactions processed as a result of access by persons that You have exposed your User ID and Password to;  </li>

<li> Consequences of misuse of use of your User ID and Password;</li>

<li> any other use of a User or Operator’s User Id and Password which breaches these Terms of Use or any applicable laws.</li>
</ul><br/><br/>
<p class="body_txtp">
You accept to notify SW Global LLC as soon as an unauthorized use of your User ID or/and Password is suspected; or you are aware of any other breach of security concerning the SW Global LLC Service.</p><br/><br/>

<p class="body_txtp">Operators acknowledge and accept that all employees, officers, agents and representatives of Operators who currently have or have had access to User ID and/or Passwords (including former employees) shall be deemed authorized Users to use the SW Global LLC Service and these Terms of Use shall be binding in them.</p>

   <p class="global_left_headP">Privacy</p>
  <p class="body_txtp">
  Your privacy and security is very important to SW Global LLC. Certain data items may be collected and used by the SW Global LLC Website in order to, for example, process your instructions or access the site. The Website may record and track the use you make of this Website. The use of this data is subject to our Privacy Policy which we advise that you read in full, the terms and conditions of which are incorporated by reference.
  </p>
   <p class="global_left_headP">Provider Websites/ Links To Third Party Sites</p>
  <p class="body_txtp">
  The SW Global LLC Website may contain links to websites, which are owned and operated by third parties independent of us (“Third Party Websites”). SW Global LLC does not sponsor, endorse or approve of the operators of Third Party Websites or the information, graphics and material, which may be found at Third Party Websites (“Third Party Material”). Subject to any applicable law, which cannot be excluded, we make no warranties or representation with regards to the quality, accuracy, merchantability or fitness of purpose of Third Party Material or products or services available through Third Party Websites; or That Third Party Material does not infringe the intellectual property rights of any person. We are not authorizing the reproduction of Third Party Material by linking material on this Website to Third Party Material.
<br/><br/>
All offers to sell and statements relating to goods and services available on Third Party Websites are the responsibility of and given by the Third Party Website operators. SW Global LLC expressly disclaims acting in any other respect on behalf of Third Party Website operators.
<br/><br/>
Customers and Providers agree and warrant not to include SW Global LLC in any legal proceedings concerning any disagreement as a result of any Commercial Transaction. This Conditions of Service shall not put aside any claims, defences or rights that you may possess concerning a Payment Transaction directly related to a technical fault in the SW Global LLC Service for which Users must notify SW Global LLC within ten (10) business days of the date of a Payment Transaction.
</p>
   <p class="global_left_headP">Electronic Communications Usage</p>
  <p class="body_txtp">
  <strong>Communication from SW Global LLC to a User or Operator:</strong><br/><br/>


SW Global LLC may communicate with you concerning SW Global LLC Use, Payment Transaction or other matters concerning the SW Global LLC Service, at any time by electronic means. These correspondences may comprise sending mails to the email address provided by You during enrolment and/or posting notices on the SW Global LLC site. You hereby agree that we may provide such communications or records by means of electronic communications.
<br/><br/>
It shall be assumed that you have received all electronic communications when SW Global LLC sends the electronic communication to the email address provided by you at the time of enrolment or as changed by you afterwards in a manner set out in the SW Global LLC User Guide or when SW Global LLC posts electronic communication on the SW Global LLC site.
<br/><br/>
<strong>Communication from a User or Operator to SW Global LLC</strong><br/><br/>

A User or Operator may contact SW Global LLC through the SW Global LLC contact page on the Website to:
<ul class="contentul">
<li>Request another electronic copy of the electronic communication.</li>
<li>Request a paper copy of an electronic communication (<span class="body_txtp">SW Global LLC</span> reserves the right to charge a fee to provide the paper copy).</li>
<li>Update your personal information such as your email address to be used for mailing electronic communications.</li>
<li>Withdraw consent to receive electronic communications (<span class="body_txtp">SW Global LLC</span> reserves the right to stop your use of the <span class="body_txtp">SW Global LLC</span> service if you withdraw consent or refuse to receive electronic communications from <span class="body_txtp">SW Global LLC</span>).
</li>
</ul><br/><br/>
<strong>Refunds</strong><br/><br/>
<p class="body_txtp">
 The Commercial Transaction between Customer and Provider governs terms and conditions on refunds of Payment Transactions. SW Global LLC cannot authorise any refunds, full or partial to any User. Further information on refunds for Users and Operators can be found on the SW Global LLC Website via the FAQ link.</p>
<br/><br/>
<strong>SW Global LLC Service and Functions</strong><br/>
<br/>
<p class="body_txtp">
 SW Global LLC business is to provide the automation of online payments between third parties and the provision of services closely related to the issuance of online payments. Since the Service is limited to online payment transactions SW Global LLC does not qualify as a financial institution and monies held by SW Global LLC or any User in connection with such Payment Transaction automation shall not be deemed as deposits by Customer or Operators neither are they insured to benefit the Customer by the Federal Deposit Insurance Corporation or any governmental agency. SW Global LLC enables you to make payments to and accept payments from third parties. SW Global LLC is therefore an independent contractor for all purposes. SW Global LLC does not have control of nor assumes the liability or legality for the products or services that are paid for with our Service. We do not guarantee the identity of any User or Operator or ensure that a Customer will complete a Payment Transaction.   </p>
   <p class="global_left_headP">Termination</p>
  <p class="body_txtp">
SW Global LLC, in its sole discretion may end your Service usage without liability to you or any third party. This also comprises without restriction inactivity or violation of this Terms of Use or other policies we may define from time to time.
<br/><br/>
After termination of your Service usage, you are liable for all Payment Transactions and other obligations, which you have incurred. After termination, SW Global LLC reserve the authority to block your login details and deny access to SW Global LLC in the future. Removing access to a User may affect access for a User’s relatives or persons who use this service on a User’s behalf, including business enterprise, its affiliates, employees, officers, parents, successors and assigns.
<br/><p class="global_left_headP">Taxes for Customers</p>
  <p class="body_txtp">
 SW Global LLC shall not be responsible for any User or Operator Taxes, which arise as a result of the SW Global LLC usage. User and Operator herein accept to cooperate with all applicable tax laws, consisting the reporting and payment of any taxes, which are related to your SW Global LLC Payment Transactions.
  </p>
   <p class="global_left_headP">Unclaimed Property </p>
  <p class="body_txtp">
  In certain circumstances, due to issues arising from processed Payment Transaction using SW Global LLC, SW Global LLC may hold your funds while an issue is being resolved.
 <br/><br/>
 We will make every effort to contact you using your contact details in our records. However, if we are unable to reach you and/or have no report of your SW Global LLC usage for more than three years, such unclaimed fund will be deemed to be Unclaimed Property by provision of applicable law.
<br/><br/>
We reserve the right to deduct a dormancy fee or other administrative charges from such unclaimed funds, as permitted by applicable law.
<br/>
   </p>
   <p class="global_left_headP">Indemnification</p>
  <p class="body_txtp">
  <strong>You agree to defend, indemnify and hold harmless SW Global LLC, its parent corporation, officers, directors, shareholders, predecessors, successors in interest, employees, agents, subsidiaries and affiliates, harmless from any demands, loss, liability, claims or expenses (including attorneys’ fees), made against SW Global LLC by any third party due to or arising out of or in connection with your use of the Website including but not limited to:</strong>
  <br/><br/>
<ul class="contentul">
<li>Your <span class="body_txtp">SW Global LLC</span> usage.</li>
<li>Non-compliance by you or breach of any part of this Terms of Use or Privacy Policy.</li>
<li>Any legal proceeding resulting from your mistakes or actions or use of the Service.</li>
<li>Your violation or negligence of any law or rights of a third party.</li>
<li>You shall indemnify us in respect of any liability incurred by us for any loss or damage, howsoever caused, suffered by us as a result of your breach of these <span class="body_txtp">SW Global LLC</span> Website Terms of Use, or your use of the <span class="body_txtp">SW Global LLC</span> Service or Website.</li>
<li>This defense and indemnification obligation will survive these Terms of Use and your use of the <span class="body_txtp">SW Global LLC</span> Website.</li>
</ul>
 <br/>
</p>
   <p class="global_left_headP">Disclaimer</p>
  <p class="body_txtp">
  THE SW Global LLC SERVICE, INCLUDING ALL CONTENT, SOFTWARE, FUNCTIONS, MATERIALS, AND INFORMATION MADE AVAILABLE ON, PROVIDED IN CONNECTION WITH OR ACCESSIBLE THROUGH THE SERVICE, IS PROVIDED "AS IS." TO THE FULLEST EXTENT PERMISSIBLE BY LAW, SW Global LLC, AND THEIR SUBSIDIARIES AND OTHER AFFILIATES, AND THEIR AGENTS, CO-BRANDERS OR OTHER PARTNERS (COLLECTIVELY, "SW Global LLC PARTIES"), MAKE NO REPRESENTATION OR WARRANTY OF ANY KIND WHATSOEVER FOR THE SERVICE OR THE CONTENT, MATERIALS, INFORMATION AND FUNCTIONS MADE ACCESSIBLE BY THE SOFTWARE USED ON OR ACCESSED THROUGH THE SERVICE, OR FOR ANY BREACH OF SECURITY ASSOCIATED WITH THE TRANSMISSION OF SENSITIVE INFORMATION THROUGH THE SERVICE. EACH SW Global LLC PARTY DISCLAIMS WITHOUT LIMITATION, ANY WARRANTY OF ANY KIND WITH RESPECT TO THE SERVICE, NONINFRINGEMENT, MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE. THE SW Global LLC PARTIES DO NOT WARRANT THAT THE FUNCTIONS CONTAINED IN THE SERVICE WILL BE UNINTERRUPTED OR ERROR FREE. THE SW Global LLC PARTIES SHALL NOT BE RESPONSIBLE FOR ANY SERVICE INTERRUPTIONS, INCLUDING, BUT NOT LIMITED TO, SYSTEM FAILURES OR OTHER INTERRUPTIONS THAT MAY AFFECT THE RECEIPT, PROCESSING, ACCEPTANCE, COMPLETION OR SETTLEMENT OF PAYMENT TRANSACTIONS OR THE SERVICE.
<br/><br/>
THIS DISCLAIMER OF LIABILITY APPLIES TO ANY DAMAGES OR INJURY CAUSED BY USE OF THIS WEBSITE’S CONTENT AND/OR SERVICES, INCLUDING BUT NOT LIMITED TO: PERFORMANCE FAILURE and/or SERVICE DELAY OR INTERRUPTION COMMUNICATION LINE FAILURE ERRORS and/or OMISSIONS COMPUTER VIRUSES and/or COMPUTER WORMS UNAUTHORIZED ACCESS TO INFORMATION THEFT OF INFORMATION USER EXPRESSLY AGREES THAT ALL RISK OF INJURY RESTS ENTIRELY WITH THE USER. IN NO EVENT WILL SW Global LLC OR ANY PERSON OR ENTITY INVOLVED IN CREATING AND MAINTAINING THIS WEBSITE’S CONTENT AND/OR SERVICES BE LIABLE FOR ANY DAMAGES ARISING OUT OF THE USE OR INABILITY TO USE THIS WEBSITE CONTENT AND/OR SERVICES. THE PROVISIONS OF THIS SECTION SHALL APPLY TO ALL MATERIALS ON THIS WEBSITE. TO THE FULLEST EXTENT PERMITTED BY LAW, SW Global LLC PARTIES DISCLAIM ALL WARRANTIES, EXPRESS OR IMPLIED, IN CONNECTION WITH THE WEBSITE AND YOUR USE THEREOF. SW Global LLC MAKES NO WARRANTIES OR REPRESENTATIONS ABOUT THE ACCURACY OR COMPLETENESS OF THIS SITE'S CONTENT OR THE CONTENT OF ANY SITES LINKED TO THIS SITE AND ASSUMES NO LIABILITY OR RESPONSIBILITY FOR ANY
<ul class="contentul">
<li>ERRORS, MISTAKES, OR INACCURACIES OF CONTENT. </li>
<li>PERSONAL INJURY OR PROPERTY DAMAGE, OF ANY NATURE WHATSOEVER, RESULTING FROM YOUR ACCESS TO AND USE OF OUR WEBSITE. </li>
<li>ANY UNAUTHORIZED ACCESS TO OR USE OF OUR SECURE SERVERS AND/OR ANY AND ALL PERSONAL INFORMATION AND/OR FINANCIAL INFORMATION STORED THEREIN. </li>
<li>ANY INTERRUPTION OR CESSATION OF TRANSMISSION TO OR FROM OUR WEBSITE. </li>
<li>ANY BUGS, VIRUSES, TROJAN HORSES, OR THE LIKE WHICH MAY BE TRANSMITTED TO OR THROUGH OUR WEBSITE BY ANY THIRD PARTY, AND/OR. </li>
<li>ANY ERRORS OR OMISSIONS IN ANY CONTENT OR FOR ANY LOSS OR DAMAGE OF ANY KIND INCURRED AS A RESULT OF THE USE OF ANY CONTENT POSTED, EMAILED, TRANSMITTED, OR OTHERWISE MADE AVAILABLE VIA THE SW Global LLC  WEBSITE. SW Global LLC DOES NOT WARRANT, ENDORSE, GUARANTEE, OR ASSUME RESPONSIBILITY FOR ANY PRODUCT OR SERVICE ADVERTISED OR OFFERED BY A THIRD PARTY THROUGH THE SW Global LLC WEBSITE OR ANY HYPERLINKED WEBSITE OR FEATURED IN ANY BANNER OR OTHER ADVERTISING.
</li>
</ul>
</p>
   <p class="global_left_headP">Limitations of Liability</p>
  <p class="body_txtp">
 Except where prohibited by law, in no event will SW Global LLC be liable to you, or any third party, for any indirect, consequential, exemplary, incidental or punitive damages, including lost profits, even if SW Global LLC has been advised of the possibility of such damages.

<br/><br/>
Subject to any responsibilities implied by law and which cannot be excluded, we, and our directors, employees, agents and contractors, are not liable for any losses, damages, liabilities, claims and expenses (including but not limited to legal costs and defense or settlement costs) whatsoever, whether direct, indirect or consequential, arising out of or referable to Content on the SW Global LLC Website, to Third Party Websites, or to access of the SW Global LLC Website, howsoever caused, whether in contract, tort including negligence, statute or otherwise.
<br/><br/>
Liability of SW Global LLC for a breach of a condition or warranty implied by law or otherwise, and which cannot be excluded, is limited to the extent possible, at SW Global LLC option to (i) the supply of the goods or services again; (ii) the repair of the goods; or the payment of the cost of having the goods or services supplied again or having the goods repaired.


   </p>
   <p class="global_left_headP">Governing Law </p>
  <p class="body_txtp">
  You agree that theSW Global LLC Website shall be deemed a passive website that does not give rise to personal jurisdiction over SW Global LLC, either specific or general, in jurisdictions other than New York. These Terms of Use shall be governed by the internal substantive laws of the State of New York, without respect to its conflict of laws principles. Any claim or dispute between you and SW Global LLC that arises in whole or in part from the SW Global LLC Website shall be decided exclusively by a court of competent jurisdiction located in New York County, New York. These Terms of Use, together with the Privacy Policy and any other legal notices published by SW Global LLC on the Website shall constitute the entire agreement between you and SW Global LLC concerning the SW Global LLC Website. If any provision of these Terms of Use is deemed invalid by a court of competent jurisdiction, the invalidity of such provision shall not affect the validity of the remaining provisions of these Terms of Use, which shall remain in full force and effect. No waiver of any term of this these Terms of Use shall be deemed a further or continuing waiver of such term or any other term, and SW Global LLC failure to assert any right or provision under these Terms of Use shall not constitute a waiver of such right or provision.
<br/><br/>
You agree that all matters relating to your access to or use of the Website, including all disputes, will be governed by the laws of New York State without regard to its conflicts of laws provisions. You agree to the personal jurisdiction by and venue in the state and federal courts of New York, and waive any objection to such jurisdiction or venue. Any claim under these Terms of Use must be brought within one (1) year after the cause of action arises, or such claim or cause of action is barred. No recovery may be sought or received for damages other than out-of-pocket expenses. In the event of any controversy or dispute between SW Global LLC and you arising out of or in connection with your use of the Website, the parties shall attempt, promptly and in good faith, to resolve any such dispute. If we are unable to resolve any such dispute within a reasonable time (not to exceed sixty (60) days), then either party may submit a dispute arising out of or relating to these Terms of Use for arbitration in accordance with the rules and procedures of American Arbitration Association in effect at that time in a proceeding to be conducted in New York and the decision rendered in connection therewith will be binding on the parties hereto, provided, however, that nothing contained in this section shall restrict or limit the right of either party to seek injunctive relief from a court of competent jurisdiction.
</p>
   <p class="global_left_headP">Agreement /Changes to the Terms of Use</p>
  <p class="body_txtp">
  These are the current Terms of Use. They replace any other terms of use for the SW Global LLC Website published on the SW Global LLC Website to date. SW Global LLC may at any time at its sole discretion vary, change, modify, add or remove portions, of this Terms of Use by publishing the varied Terms of Use on the SW Global LLC Website.  SW Global LLC is under no obligation to specifically notify you of any variation to these Terms of Use. You accept that by doing this, SW Global LLC has provided you with sufficient notice of the variation and your continued use of the Website following the posting of changes will mean that you accept and agree to the changes.
<br/><br/>
We further reserve the right to update or amend any portion or all parts of this Terms of Use by posting notices on the SW Global LLC web page or communicating the notice to you using your contact information in our records. These changes will take effect immediately upon update and shall be assumed to be accepted by you after first posting and shall apply on a going-forward basis in relation to all Payment Transactions initiated after the date of posting.
<br/><br/>
As long as you comply with these Terms of Use,SW Global LLC grants you a personal, non-exclusive, non-transferable, limited privilege to enter and use the Website.
<br/>
 </p>
   <p class="global_left_headP">Assignment </p>
  <p class="body_txtp">
  You may not assign this Terms of Use or any rights or obligations hereunder, by operation of law or otherwise, without our prior written approval and any such attempted assignment shall be void. SW Global LLC reserves the right to freely assign this Terms of Use and the rights and obligations hereunder, to any third party without notice to or consent from you, and such assignment shall operate novation and relieve and discharge SW Global LLC from any and all obligations and liability hereunder. Subject to the foregoing, this Terms of Use shall be binding upon and inure to the benefit of the parties hereto, their successors and permitted assigns.
  </p>
   <p class="global_left_headP">Intellectual Property</p>
  <p class="body_txtp">
  This Website and its contents and are protected under international copyright laws. Copyright in all text, graphics, user interfaces, visual interfaces, photographs, trademarks, logos, sounds, music, artwork and computer code (“Content”) on the SW Global LLC Website is owned or licensed by SW Global LLC.
<br/><br/>
<a href="https://www.swgloballlc.com">https://www.swgloballlc.com</a> and its respective logos are trademarks or registered trademarks of SW Global LLC.  All other product names mentioned in this web site are the trademarks or registered trademarks of their respective owners and are mentioned for identification purposes only. Any program, publication, design, product, process, software, technology, information, know-how, or idea described in this Website may be the subject of other rights, including other intellectual property rights, which are owned by SW Global LLC or other interested parties and are not licensed to you hereunder.
<br/><br/>
Users may not modify, publish, transmit, or reproduce any Content obtained from this Website, except as expressly permitted in the Terms of Use. Users acknowledge that they do not acquire any ownership rights of any Content and/or service accessed or obtained from this website. Users shall not upload, exchange, post permitted, reproduce, store in an electronic or other retrieval system, adapt upload to a third party location, framed, perform in public or transmit in any form by any process whatsoever without the specific written consent of SW Global LLC; or otherwise make available any copyright protected material owned by this Website without prior written authorization. Users shall be solely responsible for any damage resulting from the infringement of copyrights. This Website’s Content may not be stored or archived reproduced, stored in an electronic or other retrieval system, adapted, uploaded to a third party location, framed, performed in public or transmitted in any form by any process whatsoever without the specific written consent of SW Global LLC.
</p>
   <p class="global_left_headP">Violation of these Terms of Use</p>
  <p class="body_txtp">
SW Global LLC may preserve any transmittal or communication by You with SW Global LLC through the Website and may disclose any information we have about you (including your identity) if we determine that such disclosure is necessary in connection with any investigation or complaint regarding your use of the Website, to comply with any applicable law, regulation, legal process or governmental request, including exchanging information with other companies and organizations for fraud protection purposes or to identify, contact or bring legal action against someone who may be causing injury to or interference with (either intentionally or unintentionally) SW Global LLC rights or property, or the rights or property of visitors to or users of the Website.


<br/><br/>
You agree that SW Global LLC may, in its sole discretion and without prior notice, terminate your access to the Website and/or block your future access to this Website if we determine that you have violated these Terms of Use or other agreements or guidelines which may be associated with your use of the Website. You also agree that any violation by you of these Terms of Use will constitute an unlawful and unfair business practice, and will cause irreparable harm to SW Global LLC, for which monetary damages would be inadequate, and you consent to SW Global LLC obtaining any injunctive relief that SW Global LLC deems necessary or appropriate in such circumstances. These remedies are in addition to any other remedies SW Global LLC may be entitled to at law or in equity.

<br/><br/>
If SW Global LLC does take any legal action against you as a result of your violation of these Terms of Use, SW Global LLC will be entitled to recover from you, and you agree to pay, all attorneys’ fees and costs of such action, in addition to any other relief granted to SW Global LLC.

  </p>

   <p class="global_left_headP">Miscellaneous</p>
  <p class="body_txtp">
  If any of the provisions of these Terms of Use are held by a court or other tribunal of competent jurisdiction to be void or unenforceable, such provisions shall be limited or eliminated to the minimum extent necessary and replaced with a valid provision that best embodies the intent of these Terms of Use, so that these Terms of Use shall remain in full force and effect. These Terms of Use constitute the entire agreement between you and SW Global LLC with regard to your use of the Website, and any and all other written or oral agreements or understandings previously existing between you and SW Global LLC with respect to such use are hereby superseded and cancelled. SW Global LLC will not accept any counter-offers to these Terms of Use, and all such offers are hereby categorically rejected. SW Global LLC failure to insist on or enforce strict performance of these Terms of Use shall not be construed as a waiver by SW Global LLC of any provision or any right it has to enforce these Terms of Use, nor shall any course of conduct between SW Global LLC and you or any other party be deemed to modify any provision of these Terms of Use. These Terms of Use shall not be interpreted or construed to confer any rights or remedies on any third parties.
  </p>


</div>
</div>