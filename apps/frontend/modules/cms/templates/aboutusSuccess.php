<?php include_partial('global/innerHeading',array('heading'=>'About SW GLOBAL LLC'));?>
<div class="global_content4 clearfix">
<?php echo image_tag('/images/aboutBanner.png',array('alt'=>'')); ?><br/><br/>
<div class="brdBox">
  <p class="body_txtp">
  SW Global LLC is a payment gateway solution from SW Global LLC, USA. SW Global LLC is an e-payment gateway solution that enables NIS to process real time online payment transaction. It is a secure and easier way to pay. You can pay in any way you prefer, including through credit cards without sharing sensitive financial information.
</p>
<p class="body_txtp">
At SW Global LLC, we are committed to helping you meet all your online payment gateway needs. Nothing is more important to us than to ensure that valued customers like you are completely satisfied with our service.
</p>
<p class="body_txtp">
  SW Global LLC already provides services to consulates and embassies across the world for people traveling to African nations.
</p>

</div>
</div>