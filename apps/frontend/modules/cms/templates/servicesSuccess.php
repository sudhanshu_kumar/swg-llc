<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">
<?php include_partial('global/innerHeading', array('heading' => 'Services')); ?>
    <div class="clearfix"></div>
    <div class="inside_banner"><?php echo image_tag('/images/payBanner.gif', array('alt' => '')); ?></div>
    <div class="clearfix"></div>
  <div class="comBox">
    <h2>Our Services include:</h2>
      <p class="body_txtp"> At SW Global, our services include expediting applications for consumers who are interested in obtaining a visa or passports in a fast , simple and secure way.  We help our customers complete the application process in days rather than weeks. </p>
    <h2>Application Fee:</h2>
    <script>
      function openFeeDetails() {
        window.open('<?php echo url_for('misc/nisFeeDetails') ?>', 'feeDetails',
        'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, copyhistory=no, width=1000, height=530');
      }
    </script>
      <p class="body_txtp"> Please <a target="_blank" href="#" class="bluelink" onclick="openFeeDetails();return false;">click here</a> for application fee details. </p>
  </div>
</div>
</div>
<div class="content_wrapper_bottom"></div>
