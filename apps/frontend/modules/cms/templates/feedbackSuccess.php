<!--<div class="global_content4 clearfix">-->
<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">
<?php include_partial('global/innerHeading',array('heading'=>'Feedback'));?>
    <div class="clearfix"></div>
    <div class="inside_banner"><?php echo image_tag('/images/supportBanner.gif',array('alt'=>'')); ?></div>
    <div class="clearfix"></div>
<div class="brdBox">
      <p class="body_txtp"> We thank you for visiting <a href="https://www.swgloballlc.com" class="bluelink">https://www.swgloballlc.com</a>.</p>
      <p class="body_txtp"> Your feedback helps us improve the quality of services. We do review all submissions, though we cannot respond due to volume. We look forward to your contribution in making SW Global LLC a solution that can serve you better. </p>
      <p class="body_txtp"> Kindly email us on <a href="mailto:contact@swgloballlc.com" class="bluelink">contact@swgloballlc.com</a>.</p>
      <p class="body_txtp"> We value your opinion and hope to serve you better. </p>
</div>
</div>
</div>
<div class="content_wrapper_bottom"></div>
<!--</div>-->