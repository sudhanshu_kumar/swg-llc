<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class batchProcessActions extends sfActions {

    public function executeVaultreport(sfWebRequest $request) {
        $this->voltReportForm = new GatewayRequestReportForm();

        $this->formValid = "";
        if ($request->isMethod('post')) {


            $this->voltReportForm->bind($request->getParameter('gatewayRequestReport'));
            if ($this->voltReportForm->isValid()) {
                $this->formValid = "valid";
            }
        }
    }

    public function executeVaultreportSearch(sfWebRequest $request) {
        $this->voltReportForm = new GatewayRequestReportForm();
        $this->voltReportForm->bind($request->getParameter('gatewayRequestReport'));

        $from_date = "";
        $to_date = "";


        $start_date = $request->getParameter('startdate');
        $end_date = $request->getParameter('enddate');
        $maxAmount = $request->getParameter('maxAmount');
        $voltType = $request->getParameter('voltType');
        if ($start_date != "" && $start_date != "BLANK") {
            $dateArray = explode("-", $start_date);
            $from_date = date('Y-m-d H:i:s', strtotime($dateArray[0] . "-" . $dateArray[1] . "-" . $dateArray[2] . " 00:00:00"));
        } else {
            $from_date = "";
        }
        if ($end_date != "" && $end_date != "BLANK") {

            $dateArray = explode("-", $end_date);
            $to_date = date('Y-m-d H:i:s', strtotime($dateArray[0] . "-" . $dateArray[1] . "-" . $dateArray[2] . " 23:59:59"));
        } else {
            $to_date = "";
        }

        if ($this->voltReportForm->isValid()) {
            $postDataArray = $request->getParameterHolder()->getAll();
            if (isset($postDataArray['gatewayRequestReport']))
                $postDataArray = $postDataArray['gatewayRequestReport'];
            //print_r($postDataArray['from_date']);die;
            if ($postDataArray['from_date']) {
                $start_date = $postDataArray['from_date'];
                if ($start_date != "") {
                    $dateArray = explode("-", $start_date);
                    $from_date = date('Y-m-d H:i:s', strtotime($dateArray[0] . "-" . $dateArray[1] . "-" . $dateArray[2] . " 00:00:00"));
                }
            }
            if ($postDataArray['to_date']) {
                $end_date = $postDataArray['to_date'];
                if ($end_date != "") {
                    $dateArray = explode("-", $end_date);
                    $to_date = date('Y-m-d H:i:s', strtotime($dateArray[0] . "-" . $dateArray[1] . "-" . $dateArray[2] . " 23:59:59"));
                }
            }
        }

        $requestQry = Doctrine::getTable('EpPayEasyRequest')->getRequestDetails($from_date, $to_date, $maxAmount, $voltType);



        $this->from_date = $start_date;
        $this->to_date = $end_date;

        $this->page = 1;
        if ($request->hasParameter('page')) {
            $this->page = $request->getParameter('page');
        }

        $this->pager = new sfDoctrinePager('EpPayEasyRequestTable', sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($requestQry);
        $this->pager->setPage($this->page);
        $this->pager->init();
    }
}
?>