<div class="global_content4 clearfix">
<div style="font-weight:bold; padding-top:20px;">We apologise for the inconvenience. The SW Global LLC page you've requested is not available at this time.  There are several possible reasons you are unable to reach the page:</div>
<div class="errorbox">
           
                <ul>
                  <li>If you tried to load a bookmarked page, or followed a link from another Web site, it's possible that the URL you've requested has been moved</li>
                  <li>The web address you entered is incorrect</li>
                  <li>Technical difficulties are preventing us from display of the requested page</li>
                </ul>
				</div>
           
		  <div style="font-size:11px; padding:10px 0 10px 0;"> We want to help you to access what you are looking for</div>
            <div class="errorbox">
			<ul>
                  <li><?php echo link_to('Visit our  Homepage','@homepage') ?></li>
                  <li>Try returning to the page later</li>
                </ul>
          </div>

</div>