<?php

/**
 * GrayPay actions.
 *
 * @package    ama
 * @subpackage graypay
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class graypayActions extends sfActions {

    public static $version = 'V1';

    public function executeOpenInfoBox() {
        $this->setTemplate('openInfoBox');
        $this->setLayout(false);
    }

    public function executeOpenPaymentOptionBox(sfWebRequest $request) {

        $this->requestId = $request->getParameter('requestId');
        $this->setTemplate('openPaymentOptionBox');
        $this->setLayout(false);
    }

    public function executeNew(sfWebRequest $request) { 
        $userId = sfContext::getInstance()->getUser()->getAttribute('user_id', null, 'sfGuardSecurityUser');
        $this->userdetails = $user_detail = Doctrine::getTable('UserDetail')->findByUserId($userId)->getFirst();
        $this->group = $groups = $this->getUser()->getGroupDescription();
        /**
         * [WP: 085] => CR:124
         * Fetching requestId from session variable...
         */
        $requestId = $this->getUser()->getAttribute('requestId'); //$request->getParameter('requestId');
        
        $this->pStatus = false;
        $this->flag = false;
        $ipay4meOrder = Doctrine::getTable('ipay4meOrder')->findByOrderRequestDetailId($requestId);

        ##start - vineet for country zip validaton
        $countryCode = Doctrine::getTable('countryBasedZip')->getAllCountryCode();

        $arrCountry = array();
        for ($i = 0; $i < count($countryCode); $i++) {
            $arrCountry[] = $countryCode[$i]['country_code'];
        }
        $this->arrCountry = $arrCountry;
        ## end - vineet for country zip validaton

        if (count($ipay4meOrder) > 0) {
            $gateWayId = $ipay4meOrder->getFirst()->getGatewayId();
            $this->flag = true;
        }

        if ($requestId) {
            $PaymentStatus = $request->getParameter('paystatus');
            $ruleId = $request->getParameter('rule');
            if ('MV' == $request->getParameter('setmsg')) {
                $hour = 24;
            } else {
                $hour = 1; // Settings::maxcard_time_in_hour();
            }
            if (isset($PaymentStatus) && 'failure' == $PaymentStatus) {
                $this->setErrorMsg($ruleId, $requestId);

                $this->getUser()->setFlash('error', NULL, false);
                $this->pStatus = true;
                $this->flag = false;
            }
            $request_details = Doctrine::getTable('OrderRequestDetails')->find($requestId);

            ## Getting application type...
            $this->application_type = $this->getApplicationType($request_details);


            $orderRequestObj = OrderRequestDetailsTable::getInstance();
            if ($orderRequestObj->getPaymentStatus($request_details->getOrderRequestId())) {
                $this->getUser()->setFlash('notice', 'Payment has already been made for this Item');
                $this->pStatus = true;
                $this->flag = false;
            }
            $this->request_details = $request_details;
            $this->requestId = $requestId;
            //$this->getUser()->getAttributeHolder()->remove('requestId');

            ## start - change by vineet for making ZIP mandatory
            $country = '';
            if (isset($_REQUEST['ep_pay_easy_request']['country'])) {
                $country = $_REQUEST['ep_pay_easy_request']['country'];
            }

            $this->form = new EpPayEasyRequestForm('', array('task' => 'payment', 'argument' => array('country' => $country)));
            ## end - change by vineet for making ZIP mandatory

            $ipReq = (empty($_SERVER['HTTP_X_FORWARDED_FOR']) ? (empty($_SERVER['HTTP_CLIENT_IP']) ? $_SERVER['REMOTE_ADDR'] : $_SERVER['HTTP_CLIENT_IP']) : $_SERVER['HTTP_X_FORWARDED_FOR']);

            $this->form->setDefault('request_ip', $ipReq); // $request->getRemoteAddress()) ;
        } else {
            throw New Exception('Payment page not found!');
        }
    }

    public function executeCreate(sfWebRequest $request) {

        $arrCartItmes = $this->getUser()->getCartItems();
        if (count($arrCartItmes) == 0) {
            $this->getUser()->setFlash('notice', 'Payment has already been made for this Item.');
            $this->redirect('cart/list#msg  ');
        }
        $user_id = sfContext::getInstance()->getUser()->getAttribute('user_id', null, 'sfGuardSecurityUser');
        $this->userdetails = $user_detail = Doctrine::getTable('UserDetail')->findByUserId($user_id)->getFirst();
        $this->group = $groups = $this->getUser()->getGroupDescription();
        $this->pStatus = false;
        $this->flag = false;
        /**
         * [WP: 085] => CR:124
         * Fetching requestId from session variable...
         * Checking requestId exist in session variables if not redirect user to cart page with tempering message...
         */
        $requestId = $this->getUser()->getAttribute('requestId'); //$request->getParameter('requestId');
        Functions::checkRequestIdTempering($requestId);
        
        $this->request_ip = $request->getParameter('request_ip');
        $countryCode = Doctrine::getTable('countryBasedZip')->getAllCountryCode();

        ##start - vineet for country zip validaton
        $arrCountry = array();
        for ($i = 0; $i < count($countryCode); $i++) {
            $arrCountry[] = $countryCode[$i]['country_code'];
        }
        $this->arrCountry = $arrCountry;
        ## end - vineet for country zip validaton

        $request_details = Doctrine::getTable('OrderRequestDetails')->find($requestId);
        $userId = $this->getUser()->getGuardUser()->getId();
        if (!$request_details->getUserId()) {
            $request_details->setUserId($userId);
            $request_details->save();
        }

        ## Getting application type...
        $this->application_type = $this->getApplicationType($request_details);

        ## check if card registered
        $cardNumber = $_REQUEST['ep_pay_easy_request']['card_Num'];
        $cardHash = md5($cardNumber);
        $isRegistered = ApplicantVaultTable::getInstance()->getApprovedCardByHash($cardHash, $userId);
        $orderRequestObj = OrderRequestDetailsTable::getInstance();
        if ($orderRequestObj->getPaymentStatus($request_details->getOrderRequestId())) {
            $this->getUser()->setFlash('notice', 'Payment has already been made for this Item');
            $this->pStatus = true;
        }
        $PaymentStatus = $request->getParameter('paystatus');
        $ruleId = $request->getParameter('rule');
        if ('MV' == $request->getParameter('setmsg')) {
            $hour = 24;
        } else {
            $hour = 1; //Settings::maxcard_time_in_hour();;
        }
        if (isset($PaymentStatus) && 'failure' == $PaymentStatus) {
            $this->setErrorMsg($ruleId, $requestId);
            $this->getUser()->setFlash('error', NULL, false);
            $this->pStatus = true;
            $this->flag = false;
        }
        $this->request_details = $request_details;
        $this->requestId = $requestId;

        ## start - change by vineet for making ZIP mandatory
        $country = '';
        if (isset($_REQUEST['ep_pay_easy_request']['country'])) {
            $country = $_REQUEST['ep_pay_easy_request']['country'];
        }

        $this->form = new EpPayEasyRequestForm('', array('task' => 'payment', 'argument' => array('country' => $country)));
        ## end - change by vineet for making ZIP mandatory

        $this->processForm($request, $this->form);

        $this->setTemplate('new');
    }

    public function RedirectToStatusPage($status, $requestId, $ActionName, $trans_type='payment') {
        switch ($status['status']) {
            case 'success':
                if ('recharge' == $trans_type) {
                    $this->redirect('paymentGateway/thankforRecharge?valid=' . $requestId);
                }
                ## this variable provide time period which runs before page...
                ## if this is 0 that means no break will come before thanks...
                $sec = sfConfig::get('app_payment_success_time_out_limit');
                if($sec == 0){
                    $this->redirect('paymentGateway/thankYou?valid=' . Settings::encryptInput($status['validation']));
                }else{
                    $this->redirect('paymentGateway/paymentConfirmation?valid=' . $status['validation']);
                }                
                break;
            case 'failure':
                if ('recharge' == $trans_type)
                    $this->getUser()->setFlash('error', 'Recharge not done , Please try again.', true);
                else {
                    $errorObj = new ErrorMsg();
                    $errorMessage = $errorObj->displayErrorMessage("E009", $requestId);
                    $this->getUser()->setFlash('error', $errorMessage, true);                    
                }
                $this->redirect('graypay/' . $ActionName . '?paystatus=fail');
                break;
            case 'zip_failure':
                if ('recharge' == $trans_type)
                    $this->getUser()->setFlash('error', 'Recharge not done , Please try again.', true);
                else {
                    $errorObj = new ErrorMsg();
                    $errorMessage = $errorObj->displayErrorMessage("E001", $requestId);
                    $this->getUser()->setFlash('error', $errorMessage, true);                    
                }
                $this->redirect('graypay/' . $ActionName . '?paystatus=fail');
                break;
            case 'surname_failure':
                if ('recharge' == $trans_type)
                    $this->getUser()->setFlash('error', 'Recharge not done , Please try again.', true);
                else {
                    sfContext::getInstance()->getConfiguration()->loadHelpers('Url');
                    $errorObj = new ErrorMsg();
                    $url = url_for('page/printInstruction');
                    $errorMessage = $errorObj->displayErrorMessage("E056", $requestId, array('URL' => ' Kindly <a style="font-size: 15px; font-weight: bold;"href="javascript:;" onclick="instructionPopup(\'' . $url . '\');">click here</a> to read the instruction carefully.'));
                    $this->getUser()->setFlash('error', $errorMessage, true);                    
                }
                $this->redirect('graypay/' . $ActionName . '?paystatus=fail');
                break;
            case 'Already Done':
                $this->redirect('graypay/' . $ActionName);
                break;
        } //swtich
    }

    protected function processForm(sfWebRequest $request, sfForm $form) {
        $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
        if ($form->isValid()) {
            $params = array();
            $params = $request->getPostParameters();           

            ## Removing spaces from array values...
            $params['ep_pay_easy_request'] = Functions::removeWhiteSpacesInArray($params['ep_pay_easy_request']);

            ## Overwriting requestId from session varibale...
            $params['requestId'] = $this->getUser()->getAttribute('requestId');

            $activeGatewayId = $this->getActiveGatewayId($params['ep_pay_easy_request']['card_type']);
            $params['gateway_id'] = $activeGatewayId['gatewayId'];
            ## check is privilege user or not
            ////////////Fraud Prevention //////////////////////
            //            $ip_address = $request->getRemoteAddress();
            $ip_address = empty($_SERVER['HTTP_X_FORWARDED_FOR']) ? (empty($_SERVER['HTTP_CLIENT_IP']) ? $_SERVER['REMOTE_ADDR'] : $_SERVER['HTTP_CLIENT_IP']) : $_SERVER['HTTP_X_FORWARDED_FOR'];
            //Start:code for putting fps rule in lib . by:kuldeep
            //Get Validation for country and MID basis
//            if(!$this->getUser()->isPrivilegeUser()){
                $paymentValidationObj = new PaymentValidationFilter();
                $arrValidationRules = $paymentValidationObj->getCountryMidValidation(strtoupper($activeGatewayId['midService']));
                if (is_array($arrValidationRules) && in_array('NO', $arrValidationRules)) {
                  $fpsRuleId = '';
                } else if (is_array($arrValidationRules) && count($arrValidationRules) == 1 && in_array('SN', $arrValidationRules)) {
                  $fpsRuleId = '';
                } else {

                  //setup param array
                  $arrParams['ip_address'] = $ip_address;
                  $arrParams['card_number'] = $params['ep_pay_easy_request']['card_Num'];
                  $arrParams['gateway_id'] = $params['gateway_id'];
                  $arrParams['requestId'] = $params['requestId'];
                  $arrParams['request_ip'] = $params['ep_pay_easy_request']['request_ip'];
                  $arrParams['email'] = $params['ep_pay_easy_request']['email'];

                  $objValidator = new FpsRuleValidation();
                  $fpsRuleDetail = $objValidator->chkFraudPrevention($arrParams, $arrValidationRules);
                  $fpsId = $fpsRuleDetail['fpsId'];
                  $fpsRuleId = $fpsRuleDetail['ruleId'];
                }
//            }

        $isRegistered = $this->checkCardStatus($params['ep_pay_easy_request']['card_Num'], $this->getUser()->getGuardUser()->getId());
        if ($isRegistered) {
          $this->getUser()->setFlash('notice', 'This card is already registered on this portal. Please choose this card from registered card list for payment.');
          $this->redirect("paymentGateway/gotoApplicantVault");
        }//End of if($isRegistered){...

            if ($fpsRuleId) {
                $this->pStatus = true;
                $this->flag = false;
                $requestId = $params['requestId'];
                $this->setErrorMsg($fpsRuleId, $requestId);
                $this->redirect("paymentGateway/error?ruleId=$fpsRuleId");
            }
            //End:code by kuldeep
            ////////////////////////////////////////////////////
            $notTempered = false;

            ## $paymentModsArr = $this->getPaymentMode();

            $paymentOptionsObj = new paymentOptions();            
            $paymentModsArr = $paymentOptionsObj->getPaymentMode();


            $paymentModsArr = $paymentModsArr['A2'];
            $selectedCardType = $params['ep_pay_easy_request']['card_type'];
            foreach ($paymentModsArr as $key => $val) {

                $isBreak = false;
                for ($i = 0; $i < count($val['card_detail']); $i++) {
                    if ($selectedCardType == $val['card_detail'][$i]['card_type']) {
                        $notTempered = true;
                        $isBreak = true;
                        break;
                    }
                }
                if ($isBreak)
                    break;
            }
            if ($notTempered) {

                ## Start -- Kuldeep -- Getting surname check variable...
                //setting up param array
               // if(!$this->getUser()->isPrivilegeUser()){
                  if (is_array($arrValidationRules) && in_array('SN', $arrValidationRules)) {
                    $arrSurNameParam['first_name'] = $params['ep_pay_easy_request']['first_name'];
                    $arrSurNameParam['last_name'] = $params['ep_pay_easy_request']['last_name'];
                    //setup object of surname validation class
                    $objSurName = new SurNameValidation();
                    $arrReturnSurName = $objSurName->checkForSurNameValidation($arrSurNameParam, $selectedCardType);
                    $isSurNameMatch = $arrReturnSurName['isSurNameMatch'];
                    $isFullNameMatch = $arrReturnSurName['isFullNameMatch'];
                  }else {
                    $isSurNameMatch = true;
                    $isFullNameMatch = true;
                  }
//                }else {
//                  $isSurNameMatch = true;
//                  $isFullNameMatch = true;
//                }
                
                if ($isSurNameMatch && $isFullNameMatch) {
                    $payEasyObj = new PaymentManager();
                    $isBothMatch = true;

                    $host = $request->getUriPrefix();
                    $url_root = $request->getPathInfoPrefix();
                    $sitePath = $host . $url_root;                    

                    $status = $payEasyObj->paymentRequest($params, $params['gateway_id'], $isBothMatch, $activeGatewayId['midService'], $sitePath);
                    //if(!$this->getUser()->isPrivilegeUser()){
                    /**
                     * [WP:086] => CR:125
                     * Adding in_array('CCC', $arrValidationRules) condition...
                     * CCC means credit card country...
                     * Updating CardNotInCountryList table by ordernumber...
                     */
                      if (is_array($arrValidationRules) && !in_array('NO', $arrValidationRules)) {
                          if (in_array('FPS', $arrValidationRules) || in_array('IP', $arrValidationRules) || in_array('BLC', $arrValidationRules) || in_array('BLCA', $arrValidationRules) || in_array('CCT', $arrValidationRules) || in_array('CCC', $arrValidationRules)) {
                              $this->updateFpsDetail($fpsId, $status, $arrValidationRules);
                              PaymentGatewayHelper::updateCreditCardNumberInDb($status['validation']);
                          }
                      }
                    //}
                } else {
                    $status['status'] = 'surname_failure';

                    /**
                     * [WP: 086] => CR:125
                     * Added ruleId 6 while surname failure...
                     */
                    if (is_array($arrValidationRules) && in_array('SN', $arrValidationRules) && count($arrValidationRules) > 1) {
                       $fpsObj = Doctrine::getTable('FpsDetail')->find($fpsId);
                       if(!empty($fpsObj)){
                           $fpsObj->setFpsRuleId(6);
                           $fpsObj->save();
                       }
                    }
                    
                }


                //$this->ProcessBilling($params);

                $this->RedirectToStatusPage($status, $params['requestId'], 'new');
            } else {
                $errorObj = new ErrorMsg();
                $errorMsg = $errorObj->displayErrorMessage("E010", '001000');
                $this->getUser()->setFlash('notice', $errorMsg);
                //        $this->getUser()->setFlash('notice', 'It seems that javascript is disabled on your browser. Please retry after enabling it.');
                $this->redirect('cart/list');
            }
        } else {

            //$this->forward($this->getModuleName(),'create');
        }
    }

    public function updateFpsDetail($fpsId, $status, $arrValidationRules) {
        FpsDetailTable::getInstance()->updateFpsDetailForGraypay($fpsId, $status, $arrValidationRules);
    }

    public function executeError(sfWebRequest $request) {
        $this->show = false;
        $this->requestId = $request->getParameter('requestId');
        $this->isCardRegistrationAllowed = true;
        if ($request->hasParameter('requestId')) {
            $this->show = true;
            $requestId = $request->getParameter('requestId');
            $this->request_details = Doctrine::getTable('OrderRequestDetails')->find($requestId);

            ## Getting application type...
            $this->application_type = $this->getApplicationType($this->request_details);
        }
        if ($request->hasParameter('ruleId') && $request->getParameter('ruleId') == '5') {
            $this->isCardRegistrationAllowed = false;
        }
        $this->isregistered = ($request->hasParameter('registered')) ? $request->getParameter('registered') : 'no';
        $this->setTemplate('error');
    }

    public function executeCvvHelp(sfWebRequest $request) {
        
    }

    public function executeReason(sfWebRequest $request) {
        
    }

    public function executeGetProfile(sfWebRequest $request) {

        $this->setTemplate(NULL);
        $requestId = $request->getParameter('requestId');
        $user_id = sfContext::getInstance()->getUser()->getAttribute('user_id', null, 'sfGuardSecurityUser');
        if (!empty($user_id)) {
            $gatewayObj = Doctrine::getTable('Gateway')->findByName(sfConfig::get('app_active_paymentgateway'));
            $params['gateway_id'] = $gatewayObj->getFirst()->getId();
            $profileDetail = Doctrine::getTable('UserBillingProfile')->getAllAddressProfile($user_id, $requestId, $params['gateway_id']);

            if (!empty($profileDetail[0]['address'])) {
                $arrAdd = explode('~', $profileDetail[0]['address']);


                $profileDetail['0']['address1'] = $arrAdd[0];
                $profileDetail['0']['address2'] = $arrAdd[1];
                $profileDetail['0']['town'] = $arrAdd[2];
                $profileDetail['0']['state'] = $arrAdd[3];
                $profileDetail['0']['zip'] = $arrAdd[4];
                $profileDetail['0']['country'] = $arrAdd[5];
                $profileDetail = $profileDetail['0'];
            }
            $str = "";
            if (!empty($profileDetail)) {
                foreach ($profileDetail as $key => $value) {

                    $str .= $key . "=>" . $value . "##";
                }
            }
        }
        return $this->renderText($str);
    }

    protected function setErrorMsg($ruleId, $requestId) {
        $errorMsg = '';
        $errorMsgObj = new ErrorMsg();
        if (1 == $ruleId) {
            $time = Settings::maxip_time();
            $errorMsg = $errorMsgObj->displayErrorMessage("E011", $requestId, array("time" => $time));
            $this->getUser()->setFlash('notice', $errorMsg);
            //      $this->getUser()->setFlash('notice', ' You have reached the maximum allowed number of transactions form this computer at the current time. Please try again after '.$time);
        }
        if (2 == $ruleId) {
            ## Start here... ## Adding by ashwani...
            $arrSameCardLimit = array();
            $cartValidationObj = new CartValidation();
            $arrCartCapacityLimit = $cartValidationObj->checkUserCartCapacity();
            if (!empty($arrCartCapacityLimit) && count($arrCartCapacityLimit)) {
                $arrSameCardLimit = $arrCartCapacityLimit;
            } ## End here...
            $time = Settings::maxcard_time($arrSameCardLimit['transaction_period']);
            $time = str_replace(".","",$time);
            $errorMsg = $errorMsgObj->displayErrorMessage("E012", $requestId, array("time" => $time));
            $this->getUser()->setFlash('notice', $errorMsg);
            //      $this->getUser()->setFlash('notice', ' You have reached the maximum allowed number of transactions from this card at this time. Please try again after '.$time);
        }
        if (3 == $ruleId) {
            $time = Settings::maxemail_time();
            $errorMsg = $errorMsgObj->displayErrorMessage("E013", $requestId, array("time" => $time));
            $this->getUser()->setFlash('notice', $errorMsg);
            //      $this->getUser()->setFlash('notice', ' Due to anti fraud reasons this transaction can not be processed currently. Please try again after '.$time);
        }
        /**
         * Bug Id: FS#29001 Fix start
         */
        if (4 == $ruleId) {
            $errorMsg = $errorMsgObj->displayErrorMessage("E014", $requestId);
            $this->getUser()->setFlash('notice', $errorMsg);
            //    $this->getUser()->setFlash('notice', ' Your card has been declined by the Processor, please use another card.');
        }
        if (5 == $ruleId) {
            $errorMsg = $errorMsgObj->displayErrorMessage("E008", $requestId);
            $this->getUser()->setFlash('notice', $errorMsg);
            //    $this->getUser()->setFlash('notice', ' This card is black listed by Admin, please use another card.');
        }
        /**
         *
         * FS#29001 Fix Over
         */

        /**
         * [WP: 086] => CR:125
         * Setting error message for RuleId 7...
         * Credit card country does not match...
         */
        if (7 == $ruleId) {
            $errorMsg = $errorMsgObj->displayErrorMessage("E061", $requestId);
            $this->getUser()->setFlash('notice', $errorMsg);
        }

    }

    public function executeAmexVaultCardForm(sfWebRequest $request) {

        /**
         * [WP: 103] => CR: 148
         * Get cart item for checking if applications exists or not...
         */        
        $arrCartItems = $this->getUser()->getCartItems();
        if (count($arrCartItems) == 0) {
            $this->getUser()->setFlash('notice', 'There is no application exists in the cart.');
            $this->redirect('cart/list#msg');
            exit;
        }
         /**
         * [WP: 085] => CR:124
         * Fetching requestId from session...
         */
        $this->requestId = $this->getUser()->getAttribute('requestId'); //$request->getParameter('requestId');        
        $applicantVaultValues = $request->getParameter('applicantVaultValues');        
        $this->request_details = Doctrine::getTable('OrderRequestDetails')->find($this->requestId);

        ## Getting application type...
        $this->application_type = $this->getApplicationType($this->request_details);

        $this->cardType = $applicantVaultValues->getCardType();

        // start - change by vineet for making ZIP mandatory
        $country = '';
        if (isset($_REQUEST['ep_pay_easy_request']['country'])) {
            $country = $_REQUEST['ep_pay_easy_request']['country'];
        }

        //$_SESSION['cardType'] = $this->cardType;
        $request->setAttribute('mode', $this->cardType);

        $this->form = new EpAmexVaultCardForm('', array('task' => 'payment', 'argument' => array('country' => $country)));

        $ipReq = (empty($_SERVER['HTTP_X_FORWARDED_FOR']) ? (empty($_SERVER['HTTP_CLIENT_IP']) ? $_SERVER['REMOTE_ADDR'] : $_SERVER['HTTP_CLIENT_IP']) : $_SERVER['HTTP_X_FORWARDED_FOR']);
        //start - vineet for country zip validaton
        $countryCode = Doctrine::getTable('countryBasedZip')->getAllCountryCode();
        $arrCountry = array();
        for ($i = 0; $i < count($countryCode); $i++) {
            $arrCountry[] = $countryCode[$i]['country_code'];
        }

        $this->arrCountry = $arrCountry;

        //end - vineet for country zip validaton
        $this->form->setDefault('first_name', $applicantVaultValues->getFirstName());
        $this->form->setDefault('last_name', $applicantVaultValues->getLastName());
        $this->form->setDefault('address1', htmlentities(stripslashes($applicantVaultValues->getAddress1())));
        $this->form->setDefault('address2', htmlentities(stripslashes($applicantVaultValues->getAddress2())));
        $this->form->setDefault('town', $applicantVaultValues->getTown());
        $this->form->setDefault('state', $applicantVaultValues->getState());
        $this->form->setDefault('zip', $applicantVaultValues->getZip());
        $this->form->setDefault('country', $applicantVaultValues->getCountry());
        $this->form->setDefault('expiry_date', array("year" => $applicantVaultValues->getExpiryYear(), "month" => $applicantVaultValues->getExpiryMonth()));
        $this->form->setDefault('email', $applicantVaultValues->getEmail());
        $this->form->setDefault('phone', $applicantVaultValues->getPhone());
        $this->form->setDefault('card_holder', $applicantVaultValues->getCardHolder());
        $this->form->setDefault('card_first', $applicantVaultValues->getCardFirst());
        $this->form->setDefault('card_last', $applicantVaultValues->getCardLast());
        $this->cardFirst = $applicantVaultValues->getCardFirst();
        $this->cardLast = $applicantVaultValues->getCardLast();
        $this->midlen = $applicantVaultValues->getCardLen() - (strlen($this->cardFirst) + strlen($this->cardLast));
        $this->form->setDefault('request_ip', $ipReq);
    }

    public function executeCheckCardHash(sfWebRequest $request) {
        $ccnum = $request->getParameter('ccnum');
        $isCardExistInApplicantVault = ApplicantVaultTable::getInstance()->validateCardInApplicantVault($ccnum);
        if ($isCardExistInApplicantVault)
            return $this->renderText('success');
        else
            return $this->renderText('error');
    }

    public function executeAmexVaultCardPay(sfWebRequest $request) {        

        /**
         * [WP: 085] => CR:124
         * Fetching requestId from session variable...
         */
        $this->requestId = $this->getUser()->getAttribute('requestId'); //$request->getParameter('requestId');

        ## Checking URL temporing...
        Functions::checkRequestIdTempering($this->requestId);

        $request_details = Doctrine::getTable('OrderRequestDetails')->find($this->requestId);
        if ($request_details->getPaymentStatus() == 0) {
            $this->redirect('paymentGateway/paymentAlready');
        }


        /**
         * [WP: 103] => CR: 148
         * Get cart item for checking if applications exists or not...
         */
        $arrCartItems = $this->getUser()->getCartItems();
        if (count($arrCartItems) == 0) {
            $this->getUser()->setFlash('notice', 'There is no application exists in the cart.');
            $this->redirect('cart/list#msg');
            exit;
        }

        $this->request_details = $request_details;

        ## Getting application type...
        $this->application_type = $this->getApplicationType($this->request_details);

        $cardType = $_REQUEST['ep_pay_easy_request']['card_type'];
        $request->setAttribute('mode', $cardType);
        // start - change by vineet for making ZIP mandatory
        $country = '';
        if (isset($_REQUEST['ep_pay_easy_request']['country'])) {
            $country = $_REQUEST['ep_pay_easy_request']['country'];
        }
        //start - vineet for country zip validaton
        $countryCode = Doctrine::getTable('countryBasedZip')->getAllCountryCode();
        $arrCountry = array();
        for ($i = 0; $i < count($countryCode); $i++) {
            $arrCountry[] = $countryCode[$i]['country_code'];
        }
        $this->arrCountry = $arrCountry;
        //end - vineet for country zip validaton
        $form = new EpAmexVaultCardForm('', array('task' => 'payment', 'argument' => array('country' => $country)));
        $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
        $this->form = $form;
        if ($form->isValid()) {
            $params = array();
            $params = $request->getPostParameters();

            ## Removing spaces from array values...
            $params['ep_pay_easy_request'] = Functions::removeWhiteSpacesInArray($params['ep_pay_easy_request']);           

            ## Overwriting requestId from session variable...
            $params['requestId'] = $this->requestId;
            
            $activeGatewayId = $this->getActiveGatewayId($params['ep_pay_easy_request']['card_type']);
            $params['gateway_id'] = $activeGatewayId['gatewayId'];

            $ip_address = empty($_SERVER['HTTP_X_FORWARDED_FOR']) ? (empty($_SERVER['HTTP_CLIENT_IP']) ? $_SERVER['REMOTE_ADDR'] : $_SERVER['HTTP_CLIENT_IP']) : $_SERVER['HTTP_X_FORWARDED_FOR'];

            //Get Validation for country and MID basis
             $fpsRuleId = '';
             if(!$this->getUser()->isPrivilegeUser()){
              $paymentValidationObj = new PaymentValidationFilter();
              $arrValidationRules = $paymentValidationObj->getCountryMidValidation($activeGatewayId['midService']);

              //setup param array
              $arrParams['ip_address'] = $ip_address;
              $arrParams['card_number'] = $params['ep_pay_easy_request']['card_Num'];
              $arrParams['gateway_id'] = $params['gateway_id'];
              $arrParams['requestId'] = $params['requestId'];
              $arrParams['request_ip'] = $params['ep_pay_easy_request']['request_ip'];
              $arrParams['email'] = $params['ep_pay_easy_request']['email'];


              /**
               * Commented by ashwani kumar...
               */
              ##if (!is_array($arrValidationRules) && strtoupper($arrValidationRules) == "NO") {
              if (is_array($arrValidationRules) && in_array('NO', $arrValidationRules)) {
                $fpsRuleId = '';
              } else if (is_array($arrValidationRules) && count($arrValidationRules) == 1 && in_array('SN', $arrValidationRules)) {
                $fpsRuleId = '';
              } else {
                $objValidator = new FpsRuleValidation();
                $fpsRuleDetail = $objValidator->chkFraudPrevention($arrParams, $arrValidationRules);
                $fpsId = $fpsRuleDetail['fpsId'];
                $fpsRuleId = $fpsRuleDetail['ruleId'];
              }
            }

            if ($fpsRuleId) {
                $this->pStatus = true;
                $this->flag = false;
                //$requestId = $params['requestId'];
                $this->setErrorMsg($fpsRuleId, $params['requestId']);
                $this->redirect("paymentGateway/error?ruleId=$fpsRuleId");
            }

            $host = $request->getUriPrefix();
            $url_root = $request->getPathInfoPrefix();
            $sitePath = $host . $url_root;

            $payEasyObj = new PaymentManager();
                $status = $payEasyObj->paymentRequest($params, $params['gateway_id'], false, $activeGatewayId['midService'], $sitePath);
            if ($status['status'] == 'success') {
              if(!$this->getUser()->isPrivilegeUser()){
                if (is_array($arrValidationRules) && !in_array('NO', $arrValidationRules)) {
                  if (in_array('FPS', $arrValidationRules) || in_array('IP', $arrValidationRules) || in_array('BLC', $arrValidationRules) || in_array('BLCA', $arrValidationRules) || in_array('CCT', $arrValidationRules) || in_array('CCC', $arrValidationRules)) {
                    $this->updateFpsDetail($fpsId, $status, $arrValidationRules);
                    PaymentGatewayHelper::updateCreditCardNumberInDb($status['validation']);
                  }
                }
              }
              $this->RedirectToStatusPage($status, $params['requestId'], 'new');
            } else {
                //        $this->getUser()->setFlash('notice', 'Your bank has declined the transaction, please contact your bank for further details. Please try another card.', true);
                $errorMsgObj = new ErrorMsg();
                //$requestId = $params['requestId'];
                $errorMsg = $errorMsgObj->displayErrorMessage("E015", $params['requestId']);
                $this->getUser()->setFlash('notice', $errorMsg, true);
                $this->redirect("paymentGateway/registeredCardError");
            }
        } else {
            $this->midlen = $request->getParameter('len');
            $this->setTemplate('amexVaultCardForm');
        }
    }

    public function checkIfAlreadyPaid($appType, $appId) {

        $appIDArray = array();
        switch (strtolower($appType)) {
            case 'passport';
                $appKey = 'passport_id';
                break;
            case 'visa';
                $appKey = 'visa_id';
                break;
            case 'freezone';
                $appKey = 'freezone_id';
                break;
        }

        if (isset($appKey)) {
            $appIDArray[$appKey] = $appId;
            return $cartDetails = CartItemsTransactionsTable::getInstance()->getApplicationDetails($appIDArray);
        } else {
            return;
        }
    }
   
    public function getActiveGatewayId($cardType) {
        if($this->getUser()->isPrivilegeUser()){
          $paymentModeObj = new PaymentModeManager();
          $midService = $paymentModeObj->getPrivilegeUserGatewayId();
        }else{
          $payModeObj = new PaymentModeManager();
          $pcountry = $payModeObj->getFirstProcessingCountry();
          $midService = $payModeObj->getActiveMidService($pcountry, $cardType);
        }
        $midService = strtolower($midService);

        /**
         * Fetching gateway id from mid details table...
         */
        $midDetailsObj = Doctrine::getTable('MidDetails')->findByServiceAndServiceType('graypay', $midService);

        if(count($midDetailsObj)){
            $gatewayId = $midDetailsObj->getFirst()->getGatewayId();
        }else{
                $gatewayId = 1;
                $midService = 'ps1';
        }

//        switch ($midService) {
//            case 'ps1':
//                $gatewayId = 1;
//                break;
//            case 'llc2':
//                $gatewayId = 8;
//                break;
//            default:
//                $gatewayId = 1;
//                $midService = 'ps1';
//                break;
//        }
        return array('midService' => $midService, 'gatewayId' => $gatewayId);
    }

    /** function : checkCardStatus($cardNumber, $userId)
         * @purpose : return true if card is registered with User/ and false
         * @param : ($cardNumber, $userId)
         * @return : true/false
         * @author :
         * @date :
         */
    private function checkCardStatus($cardNumber, $userId) {
      $cardHash = md5($cardNumber);
      $isRegistered = ApplicantVaultTable::getInstance()->getApprovedCardByHash($cardHash, $userId);
      if (count($isRegistered) > 0) {
        return true;
      }
    }

    private function getApplicationType($request_details){
        $appDetails = CartItemsTransactionsTable::getInstance()->getRetryIdDetailsOnNis($request_details->getRetryId());
        if (count($appDetails) == 1) {
            $application_type = strtolower($appDetails[0]['app_type']);
        }else{
            $application_type = '';
        }
        return $application_type;
    }
}
