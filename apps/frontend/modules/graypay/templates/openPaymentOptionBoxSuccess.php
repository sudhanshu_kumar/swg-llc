<?php use_helper('Form');?>
<?php use_helper('Url');?>
<style type="text/css">

.popupBody
{background-color:#ccc;margin:0px auto;padding:15px;font-family:arial;}
.popupContent {height:auto;margin:0px auto;width:423px; *width:100%; font-size:12px;color:#333333;background-color:#fff;padding:10px;font-family:arial;z-index:1000;}
.flLft
{float:left;font-family:arial;}
.flRt
{float:right;font-family:arial; margin-top:15px;}
.greenButton {
background:#6998b0 url(../../images/buttonBg.gif) repeat-x scroll left top;border:1px solid #3e7693;color:#FFFFFF;cursor:pointer;font-family:arial,verdana,helvetica,sans-serif;font-size:12px;height:22px;width:130px;padding:0 6px;text-align:center; font-weight:bold;}
ul.pBold
{
font:bold 11pxArial, Helvetica, sans-serif;list-style-type:none;padding:0px;margin:10px 0 0 0px;
}
ul.fontNor, ul.fontNor li
{font:normal 12px Arial, Helvetica, sans-serif!important;}
ul.pBold li
{
font:bold 11px Arial, Helvetica, sans-serif;list-style-type:none;background:transparent url(../../images/bulletArrow.gif) no-repeat left -1px;padding-left:15px;text-align:left;padding-bottom:10px;
}
H3 {
	color: #3e7693;
	font-weight: bold;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 15px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 3px;
	margin-left: 0px;
	text-transform: capitalize;
	padding: 0px;
}
.clear {
	clear: both;
}
iframe {overflow:hidden;}
</style>

<body id='popupContent'>
<div class="popupBody">
  <div  class="popupContent">
    <h3>Confirmation message</h3>
    <div class="pBold">
        <?php echo htmlspecialchars_decode('Proceed for payment by credit card or ewallet ?');?>

    </div>
    <!--<div class="flLft"><input name="" type="checkbox" value="" id="info_check"/> I have read the Broadcast message</div> -->
    <div align="center">
    <div class="flRt"><input name="" type="button" value="eWallet" class="greenButton" onclick= "parent.window.location = '<?php echo  url_for($this->getModuleName().'/newViaEwallet?requestId='.$requestId) ?>'" />
    &nbsp;
    <input name="" type="button" value="Credit Card Payment" class="greenButton" onClick="parent.emailwindow.hide();" /></div>
    </div>
    <div class="clear"></div>
  </div>
</div>
</body>
</html>
<script>
  //console.log(document.getElementById('popupContent'));
nW = document.getElementById('popupContent').offsetWidth;
nH = document.getElementById('popupContent').offsetHeight;
// console.log(nW+' : '+nH);
window.parent.emailwindow.setSize(nW+20,nH+20);
</script>