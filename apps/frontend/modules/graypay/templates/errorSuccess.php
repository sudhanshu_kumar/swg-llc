<?php include_partial('global/innerHeading',array('heading'=>'Order Status'));?>

<div class="">
  <?php if($show) include_partial('paymentGateway/orderDetail', array('request_details'=>$request_details, 'application_type' => $application_type)); ?>
</div>

<div class="global_content4 clearfix">
  <div class="wrapForm2">
    <?php
    $sf = sfContext::getInstance()->getUser();
    if($sf->hasFlash('notice')){ ?><br />
    <div id="flash_error" class="alertBox" >
      <?php
      echo nl2br($sf->getFlash('notice'));
      ?>
    </div><br />
    <?php
    ## If card type contains Mo then only this link will enable...
    $MoFlag = sfContext::getInstance()->getUser()->getAttribute('MoFlag');
    if($MoFlag){ ?>
    <!--<div class="highlight yellow" id="flash_notice_content" style="margin-right: 0px;margin-left: 0px;"><b><span style="font-size: 15px;"><a href="<?php //echo url_for("mo_configuration/trackingProcess?requestId=".$requestId);?>">CLICK HERE</a> if you would like to attempt this transaction via Money Order.</span></b></div>-->
    <?php } ?>

    
    <?php }?>

    <div class="thanksArea">
      <p class="redBigMessage" >
        <?php if($isCardRegistrationAllowed) {?>Your transaction cannot be processed due to excessive use of card. <?php if($isregistered =='yes'){echo "  ";}else{?> Please <a href='javascript: void(0)' onclick='window.open("<?php echo url_for('userAuth/failurePopUp');?>",
          "windowname1","width=900, height=580")' style="font-size:15px;font-weight:bold">click here</a> to know more about the failure and resolution.
            <?php }}?><h2>
          <center style="font-size:15px;font-weight:bold"><a href="<?php echo url_for('cart/list')?>" style="font-size:15px;font-weight:bold">Click here</a> to see your cart or to use another card for payment.</center>
        </h2>
      </p>
    </div>
  </div>
</div>
