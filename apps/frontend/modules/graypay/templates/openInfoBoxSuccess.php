<?php use_helper('Form');?>
<?php use_helper('Url');?>
<style type="text/css">

.popupBody
{background-color:#ccc;margin:0px auto;padding:10px;font-family:arial;}
.popupContent {height:auto;margin:0px auto;width:423px; width:auto; font-size:12px;color:#333333;background-color:#fff;padding:10px;font-family:arial;z-index:1000;}
.flLft
{float:left;font-family:arial;}
.flRt
{float:right;font-family:arial; margin-top:15px;}
.greenButton {
background:#6998b0 url(../../images/buttonBg.gif) repeat-x scroll left top;border:1px solid #3e7693;color:#FFFFFF;cursor:pointer;font-family:arial,verdana,helvetica,sans-serif;font-size:12px;height:22px;width:135px;padding:0 6px;text-align:center; font-weight:bold;}
ul.pBold
{
font:bold 11pxArial, Helvetica, sans-serif;list-style-type:none;padding:0px;margin:10px 0 0 0px;
}
ul.fontNor, ul.fontNor li
{font:normal 12px Arial, Helvetica, sans-serif!important;}
ul.pBold li
{
font:bold 11px Arial, Helvetica, sans-serif;list-style-type:none;background:transparent url(../../images/bulletArrow.gif) no-repeat left -1px;padding-left:15px;text-align:left;padding-bottom:10px;
}
H3 {
	color: #3e7693;
	font-weight: bold;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 15px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 3px;
	margin-left: 0px;
	text-transform: capitalize;
	padding: 0px;
}
.clear {
	clear: both;
}
iframe {overflow:hidden;}
</style>

<body id='popupContent'>
<div class="popupBody">
  <div  class="popupContent">
    <center><h3>Confirmation message</h3></center>
    <div class="form1">
        <?php
       // echo htmlspecialchars_decode('In iPay4Me, the user cannot make multiple payments using a credit card. However, if the user wants to make multiple payments, the user has an option of eWallet.<br><br>');

        //echo htmlspecialchars_decode('Know your customer (KYC) PIN Generation initiative is the due diligence and bank regulation that financial institutions and other regulated companies must perform    to identify their clients and ascertain relevant information pertinent to doing financial business with them. In this process customer is asked to upload certain documents to ascertain the identity of the customer and thereafter the system generates a PIN for transaction safety and authentication.<br><br>');
        //echo htmlspecialchars_decode('Do you wish to proceed to KYC for eWallet or you want to continue with the payment?');
?><span class="infoBox">
<?php
echo htmlspecialchars_decode('THE SW Global LLC EWALLET IS PROVIDED ONLY TO FACILITATE FASTER PROCESSING OF LARGE NUMBERS OF APPLICATIONS.  IT IS NOT MANDATORY TO USE THIS SERVICE IN ORDER TO PROCESS VISA OR PASSPORT APPLICATIONS.');
?></span><?php
echo htmlspecialchars_decode('<br><br>The SW Global LLC eWallet is provided for customers who process large numbers of transactions daily.<br><br>');

echo htmlspecialchars_decode('Please note that we will be introducing the SW Global LLC ewallet for customers who process large numbers of transactions daily (commercial customers).  This service will be available effective MIDNIGHT GMT Sunday October 24th.<br><br>

The ewallet will have the following features:<br>
<ul>
<li>Ability to load up to $15,000 per month on your ewallet account;</li>
<li>Ability to process unlimited applications at the same time up to the value of the amount loaded;</li>
<li>Dedicated customer service;</li>
</ul>
To use the SW Global LLC ewallet, users must:<br><br>
<ul>
<li>Register (this will include the collection of basic KYC information and ID*).  SMS verification is required.  A verification email or phone call may also be required. SMS charges will be deducted from the customer’s eWallet account;</li>
<li>Ensure that credit cards used for loading the eWallet ALWAYS matches the name and billing address of the registered card holder (also applicable to corporate cards).</li>
</ul>
Fee:  A service charge of 4% per loading or funding of the ewallet (minimum $10 and maximum $50)<br><br>

<b>NOTE:  Due to anti money laundering laws, there are no refunds for deposits into eWallets.  Such funds MUST BE used to pay for Visa’s or Passports on the NIS platform.  Customers are urged to be aware of this and therefore regulate funding of eWallet accounts accordingly.</b><br><br>

"The Know Your Customer (KYC) PIN Generation initiative is the due diligence and bank regulation that financial institutions and other regulated companies must perform to identify their clients and ascertain relevant information pertinent to doing financial business with them. In this process the customer is asked to upload certain documents to ascertain the identity of the customer and thereafter the system generates a PIN for transaction safety and authentication. On authenticating the Pin, an eWallet account is generated for the customer which can be online recharged. Payment via eWallet is then enabled."
</pre>');
?>
    </div>
    <!--<div class="flLft"><input name="" type="checkbox" value="" id="info_check"/> I have read the Broadcast message</div> -->
    <div align="center">
    <div class="flRt"><input name="" type="button" value="Pay using eWallet" class="greenButton" onclick= "parent.window.location = '<?php echo  url_for('ewallet/kyc') ?>'" />
    &nbsp;
    <input name="" type="button" value="Pay using Credit Card" class="greenButton" onClick="parent.emailwindow.hide();" /></div>
    </div>
    <div class="clear"></div>
  </div>
</div>
</body>
</html>
<script>
  //console.log(document.getElementById('popupContent'));
nW = document.getElementById('popupContent').offsetWidth;
nH = document.getElementById('popupContent').offsetHeight;
// console.log(nW+' : '+nH);
window.parent.emailwindow.setSize(nW+20,nH+20);
</script>