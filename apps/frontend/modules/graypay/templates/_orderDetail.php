<table width="100%" cellpadding="0" cellspacing="0">
  <tr>
    <td class="blbar" colspan="3">Order Details -
      <?php //echo $request_details->getMerchant()->getAddress();?>
    </td>
  </tr>
  <tr>
    <td width="77%" class="txtBold">Item</td>
    <td width="23%" class="txtBold txtRight">Price</td>
  </tr>
  <tr>
    <td ><span class="txtBold">
      <?php  echo $request_details->getDescription();?>
      </span></td>
    <td class="txtRight"><?php echo format_amount($request_details->getAmount(),1);?>
    <?php if(isset($application_type)) { ?>
    <?php if($application_type == 'vap'){ ?>
        <?php echo '<i><p style="font-size:10px;text-align:right">(Inclusive service charge: $'.sfConfig::get('app_visa_arrival_additional_charges').')</p></i>'; ?>
        <?php } ?>
    <?php } ?>
    </td>
  </tr>
  <tr class="blTxt">
    <td class="txtBold">&nbsp;</td>
    <td class="txtRight blTxt"><span class="bigTxt">Total: <?php echo format_amount($request_details->getAmount(),1);?></span></td>
  </tr>
</table>
