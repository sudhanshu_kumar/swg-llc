<?php

/**
 *  paymentNotification actions.
 *
 * @package    mysfp
 * @subpackage  order
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12479 2008-10-31 10:54:40Z fabien $
 */
class ewalletActions extends sfActions {

    /**
     * Executes index actionvalidated params
     *
     * @param sfRequest $request A request object
     */
    public function executeRecharge(sfWebRequest $request) {

        $RechargeParam = $request->getParameter('rechargeStatus');
        if ('' == $RechargeParam)
            $this->getUser()->setFlash('notice', NULL);
        $rehargeForm = new RechargeForm();
        $this->rehargeForm = $rehargeForm;
        $postDataArray = $request->getParameterHolder()->getAll();
        if ($request->isMethod('post')) {
            $this->rehargeForm->bind($request->getParameter('recharge'));
            if ($this->rehargeForm->isValid()) {
                if (Settings::getRechargeLimitFlag()) {

                    $this->getContext()->getConfiguration()->loadHelpers(array('EPortal'));
                    $ewalletManagerObj = new EwalletManager();
                    $userid = $this->getUser()->getGuardUser()->getId();
                    $rechargeLimit = $ewalletManagerObj->getRechargeLimit($userid);

                    $totalAmount = Doctrine::getTable('ipay4meRechargeOrder')->getTotalRechargeAmount($rechargeLimit['days']);

                    if ($totalAmount['sum'] >= $rechargeLimit['amount']) {
                        $this->getUser()->setFlash('notice', 'You can not recharge more than ' . format_amount($rechargeLimit['amount'], 1) . ' within ' . $rechargeLimit['days'] . ' days');
                        $this->redirect('ewallet/recharge?rechargeStatus=recharge');
                    }
                }
                $rechargeGateway = Settings::getRechargeGateway();
                if (count($rechargeGateway) > 1)
                    $this->forward($this->getModuleName(), 'selectGateway');
                else
                    $this->redirect($this->getModuleName() . '/confirmDetails?rechageconfirmAmount=' . $postDataArray['recharge']['rechargeAmt']);
                //$this->forward($this->getModuleName(),'confirmDetails');
            }
        }
    }

    public function executeSelectGateway(sfWebRequest $request) {
        $postDataArray = $request->getParameterHolder()->getAll();
        $this->rechargeGateway = Settings::getRechargeGateway();
        $this->rechargeAmount = $postDataArray['recharge']['rechargeAmt'];
    }

    public function executeConfirmDetails(sfWebRequest $request) {
        try {
            $this->selectedGateway = '';
            if ($request->hasParameter('gateway')) {
                $this->selectedGateway = $request->getParameter('gateway');
            } else {
                $rechargeGateway = Settings::getRechargeGateway();
                foreach ($rechargeGateway as $key => $val)
                    $this->selectedGateway = $key;
            }

            $postDataArray = $request->getParameterHolder()->getAll();

            $pinObj = new pin();
            $userObj = $this->getUser()->getGuardUser();
            $this->userDetailObj = $userObj->getUserDetail();
            $user_id = $userObj->getId();
            $this->smsCharge = $pinObj->getTotSmsCharge($user_id);

            $gateway_id = $this->selectedGateway;

            $ewalletManager = new EwalletManager();
            $this->serviceCharge = $ewalletManager->getServiceCharge($gateway_id, 'ewallet_recharge', $postDataArray['rechageconfirmAmount']);
            if ($request->isMethod('post') && $request->getPostParameter('rechageconfirmAmount')) {

                $userId = sfContext::getInstance()->getUser()->getGuardUser()->getId();
                $accountNo = sfContext::getInstance()->getUser()->getGuardUser()->getUserDetail()->getMasterAccountId();

                $rechargeAmount = $postDataArray['rechageconfirmAmount'];
                $totalAmount = $rechargeAmount + $this->serviceCharge + $this->smsCharge;

                $userObjAll = Doctrine::getTable('UserDetail')->findByUserId($userId);
                $userObj = $userObjAll->getFirst();

                $userObj->setSmsCharge($this->smsCharge);

                $rechargeObj = new RechargeOrder();

                $rechargeObj->setUserId($userId);
                $rechargeObj->setGatewayId($gateway_id);
                $rechargeObj->setAccountNo($accountNo);
                $rechargeObj->setAmount($totalAmount);
                $rechargeObj->setItemFee($postDataArray['rechageconfirmAmount']);
                $rechargeObj->setServiceCharge($this->serviceCharge);
                $userObj->save();
                $rechargeObj->save();
                $rechargeOrderId = $rechargeObj->id;

                $getParam = base64_encode($rechargeOrderId);


                if ($gateway_id == Settings::getVbvGatewayId()) {
                    $this->redirect('vbv_configuration/recharge?rechargeRequestId=' . $getParam); //return $this->renderText($getParam);
                } else {
                    $this->redirect('paymentGateway/recharge?requestId=' . $getParam);
                }
            }
            $this->rechargeAmount = $postDataArray['rechageconfirmAmount'];
        } catch (Exception $e) {
            throw New Exception($e->getMessage());
        }
    }

    public function executeKyc(sfWebRequest $request) {
        $userId = sfContext::getInstance()->getUser()->getAttribute('user_id', null, 'sfGuardSecurityUser');
        $user_detail = Doctrine::getTable('UserDetail')->findByUserId($userId);
        $kycStatus = $user_detail->getFirst()->getKycStatus();

        $this->kycStatus = $kycStatus;
        $this->disapproveReason = $user_detail->getFirst()->getDisapproveReason();

        if (0 == $kycStatus && $request->isMethod('post')) {

            $tmpImgName = $request->getPostParameter('tmpImgName');
            $tmpDocName = $request->getPostParameter('tmpDocName');

            $chkDoc = $request->getPostParameter('chkDoc');
            $chkDoc = unserialize(base64_decode($chkDoc));


            $frmtemp = '/temp/';
            $from_path = sfConfig::get('sf_upload_dir') . $frmtemp;

            $path = '/' . $userId;
            $final_path = sfConfig::get('sf_upload_dir') . $path;
            if (!file_exists($final_path))
                mkdir($final_path);
            chmod($final_path, 0755);
            ///////////////////////////// Image Upload Function /////////////////////////////////////////
            if ("on" == $request->getPostParameter('chkImage') && isset($tmpImgName)) {
                $fromImg = $from_path . $tmpImgName;
                $file_name = "img.jpeg";

                $path_to_upload = $final_path . '/' . $file_name;

                $cmd = 'mv ' . $fromImg . ' ' . $path_to_upload;
                exec($cmd, $output, $return_val);
            } else if (isset($tmpImgName)) {
                $fromImg = $from_path . $tmpImgName;
                @unlink($fromImg);
            }

            ////////////////////////////////////////////////////////////////////////////////////////////////
            ////////////////////////////// File Upload Function ////////////////////////////////////////////
            $tmpDocArr = unserialize(base64_decode($tmpDocName));

            for ($i = 1; $i <= 3; ++$i) {
                $chkPar = "chkDoc" . $i;
                $file = "doc" . $i;
                if ("on" == $request->getPostParameter($chkPar) && isset($_FILES[$file])) {

                    $fromDoc = $from_path . $tmpDocArr[$i];
                    $ext = preg_replace('/^.*\.([^.]+)$/D', '$1', $tmpDocArr[$i]);
                    $file_name = "doc" . $i . "." . $ext;

                    $path_to_upload = $final_path . '/' . $file_name;

                    $cmd = 'mv ' . $fromDoc . ' ' . $path_to_upload;
                    exec($cmd, $output, $return_val);
                } else if (isset($tmpDocArr[$i])) {
                    $fromDoc = $from_path . $tmpDocArr[$i];
                    @unlink($fromDoc);
                }
            }

            $user_detail->getFirst()->setKycStatus(2);
            $user_detail->save();
            $this->kycStatus = 2;
            if (Settings::isEwalletPinActive()) {
                $this->redirect('ewallet/verifyMobileNumber');
            }
        }
    }

    public function executeVerifyMobileNumber(sfWebRequest $request) {
        $this->form = new VerifyMobileNumberForm();

        if ($request->isMethod('post')) {
            $this->processVerifyMobileNumber($request, $this->form);
        }
        $this->setTemplate('verifyMobileNumber');
    }

    public function processVerifyMobileNumber(sfWebRequest $request, sfForm $form) {
        $form->bind($request->getParameter('mobile'));

        if ($form->isValid()) {
            $userId = $this->getUser()->getGuardUser()->getUserDetail()->getId();
            $mobileNumber = $request->getPostParameter('mobile[mobile_number]');
            $userDetaills = Doctrine::getTable('userDetail')->find($userId);
            $userDetaills->setMobilePhone($mobileNumber);
            $userDetaills->save();
            $this->redirect('ewallet_pin/pinGeneration');
        }
    }

    public function executeUploadImage(sfWebRequest $request) {
        $this->getContext()->getConfiguration()->loadHelpers('Asset');
        $c = exec('file ' . $_FILES['image']['tmp_name']);
        $data = stripos($c, "JPEG");

        if ($data != "") {

            if (isset($_FILES['image']) && isset($_FILES['image']['tmp_name']) && $_FILES['image']['tmp_name'] != "") {
                $tmpfileName = $_FILES['image']['tmp_name'];
                //                  if ($_FILES['image']['size']/1024 > 20)
                if ($_FILES['image']['size'] / 1024 > 400) {
                    echo "<script>window.parent.document.getElementById('file-info').src='';</script>";
                    echo "<script>window.parent.document.getElementById('image').value=''</script>";
                    echo "<script>window.parent.document.getElementById('file-info').style.display='none'</script>";
                    echo "<script>window.parent.document.getElementById('chkImage').disabled=true</script>";
                    echo "<script>window.parent.document.getElementById('chkImage').checked=false</script>";
                    echo "<script>alert('The uploaded image is bigger than the allowed image size');</script>";
                    die;
                }
                $pin_serial_no = md5(date('Y_m_d_H:i:s'));

                $path = '/temp';
                $final_path = sfConfig::get('sf_upload_dir') . $path;
                if (is_dir($final_path) == '') {
                    mkdir($final_path, 0777, true);
                    chmod($final_path, 0777);
                }
                $file_name = $pin_serial_no . "_temp.jpeg";
                $path_to_upload = $final_path . '/' . $file_name;
                //        $apath =  $request->getUri().'../../../../uploads/'.$path.'/'.$file_name;

                $upload = move_uploaded_file($tmpfileName, $path_to_upload);
                chmod($path_to_upload, 0755);

                $imgPAth = image_path('/uploads/' . $path . '/' . $file_name);
                echo "<script>window.parent.document.getElementById('tmpImgName').value='" . $file_name . "'</script>";
                echo "<script>window.parent.document.getElementById('file-info').src='" . $imgPAth . "?" . rand('111', '9999') . "'</script>";
                echo "<script>window.parent.document.getElementById('file-info').style.display=''</script>";
                echo "<script>window.parent.document.getElementById('chkImage').disabled=false</script>";
                echo "<script>window.parent.document.getElementById('chkImage').checked=false</script>";
                die;
            }
        } else {
            echo "<script>window.parent.document.getElementById('file-info').src='';</script>";
            echo "<script>window.parent.document.getElementById('file-info').style.display='none'</script>";
            echo "<script>window.parent.document.getElementById('chkImage').disabled=true</script>";
            echo "<script>window.parent.document.getElementById('chkImage').checked=false</script>";
            echo "<script>window.parent.document.getElementById('image').value=''</script>";
            echo "<script>alert('The uploaded image should be jpeg image only');</script>";
            die;
        }
    }

    public function executeUploadDoc(sfWebRequest $request) {

        $docNum = $_REQUEST['docNum'];
        $docSize = $_REQUEST['docSize'];
        $docNameVar = $_REQUEST['docName'];

        $docName = 'doc' . $docNum;
        $msg_err = 'errorDoc' . $docNum;
        $chkDoc = 'chkDoc' . $docNum;

        $fileSize = $_FILES[$docName]['size'];
        $fileName = $_FILES[$docName]['name'];

        $c = exec('file ' . $_FILES[$docName]['tmp_name']);
        $data = stripos($c, "JPEG");

        if ($data != "") {

            if (isset($_FILES[$docName]) && isset($_FILES[$docName]['tmp_name']) && $_FILES[$docName]['tmp_name'] != "") {
                $totSize = $docSize + ($_FILES[$docName]['size'] / 1024);
                //                 if($totSize>200)
                if ($totSize > 1024) {
                    echo "<script>alert('You have total 1mb limit to load all document');</script>";
                    echo "<script>window.parent.document.getElementById('" . $chkDoc . "').disabled=true</script>";
                    echo "<script>window.parent.document.getElementById('" . $chkDoc . "').checked=false</script>";
                    echo "<script>window.parent.document.getElementById('$docName').value='';</script>";
                    echo "<script>window.parent.document.getElementById('$msg_err').innerHTML='';</script>";
                    die;
                } else {
                    $tmpfileName = $_FILES[$docName]['tmp_name'];
                    $pin_serial_no = md5(date('Y_m_d_H:i:s'));
                    $ext = preg_replace('/^.*\.([^.]+)$/D', '$1', $fileName);
                    $file_name = $pin_serial_no . $docNum . "." . $ext;

                    if ("" == $docNameVar)
                        $docNameVar = array();
                    else
                        $docArr=unserialize(base64_decode($docNameVar));

                    $docArr[$docNum] = $file_name;
                    $doctemp = base64_encode(serialize($docArr));

                    $path = '/temp';
                    $final_path = sfConfig::get('sf_upload_dir') . $path;
                    if (is_dir($final_path) == '') {
                        mkdir($final_path, 0777, true);
                        chmod($final_path, 0777);
                    }

                    $path_to_upload = $final_path . '/' . $file_name;

                    $upload = move_uploaded_file($tmpfileName, $path_to_upload);
                    chmod($path_to_upload, 0755);


                    echo "<script>window.parent.document.getElementById('totsize').value=$totSize;</script>";
                    echo "<script>window.parent.document.getElementById('tmpDocName').value='" . $doctemp . "'</script>";
                    echo "<script>window.parent.document.getElementById('" . $chkDoc . "').disabled=false</script>";
                    echo "<script>window.parent.document.getElementById('" . $chkDoc . "').checked=false</script>";
                    echo "<script>window.parent.document.getElementById('$msg_err').style.color='blue';</script>";
                    echo "<script>window.parent.document.getElementById('$msg_err').innerHTML='$fileName';</script>";
                    die;
                }
            }
        } else {

            echo "<script>window.parent.document.getElementById('" . $chkDoc . "').disabled=true</script>";
            echo "<script>window.parent.document.getElementById('" . $chkDoc . "').checked=false</script>";
            echo "<script>window.parent.document.getElementById('$docName').value=''</script>";
            echo "<script>window.parent.document.getElementById('$msg_err').innerHTML='';</script>";
            echo "<script>alert('The uploaded document should be jpeg only');</script>";
            die;
        }
    }

    public function executeNewEwalletUser() {

    }

    public function executeNewEwalletUserSearch(sfWebRequest $request) {

        $postDataArray = $request->getParameterHolder()->getAll();

        if ($postDataArray['from_date'] != "" && $postDataArray['from_date'] != "BLANK") {
            $this->fromDate = $postDataArray['from_date'];
        } else {
            $this->fromDate = "";
        }
        if ($postDataArray['to_date'] != "" && $postDataArray['to_date'] != "BLANK") {
            $this->toDate = $postDataArray['to_date'];
        } else {
            $this->toDate = "";
        }
        $userObj = Doctrine::getTable('UserDetail')->getNewEwallet($this->fromDate, $this->toDate);

        $this->page = 1;

        if ($request->hasParameter('page')) {
            $this->page = $request->getParameter('page');
        }

        $this->pager = new sfDoctrinePager('UserDetail', sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($userObj);
        $this->pager->setPage($this->page);
        $this->pager->init();
    }

    public function executeDownloadfile(sfWebRequest $request) {
        $fileName = $request->getParameter('fileName');
        $folder = $request->getParameter('folder');
        $filePath = addslashes(sfConfig::get('sf_upload_dir') . '/' . $folder . '/' . $fileName);

        $this->setLayout(false);
        sfConfig::set('sf_web_debug', false);

        // Adding the file to the Response object
        // $this->getResponse()->setHttpHeader("Content-Length: ",filesize($filePath),TRUE);
        // $this->getResponse()->setHttpHeader('Content-type','application/csv',TRUE);
        $this->getResponse()->setHttpHeader('Content-type', 'application/force-download');
        $this->getResponse()->setHttpHeader('Content-Disposition', "attachment; filename={$fileName}", TRUE);
        $this->getResponse()->sendHttpHeaders();
        $this->getResponse()->setContent(file_get_contents($filePath));
        return sfView::NONE;
    }

    public function executeSetUserStatus(sfWebRequest $request) {
        $userId = $request->getParameter('user_id');
        $getStatus = $request->getParameter('status');
        $reason = $request->getParameter('reason');
        $pinObj = new pin();
        $setStaus = $pinObj->setKycStatus('1', $userId);

        // $setStaus = Doctrine::getTable('UserDetail')->updateEwalletStatus($userId,$status,$reason);
        if ($setStaus) {

            $subject = "eWallet registration status mail";
            $partialName = 'ewalletStatus';
            sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
            $taskId = EpjobsContext::getInstance()->addJob('SendEwalletStatusMail', "ewallet/sendStatusEmail", array('userid' => $userId, 'partialName' => $partialName, 'status' => $status, 'reason' => base64_encode($reason)));
            $this->logMessage("sceduled mail job with id: $taskId", 'debug');
            $this->getUser()->setFlash('notice', sprintf('User approved successfully'));
            $this->forward($this->getModuleName(), 'newEwalletUserSearch');
        }
    }

    public function executeSendStatusEmail(sfWebRequest $request) {
        $this->setLayout(null);
        $userid = $request->getParameter('userid');
        $subject = 'eWallet Secure Certificate Status'; //$request->getParameter('subject');
        $partialName = $request->getParameter('partialName');
        $getStatus = $request->getParameter('status');
        $reason = base64_decode($request->getParameter('reason'));
        $sendMailObj = new Mailing();
        $mailInfo = $sendMailObj->sendStausEmail($userid, $subject, $partialName, $getStatus, $reason);
        return $this->renderText($mailInfo);
    }

    public function executeEwalletUserList(sfWebRequest $request) {
        $this->form = new EwalletUserListForm();
        $getParams = $request->getParameter('ewalletUserList');
        if ($request->isMethod('post')) {
            $this->form->bind($request->getParameter('ewalletUserList'));
            if ($this->form->isValid()) {
                $this->forward($this->getModuleName(), 'ewalletUserListing');
            } else {
                echo $this->form->renderGlobalErrors();
            }
        } else {

            if ($request->getParameter('page'))
                $this->forward($this->getModuleName(), 'ewalletUserListing');
        }
    }

    public function executeEwalletUserListing(sfWebRequest $request) {//die;
        $this->form = new EwalletUserListForm(); //print_r($request->getParameter('ewalletUserList'));die;
        if ($request->isMethod('post'))
            $this->form->bind($request->getParameter('ewalletUserList'));

        if (($request->getParameter('status'))) {
            $status = $request->getParameter('status');
            if ($status == 'activate') {
                $this->chgStatus = 'unblocked';
            } elseif ($status == 'deactivate') {
                $this->chgStatus = 'blocked';
            }
            $this->setStatus($request);
        } else {
            $this->chgStatus = '';
        }

        $uname = '';
        $status_option = '';
        if ($request->getParameter('selectBy') == 'user_type') {
            if ($this->form->getValue('status_option') != 'BLANK') {
                $status_option = $this->form->getValue('status_option');
            } else {
                $status_option = '';
            }

            $uname = '';
        } else if ($request->getParameter('selectBy') == 'user_name') {
            if ($this->form->getValue('uname') != 'BLANK') {
                $uname = $this->form->getValue('uname');
            } else {
                $uname = '';
            }
            $status_option = '';
        }
        // echo $status_option."---".$uname; die;

        $userObj = Doctrine::getTable('sfGuardUser')->getAllEwalletUsers($uname, $status_option);
        $this->maxToBlock = sfConfig::get('app_number_of_failed_attempts_for_blocked_user');

        $this->setTemplate('ewalletUserList');

        $this->page = 1;
        if ($request->hasParameter('page')) {
            $this->page = $request->getParameter('page');
        }

        $this->pager = new sfDoctrinePager('sfGuardUser', sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($userObj);
        $this->pager->setPage($this->page);
        $this->pager->init();
    }

    public function setStatus($request) {
        $userId = $request->getParameter('id');
        $status = $request->getParameter('status');
        $sfUid = $request->getParameter('sfUid');
        if ($userId != '') {

            $user = Doctrine::getTable('UserDetail')->find($userId);
            $sfGuardUserDetail = Doctrine::getTable('sfGuardUser')->find(array($sfUid));
            $applicationArr = array(new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_EWALLETINFO, $sfGuardUserDetail->getUsername(), $sfGuardUserDetail->getId()));

            if ($status == "activate") {

                $user->setFailedAttempt(0);
                $user->save();


                #End Code To Send Mail To The eWallet Owner
                //resetting password
                $pass_word = PasswordHelper::generatePassword();
                $sfGuardUserDetail->setIsActive(false);
                $sfGuardUserDetail->setPassword($pass_word);
                $sfGuardUserDetail->setLastLogin(NULL);
                $sfGuardUserDetail->save();

                #Generate Activation Token For Activation Link
                $hashAlgo = sfConfig::get('app_user_token_hash_algo');
                $userPwdSalt = $sfGuardUserDetail->getSalt();
                $updatedAt = $sfGuardUserDetail->getUpdatedAt();

                $activationToken = Doctrine::getTable('sfGuardUser')->getActivationToken($hashAlgo, $userPwdSalt, $updatedAt);


                $subject = "eWallet Unblock Account Mail";
                $partialName = 'ewallet_status';
                sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
                $activation_url = url_for('signup/activate', true) . '?i=' . $sfUid . '&t=' . $activationToken;
                $taskId = EpjobsContext::getInstance()->addJob('SendEwalletUnblockMail', "signup/sendEmail", array('userid' => $sfUid, 'subject' => $subject, 'partialName' => $partialName, 'activation_url' => $activation_url, 'password' => $pass_word));
                $this->logMessage("sceduled mail job with id: $taskId", 'debug');

                #End Code To Send Mail To The eWallet Owner
                //auditing the event
                $eventHolder = new pay4meAuditEventHolder(
                                EpAuditEvent::$SUBCATEGORY_USER_UNBLOCK,
                                EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_ACCOUNT_UNBLOCK, array('username' => $sfGuardUserDetail->getUsername())),
                                $applicationArr);

                //$user->setFailedAttempt (0);
                //$status_msg = "Activated";
            }
            if ($status == "deactivate") {
                $user->setFailedAttempt(sfConfig::get('app_number_of_failed_attempts_for_blocked_user'));
                $user->save();
                $status_msg = "Deactivated";
                $sfGuardUserDetail->setIsActive(true);
                $sfGuardUserDetail->save();
                $classObj = new sessionAuditManager();
                $classObj->doLogoutForcefully($user->getId());

                //auditing the event
                $eventHolder = new pay4meAuditEventHolder(
                                EpAuditEvent::$SUBCATEGORY_USER_BLOCK,
                                EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_ACCOUNT_BLOCK, array('username' => $sfGuardUserDetail->getUsername())),
                                $applicationArr);
            }

            //$this->getUser()->setFlash('notice', sprintf('User '.$status_msg.' successfully'),TRUE);
            //$this->redirect('report/ewalletUserList');
        }
    }

    public function executeAcctStmtSearch(sfWebRequest $request) {
        $this->eWalletStatementForm = new EWalletStatementForm();

        $this->formValid = "";
        if ($request->isMethod('post')) {


            $this->eWalletStatementForm->bind($request->getParameter('eWalletStatement'));
            if ($this->eWalletStatementForm->isValid()) {
                $this->formValid = "valid";
            }
        }
        $this->checkIsSuperAdmin();
    }

    public function checkIsSuperAdmin() {
        if ($this->getUser() && $this->getUser()->isAuthenticated()) {
            $group_name = $this->getUser()->getGroupNames();
            if (in_array(sfConfig::get('app_pfm_role_admin'), $group_name)) {
                $this->redirect('@adminhome');
            }
        }
    }

    public function executeAccountStatement(sfWebRequest $request) {

        $this->eWalletStatementForm = new EWalletStatementForm();
        $this->eWalletStatementForm->bind($request->getParameter('eWalletStatement'));

        $from_date = "";
        $to_date = "";
        $type = "";

        $start_date = $request->getParameter('startdate');
        $end_date = $request->getParameter('enddate');
        if ($start_date != "" && $start_date != "BLANK") {
            $dateArray = explode("-", $start_date);
            $from_date = date('Y-m-d H:i:s', strtotime($dateArray[0] . "-" . $dateArray[1] . "-" . $dateArray[2] . " 00:00:00"));
        } else {
            $from_date = "";
        }
        if ($end_date != "" && $end_date != "BLANK") {

            $dateArray = explode("-", $end_date);
            $to_date = date('Y-m-d H:i:s', strtotime($dateArray[0] . "-" . $dateArray[1] . "-" . $dateArray[2] . " 23:59:59"));
        } else {
            $to_date = "";
        }

        if ($this->eWalletStatementForm->isValid()) {
            $postDataArray = $request->getParameterHolder()->getAll();
            if (isset($postDataArray['eWalletStatement']))
                $postDataArray = $postDataArray['eWalletStatement'];
            //print_r($postDataArray['from_date']);die;
            if ($postDataArray['from_date']) {
                $start_date = $postDataArray['from_date'];
                if ($start_date != "") {
                    $dateArray = explode("-", $start_date);
                    $from_date = date('Y-m-d H:i:s', strtotime($dateArray[0] . "-" . $dateArray[1] . "-" . $dateArray[2] . " 00:00:00"));

                    //$dateArray = explode("-",$start_date);
                    //$from_date = date('Y-m-d H:i:s',strtotime($start_date['year']."-".$start_date['month']."-".$start_date['day']." 00:00:00"));
                }
            }
            if ($postDataArray['to_date']) {
                $end_date = $postDataArray['to_date'];
                if ($end_date != "") {
                    $dateArray = explode("-", $end_date);
                    $to_date = date('Y-m-d H:i:s', strtotime($dateArray[0] . "-" . $dateArray[1] . "-" . $dateArray[2] . " 23:59:59"));

                    //$dateArray = explode("-",$end_date);
                    //$to_date = date('Y-m-d H:i:s',strtotime($end_date['year']."-".$end_date['month']."-".$end_date['day']." 23:59:59"));
                }
            }

            if ($postDataArray['type']) {
                $type = $postDataArray['type'];
                if ($type == "both") {
                    $type = "";
                }
            }
        }
        $this->from_date = $start_date;
        $this->to_date = $end_date;

        $user_id = $this->getUser()->getGuardUser()->getId();

        $user_detail = Doctrine::getTable('UserDetail')->findByUserId($user_id);

        if ($user_detail->getFirst()->getMasterAccountId()) {
            $ewallet_account_id = $user_detail->getFirst()->getMasterAccountId();
            $masterAcctObj = new EpAccountingManager;
            $this->acctDetails = $masterAcctObj->getAccount($ewallet_account_id);
            $this->statementObj = $masterAcctObj->getStatementDetails($from_date, $to_date, $ewallet_account_id, $type);

            $walletDetailsObj = $masterAcctObj->getTransactionDetails($ewallet_account_id, $from_date, $to_date, $type);

            $this->page = 1;
            if ($request->hasParameter('page')) {
                $this->page = $request->getParameter('page');
            }

            $this->pager = new sfDoctrinePager('EpMasterLedger', sfConfig::get('app_records_per_page'));
            $this->pager->setQuery($walletDetailsObj);
            $this->pager->setPage($this->page);
            $this->pager->init();
        }
    }
}