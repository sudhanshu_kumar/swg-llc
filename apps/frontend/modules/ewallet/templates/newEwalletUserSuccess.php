

<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">
  <?php include_partial('global/innerHeading',array('heading'=>'New eWallet User Section'));?>
<?php use_helper('Form') ?>
  <div class="clear"></div>
    <div class="tmz-spacer"></div>

    <?php echo form_tag('report/indEwalletAccountsSearch',array('name'=>'ewallet_account_form','method'=>'get','id'=>'newewallet_account_form', 'onsubmit' => 'return newewalletAccounts(this);')) ?>


    
        <table width="100%" cellpadding="0" cellspacing="0">
            <tbody>
                <tr>
                    <th><label for="split_from_date">From Date</label></th>
                    <td><input type="text" id="from_date" name="from_date" class="txt-input" onfocus="showCalendarControl(from_date)" readonly="true" maxlength="10"/></td>
                </tr>
                <tr>
                    <th><label for="split_from_date">To Date</label></th>
                    <td><input type="text" id="to_date" name="to_date" class="txt-input" onfocus="showCalendarControl(to_date)" readonly="true" maxlength="10"/></td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <input type="button" onclick="newewalletAccounts()" class="normalbutton" value="Search" name="commit"/>                        
                       
                       
                   </td>
                </tr>
        </tbody></table>
        <br/>
        <div align="center" id="result_div"/>
        </div>


    </form>
</div>



<script>
    function newewalletAccounts(){
        err =0;

        if((document.getElementById('from_date').value != '') && (document.getElementById('to_date').value != '')){
            var start_date = document.getElementById('from_date').value;
            var end_date = document.getElementById('to_date').value;
            start_date = new Date(start_date.split('-')[2],start_date.split('-')[1]-1,start_date.split('-')[0]);
            end_date = new Date(end_date.split('-')[2],end_date.split('-')[1]-1,end_date.split('-')[0]);

            if(start_date.getTime()>end_date.getTime()) {
                alert("<From date> cannot be greater than <To date>");
                document.getElementById('from_date').focus();
                return false;
            }
        }
        /* ================= [ END ] Date Field Validations ====================  */
        if(err == 0) {
            $('#result_div').html('<?php echo image_tag('../images/ajax-loader.gif'); ?>');
            $.post('newEwalletUserSearch',$("#newewallet_account_form").serialize(), function(data){
                if(data=='logout'){
                    location.reload();
                }
                else {
                    $("#result_div").html(data);
                }
            });

        }
    }
</script>
</div>
<div class="content_wrapper_bottom"></div>