<?php include_partial('global/innerHeading',array('heading'=>'Receipt'));?>
<?php
use_helper('Form');
//echo "<br/>";
$sf = sfContext::getInstance()->getUser();
if($sf->hasFlash('notice')){
    ?><div id="flash_notice" class="alertBox" ><?php
    echo nl2br($sf->getFlash('notice'));

    ?></div>
<div id="Reciept"  class="global_content4 clearfix">
        <?php }

    else{
        ?>
        <?php echo form_tag($this->getModuleName().'/'.$this->getActionName(),array('name'=>'rechargeConfirm_form','class'=>'', 'method'=>'post','id'=>'rechargeConfirm_form')) ?>
 <table width="100%" cellpadding="3" cellspacing="0" >
        <tr class="trRow">
            <td align="left" valign="top" colspan="3" ><?php echo _('Confirm Details');?>  </td>
        </tr>

        <tr>
            <?php echo formRowComplete('Name',$userDetailObj->getFirstName()." ".$userDetailObj->getLastName(),'','name','','name_row'); ?>
            <input type="hidden" id="rechageconfirmAmount" name="rechageconfirmAmount" value="<?php echo $rechargeAmount; ?>">
        </tr>
        <tr>
            <?php echo formRowComplete('Recharge Amount',format_amount($rechargeAmount,1),'','name','','name_row'); ?>
        </tr>
        <?php
        if($serviceCharge!=0)
        {
            ?>
        <tr>
            <?php echo formRowComplete('Service Charges',format_amount($serviceCharge,1),'','name','','name_row'); ?>

        </tr>
        <?php
    }
    ?>
    <?php
    if($smsCharge!=0)
    {
        ?>
        <tr>
            <?php echo formRowComplete('Sms Charges ',format_amount($smsCharge,1),'','name','','name_row'); ?>
        </tr>
        <?php
    }
    ?>
        <tr>
            <?php echo formRowComplete('Total Amount',"<b>".format_amount($serviceCharge+$rechargeAmount+$smsCharge,1)."</b>",'','name','','name_row'); ?>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <input type ="hidden" name="gateway" id="gateway" value ="<?php echo $selectedGateway;?>"/>

                &nbsp;
                <input type="button" value="Back" id="Back" name="Back" class="paymentbutton" onclick="history.go(-1)" />
                <input type="submit" name="commit" value="Recharge" id="commit" class="paymentbutton" onclick="return validateSearchForm();">
            </td>
        </tr>

    </table>
    </form>
    <?php } ?>

    <br>
    <div id='error' class='error_list' style="color:red"></div>
    <div align="center" id="loader" class="transparent_class"><div style="padding-top:265px;display:none"><?php echo image_tag('../images/ajax-loader_1.gif'); ?></div></div>
    <div id="iframe">

    </div>

</div>
<script>
    function  validateSearchForm(){
<?php if($selectedGateway=='400'){
    $url = url_for($this->getModuleName().'/'.$this->getActionName());

    $payUrl = url_for('vbv_configuration/VbvRecharge');
    ?>
            var url = "<?php echo $url;?>";
            var payUrl = "<?php echo $payUrl;?>"
//            document.getElementById('commit').style.display = "none";

            $.post(url, $("#rechargeConfirm_form").serialize(), function(data){
            alert(data);

                if(data) {

                    $.post(payUrl, { rechargeRequestId:data }, function(data){
                        if(data=='error'){
                            $('#error').html('Invalid Order Number');

                        } else{
                            //                             alert(data);

                            $('#iframe').html(data);
                        }


                    });
                }

            });




    <?php }else{?>
            document.rechargeConfirm_form.submit();
    <?php }?>
            return false;
        }


</script>


