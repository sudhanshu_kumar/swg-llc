<?php use_helper('Pagination');
use_helper('Form');

include_partial('global/innerHeading',array('heading'=>'eWallet Statement'));?>
<?php use_javascript('jquery.form.js'); ?>
<?php use_javascript('jquery.datePicker.js'); ?>
<?php use_javascript('date.js'); ?>
<?php use_stylesheet('datePicker.css'); ?>
<script type="text/javascript">
function disableForm(frm){

        if(validate_frm(frm)){
            $('#submit').attr('disabled',true);
            return true;
        }else{
            return false;
        }
    }
    
$(document).ready(function() {

    var curr = '<?php echo date('Y/m/d', mktime(0, 0, 0, date("m")  , date("d")+1, date("Y"))); ?>';

    Date.firstDayOfWeek = 0;
    Date.format = 'yyyy/mm/dd';

    $(function()
    {
        $('.date-pick').datePicker(
            {
                startDate: '1900/01/01',
                endDate: curr
            }
        );
    });
 });
</script>
<div class="global_content4 clearfix">
<div class="brdBox">
 <div class="clearfix"/>
<?php //echo form_tag($sf_context->getModuleName().'/display','name=search id=search');?>
<?php echo form_tag('ewallet/acctStmtSearch',array('name'=>'ewallet_report_form','class'=>'', 'method'=>'post','id'=>'ewallet_report_form')) ?>

  <div class="wrapForm2">

            <table width="50%">
                <?php echo $eWalletStatementForm;?>

                <tr>
                    <td>&nbsp;</td>
                    <td><div class="lblButton">
                            <?php   echo submit_tag('Search',array('class' => 'button')); ?>
                        </div>
                        <div class="lblButtonRight">
                            <div class="btnRtCorner"></div>
                    </div></td>
                </tr>
            </table>
            <br />
            <div id="result_div" style="width:500px" align="center"></div>
        </div>


</form>
</div></div>
</div>
<script>
<?php
if("valid" == $formValid )
{
    ?>
        $(document).ready(function()
        {
            validate_search_form();
        });
    <?php
}
?>
    
   function validate_search_form(){
        $('#result_div').html('<?php echo image_tag('../images/ajax-loader.gif'); ?>');
        var err = 0;

        if(err == 0){
            $.post('accountStatement',$("#ewallet_report_form").serialize(), function(data){
                
                if(data == "logout"){
                    location.reload();
                }else{
                    $("#result_div").html(data);
                }
            });
        }


    }

  
</script>
