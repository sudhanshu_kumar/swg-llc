
<?php  //echo ePortal_pagehead(" ",array('class'=>'_form')); ?>
<?php use_helper('Pagination');  ?>

<?php
if($chgStatus != ''){
?>
<div id="flash_notice" class="error_list">
<span>user <?php echo $chgStatus; ?> successfully.</span>
</div>
<?php } ?>

<div class="wrapTable">
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
      <tr class="alternateBgColour">
        <th width="100%" >
          <span class="l">Found <b><?php echo $pager->getNbResults(); ?></b> results matching your criteria.</span>
          <span class="r">Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total  <b><?php echo $pager->getNbResults(); ?></b>  results</span>
        </th>
      </tr>
    </table>
    <br class="pixbr" />
</div>

<div class="wrapTable" style="width:857px;overflow:auto">
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
    <thead>
      <tr class="horizontal">
        <th width="2%" align="center">S.No.</th>
        <th align="center">UserName</th>
        <th align="center">Name</th>
        <th align="center">Email</th>
        <th align="center">Account Number</th>
        <th align="center">Amount</th>
        <th align="center">Block/Unblock</th>
      </tr>
    </thead>
    <tbody>
      <?php

      $username = $sf_request->getParameter('username') == '' ? "BLANK" : $sf_request->getParameter('username');
      $status_option = $sf_request->getParameter('status_option') == '' ? "BLANK" : $sf_request->getParameter('status_option');
      $selectBy = $sf_request->getParameter('selectBy') == '' ? "BLANK" : $sf_request->getParameter('selectBy');
      if(($pager->getNbResults())>0) {
        $limit = sfConfig::get('app_records_per_page');
        $i = max(($page-1),0)*$limit ;
        foreach ($pager->getResults() as $result):
        $i++;
        
        if($result->getUserDetail()->getFirst()->getEpMasterAccount()){
         $keyval = $result->getUserDetail()->getFirst()->getEpMasterAccount();
        }else{
         $keyval = '';
        }
      ?>
      <tr class="alternateBgColour">
        <td align="center"><?php echo $i ?></td>
        <td align="center"><?php echo $result->getUsername(); ?></td>
        <td align="center"><?php echo $result->getUserDetail()->getFirst()->getName(); ?></td>
        <td align="center"><?php echo $result->getUserDetail()->getFirst()->getEmail(); ?></td>
        <td align="center"><?php
        if($keyval != ''){
           echo $keyval['account_number'];
        }else{
            echo "-";
        }
         
        ?></td>
        <td align="center"><?php
         if($keyval != ''){
           echo format_amount($keyval['clear_balance'],NULL,1);
         }else{
           echo "-";
         }
        ?></td>
        <td align="center">

        <?php if($result->getUserDetail()->getFirst()->getFailedAttempt() == $maxToBlock){
          ?>
          <a class="activeInfo" href="#" title="Unblock" id='act' onclick="activateDeactivate(<?php echo $result->getUserDetail()->getFirst()->getId(); ?>,'activate',<?php echo $result->getId(); ?>);"></a>

          <?php
          //echo link_to(' ', 'report/setEwalletBlockUnblock?status=deactivate&id='.$result->getId(), array('method' => 'get',  'class' => 'deactiveInfo', 'title' => 'Deactivate')) ;
        }else{
            ?>
            <a class="deactiveInfo"  title="Block" id='dact' onclick="activateDeactivate(<?php echo $result->getUserDetail()->getFirst()->getId(); ?>,'deactivate',<?php echo $result->getId(); ?>);"> </a>
           <?php
          //echo link_to(' ', 'report/setEwalletBlockUnblock?status=activate&id='.$result->getId(), array('method' => 'get',  'class' => 'activeInfo', 'title' => 'Activate')) ;
        }
        ?>
        </td>

      </tr>
        <?php endforeach; ?>
      <tr>
        <td colspan="7">
          <div class="paging pagingFoot"><?php  echo ajax_pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName().'?username='.$username.'&status_option='.$status_option.'&selectBy='.$selectBy), 'listing_div') ?>
          </div>
        </td>
      </tr>
      <?php }
      else { ?>
        <tr><td  align='center' class='error' colspan="7">No Results found</td></tr>
      <?php } ?>
    </tbody>
  </table>
</div>


<script>
    function activateDeactivate(userId,status,sfUid){

        var  showStatus;
        if(status == 'activate')
          showStatus = 'unblock';

        if(status == 'deactivate')
          showStatus = 'block';


        var answer = confirm("Do you want to "+showStatus+" this user.")
        if (answer){
           var url = "<?php echo url_for('ewallet/ewalletUserListing'); ?>";
           var page = "<?php echo $page; ?>";
           var status_option = "<?php echo $status_option; ?>";
           var selectBy = "<?php echo $selectBy; ?>";
           if(status_option == ''){
           $.post(url, { id: userId, status: status, page: page, sfUid: sfUid, status_option: status_option, selectBy: selectBy },
           function(data){

               //$("#flash_notice").html('<span>Bank '+status+' successfully</span>');
               $("#listing_div").html(data);
               //window.location.href = "URL";
               //setTimeout( "refresh()", 2*1000 );             
           });
          }else{
           $.post(url, { id: userId, status: status, sfUid: sfUid, status_option: status_option,selectBy: selectBy },
           function(data){

               //$("#flash_notice").html('<span>Bank '+status+' successfully</span>');
               $("#listing_div").html(data);
               //window.location.href = "URL";
               //setTimeout( "refresh()", 2*1000 );
           });
          }
        }        
       
    }

   function refresh(){
     window.location.reload( true );
   }

</script>
