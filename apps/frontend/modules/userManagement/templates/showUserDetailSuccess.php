<?php use_helper('Pagination'); ?>
<style>
#1popupContent {
background-color:#bca;
width:100px;
border:1px solid green;
}
</style> 
<div class="brdBox">
  <div class="clearfix"/>
    <div class="clearfix">
      <div id="nxtPage" class="scroll_div">
        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
          <tr>
            <td class="blbar" colspan="13" align="right">
             <div style="float:left">User Details</div>
            <span>Showing <b><?php   
            echo $pager->getFirstIndice();  ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total  <b><?php echo $pager->getNbResults(); ?></b>  results</span></td>
          </tr>
          <tr>
            <td width="8%" ><span class="txtBold">S. No.</span></td>
            <td width="18%"><span class="txtBold">User ID</span></td>
            <td width="18%"><span class="txtBold">User Name</span></td>
            <td width="18%"><span class="txtBold">DOB</span></td>
            <td width="20%"><span class="txtBold">Email</span></td>
            <td width="12%"><span class="txtBold">Phone</span></td>
            <td width="12%"><span class="txtBold">Payment Right</span></td>
            <td width="15%"><span class="txtBold">Account Status</span></td>
            <td width="15%"><span class="txtBold">Account Type</span></td>
            <td width="15%"><span class="txtBold">A/C Creation Date(d/m/y)</span></td>
            <td width="15%"><span class="txtBold">A/C Updation Date(d/m/y)</span></td>
            <td width="15%"><span class="txtBold">Action</span></td>
          </tr>
          <tbody>
            <?php
            $i = $pager->getFirstIndice();

//            $checkedVal = $sf_request->getParameter('checkedVal');
//            $concatUrl = '?checkedVal='.$checkedVal;
//            $concatUrl .= ($sf_request->getParameter('email') != '')? '&email='.$sf_request->getParameter('email') : "";
//            $concatUrl .= ($sf_request->getParameter('fdate') != '')? '&fdate='.$sf_request->getParameter('fdate') : "";
//            $concatUrl .= ($sf_request->getParameter('tdate') != '')? '&tdate='.$sf_request->getParameter('tdate') : "";

            if($pager->getNbResults()>0)
            {
              foreach ($pager->getResults() as $details):
              if($details['payment_rights'] == '0'){
                  $paymentRight = 'Active';
              }else{
                  $paymentRight = 'Blocked';
              }
              if($details['deleted'] == ''){
                  $accountStatus = 'Working';
              }else{
                  $accountStatus = 'Deleted';
              }
              $accountType = 'N/A';
              if(isset($details['sfGuardUser']['username']) && $details['sfGuardUser']['username'] != ''){                                      
                 ## Fetching login type...
                 $accountType = Settings::getLoginType($details['sfGuardUser']['username']);
              }
              $supportUserFlag = 1;
              if(isset($urlArray['host'])){
                  $supportUserFlag = 0;
              }
              $accountCreationDate = (isset($details['created_at'])?date_format(date_create($details['created_at']),'d-m-Y'):'N/A');
              $accountUpdationDate = (isset($details['created_at'])?date_format(date_create($details['updated_at']),'d-m-Y'):'N/A');
             ?>
             <tr>
              <td width="8%"><?php echo $i; ?></td>
              <td width="18%"><span><?=$details['user_id']  ?></span></td>
              <td width="25%"><span><? echo (ucwords($details['first_name']).' '.ucwords($details['last_name']))     ; ?></span></td>
              <td width="20%"><span><? if($details['dob'] != ''){echo date_format(date_create($details['dob']), 'Y-m-d');} else { echo 'N/A';} ?></span></td>
              <td width="20%"><span><? echo $details['email']; ?></span></td>
              <td width="12%"><span><?php if($details['mobile_phone'] != '') {echo $details['mobile_phone'];} else { echo 'N/A'; }?></span></td>
              <td width="12%"><span><?php if(isset($urlArray['host'])){ echo $paymentRight; } else { echo 'N/A';} ?></span></td>
              <td width="12%"><span><?php echo $accountStatus; ?></span></td>
              <td width="12%"><span><?php  echo $accountType; ?></span></td>
              <td width="12%"><span><?php  echo $accountCreationDate; ?></span></td>
              <td width="12%"><span><?php  echo $accountUpdationDate;?></span></td>
              <td width="6%"><a href ="javascript:void(0)" id="edit" onclick="showEditForm('<?php echo $details['user_id'];?>',<?php echo $supportUserFlag;?>)">Edit</a></td>
              

            </tr>
            <?php
            $i++;
            endforeach;

            ?>
            <tr>
              <td class="blbar" colspan="13" height="25px" align="right">
                 <div class="paging pagingFoot"><?php  echo ajax_pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName()), 'nxtPage') ?>
                   </div>
              </td>
            </tr>
            <?php } else { ?>
            <tr><td  align='center' class='error' colspan="13">No Results found</td></tr>
            <?php } ?>
          </tbody>

        </table>
        <br>
      </div>
    </div>
    <div id="popupContent"></div>

<div id="backgroundPopup"></div>
  </div>

  <script>
     function showEditForm(user_id,supportUserFlag){
         var page = '<?php echo $page;?>'
         var url = '<?php echo url_for('userManagement/edit');?>'+"/page/"+page+"/user_id/"+user_id+"/supportUserFlag/"+supportUserFlag;
         emailwindow = dhtmlmodal.open('EmailBox', 'iframe', url ,'Edit User Details', ' width=420,height=380,center=1,border=0, scrolling=no')
     }


  </script>

