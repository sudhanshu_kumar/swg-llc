
<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">
  <?php
use_javascript('dhtmlwindow.js');
use_stylesheet('dhtmlwindow.css');
use_javascript('modal.js');
use_stylesheet('modal.css');
use_helper('Pagination');
use_helper('Form');?>
<?php include_partial('global/innerHeading',array('heading'=>'Search User Details'));?>
<div class="clear"></div>
<div class="tmz-spacer"></div>
<div id="showMsgAfterUpation"></div><br/>
<form action='#' method="post" name="frm_user_detail" id="frm_user_detail" onsubmit="return false;">
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" align="center">
      <tbody>
        <tr>
          <td width="99%" valign="top" colspan="2">
            Please enter any one of below :
          </td>
        </tr>
        <tr>
          <td width="20%">Email address</td>
          <td width="79%" ><?php $email = (isset($email))?$email:"";
          echo input_tag('email', $email, array('size' => 40, 'maxlength' => 50,'class'=>'txt-input')); ?></td>
        </tr>
        <tr>
          <td width="20%">Phone Number<br/>
          <span class="red">Note :Please do not enter (+) sign</span></td>
          <td width="79%" ><?php $phone = (isset($fdate))?$phone:"";
          echo input_tag('phone', $phone, array('size' => 30, 'maxlength' => 20,'class'=>'txt-input')); ?></td>
        </tr>
        <tr>
          <td width="20%">User ID</td>
          <td width="79%" ><?php $userId = (isset($tdate))?$userId:"";
          echo input_tag('userId', $userId, array('size' => 30, 'maxlength' => 15,'class'=>'txt-input')); ?></td>
        </tr>
        <tr>  
        <td>&nbsp;</td>        
          <td >
            <input type="button" name="btn_search" id="btn_search" value="Search" class="normalbutton" onclick="searchUserDetails();">
            <input type ="hidden" name ="page" id ="page">
            </td>
        </tr>

      </tbody>

    </table>
    <!--  This hidden page variable is using to maintain paging ... -->
    <input type="hidden" name="page" id="page" value="" />
  </form>
   <br />
  <div id="result_div"  align="center"></div>
  <div id="entry">
  </div>
</div>
</div>
<div class="content_wrapper_bottom"></div>
<script>

  function searchUserDetails(){
      $('#page').val(1);
      <?php
      ## removing paging...
//      sfContext::getInstance()->getUser()->getAttributeHolder()->remove('page');
      ?>
      checkValidations();
  }


  function checkValidations(pageNo,alertMsg){
   if(alertMsg !=undefined){
       $('#showMsgAfterUpation').html('<b><center><font color ="green">'+alertMsg+'</font></center></b>');
       $('#showMsgAfterUpation').show();
       $('#showMsgAfterUpation').hide(2500);
   }

   if(pageNo == undefined || pageNo == ''){
        var page = $('#page').val();
   }else{
        var page = pageNo;
   }
    var err = 0;
    var email = $.trim($('#email').val());
    var phone = $.trim($('#phone').val());
    var userId = $.trim($('#userId').val());   

  if(email != '' || phone != '' || userId != ''){
    if(email != "")
    {
      var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
      if(reg.test(email) == false) {
        alert('Wrong Email Address');
        err += 1;
      }
    }
   if(phone != ''){
       var reg = /^[0-9 [\]]{4,20}?$/;
       if(reg.test(phone) == false){
           alert('Wrong Phone Number');
           err += 1;
       }
   }
   if(userId != ''){
      if(isNaN(userId)){
          alert('Please enter numeric value in User ID field');
          err += 1;
      }
    }
   }
    if(err == 0){
      var url = "<?php echo url_for('userManagement/showUserDetail'); ?>";
      $('#result_div').html('<?php echo image_tag('../images/ajax-loader.gif'); ?>');
      $("#result_div").show();
      $('#entry').html('');
      $.post(url, {email:email,phone:phone,userId:userId,'page':page},function(data){
        $("#result_div").hide();
        $('#entry').html(data);
        
      });
    }else{
      return false;
    }

  }
</script>