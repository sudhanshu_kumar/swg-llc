<?php use_stylesheet('style.css');
use_javascript('calendar.js');
echo include_stylesheets();
echo include_javascripts(); ?>
<div id="flash_notice" class="alertBox" style="display:none"></div>
<div class="clearfix">
  <div id="nxtPage">
   <?php echo form_tag('userManagement/modify',' class="dlForm multiForm" id="pfm_create_user_form" name="pfm_create_user_form" onSubmit ="return validateForm();"');?>
   <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
      <tr>
          <td width="200px;"><?php echo $form['user_id']->renderLabel(); ?><span class="red"></span></td>
          <td><?php echo $form['user_id']->render(); ?>
              <br>
              <div class="red" id="user_id_error">
                  <?php echo $form['user_id']->renderError(); ?>
              </div>
          </td>
      </tr>
      <tr>
          <td width="200px;"><?php echo $form['first_name']->renderLabel(); ?><span class="red">*</span></td>
          <td><?php echo $form['first_name']->render(); ?>
              <br>
              <div class="red" id="first_name_error">
                  <?php echo $form['first_name']->renderError(); ?>
              </div>
          </td>

      </tr>
      <tr>
          <td ><?php echo $form['last_name']->renderLabel(); ?><span class="red">*</span></td>
          <td colspan="2"><?php echo $form['last_name']->render(); ?>
              <br>
              <div class="red" id="last_name_error">
                  <?php echo $form['last_name']->renderError(); ?>
              </div>

          </td>
      </tr>
      <tr>
          <td ><?php echo $form['dob']->renderLabel(); ?><span class="red"></span></td>
          <td colspan="2"><?php echo $form['dob']->render(); ?>
              <br>
              <div class="red" id="dob_error">
                  <?php echo $form['dob']->renderError(); ?>                  
              </div>

          </td>
      </tr>
      <tr>
          <td><?php echo $form['address']->renderLabel(); if($supportUserFlag == 0){?><span class="red">*</span> <?php } ?></td>
          <td colspan="2"><?php echo $form['address']->render(); ?>
              <br>
              <div class="red" id="address_error">
                  <?php echo $form['address']->renderError(); ?>
              </div>

          </td>
      </tr>
      <tr>
          <td ><?php echo $form['email']->renderLabel(); ?><span class="red">*</span></td>
          <td colspan="2"><?php echo $form['email']->render(); ?>
              <br>
              <div class="red" id="email_error">
                  <?php echo $form['email']->renderError(); ?>
              </div>
          </td>
      </tr>
      <tr>
          <td ><?php echo $form['phone']->renderLabel();if($supportUserFlag == 0){ ?><span class="red">*</span><?php } ?></td>
          <td colspan="2"><?php echo $form['phone']->render(); ?>
              <br>
              <div class="red" id="phone_error">
                  <?php echo $form['phone']->renderError(); ?>
              </div>
          </td>
      </tr><br/>
      <tr>
        <td align="center" colspan="2">
          <div class="divBlock">
            <center>
            <input type = "hidden" name = "supportUserFlag" value="<?php echo $supportUserFlag; ?>">
            <input style="" type="submit" class="loginbutton"  id="multiFormSubmit" value="Save">
            &nbsp;&nbsp;
            <input style="" type="button" class="loginbutton"  id="multiFormSubmit" value="Close" onclick="parent.emailwindow.hide();">
            </center>
          </div>
        </td>
      </tr>
    </table>
    </form>
    </div>
</div>
<script>

    function validateForm(){
        var err = 0;
        if($.trim($('#report_user_first_name').val()) == ''){
            $('#first_name_error').html('Please enter First Name');
            $('#report_user_first_name').focus();
            err += 1;
        }else{
            $('#first_name_error').html('');
        }
        if($.trim($('#report_user_last_name').val()) == ''){
            $('#last_name_error').html('Please enter Last Name');
            $('#report_user_last_name').focus();
            err += 1;
        }else{
            $('#last_name_error').html('');
        }
        <?php if($supportUserFlag == 0) {?>
        if($.trim($('#report_user_address').val()) == ''){
            $('#address_error').html('Please enter Address');
            $('#report_user_address').focus();
            err += 1;
        }else{
            $('#address_error').html('');
        }
        <?php }?>
        if($.trim($('#report_user_email').val()) == ''){
            $('#email_error').html('Please enter Email Address');
            $('#report_user_email').focus();
            err += 1;
        }else{
             $('#email_error').html('');
        }
        <?php if($supportUserFlag == 0) {?>
        if($.trim($('#report_user_phone').val()) == ''){
            $('#phone_error').html('Please enter Phone Number');
            $('#report_user_phone').focus();
            err += 1;
        }else{
            $('#phone_error').html('');
        }
        <?php }?>



        if(err){
            return false
        }else{
            return true;
        }

    }

    </script>


