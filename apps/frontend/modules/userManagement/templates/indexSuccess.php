<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">
<?php use_helper('Pagination');
include_partial('global/innerHeading',array('heading'=>'Report User List'));?>
<div class="clear"></div>
<div class="tmz-spacer"></div>
<?php
$sf = sfContext::getInstance()->getUser();
if($sf->hasFlash('notice')){
  ?>
  <div id="flash_notice" class="alertBox" >
    <?php
  echo nl2br($sf->getFlash('notice'));

  ?>
  </div>
  <?php }?>
<table class="innerTable_content">
 <tr>
      <td class="blbar" colspan="4" align="right"><div class="float_left">Report User List</div>
        <span>Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total <b><?php echo $pager->getNbResults(); ?></b> results</span></td>
    </tr>
    <?php if(($pager->getNbResults())>0) {?>
    <tr>
        <td width="3%" class="txtBold" align="center">S.No.</td>
        <td width="15%" class="txtBold" align="center">User Name</td>
        <td width="10%" class="txtBold" align="center">Email</td>
        <td width="10%" class="txtBold" align="center">Block/Unblock</td>
    </tr>
    <?php
    $i=0;
    foreach ($pager->getResults() as $result):
    $i++;
    ?>
    <tr>
        <td align="center"><?php echo $i;?></td>
        <td align="center"><?php echo $result->getUserName() ?></td>
        <td align="center"><?php echo $result->getUserDetail()->getEmail(); ?></td>
      <td align="center"><?php if($result->getIsActive() == 1){ //echo $result->getIsActive();?>
            <a class="deactiveInfo"  href="#" title="Block" id='act' onclick="activateDeactivate(<?php echo $result->getUserDetail()->getId(); ?>,'deactivate',<?php echo $result->getId(); ?>);"></a>
            <?php }else { echo $result->getIsActive();?>
            <a class="activeInfo" title="Unblock" id='dact' onclick="activateDeactivate('<?php echo $result->getUserDetail()->getId(); ?>','activate','<?php echo $result->getId(); ?>');"> </a>
            <?php }?>
        <?//php echo link_to('Edit ', 'userManagement/edit?id='.$result->getId(), array('method' => 'get', 'class' => 'editInfo', 'title' => 'Edit')) ?>
        </td>
    </tr>
    <?php endforeach; ?>
<tr>
      <td class="blbar" colspan="7" height="25px" align="right"><?php
   echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName()));
        ?>
    </td>
</tr>
<?php }else {?>
    <tr>
      <td  align='center' class='error'><div class="red" >No Report Users Found</div></td>
    </tr>
<?php }?>
</table>
  </div></div><div class="content_wrapper_bottom"></div>

<script>
    function activateDeactivate(userId,status,sfUid){
    
        var  showStatus;
        if(status == 'activate')
            showStatus = 'unblock';

        if(status == 'deactivate')
            showStatus = 'block';


        var answer = confirm("Do you want to "+showStatus+" this user.")
        if (answer){
            var url = "<?php echo url_for('userManagement/setStatus'); ?>";
            var page = "<?php echo $page; ?>";

            if(showStatus != ''){
              
                $.post(url, { id: userId, status: status, page: page, sfUid: sfUid},
                function(data){
                    setTimeout( refresh(showStatus), 10 );
                });
            }
        }

    }
 function refresh(showStatus){    
     window.location.reload( true );
      //$("#flash_notice").html('<span>Report User '+status+' successfully</span>');
   }
</script>
