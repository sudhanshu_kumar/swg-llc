<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">
<?php // if($isPrivilegeUser){ ?>
<?php include_partial('global/innerHeading',array('heading'=>'Instructions'));?>
<?php // } ?>
<?php // if($isPrivilegeUser){ ?>
    <div class="clear"></div>
    <div class="global">
    <div class="comBox">
        <h3><a name="PayOption">Guidelines:</a></h3>
        <a name="PayOption"> </a>
        <p class="padding_15">In order to minimize and/or prevent the fraudulent use of Credit/Debit cards on this site, SW Global LLC DOES NOT allow usage of a single card more than once on our platform. If you intend to use your card multiple times on this site, you may request permission to do so by following the procedure outlined below.<br></p>
        <br/>
        <h3 ><a name="PayOption">How to Request/register</a></h3>
        <p class="body_txtp">
        <ol>
          <li>Fill out the request form on the next page. You are required to produce proof of your name and billing address as associated with your credit card. A scanned copy of your latest credit card statement (clearly indicating address) shall suffice for this purpose.<br>
            <br>
            If your credit card statement carries the full credit card numbers on it, please ensure to hide (by overwriting it with dark black pen) the 8 middle digits of your credit card leaving only first four and last four digits visible. For example, if your credit card number is 1234-5678-5678-9876, please mask  5678-5678 so that it appears as 1234-xxxx-xxxx-9876 in the scanned copy that you upload for our review. </li>
                <li>Submit it for our review and approval. You'll also be sent a notification email confirming your submission.</li>
          <li>Once your request is processed, a notification will be sent to you for confirmation and next steps.</li>
        </ol>
            <br />
        <p class="highlight yellow" align="center">On successful approval you will be able to use your card for multiple payments on SW Global LLC platform.</p>
        </p>
    </div>
</div>
    <div align="center">
      <input type="button" onclick="gotoNext()" value="Continue" class="normalbutton">
</div>
</div>
</div>
<div class="content_wrapper_bottom"></div>
 <?php //} else{?>
    <!--<center><h2>This facility is temporarily Not Available</h2></center> -->
 <?php //} ?>
<script>
  function gotoNext(){
    window.location = '<?php echo url_for('userAuth/newContectDetails'); ?>';
  }
</script>