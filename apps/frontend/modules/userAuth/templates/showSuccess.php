<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">
<?php
use_helper('Pagination');
use_helper('Form');
include_partial('global/innerHeading',array('heading'=>'Approved Credit Card List'));
?>
      <?php
      $sf = sfContext::getInstance()->getUser();
      if($sf->hasFlash('notice')){ ?>
      <div id="flash_notice" class="alertBox" >
        <?php
        echo nl2br($sf->getFlash('notice'));
        ?>
      </div>
      <br/>
      <?php }?>
        <?php
        $sf = sfContext::getInstance()->getUser();
        if($sf->hasFlash('error')){ ?>
        <div id="flash_error" class="alertBox" >
          <?php
          echo nl2br($sf->getFlash('error'));
          ?>
      </div>
      <br/>
        <?php }?>
      <div class="clear">&nbsp;</div>
      <div id="nxtPage">
        <table width="99%" border="0" cellpadding="0" cellspacing="0" class="dataTable" align="center">
          <tr>
            <td class="blbar" colspan="4" align="right"><span>Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total <b><?php echo $pager->getNbResults(); ?></b> results</span></td>
          </tr>
          <tr>
            <td width="5%" ><span class="txtBold">S. No.</span></td>
            <td width="15%"><span class="txtBold">Credit Card number</span></td>
            <td width="10%"><span class="txtBold">Card Holder Name</span></td>
            <td width="10%"><span class="txtBold">Action</span></td>
          </tr>
          <tbody>
            <?php
            $i = $pager->getFirstIndice();

            if($pager->getNbResults()>0)
            {
              foreach ($pager->getResults() as $details):

             $cardLen = $details['card_len'];
             $middlenum = '';
             $cardDisplay = '';
             if($cardLen == 13){
               $middlenum = "-XXXXX-";
             }else if($cardLen == 16){
               $middlenum = "-XXXX-XXXX-";
             }else if($cardLen == 15){
               $middlenum = "-XXXXXXX-";
             }
             $cardDisplay = $details['card_first'].$middlenum.$details['card_last']
              ?>
            <tr>
              <td width="10%"><?php echo $i;?></td>
              <td width="35%"><span>
                <?= "<b>".$cardDisplay."</b>"; ?>
                </span></td>
              <td width="35%"><span>
                <?= ucwords($details['card_holder']) ?>
                </span></td>
              <?php
                $encriptedAppId = SecureQueryString::ENCRYPT_DECRYPT($details['id']);
                $encriptedAppId = SecureQueryString::ENCODE($encriptedAppId);
              ?>
              <td width="20%"><span>
                <?= link_to("Details", url_for("userAuth/authDetails?id=".$encriptedAppId))?>
                </span></td>
            </tr>
            <?php
            $i++;
            endforeach; ?>
            <tr>
              <td class="blbar" colspan="5" height="25px" align="right"><div class="paging pagingFoot">
                  <?php
                  echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName()),'detail_div');
                  ?>
                </div></td>
            </tr>
            <?php } else { ?>
            <tr>
              <td  align='center' class='red' colspan="4">No Results found</td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
        <br>
      </div>
    </div>
  </div>
<div class="content_wrapper_bottom"></div>
