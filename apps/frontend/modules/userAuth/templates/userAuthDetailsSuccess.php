<script>
  function validateForm(){
    var comments = $("#approver_comments").val();

    if(comments==''){
      alert("Please provide comments");
      $("#approver_comments").focus();
      return false;
    }else if(comments.length>255){
      alert("Comments should not more then 255 characters");
      $("#approver_comments").focus();
      return false;
    }

  }
</script>
<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container"> 
     <?php
use_helper('Pagination');
use_helper('Form');
include_partial('global/innerHeading',array('heading'=>"Applicant's Credit Card Details"));
?>
    <div class="clear"></div>
    <div class="tmz-spacer"></div>
        <?php
        $sf = sfContext::getInstance()->getUser();
        if($sf->hasFlash('notice')){ ?>
        <div id="flash_notice" class="alertBox" >
          <?php
          echo nl2br($sf->getFlash('notice'));
          ?>
        </div><br/>
        <?php }?>
        
          <?php
          $sf = sfContext::getInstance()->getUser();
          if($sf->hasFlash('error')){ ?>
          <div id="flash_error" class="alertBox" >
            <?php
            echo nl2br($sf->getFlash('error'));
            ?>
          </div><br/>
          <?php }?>

   


      
        <form action="<?= url_for("supportTool/processAction")?>" method="post" onsubmit="return validateForm();">

          <table border="1" style="width:100%; margin-bottom:10px;">

            <tr>
              <td>
                <table width="100%" style="margin:auto; vertical-align:top;">
                  <tr>
                    <td class="blbar" colspan="2" align="right">      <p>Billing information</p>

                    </td>
                  </tr>


                  <tr>
                    <td> First Name</td>
                    <td><?= $userAuthDetails['first_name'] ?>
                    </td>
                  </tr>
                  <tr>
                    <td>Last Name</td>
                    <td><?php echo $userAuthDetails['last_name'] ?>
                    </td>
                  </tr>

                  <tr>
                    <td>Address 1</td>
                    <td><?php echo nl2br($address1) ?>
                    </td>
                  </tr>

                  <tr>
                    <td>Address 2</td>
                    <td><?php if(isset($address2) && $address2!='') echo $address2; else echo "--"; ?>
                    </td>
                  </tr>

                  <tr>
                    <td>Town</td>
                    <td><?php echo $userAuthDetails['town'] ?>
                    </td>
                  </tr>

                  <tr>
                    <td>State</td>
                    <td><?php if($userAuthDetails['state']!='') echo $userAuthDetails['state']; else echo "--"; ?>
                    </td>
                  </tr>

                  <tr>
                    <td>Zip</td>
                    <td><?php if($userAuthDetails['zip']!='') echo  $userAuthDetails['zip']; else echo "--"; ?>
                    </td>
                  </tr>

                  <tr>
                    <td>Country</td>
                    <td><?= CountryTable::getCountryName($userAuthDetails['country']) ;?>
                    </td>
                  </tr>

                  <tr>
                    <td>Email</td>
                    <td><?php echo $userAuthDetails['email'] ?>
                    </td>
                  </tr>

                  <tr>
                    <td>Phone Number</td>
                    <td><?php echo $userAuthDetails['phone'] ?>
                    </td>
                  </tr>

                </table>
                <br/>
              </td>
              <td style="width:50%; vertical-align:top;">
                <table width="100%" style="margin:auto; margin-bottom:10px;">
                <tr>
                  <td class="blbar" colspan="2" align="right">      <p>Card information</p></td>
                </tr>
                <?php
                switch ($userAuthDetails['card_type']){
                  case "V" :
                    $cardtype = "Visa Card";
                    break;
                  case "A":
                    $cardtype = "American Express";
                    break;

                  case "M":
                    $cardtype = "Master Card";
                    break;
                  default :
                    $cardtype = "-- Not Set --";


                  }


                  ?>
                <tr>
                  <td>Card Type</td>
                  <td><?php echo $cardtype ?>
                  </td>
                </tr>
                <?php


                $cardLen = $userAuthDetails['card_len'];
                $middlenum = '';
                $cardDisplay = '';
                if($cardLen == 13){
                  $middlenum = "-XXXXX-";
                }else if($cardLen == 16){
                  $middlenum = "-XXXX-XXXX-";
                }else if($cardLen == 15){
                  $middlenum = "-XXXXXXX-";
                }
                $cardDisplay = $userAuthDetails['card_first'].$middlenum.$userAuthDetails['card_last']
                ?>
                <tr>
                  <td>Card number</td>
                  <td><?php echo "<b>".$cardDisplay."</b>" ?> <!--<br> <img src="<?php echo image_path('visa_new.gif'); ?>" alt="" class="pd4" />-->
                  </td>
                </tr>

                <tr>
                  <td>Card Holder Name</td>
                  <td><?php echo ucwords($userAuthDetails['card_holder']) ?>
                  </td>
                </tr>
                <?php $month = array('January','February','March','April','May','June','July','August','September','October','November','December') ; ?>
                <tr>
                  <td>Expiry Date</td>
                  <td><?php echo $month[$userAuthDetails['expiry_month']-1]." ".$userAuthDetails['expiry_year'] ?>
                  </td>
                </tr>
              </td>
            </tr>
            <?php if(file_exists(sfConfig::get('sf_upload_dir').'/applicant_profile/'.$userAuthDetails['address_proof']) && $userAuthDetails['address_proof']!=''){ ?>
            <tr>
              <td colspan="2"><a href="#" onclick="javascript:window.open('<?= _compute_public_path($userAuthDetails['address_proof'], 'uploads/applicant_profile', '', true);;?>','MyPage','width=750,height=700,scrollbars=1,resizable=yes');">View uploaded credit card statement</a>

              </td>
            </tr>
            <?php } if(file_exists(sfConfig::get('sf_upload_dir').'/applicant_profile/'.$userAuthDetails['user_id_proof']) && $userAuthDetails['user_id_proof'] != ''){ ?>
                        
            <tr>
              <td colspan="2"><a href="#" onclick="javascript:window.open('<?= _compute_public_path($userAuthDetails['user_id_proof'], 'uploads/applicant_profile', '', true);;?>','MyPage','width=750,height=700,scrollbars=1,resizable=yes');">View uploaded identity proof</a>

              </td>
            </tr>
            <tr>
                  <td>Do you want to do multiple transactions?</td>
                  <td><?php echo "<b>".$userAuthDetails['transaction_type']."</b>" ?>
                  </td>
            </tr>
              <?php }  if($userAuthDetails['reason'] != ''){ ?>
            <tr>
                  <td>Reason</td>
                  <td><?php echo $userAuthDetails['reason'] ?>
              </td>
            </tr>
            <?php } if(file_exists(sfConfig::get('sf_upload_dir').'/applicant_profile/'.$userAuthDetails['agent_proof']) && $userAuthDetails['agent_proof'] != ''){ ?>
                        
            <tr>
              <td colspan="2"><a href="#" onclick="javascript:window.open('<?= _compute_public_path($userAuthDetails['agent_proof'], 'uploads/applicant_profile', '', true);;?>','MyPage','width=750,height=700,scrollbars=1,resizable=yes');">View uploaded proof</a>

              </td>
            </tr>
            <?php }  ?>
               

          </table>
          <tfoot>


            <tr>

              <td colspan="2" colspan="2">


                <div class="pixbr XY20" style="width:350px; margin:auto;">
                  <center id="multiFormNav">

                    <input type="button" value="Back" onclick="javascript:history.go(-1)" class="normalbutton" id="Cancel" name="cancle"/>
                  </center>
                </div>
              </td>
            </tr>
          </tfoot>
        </form>
        </table>
    </div>
</div>
<div class="content_wrapper_bottom"></div>
