<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">
<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>
<?php use_helper('Form');
include_partial('global/innerHeading', array('heading' => 'Register Credit Card')); ?>
<?php
$sf = sfContext::getInstance()->getUser();
if ($sf->hasFlash('notice')) {
?>
    <div id="flash_notice" class="alertBox" >
    <?php
    echo nl2br($sf->getFlash('notice'));
    ?>
</div>
<?php } ?>
    <div class="clear"></div>
    <form  id="frm_billing_info"  name="frm_billing_info" action="<?= url_for("userAuth/saveApplicantVault") ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?> onsubmit="return checkRequestedCardType();" >
        <?php if (!$form->getObject()->isNew()): ?>
            <input type="hidden" name="sf_method" value="put" />
        <?php endif; ?>
      <div class="comBox">
        <h3><a name="PayOption">Guidelines:</a></h3>
        <p class="body_txtp">
        <ol>
                                <li>Fill out the request form below. You are required to produce proof of your name and billing address as associated with your credit card. A scanned copy of your latest credit card statement shall suffice for this purpose. In case your credit card statement displays the full digits of your card number on it, please ensure to hide (by overwriting it with dark black pen) the 8 middle digits of your credit card leaving only the first four and last four digits visible.<br>
                                    <b>For example, if your credit card statement shows your credit card number 1234-5678-5678-9876, please mask  5678-5678 so that it appears as 1234-XXX-XXX-9876 in the scanned copy that you upload for our review.</b></li>
                                <li>Submit it for approval. You'll also be sent a notification email confirming your submission.</li>
                                <li>Once your request is processed, a notification will be sent to you for confirmation and next steps..</li>
                                <li>On successful approval you will be able to use your card for multiple payments on SWGloballlc platform.</li>
                                <li>Please ensure that the phone number you have provided is a valid phone number on which you are reachable, we may contact you on this phone number should we need any more information before approving your account. Failure to provide a valid phone number will result into rejection of your request.</li>
                            </ol>
        <br />
        <p class="highlight yellow">NOTE: We DO NOT store your credit card information on our platform for enhanced security.</p>
        </p>
      </div>
      <div class="clear"></div>
      <table cellpadding="0" cellspacing="0" border="0" width="100%">
                <tr>
        <td width="50%" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0" id="billinfo_id">
                            <tr>
              <td class="tmz-tab-heading" colspan="2" align="right"><p>Billing information</p></td>
                            </tr>
                            <tr>
                                <td colspan="2" align="right"><p class="red">*Required Information </p></td>
                            </tr>
                            <tr>
                                <td colspan="2" align="right"><p class="red">Please enter the exact billing address which is printed on your Credit Card statement</p></td>
                            </tr>
                            <tr>
              <td width="30%"><?php echo $form['first_name']->renderLabel(); ?><span class="red">*</span></td>
                                <td><?php echo $form['first_name']->render(); ?> <br>
                                    <div class="red" id="first_name_error"> <?php echo $form['first_name']->renderError(); ?> </div></td>
                            </tr>
                            <tr>
                                <td><?php echo $form['last_name']->renderLabel(); ?><span class="red">*</span></td>
                                <td><?php echo $form['last_name']->render(); ?> <br>
                                    <div class="red" id="last_name_error"> <?php echo $form['last_name']->renderError(); ?> </div></td>
                            </tr>
                            <tr>
                                <td><?php echo $form['address1']->renderLabel(); ?><span class="red">*</span></td>
                                <td><?php echo $form['address1']->render(); ?> <br>
                                    <div class="red" id="address1_error"> <?php echo $form['address1']->renderError(); ?> </div></td>
                            </tr>
                            <tr>
                                <td><?php echo $form['address2']->renderLabel(); ?></td>
                                <td><?php echo $form['address2']->render(); ?> <br>
                                    <div class="red" id="address2_error"> <?php echo $form['address2']->renderError(); ?> </div></td>
                            </tr>
                            <tr>
                                <td><?php echo $form['town']->renderLabel(); ?><span class="red">*</span></td>
                                <td><?php echo $form['town']->render(); ?> <br>
                                    <div class="red" id="town_error"> <?php echo $form['town']->renderError(); ?> </div></td>
                            </tr>
                            <tr>
                                <td><?php echo $form['state']->renderLabel(); ?></td>
                                <td><?php echo $form['state']->render(); ?> <br>
                                    <div class="red" id="state_error"> <?php echo $form['state']->renderError(); ?> </div></td>
                            </tr>
                            <tr>
                                <td><?php echo $form['zip']->renderLabel(); ?></td>
                                <td><?php echo $form['zip']->render(); ?> <br>
                                    <div class="red" id="zip_error"> <?php echo $form['zip']->renderError(); ?> </div></td>
                            </tr>
                            <tr><!-- CR 110 -->
                                <td><?php echo $form['country']->renderLabel(); ?><span class="red">*</span></td>
                                <td><select id="applicant_vault_country" name="applicant_vault[country]">
                                        <option ringto="" value="">Please Select Country</option>
                                        <option ringto="+93-" value="AF">Afghanistan</option>
                                        <option ringto="+355-" value="AL">Albania</option>
                                        <option ringto="+213-" value="DZ">Algeria</option>
                                        <option ringto="+1684-" value="AS">American Samoa</option>
                                        <option ringto="+376-" value="AD">Andorra</option>
                                        <option ringto="+244-" value="AO">Angola</option>
                                        <option ringto="+1264-" value="AI">Anguilla</option>
                                        <option ringto="+672-" value="AQ">Antarctica</option>
                                        <option ringto="+1268-" value="AG">Antigua &amp; Barbuda</option>
                                        <option ringto="+54-" value="AR">Argentina</option>
                                        <option ringto="+374-" value="AM">Armenia</option>
                                        <option ringto="+297-" value="AW">Aruba</option>
                                        <option ringto="+61-" value="AU">Australia</option>
                                        <option ringto="+43-" value="AT">Austria</option>
                                        <option ringto="+994-" value="AZ">Azerbaijan</option>
                                        <option ringto="+1242-" value="BS">Bahamas</option>
                                        <option ringto="+973-" value="BH">Bahrain</option>
                                        <option ringto="+880-" value="BD">Bangladesh</option>
                                        <option ringto="+1246-" value="BB">Barbados</option>
                                        <option ringto="+375-" value="BY">Belarus</option>
                                        <option ringto="+32-" value="BE">Belgium</option>
                                        <option ringto="+501-" value="BZ">Belize</option>
                                        <option ringto="+229-" value="BJ">Benin</option>
                                        <option ringto="+1441-" value="BM">Bermuda</option>
                                        <option ringto="+975-" value="BT">Bhutan</option>
                                        <option ringto="+591-" value="BO">Bolivia</option>
                                        <option ringto="+387-" value="BA">Bosnia/Herzegovina</option>
                                        <option ringto="+267-" value="BW">Botswana</option>
                                        <option ringto="+55-" value="BR">Brazil</option>
                                        <option ringto="+1284-" value="VG">British Virgin Islands</option>
                                        <option ringto="+673-" value="BN">Brunei</option>
                                        <option ringto="+359-" value="BG">Bulgaria</option>
                                        <option ringto="+226-" value="BF">Burkina Faso</option>
                                        <option ringto="+257-" value="BI">Burundi</option>
                                        <option ringto="+855-" value="KH">Cambodia</option>
                                        <option ringto="+237-" value="CM">Cameroon</option>
                                        <option ringto="1-" value="CA">Canada</option>
                                        <option ringto="+238-" value="CV">Cape Verde Islands</option>
                                        <option ringto="+1345-" value="KY">Cayman Islands</option>
                                        <option ringto="+236-" value="CF">Central African Republic</option>
                                        <option ringto="+235-" value="TD">Chad Republic</option>
                                        <option ringto="+56-" value="CL">Chile</option>
                                        <option ringto="+86-" value="CN">China</option>
                                        <option ringto="+6724-" value="CX">Christmas Island</option>
                                        <option ringto="+6722-" value="CC">Cocos Keeling Island</option>
                                        <option ringto="+57-" value="CO">Colombia</option>
                                        <option ringto="+269-" value="KM">Comoros</option>
                                        <option ringto="+243-" value="CD">Congo Democratic Republic</option>
                                        <option ringto="+242-" value="CG">Congo, Republic of</option>
                                        <option ringto="+682-" value="CK">Cook Islands</option>
                                        <option ringto="+506-" value="CR">Costa Rica</option>
                                        <option ringto="+225-" value="CI">Cote D'Ivoire</option>
                                        <option ringto="+385-" value="HR">Croatia</option>
                                        <option ringto="+53-" value="CU">Cuba</option>
                                        <option ringto="+357-" value="CY">Cyprus</option>
                                        <option ringto="+420-" value="CZ">Czech Republic</option>
                                        <option ringto="+45-" value="DK">Denmark</option>
                                        <option ringto="+253-" value="DJ">Djibouti</option>
                                        <option ringto="+1767-" value="DM">Dominica</option>
                                        <option ringto="+1809, 1829-" value="DO">Dominican Republic</option>
                                        <option ringto="+593-" value="EC">Ecuador</option>
                                        <option ringto="+20-" value="EG">Egypt</option>
                                        <option ringto="+503-" value="SV">El Salvador</option>
                                        <option ringto="+240-" value="GQ">Equatorial Guinea</option>
                                        <option ringto="+291-" value="ER">Eritrea</option>
                                        <option ringto="+372-" value="EE">Estonia</option>
                                        <option ringto="+251-" value="ET">Ethiopia</option>
                                        <option ringto="+500-" value="FK">Falkland Islands</option>
                                        <option ringto="+298-" value="FO">Faroe Island</option>
                                        <option ringto="+679-" value="FJ">Fiji Islands</option>
                                        <option ringto="+358-" value="FI">Finland</option>
                                        <option ringto="+33-" value="FR">France</option>
                                        <option ringto="+596-" value="TF">French Antilles/Martinique</option>
                                        <option ringto="+594-" value="GF">French Guiana</option>
                                        <option ringto="+689-" value="PF">French Polynesia</option>
                                        <option ringto="+241-" value="GA">Gabon Republic</option>
                                        <option ringto="+220-" value="GM">Gambia</option>
                                        <option ringto="+995-" value="GE">Georgia</option>
                                        <option ringto="+49-" value="DE">Germany</option>
                                        <option ringto="+233-" value="GH">Ghana</option>
                                        <option ringto="+350-" value="GI">Gibraltar</option>
                                        <option ringto="+30-" value="GR">Greece</option>
                                        <option ringto="+299-" value="GL">Greenland</option>
                                        <option ringto="+1473-" value="GD">Grenada</option>
                                        <option ringto="+590-" value="GP">Guadeloupe</option>
                                        <option ringto="+1671-" value="GU">Guam</option>
                                        <option ringto="+502-" value="GT">Guatemala</option>
                                        <option ringto="+224-" value="GN">Guinea Republic</option>
                                        <option ringto="+245-" value="GW">Guinea-Bissau</option>
                                        <option ringto="+592-" value="GY">Guyana</option>
                                        <option ringto="+509-" value="HT">Haiti</option>
                                        <option ringto="+504-" value="HN">Honduras</option>
                                        <option ringto="+852-" value="HK">Hong Kong S.A.R., China</option>
                                        <option ringto="+36-" value="HU">Hungary</option>
                                        <option ringto="+354-" value="IS">Iceland</option>
                                        <option ringto="+91-" value="IN">India</option>
                                        <option ringto="+62-" value="ID">Indonesia</option>
                                        <option ringto="+964-" value="IQ">Iraq</option>
                                        <option ringto="+353-" value="IE">Ireland</option>
                                        <option ringto="+972-" value="IL">Israel</option>
                                        <option ringto="+39-" value="IT">Italy</option>
                                        <option ringto="+1876-" value="JM">Jamaica</option>
                                        <option ringto="+81-" value="JP">Japan</option>
                                        <option ringto="+962-" value="JO">Jordan</option>
                                        <option ringto="+254-" value="KE">Kenya</option>
                                        <option ringto="+686-" value="KI">Kiribati</option>
                                        <option ringto="+3774-" value="XK">Kosovo</option>
                                        <option ringto="+965-" value="KW">Kuwait</option>
                                        <option ringto="+996-" value="KG">Kyrgyzstan</option>
                                        <option ringto="+856-" value="LA">Laos</option>
                                        <option ringto="+371-" value="LV">Latvia</option>
                                        <option ringto="+961-" value="LB">Lebanon</option>
                                        <option ringto="+266-" value="LS">Lesotho</option>
                                        <option ringto="+231-" value="LR">Liberia</option>
                                        <option ringto="+218-" value="LY">Libya</option>
                                        <option ringto="+423-" value="LI">Liechtenstein</option>
                                        <option ringto="+370-" value="LT">Lithuania</option>
                                        <option ringto="+352-" value="LU">Luxembourg</option>
                                        <option ringto="+853-" value="MO">Macau</option>
                                        <option ringto="+389-" value="MK">Macedonia</option>
                                        <option ringto="+261-" value="MG">Madagascar</option>
                                        <option ringto="+265-" value="MW">Malawi</option>
                                        <option ringto="+60-" value="MY">Malaysia</option>
                                        <option ringto="+960-" value="MV">Maldives</option>
                                        <option ringto="+223-" value="ML">Mali Republic</option>
                                        <option ringto="+356-" value="MT">Malta</option>
                                        <option ringto="+692-" value="MH">Marshall Islands</option>
                                        <option ringto="+222-" value="MR">Mauritania</option>
                                        <option ringto="+230-" value="MU">Mauritius</option>
                                        <option ringto="+52-" value="MX">Mexico</option>
                                        <option ringto="+691-" value="FM">Micronesia</option>
                                        <option ringto="+373-" value="MD">Moldova</option>
                                        <option ringto="+377-" value="MC">Monaco</option>
                                        <option ringto="+976-" value="MN">Mongolia</option>
                                        <option ringto="+382-" value="ME">Montenegro</option>
                                        <option ringto="+1664-" value="MS">Montserrat</option>
                                        <option ringto="+212-" value="MA">Morocco</option>
                                        <option ringto="+258-" value="MZ">Mozambique</option>
                                        <option ringto="+95-" value="MM">Myanmar (Burma)</option>
                                        <option ringto="+264-" value="NA">Namibia</option>
                                        <option ringto="+674-" value="NR">Nauru</option>
                                        <option ringto="+977-" value="NP">Nepal</option>
                                        <option ringto="+31-" value="NL">Netherlands</option>
                                        <option ringto="+687-" value="NC">New Caledonia</option>
                                        <option ringto="+64-" value="NZ">New Zealand</option>
                                        <option ringto="+505-" value="NI">Nicaragua</option>
                                        <option ringto="+227-" value="NE">Niger Republic</option>
                                        <option ringto="+234-" value="NG">Nigeria</option>
                                        <option ringto="+683-" value="NU">Niue Island</option>
                                        <option ringto="+6723-" value="NF">Norfolk</option>
                                        <option ringto="+850-" value="KP">North Korea</option>
                                        <option ringto="+47-" value="NO">Norway</option>
                                        <option ringto="+968-" value="OM">Oman Dem Republic</option>
                                        <option ringto="+92-" value="PK">Pakistan</option>
                                        <option ringto="+680-" value="PW">Palau Republic</option>
                                        <option ringto="+970-" value="PS">Palestine</option>
                                        <option ringto="+507-" value="PA">Panama</option>
                                        <option ringto="+675-" value="PG">Papua New Guinea</option>
                                        <option ringto="+595-" value="PY">Paraguay</option>
                                        <option ringto="+51-" value="PE">Peru</option>
                                        <option ringto="+63-" value="PH">Philippines</option>
                                        <option ringto="+48-" value="PL">Poland</option>
                                        <option ringto="+351-" value="PT">Portugal</option>
                                        <option ringto="+1787-" value="PR">Puerto Rico</option>
                                        <option ringto="+974-" value="QA">Qatar</option>
                                        <option ringto="+262-" value="RE">Reunion Island</option>
                                        <option ringto="+40-" value="RO">Romania</option>
                                        <option ringto="+7-" value="RU">Russia</option>
                                        <option ringto="+250-" value="RW">Rwanda Republic</option>
                                        <option ringto="+1670-" value="MP">Saipan/Mariannas</option>
                                        <option ringto="+378-" value="SM">San Marino</option>
                                        <option ringto="+239-" value="ST">Sao Tome/Principe</option>
                                        <option ringto="+966-" value="SA">Saudi Arabia</option>
                                        <option ringto="+221-" value="SN">Senegal</option>
                                        <option ringto="+381-" value="RS">Serbia</option>
                                        <option ringto="+248-" value="SC">Seychelles Island</option>
                                        <option ringto="+232-" value="SL">Sierra Leone</option>
                                        <option ringto="+65-" value="SG">Singapore</option>
                                        <option ringto="+421-" value="SK">Slovakia</option>
                                        <option ringto="+386-" value="SI">Slovenia</option>
                                        <option ringto="+677-" value="SB">Solomon Islands</option>
                                        <option ringto="+252-" value="SO">Somalia Republic</option>
                                        <option ringto="+685-" value="WS">Somoa</option>
                                        <option ringto="+27-" value="ZA">South Africa</option>
                                        <option ringto="+82-" value="KR">South Korea</option>
                                        <option ringto="+34-" value="ES">Spain</option>
                                        <option ringto="+94-" value="LK">Sri Lanka</option>
                                        <option ringto="+290-" value="SH">St. Helena</option>
                                        <option ringto="+1869-" value="KN">St. Kitts</option>
                                        <option ringto="+1758-" value="LC">St. Lucia</option>
                                        <option ringto="+508-" value="PM">St. Pierre</option>
                                        <option ringto="+1784-" value="VC">St. Vincent</option>
                                        <option ringto="+249-" value="SD">Sudan</option>
                                        <option ringto="+597-" value="SR">Suriname</option>
                                        <option ringto="+268-" value="SZ">Swaziland</option>
                                        <option ringto="+46-" value="SE">Sweden</option>
                                        <option ringto="+41-" value="CH">Switzerland</option>
                                        <option ringto="+963-" value="SY">Syria</option>
                                        <option ringto="+886-" value="TW">Taiwan</option>
                                        <option ringto="+992-" value="TJ">Tajikistan</option>
                                        <option ringto="+255-" value="TZ">Tanzania</option>
                                        <option ringto="+66-" value="TH">Thailand</option>
                                        <option ringto="+228-" value="TG">Togo Republic</option>
                                        <option ringto="+690-" value="TK">Tokelau</option>
                                        <option ringto="+676-" value="TO">Tonga Islands</option>
                                        <option ringto="+1868-" value="TT">Trinidad &amp; Tobago</option>
                                        <option ringto="+216-" value="TN">Tunisia</option>
                                        <option ringto="+90-" value="TR">Turkey</option>
                                        <option ringto="+993-" value="TM">Turkmenistan</option>
                                        <option ringto="+1649-" value="TC">Turks &amp; Caicos Island</option>
                                        <option ringto="+688-" value="TV">Tuvalu</option>
                                        <option ringto="+256-" value="UG">Uganda</option>
                                        <option ringto="+380-" value="UA">Ukraine</option>
                                        <option ringto="+971-" value="AE">United Arab Emirates</option>
                                        <option ringto="+44-" value="GB">United Kingdom</option>
                                        <option ringto="+598-" value="UY">Uruguay</option>
                                        <option ringto="+1-" value="US">USA/Canada</option>
                                        <option ringto="+998-" value="UZ">Uzbekistan</option>
                                        <option ringto="+678-" value="VU">Vanuatu</option>
                                        <option ringto="+3966-" value="VA">Vatican City</option>
                                        <option ringto="+58-" value="VE">Venezuela</option>
                                        <option ringto="+84-" value="VN">Vietnam</option>
                                        <option ringto="+1340-" value="VI">Virgin Islands (US)</option>
                                        <option ringto="+681-" value="WF">Wallis/Futuna Islands</option>
                                        <option ringto="+967-" value="YE">Yemen Arab Republic</option>
                                        <option ringto="+260-" value="ZM">Zambia</option>
                                        <option ringto="+263-" value="ZW">Zimbabwe</option>
                                    </select>
                                    <br>
                                    <div class="red" id="country_error"> <?php echo $form['country']->renderError(); ?> </div></td>
                            </tr>
                            <tr>
                                <td><?php echo $form['email']->renderLabel(); ?><span class="red">*</span></td>
                                <td><?php echo $form['email']->render(); ?> <br>
                                    <div class="red" id="email_error"> <?php echo $form['email']->renderError(); ?> </div></td>
                            </tr>
                            <tr>
                                <td><?php echo $form['phone']->renderLabel(); ?><span class="red">*</span></td>
              <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0" class="no_border_new">
                <tr>
                  <td width="50"><input type="text" class="phone_small_box" name="mobile_frefix" id="mobile_frefix" style="width:38px; border:1px solid #A3ABB8; padding:2px;" readonly></td>
                  <td><?php echo $form['phone']->render(); ?></td>
                        </tr>
                <tr>
                  <td colspan="2"><div class="red" id="phone_error"> <?php echo $form['phone']->renderError(); ?> </div></td>

                </tr>
              </table></td>
              </tr>
            <tr class="no_display" id="billinfo_td">
              <td colspan="2">&nbsp;</td>
              </tr>
                    </table>
                </td>
        <td width="50%" valign="top" colspan="2"><table width="100%" cellpadding="0" cellspacing="0" border="0" id="cardinfo_id">
                        <tr>
              <td class="tmz-tab-heading" colspan="2" align="right"><p>Card information</p></td>
                        </tr>
                        <tr>
              <td width="30%"><?php echo $form['card_type']->renderLabel(); ?><span class="red">*</span></td>
                            <td><?php echo $form['card_type']->render(); ?> <br>
                                <div class="red" id="card_type_error"> <?php echo $form['card_type']->renderError(); ?> </div></td>
                        </tr>
                        <tr>
                            <td><?php echo $form['card_Num']->renderLabel(); ?><span class="red">*</span></td>
                            <td><?php echo $form['card_Num']->render(); ?> <br>
                                <img src="<?php echo image_path('visa_new.gif'); ?>" alt="" class="pd4" /> <br>
                                <div class="red" id="card_Num_error"><?php echo $form['card_Num']->renderError(); ?> </div></td>
                        </tr>
                        <tr>
                            <td><?php echo $form['card_holder']->renderLabel(); ?><span class="red">*</span></td>
                            <td><?php echo $form['card_holder']->render(); ?> <br>
                                <div class="red" id="card_holder_error"> <?php echo $form['card_holder']->renderError(); ?> </div></td>
                        </tr>
                        <tr>
                            <td><?php echo $form['expiry_date']->renderLabel(); ?><span class="red">*</span></td>
                            <td><?php echo $form['expiry_date']->render(); ?> <br>
                                <div class="red" id="expiry_date_error"> <?php echo $form['expiry_date']->renderError(); ?> </div></td>
                        </tr>
                        <tr>
                            <td><?php echo $form['user_id_proof']->renderLabel(); ?><span class="red">*</span></td>
                            <td><?php echo $form['user_id_proof']->render(); ?> <br>
                <span class="red">NOTE: Kindly provide valid photo identity.<br>
                (For example: License, PAN card, Passport)</span> <br>
                                -Please upload JPG/GIF/PNG/PDF images only. <br>
                                -File size upto 400 KB.
                                <div class="red" id="user_id_proof_error"> <?php echo $form['user_id_proof']->renderError(); ?> </div></td>
                        </tr>
                        <tr>
                            <td><?php echo $form['address_proof']->renderLabel(); ?><span class="red">*</span></td>
                            <td><?php echo $form['address_proof']->render(); ?> <br>
                                -Please upload JPG/GIF/PNG/PDF images only. <br>
                                -File size upto 400 KB.
                                <div class="red" id="address_proof_error"> <?php echo $form['address_proof']->renderError(); ?> </div></td>
                        </tr>
                        <tr>
              <td colspan="2"><ol>
                                    The credit card statement must clearly show proof of your name and billing
                                    address as associated with the credit card. Please mask your credit card number
                                    middle 8 digits as <a href='javascript: void(0)' onclick='window.open("<?php echo url_for('userAuth/exampleOfCard'); ?>",
                                        "windowname1","width=400, height=170")'>described here</a>.
                </ol></td>
                        </tr>
                        <tr>
              <td colspan="2" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0">
                <tr>
                            <td nowrap="nowrap"><u><?php echo $form['transaction_type']->renderLabel(); ?></u><span class="red">***</span></td>
                  <td width="50%"><span class="txn" id="spantxn" ><?php echo $form['transaction_type']->render(); ?></span><br />
                                <div class="red" id="err_txn_type"> <?php echo $form['transaction_type']->renderError(); ?> </div></td>
                        </tr>
                <tr class="no_display" id="reason_div">
                            <td colspan="2"><table width="100%">
                                    <tr>
                        <td width="30%"><?php echo $form['reason']->renderLabel(); ?><span class="red">*</span></td>
                                        <td><?php echo $form['reason']->render(); ?>
                                            <div class="red" id="reason_error"> <?php echo $form['reason']->renderError(); ?> </div></td>
                                    </tr>
                                    <tr>
                                        <td><?php echo $form['agent_proof']->renderLabel(); ?></td>
                        <td><?php echo $form['agent_proof']->render(); ?> <br />
                            <span class="red"> NOTE: Please provide proof to justify above reason.</span><br />
                          -Please upload JPG/GIF/PNG/PDF images only. <br />
                                            -File size upto 400 KB.
                          <div class="red" id="agent_proof_error"> <?php echo $form['agent_proof']->renderError(); ?> </div></td>
                                    </tr>
                                </table></td>
                        </tr>
              </table></td>
            </tr>
                    </table>
                </td>
            </tr>
        </table>
        <br/>
        <tfoot>
            <tr>
          <td  colspan="3"><?php if (!$form->getObject()->isNew()): ?>
                                    &nbsp;
                    <?php //echo link_to('Delete', 'payeasy/delete?id='.$form->getObject()->getId(), array('method' => 'delete', 'confirm' => 'Are you sure?')) ?>
                    <?php endif; ?>
                                    <input type="hidden" name="requestId" id="requestId" value="<?php echo $requestId; ?>">
            <div align="center" id="payButDiv">
                        <?= $form->renderHiddenFields(); ?>
              <input type="submit" value="Submit" id="payBut" name="payBut" class="normalbutton" />
                        <?= $form->renderGlobalErrors(); ?>
            </div></td>
                        </tr>
                    </tfoot>
                </form>
            </div>
</div>
<div class="content_wrapper_bottom"></div>
<!--            </div>-->
            <br/>
            <script>

                function checkRequestedCardType(){
                    var visaReg = /^4[0-9]{12}(?:[0-9]{3})?$/;
                    var masterReg = /^5[1-5][0-9]{14}$/;
                    var americanReg = /^3[47][0-9]{13}$/;
                    var jcbReg = /^(?:2131|1800|35\d{3})\d{11}$/;
                    var dinersReg = /^3(?:0[0-5]|[68][0-9])[0-9]{11}$ /;
                    var cardType = document.getElementById('applicant_vault_card_type').value;
                    var cardNum = document.getElementById('applicant_vault_card_Num').value;
                    var tmpyear;
                    var tmpmonth;
                    /* Start Varun
                     * Variable added for jJavascript Validations*/
                    var err  = 0;
                    var nameLen = jQuery.trim($('#applicant_vault_first_name').val()).length;
                    var lastNameLen = jQuery.trim($('#applicant_vault_last_name').val()).length;
                    var address1Len = jQuery.trim(document.getElementById('applicant_vault_address1').value).length;
                    var address2Len = jQuery.trim(document.getElementById('applicant_vault_address2').value).length;
                    var townLen = jQuery.trim(document.getElementById('applicant_vault_town').value).length;
                    var stateLen = jQuery.trim(document.getElementById('applicant_vault_state').value).length;
                    var zipLen = document.getElementById('applicant_vault_zip').value.length;
                    var emailLen = document.getElementById('applicant_vault_email').value.length;
                    var phoneLen = jQuery.trim(document.getElementById('applicant_vault_phone').value).length;
                    var cardHolderNameLen = jQuery.trim($('#applicant_vault_card_holder').val()).length;

                    /* START FIRST NAME VALIDATION */
                    var firstName = jQuery.trim($('#applicant_vault_first_name').val());

                    if(firstName == ""){
                        $('#first_name_error').html('Please enter First Name.');
                        err = err +1;
                    }else if(nameLen < $('#applicant_vault_first_name').val().length){
                        $('#first_name_error').html('Please remove blank spaces before and after First Name.');
                        err = err +1;
                    } else if(validateName(firstName) != 0){
                        $("#first_name_error").html('Please enter correct Fisrt Name.');
                        err = err+1;
                    } else if(nameLen < 2) {
                        $('#first_name_error').html('First Name cannot be less than 2 characters.');
                        err = err +1;
                    } else if(nameLen > 32) {
                        $('#first_name_error').html('First Name cannot be greater than 32 characters.');
                        err = err +1;
                    } else {
                        $('#first_name_error').html("");
                    }
                    /* END FIRST NAME VALIDATION */


                    /* START LAST NAME VALIDATION */
                    var lastName = jQuery.trim($('#applicant_vault_last_name').val());

                    if(lastName == ""){
                        $('#last_name_error').html('Please enter Last Name.');
                        err= err +1;
                    } else if(lastNameLen < $('#applicant_vault_last_name').val().length){
                        $('#last_name_error').html('Please remove blank spaces before and after Last Name.');
                        err = err +1;
                    } else if(validateName(lastName) != 0){
                        $("#last_name_error").html('Please enter correct Last Name.');
                        err = err+1;
                    } else if(lastNameLen < 2) {
                        $('#last_name_error').html('Last Name cannot be less than 2 characters.');
                        err= err +1;
                    } else if(lastNameLen > 32) {
                        $('#last_name_error').html('Last Name cannot be greater than 32 characters.');
                        err= err +1;
                    } else{
                        $('#last_name_error').html("");
                    }
                    /* END LAST NAME VALIDATION */

                    if(jQuery.trim($('#applicant_vault_address1').val()) == "")
                    {
                        $('#address1_error').html('Please enter Address 1.');
                        err = err+1;
                    }
                    else if(validateString($('#applicant_vault_address1').val())!=0){
                        $('#address1_error').html('Please enter correct Address 1.');
                        err = err+1;
                    }
                    else if(address1Len < 2)
                    {
                        $('#address1_error').html('Address1 cannot be less than 2 characters.');
                        err= err +1;
                    }
                    else if(address1Len > 48)
                    {
                        $('#address1_error').html('Address1 cannot be greater than 48 characters.');
                        err= err +1;
                    }
                    else{
                        $('#address1_error').html('');
                    }
                    if(address2Len>0){
                        if(validateString($('#applicant_vault_address2').val())!=0){
                            $('#address2_error').html('Please enter correct Address 2.');
                            err = err+1;
                        }
                        else if(address2Len < 2)
                        {
                            $('#address2_error').html('Address2 cannot be less than 2 characters.');
                            err= err +1;
                        }
                        else if(address2Len > 48)
                        {
                            $('#address2_error').html('Address2 cannot be greater than 48 characters.');
                            err= err +1;
                        }
                        else{
                        $('#address2_error').html('');
                    }
                    }
                    else{
                        $('#address2_error').html('');
                    }

                    if(jQuery.trim($('#applicant_vault_town').val()) == "")
                    {
                        $('#town_error').html('Please enter Town.');
                        err =err+1;
                    }
                    else if(townLen < 2)
                    {
                        $('#town_error').html('Town cannot be less than 2 characters');
                        err= err +1;
                    }
                    else if(townLen > 48)
                    {
                        $('#town_error').html('Town cannot be greater than 48 characters.');
                        err= err +1;
                    }
                    else if(validateString($('#applicant_vault_town').val())!=0){
                        $('#town_error').html('Please enter correct Town.');
                        err = err+1;
                    }
                    else{
                        $('#town_error').html('');
                    }

                    if(stateLen>0)
                    {
                        if(stateLen < 2)
                        {
                            $('#state_error').html('State cannot be less than 2 characters.');
                            err= err +1;
                        }
                        else if(stateLen > 32)
                        {
                            $('#state_error').html('State cannot be greater than 32 characters.');
                            err= err +1;
                        } else {
                              $('#state_error').html("");
                         }
                    }
                    else{
                        $('#state_error').html('');
                    }
                    if(zipLen>0){
                        if(zipLen > 16)
                        {
                            $('#zip_error').html('ZIP Code cannot be greater than 16 characters.');
                            err= err +1;
                        }
                    }
                    else{
                        $('#zip_error').html('');
                    }

                    if($('#applicant_vault_zip').val()!='')
                    {
                    if(validateZip($('#applicant_vault_zip').val()) !=0)
                      {
                          $('#zip_error').html("Please enter Valid ZIP(Postal Code).");
                          err = err+1;
                      }
                      else
                      {
                          $('#zip_error').html("");
                      }
                    }
                    else{
                        $('#zip_error').html('');
                    }



                    if($('#applicant_vault_country').val() == "")
                    {
                        $('#country_error').html("Please select Country.");
                        err = err+1;
                    }
                    else
                    {
                        $('#country_error').html("");
                    }

                    if(jQuery.trim($('#applicant_vault_email').val()) == "")
                    {
                        $('#email_error').html('Please enter Email address.');
                        err =err+1;
                    }
                    else if(validateEmail($('#applicant_vault_email').val())!=0){
                        $('#email_error').html('Please enter correct Email address.');
                        err = err+1;
                    }
                    else if(emailLen < 8)
                    {
                        $('#email_error').html('Email cannot be less than 8 characters.');
                        err= err +1;
                    }
                    else if(emailLen > 66)
                    {
                        $('#email_error').html('Email cannot be greater than 66 characters.');
                        err= err +1;
                    }
                    else{
                        $('#email_error').html('');
                    }

                    if(jQuery.trim($('#applicant_vault_phone').val()) == "")
                    {
                        $('#phone_error').html('Please enter Phone Number.');
                        err =err+1;
                    }
                    else if(validatePhone($('#applicant_vault_phone').val())!=0){
                        $('#phone_error').html('Please enter valid Phone Number<br/>Phone Number should be between 8-16 digits.');
                        err = err+1;
                    }
                    else{
                        $('#phone_error').html('');
                    }


                    if(cardNum != '' && cardType != ''){

                        var cardLen = cardNum.length;
                        if(cardType =='V'){
                            if(!visaReg.test(cardNum)){
                                $("#card_Num_error").html('Please enter correct Card Number.')
                                err = err+1;
                            }else if(!(cardLen == 13 || cardLen == 16)){
                                  $("#card_Num_error").html('Please enter correct Card Number.')
                                  err = err+1;
                            }else{
                              $('#card_Num_error').html('');
                            }
                        }
                         

                        
                        if(cardType =='A'){
                          
                            if(!americanReg.test(cardNum)){
                              $("#card_Num_error").html('Please enter correct Card Number.');
                                err = err+1;
                            }else
                            if(cardLen != 15){
                                $("#card_Num_error").html('Please enter correct Card Number.');
                                err = err+1;
                            }else{
                            $('#card_Num_error').html('');

                            }
                        }
                         
                        
                        if(cardType =='M'){
                          
                            if(!masterReg.test(cardNum)){
                                $("#card_Num_error").html('Please enter correct Card Number.')
                                err = err+1;
                            }else
                            if(cardLen != 16){
                                $("#card_Num_error").html('Please enter correct Card Number.')
                                err = err+1;
                            } else{
                            $('#card_Num_error').html('');
                        }
                        }
                       
                    }else{
                        /* HTML error field made empty in case of no Error*/
                        $('#card_Num_error').html('Please enter a Card Number.');
                        err = err+1;
                    }
                    
                    /* Start Javascript added for Card Holder name*/

                    var cardHolderName = jQuery.trim($('#applicant_vault_card_holder').val());

                    if(cardHolderName == ""){
                        $('#card_holder_error').html('Please enter Card Holder name.');
                        err= err +1;
                    } else if(cardHolderNameLen < $('#applicant_vault_card_holder').val().length){
                        $('#card_holder_error').html('Please remove blank spaces before and after Card Holder Name.');
                        err = err +1;
                    } else if(validateName($('#applicant_vault_card_holder').val())!=0){
                        $("#card_holder_error").html('Please enter correct Card Holder name.');
                        err = err+1;
                    } else if(cardHolderNameLen < 2){
                        $('#card_holder_error').html('Card Holder Name cannot be less than 2 characters.');
                        err= err +1;
                    } else if(cardHolderNameLen > 32){
                        $('#card_holder_error').html('Card Holder Name cannot be greater than 32 characters.');
                        err= err +1;
                    } else{
                        $('#card_holder_error').html("");
                    }


                    if ($('#applicant_vault_expiry_date_year').val() == '') {
                        $('#expiry_date_error').html('Please select the Expiration Year.');
                        err= err +1;
                    }else{
                        $('#expiry_date_error').html('');
                    }


                    if ($('#applicant_vault_expiry_date_month').val() == '') {
                        $('#applicant_vault_expiry_date_month').html('Please select the Expiration Month.');
                        err= err +1;
                    }else{
                        $('#expiry_date_error').html('');
                    }


                    tmpyear = document.getElementById('applicant_vault_expiry_date_year').value;
                    tmpmonth = document.getElementById('applicant_vault_expiry_date_month').options[document.getElementById('applicant_vault_expiry_date_month').selectedIndex].value;



                    if(!ValidateExpDate(tmpyear,tmpmonth)){
                        document.getElementById('expiry_date_error').innerHTML = 'This card has already expired.';
                        err = err+1;
                    }

                    /* Javascipt validations added for User id proof , Address proof and Transaction Type */
                    if(jQuery.trim($('#applicant_vault_user_id_proof').val()) == "")
                    {
                        $('#user_id_proof_error').html('Please upload identity proof.');
                        err= err +1;
                    }
                    else
                    {
                        $('#user_id_proof_error').html("");
                    }

                    if(jQuery.trim($('#applicant_vault_address_proof').val()) == "")
                    {
                        $('#address_proof_error').html('Please upload credit card statement.');
                        err= err +1;
                    }
                    else
                    {
                        $('#address_proof_error').html("");
                    }
                   

                    if($("input[name='applicant_vault[transaction_type]']:checked").next('label').text() == "")
                    {
                        $('#err_txn_type').html('Please select transaction type');
                        err= err +1;
                    } else
                    {
                        $('#err_txn_type').html("");
                    }

                    if ($('#reason_div').is(':visible') && (document.getElementById('applicant_vault_reason').value == '')) {
                        document.getElementById('reason_error').innerHTML = 'Reason is required.';
                        err = err+1;
                    }else{
                        document.getElementById('reason_error').innerHTML = '';
                    }                    

                    if(err > 0)
                    {
                        return false;
                    }
                    else{ 
                        return true;
                    }
                }
                /* END Varun */

                $(document).ready(function() { 
                    $("#applicant_vault_country").change(function() {
                        var phone_prefix = $('#applicant_vault_country option:selected').attr('ringto');
                        $('#mobile_frefix').val(phone_prefix);

                    });

                    $('#applicant_vault_user_id_proof').keypress(function(){                        
                        return false;
                    });
                    $('#applicant_vault_address_proof').keypress(function(){
                        return false;
                    });
                    $('#applicant_vault_agent_proof').keypress(function(){
                        return false;
                    });


<?php if (isset($_REQUEST['applicant_vault']['country']) && $_REQUEST['applicant_vault']['country'] != '') { ?>

                        $('#applicant_vault_country option[value="<?= $_REQUEST['applicant_vault']['country']; ?>"]').attr('selected', 'selected');

                        var phone_prefix = $('#applicant_vault_country option:selected').attr('ringto');
                        $('#mobile_frefix').val(phone_prefix);




<?php } ?>
    });

    function displayDiv(){
        $('#err_txn_type').html('');

        if($("input[name='applicant_vault[transaction_type]']:checked").next('label').text() == 'Yes'){
            $('#reason_div').show();
            $('#applicant_vault_agent_proof').attr("disabled","");
//			alert('a');
//			alert($('#reason_div').height());
//			alert($('#cardinfo_id').height());

			var newHeight = parseInt($('#reason_div').height()); // + parseInt(24);
//			alert(newHeight);
			$('#billinfo_td').attr('height',newHeight+'px;');
			$('#billinfo_td').show();

        }else{
            $('#reason_div').hide();
			$('#billinfo_td').hide();
            $('#applicant_vault_reason').val('');
            $('#applicant_vault_agent_proof').val("");
            $('#applicant_vault_agent_proof').attr("disabled","disabled");
        }
    }
    if($("input[name='applicant_vault[transaction_type]']:checked").next('label').text() == 'Yes'){
        $('#reason_div').show();
        $('#applicant_vault_agent_proof').attr("disabled","");
    }
    /* START Varun
     * Functions added to check regular expressions for javascript validations */
    function validateName(str)
    {
        var reg = /^[A-Za-z \-\.]*$/; //allow alphabet,spaces and hyphen only(as hyphen are allowed in passport & visa application form) ...
        if(reg.test(str) == false) {
            return 1;
        }

        return 0;
    }
  
    function validateString(str)
    {
        var reg = /[^\s+]/; //allow special characters ` ' - . and space
        if(reg.test(str) == false) {
            return 1;
        }

        return 0;
    }

    function validateZip(zip) {
        var reg = /^([A-Za-z0-9])+$/;
        if(reg.test(zip) == false) {
            return 1;
        }

        return 0;
    }

    function validateEmail(email) {

        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        if(reg.test(email) == false) {

            return 1;
        }

        return 0;
    }
    function validatePhone(phone) {

        var reg =  /^[0-9[\]+()-]{8,16}?$/;
        if(reg.test(phone) == false) {

            return 1;
        }

        return 0;
    }
    /* END Varun */

</script>
