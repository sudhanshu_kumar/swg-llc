<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class userAuthActions extends sfActions
{
  public function executeNewContectDetails(sfWebRequest $request){
    $this->isPrivilegeUser = $this->getUser()->isPrivilegeUser();
    $this->setTemplate("contectDetails");
    $this->form = new ApplicantVaultForm();
  }

  public function executeRegisterCreditCard(sfWebRequest $request){
    $this->isPrivilegeUser = $this->getUser()->isPrivilegeUser();
    $this->setTemplate("registerCreditCard");
  }


  public function executeSaveApplicantVault(sfWebRequest $request){
    $this->isPrivilegeUser = $this->getUser()->isPrivilegeUser();
    $this->setTemplate("contectDetails");
    $this->form = new ApplicantVaultForm();

    $mobile_prefix = $request['mobile_frefix'];

    $formVal = $request->getParameter($this->form->getName());
    $phone = $formVal['phone'];
    $mobile_prefix = $request['mobile_frefix'];
    $phone_number = $mobile_prefix.''.$phone;

    $formVal['address1'] = htmlspecialchars(addslashes($formVal['address1']));
    $formVal['address2'] = htmlspecialchars(addslashes($formVal['address2']));
    $cardnumber = $formVal['card_Num'];
    $cardnumberFirst = substr($cardnumber, 0,4);
    $cardnumberLast = substr($cardnumber, -4);
    $cardnumberLast = str_pad($cardnumberLast, 4, "0", STR_PAD_LEFT);
    $cardLen = strlen($cardnumber);

    $this->form->bind($request->getParameter($this->form->getName()),$request->getFiles($this->form->getName()));
    if($this->form->isValid()){
        
      //      $this->form->save();
       $isValid4Registration = ApplicantVaultTable::getInstance()->isValid4Registration($cardnumber);
      if($isValid4Registration == 'New' || $isValid4Registration == 'Approved')
      {
        $msg = ($isValid4Registration == 'New')? "Pending" : 'Approved';
        $this->getUser()->setFlash('notice',"This card is already in $msg state, please use another card.");
      }
      else{
        $userId = $this->getUser()->getGuardUser()->getId();
        $formObj = $this->form->getObject();
        $formObj->setCardFirst($cardnumberFirst);
        $formObj->setCardLast($cardnumberLast);
        $formObj->setCardLen($cardLen);
        $formObj->setCardHash(md5($cardnumber));
        $formObj->setExpiryYear($formVal['expiry_date']['year']);
        $formObj->setExpiryMonth($formVal['expiry_date']['month']);
        $formObj->setPhone($phone_number);
        $formObj->setUserId($userId);
        $obj = $this->form->save();
        $request_id = $obj->id;
        $tableobj = ApplicantVaultTable::getInstance()->find($request_id);
        $tableobj->setPhone($phone_number);
        $tableobj->setAddress1($formVal['address1']);
        $tableobj->setAddress2($formVal['address2']);
        $tableobj->save();
        //send mail to the customer, for application submission
        $mailUrl = "notification/creditCardRequestMail" ;
        $mailTaskId = EpjobsContext::getInstance()->addJob('CreditCardAddRequestMail',$mailUrl, array('request_id'=>$request_id,"user_id"=>$userId));

        $this->getUser()->setFlash('notice','<b>We will get back to you with approval status of your request in 2 working days. You will be notified on your email about the final status of your request.</b>');
        $this->redirect("userAuth/showCardDetails");
      }
    }else{
    }

    //    die;
  }

  public function executeFailurePopUp(){

    $this->setLayout('popupLayout');
  }

  public function executeExampleOfCard(){

    $this->setLayout('popupLayout');
  }

  public function executeShowCardDetails(sfWebRequest $request){
    $user = $this->getUser()->getGuarduser()->getId();
    $userCardQuery = ApplicantVaultTable::getInstance()->getUserCardDetails($user);
    $page = 1;
    if($request->hasParameter('page')) {
      $page = $request->getParameter('page');
    }

    $this->pager = new sfDoctrinePager('SupportRefundList',sfConfig::get('app_records_per_page'));
    $this->pager->setQuery($userCardQuery);
    $this->pager->setPage($this->getRequestParameter('page',$page));
    $this->pager->init();
    $this->setTemplate("show");
  }




  public function executeAuthDetails(sfWebRequest $request)
  {
    $getAppId = SecureQueryString::DECODE($request->getParameter('id'));
    $userAuthId = SecureQueryString::ENCRYPT_DECRYPT($getAppId);
    $this->userAuthDetailsObj = Doctrine::getTable("ApplicantVault")->find($userAuthId);
    $this->address1 = htmlentities(stripslashes($this->userAuthDetailsObj->getAddress1()));
    if($this->userAuthDetailsObj->getAddress2())
    {            
    $this->address2 = htmlentities(stripslashes($this->userAuthDetailsObj->getAddress2()));
    }
    //      die($this->userAuthDetailsObj->getCountryName());
    $this->userAuthDetails = $this->userAuthDetailsObj->toArray();
    $this->setTemplate("userAuthDetails");

  }
}
