<?php use_helper('Form');
use_helper('Pagination'); ?>
<table width="100%" id="addNewCountryTable" style="margin:0px;">
   <tr>
    <td class="blbar" colspan="2" align="right">
    <div style="float:left">Add New Configuration</div><span class = "txtBold"><input type = "button" value="X" onclick="disablePopup();"></span>
    </td>
    </tr>
   <tr>
   <td width="50%"><span class = "txtBold">Country</span></td>
   <td width="50%"><span class = "txtBold">Register Card</span></td>
   </tr>
   <tr>
   <?php $yesNoArray = array('YES','NO');?>
   <td width="50%"> <?php echo select_tag('country_id', options_for_select($finalCountryArray,'')); ?></td>
   <td width="50%"> <?php echo select_tag('isRegistered', options_for_select($yesNoArray, array('include_custom' => '-- Please Select --')),array('id' =>'isRegistered')); ?></td>
   </tr>
   <tr>
    <td colspan="2"><input type="button" id = "saveButtonn" ame="btn" class = "loginbutton" value="Save" onclick="saveNewCountry();" /></td>
   </tr>
  </table>

<script>
   function saveNewCountry(){
       $('#result_div').html('<b><font color ="green">Please Wait While Country is Adding...</font></b>');
       $("#result_div").show();
       var recordPerPager= '<?php echo sfConfig::get('app_records_per_page')?>'
       var countryCode =  $("#country_id").val();
       if(countryCode == parseInt(0)){
          countryCode = 'ALL';
       }
       if(countryCode == 'NoCountry'){
           alert('No Country to Save');
           return false;
       }
       $('#saveButtonn').attr("disabled", true);
       var isRegistered = $("#isRegistered [value='"+$('#isRegistered').val()+"']").text();
       var url = "<?php echo url_for('configurationManager/saveNewCountry') ?>";
       $.post(url,{countryCode : countryCode,isRegistered : isRegistered },function(data){
            $("#result_div").html('<b><font color ="green">Country Added Successfully</font></b>');            
            $('#saveButtonn').attr("value", 'Wait...');
            $("#result_div").hide(2500);
            var dataArray = data.split('^');
            var recordCount = dataArray[1];
            var noOfPages = Math.ceil(recordCount/recordPerPager);
            var page = $('#page').val();
            disablePopup();

           var pagenoUri = '<?php echo url_for('configurationManager/viewAjax');?>';
           ajax_paginator('nxtPage', pagenoUri+'/page/'+(parseInt(noOfPages)));

           getPaginBar(noOfPages);

            if(parseInt(noOfPages) == parseInt(page)){                
                $('#noRecordFound').hide();                
                if(countryCode != 'ALL') {
                    $('#removeAllButton').show();
                     $('#chk_all').removeAttr("disabled");
                }
            }
        });
    }
 </script>
