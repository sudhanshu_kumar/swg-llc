<?php use_stylesheet('style.css');
use_javascript('calendar.js');
echo include_stylesheets();
echo include_javascripts(); ?>
<div id="flash_notice" class="alertBox" style="display:none"></div>
<div class="clearfix">
  <div id="nxtPage">
  <form action='#' method="post" name="frm_tra_detail" id="frm_tra_detail" onsubmit="return false;">
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
      <tr>
      <?php if(empty($id)){?>
          <td width="200px;"><?php echo $form['country_code']->renderLabel(); ?><span class="red"></span></td>
          <td><?php echo $form['country_code']->render(); ?>
              <br>
              <div class="red" id="country_code_error">
                  <?php echo $form['country_code']->renderError(); ?>
              </div>
          </td>
          <?php }else{?>

          <td>Country Name </td>
          <td width="200px;"><?php
          $countryName =  Doctrine::getTable('Country')->findBy('id', $countryId);
          if($countryName[0]['country_name'] == ''){echo 'ALL';} else { echo ucwords($countryName[0]['country_name']);  } 
          ?></td>
          <input type="hidden" name="hdn_country" id="hdn_country" value="<?php echo $countryId;?>">
          <?php } ?>
      </tr>

      <tr>
          <td width="200px;"><?php echo $form['card_type']->renderLabel(); ?><span class="red"></span></td>
          <td><?php echo $form['card_type']->render(); ?>
              <br>
              <div class="red" id="card_type_error">
                  <?php echo $form['card_type']->renderError(); ?>
              </div>
          </td>

      </tr>
      <tr>
          <td ><?php echo $form['service']->renderLabel(); ?><span class="red"></span></td>
          <td colspan="2"><?php echo $form['service']->render(); ?>
              <br>
              <div class="red" id="service_error">
                  <?php echo $form['service']->renderError(); ?>
              </div>

          </td>
      </tr>
      <tr>
          <td ><?php echo $form['validation']->renderLabel(); ?><span class="red"></span></td>
          <td colspan="2"><?php echo $form['validation']->render(); ?>
              <br>
              <div class="red" id="validation_error">
                  <?php echo $form['validation']->renderError(); ?>
              </div>

          </td>
      </tr>
      <tr>
          <td ><?php echo $form['cart_capacity']->renderLabel(); ?><span class="red"></span></td>
          <td colspan="2"><?php echo $form['cart_capacity']->render(); ?>
              <br>
              <div class="red" id="cart_capacity_error">
                  <?php echo $form['cart_capacity']->renderError(); ?>
              </div>

          </td>
      </tr>
      <tr>
          <td ><?php echo $form['cart_amount_capacity']->renderLabel(); ?><span class="red"></span></td>
          <td colspan="2"><?php echo $form['cart_amount_capacity']->render(); ?>
              <br>
              <div class="red" id="cart_amount_capacity_error">
                  <?php echo $form['cart_amount_capacity']->renderError(); ?>
              </div>
          </td>
      </tr>
      <tr>
          <td ><?php echo $form['number_of_transaction']->renderLabel(); ?><span class="red"></span></td>
          <td colspan="2"><?php echo $form['number_of_transaction']->render(); ?>
              <br>
              <div class="red" id="number_of_transaction_error">
                  <?php echo $form['number_of_transaction']->renderError(); ?>
              </div>
          </td>
      </tr>
      <tr>
          <td ><?php echo $form['transaction_period']->renderLabel(); ?><span class="red"></span></td>
          <td colspan="2"><?php echo $form['transaction_period']->render(); ?>
          <input type="hidden" name="hdn_id" id="hdn_id" value="<?php echo $id;?>">
              <br>
              <div class="red" id="transaction_period_error">
                  <?php echo $form['transaction_period']->renderError(); ?>
              </div>
          </td>
      </tr>


      <tr>
        <td align="center" colspan="2">
          <div class="divBlock">
            <center><input style="" type="button" class="loginbutton"  id="multiFormSubmit" value="Save" onclick="validateForm();">
            &nbsp;&nbsp;
            <input style="" type="button" class="loginbutton"  id="multiFormSubmit" value="Close" onclick="parent.emailwindow.hide();">

            </center>
          </div>
        </td>
      </tr>
    </table>
    </form>
    <br>
  </div>
</div>

<script type="text/javascript">


  function validateForm(){
     var err  = 0;
     var reg = /^[\-0-9a-zA-Z\,_]+$/;
     var reg1 = /^[\a-zA-Z\ ]+$/;
     if($('#country_code').val() == "" || reg1.test($('#country_code').val()) == false)
    {
      $('#country_code_error').html("Please enter valid Country");
      err = err+1;
    }else
    {
      $('#country_code_error').html(" ");
    }
    
  if($('#card_type').val() == null)
    {
      $('#card_type_error').html("Please select Card Type");
      err = err+1;
    }else
    {
      $('#card_type_error').html(" ");
    }

      if($('#service').val() == null)
      {
        $('#service_error').html("Please enter valid Service");
        err = err+1;
      }else
      {
        $('#service_error').html(" ");
      }

      if($('#validation').val() == null)
      {
        $('#validation_error').html("Please select valid Validation");
        err = err+1;
      }else
      {
        $('#validation_error').html(" ");
      }

      if($('#cart_capacity').val() == "" || isNaN($('#cart_capacity').val()))
      {
        $('#cart_capacity_error').html("Please enter valid Cart Capacity");
        err = err+1;
      }else
      {
        $('#cart_capacity_error').html(" ");
      }

      if($('#cart_amount_capacity').val() == "" || isNaN($('#cart_amount_capacity').val()))
      {
        $('#cart_amount_capacity_error').html("Please enter valid Cart Amount Capacity");
        err = err+1;
      }else
      {
        $('#cart_amount_capacity_error').html(" ");
      }
      
      if($('#number_of_transaction').val() == "" || isNaN($('#number_of_transaction').val()))
      {
        $('#number_of_transaction_error').html("Please enter valid Number of Transaction");
        err = err+1;
      }else
      {
        $('#number_of_transaction_error').html(" ");
      }

      if($('#transaction_period').val() == "" || isNaN($('#transaction_period').val()))
      {
        $('#transaction_period_error').html("Please enter valid Transaction Period");
        err = err+1;
      }else
      {
        $('#transaction_period_error').html(" ");
      }


      if(err == 0)
      {
        var posturl = "<?php echo url_for('configurationManager/saveCountryValidationDetail'); ?>";
          $.ajax({
               type: "POST",
               url: posturl,
               data: $('#frm_tra_detail').serialize(),
               success: function(msg){
                 if(jQuery.trim(msg) == 'success'){
                    parent.emailwindow.hide();
                    window.parent.location =  "<?php echo url_for('configurationManager/countryPaymentModeValidations?page='.$endLimit.'&mode='.$mode);?>";
                 }
                else if(jQuery.trim(msg) == 'failed'){
                    parent.emailwindow.hide();
                    window.parent.location.reload();
                 }
               }
             });
       
   }else{
          return false;
      }
  }



  </script>