<?php
/**
 * [WP: 106] => CR:151
 * If pos amount doesn't match with application amount then
 * Two button (Pay Application and No thanks) will be displayed...
 */
if(empty($appPOSAmount)){
    $posCheckFlag = false;
    $readonly  = '';
}else{
    $posCheckFlag = true;
    $readonly  = 'readonly';
}
?>
<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
    <div class="content_container">
        <?php use_helper('Form');
        include_partial('global/innerHeading',array('heading'=>'Update Visa On Arrival Application Payment Status'));
        ?>

        <?php
        $sf = sfContext::getInstance()->getUser();
        if($sf->hasFlash('notice')){ ?>
        <div id="flash_notice" class="alertBox" >
            <?php
            echo nl2br($sf->getFlash('notice'));
            ?>
        </div><br/>
        <?php } ?>

        <table width="100%" cellpadding="0" cellspacing="0" border="0">
            <?php echo form_tag('configurationManager/checkVapPaymentStatus',array('name'=>'search_form','class'=>'', 'method'=>'post','id'=>'search_form','onSubmit'=>'return validateForm()')) ?>
            <?php $app_id = (isset($appId)?$appId:"");
            echo formRowComplete('NIS Application Id<span class="red">*</span>',input_tag('app_id',$app_id,array('size' => 24, 'maxlength' => 20, 'class'=>'txt-input', 'readonly' => $readonly))); ?>
            <?php if(!$posCheckFlag){   ?>
            <tr>
                <div class="clear" />
                <td colspan="2" style="padding-right:120px;" align="center"><input type="submit" name="submit" id="btnSubmit" value="Pay Application" class="normalbutton"></td>
            </tr>
            <?php }else{ ?>
            <tr>
                <td colspan="2">
                    <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td colspan="2"><?php echo $errorMsg; ?></td>

                   </tr>
                    <tr>
                        <div class="clear" />

                        <td align="center" colspan="2">

                            <input type="button" name="btnPayApplication" id="btnPayApplication"  value="Pay Application" class="normalbutton">
                            <input type="button" name="noThanks" id="btnNoThanks" value="No, Thanks" class="normalbutton">
                        </td>
                    </tr>

                    </table>
                </td>
            </tr>


            <script>
                $(document).ready(function(){
                    $('#btnPayApplication').click(function(){

                        /* Disabled Pay Application button  */
                        $('#btnPayApplication').val('Please Wait...');
                        $('#btnPayApplication').attr('disabled', 'disabled');

                        /* Disabled No, Thanks button  */
                        $('#btnNoThanks').attr('disabled', 'disabled');                        
                        
                        var url = '<?php echo url_for('configurationManager/applicationPaidProcess?appData='.$appId.'~vap');?>';
                        document.search_form.action = url;
                        document.search_form.submit();
                    });
                    $('#btnNoThanks').click(function(){
                        var url = '<?php echo url_for('configurationManager/checkVapPaymentStatus');?>';
                        //document.search_form.action = url;
                        //document.search_form.submit();
                        window.location.href = url;
                    });
                })
            </script>
            <?php } ?></form>
        </table><br/>
        <div class="clear">&nbsp;</div>
        <?php if($showReceiptFlag){ ?>
        <table align="center" class="nobrd" width ="100%" id="printButtonId">
                <tr>
                    <td align="right" width ="45%"><input type="button" class="normalbutton"  onclick="javascript:window.open('<?= $ackUrl ?>','MyPage','width=750,height=700,scrollbars=1,resizable=yes');"  value="Print Acknowledgement">
                    </td>
                    <td align="left" ><input type="button" class="normalbutton"  onclick="javascript:window.open('<?= $payReceiptUrl ?>','MyPage','width=750,height=700,scrollbars=1,resizable=yes');"  value="Print Receipt">
                    </td>
                </tr>
            </table>
            <?php } ?>


    </div>
</div>
<div class="content_wrapper_bottom"></div>

<script>

    function validateForm(){

        var appId = $('#app_id').val();
        appId = $.trim(appId);
        if(appId == ''){
            alert('Please enter the Application Id.');
            return false;
        }else if(isNaN(appId)){
            alert('Application Id should be numeric only.');
            return false;
        }

        if(confirm('Are you sure?')){
            $('#printButtonId').hide();


            $('#btnSubmit').val('Please Wait...');
            $('#btnSubmit').attr('disabled', 'disabled');


            return true;
        }else{
            $('#btnSubmit').val('Pay Application');
            $('#btnSubmit').attr('disabled', '');
            return false;
        }

    }

</script>














