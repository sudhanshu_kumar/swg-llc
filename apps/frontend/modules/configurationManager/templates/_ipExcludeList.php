 <?php use_helper('Pagination');?>
<div id="nxtPage">
<table width="99%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
    <tr>
            <td class="blbar" colspan="6" align="right"><div class="float_left">IP List</div>
              <span>Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total <b><?php echo $pager->getNbResults(); ?></b> results</span></td>
        </tr>
        <tr>
    <tr>
    <input type="hidden" name="page" id="<?php echo $page; ?>">
    <td class="txtBold">Sr. No</td>
    <td class="txtBold">IP Address </td>
    <td class="txtBold">IP Configuration Date</td>
    <td class="txtBold">IP Updation Date </td>
    <td width="15%" class="txtBold">Action</td>
    </tr>
    <?php if($pager->getNbResults()>0) {
        $i = $pager->getFirstIndice();
        foreach($pager->getResults() as $results) {
    ?>
    <tr id="tr_id_<?php echo $results['id']; ?>">
    <td><?php echo $i; ?></td>
    <td width = "18% " id="ip_<?php echo $results['id']; ?>"> <input type="text" name="validation" readonly="readonly" value="<?php echo $results['ip_address'];?>" /> </td>
    <td><?php echo date_format(date_create($results['created_at']),'Y-m-d'); ?></td>
    <td><?php echo date_format(date_create($results['updated_at']),'Y-m-d'); ?></td>
    <td  valign="top">
    <table width="100%" cellspacing="0" cellpadding="0" style="border:0px;" >
    <tr>
    <td style="border:0px;" id="edit_<?php echo $results['id']; ?>"><span><a  href="javascript:void(0);" onclick="editRow('<?php echo $results['id']; ?>')">Edit</a></span></td>
    <td id="save_<?php echo $results['id']; ?>" class="no_display no_border_padding" ><span><a  href="javascript:void(0);" onclick="saveRow('<?php echo $results['id']; ?>')">Save</a></span></td>
    <td id="cancel_<?php echo $results['id']; ?>" class="no_display no_border_padding" ><span><a  href="javascript:void(0);" onclick="cancelRow('<?php echo $results['id']; ?>')">Cancel</a></span></td>
    <td id="delete_<?php echo $results['id']?>" style="border:0px;"><span><a  href="javascript:void(0);"  onclick="deleteRow('<?php echo $results['id']?>')">Delete</a></span></td>
    </tr>
    </table>
   </td>

    </tr>
    <?php $i++;  } } else { ?>
    <tr> <td colspan="6" align="center" class="txtBold">No Record Found</td></tr>
    <?php } ?>

   <tr id ="lastRow">
    <td class="blbar" colspan="6" height="25px" align="right">
    <div class = "paging pagingFoot" id = "footPaging">
   <?php  echo ajax_pager_navigation($pager, url_for($sf_context->getModuleName().'/ipExcludeView'), 'nxtPage') ?></div></td>
   </tr>
    </table>
    </div>
 <script>
   function addNewIP(){
       $('#addIPLink').hide();
       $('#addIPRow').show();
       if($('#newIP').length > 0){
           $('#newIP').val('');
           $('#newIP').focus();
       }
   }
   
   function saveNewIP(){
       var ipAddress = jQuery.trim($('#newIP').val());
       if(ipAddress == ''){
           alert('Please enter IP Address');
           $('#newIP').focus();
           return false;
       }
       var url = "<?php echo url_for('configurationManager/saveNewIpExcludeConfiguration')?>";
      $.post(url,{ipAddress:ipAddress},function(data){
          $('#addIPRow').hide();
          $('#msgDiv').show();
          $('#msgDiv').html(data);
          $('#msgDiv').hide(5000);
          $('#addIPLink').show();
          getPagingBar();

      });
   }

    function editRow(id){

          $('#addIPRow').hide();
          $('#addIPLink').hide();
          $('#tr_id_'+id+ '> td' ).children('input').attr('class','txt-input');
          $('#tr_id_'+id+ '> td').children('input').attr('readonly','');
          $('#save_'+id).show();
          $('#cancel_'+id).show();
          $('#edit_'+id).hide();
          $('#delete_'+id).hide();
          $('#tr_id_'+id+ '> td' ).children('input').focus();
          
        
    }

    function cancelRow(id){
        $('#save_'+id).hide();
        $('#cancel_'+id).hide();
        $('#edit_'+id).show();
        $('#delete_'+id).show();
        $('#tr_id_'+id+ '> td' ).children('input').attr('class','');
        $('#tr_id_'+id+ '> td').children('input').attr('readonly','readonly');
        $('#addIPRow').hide();
        $('#addIPLink').show();
    }

  function saveRow(id){
      var ipAddress = jQuery.trim($('#ip_'+id+' > input').val());
      if(ipAddress == ''){
          alert('Please enter IP Address');
          $('#ip_'+id+' > input').focus();
          return false;
      }
      if(confirm("Do you realy want to change this value?") == true){
      var url = "<?php echo url_for('configurationManager/updateIpExcludeConfiguration')?>";
     
      $.post(url,{id:id,ipAddress:ipAddress},function(data){ 
          $('#msgDiv').show();
          $('#msgDiv').html(data);
          $('#msgDiv').hide(5000);
          $('#addIPLink').show();
          getPagingBar();

      });
     }
  }

  function deleteRow(id){
      $('#addIPRow').hide();
      $('#addIPLink').hide();
      if(confirm("Do you realy want to delete this entry?") == true){
          var url = "<?php echo url_for('configurationManager/deleteIpExcludeConfiguration')?>";
          $.post(url,{id:id},function(data){
              $('#msgDiv').show();
              $('#msgDiv').html(data);
              $('#msgDiv').hide(5000);
              $('#addIPLink').show();
              getPagingBar(1);
            });
        }
  }

function getPagingBar(deletedFlag){
    var page = "<?php echo $page ?>";
    var url = "<?php echo url_for('configurationManager/ipExcludeView')?>";
    if(deletedFlag == '1'){
          $.post(url,{page:page,deletedFlag:deletedFlag},function(data){
            $('#pagerDiv').html(data);
          });
    }else{
        $.post(url,{page:page},function(data){
            $('#pagerDiv').html(data);
          });
    }


}

</script>