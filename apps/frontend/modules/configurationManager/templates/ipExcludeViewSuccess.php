<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">
    <?php
    use_helper('Pagination');
    use_helper('Form');
    include_partial('global/innerHeading',array('heading'=>'Excluded IP List'));
    ?>
    <div class="clear"></div>
    <div class="tmz-spacer"></div>
    <div id = "msgDiv" align="center" class="txtBold"></div><br/>
    <form name="ipExcludeViewForm" id="ipExcludeViewForm" action="<?php echo url_for('configurationManager/ipExcludeView')?>">
    <table width="100%" cellspacing="0" cellpadding="0">
    <tr id="addIPLink"><td><a href="javascript:void(0)" onclick="addNewIP()" id="">Add New IP</a></td></tr>
    <tr id="addIPRow" class='no_display' >
        <td class="txtBold"  width="150px;">Please enter IP Address</td>
        <td width="200px;"><input style="border:0.2px solid black" class ="txt-input" type="text" name="newIP" id="newIP"  ></td>
        <td width="" align="left">
            <input type="button" name="save" class="normalbutton" value="Save" onclick="saveNewIP()">
            <input type="button" name="save" class="normalbutton" value="Cancel" onclick="$('#addIPRow').hide();$('#addIPLink').show();">
        </td></tr>
    </table><br/>
    <div id="pagerDiv">
    <?php  include_partial('ipExcludeList', array('pager' => $pager,'page'=>$page)) ?>
    </div>
    </form>
  </div>
</div>
<div class="content_wrapper_bottom"></div>


