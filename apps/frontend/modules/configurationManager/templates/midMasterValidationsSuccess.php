


     <div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">
  <?php include_partial('global/innerHeading',array('heading' =>'Mid Validation(s) '));
use_helper('Pagination'); ?>
 <div class="clear"></div>
    <div class="tmz-spacer"></div>
    <table width="100%" border="100" cellpadding="0" cellspacing="0" class="dataTable" id="hdn_datatable">
    <tr>
    <td class="blbar" colspan="8" align="right">
    <div style="float:left">Support Request List</div>
    <span>Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total  <b><?php echo $pager->getNbResults(); ?></b>  results</span></td>
    </tr>
    <tr>
    <td width = "5%"> <span class = "txtBold"> Sr No</span></td>
    <td width = "18%"> <span class = "txtBold"> MID Service </span></td>
    <td width = "18%"> <span class = "txtBold"> Validation(s) </span></td>
    <td width = "18%"> <span class = "txtBold"> Visa Amount Limit($) </span></td>
    <td width = "18%"> <span class = "txtBold"> Master Amount Limit($) </span></td>
    <td width = "18%"> <span class = "txtBold">Cart Amount Limit</span></td>
    <td width = "18%"> <span class = "txtBold">Last Update Date</span></td>
    <td width = "5%"> <span class = "txtBold"> Action </span></td>
    </tr>

<?php $i = $pager->getFirstIndice();
if($pager->getNbResults()>0){
    foreach($pager->getResults() as $list):?>
        <tr id="tr_id_<?php echo $list['id']; ?>">
        <td width = "5%">  <?php echo $i;?></td>
        <td width = "18%" ><?php echo $list['mid_service'];?> </td>
        <td width = "18% " id="validation_<?php echo $list['id']; ?>"> <input type="text" name="validation" readonly="readonly" value="<?php echo $list['validation'];?>" /> </td>
        <td width = "18%" id="visa_<?php echo $list['id']; ?>"> <input type="text" name="visa" readonly="readonly" value="<?php echo $list['visa_amount_limit'];?>"  /> </td>
        <td width = "18%" id="master_<?php echo $list['id']; ?>"> <input type="text" name="master" readonly="readonly" value="<?php echo $list['master_amount_limit'];?>" /> </td>
        <td width = "18%" id="cart_<?php echo $list['id']; ?>"> <input type="text" name="cart" readonly="readonly" value="<?php echo $list['cart_amount_limit'];?> " /> </td>
        <td width = "18%">  <?php echo date_format(date_create($list['updated_at']),'d-m-Y');?> </td>
        <td width = "5%"><span><a id="edit_<?php echo $list['id']; ?>" href="javascript:void(0);" onclick="edit('<?php echo $list['id']; ?>')">Edit</a></span>
            <span><a id="save_<?php echo $list['id']; ?>" href="javascript:void(0);" style="display: none" onclick="save('<?php echo $list['id']; ?>')">Save</a></span>
        </td>
        </tr>

    <?php $i++;
    endforeach;?>
    <tr>
    <td class="blbar" colspan="8" height="25px" align="right">
    <div class = "paging pagingFoot">
   <?php echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName())) ?></div></td>
    </tr>

    <?php }else {?>
    <tr><td  align='center' class='error' colspan="8">No MID Available</td></tr>
   <?php } ?>
</table>


</div>
</div>
<div class="content_wrapper_bottom"></div>

<script>
function edit(id){
        if(confirm("Do you realy want to change these values?") == true){
          $('#tr_id_'+id+ '> td' ).children('input').attr('class','txt-input');
          $('#tr_id_'+id+ '> td').children('input').attr('readonly','');
          $('#save_'+id).attr('style','display: block');
          $('#edit_'+id).attr('style','display: none');
        }
        

    }


    function save(id){
        var validation = $('#validation_'+id+' > input').val();
        var visa = $('#visa_'+id+' > input').val();
        var master = $('#master_'+id+' > input').val();
        var cart = $('#cart_'+id+' > input').val();
        <?php
         $saveUrl = url_for('configurationManager/saveMidValidation');
        ?>

       var err  = 0;
       var reg = /^[\-0-9a-zA-Z\,]+$/;
       if(validation == "" || reg.test(validation) == false)
            {
                alert('Please enter a valid value for Validation(s)');
                err = err+1;
            }
       if(isNaN(visa)){
               alert('Please enter a valid numeric value for Visa Amount Limit');
                err = err + 1;
            }
       if(isNaN(master)){
               alert('Please enter a valid numeric value for Master Amount Limit');
                err = err + 1;
            }
       if(isNaN(cart)){
               alert('Please enter a valid numeric value for Cart Amount Limit');
                err = err + 1;
            }
            if(err != 0)
            {
                return false;
            }
            else{
       var saveUrl = "<?php echo $saveUrl;?>"
         $.post(saveUrl,{id:id,validation :validation,visa:visa,master:master,cart:cart}, function(data){
            alert(data); });

        $('#tr_id_'+id+ '> td' ).children('input').attr('class','');
        $('#tr_id_'+id+ '> td').children('input').attr('readonly','readonly');
        $('#save_'+id).attr('style','display: none');
        $('#edit_'+id).attr('style','display: block');
        }

    }
</script>