<?php

/**
 * configurationManager actions.
 *
 * @package    ama
 * @subpackage configurationManager
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class configurationManagerActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
    public function executeIndex(sfWebRequest $request)
    {
        //    $this->forward('default', 'module');
    }
    public function executeView(sfWebRequest $request){
        $pagingBar = $request->getParameter('pagingBar');
        $allRecordQuery = Doctrine::getTable('CountryBasedRegisterCard')->getTotalRecord();

        $this->page = 1;
        if($request->hasParameter('page')) {
            $this->page = $request->getParameter('page');
        }
        $this->pager = new sfDoctrinePager('CountryBasedRegisterCard',sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($allRecordQuery);
        $this->pager->setPage($this->getRequestParameter('page',$this->page));
        $this->pager->init();
        // For ajax paging only
        if(isset($pagingBar) && $pagingBar != ''){
            sfContext::getInstance()->getConfiguration()->loadHelpers('Url');
            sfContext::getInstance()->getConfiguration()->loadHelpers('Pagination');
            $firstRecord = $this->pager->getFirstIndice();
            $lastRecord = $this->pager->getLastIndice();
            $totalRecord = $this->pager->getNbResults();
            $footPaging = ajax_pager_navigation($this->pager, url_for($this->getModuleName().'/viewAjax'),'nxtPage') ;
            $totalData = '';
            $totalData .= $firstRecord.'####'.$lastRecord.'####'.$totalRecord.'####'.$footPaging;

            return $this->renderText($totalData);

        }

    }

    public function executeViewAjax(sfWebRequest $request){
        $pagingBar = $request->getParameter('pagingBar');
        $allRecordQuery = Doctrine::getTable('CountryBasedRegisterCard')->getTotalRecord();

        $this->page = 1;
        if($request->hasParameter('page')) {
            $this->page = $request->getParameter('page');
        }
        $this->pager = new sfDoctrinePager('CountryBasedRegisterCard',sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($allRecordQuery);
        $this->pager->setPage($this->getRequestParameter('page',$this->page));
        $this->pager->init();
        //For ajax paging only
        if(isset($pagingBar) && $pagingBar != ''){
            sfContext::getInstance()->getConfiguration()->loadHelpers('Url');
            sfContext::getInstance()->getConfiguration()->loadHelpers('Pagination');
            $firstRecord = $this->pager->getFirstIndice();
            $lastRecord = $this->pager->getLastIndice();
            $totalRecord = $this->pager->getNbResults();
            $footPaging = ajax_pager_navigation($this->pager, url_for($this->getModuleName().'/'.$this->getActionName()),'nxtPage') ;
            $totalData = '';
            $totalData .= $firstRecord.'####'.$lastRecord.'####'.$totalRecord.'####'.$footPaging;

            return $this->renderText($totalData);

        }

    }

    public function executeRemoveCountryCardRow(sfWebRequest $request){
        $id = $request->getParameter('id');
        $isRemoved = Doctrine::getTable('CountryBasedRegisterCard')->removeRowById($id);
        if($isRemoved){
            return $this->renderText(true);
        }else {
            return $this->renderText(false);
        }
    }

    public function executeRemoveMultipleRow(sfWebRequest $request){
        $chkid = ltrim($request->getParameter('chkid'),',');
        $idArray = explode(',',$chkid);
        $isRemoved = Doctrine::getTable('CountryBasedRegisterCard')->reomveMultipleRow($idArray);
        if($isRemoved){
            return $this->renderText(true);
        }else{
            return $this->renderText(false);
        }
    }

    public function executeAddNewCountry(sfWebRequest $request) {

        $countryArray = Doctrine::getTable('Country')->getCountryForValidation();

        // find country already present in CountryPaymentMode table;
        $presentCountry = Doctrine::getTable('CountryBasedRegisterCard')->findAll()->toArray();

        foreach($presentCountry as $country){
            if($country['country_code'] == 'ALL'){
                $allFlag = true;
            }
        }

        // making all country array
        $alreadyPresentCountry = array();
        // if entries are already present in country_based_register_card table
        if(count($presentCountry) > 0){
            for($i=0;$i<count($presentCountry);$i++){
                $alreadyPresentCountry[$presentCountry[$i]['country_code']] = $presentCountry[$i]['country_code'] ;
            }
            //$alreadyPresentCountry = array_diff($alreadyPresentCountry,$allArray);
            foreach($alreadyPresentCountry AS $key => $value){
                if($value == 'ALL'){
                    continue;
                }else{
                    if(array_key_exists($key, $countryArray)){
                        unset($countryArray[$key]);
                    }
                }
            }
            if(!$allFlag){
                array_unshift($countryArray, 'ALL');
            }
            $this->finalCountryArray = $countryArray;
        }else{
            array_unshift($countryArray, 'ALL');
            $this->finalCountryArray = $countryArray;
        }


    }

    public function executeSaveNewCountry(sfWebRequest $request){
        $country_code  = $request->getParameter('countryCode');
        $isRegistered = $request->getParameter('isRegistered');
        $countryRegisterObj = new CountryBasedRegisterCard();

        $countryRegisterObj->setCountryCode($country_code);
        $countryRegisterObj->setIsRegistered($isRegistered);

        $countryRegisterObj->save();
        $id = $countryRegisterObj->getId();
        $totalRecord = Doctrine::getTable('CountryBasedRegisterCard')->findAll()->toArray();
        $totalRecordCount = count($totalRecord);
        $totalData = $id.'^'.$totalRecordCount;
        return $this->renderText($totalData);

    }

    public function executeChangeAllStatus(sfWebRequest $request){
        $id = $request->getParameter('id');
        $isRegistered = $request->getParameter('isRegister');
        $changedFlag = Doctrine::getTable('CountryBasedRegisterCard')->changeAllStatus($id,$isRegistered);
        if($changedFlag){
            return $this->renderText('Status Changed');
        }else {
            return false;
        }

    }

    public function executeMidMasterValidations(sfWebRequest $request)
    {
        $midMasterDetails = Doctrine::getTable('MidMaster')->getMidMasterList();
        $page = 1;
        if ($request->hasParameter('page')) {
            $page = $request->getParameter('page');
        }
        $this->pager = new sfDoctrinePager('MidMasterList',sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($midMasterDetails);
        $this->pager->setPage($this->getRequestParameter('page', $page));
        $this->pager->init();
    }

    public function executeSaveMidValidation(sfWebRequest $request) {

        $id = $request['id'];
        $validation = $request['validation'];
        $visa = $request['visa'];
        $master = $request['master'];
        $cart = $request['cart'];
        $MidValidation = Doctrine::getTable("MidMaster")->setValidations($id,$validation,$visa,$master,$cart);
        return $this->renderText('Values has been changed successfully');
    }
    public function executeCountryPaymentModeValidations(sfWebRequest $request)
    {
        $countryPaymentModeDetails = Doctrine::getTable('CountryPaymentMode')->getCountryPaymentModeList();


        $page = 1;
        if ($request->hasParameter('page')) {
            $page = $request->getParameter('page');
        }
        $mode ='';
        if ($request->hasParameter('mode')) {
            $mode = $request->getParameter('mode');
        }
        $this->pager = new sfDoctrinePager('CountryPaymentModeList',sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($countryPaymentModeDetails);
        $this->pager->setPage($this->getRequestParameter('page', $page));
        $this->pager->init();
        $this->page = $page;

        if($mode == 'add'){
            $startLimit = ((sfConfig::get('app_records_per_page')) * ( $page - 1)) + 1;
            if($startLimit > $this->pager->getFirstIndice() && $page > 1){
                $page = $page - 1;
                $this->redirect('configurationManager/countryPaymentModeValidations?page='.$page);
            }
        }else if($mode == 'edit'){
            $this->redirect('configurationManager/countryPaymentModeValidations?page='.$page);
        }

    }

    public function executeSaveCountryPaymentModeValidation(sfWebRequest $request) {
        $id = $request['id'];
        $cardType = $request['cardtype'];
        $service = $request['service'];
        $validation = $request['validation'];
        $cartCapacity = $request['cartcapacity'];
        $cartAmountCapacity = $request['cartamountcapacity'];
        $numberOfTransaction = $request['numberoftransaction'];
        $transactionPeriod = $request['transactionperiod'];

        $countryValidation = Doctrine::getTable("CountryPaymentMode")->setValidations($id,$cardType,$service,$validation,$cartCapacity,$cartAmountCapacity,$numberOfTransaction,$transactionPeriod);
        return $this->renderText('Values has been changed successfully');

    }

    public function executeAddCountry(sfWebRequest $request){
        $totalRecords = $request->getParameter('totalRecords');
        $this->id = $request->getParameter('id');
        $this->mode = $request->getParameter('mode');
        if($this->mode == "edit"){
            $this->endLimit = $request->getParameter('page');
        }else if($this->mode == "add"){
            $this->endLimit =  ($totalRecords+'1')/(sfConfig::get('app_records_per_page'));
            $this->endLimit = ceil($this->endLimit);
        }

        $this->setLayout(false);
        // find total country from table country
        $countryArray = Doctrine::getTable('Country')->getCountryForValidation();

        // find country already present in CountryPaymentMode table;
        $presentCountry = Doctrine::getTable('CountryPaymentMode')->findAll()->toArray();
        // making all country array
        $alreadyPresentCountry = array();
        // if entries are already present in country_based_register_card table
        if(count($presentCountry) > 0){
            for($i=0;$i<count($presentCountry);$i++){
                $alreadyPresentCountry[$presentCountry[$i]['country_code']] = $presentCountry[$i]['country_code'] ;
            }
            //$alreadyPresentCountry = array_diff($alreadyPresentCountry,$allArray);
            foreach($alreadyPresentCountry AS $key => $value){
                if($value == 'ALL'){
                    continue;
                }else{
                    if(array_key_exists($key, $countryArray)){
                        unset($countryArray[$key]);
                    }
                }
            }
            $finalCountryArray = $countryArray;
        }

        $this->form = new NewCountryForm(NULL, array('countryId' => $finalCountryArray));
        if($this->mode == 'edit'){

            //set default
            $arrValidationDetails = Doctrine::getTable('CountryPaymentMode')->find($this->id);
            $arrCardType = explode(',',$arrValidationDetails->getCardType());
            $serviceArr = explode(',',$arrValidationDetails->getService());
            if(strpos($arrValidationDetails->getValidation(),',')){
                $arrVlaidation = explode(',',$arrValidationDetails->getValidation());
            }else{
                $arrVlaidation = $arrValidationDetails->getValidation();
            }
            $this->form->setDefault('validation', $arrVlaidation);
            $this->form->setDefault('cart_capacity', $arrValidationDetails->getCartCapacity());
            $this->form->setDefault('card_type', $arrCardType);
            $this->form->setDefault('service', $serviceArr);
            //$this->form->setDefault('country_code', $arrValidationDetails->getCountryCode());
            $this->form->setDefault('cart_amount_capacity', $arrValidationDetails->getCartAmountCapacity());
            $this->form->setDefault('number_of_transaction', $arrValidationDetails->getNumberOfTransaction());
            $this->form->setDefault('transaction_period', $arrValidationDetails->getTransactionPeriod());
            $this->countryId = $arrValidationDetails->getCountryCode();
        }

    }

    public function executeSaveCountryValidationDetail(sfWebRequest $request){
        $this->setLayout(false);
        $this->setTemplate(NULL) ;
        $postArray = $request->getPostParameters();
        // WP[90],Gaurav Sharma,Service Values Saved with comma seperated value //
        $serviceArr=$postArray['service'];
        $postArray['service']=implode(',',$serviceArr);
        if(!empty ($postArray['country_code'])){
            $countryCode = $postArray['country_code'];
            $checkCountryExist = Doctrine::getTable("CountryPaymentMode")->checkCountryCode($countryCode);
            if($checkCountryExist == 0){
                $this->saveData($postArray);
                sfContext::getInstance()->getUser()->setAttribute('sessMsg', 'Validation(s) successfully added.');
                return $this->renderText('success');
            }else{
                sfContext::getInstance()->getUser()->setAttribute('sessMsg', 'Validation(s) not added due to some problem.');
                return $this->renderText('failed');
            }
        }else {
            $this->saveData($postArray);
            sfContext::getInstance()->getUser()->setAttribute('sessMsg', 'Values has been changed successfully');
            return $this->renderText('success');
        }



    }

    private function saveData($postArray)
    {
        //card type array
        $strCardType = '';
        for($i=0;$i<count($postArray['card_type']);$i++){
            $strCardType .= $postArray['card_type'][$i] .',';
        }
        //validation array
        $strValidation = '';
        for($i=0;$i<count($postArray['validation']);$i++){
            $strValidation .= $postArray['validation'][$i] .',';
        }

        if(empty($postArray['hdn_id'])){
            $countryPaymentModeAdd =new CountryPaymentMode();
            $countryPaymentModeAdd->setCountryCode($postArray['country_code']);
            $countryPaymentModeAdd->setCardType(substr($strCardType,0,-1));
            $countryPaymentModeAdd->setService($postArray['service']);
            $countryPaymentModeAdd->setValidation(substr($strValidation,0,-1));
            $countryPaymentModeAdd->setCartCapacity($postArray['cart_capacity']);
            $countryPaymentModeAdd->setCartAmountCapacity($postArray['cart_amount_capacity']);
            $countryPaymentModeAdd->setNumberOfTransaction($postArray['number_of_transaction']);
            $countryPaymentModeAdd->setTransactionPeriod($postArray['transaction_period']);
            $countryPaymentModeAdd->save();

        }else{

            $countryValidation = Doctrine::getTable("CountryPaymentMode")->setValidations($postArray['hdn_id'],substr($strCardType,0,-1),$postArray['service'],
                substr($strValidation,0,-1),$postArray['cart_capacity'],$postArray['cart_amount_capacity'],$postArray['number_of_transaction'],$postArray['transaction_period']);
        }
    }

    public function executeDeleteCountryPaymentModeValidation(sfWebRequest $request)
    {
        $id = $request['id'];
        $removeValidationId = Doctrine::getTable("CountryPaymentMode")->removeValidations($id);
        if($removeValidationId){
            sfContext::getInstance()->getUser()->setAttribute('sessMsg', 'Validation(s) removed successfully.');
            return $this->renderText('Validations removed successfully');
        }else {
            sfContext::getInstance()->getUser()->setAttribute('sessMsg', 'Validations not removed due to some problem.');
            return $this->renderText('Validations not removed due to some problem');
        }

    }

    public function executeIpExcludeView(sfWebRequest $request){

        $ipRecordQuery = Doctrine::getTable('IpExcludeConfiguration')->getTotalIpRecordQuery();

        if($request->hasParameter('page')) {
            $this->page = $request->getParameter('page');
            if($request->hasParameter('deletedFlag')){
                $allIpList = Doctrine::getTable('IpExcludeConfiguration')->findAll()->toArray();
                $totalCount = count($allIpList );
                $pagePerRecord = sfConfig::get('app_records_per_page');
                if($totalCount%$pagePerRecord == '0'){
                    if($this->page != '') {
                        $this->page = $this->page - 1;
                    }
                }
            }

            $this->setTemplate('ipExcludeViewAjax');
        }else {
            $this->page = 1;
            //            $this->pager = new sfDoctrinePager('IpExcludeConfiguration',sfConfig::get('app_records_per_page'));
            //            $this->pager->setQuery($ipRecordQuery);
            //            $this->pager->setPage($this->getRequestParameter('page',$this->page));
            //            $this->pager->init();
            $this->setTemplate('ipExcludeView');
        }
        $this->pager = new sfDoctrinePager('IpExcludeConfiguration',sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($ipRecordQuery);
        $this->pager->setPage($this->page);
        $this->pager->init();
    }

    public function executeUpdateIpExcludeConfiguration(sfWebRequest $request){
        $id = $request->getParameter('id');
        $newIp = $request->getParameter('ipAddress');
        $ipExcludeObj = Doctrine::getTable('IpExcludeConfiguration')->find($id);
        $ipExcludeObj->setIpAddress($newIp);
        if(!$ipExcludeObj->save()){
            return $this->renderText('IP has been updated successfully.');
        }else {
            return $this->renderText('Problem in saving data ,Please try again...');

        }
    }

    public function executeDeleteIpExcludeConfiguration(sfWebRequest $request){
        $id = $request->getParameter('id');
        $ipExcludeObj = Doctrine::getTable('IpExcludeConfiguration')->find($id);
        if($ipExcludeObj->delete()){
            return $this->renderText('Row has been deleted successfully.');
        }else{
            return $this->renderText('There is some problem in deleting data');
        }


    }

    public function executeSaveNewIpExcludeConfiguration(sfWebRequest $request){
        $ipAddress = $request->getParameter('ipAddress');
        if($ipAddress != ''){
            $ipAddressObj = new  IpExcludeConfiguration();
            $ipAddressObj->setIpAddress($ipAddress);
            if(!$ipAddressObj->save()){
                return $this->renderText('New IP Address has been saved successfully.');
            }else{
                return $this->renderText('There is some problem in saving data ,please try again later');
            }
        }else{
            return $this->renderText('Please enter IP Address');
        }

    }

    public function executeCurrencyRate(sfWebRequest $request){
        $this->currencyRateArray = Doctrine::getTable('CurrencyRate')->findAll()->toArray();
    }

    public function executeUpdateCurrencyRate(sfWebRequest $request){
        $id = $request->getParameter('id');
        $passportFees = $request->getParameter('passportFees');
        $visaFees = $request->getParameter('visaFees');
        $visaArrivalFees = $request->getParameter('visaArrivalFees');
        $additionalForVap = $request->getParameter('additionalForVap');
        $currencyRateObj =Doctrine::getTable('CurrencyRate')->find($id);
        $currencyRateObj->setPassport($passportFees);
        $currencyRateObj->setVisa($visaFees);
        $currencyRateObj->setVisaArrival($visaArrivalFees);
        $currencyRateObj->setAdditionalChargesForVap($additionalForVap);
        if(!$currencyRateObj->save()){
            $this->getUser()->setFlash('notice','Value Updated Successfully');
            return $this->renderText('success');
        }else {
            $this->getUser()->setFlash('error','There is some problem while updating data ,please try again later..');
            return $this->renderText('failure');

        }
    }
    public function executeDeleteCurrencyRateRow(sfWebRequest $request){
        $id = $request->getParameter('id');
        $currencyRateObj = Doctrine::getTable('CurrencyRate')->find($id);
        if($currencyRateObj->delete()){
            return $this->renderText('Row has been deleted successfully.');
        }else{
            return $this->renderText('There is some problem in deleting data');
        }


    }

  /**
   * [WP: 086] => CR:125
   * @param <type> $request
   *
   */
    public function executeCreditCardCountry(sfWebRequest $request){

        $this->data = '';
        if($request->isMethod('POST')){

            ini_set('memory_limit', '512M');
            set_time_limit(0);

            if($_FILES['countryList']['error'] == 0 ){

                $fileName = explode('.', $_FILES['countryList']['name']);
                $fileExt = end($fileName);

                if(strtolower($fileExt) != 'xls'){
                    $msg  = 'File format not supported.';
                }else{
                    if($_FILES['countryList']['size'] > '1048576'){
                        $msg  = 'File size should not be greater than 1 MB.';
                    }else{
                        $tmpName = $_FILES['countryList']['tmp_name'];

                        error_reporting(E_ALL ^ E_NOTICE);
                        $fileName = $tmpName; //sfConfig::get('sf_root_dir').'/web/uploads/BIN/example123.xls'; //sfConfig::get('sf_root_dir').'/plugins/sfExcelReaderPlugin/data/example.xls';
                        $this->data = new sfExcelReader($fileName);

                        $dataArray = $this->data->sheets[0]['cells'];
                        $unSupportedData = array();
                        
                        if(count($dataArray) > 0){
                            if(strtolower($dataArray[1][1]) == 'bin' && strtolower($dataArray[1][2]) == 'processor bin' && strtolower($dataArray[1][3]) == 'region' && strtolower($dataArray[1][4]) == 'country' && strtolower($dataArray[1][5]) == 'unknown'){

                                $dataArrayCount = count($dataArray);

                                for($i=2;$i<=$dataArrayCount;$i++){

                                    $bin = $dataArray[$i][1];
                                    $processorBin = $dataArray[$i][2];
                                    $country = $dataArray[$i][4];

                                    ## If bin, processor bin and country field not empty then only following process start...
                                    if($bin != '' && $processorBin != '' && $country != ''){

                                        $binObj = Doctrine::getTable('CardCountryList')->findByBin($bin);
                                        if(count($binObj) > 0){
                                            $binTableObj = $binObj->getFirst();
                                        }else{
                                            $binTableObj = new CardCountryList();
                                        }

                                        $binTableObj->setBin($bin);
                                        $binTableObj->setProcessorBin($processorBin);
                                        if($dataArray[$i][3] != ''){
                                            $binTableObj->setRegion($dataArray[$i][3]);
                                        }
                                        $binTableObj->setCountry($country);
                                        if($dataArray[$i][5] != ''){
                                            $binTableObj->setUnknown($dataArray[$i][5]);
                                        }
                                        $binTableObj->save();

                                    }else{
                                        $unSupportedData[$i] = $dataArray[$i];
                                    }
                                    $msg  = 'Data have been imported successfully.';
                                }
                            }else{
                                $msg  = 'Please check your data with sample data.';
                            }
                        }//End of if(count($dataArray) > 0){...

                    }
                }

            }//End of if($_FILES['countryList']['error'] == 0 ){...
            else{
                $msg = 'File contains some errors.';
            }
            
            $this->getUser()->setFlash('notice', $msg);
            $this->redirect('configurationManager/creditCardCountry');

        }//End of if($request->isMethod('POST')){...

        $allRecordQuery = Doctrine::getTable('CardCountryList')->getAllRecords();

        $this->page = 1;
        if($request->hasParameter('page')) {
            $this->page = $request->getParameter('page');
        }
        $this->pager = new sfDoctrinePager('CardCountryList', 20); //sfConfig::get('app_records_per_page')
        $this->pager->setQuery($allRecordQuery);
        $this->pager->setPage($this->getRequestParameter('page',$this->page));
        $this->pager->init();
    }

    public function executeDownloadSampleBinData(sfWebRequest $request) {

        sfContext::getInstance()->getConfiguration()->loadHelpers(array('Asset'));
        $fileName = 'sampleBinData.xls';
        $filePath = addslashes(sfConfig::get('sf_web_dir') . '/uploads/BIN/sampleBinData.xls');
        $this->setLayout(false);
        sfConfig::set('sf_web_debug', false);

        // Adding the file to the Response object
        // $this->getResponse()->setHttpHeader("Content-Length: ",filesize($filePath),TRUE);
        // $this->getResponse()->setHttpHeader('Content-type','application/csv',TRUE);
        $this->getResponse()->setHttpHeader('Content-type', 'application/force-download');
        $this->getResponse()->setHttpHeader('Content-Disposition', "attachment; filename={$fileName}", TRUE);
        $this->getResponse()->sendHttpHeaders();
        $this->getResponse()->setContent(file_get_contents($filePath));
        return sfView::NONE;
    }



  /**
   * [WP: 091] => CR: 131
   * @param <type> $request
   */

  public function executeCheckVapPaymentStatus(sfWebRequest $request){

      /**
        * [WP: 106] => CR:151
        * If pos amount doesn't match with application amount then
        * showing confirmation before updating application on swglobal llc portal...
        */

      $this->showReceiptFlag = false;
      $this->appPOSAmount = '';
      if($request->isMethod('POST')){         

          $this->appId = trim(($request->hasParameter('app_id')) ? $request->getParameter('app_id') : '');

          ## Encrypt application id...
          $encAppId = Settings::encryptInput($this->appId);
          
          ## Setting encrypted application id into session...
          $this->getUser()->setAttribute('appId', $encAppId);
          
          if(empty($this->appId) || $this->appId == ''){
              $this->getUser()->setFlash('notice','Please enter Application Id.');
              $this->redirect('configurationManager/checkVapPaymentStatus');
          }
          
          $vapRecord = Doctrine::getTable('VapApplication')->find($this->appId);
          if(empty($vapRecord)) {
              $this->getUser()->setFlash('notice','This Application does not exist');
              $this->redirect('configurationManager/checkVapPaymentStatus');
          }else if ($vapRecord->getStatus() != "New"){
              $this->getUser()->setFlash('notice','This Application is already in '.$vapRecord->getStatus().' status. Please take print receipt.');
              $this->redirect('configurationManager/checkVapPaymentStatus?appId='.$encAppId);
          }else{
              $from_date = '';
              $to_date = '';
              ## Fetching Application fees...
              $appObj = Doctrine::getTable('VapApplication')->find($this->appId);
              $appAmount = $appObj->getDollarFee();             

              sfContext::getInstance()->getUser()->getAttributeHolder()->remove('success_notice');
              $paymentStatus = PosHelper::getPaymentStatusThroughPos($this->appId,$from_date,$to_date, $appAmount);
              if(count($paymentStatus)){

                  if(isset($paymentStatus['amount'][0]) && $paymentStatus['amount'][0] != ''){
                    if($paymentStatus['amount'][0] < $appAmount){
                        $this->appPOSAmount = 'lesser';
                        $this->errorMsg = 'The application amount is USD '.$appAmount.' and customer has paid USD '.$paymentStatus['amount'][0].' on POS terminal. Are you sure. You really want to update this transaction at SWGLOBAL LLC portal?';                        

                    }else if($paymentStatus['amount'][0] > $appAmount && $paymentStatus['amount'][0] != $appAmount){
                        $this->appPOSAmount = 'greater';
                        $this->errorMsg = 'The application amount is USD '.$appAmount.' and customer has paid USD '.$paymentStatus['amount'][0].' on POS terminal. Are you sure. You really want to update this transaction at SWGLOBAL LLC portal?';
                        
                    }else{
                  $appData = $this->appId.'~vap';                 
                  $this->redirect('configurationManager/applicationPaidProcess?appData='.$appData);
                    }
                }

              }else{
                  $this->getUser()->setFlash('notice','This application is not paid on the POS side.');
                  $this->redirect('configurationManager/checkVapPaymentStatus');
              }
          }
      }
      
      $returnStatus = $request->getParameter('appId');
      if(!empty($returnStatus) &&  $returnStatus != ''){

              $decAppId = Settings::decryptInput($this->getUser()->getAttribute('appId'));
          
              $returnStatus = Settings::decryptInput($returnStatus);

              if($returnStatus == $decAppId){
                  $vapRecordObj = Doctrine::getTable('VapApplication')->find($returnStatus);
                  $refNo = $vapRecordObj->getRefNo();
                  $encriptedRefId = SecureQueryString::ENCRYPT_DECRYPT($refNo);
                  $encriptedRefId = SecureQueryString::ENCODE($encriptedRefId);

                  $this->showReceiptFlag = true;
                  $encriptedAppId = SecureQueryString::ENCRYPT_DECRYPT($returnStatus);
                  $encriptedAppId = SecureQueryString::ENCODE($encriptedAppId);
                  $nisUrl = sfConfig::get("app_nis_portal_url");

                  $this->payReceiptUrl = $nisUrl . 'VisaArrivalProgram/visaArrivalPrintReceipt?visa_app_id=' . $encriptedAppId . '&visa_app_refId=' . $encriptedRefId;
                  $this->ackUrl = $nisUrl . 'VisaArrivalProgram/visaArrivalAcknowledgmentSlip?id=' . $encriptedAppId;
              }else{
                  $errorObj = new ErrorMsg();
                  $errorMsg = $errorObj->displayErrorMessage("E059", '001000');
                  $this->getUser()->setFlash('notice', $errorMsg);
                  $this->redirect('configurationManager/checkVapPaymentStatus');
              }                                   
      }else{
          if($this->appPOSAmount == ''){
              ## Removing application id from session...
              $this->getUser()->getAttributeHolder()->remove('appId');
          }
      }
  }//End of public function executeCheckVapPaymentStatus(sfWebRequest $request){...

  /**
   * [WP: 091] => CR: 131
   * @param <type> $request
   */

  public function executeApplicationPaidProcess(sfWebRequest $request){

        $appVar = $request->getParameter('appData');
        $appDetails[0] = SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT($appVar));
        
        $request->setParameter('appDetails', $appDetails);
        $request->setParameter('posPayment', true);
        $this->forward('passport','applicationPOSPayment');

    }

    /**
   * [WP: 091] => CR: 131
   * @param <type> $request
   */
    public function executeSendPOSPaymentNotify(sfWebRequest $request){
        $requestId = $request->getParameter('requestId');
        $this->request_details = Doctrine::getTable('OrderRequestDetails')->find($requestId);
        $appDetails = CartItemsTransactionsTable::getInstance()->getRetryIdDetailsOnNis($this->request_details->getRetryId());
        if (count($appDetails) == 1) {
            $appId = $appDetails[0]['id'];
            $application_type = strtolower($appDetails[0]['app_type']);
            if ($application_type == 'vap') {

                /**
                 * Inserting application data into transaction service charge table...
                 */

                ## Checking duplicate record for the same request id...
                $serviceObj = Doctrine::getTable('TransactionServiceCharges')->getApplicationDetails($appDetails[0]['id'], $appDetails[0]['app_type'], $requestId);
                if(count($serviceObj) < 1){
                    $serviceChargeObj = new TransactionServiceCharges();
                    $serviceChargeObj->setAppId($appDetails[0]['id']);
                    $serviceChargeObj->setRefNo($appDetails[0]['ref_no']);
                    $serviceChargeObj->setAppType($appDetails[0]['app_type']);
                    $serviceChargeObj->setServiceCharge(sfConfig::get('app_visa_arrival_additional_charges'));

                    ## Fetching Application fees...
                    $appObj = Doctrine::getTable('VapApplication')->find($appDetails[0]['id']);
                    $dollarFees = $appObj->getDollarFee();

                    ## Getting processing country...
                    $processingCountry = $appObj->getProcessingCountryId();

                    $serviceChargeObj->setAppAmount($dollarFees);

                    $serviceChargeObj->setOrderRequestDetailId($requestId);
                    /**
                     * [WP: 112] => CR: 158
                     * Updated payment status...
                     * Setting 0 means payemnt is done.
                     */
                    $serviceChargeObj->setStatus(0);
                    $serviceChargeObj->save();
                }

                /**
                 * Creating Order...
                 */
                $params = $this->createOrder($request, $processingCountry);
                $order_number = $params['orderNumber'];

                $paymentResponseArray = array();
                $paymentResponseArray['response_code'] = 1;
                $paymentResponseArray['response_text'] = 'Approved';
                $paymentResponseArray['card_Num'] = '4444444444444444'; //$params['cardNumber'];
                $paymentResponseArray['card_type'] = 'V';
                $paymentResponseArray['status'] = 1;


                /**
                 * Sending Payment Notification...
                 */
                $payManagerObj = new PaymentManager();
                $orderObj = $payManagerObj->updatePaymentRequestNmi($params['order_id'], $paymentResponseArray);
                $updateOrder = $payManagerObj->updateOrderRequest($params['order_id'], $paymentResponseArray);
                $gateway_id = 10;
                $updateOrder = $payManagerObj->updateSplitAmount($requestId, $gateway_id);
                $result = $payManagerObj->paymentSuccess($order_number);
                $status = "success";

                /**
                 * Inserting Request...
                 */
                $nmiRequestId = $this->saveNmiRequest('', $params['orderNumber'], $params['amount']);

                /**
                 * Insering response...
                 */

                $transactionResponse = array();
                $transactionResponse['result'] = 1;
                $transactionResponse['result'] = 1;
                $transactionResponse['result-text'] = 'Approved';
                $transactionResponse['transaction-id'] = '';
                $transactionResponse['result-code'] = 1;
                $transactionResponse['authorization-code'] = NULL;
                $transactionResponse['avs-result'] = NULL;
                $transactionResponse['action-type'] = 'POS';
                $transactionResponse['amount'] = $params['amount'];
                $transactionResponse['ip-address'] = $params['ip_address'];
                $transactionResponse['industry'] = NULL;
                $transactionResponse['processor-id'] = 0;
                $transactionResponse['currency'] = 'USD';
                $transactionResponse['tax-amount'] = 0;
                $transactionResponse['shipping-amount'] = 0;
                $transactionResponse['billing'][0]['first-name'] = $params['fname'];
                $transactionResponse['billing'][0]['last-name'] = $params['lname'];
                $transactionResponse['billing'][0]['address1'] = NULL;
                $transactionResponse['billing'][0]['city'] = NULL;
                $transactionResponse['billing'][0]['country'] = $processingCountry;
                $transactionResponse['billing'][0]['phone'] = $params['phone'];
                $transactionResponse['billing'][0]['email'] = $params['billing-email'];
                $transactionResponse['billing'][0]['cc-number'] = '444444******4444';
                $transactionResponse['billing'][0]['cc-exp'] = NULL;

                $saveResponse = Doctrine::getTable('EpNmiResponse')->setResponse($transactionResponse, $nmiRequestId, $params['orderNumber'], '');
                
                                
               // WP[094]=> setting data to NIS table for POS report purpose.. 
                
                    $serviceChargeObj = new TransactionServiceChargesNIS();
                    $serviceChargeObj->setAppId($appDetails[0]['id']);
                    $serviceChargeObj->setRefNo($appDetails[0]['ref_no']);
                    $serviceChargeObj->setAppType($appDetails[0]['app_type']);
                    $serviceChargeObj->setServiceCharge(sfConfig::get('app_visa_arrival_additional_charges'));

                    ## Fetching Application fees...
                    $appObj = Doctrine::getTable('VapApplication')->find($appDetails[0]['id']);
                    $dollarFees = $appObj->getDollarFee();
                    $serviceChargeObj->setAppAmount($dollarFees);
                    $serviceChargeObj->setAppConvertAmount('0');
                    $serviceChargeObj->setAppCurrency('1');
                    $serviceChargeObj->setAppConvertServiceCharge('0');
                    $serviceChargeObj->save();

               // end of WP[094]

            }//End of if ($this->application_type == 'vap') {...

            $msg = 'This Application has been successfully paid.';
            $success_notice = sfContext::getInstance()->getUser()->getAttribute('success_notice');
            if($success_notice != ''){
                $msg = $msg . $success_notice;
            }
            $this->getUser()->setFlash('notice',$msg);

            ## Encrypt application id...
            $appId = Settings::encryptInput($appId);

        }//End of if (count($appDetails) == 1) {...
        else{
            $this->getUser()->setFlash('notice','Problem found during process. Please try again.');
            $appId = '';
        }
        
        $this->redirect('configurationManager/checkVapPaymentStatus?appId='.$appId);
    }
    
  /**
   * [WP: 091] => CR: 131
   * @param <type> $request
   */
    
    public function createOrder($request, $processingCountry) {
        $params = array();
        $params['requestId'] = $request->getParameter('requestId');             // order request detail id
        $params['cardNumber'] = '4444444444444444';
        $params['cardType'] = 'V';
        $params['billing-email'] = 'test@test.com';
        $params['ip_address'] = '192.168.10.30';
        $params['fname'] = 'pos test';
        $params['lname'] = 'pos test';
        $params['card_holder'] = 'pos';
        $params['phone'] = '444444444444';
        $concat_str = "~";
        $params['address'] = $concat_str . $processingCountry;
        $params['payorName'] = 'pos';
        $params['card_len'] = strlen($params['cardNumber']);
        $params['card_first'] = substr($params['cardNumber'], 0, 4);
        $params['card_last'] = substr($params['cardNumber'], -4, 4);
        /*
         * [WP: 103] => CR: 148
         * Inserting processing country into ipay4me order...
         */
        $params['processing_country'] = $processingCountry;
        $retArr = $this->saveIp4mOrder($params);

        $params['gateway_id'] = $retArr['gatewayId'];
        $params['amount'] = $retArr['amount'];
        $params['orderNumber'] = $retArr['orderNumber'];
        $params['order_id'] = $retArr['order_id'];

        return $params;
    }
    /**
    * [WP: 091] => CR: 131
    * @param <type> $request
    */
    function saveIp4mOrder($params) {
        $retArr = array();

        ## Getting payment gateway id...
        $intGatewayId = 10;

        $request_details = Doctrine::getTable('OrderRequestDetails')->find($params['requestId']);
        $amount = $request_details->getAmount();
        ## Order no
        $version = 'V1';
        $orderManagerObj = orderServiceFactory::getService($version);
        $orderNumber = $orderManagerObj->getOrderNumber();
        $saveOrderDetail = new ipay4meOrder();
        $saveOrderDetail->setOrderNumber($orderNumber);
        $saveOrderDetail->setGatewayId($intGatewayId);
        $saveOrderDetail->setOrderRequestDetailId($params['requestId']);
        /* [WP: 103] => CR: 148 Inserting processing country */
        $saveOrderDetail->setProcessingCountry($params['processing_country']);
        $saveOrderDetail->setPayorName($params['payorName']);
        $saveOrderDetail->setCardHolder($params['card_holder']);
        $saveOrderDetail->setAddress($params['address']);
        $saveOrderDetail->setPhone($params['phone']);
        $saveOrderDetail->setCardFirst($params['card_first']);
        $saveOrderDetail->setCardLast($params['card_last']);
        $saveOrderDetail->setCardLen($params['card_len']);
        $saveOrderDetail->setCardType($params['cardType']);
        $saveOrderDetail->setCardHash(md5($params['cardNumber']));
        $saveOrderDetail->save();
        $retArr['amount'] = $amount;
        $retArr['orderNumber'] = $orderNumber;
        $retArr['gatewayId'] = $intGatewayId;
        $retArr['order_id'] = $saveOrderDetail->getId();
        return $retArr;
    }
    /**
    * [WP: 091] => CR: 131
    * @param <type> $request
    */
    function saveNmiRequest($transactionId, $orderNumber, $amount)
    {
        $epNmiRequest = new EpNmiRequest();
        $epNmiRequest->setGatewayId(10);
        $epNmiRequest->setOrderId($orderNumber);
        $epNmiRequest->setTransactionId('');
        $epNmiRequest->setAmount($amount);
        $epNmiRequest->save();

        return $epNmiRequest->getId();
    }
}
