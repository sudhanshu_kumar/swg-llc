
<?php  echo ePortal_pagehead(" ",array('class'=>'_form')); ?>
<?php use_helper('Pagination');  ?>



<div class="clearfix">
<div id="nxtPage">

<table width="100%" class="innerTable_content">
  <tr>
    <td class="blbar" colspan="5" align="right">
    <span>Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total  <b><?php echo $pager->getNbResults(); ?></b>  results</span></td>
  </tr>
  <tr>
  <td width="5%"><span class="txtBold">S. No.</span></td>
  <td width="9%"><span class="txtBold">Service Name</span></td>
  <td width="9%"><span class="txtBold">User Name</span></td>
  <td width="9%"><span class="txtBold">Email</span></td>
  <!--  <td width="9%"><span class="txtBold">Block/Unblock</span></td> -->


<?php

$email = $sf_request->getParameter('email');
$userType = $sf_request->getParameter('userType');

//$status_option = $sf_request->getParameter('status_option') == '' ? "BLANK" : $sf_request->getParameter('status_option');
//$selectBy = $sf_request->getParameter('selectBy') == '' ? "BLANK" : $sf_request->getParameter('selectBy');
if(($pager->getNbResults())>0) {
  $limit = sfConfig::get('app_records_per_page');
  $i = max(($page-1),0)*$limit ;
  foreach ($pager->getResults() as $result):
  $i++;

  $userName = $result->getUsername();
  if(strtolower($userType) == 'admin'){
    $serviceName = 'ipay4me';
  }else{
    if(strstr($userName, sfConfig::get('app_openid_url_google_url'))){
      $serviceName= 'google';
    }else if(strstr($userName, sfConfig::get('app_openid_url_yahoo_url')))
    {
      $serviceName= 'yahoo';
    }else
    {
      $serviceName= 'ipay4me - openid';
    }
  }
  ?>
  <tr>
    <td ><?php echo $i;?></td>
    <td ><span><?php echo $serviceName; ?></span></td>
    <td ><span><?php echo ucfirst($result->getUserDetail()->getFirstName()).' '.ucfirst($result->getUserDetail()->getLastName());?></span></td>
    <td><span><?php echo $result->getUserDetail()->getEmail(); ?> </span></td>

    <!--
  <td align="center"><span>
  <?php //if($result->getUserDetail()->getFailedAttempt() == $maxToBlock){
  ?>
  <a class="activeInfo" href="#" title="Unblock" id='act' onclick="activateDeactivate(<?php //echo $result->getUserDetail()->getId(); ?>,'activate',<?php //echo $result->getId(); ?>);"></a>

  <?php
  //echo link_to(' ', 'report/setEwalletBlockUnblock?status=deactivate&id='.$result->getId(), array('method' => 'get',  'class' => 'deactiveInfo', 'title' => 'Deactivate')) ;
  //}else{
  ?>
  <a class="deactiveInfo"  title="Block" id='dact' onclick="activateDeactivate(<?php //echo $result->getUserDetail()->getId(); ?>,'deactivate',<?php //echo $result->getId(); ?>);"> </a>
  <?php
  //echo link_to(' ', 'report/setEwalletBlockUnblock?status=activate&id='.$result->getId(), array('method' => 'get',  'class' => 'activeInfo', 'title' => 'Activate')) ;
  //}
  ?>
  </span>
  </td>
  -->

  </tr>
  <?php endforeach; ?>
  <tr>
    <td class="blbar" colspan="5" height="25px" align="right">
      <?php  echo ajax_pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName().'?userType='.$userType.'&email='.$email), 'listing_div') ?>
    </td>
  </tr>
  <?php }
else { ?>
  <tr><td  align='center' class='error' colspan="5" style="color:red">No Results found</td></tr>
  <?php } ?>
  </tbody>
</table>
</div>
</div>

<script>
  function activateDeactivate(userId,status,sfUid){
    //
    //        var  showStatus;
    //        if(status == 'activate')
    //          showStatus = 'unblock';
    //
    //        if(status == 'deactivate')
    //          showStatus = 'block';
    //
    //
    //        var answer = confirm("Do you want to "+showStatus+" this user.")
    //        if (answer){
    //           var url = "<?php //echo url_for('signup/userListing'); ?>";
    //           var page = "<?php //echo $page; ?>";
    //           var status_option = "<?php //echo $status_option; ?>";
    //           var selectBy = "<?php //echo $selectBy; ?>";
    //
    //           if(status_option == ''){
    //           $.post(url, { id: userId, status: status, page: page, sfUid: sfUid, status_option: status_option, selectBy: selectBy },
    //           function(data){
    //
    //               //$("#flash_notice").html('<span>Bank '+status+' successfully</span>');
    //               $("#listing_div").html(data);
    //               //window.location.href = "URL";
    //               //setTimeout( "refresh()", 2*1000 );
    //           });
    //          }else{
    //           $.post(url, { id: userId, status: status, sfUid: sfUid, status_option: status_option,selectBy: selectBy },
    //           function(data){
    //
    //               //$("#flash_notice").html('<span>Bank '+status+' successfully</span>');
    //               $("#listing_div").html(data);
    //               //window.location.href = "URL";
    //               //setTimeout( "refresh()", 2*1000 );
    //           });
    //          }
    //        }

  }

function refresh(){
  window.location.reload( true );
}

</script>
