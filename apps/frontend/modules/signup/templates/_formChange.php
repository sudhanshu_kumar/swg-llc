<?php include_stylesheets_for_form($form) ?>
<script>

    $(document).ready(function()
    {
        $('#sf_guard_user_password').keypress(function(){

            url ='<?php echo url_for('signup/calculateStrength');?>';
            password=$('#sf_guard_user_password').val();

            $("#strength").load(url, {password: password,username:'',byPass:1 },function (data){
                if(data=='logout'){
                    $("#strength").html('');
                    location.reload();
                } });
        });
    });

</script>
<?php use_stylesheet('/css/sfPasswordStrengthStyle.css'); ?>
<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">
<?php include_partial('global/innerHeading',array('heading'=>'Change Password'));?>
    <div class="clear"></div>
    <div class="tmz-spacer"></div>
    <?php

    $sf = sfContext::getInstance()->getUser();
    if($sf->hasFlash('notice')){
        ?>
    <div id="flash_notice" class="alertBox" >
      <?php
        echo nl2br($sf->getFlash('notice'));
        ?>
    </div>
    <?php
    }
    if($sf->hasFlash('error')){
        ?>
    <div id="flash_notice" class="alertBox" >
      <?php
        echo nl2br($sf->getFlash('error'));
        ?>
    </div>
    <?php
    }
    ?>
    <div class="brdBox">
        <!--<div class="brdBox"> <?php //echo ePortal_pagehead('Change Password'); ?>-->
        <div class="clearfix"/>
        <form action="<?php echo url_for('signup/savechange'); ?>" class="dlForm" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
        <table cellpadding="0" cellspacing="0" border="0" width="99%">
          <tr>
            <td colspan="2" class="blbar">Change Password</td>
          </tr>
        <?php if (!$form->getObject()->isNew()): ?>
                    <input type="hidden" name="sf_method" value="put" />
                    <?php endif; ?>
              <?php //echo $form ?>

              <tr>
                <td width="30%"><?php echo $form['old_password']->renderLabel(); ?><span class="red">*</span></td>
              <td><?php echo $form['old_password']->render(); ?> <br>
              <div class="red" id="first_name_error"> <?php echo $form['old_password']->renderError(); ?> </div></td>
          </tr>
              <tr>
                <td><?php echo $form['password']->renderLabel(); ?><span class="red">*</span></td>
              <td><?php echo $form['password']->render(); ?> <br><span id="strength"></span>
              <div class="red" id="first_name_error"> <?php echo $form['password']->renderError(); ?> </div></td>
          </tr>
              <tr>
                <td><?php echo $form['confirm_password']->renderLabel(); ?><span class="red">*</span></td>
              <td><?php echo $form['confirm_password']->render(); ?> <br>
              <div class="red" id="first_name_error"> <?php echo $form['confirm_password']->renderError(); ?> </div></td>
          </tr>
          <tr><td></td><td> <input type="submit" onclick="" value="Change Password" class="normalbutton"></td></tr>
        </table>
		
        
         
                   
        </form>
	  <div class="clear">&nbsp;</div>
      <table width="99%" cellpadding="0" cellspacing="0" border="0">
            <tbody>
                <!--tr><td colspan="2">      <p class="red">*Required Information</p></td></tr-->
          <tr>
            <td colspan="2"><?php include_partial('global/passwordPolicy') ?></td>
          </tr>
            </tbody>
        </table>
        </form>
    </div>
</div>
</div>
</div>
<div class="content_wrapper_bottom"></div>
