<?php use_helper('Form');?>
<?php use_helper('DateForm');?>
<?php echo ePortal_pagehead(" ",array('class'=>'_form')); ?>
<?php include_partial('global/innerHeading',array('heading'=>'User List'));?>

<div class="global_content4 clearfix">
  <div > <?php //echo ePortal_pagehead('Change Password'); ?>


    <table width="100%">

      <tbody>
        <tr>
          <td>Select User Type<span style="color:red">*</span></td>
          <td>
            <?php echo select_tag('status_option', options_for_select($status_list, '', array('include_custom'=>'Please Select ')));?>
            <div class="red" id="err_status_option"></div>
          </td>
        </tr>
        <tr>
          <td>Email Id</td>
          <td>
            <?php //echo  radiobutton_tag('selectBy', 'user_name', false,array('onclick' => 'setMendetory()')).'&nbsp;&nbsp'.input_tag('uname', '', array('class'=>'txt-input'));?>
            <?php echo  input_tag('email', '', array('class'=>'txt-input'));?>
            <div class="red" id="err_email"></div>
          </td>
        </tr>


        <?php //echo formRowComplete('Select User Type',radiobutton_tag('selectBy', 'user_type', true,array('onclick' => 'unSetMendetory()')).'&nbsp;&nbsp'.select_tag('status_option', options_for_select($status_list, '', array('include_custom'=>'All'))),'','status_option','err_status_option','status_option_row'); ?>

        <?php //echo formRowComplete('Username<sup id=sp></sup>', radiobutton_tag('selectBy', 'user_name', false,array('onclick' => 'setMendetory()')).'&nbsp;&nbsp'.input_tag('uname', '', ''), '', 'username', 'err_username', 'username_row'); ?>
        <tr>
          <td>&nbsp;</td>
          <td><div class="lblButton">
              <?php echo button_to('Search','',array('class'=>'button','onClick'=>'validateRequestForm()')); ?>

            </div>
            <div class="lblButtonRight">
              <div class="btnRtCorner"></div>
          </div></td>
        </tr>
        <tr><td colspan="2"><div id="listing_div" ></div></td></tr>

      </tbody>

    </table>

  </div>
</div>
</div>






<script>
  function setMendetory(){
    $('#sp').html('*');
  }
  function unSetMendetory(){
    $('#sp').html('');
    $("#err_username").html("");
  }


  //  $(document).ready(function(){
  //    $.post('userListing',"", function(data){
  //        if(data == "logout"){
  //          location.reload();
  //        }else{
  //         $("#listing_div").html(data);
  //        }
  //
  //      });
  //    });



  function validateRequestForm(){
   var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

    var err=0;
    $("#err_status_option").html("");
    $("#err_email").html("");
    var email = $('#email').val();
    var status_option = $('#status_option').val();
    var selectBy = '';
    if(status_option == '')
    {
      $("#err_status_option").html("Please Select User Type");
      return false;
    }
    if(email){
        if(reg.test(email) == false) {

         $("#err_email").html("Invalid E-Mail Id");
          return false;
        }
       }
 

    
//    if(email == '')
//    {
//      $("#err_email").html("Please Enter Email Id");
//      return false;
//    }
    //    var selectBy = $("input[@name='selectBy']:checked").val();
    //    if(selectBy == 'user_name' && username == ''){
    //      $("#err_username").html("Please Enter Username");
    //      $("#listing_div").html('');
    //      return false;
    //     }
    //    if(status_option == ''){
    //    if(username == ""){
    //        err = err+1;
    //        $("#err_username").html("Please Enter A Username");
    //    }
    //    else {
    //        $("#err_username").html("");
    //    }
    //
    //    }
    //
    //    if(username == '' && status_option == ''){
    //        $("#err_username").html("Either Enter Username Or Choose An Option To Proceed");
    //    }else{
    //        $("#err_username").html("");
    //    }

    if(err == 0) {
      var url = "<?php echo url_for('signup/userListing'); ?>";
      // $.post(url, { username: username, status_option: status_option, selectBy: selectBy },
      $.post(url, { email: email, userType: status_option},
      function(data){
        if(data == "logout"){
          location.reload();
        }else{
          $("#listing_div").html(data);
        }

      });
    }
  }
</script>