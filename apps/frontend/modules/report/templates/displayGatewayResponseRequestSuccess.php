<?php use_helper('Pagination');

?>
<div class="clearfix">
    <div id="nxtPage">
       <table class="innerTable_content">
  <tr>
    <td class="blbar" colspan="5" align="right">
    <div style="float:left">Response Request Report</div>
    <span>Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total  <b><?php echo $pager->getNbResults(); ?></b>  results</span></td>
  </tr>
  <tr>
      <td width="6%"><span class="txtBold">S. No.</span></td>
      <td width="40%"><span class="txtBold">Response Code</span></td>
      <td width="40%"><span class="txtBold">Response Text</span></td>
      <td width="14%"><span class="txtBold">Count</span></td>
     
  </tr>
 
    <?php
      if(($pager->getNbResults())>0) {
          $limit = sfConfig::get('app_records_per_page');
          $page = $sf_context->getRequest()->getParameter('page',0);
          $i = max(($page-1),0)*$limit ;
    ?>
  
   
   
    <?php
      foreach ($pager->getResults() as $key=>$result):
        $i++;
        $response_code  = $result->getResponseCode();
    ?>
   
   
    <tr>
  
      <td ><?php  echo $i;?></td>
      <td ><?php   echo $response_code;?></td>
      <td ><?php  if(!empty($responseArr[$response_code]))
                      {echo $responseArr[$response_code];
                    }  ?></td>
      <td ><?php

      echo link_to($result->getCountResponse(),'report/detailResponseCount?gateway='.Settings::encryptInput($gateway).'&startDate='.Settings::encryptInput($startDate).'&endDate='.Settings::encryptInput($endDate).'&responseCode='.Settings::encryptInput($response_code), array(
                'popup' => array('Window title', 'width=900,height=545,left=320,top=0,scrollbars=yes',)
            )) ?>
</td>
    </tr>
    <?php endforeach; ?>

 

    <tr>
       
        <td class="blbar" colspan="8" height="25px" align="right">
       
        <?php  echo ajax_pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName()).'?startDate='.$startDate.'&endDate='.$endDate.'&gateway='.$gateway.'&usePager=true','result_div');?></td>
    </tr>
    <?php
    }else { ?>
    <tr><td  colspan="8" align='center' ><div style="color:red"> No record found</div></td></tr>
    <?php } ?>
     
  </table>
  
  </div>
</div>



