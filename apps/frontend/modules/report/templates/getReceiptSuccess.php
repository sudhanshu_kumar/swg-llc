<?php
use_javascript('dhtmlwindow.js');
use_stylesheet('dhtmlwindow.css');
use_javascript('modal.js');
use_stylesheet('modal.css');
?>
<?php
$dollar = 1;
$additional_charges = sfConfig::get('app_visa_arrival_additional_charges');
if($getDetails->getCurrency() == $dollar){
    $totalAmount = $getDetails->getAmount();    
}else{
    $totalAmount = $getDetails->getConvertAmount();    
    $additional_charges = CurrencyManager::getConvertedAmount($additional_charges, $dollar, $getDetails->getCurrency());
}
$currencySymbol = CurrencyManager::currencySymbolByCurrencyId($getDetails->getCurrency());

//echo "<br/>";
$sf = sfContext::getInstance()->getUser();
if($sf->hasFlash('notice')){
  ?>

<div id="flash_notice" class="alertBox" >
  <?php
  echo nl2br($sf->getFlash('notice'));

  ?>
</div>
<div id="Reciept"  class="clear">
<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">
<?php }

else{
?>
<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">
    <?php include_partial('global/innerHeading',array('heading'=>'Receipt'));?>
    <div id="Reciept"  class="clear"> <span class="historyAlign"> <?php echo link_to('Payment History', url_for('report/paymentHistory') , 'class = bluelink');?> > Receipt - <?php echo sfConfig::get('app_swglobal_as_merchant'); //echo $getDetails['Name'].', '.date('M j, Y', strtotime($getDetails['TransactionDate']));?></span> <br/>
      <br/>
  <div id="audit" align="left">
        <div>
          <h2 align="left" class="hd1" >Your order has been sent to <?php echo sfConfig::get('app_swglobal_as_merchant');?>.</h2>
        </div>
    <div class="hd2"><?php echo sfConfig::get('app_swglobal_as_merchant');?> is responsible for charging your order.</div>
    <ul class="ulOrder">
      <li><?php echo date('M j', strtotime($getDetails->getPaidDate()));?>&nbsp;&nbsp;-&nbsp;&nbsp;<?php echo sfConfig::get('app_swglobal_as_merchant');?> received your order.</li>
      <li><?php echo date('M j', strtotime($getDetails->getPaidDate()));?>&nbsp;&nbsp;-&nbsp;&nbsp;You placed an order with <?php echo sfConfig::get('app_swglobal_as_merchant');?> on <?php echo date('F j', strtotime($getDetails->getPaidDate()));?>. A copy of this receipt is sent to your email address.</li>
    </ul>
  </div>
  <table width="99%" cellpadding="3" cellspacing="0">
        <tr class="blbar">
          <td align="left" valign="top" colspan="2">Order Date -
            <?php   echo  date('M j, Y', strtotime($getDetails->getPaidDate()));?>
            <br />
            SW Global LLC Order Number:<?php echo $receiptId ?> </td>
          <td class="brdnone" width="10%"><div class="flR noPrint" ><span onclick="javascript:printMe('Reciept')" ><?php echo image_tag('/images/printer.png');?>&nbsp;Print</span></div>
            <div class="noPrint order_details"><span onclick="confirmViewApp();" >Order Details</span></div></td>
      <!-- <div style="width:150px;cursor:pointer;" class="noPrint"><span onclick="confirmSubmission();" >Contact Support</span></div><div style="width:150px;cursor:pointer;" class="noPrint"><span onclick="confirmViewApp();" >View Application Detail</span></div> -->
    </tr>
    <tr class="blTxt">
      <td width="20%" ><b>Payment Status</b></td>
      <td width="65%"><b>Item</b></td>
      <td width="15%" align="right"><b>Price</b></td>
    </tr>
    <tr>
          <td ><span >
            <?php if($getDetails->getPaymentStatus()==0){echo 'Successful'; }?>
            </span></td>
          <td ><span >
          <?php
          $name = $getDetails->getName();
          if(is_numeric($name))
          {
            ?>
            <b><?php echo $getDetails->getDescription().' - ' ?></b>Number of application(s) for which payment has been made is <?php echo $getDetails->getName() ;?>.<br>
            Your application(s) will be processed further on <?php echo $merchantDetails->getAbbr(); ?> Portal.
          <?php
        }
        else
        {
          ?>
            <b><?php echo $getDetails->getDescription().' - ' ?></b> Payment
            <?php //echo $merchantDetails->getDescription(); ?>
            for customer <?php echo $getDetails->getName(); ?>. <br />
            Your application will be processed further on <?php echo $merchantDetails->getAbbr(); ?> Portal.
          <?php
        }
        ?>
      </span>
            <table>
              <tbody>
                <tr class="sub_bg" width="100%" border="1" cellpadding="2" cellspacing="0">
                        <td >S. No.</td>
                        <td>Application Type</td>
                        <td>Application Id</td>
                        <td>Reference Number</td>
                        <td align="right">Amount(<?php echo html_entity_decode($currencySymbol); ?>)</td>
                        <td>Action</td>
                    </tr>
           <?php
           $i=1;
           foreach ($ipayNisDetailsQuery as $ipayNisDetails):
               $appObj = CartItemsTransactionsTable::getInstance();
              //            echo "<pre>";print_r(get_class_methods($result));die;
              $app['passport_id'] = $ipayNisDetails['passport_id'];
              $app['visa_id'] = $ipayNisDetails['visa_id'];
              $app['freezone_id'] = $ipayNisDetails['freezone_id'];
              $app['visa_arrival_program_id'] = $ipayNisDetails['visa_arrival_program_id'];

              $appDetails = $appObj->getApplicationDetails($app);              
              $appAmount = $appDetails['amount'];
              $encryptAapplicationId = SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT($appDetails['id']));
              if($appDetails['app_type']=='passport')
              {
                  /**
                   * [WP: 096]
                   */
                  ##$printUrl = url_for("passport/showPrintSuccess?id=".SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT($appDetails['id'])));
                  $printUrl = Functions::printApplicationURL('passport', $encryptAapplicationId);
              }
              if($appDetails['app_type'] =='visa')
              {
                  /**
                   * [WP: 096]
                   */
                  ##$printUrl = url_for("visa/printVisaApp?id=".SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT($appDetails['id'])));
                  $printUrl = Functions::printApplicationURL('visa', $encryptAapplicationId);
              }
              if($appDetails['app_type'] =='vap')
              {
                  $printUrl = url_for("visaArrival/printVisaApp?id=".SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT($appDetails['id'])));
                  $appAmount = $appAmount + sfConfig::get('app_visa_arrival_additional_charges');
              }
              if($appDetails['app_type']=='freezone')
              {
                  if($appDetails['visacategory_id'] == 102){
                   /**
                    * [WP: 096]
                    */
                    ##$printUrl = url_for("visa/printFreezone?id=".SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT($appDetails['id'])));
                    $printUrl = Functions::printApplicationURL('reentryfreezone', $encryptAapplicationId);
                  }else{
                   /**
                    * [WP: 096]
                    */
                    ##$printUrl = url_for("visa/printVisaApp?id=".SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT($appDetails['id'])));
                    $printUrl = Functions::printApplicationURL('entryfreezone', $encryptAapplicationId);
                  }
              }
              //$appAmount = PaymentModeManager::convertRate($appDetails['app_type'], $amount, $processingCountry);


              ## Getting application amount...
              $appAmountDetails = Doctrine::getTable('TransactionServiceCharges')->getApplicationDetails($appDetails['id'], $appDetails['app_type'], $getDetails->getId());

              if(count($appAmountDetails) > 0){
                  if($appAmountDetails[0]['app_currency'] == $dollar){
                      $appAmount = $appAmountDetails[0]['app_amount'];
                      $additional_charges = $appAmountDetails[0]['service_charge'];
                  }else{
                      $appAmount = $appAmountDetails[0]['app_convert_amount'];
                      $additional_charges = $appAmountDetails[0]['app_convert_service_charge'];
                  }
              }else{
                  $additional_charges = sfConfig::get('app_visa_arrival_additional_charges');
              }

              $currencySymbol = PaymentModeManager::currencySymbol($getDetails->getCurrency());




              ?>
                <tr>
                        <td valign="top"><?php echo $i++; ?></td>
                        <td valign="top"><?php if($appDetails['app_type']=='vap'){ echo sfConfig::get('app_visa_arrival_title') ;} else { echo ucwords($appDetails['app_type']); }?></td>
                        <td valign="top"><?php echo $appDetails['id'];?></td>
                        <td valign="top"><?php echo $appDetails['ref_no']; ?></td>
                        <td valign="top" align='right'><?php echo format_amount($appAmount);  ?>
                        <?php if($appDetails['app_type'] =='vap' || $appDetails['app_type'] == 'visa' || $appDetails['app_type'] == 'freezone'){
                            if((int)$additional_charges > 0){
                                //$additional_charges = ($processingCountry == 'GB')?sfConfig::get('app_visa_arrival_additional_pound_charges'):sfConfig::get('app_visa_arrival_additional_charges');
                                //echo '<i><p style="font-size:10px;text-align:right">(Inclusive service charge: '.html_entity_decode($currencySymbol).$additional_charges.')</p></i>';
                                echo Functions::getAdditionalText($additional_charges, $currencySymbol);
                            }
                          }?>
                        </td>
                  <td valign="top" align="center"><span onclick="openPopUp('<?php echo $printUrl; ?>');"><img src="<?php echo image_path('print_icon.png'); ?>" alt="Print" class="hand" title="Print" /></span></td>
              </tr>
              <?php endforeach;?>
             </tbody>
            </table></td>            
      <td align="right" ><?php echo format_amount($totalAmount,1,0,$currencySymbol); ?></td>
    </tr>
    <tr class="blTxt">
      <td >&nbsp;</td>
      <td  align="right" ><b>Total:</b></td>
      <td align="right" class="txtBold"><?php echo format_amount($totalAmount,1,0,$currencySymbol); ?></td>
    </tr>
      </table>
      <br />
  <table width="99%" cellpadding="3" cellspacing="0">
    <tr>
      <td valign="top" width="50%"><span class="txtBold">Purchased from:</span><br/>
            <?php echo html_entity_decode($merchantDetails->getAddress()); //echo "<br/>"; //echo $getDetails['Phone'];echo "<br/>"; echo $getDetails['Email'];  ?> <a href="https://www.swgloballlc.com/">https://www.swgloballlc.com/</a> </td>
      <td colspan="2" valign="top" width="50%"><span class="txtBold">Paid with:</span><br/>
        <?php
        if( 6 == $cardData->getGatewayId())
        {
          echo "Money Order";
        }
        else if( 7 == $cardData->getGatewayId())
        {
          echo "Wire Transfer";
        }
        else
        {
        $cardType = '';
        if(strtolower($cardData->getCardType()) == 'v')
        $cardType = 'VISA';
        else if(strtolower($cardData->getCardType()) == 'm')
        $cardType = 'Master';

        else if(strtolower($cardData->getCardType()) == '')
        $cardType = '';

        else if(strtolower($cardData->getCardType()) == '')
        $cardType = '';


        ?>
        <?php echo $cardType.' xxxx-'.$cardData->getCardLast(); ?><br />
        <?php echo ($cardData->getCardHolder())?$cardData->getCardHolder():"";?><br />
            <?php echo ($cardData->getAddress())?format_address($cardData->getAddress()):""; ?> <?php echo $cardData->getPhone(); ?>
        <?php
      }
      ?>
      </td>
    </tr>
    <tr>  <td align="center" colspan="3">For refunds,please email <a href="mailto:refund@swgloballlc.com">refund@swgloballlc.com</a> or call (877) 693-1919. For help with this order, please <?php echo link_to('Contact Support',url_for('requestRefund/openInfoBox?requestId='.Settings::encryptInput($receiptId))) ?></td></tr>
  </table>
    
  <?php
  }?>
</div>
  </div>
</div>
<div class="content_wrapper_bottom"></div>
<script>

  function confirmSubmission(){
    //    emailwindow=dhtmlmodal.open('EmailBox', 'iframe', '<?php echo url_for('requestRefund/openInfoBox?requestId='.$receiptId); ?>', 'Confirm Refund', 'width=620px,height=450px,center=1,border=0, scrolling=no')
    //
    //    emailwindow.onclose=function(){
    //      var theform=this.contentDoc.forms[0];
    //      var theemail=this.contentDoc.getElementById("emailfield")
    //      if (theemail.value.indexOf("@")==-1){
    //        alert("Please enter a valid email address")
    //        return false
    //      }
    //      else{
    //        document.getElementById("youremail").innerHTML=theemail.value //Assign the email to a span on the page
    //        return true ;
    //      }
    //    }
    window.location = '<?php echo url_for('requestRefund/openInfoBox?requestId='.$receiptId); ?>';

  }

  function confirmViewApp(){
      window.location = '<?php echo url_for('requestRefund/applicationDetails?orderNumber='.Settings::encryptInput($receiptId)); ?>';
  }

   function openPopUp(url){
      url = url + '/mode/cart'
      window.open (url, "mywindow","status=1,toolbar=0,width=1000px,height=800px,location=0,menubar=0,scrollbars=1");
     // alert(url);
    }

</script>
