<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">
<?php
use_helper('Pagination');
use_helper('Form');
include_partial('global/innerHeading',array('heading'=>'Registered Card Report'));
?>
<?php
use_javascript('dhtmlwindow.js');
use_stylesheet('dhtmlwindow.css');
use_javascript('modal.js');
use_stylesheet('modal.css');
?>
    <div class="clear"></div>
    <div class="tmz-spacer"></div>
    <?php
      $sf = sfContext::getInstance()->getUser();
      if($sf->hasFlash('notice')){ ?>
      <div id="flash_notice" class="alertBox" >
        <?php
        echo nl2br($sf->getFlash('notice'));
        ?>
    </div>
    <br/>
      <?php }?>
    <?php
    $sf = sfContext::getInstance()->getUser();
    if($sf->hasFlash('error')){ ?>
      <div id="flash_error" class="alertBox" >
        <?php
        echo nl2br($sf->getFlash('error'));
        ?>
    </div>
    <br/>
      <?php }?>

    <?php echo form_tag('report/registeredCardReport',array('name'=>'card_search_form','class'=>'', 'method'=>'post','id'=>'cardr_search_form','onSubmit'=>'return validateForm()')) ?>
    <table width="99%" cellpadding="0" cellspacing="0" border="0">
        <?php
        $email = isset($email)?$email:'';
        //$email = ($userEmail)?$userEmail:$email;
        $cardFirst= isset($card_first)?$card_first:'';
        $cardLast = isset($card_last)?$card_last:'';

        echo formRowComplete('Email Id<span class="red">*</span><p>(Card Holder Email OR Log on Email)</p>',input_tag('email', $email, array('size' => 40, 'maxlength' => 50,'class'=>'txt-input')));
        ?><tr>
            <td align="center" colspan="2">OR</td>
        </tr>
        <tr><?php echo formRowComplete('Card First<span class="red">*</span>',input_tag('card_first', $cardFirst, array('size' => 25, 'maxlength' => 4,'class'=>'txt-input')));?>
        </tr>
        <tr><?php echo formRowComplete('Card Last<span class="red">*</span>',input_tag('card_last', $cardLast, array('size' => 25, 'maxlength' => 4,'class'=>'txt-input')));?>
        </tr>
        <?php
        echo "<tr>";?>
        <tr>
          <td>&nbsp;</td>
        <td><?php   echo submit_tag('Search',array('class' => 'normalbutton')); ?>
        </td>
        </tr>
      </table>
    <br>
    <?php    
    $colspan = 9;
    ?>
    <?php if($showDataFlag){?>
      <div class="clearfix">
      <div id="nxtPage">
        <table width="99%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
          <tr>
            <td class="blbar" colspan="<?php echo $colspan; ?>" align="right"><div class="float_left">Registered Card Details</div>
              <span>Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total <b><?php echo $pager->getNbResults(); ?></b> results</span></td>
          </tr>
          <tr>
            <!--td width="7%" ><span class="txtBold">S. No.</span></td-->
            <td width="23%"><span class="txtBold">Business Name</span></td>
            <td width="23%"><span class="txtBold">Address</span></td>
            <td width="23%"><span class="txtBold">Card Holder</span></td>
            <td width="15%"><span class="txtBold">Phone</span></td>
            <!--td width="40%"><span class="txtBold">Card Email</span></td-->
            <td width="40%"><span class="txtBold">Log on Email Address</span></td>
            <td width="15%"><span class="txtBold">Card Type</span></td>            
            <td width="15%"><span class="txtBold">Card Number(s)</span></td>
            <td width="5%" title="Approved Transaction(s) by this card."><span class="txtBold">Transaction(s)</span><p>(From last 1 Year)</p></td>
            <td width="5%" title="Approved Transaction(s) by this card."><span class="txtBold">Last Transaction Date</span></td>
          </tr>
          <tbody>
            <?php
            $i = $pager->getFirstIndice();

            if($pager->getNbResults()>0)
            {
              ## Creating start date...
              $start_date = date('Y-m-d', mktime(0, 0, 0, date("m"), date("d")-365, date("Y")));
              
              foreach ($pager->getResults() as $details):

              ## Fetching logged in email address from user id...
              $userObj = Doctrine::getTable('UserDetail')->findByUserId($details['user_id']);              
              $userEmailAddress = (count($userObj) > 0)?$userObj->getFirst()->getEmail():'--';

              ## Fetching all registered card number with user id...
              $associatedCardNumbers = '';
              $applicantVaultObj = Doctrine::getTable('ApplicantVault')->findByUserId($details['user_id']);
              if(count($applicantVaultObj) > 0){
                  foreach($applicantVaultObj as $applicantVault){
                      $cardFirst = $applicantVault['card_first'];
                      $cardLast = $applicantVault['card_last'];
                      $cardlen = $applicantVault['card_len'];
                      $cardHash = $applicantVault['card_hash'];
                      
                      $count = $cardlen - 8;
                      $cardMid = '';
                      for($j = 0; $j < $count; $j++){
                        $cardMid .= 'X';
                      }                      
                      
                      if($cardHash == $details['card_hash']){
                          $associatedCards = '<b>'.$cardFirst . $cardMid . $cardLast.'</b>';
                      }else{
                          $associatedCards = $cardFirst . $cardMid . $cardLast;
                      }
                      $associatedCardNumbers = $associatedCardNumbers.', '.$associatedCards;
                      
                  }//End of foreach($applicantVaultObj as $applicantVault){...
              }//End of if(count($applicantVaultObj) > 0){...

              if($associatedCardNumbers != ''){
                    $associatedCardNumbers = ltrim($associatedCardNumbers, ', ');
              }


              ## Fetching stats...
              $state = (!empty($details['state']))?$details['state']:'';

              ## Fetching country name...
              $countryObj = Doctrine::getTable('Country')->find($details['country']);
              $countryName = (!empty($countryObj))?$countryObj->getCountryName():'--';

              $username = ucfirst($details['first_name']).' '.ucfirst($details['last_name']);

              
              ## Fetching approved order numbers...
              $approvedTransactionsObj = Doctrine::getTable('ipay4meOrder')->getPaidOrderByCardHash($details['card_hash'], '0', $details['user_id'], $start_date);
              $approvedTransactions = count($approvedTransactionsObj);
              if($approvedTransactions > 0){
                  
                  $lastTransactionDate = $approvedTransactionsObj->getFirst()->getUpdatedAt();
              }

              ?>
            <tr>
              <!--td width="7%"><?php //echo $i;?></td-->
              <td width="23%"><span><?php echo $username ?></span></td>
              <td width="23%"><span>
                <?php echo $details['address1'] ?>
                <?php echo ($details['address2'] != '')?'&nbsp;'.$details['address2']:''; ?>&nbsp;
                <?php echo $details['town']; ?>&nbsp;
                <?php echo $state; ?>&nbsp;
                <?php echo $countryName; ?>
              </span></td>
              <td width="23%"><span><?php echo $details['card_holder']; ?></span></td>
              <td width="15%"><span><?php echo $details['phone']; ?></span></td>
              <!--td width="40%"><span><?php //echo $details['email'] ?></span></td-->
              <td width="40%"><span><?php echo $userEmailAddress; ?></span></td>
              <td width="40%"><span><?php echo $details['card_type'] ?></span></td>              
              <td width="15%"><span><?php echo $associatedCardNumbers; ?></span></td>
              <td width="5%"><span>
               <?php if($approvedTransactions > 0){ ?>
                <a href="javascript:void(0);" title="Click to see transactions in details." onclick="showTransactionDetails('<?php echo $i; ?>', '<?php echo $details['card_first']; ?>', '<?php echo $details['card_last']; ?>', '<?php echo $details['card_hash']; ?>', '<?php echo $details['user_id']; ?>', '<?php echo $start_date; ?>');"><?php echo ($approvedTransactions > 0)?$approvedTransactions:'--';?></a>
               <?php } else { ?>
                --
               <?php } ?>
                </span></td>
              <td width="5%"><span><?php echo (isset($lastTransactionDate))?$lastTransactionDate:'--';?></span></td>
            </tr>
            <tr id="tr_<?php echo $i; ?>" style="display:none;">
                <td colspan="<?php echo $colspan; ?>" id="divId_<?php echo $i; ?>">
                    
                </td>
            </tr>
            <?php
            $i++;
            endforeach; ?>
            <tr>
              <td class="blbar" colspan="<?php echo $colspan; ?>" height="25px" align="right"><div class="paging pagingFoot">
                  <?php
                    echo pager_navigation($pager, url_for('report/registeredCardReport').'?email='.Settings::encryptInput($email).'&card_first='.Settings::encryptInput($card_first).'&card_last='.Settings::encryptInput($card_last),'detail_div');
                  ?>
                </div></td>
            </tr>
            <?php } else { ?>
            <tr>
              <td  align='center' class='red' colspan="<?php echo $colspan; ?>">No record found.</td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
        <br>
      </div>
    </div>
    <?php } ?>
  </div>
</div>
<div class="content_wrapper_bottom"></div>
<div id="loading" class="no_display"><?php echo image_tag('/images/ajax-loader_1.gif',array('alt'=>'Loading ...', 'align' => 'absmiddle'));?><br/>&nbsp;&nbsp;Please wait...</div>
<script>
  
  function validateForm(){
       var email = $.trim($('#email').val());
       var card_first = $.trim($('#card_first').val());
       var card_last = $.trim($('#card_last').val());
       if(email != '' && (card_first != '' || card_last != '')){
          alert('Please enter either email or Card details.');
          return false;
       }
       if(email != ''){
               var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
               if(reg.test(email) == false) {
                    alert('Please provide a valid email id');
                    email.focus;
                    return false;
               }
       }else{

           if( (card_first != '' && card_last == '') || (card_first == '' && card_last != '' ) ){
               alert('Please enter complete card details.');
               return false;
           }


           if(card_first != '' ){
                  if(isNaN(card_first)){
                      alert('Card First should be numeric only.');
                      return false;
                  }
                  if(card_first.length > 4){
                        alert('Please Enter only first four card digits.');
                        return false;
                  }
           }
           if(card_last != ''){
              if(isNaN(card_last)){
                  alert('Card Last should be numeric only.');
                  return false;
              }
               if(card_last.length > 4){
                    alert('Please Enter only first four card digits.');
                    return false;
                }
           }
       }  
  }

  function showTransactionDetails(divId, card_first, card_last, card_hash, user_id, start_date){
      
      for(var k = 1; k<= '<?php echo $i; ?>'; k++){
          if(k != divId){
              $('#tr_'+k).hide();
          }
      }
      $('#tr_'+divId).toggle();
      $('#divId_'+divId).html($('#loading').html());
      url = '<?php echo url_for('report/cardDetails'); ?>';
      $.ajax({
            type: "POST",
            url: url,
            data: 'card_first='+card_first+'&card_last='+card_last+'&card_hash='+card_hash+'&user_id='+user_id+'&start_date='+start_date,
            success: function (data) {
                data = jQuery.trim(data);                
                $('#divId_'+divId).html(data);
            }//End of success: function (data) {...
        }); 


  }
</script>
