<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
<div class="content_container">
<?php use_helper('Form'); ?>
<?php use_helper('DateForm'); ?>
<?php // use_helper('DateForm'); ?>
<?php //use_javascript('common');
include_partial('global/innerHeading',array('heading'=>'Revenue Report','class'=>'_form'));?>
<div class="clear"></div>
<div class="tmz-spacer"></div>
  <form name='passportactivitiesadmin' action='<?php echo url_for('report/nisDollarRevenue');?>' method='post' class='dlForm multiForm' onsubmit="return validateForm()">
    <table width="99%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >

     
      <tr>
          <td width="24%">
              Project Name<sup><font color="red">*</font></sup>:
          </td>
          <td width="24%" class="border_none" align="left">
              <?php echo select_tag('project_id', options_for_select($project, $project_id, array('include_custom' => '-- Please Select --')),array("onchange"=>"getAccount('ALL')",'style'=>'width:150px')); ?>
              <div class="red" id="sel_project_err">
              </div>
          </td>
          <td width="24%">
              Account Name<sup><font color="red">*</font></sup>:
          </td >
          <td width="24%" class="border_none" align="left">
          <input type="hidden" id="hdn_account_id" name="hdn_account_id" value="<?php echo $account_id?>">
              <?php
             
              $array = array(''=>'-- Please Select --');
              echo select_tag('account_id', options_for_select($array,$account_id),array('style'=>'width:150px')); ?>
              <div class="red" id="sel_account_err">
              </div>
          </td>

      </tr>

      <tr>
          <td>
              Select Year<sup><font color="red">*</font></sup>:
          </td>
          <td width="24%" class="border_none">

              <?php 
              $arrYear = array('2010'=>'2010','2011'=>'2011','2012'=>'2012','2013'=>'2013','2014'=>'2014','2015'=>'2015','2016'=>'2016','2017'=>'2017','2018'=>'2018','2019'=>'2019','2020'=>'2020');
              echo select_tag('year', options_for_select($arrYear, $year, array('include_custom' => '-- Please Select --')),array('onchange'=>'getMonth("A")','style'=>'width:150px')); ?>
              <div class="red" id="sel_year_err">
              </div>
              <input type="hidden" name="hdn_year" id="" >
          </td>
          <td width="24%" align="">
              Select Month<sup><font color="red">*</font></sup>:
          </td>
          <td width="24%" class="border_none" align="left">
              <?php
             // echo $month;
              $array = array(''=>'-- Please Select --');
              echo select_tag('month', options_for_select($array),array('style'=>'width:150px')); ?>
              <div class="red" id="sel_month_err">
              </div>
          </td>
          <input type="hidden" name="hdn_month" id="hdn_month" value="<?php echo $month;?>" >
      </tr>

      <tr>
        <td colspan="4" align="center">
          <input  type="submit" value="Generate" id="save" name="Save" class="normalbutton" />
        </td>
      </tr>
    </table>

  </form>
<br />
<br />
<?php
if($sf_params->has("month")){
  $month = array('January','February','March','April','May','June','July','August','September','October','November','December') ;
  $title = "Revenue Report for the Month of ".$month[$sf_params->get("month")-1]." ".$sf_params->get("year");
}
if($isfound){ ?>
    <h2><?=$title ?> </h2>
<?php foreach($accountDetail as $key=>$valueBank){
    
    foreach ($dataRevenue as $value){
        
        if($valueBank['id'] == $value['account_id']){    
    ?>

<table width="99%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
<tr>
    <td colspan="2" >
        <b>Bank Name:</b>
    </td>
    <td colspan="4" class="border_none">
        <?php echo $valueBank['bank_name']?>
    </td>
   

</tr>
<tr>
    <td width="5%" colspan="2" >
        <b>Account Name:</b>    </td>
    <td class="border_none" colspan="4">
        <?php echo $valueBank['account_name']?>    </td>
</tr>
<tr>
    <td width="5%" colspan="2">
        <b>Account Number:</b>    </td>
    <td class="border_none" colspan="4">
        <?php echo $valueBank['account_number']?>    </td>
</tr>
<tr>
    <td colspan="2" width="5%">
        <b>Bank Location (City, State and Country):</b>    </td>
    <td colspan="4" class="border_none">
        <?php echo $valueBank['city'] .', '.$valueBank['state'].', '.$valueBank['country']; ?>
    </td>

</tr>
<tr>
    <td class="blbar" colspan=5 align="right">
        <div class="float_left">Details</div>
    </td>
</tr>
  
    <tr >

      <td width="5%"><span class="txtBold">S.No.</span></td>
      <td width="20%"><span class="txtBold">Date</span></td>
      <td width="20%" align="right"><span class="txtBold">Amount ( $ )</span></td>
      <td width="20%" align="right"><span class="txtBold">Commulative Amount ( $ )</span></td>
      <td width="20%"><span class="txtBold">Remark</span></td>

    </tr>

    <!--</thead>-->
    <tbody>
      <?php
      $i = 1;
      $commAmount = 0;
      
        $dateArr = explode(",", $value['total_date']);
        $amountArr = explode(",", $value['total_amount']);
        $remarkArr = explode(",", $value['total_remark']);
        //  echo "<pre>";print_r($dateArr);

        for($j= 0; $j<count($dateArr) ;$j++){?>
      <tr>
        <td><?= $i; ?></td>
        <td><?= date_format(date_create($dateArr[$j]), "F d,Y"); ?></td>
        <td align="right"><?= number_format($amountArr[$j],2); ?></td>
        <td></td>
        <td><?php if($remarkArr[$j]!="0") echo $remarkArr[$j]; else echo "  "; ?></td>
      </tr>
      <?php
      $i++;
    }
    $commAmount = number_format($commAmount + $value['sum'],2);
    ?>
      <tr><td></td><td><b>Total</b></td><td align="right"><b><?= "$ ".number_format($value['sum'],2); ?></b></td><td align="right"><b><?= "$ ".$commAmount; ?></b></td><td></td></tr>
      <?php

     
     ?>
      <!-- <tr><td colspan="3"><b>Total Amount</b></td><td><b></b></td><td>&nbsp;</td></tr>-->
    </tbody>
  </table>
  <br />
  <?php  }




    }}?>
  <br>
  <center class='multiFormNav'>
    <input type='button' value='Export to Excel' class="normalbutton" onclick="window.open('<?php echo _compute_public_path($filename, 'uploads/excel', '', true); ?>');return false;">&nbsp;
  </center>
  <?php //}else if(($month >=11 && $year == '2010') && !$isfound){
  ?>
    <!--<center class='multiFormNav'><font color="red"><h2><?//=//$title ?> </h2>
  Entries not allowed before Dec/2010 </font></center> -->
  <?php }else if(!$isfound && $sf_params->has("month")){?>
  <center class='multiFormNav'><font color="red"><h2><?=$title ?> </h2>
  No record found</font></center>

<?php  } ?>
<br>
</div>
</div><div class="content_wrapper_bottom"></div>
<script type="text/javascript">

window.document.onload = getAccount('ALL'),getMonth();
//window.document.onload = getAccount();
    function getAccount(all){
        
        var project_id = $('#project_id').val();
        var account_id = '';
        var mode = '';
        if($('#hdn_account_id').val() !=''){
            account_id = $('#hdn_account_id').val();
        }
        if(all !=''){
            mode = all;
        }
        var url = "<?php echo url_for('report/projectAccount'); ?>";
        $.post(url, {project_id: project_id,account_id: account_id,mode: mode},function(data){
            $('#account_id').html(data);
        });
    }

  function validateForm(){

      var err  = 0;

      if($('#project_id').val() == "")
      {
          $('#sel_project_err').html("Please select Project Name");
          err = err+1;
      }else
      {
          $('#sel_project_err').html(" ");
      }

      if($('#account_id').val() == "" )
      {
          $('#sel_account_err').html("Please select Account Name");
          err = err+1;
      }else
      {
          $('#sel_account_err').html(" ");
      }
      
      if($('#year').val() == "")
      {
          $('#sel_year_err').html("Please select Year");
          err = err+1;
      }else
      {
          $('#sel_year_err').html(" ");
      }

      if($('#month').val() == "" )
      {
          $('#sel_month_err').html("Please select Month");
          err = err+1;
      }else
      {
          $('#sel_month_err').html(" ");
      }




      if(err == 0)
      {

         return true;
      }else{
          return false;
      }
  }
  function getMonth(){
      var sel_year = $('#year').val();
      var hdn_month = '';
      if($('#hdn_month').val() !=''){
          hdn_month = $('#hdn_month').val();
      }
      
      var url = "<?php echo url_for('report/getMonth'); ?>";
      $.post(url, {sel_year: sel_year,hdn_month: hdn_month},function(data){
          $('#month').html(data);
      });
  }
</script>