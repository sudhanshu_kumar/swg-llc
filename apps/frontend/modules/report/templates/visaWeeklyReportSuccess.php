<?php use_helper('Form'); ?>
<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">
  
<?php include_partial('global/innerHeading',array('heading'=>'Visa Report')); ?>
    <div class="clear"></div>
    <div class="tmz-spacer"></div>
<?php
$currentYear = date('Y');
$yearArray = array();
for($i='2012';$i<=$currentYear;$i++){
    $yearArray[$i] = $i;
}

$startDate = isset($startDate)?$startDate:'';
$endDate = isset($endDate)?$endDate:'';

//$monthArray = array('January'=>'January','February'=>'February','March'=>'March','April'=>'April','May'=>'May','June'=>'June','July'=>'July','August'=>'August','September'=>'September','October'=>'October','November'=>'November','December'=>'December');
//$year = isset($year)?$year:'';
?>

<form action="<?php echo url_for('report/visaWeeklyReport');?>" method="post" onsubmit="return validateForm()">
<table width="99%" border="0" cellpadding="0" cellspacing="0" class="dataTable">
    <tr>
    <td>Start Date<span class="red">*</span></td>
    <td><input type ="text" name="fdate" onfocus="showCalendarControl(fdate)" class="txt-input" readonly="true" id="fdate" value="<?php echo $startDate; ?>"></td>
    </tr>
    <tr>
    <td>End Date<span class="red">*</span></td>
    <td><input type ="text" name="tdate" onfocus="showCalendarControl(tdate)" class="txt-input" readonly="true" id="tdate" value="<?php echo $endDate; ?>"></td>
    </tr>
    <tr>
    <td>Report Type<span class="red">*</span></td>
    <td>
        <input type ="radio" name="reportType" class="txt-input" id="weeklyReport" value="weekly" <?php echo ($reportType == 'weekly')?'checked':''; ?>>&nbsp;Weekly&nbsp;&nbsp;
        <input type ="radio" name="reportType" class="txt-input" id="monthlyReport" value="monthly" <?php echo ($reportType == 'monthly')?'checked':''; ?>>&nbsp;Monthly
    </td>
    </tr>
    <tr>
    <td colspan="2"><span class="red"><strong>Note:</strong></span>&nbsp;&nbsp;&nbsp;Only <strong>3 months</strong> transactions can be serached at a time.</td>
    </tr>
    <tr>
    <td colspan="2" align="center"><input type = "submit"  name="search" value="Search" class="normalbutton"></td>
    </tr>
</table><br/>

</form>

<?php
/**
 * [WP: 112] => CR: 158
 * Created partial for weekly and monthly report...
 */
if($reportType == 'weekly'){
    include_partial('visaWeeklyReport',array('reportArr'=> $reportArr));
}else{
    include_partial('visaMonthlyReport',array('reportArr'=> $reportArr));
}

?>

  </div>
  </div>
  <div class="content_wrapper_bottom"> </div>

  <script>
 function validateForm(){
            var start_date = fdate = $('#fdate').val();
            var end_date = tdate = $('#tdate').val();
            if(fdate == ''){
                alert('Please select Start date.');
                return false;
            }else if(tdate == ''){
                alert('Please select End date.');
                return false;
            }
            var current_date = new Date();
            if(fdate != ''){
              start_date = new Date(start_date.split('-')[0],start_date.split('-')[1]-1,start_date.split('-')[2]);
              if(start_date.getTime()>current_date.getTime()) {
                alert('Start date cannot be greater than today\'s date.');
                return false;
              }
            }
            if(tdate != ''){
              end_date = new Date(end_date.split('-')[0],end_date.split('-')[1]-1,end_date.split('-')[2]);
              if(end_date.getTime()>current_date.getTime()) {
                alert('End date cannot be greater than today\'s date.');
                return false;
              }
            }
            if((fdate != '') && (tdate != '')){
              if(start_date.getTime()>end_date.getTime()) {
                alert('Start date cannot be greater than End date.');
                return false;
              }
            }
            
            var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
            var diffDays = Math.round(Math.abs((start_date.getTime() - end_date.getTime())/(oneDay)));
            if(diffDays > 93){
                alert("You can select only 3 months transactions at a time.");
              return false;
            }
            return true;

 } 
  </script>
