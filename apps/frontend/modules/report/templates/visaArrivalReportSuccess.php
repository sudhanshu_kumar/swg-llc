<?php use_helper('Form'); ?>
<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">
  
<?php include_partial('global/innerHeading',array('heading'=>'Visa On Arrival Report')); ?>
    <div class="clear"></div>
    <div class="tmz-spacer"></div>
<?php
$currentYear = date('Y');
$yearArray = array();
for($i='2012';$i<=$currentYear;$i++){
    $yearArray[$i] = $i;
}

$startDate = isset($startDate)?$startDate:'';
$endDate = isset($endDate)?$endDate:'';

//$monthArray = array('January'=>'January','February'=>'February','March'=>'March','April'=>'April','May'=>'May','June'=>'June','July'=>'July','August'=>'August','September'=>'September','October'=>'October','November'=>'November','December'=>'December');
//$year = isset($year)?$year:'';
?>

<form action="<?php echo url_for('report/visaArrivalReport');?>" method="post" onsubmit="return validateForm()">
<table width="99%" border="0" cellpadding="0" cellspacing="0" class="dataTable">
    <tr>
    <td>Start Date<span class="red">*</span></td>
    <td><input type ="text" name="fdate" onfocus="showCalendarControl(fdate)" class="txt-input" readonly="true" id="fdate" value="<?php echo $startDate; ?>"></td>
    </tr>
    <tr>
    <td>End Date<span class="red">*</span></td>
    <td><input type ="text" name="tdate" onfocus="showCalendarControl(tdate)" class="txt-input" readonly="true" id="tdate" value="<?php echo $endDate; ?>"></td>
    </tr>
    <tr>
    <td colspan="2" align="center"><input type = "submit"  name="search" value="Search" class="normalbutton"></td>
    </tr>
</table><br/>

</form>

<?php if(isset($reportArr)) { ?>
    <table width="99%" border="0">
    <tr>
    <td class="blbar" align="left" colspan="6">Month Wise Visa On Arrival Report </td>
    </tr>
    <tr>    
    <td class="txtBold">Month</td>
    <td class="txtBold">Total Number of Applications</td>
    <td class="txtBold">Application Amount($)</td>
    <td class="txtBold">Service Charge($)</td>
    <td class="txtBold">Total Amount($)</td>
    </tr>
    <?php
    $tmpYear = '';
    if(count($reportArr) > 0) {
    for($i=0;$i<count($reportArr);$i++) {

     if($tmpYear != $reportArr[$i]['year']){ ?>
    <tr bgcolor="#EEEEEE">
        <td class="txtBold" colspan="5">YEAR: <?php echo $reportArr[$i]['year']; ?></td>
    </tr>
         
         
<?php }
     $tmpYear = $reportArr[$i]['year'];
        
?>

    <tr>    
    <td><?php echo $reportArr[$i]['month']; ?></td>
    <td><?php echo ($reportArr[$i]['count'] > 0)?$reportArr[$i]['count']:'--'; ?></td>
    <td><?php echo ($reportArr[$i]['app_amount'] > 0)?number_format($reportArr[$i]['app_amount'],2):'--';?></td>
    <td><?php echo ($reportArr[$i]['service_charge'] > 0)?number_format($reportArr[$i]['service_charge'],2):'--';?></td>
    <td><?php echo ($reportArr[$i]['total_amount'] > 0)?number_format($reportArr[$i]['total_amount'],2):'--';?></td>
    </tr>
    <?php } } else { ?>
    <td align="center" colspan="5">No Record Found</td>
    <?php } ?>
   </table>

<?php }  ?>
  </div>
  </div>
  <div class="content_wrapper_bottom"> </div>

  <script>
 function validateForm(){
            var start_date = fdate = $('#fdate').val();
            var end_date = tdate = $('#tdate').val();
            if(fdate == ''){
                alert('Please select Start date.');
                return false;
            }else if(tdate == ''){
                alert('Please select End date.');
                return false;
            }
            var current_date = new Date();
            if(fdate != ''){
              start_date = new Date(start_date.split('-')[0],start_date.split('-')[1]-1,start_date.split('-')[2]);
              if(start_date.getTime()>current_date.getTime()) {
                alert('Start date cannot be greater than today\'s date.');
                return false;
              }
            }
            if(tdate != ''){
              end_date = new Date(end_date.split('-')[0],end_date.split('-')[1]-1,end_date.split('-')[2]);
              if(end_date.getTime()>current_date.getTime()) {
                alert('End date cannot be greater than today\'s date.');
                return false;
              }
            }
            if((fdate != '') && (tdate != '')){
              if(start_date.getTime()>end_date.getTime()) {
                alert('Start date cannot be greater than End date.');
                return false;
              }
            }
            return true;

 } 
  </script>
