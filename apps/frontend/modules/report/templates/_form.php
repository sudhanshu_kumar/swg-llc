

<?php use_helper('DateForm');?>
<?php use_helper('Form');
 use_helper('Pagination');?>


   
        <?php echo form_tag('report/gatewayResponseRequest',array('name'=>'pfm_response_form','class'=>'', 'method'=>'post','id'=>'pfm_response_form', 'onsubmit'=>'return validateForm();')) ?>
 <div class="clear"></div>
		<div style="display:none;" class='red' id="errMsg"></div>
            <table width="100%" cellpadding="0" cellspacing="0">
	 	
                <?php //echo $form;?>	
			<tr>
			  <td width="20%"><?php echo $form['gateway']->renderLabel(); ?></td>
			  <td width="79%" ><?php echo $form['gateway']->render(); ?>
				<br>
                			<div class="red" id="gateway_error">
			                    <?php echo $form['gateway']->renderError(); ?>
			                </div>
			</td>
			</tr>
				
			<tr>
			  <td width="20%"><?php echo $form['startDate']->renderLabel(); ?></td>
			  <td width="79%" ><?php 
			  echo input_tag('startDate', '', array('size' => 20, 'maxlength' => 10,'class'=>'txt-input','onfocus'=>'showCalendarControl(startDate)','readonly'=>'true')); ?>
				<br>
                			<div class="red" id="startDate_error">
			                    <?php echo $form['startDate']->renderError(); ?>
			                </div>
				</td>
			</tr>
			<tr>
			  <td width="20%"><?php echo $form['endDate']->renderLabel(); ?></td>
			  <td width="79%" ><?php 
			  echo input_tag('endDate', '', array('size' => 20, 'maxlength' => 10,'class'=>'txt-input','onfocus'=>'showCalendarControl(endDate)','readonly'=>'true')); ?>
				<br>
                			<div class="red" id="endDate_error">
			                    <?php echo $form['endDate']->renderError(); ?>
			                </div>
				</td>
			</tr>
	
                <tr>
                    <td>&nbsp;</td>
                    <td><div class="lblButton">
                            <?php   //echo button_to('Search','',array('class' => 'button','onClick'=>'validate_response_form()')); ?>
                            <?php   echo submit_tag('Search',array('class' => 'normalbutton'),array('class' => 'button','onClick'=>'validate_response_form()')); ?>
                           
                        </div>
                        <div class="lblButtonRight">
                            <div class="btnRtCorner"></div>
                    </div></td>
                </tr>
            </table>
            <br />
            <div id="result_div" align="center" style="width:inherit!important;"></div>

        </form>

<script>
    function validateForm(){     

	var startDate = $('#startDate').val();
	var endDate = $('#endDate').val();
	var errMsg = false;
    if($('#report_gateway').val() == ''){
        $('#gateway_error').html('Please select gateway.');
        errMsg = true;
    }else{
        $('#gateway_error').html('');
    }
   if(startDate == ''){
        $('#startDate_error').html('Please select start date.');
        errMsg = true;
    }else{
        $('#startDate_error').html('');
    }
    if(endDate == ''){
        $('#endDate_error').html('Please select end date.');
        errMsg = true;
    }else{
        $('#endDate_error').html('');
    }
	if(startDate != '' && endDate != '' ){
		stDateArray = startDate.split('-');
		startDate = stDateArray[0]+'/'+stDateArray[1]+'/'+stDateArray[2];

		enDateArray = endDate.split('-');
		endDate = enDateArray[0]+'/'+enDateArray[1]+'/'+enDateArray[2];

		var Date1 = new Date(startDate);
	 	var Date2 = new Date(endDate);

		 if (Date1 > Date2)
		 {
		     $('#startDate_error').html('Start Date can not be greater than End Date.');		   
		     errMsg = true;
		 }else{
             $('#startDate_error').html('');		   
         }
	}

	if(errMsg){

	}else{
		validate_response_form();		
	}
        return false;
    }
</script>






