<?php
use_javascript('dhtmlwindow.js');
use_stylesheet('dhtmlwindow.css');
use_javascript('modal.js');
use_stylesheet('modal.css');

?>

  <div id="nxtPage">
    <table width="99%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
      <tr>
      <td colspan="" ><b>Bank Name:</b> </td>
      <td colspan="" class="border_none"><?php echo $accountDetail[0]['bank_name']?> </td>
      <td  class="border_none" align="right" colspan="2"><span class="hand bluelink" onclick="openBankAccouontWindow('edit')"><u>Edit Account</u></span> </td>
      </tr>
      <tr>
      <td colspan=""><b>Account Name:</b> </td>
      <td colspan="3" class="border_none"><?php echo $accountDetail[0]['account_name']?> </td>
      </tr>
      <tr>
      <td colspan=""><b>Account Number:</b> </td>
      <td colspan="3" class="border_none"><?php echo $accountDetail[0]['account_number']?> </td>
      </tr>
      <tr>
      <td colspan="" width="30%"><b>Bank Location (City, State and Country):</b> </td>
      <td colspan="3" class="border_none"><?php echo $accountDetail[0]['city'] .', '.$accountDetail[0]['state'].', '.$accountDetail[0]['country']; ?> </td>
      </tr>
      <tr>
      <td class="blbar" colspan=6 align="right"><div class="float_left">Details</div></td>
      </tr>
      <tr >
        <td width="25%" align="left"><span class="txtBold">Date</span></td>
        <td  width="25%" align="right"><span class="txtBold">Amount ( $ )</span></td>
        <td width="25%" align="left"><span class="txtBold">Remarks</span></td>
        <td width="25%" align="left"><span class="txtBold">Action</span></td>
      </tr>
      <tbody>
        <?php
        $month =$sf_request->getParameter('sel_month');
        $year = $sf_request->getParameter('sel_year');

        if(count($arrRevDetail)>0)
        {

          $allid='';
          foreach ($arrRevDetail as $result):  ?>
          <?php if(($result['date'] == date('Y-m-d')) && $result['id'] == ''){?>
        <tr>
          <td width="25%" align="left"><?php echo date("F d, Y",strtotime($result['date']));?>
            <input type="hidden" name="date_update" id="date_update" value="<?php echo $result['date'];?>" />
            <input type="hidden" name="hdn_id" id="hdn_account_new" value="<?php echo $accountDetail[0]['id'];?>" />
          </td>
          <td width="25%" align="right"><input class="txt-input" maxlength="10" size="10" type="text"  name="amount_update" id="amount_update" value="<?php echo $result['amount']?>"></td>
        <td width="25%" align="left"><input type="checkbox"  <?php if($result['remark']=='Holiday'){ echo 'checked=checked';}?> name="remark_update" id="remark_update" value="Holiday"  onclick="getAmount(this.id)">
          Holiday</td>
        <td width="25%" align="left"></td>
        </tr>
        <tr>
        <td colspan="6" align="center"><div id="postBtn">
            <input  type="button" value="Save" id="save" name="Save" class="normalbutton" onclick="postData();"/>
          </div></td>
        </tr>
  <?php }else{ ?>
        <tr>
          <td width="25%" align="left" colspan=""><?php echo date("F d, Y",strtotime($result['date']))?></td>
          <td width="25%" align="right"><?php echo $result['amount'];?></td>
          <?php if($result['remark']=='Holiday'){ ?>
          <td width="25%" align="left">Holiday</td>
          <?php } else{?>
          <td width="25%" align="left">--</td>
          <?php }?>
        <?php if($month == date('m') && $result['action'] == ''){?>
        <td width="25%" align="left"><span class="bluelink" onclick="openEditWindow('<?php echo $result['date'];?>','<?php echo $accountDetail[0]['id'];?>','edit')"><img src="<?php echo image_path('edit_icon.png'); ?>" alt="Edit" title="Edit" /></span> </td>
          <?php }else if($result['action'] == 'Approved'){ ?>
        <td width="25%" align="left"><span class="bluelink" onclick="openEditWindow('<?php echo $result['date'];?>','<?php echo $accountDetail[0]['id'];?>','edit')">Approved for Edit</span> </td>
          <?php }else if($result['action'] == 'Declined'){ ?>
        <td width="25%" align="left"><span class="bluelink" onclick="openEditWindow('<?php echo $result['date'];?>','<?php echo $accountDetail[0]['id'];?>','edit')">Declined for Edit</span> </td>
          <?php }else{ ?>
        <td width="25%" align="left"><span class="bluelink" onclick="openRequestEditWindow('<?php echo $result['date'];?>','<?php echo $accountDetail[0]['id'];?>','<?php echo $accountDetail[0]['project_id'];?>')">Request to Edit</span> </td>
          <?php } ?>
        </tr>
    <?php }

  endforeach; ?>
  <?php } else { ?>
      <tr>
        <td  align='center' class='error' colspan="4">Entries not allowed before Dec/2010</td>
      </tr>
        <?php } ?>
      </tbody>
    </table>
    <br>
  </div>
  <?php
  ?>
<script>

  function postData(){
    //var hdn_id = $('#hdn_id').val();
    var amount_update = $('#amount_update').val();

    var hdn_account = $('#hdn_account_new').val();

    var remark_update = '' ;
    if($("input:checked").length == 1){
      remark_update = $('#remark_update').val();
    }

    var date_update = $('#date_update').val();
    $('#postBtn').html('<?php echo image_tag('../images/ajax-loader.gif'); ?>');
    $("#postBtn").show();
    $.post("<?php echo url_for('report/nisDollarUpdate') ?>", {amount_update: amount_update, remark_update: remark_update,date_update: date_update,hdn_account: hdn_account},
    function(){
      $("#postBtn").hide();
      reloadWindow();

    }
  );
  }

  function reloadWindow() {
    var project_id = parent.document.getElementById('project_id').value;
    var account_id = parent.document.getElementById('account_id').value;
    var selmonth = parent.document.getElementById('sel_mon').value;
    var selyear = parent.document.getElementById('sel_year').value;
    var url = "<?php echo url_for('report/nisDollarReportList'); ?>";
    $.post(url, {sel_month: selmonth,sel_year: selyear,project_id: project_id,account_id: account_id},function(data){
      $('#entry').html(data);
    });
  }

  function openEditWindow(revDate,accountId){

    accountId = '&accountId='+accountId;
    emailwindow = dhtmlmodal.open('EmailBox', 'iframe', '<?php echo url_for('report/nisRevDetail').'?date='?>' + revDate +accountId, 'Revenue Transaction', 'width=620px,height=250px,center=1,border=0, scrolling=no')
  }

  function openRequestEditWindow(revDate,accountId,projectId){

    accountId = '&accountId='+accountId;
    projectId = '&projectId='+projectId;
    emailwindow = dhtmlmodal.open('EmailBox', 'iframe', '<?php echo url_for('report/nisRequestEdit').'?date='?>' + revDate +accountId + projectId, 'Revenue Transaction', 'width=620px,height=250px,center=1,border=0, scrolling=no')
  }


  function getAmount(chkId){
    //alert(chkId);
    if(document.getElementById(chkId).checked == true){
      document.getElementById('amount_update').value = 0;
      document.getElementById('amount_update').readOnly = true;

    }else{
      document.getElementById('amount_update').value = '0.00';
      document.getElementById('amount_update').readOnly = false;
    }

  }
</script>
