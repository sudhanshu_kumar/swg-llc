<?php use_helper('Pagination');
use_helper('Form');

include_partial('global/innerHeading',array('heading'=>'Report'));?>
<div class="global_content4 clearfix">
<div class="brdBox">
<div class="clearfix"/>
<?php //echo form_tag($sf_context->getModuleName().'/display','name=search id=search');?>
<?php echo form_tag('report/reportSearch',array('name'=>'ewallet_report_form','class'=>'', 'method'=>'post','id'=>'ewallet_report_form')) ?>
<div style="border:1px solid #CCCCCC;width:500px;height:320px;"><br>
<div style="border:1px solid #CCCCCC;width:450px;height:50px;margin-left:10px">
<table width="80%" style="margin-left:15px;margin-top:5px;">
<tr>
    <th width="29%"><?php echo $reportForm['gateway']->renderLabel(); ?></td>
    <td>
        <?php echo $reportForm['gateway']->render(); ?>
        <?php echo $reportForm['gateway']->renderError(); ?>
    </td>
</tr>
</table>
</div>
<div>&nbsp;</div>
<div style="border:1px solid #CCCCCC;width:450px;height:200px;margin-left:10px;">
    <div style="border:0px solid #CCCCCC;width:400px;height:50px;margin-left:10px;margin-top:5px;">
        <table width="80%" style="margin-left:10px;margin-top:5px;">
            <tr>
                <th><?php echo $reportForm['order_number']->renderLabel(); ?></th>
                <td>
                <?php echo $reportForm['order_number']->render(); ?></td>
            </tr>
        </table>
    </div>
    <div><center>OR</center></div>
    <div style="border:0px solid #CCCCCC;width:400px;height:120px;margin-left:10px">
        <table width="80%" style="margin-left:10px;margin-top:5px;">
            <tr>
                <th><?php echo $reportForm['from_date']->renderLabel(); ?></th>
                <td>
                    <?php echo $reportForm['from_date']->render(); ?>
                    <?php echo $reportForm['from_date']->renderError(); ?></td>
            </tr>
            <tr>
                <th><?php echo $reportForm['to_date']->renderLabel(); ?></th>
                <td>
                    <?php echo $reportForm['to_date']->render(); ?>
                    <?php echo $reportForm['to_date']->renderError(); ?>
                </td>
            </tr>

            <tr>
                <th><?php echo $reportForm['status']->renderLabel(); ?></th>
                <td>
                    <?php echo $reportForm['status']->render(); ?>
                    <?php echo $reportForm['status']->renderError(); ?>
                </td>
            </tr>
        </table>
    </div>


</div>
<div style="border:0px solid #CCCCCC;width:450px;height:50px;margin-left:10px">
    <div class="lblButton">
                    <?php   echo submit_tag('Search',array('class' => 'button','onclick'=>"return validate()")); ?>
                </div>
                <div class="lblButtonRight">
                    <div class="btnRtCorner"></div>
            </div>
</div>


</div>
<br />
<div id="result_div" style="width:500px" align="center"></div>
</div>

<!--div class="wrapForm2">
<table width="50%">
<tr>


<th width="29%"><?php echo $reportForm['gateway']->renderLabel(); ?></td>
<td>
<?php echo $reportForm['gateway']->render(); ?>
<?php echo $reportForm['gateway']->renderError(); ?></td>

</tr>
<tr>
<td colspan="2">
<table width="100%">
<tr>
<th><?php echo $reportForm['order_number']->renderLabel(); ?></th>
<td>
<?php echo $reportForm['order_number']->render(); ?></td>
</tr>
<tr>
<td colspan="2" align="center">OR</td>
</tr>
<tr>
<th><?php echo $reportForm['from_date']->renderLabel(); ?></th>
<td>
<?php echo $reportForm['from_date']->render(); ?>
<?php echo $reportForm['from_date']->renderError(); ?></td>
</tr>
<tr>
<th><?php echo $reportForm['to_date']->renderLabel(); ?></th>
<td>
<?php echo $reportForm['to_date']->render(); ?>
<?php echo $reportForm['to_date']->renderError(); ?>
</td>
</tr>

<tr>
<th><?php echo $reportForm['status']->renderLabel(); ?></th>
<td>
<?php echo $reportForm['status']->render(); ?>
<?php echo $reportForm['status']->renderError(); ?>
</td>
</tr>

</table>


</td>
</tr>
<tr>
<td>&nbsp;</td>
<td><div class="lblButton">
<?php   echo submit_tag('Search',array('class' => 'button','onclick'=>"return validate()")); ?>
</div>
<div class="lblButtonRight">
<div class="btnRtCorner"></div>
</div></td>
</tr>

</table>
<br />
<div id="result_div" style="width:500px" align="center"></div>
</div>


</form>
</div></div>
</div-->
<script>
<?php
if("valid" == 1 )
{
    ?>
        $(document).ready(function()
        {
            validate_search_form();
        });
    <?php
}
?>
    function validate() {
        document.getElementById("result_div").innerHTML='';
        var errmsg = "";
        var flag = "";
        if( (($('#report_order_number').val()==''))){
            flag=1;
            if(($('#report_from_date').val()=="")){
                errmsg+="Select start date\n";
            }
            if(($('#report_to_date').val()=="")){
                errmsg+="Select from date\n";
            }
            if(($('#report_status').val()=="")){
                errmsg+="Select status\n";
            }
            if((document.getElementById('report_from_date').value != '') && (document.getElementById('report_to_date').value != '')){
                var start_date = document.getElementById('report_from_date').value;
                var end_date = document.getElementById('report_to_date').value;
                start_date = new Date(start_date.split('-')[0],start_date.split('-')[1]-1,start_date.split('-')[2]);
                end_date = new Date(end_date.split('-')[0],end_date.split('-')[1]-1,end_date.split('-')[2]);
                if(start_date.getTime()>end_date.getTime()) {
                    errmsg+='From date cannot be greater than To date';
                }
            }
        }else{
            if((document.getElementById('report_from_date').value != '') && (document.getElementById('report_to_date').value != '')){
                var start_date = document.getElementById('report_from_date').value;
                var end_date = document.getElementById('report_to_date').value;
                start_date = new Date(start_date.split('-')[0],start_date.split('-')[1]-1,start_date.split('-')[2]);
                end_date = new Date(end_date.split('-')[0],end_date.split('-')[1]-1,end_date.split('-')[2]);

                if(start_date.getTime()>end_date.getTime()) {
                    errmsg+='From date cannot be greater than To date';
                }
            }
        }

        if(errmsg!="")
        {
            if(flag)
                alert('Please enter Order No Or\n'+errmsg);
            else
                alert(errmsg);
        }
        else{
            validate_search_form();
        }
        return false;
    }
    function validate_search_form(){
        $('#result_div').html('<?php echo image_tag('../images/ajax-loader.gif'); ?>');
        var err = 0;

        if(err == 0){
            $.post('reportStatement',$("#ewallet_report_form").serialize(), function(data){

                if(data == "logout"){
                    location.reload();
                }else{
                    $("#result_div").html(data);
                }
            });
        }


    }


</script>
