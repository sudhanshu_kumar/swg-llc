<table width="100%" align="center" border="1px solid black" cellpadding="0" cellspacing="0" >
    <tr>
        <td class="blbar" colspan="2" align="right"><div class="float_left">Card Transaction Summary</div></td>
    </tr>
    <tr>
        <td><strong>Last One Year</strong></td>
        <td><strong>Last One Month</strong></td>
    </tr>
    <tr>
        <td width="50%">
            <table width="100%" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="20%"><span class="txtBold">Success</span></td>
                    <td width="20%"><span class="txtBold">Failed</span></td>
                    <td width="20%"><span class="txtBold">Refund</span></td>
                    <td width="20%"><span class="txtBold">Charge-Back</span></td>
                    <td width="20%"><span class="txtBold">Total</span></td>
                </tr>
            </table>
        </td>
        <td width="50%">
            <table width="100%" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="20%"><span class="txtBold">Success</span></td>
                    <td width="20%"><span class="txtBold">Failed</span></td>
                    <td width="20%"><span class="txtBold">Refund</span></td>
                    <td width="20%"><span class="txtBold">Charge-Back</span></td>
                    <td width="20%"><span class="txtBold">Total</span></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table width="100%" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="20%"><?php echo $cardReportArray['year']['0'];?></td>
                    <td width="20%"><?php echo $cardReportArray['year']['1'];?></td>
                    <td width="20%"><?php echo $cardReportArray['year']['2'];?></td>
                    <td width="20%"><?php echo $cardReportArray['year']['3'];?></td>
                    <td width="20%"><?php echo $cardReportArray['year']['0']+$cardReportArray['year']['1']+$cardReportArray['year']['2']+$cardReportArray['year']['3'];?></td>
                </tr>
            </table>
        </td>
        <td>
            <table width="100%" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="20%"><?php echo $cardReportArray['month']['0'];?></td>
                    <td width="20%"><?php echo $cardReportArray['month']['1'];?></td>
                    <td width="20%"><?php echo $cardReportArray['month']['2'];?></td>
                    <td width="20%"><?php echo $cardReportArray['month']['3'];?></td>
                    <td width="20%"><?php echo $cardReportArray['month']['0']+$cardReportArray['month']['1']+$cardReportArray['month']['2']+$cardReportArray['month']['3'];?></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="blbar" colspan="2" height="10px" align="right"></td>
    </tr>
</table>