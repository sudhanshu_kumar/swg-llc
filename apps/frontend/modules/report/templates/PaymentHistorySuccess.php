<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">
<?php use_helper('Pagination');  
include_partial('global/innerHeading',array('heading'=>'Payment History'));?>
<div class="clear"></div>
	  <div class="tmz-spacer"></div>
<div id="nxtPage">
<table width="99%" cellpadding="0" cellspacing="0" border="0">
  <tr>
    <td class="blbar" colspan="7" align="right">
    <span>Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total  <b><?php echo $pager->getNbResults(); ?></b>  results</span></td>
  </tr>
  <tr>
      <td width="6%"><span class="txtBold">S. No.</span></td>
      <td width="15%"><span class="txtBold">Bill Number</span></td>
      <td width="25%"><span class="txtBold">Transaction Time</span></td>
      <td width="21%"><span class="txtBold">Application Type</span></td>
      <td width="21%"><span class="txtBold">Order Number</span></td>      
      <td width="21%" class="txtRight"><span class="txtBold">Price</span></td>
      <td width="21%"><span class="txtBold">Support</span></td>

  </tr>
    <?php
      if(($pager->getNbResults())>0) {
          $limit = sfConfig::get('app_records_per_page');
          $page = $sf_context->getRequest()->getParameter('page',0);
          $i = max(($page-1),0)*$limit ;
    ?>
    <?php
      $dollar = 1;
      foreach ($pager->getResults() as $result):
//      echo "<pre>";print_r($result->toArray());die;
        if($result->getCurrency() == $dollar){
            $orderAmount = $result->getAmount();
        }else{
            $orderAmount = $result->getConvertAmount();
        }
        $currencySymbol = PaymentModeManager::currencySymbol($result->getCurrency());

        $i++;
    ?>
    <tr>
      <td><?php echo $i;?></td>
      <td><?php echo $result->getOrderRequestDetails()->getOrderRequest()->getBill()->getFirst()->getBillNumber();?></td>
      <td width="21%"><span><?php echo $result->getResponseTransactionDate();?></span></td>
      <td width="21%"><span class="txtBold"><?php echo $result->getMname(); //echo $result->getDescription ;?>- </span><?php echo $result->getDescription();// echo $result->getMname();?></td>
      <td width="21%"><span class="txtBold"><?php echo link_to($result->getRequestOrderNumber(), url_for('report/getReceipt?receiptId='.Settings::encryptInput($result->getRequestOrderNumber())),array('title'=>'View Details'));?></span></td>
      <td width="21%" class="txtRight"><?php echo format_amount($orderAmount,1,0,$currencySymbol);?></td>
      <td width="21%" class="txtRight">
          <div class="noPrint"><span><a href="<?php echo url_for('requestRefund/applicationDetails?orderNumber='.Settings::encryptInput($result->getRequestOrderNumber())) ?>" title="View Application Details"><?php echo image_tag("/images/duplicate_app.gif");?></a></span>
              <span><a href="<?php echo url_for('requestRefund/openInfoBox?requestId='.Settings::encryptInput($result->getRequestOrderNumber())) ?>" title="Contact Support"><?php echo image_tag("/images/support.gif");?></a></span>
      </div>
      </td>
    </tr>
    <?php endforeach; ?>
    <tr>
        <td class="blbar" colspan="7" height="25px" align="right">
        <?php echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName())) ?></td>
    </tr>
    <?php
    }else { ?>
    <tr><td  colspan="7" align='center' ><div style="color:red"> No record found</div></td></tr>
    <?php } ?>
  </table>
  </div>
</div>
</div>
<div class="content_wrapper_bottom"></div>
