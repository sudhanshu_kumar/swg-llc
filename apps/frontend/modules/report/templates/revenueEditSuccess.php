<?php use_stylesheet('style.css');
use_javascript('calendar.js');
echo include_stylesheets();
echo include_javascripts();
use_helper('Form');?>

<div class="clearfix">
    <div id="nxtPage">
        <form action='#' method="post" name="frm_tra_detail" id="frm_tra_detail" onsubmit="return false;">
            <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
                <tr>
                    <td width="200px;">Date:</td>
                    <td><?php echo $date; ?>
                      <input type="hidden" name="hdn_id" id="hdn_id" value="<?php echo $id;?>">
                    </td>
                </tr>
                <tr>
                    <td >Account:</td>
                    <td colspan="2"><?php echo $accountName; ?>
                    
                    </td>
                </tr>
                
                <tr>
                    <td >Project:</td>
                    <td colspan="2"><?php echo $projectName; ?>
                   
                    </td>
                </tr>
                
                <tr>
                    <td >Support Request:</td>
                    <td colspan="2"><?php echo $remark; ?>
                     
                    </td>
                </tr>

                <tr>
                <td>
                Admin Comments:<span class="red">*</span>
                </td>
                <td>
                    <?php echo textarea_tag('comment_txt',$adminComments)?>
                    <br>
                    <div class="red" id="comment_error">

                    </div>
                   
                </td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                        <div class="divBlock">
                            <center><input style="" type="button" class="loginbutton"  id="multiFormSubmit" value="Approve" onclick="validateForm('approve');">
                                       &nbsp;&nbsp;

                            <input style="" type="button" class="loginbutton"  id="multiFormSubmit" value="Decline" onclick="validateForm('decline');">
                                &nbsp;&nbsp;
                                <input style="" type="button" class="loginbutton"  id="multiFormSubmit" value="Close" onclick="parent.emailwindow.hide();">
                            </center>
                        </div>
                    </td>
                </tr>
            </table>
        </form>
        <br>
    </div>
</div>

<script type="text/javascript">


    function validateForm(action){

        var err  = 0;

        if($('#comment_txt').val() == "")
        {
            $('#comment_error').html("Please enter comments");
            err = err+1;
        }else
        {
            $('#comment_error').html(" ");
        }


        if(err == 0)
        {


            // Call ajax
            var hdn_id = $('#hdn_id').val();
            
            var comments = $('#comment_txt').val();


            $.post("<?php echo url_for('report/saveAdminEditRequest') ?>", {hdn_id: hdn_id,comments: comments,mode: action},
            function(){
                parent.reloadWindow();
                parent.emailwindow.hide();
            }
        );
        }else{
            return false;
        }
    }

    function getAmount(chkId){

        if(document.getElementById(chkId).checked == true){
            document.getElementById('order_amount').value = '0';
            document.getElementById('order_amount').readOnly = true;

        }else{
            document.getElementById('order_amount').value = '';
            document.getElementById('order_amount').readOnly = false;
        }

    }

</script>