<?php  echo ePortal_pagehead(" ",array('class'=>'_form')); ?>
<?php use_helper('Pagination');  ?>
<div class="wrapTable">
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
        <tr>
            <td class="blbar" colspan="7" align="right">
                <div style="float:left">Fraud Prevention Report</div>
                    <span></span></td>
        </tr>
    </table>    
</div>


<div id="search_results">
    <div class="wrapTable">
        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >       
        <thead>
            <tr>    
                <?php
                foreach($pfsrule as $val){
                    ?>
                <td align="center"><b><?php echo ucfirst($val['description']); ?></b></td>
                <?php }?>
            </tr>
        </thead>
      <?php if(count($pfSdetail)>0){?>
        <tbody>
            <tr class="alternateBgColour">
               <?php
               foreach($pfSdetail as $val){
                     $pdetail[$val['fps_rule_id']] = $val['count'];
                }
               
               foreach($pfsrule as $val){                   
                       if (array_key_exists($val['id'], $pdetail))
                       $total = $pdetail[$val['id']];
                       else
                       $total = 0;
                   ?>
                <td align="center"><?php echo $total;?></td>
                <?php } ?>
            </tr>

    

        </tbody>



        


<?php
}else {
?>
    <tr><td colspan="3"  align='center' class='error' >No Record Found</td></tr>
    <?php } ?>
    </table>
</div>
</div>
