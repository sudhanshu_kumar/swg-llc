<?php use_stylesheet('style.css');
use_javascript('calendar.js');
echo include_stylesheets(); 
echo include_javascripts();
use_helper('Form');?>

<div class="clearfix">
    <div id="nxtPage">
        <form action='#' method="post" name="frm_tra_detail" id="frm_tra_detail" onsubmit="return false;">
            <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
                <tr>
                    <td width="200px;">Date:</td>
                    <td><?php echo $revDate; ?>

                    </td>
                </tr>
                <tr>
                    <td >Current Amount:</td>
                    <td colspan="2"><?php echo $revAmount; ?>
                    <input type="hidden" name="hdn_amount" id="hdn_amount" value="<?php echo $revAmount;?>">
                    </td>
                </tr>
               
                <tr>
                <td>
                Comment:<span class="red">*</span>
                </td>
                <td>
                    <?php echo textarea_tag('comment_txt')?>
                    <br>
                    <div class="red" id="comment_error">
                        
                    </div>
                    <input type="hidden" name="hdn_remark" id="hdn_remark" value="<?php echo $remark;?>">
                    <input type="hidden" name="hdn_id" id="hdn_id" value="<?php echo $revDate;?>">
                    <input type="hidden" name="hdn_account" id="hdn_account" value="<?php echo $accountId;?>">
                    <input type="hidden" name="hdn_project" id="hdn_project" value="<?php echo $projectId;?>">
                </td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                        <div class="divBlock">
                            <center><input style="" type="button" class="normalbutton"  id="multiFormSubmit" value="Send Request" onclick="validateForm();">
                                &nbsp;&nbsp;
                                <input style="" type="button" class="normalbutton"  id="multiFormSubmit" value="Close" onclick="parent.emailwindow.hide();">
                            </center>
                        </div>
                    </td>
                </tr>
            </table>
        </form>
        <br>
    </div>
</div>

<script type="text/javascript">


    function validateForm(){

        var err  = 0;

        if($('#comment_txt').val() == "")
        {
            $('#comment_error').html("Please enter comments");
            err = err+1;
        }else
        {
            $('#comment_error').html(" ");
        }

        
        if(err == 0)
        {

           
            // Call ajax
            var hdn_date = $('#hdn_id').val();
            var hdn_account = $('#hdn_account').val();
            var amount = $('#hdn_amount').val();
            var project = $('#hdn_project').val();
            var remark = $('#hdn_remark').val();
            var comments = $('#comment_txt').val();

           
            $.post("<?php echo url_for('report/saveEditRequest') ?>", {hdn_date: hdn_date,amount: amount, remark: remark,comments: comments,hdn_account: hdn_account,project: project },
            function(data){
                parent.reloadWindow();
                parent.emailwindow.hide();
                parent.document.getElementById('flash_notice').style.display = 'block';
                parent.document.getElementById('flash_notice').innerHTML = data;
            }
        );
        }else{
            return false;
        }
    }

    function getAmount(chkId){

        if(document.getElementById(chkId).checked == true){
            document.getElementById('order_amount').value = '0';
            document.getElementById('order_amount').readOnly = true;

        }else{
            document.getElementById('order_amount').value = '';
            document.getElementById('order_amount').readOnly = false;
        }

    }

</script>