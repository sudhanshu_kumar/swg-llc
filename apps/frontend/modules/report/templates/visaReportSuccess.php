<?php use_helper('Form'); ?>
<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
    <div class="content_container">
        <?php include_partial('global/innerHeading',array('heading'=>'Visa Report')); ?>
        <div class="clear"></div>
        <div class="tmz-spacer"></div>
        <?php
        $startDate = isset($startDate)?$startDate:'';
        $endDate = isset($endDate)?$endDate:'';
        ?>
        
        <form action="<?php echo url_for('report/visaReport#data');?>" method="post" onsubmit="return validateForm()">
            <table width="99%" border="0" cellpadding="0" cellspacing="0" class="dataTable">
                <tr>
                    <td width="20%">Start Date<span class="red">*</span></td>
                    <td><input type ="text" name="fdate" onfocus="showCalendarControl(fdate)" class="txt-input" readonly="true" id="fdate" value="<?php echo $startDate; ?>"></td>
                </tr>
                <tr>
                    <td width="20%;">End Date<span class="red">*</span></td>
                    <td><input type ="text" name="tdate" onfocus="showCalendarControl(tdate)" class="txt-input" readonly="true" id="tdate" value="<?php echo $endDate; ?>"></td>
                </tr>
                <tr>
                    <td width="20%;">Report Type<span class="red">*</span></td>
                    <td>
                        <input type ="radio" name="reportType" class="txt-input" id="weeklyReport" value="weekly" <?php echo ($reportType == 'weekly')?'checked':''; ?>>&nbsp;Weekly&nbsp;&nbsp;
                        <input type ="radio" name="reportType" class="txt-input" id="monthlyReport" value="monthly" <?php echo ($reportType == 'monthly')?'checked':''; ?>>&nbsp;Monthly
                    </td>
                </tr>
                <tr>
                    <td width="20%;">Country Wise Report</td>
                    <td>
                        <input type ="checkbox" name="countryWise" class="txt-input" id="countryWise" value="countryWise" >&nbsp;&nbsp;
                    </td>
                </tr>
                <tr id="countryDiv" style="display:none;">
                    <td valign="top" width="20%;">Select Country:<span class="red">*</span></td>
                    <td>
                        <table width="100%" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="50">
                                    <select name="countries" id="countries" size="5" multiple style="width:150px;">
                                        <?php foreach($countryListObj AS $key=> $value){ 
                                            if(!isset($selectedCountries[$key])) { ?>
                                            <option value="<?php echo $key; ?>"><?php echo ucwords($value); ?></option>
                                        <?php } } ?>
                                    </select>


                                </td>
                                <td width="20">
                                    <input type="button" name="moveRight" value="&nbsp;&nbsp;&gt;&nbsp;&nbsp;" class="normalbutton" title="click here to add coutry into right box." onclick="addCountry();" />
                                    <br /><br />
                                    <input type="button" name="moveLeft" value="&nbsp;&nbsp;&lt;&nbsp;&nbsp;" class="normalbutton" title="click here to remove country from right box." onclick="removeCountry();" />
                                </td>
                                <td>                                  
                                    <select name="selectedCountries[]" id="selectedCountries" size="5" multiple style="width:150px;">
                                        <?php foreach($countryListObj AS $key=> $value){
                                            if(isset($selectedCountries[$key])) { ?>
                                            ?>
                                            <option value="<?php echo $key; ?>"><?php echo ucwords($value); ?></option>
                                        <?php } } ?>

                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3"><i>You can select maximum 5 country.</i></td>
                            </tr>
                        </table>


                    </td>
                </tr>









                <tr>
                    <td colspan="2"><span class="red"><strong>Note:</strong></span>&nbsp;&nbsp;&nbsp;Only <strong><?php echo $reported_months; ?> month(s)</strong> transactions can be searched at a time.</td>
                </tr>
                <tr>
                    <td colspan="2" align="center"><input type = "submit"  name="search" value="Search" class="normalbutton"></td>
                </tr>
            </table><br/>

        </form>

        <a name="data"></a>

        <?php
                /**
                 * [WP: 112] => CR: 158
                 * Created partial for weekly and monthly report...
                 */
        if($reportType == 'weekly'){
            if($countryWise == 'countryWise'){
                $fileName = 'visaWeeklyReportByCountry';
            }else{
               $fileName = 'visaWeeklyReport';
            }
        }else{
            if($countryWise == 'countryWise'){
                $fileName = 'visaMonthlyReportByCountry';
            }else{
                $fileName = 'visaMonthlyReport';
            }            
        } ?>
        <div id="loading" class="no_display"><?php echo image_tag('/images/ajax-loader_1.gif',array('alt'=>'Loading ...', 'align' => 'absmiddle'));?><br/>&nbsp;&nbsp;Please wait...</div>
        <div id="exportExcel_error" class="red"></div>
        <?php include_partial($fileName,array('reportArr'=> $reportArr, 'countryListObj' => $countryListObj));

        $reportArr = json_encode($sf_data->getRaw('reportArr'));
        $countryListObj = json_encode($sf_data->getRaw('countryListObj'));

        ?>

    </div>
</div>
<div class="content_wrapper_bottom"> </div>
<form name="downloadForm" id="downloadForm" method="post" action="<?php echo url_for('report/downloadVisaReport'); ?>" >
    <input type="hidden" name="filename" value="<?php echo $excelFileName; ?>" />
</form>

<script>
    function validateForm(){
        var start_date = fdate = $('#fdate').val();
        var end_date = tdate = $('#tdate').val();
        if(fdate == ''){
            alert('Please select Start date.');
            return false;
        }else if(tdate == ''){
            alert('Please select End date.');
            return false;
        }
        var current_date = new Date();
        if(fdate != ''){
            start_date = new Date(start_date.split('-')[0],start_date.split('-')[1]-1,start_date.split('-')[2]);
            if(start_date.getTime()>current_date.getTime()) {
                alert('Start date cannot be greater than today\'s date.');
                return false;
            }
        }
        if(tdate != ''){
            end_date = new Date(end_date.split('-')[0],end_date.split('-')[1]-1,end_date.split('-')[2]);
            if(end_date.getTime()>current_date.getTime()) {
                alert('End date cannot be greater than today\'s date.');
                return false;
            }
        }
        if((fdate != '') && (tdate != '')){
            if(start_date.getTime()>end_date.getTime()) {
                alert('Start date cannot be greater than End date.');
                return false;
            }
        }

        var reported_months = '<?php echo $reported_months; ?>';
        var reported_days = parseInt(reported_months * 31);

        if(reported_months == 0){
            alert("There is no limit set for date range.");
            return false;
        }

        var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
        var diffDays = Math.round(Math.abs((start_date.getTime() - end_date.getTime())/(oneDay)));
        if(diffDays > reported_days){
            alert("You can select only "+reported_months+" month(s) transactions at a time.");
            return false;
        }

        if (!$("input[name='reportType']").is(':checked')) {
            alert('Please select Report Type.');
            return false;
        }

        $("#selectedCountries option").attr("selected","selected");

        if($('#countryWise').is(':checked')){ 
            if($('#selectedCountries').find("option").length == 0){
                alert("Please select country.");
                return false;
            }
        }

        


        return true;

    }

    function addCountry(){

        var selectedCountryLength = $('#selectedCountries').find("option").length;
        
        //Code Starts
        var selectedOpts = $('#countries option:selected');
        //Code Ends

        //Code Starts
        if (selectedOpts.length == 0) {
           alert("Please select country first then move from left to right.");
           e.preventDefault();
        }
        //Code Ends

        var totalCountryLength = parseInt(selectedCountryLength) + parseInt(selectedOpts.length);

        if(totalCountryLength > 5){
            alert('You can have only 5 countries at a time.');
            e.preventDefault();
        }

        //Code Starts
        $('#selectedCountries').append($(selectedOpts).clone());
        $(selectedOpts).remove();
        e.preventDefault();
        

    }

    function removeCountry(){
        var selectedOpts = $('#selectedCountries option:selected');
        if (selectedOpts.length == 0) {
            alert("Please select country first then move from right to left.");
            e.preventDefault();
        }

        $('#countries').append($(selectedOpts).clone());
        $(selectedOpts).remove();
        e.preventDefault();
    }

    $('#countryWise').click(function(){
        if($('#countryWise').is(':checked')){ 
            $('#countryDiv').show();
        }else{
            $('#countryDiv').hide();
        }
        
    })

    var countryWise = '<?php echo $countryWise; ?>';
    if(countryWise != ''){
        $('#countryWise').attr('checked','checked');        
        $('#countryDiv').show();        
    }





</script>

<script>

    var reportArr = '<?php echo $reportArr; ?>';
    var countryListObj = '<?php echo $countryListObj; ?>';
    var reportType = '<?php echo $reportType; ?>';
    var countryWise = '<?php echo $countryWise; ?>';

    /*
    function exportExcel(){
        $('#downloadForm').submit();
    }*/


    
    function exportExcel(){
        $('#exportExcel').hide();
        $('#loading').show();
        var url = "<?php echo url_for('report/downloadVisaReportInExcel'); ?>";
        $.ajax({
            type: "POST",
            url: url,
            data: {reportType: reportType, countryWise: countryWise, reportArr: reportArr, countryListObj: countryListObj},
            success: function (data) {
                data = jQuery.trim(data);
                if(data == 'yes'){
                    $('#exportExcel_error').html('');
                    $('#downloadForm').submit();
                    //var downloadURL = '< ?php echo _compute_public_path($excelFileName, 'uploads/excel', '', true); ?>';
                    //var win = window.open('< ?php echo url_for('report/downloadVisaReport'); ?>/filename/<?php echo $excelFileName; ?>','','')
                    
                   // window.open('<?php //echo _compute_public_path($excelFileName, 'uploads/excel', '', true); ?>');
                }else{
                    $('#exportExcel_error').html('Error found while creating excel sheet. Please try again.');
                }
                $('#exportExcel').show();
                $('#loading').hide();

            }//End of success: function (data) {...
        });
    }//End of function exportExcel(){...
    
    
</script>
