<?php use_helper('Pagination');
 use_helper('EPortal');
?>

<div class="wrapTable">
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >

    </table>
    <br class="pixbr" />
</div>


<div class="clearfix">
    <div id="nxtPage">
        <table class="innerTable_content">
            <tr>
                <td class="blbar" colspan="7" align="right">
                    <div style="float:left">Detailed Report</div>
                <span>Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total  <b><?php echo $pager->getNbResults(); ?></b>  results</span></td>
            </tr>
            <tr>
                <td colspan="7">
                    <div id = "csvDiv" style="cursor:pointer;" onclick="javascript:progBarDiv('merchantReportCsv', 'csvDiv', 'progBarDiv')"><span  class="floatLeft">Click here to download data in <font color="red"><b><u>CSV</u></b></font> Format</span></div><div id ="progBarDiv"  style="display:none;"></div><br class="pixbr" />
                </td>
            </tr>
            <tr>
                <td width="4%" ><span class="txtBold">S. No.</span></td>
                <td width="9%"><span class="txtBold">Transaction No.</span></td>
                <td width="14%"><span class="txtBold">Description</span></td>
                <td width="12%"><span class="txtBold">Paid Date</span></td>
                <td width="9%"><span class="txtBold">Total Amount ($)</span></td>
                <td width="9%"><span class="txtBold">Payment ($)</span></td>
                <td width="6%"><span class="txtBold">Fee ($)</span></td>
            </tr>
            <?php
            $grandTotal = '';
            $grandpayment = '';
            $grandfee = '';
            if(($pager->getNbResults())>0) {
                $from_date = $fromDate == "" ? "BLANK" : $fromDate;
                $to_date = $toDate == "" ? "BLANK" : $toDate;

                $limit = sfConfig::get('app_records_per_page');
                $page = $sf_context->getRequest()->getParameter('page',0);
                $i = max(($page-1),0)*$limit ;
            }
            ?>
            <?php
            foreach ($pager->getResults() as $result):
            //        echo "<pre>";print_r($result);die;
            $i++;
            ?>
            <tr>
                <td width="5%"><?php echo $i;?></td>
                <td width="9%"><span><?php echo $result['OrderRequest']['transaction_number'];?></span></td>
                <td width="9%"><span><?php echo $result['description'];?></span></td>
                <td width="9%"><span><?php echo $result['paid_date'];?></span></td>
                <?php
                $amount =0;
                $payment =0;

                foreach($result['OrderRequestSplit'] as $val){
                    if($val['merchant_code']==sfConfig::get('app_swglobal_merchant_code'))
                    $payment = $val['item_fee'];
                    else
                    $amount+= $val['item_fee'];

                }
                $payment= $amount;
                $fee = $amount-$payment;
                $grandTotal+=$amount;
                $grandpayment+= $payment;
                $grandfee+= $fee;

                ?>

                <td width="9%"> <?php echo format_amount($amount/100);?> </td>
                <td width="9%"> <?php echo format_amount($payment/100);?> </td>
                <td width="9%"> <?php echo format_amount($fee/100);?> </td>
            </tr>
            <?php endforeach; ?>
            <tr>
                <td colspan="4"><b>Total</b></td>
                <td><span class="txtBold"> <?php echo format_amount($grandTotal/100);?></span></td>
                <td><span class="txtBold"> <?php echo format_amount($grandpayment/100);?></span></td>
                <td><span class="txtBold"> <?php echo format_amount($grandfee/100);?></span></td>
            </tr>
            <tr>
                <td class="blbar" colspan="7" height="25px" align="right">
                    <?php
                    echo ajax_pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName()).'?startdate='.$from_date.'&enddate='.$to_date.'&usePager=true','detail_div');
                    ?>

                </td>
            </tr>


        </table>
    </div>
</div>
<script>
function progBarDiv(url, referenceDivId, targetDivId){
    $('#'+targetDivId).html('<?php echo image_tag('../images/ajax-loader_1.gif'); ?>');
    $('#'+referenceDivId).css("display","none");
    $('#'+targetDivId).css("display","inline");
    $('#'+targetDivId).load(url, {byPass:1 },function (data){
        
                            if(data=='logout'){
                                $('#'+targetDivId).html(data);
                                location.reload();
                              } });

}
</script>
