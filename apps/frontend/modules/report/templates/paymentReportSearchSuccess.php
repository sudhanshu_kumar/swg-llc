<?php use_helper('Pagination');?>

<div id="search_result">
    <div class="wrapTable">
<table width="100%">
  <tr>
    
    <td class="blbar" colspan="5">
    <div>
    <div style="float:left"><span>User Payment report</span></div>
    <div style="float:right"><span>Showing <b><?php echo ( $pager->getNbResults()>0 ? $pager->getFirstIndice() : 0 ); ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total  <b><?php echo $pager->getNbResults(); ?></b>  results</span></div></div></td>
  </tr>
  <tr>
      <td width="5%"><span class="txtBold">S. No.</span></td>
      <td width="15%"><span class="txtBold">Order Number</span></td>
      <td width="25%"><span class="txtBold">Bill Number</span></td>
      <td width="21%"><span class="txtBold">Date</span></td>
      <td width="21%"><span class="txtBold">Payment Status</span></td>
      
  </tr>
    <?php
      if(($pager->getNbResults())>0) {
          $limit = sfConfig::get('app_records_per_page');
          $page = $sf_context->getRequest()->getParameter('page',0);
          $i = max(($page-1),0)*$limit ;
          if($billNo =='')
          $billNo = "BLANK";
          if($orderNo =='')
          $orderNo = "BLANK";
    ?>
    <?php
      foreach ($pager->getResults() as $result):
//      echo "<pre>";print_r($result->toArray());die;
        $i++;
    ?>
    <tr>
      <td><?php echo $i;?></td>
      <td><?php echo $result->getRequestOrderNumber(); ?></td>
      <td width="21%"><?php echo $result->getOrderRequestDetails()->getOrderRequest()->getBill()->getFirst()->getBillNumber();?></td>
      <td width="21%"><?php echo $result->getDate(); ?> </td>
      <td width="21%"><?php  echo ( $result->getPaymentStatus()==0 ? 'Done' : 'Not Done' );?></td>
      
    </tr>
    <?php endforeach; ?>
    <tr>
        <td class="blbar" colspan="6" height="25px" align="right">
        
        <?php   echo ajax_pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName().'?from_date='.$from_date.'&to_date='.$to_date.'&bill_number='.$billNo.'&order_number='.$orderNo), 'result_div') ?>
        </td>
    </tr>
    <?php
    }else { ?>
    <tr><td  colspan="5" align='center' ><div style="color:red"> No record found</div></td></tr>
    <?php } ?>
  </table>
  </div>
</div>

