<?php use_helper('Form'); ?>
<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
    <div class="content_container">
        <?php include_partial('global/innerHeading',array('heading'=>'Transaction Report'));?>

        <div class="clear"></div>
        <div class="tmz-spacer"></div>

        <form name="chartForm" id="chartForm" method="post" action="" >
            <table cellpadding="0" cellspacing="0" width="100%;">
                <tr>
                    <td width="100%" class="grey_bg_table">
                        <table width="100%" cellpadding="0" cellpadding="0">
                            <tr>
                                <td>
                                    <span class="txtBold" width="33%"> Report Type:</span>&nbsp;&nbsp;
                                    <input type="radio" onclick="showWeekly()" name="reportType" value="weekly" checked="checked">&nbsp;Weekly &nbsp;
                                    <input type="radio" onclick="showDateRange()" name="reportType" value="dateRange">&nbsp;Date Range
                                </td>
                                <td><span class="txtBold" width="33%"> Chart Type:</span>&nbsp;&nbsp;
                                <input type="radio" name="chartType" id="barChart" value="bar" checked="checked">&nbsp;Bar Chart&nbsp;&nbsp;
                                    <input type="radio" name="chartType" id="pieChart" value="pie">&nbsp;Pie Chart
                                    
                                </td>
                                <td><span class="txtBold" width="33%"> Currency:</span>&nbsp;&nbsp;
                                    <input type="radio" name="currency" id="dollar" value="dollar" checked="checked">&nbsp;Dollar&nbsp;&nbsp;
                                    <input type="radio" name="currency" id="pound" value="pound" >&nbsp;Pound
                                </td>
                            </tr>

                            <tr>
                                <td id="weeklyId" colspan="3" width="100%" >
                                    <span class="txtBold"> Week:</span> &nbsp;&nbsp;<select name="dateRange" id="dateRange" >
                                        <option value="7">Last 7 Days</option>
                                        <option value="15">Last 15 Days</option>
                                        <option value="30">Last 30 Days</option>
                                    </select>
                                </td>
                                <td id="dateRangeId" class="no_display" colspan="3" width="100%">
                                    <?php $fdate = (isset($fdate))?$fdate:"";
                                    echo "<span class=\"txtBold\">From:</span> ". input_tag('fdate', $fdate, array('size' => 20, 'maxlength' => 10,'class'=>'txt-input', 'onfocus'=>'showCalendarControl(fdate)', 'readonly'=>'true')); ?>

                                    <?php $tdate = (isset($tdate))?$tdate:"";
                                    echo "<span class=\"txtBold\">To:</span> ".input_tag('tdate', $tdate, array('size' => 20, 'maxlength' => 10,'class'=>'txt-input', 'onfocus'=>'showCalendarControl(tdate)', 'readonly'=>'true')); ?>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" align="center">

                                    &nbsp;&nbsp;<input type="button" name="btnSubmit" id="btnSubmit"  class="normalbutton" value="Search" onclick="submitData();" />
                                </td>

                            </tr>
                        </table>
                    </td>
                </tr>
                </table>
                </form>
                <div class="tmz-spacer"></div>
                <table cellpadding="0" cellspacing="0" width="100%;" id="data_grid_id" class="no_display">

                <tr>
                    <td class="grey_bg_table">
                        <table width="100%" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="50%" valign="top">
                                    <div id="transactionReportByGatewayAndAmountId" style="width: 100%; height:200px;text-align:center;"></div>
                                </td>
                                <td width="50%" valign="top">
                                    
                                    <div id="transactionReportByGatewayAndStatusId" style="width: 100%; height:200px;text-align:center;"></div>
                                    <div id="radioButtonDiv" class="no_display" align="center">
                                        <input type="radio" name="paymentStatus" value="0"  onclick="transactionReportByGatewayAndStatus(0)" checked="checked">&nbsp;Success &nbsp;&nbsp;
                                        <input type="radio" name="paymentStatus" value="1"  onclick="transactionReportByGatewayAndStatus(1)">&nbsp;Fail &nbsp;&nbsp;
                                        <input type="radio" name="paymentStatus" value="2"  onclick="transactionReportByGatewayAndStatus(2)">&nbsp;Refund &nbsp;&nbsp;
                                        <input type="radio" name="paymentStatus" value="3" onclick="transactionReportByGatewayAndStatus(3)">&nbsp;Charge-back
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                 <td width="50%" valign="top" colspan="2" align="center">
                                    <div id="transactionReportByStatusId" style="width: 100%; height:200px;text-align:center;"></div>
                                </td>

                            </tr>                            
                            <tr>
                                <td colspan="2" valign="top">
                                    <div id="countryWiseCollectionId" style="width: 100%; height:200px;text-align:center;"></div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" valign="top">
                                    <div id="countryWiseTransactionId" style="width: 100%; height:200px;text-align:center;"></div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        

        <div class="clear">&nbsp;</div>


    </div>
</div>
<div class="content_wrapper_bottom"></div>
<script type="text/javascript">

    //$(function() {
        //$('#tdate').datepick();
        //$('#fdate').datepick();
    //});

    $(document).ready(function(){
        $('#fdate').click(function(){
            changePopupDivPosition();
        });
        $('#tdate').click(function(){            
            changePopupDivPosition();
        });
    });

    function changePopupDivPosition(){
        if($('#data_grid_id').is(':visible')){
            $('#CalendarControl').css("top","195px");
        }
    }

    function showDateRange(){
        $('#weeklyId').hide();
        $('#dateRangeId').show();       
    }

    function showWeekly(){
        $('#dateRangeId').hide();
        $('#weeklyId').show();       
        if($('#CalendarControl').length > 0){
            $('#CalendarControl').hide();
        }        
    }
    var reportType = '';
    var chartType = '';
    var tdate = '';
    var fdate = '';
    var currency = '';

    function submitData(){
        var validatFlag = validateForm();
        if(validatFlag){

            reportType = $('input[name=reportType]:checked').val();
            chartType = $('input[name=chartType]:checked').val();
            currency = $('input[name=currency]:checked').val();
            
            if(reportType == 'weekly'){
                var dateRange = $('#dateRange').val();
                tdate = new Date();
                tdate = tdate.getFullYear()+'-'+(tdate.getMonth()+1)+'-'+tdate.getDate();

                fdate = new Date();
                fdate.setDate(fdate.getDate()-(parseInt(dateRange)-1));
                fdate = fdate.getFullYear()+'-'+(fdate.getMonth()+1)+'-'+fdate.getDate();

                /* Clear from and to date... */
                $('#fdate').val('');
                $('#tdate').val('');

            }else{
                fdate = $('#fdate').val();
                tdate = $('#tdate').val();
                $("option[value='7']").attr('selected', 'selected');
            }

            $("input[name='paymentStatus'][value='0']").attr("checked", "checked");
            
            transactionReportByGatewayAndAmount(fdate,tdate,chartType);
            transactionReportByGatewayAndStatus(0);
            CountryWiseCollection(fdate,tdate,chartType);
            countryWiseTransaction(fdate,tdate,chartType);
            transactionReportByStatus(fdate,tdate,chartType);
            $('#data_grid_id').show();
        }else{
            return false
        }

    }

    function validateForm(){
        var reportType = $('input[name=reportType]:checked').val();
        if(reportType == 'dateRange'){
            var start_date = fdate = $('#fdate').val();
            var end_date = tdate = $('#tdate').val();
            if(fdate == ''){
                alert('Please select From date.');
                return false;
            }else if(tdate == ''){
                alert('Please select To date.');
                return false;
            }
            var current_date = new Date();            
            if(fdate != ''){
              start_date = new Date(start_date.split('-')[0],start_date.split('-')[1]-1,start_date.split('-')[2]);
              if(start_date.getTime()>current_date.getTime()) {
                alert('From date cannot be greater than today\'s date.');
                return false;
              }
            }
            if(tdate != ''){
              end_date = new Date(end_date.split('-')[0],end_date.split('-')[1]-1,end_date.split('-')[2]);
              if(end_date.getTime()>current_date.getTime()) {
                alert('To date cannot be greater than today\'s date.');
                return false;
              }
            }
            if((fdate != '') && (tdate != '')){
              if(start_date.getTime()>end_date.getTime()) {
                alert('From date cannot be greater than To date.');
                return false;
              }
            }
        }
        return true;
    }






    function transactionReportByGatewayAndAmount(fdate,tdate,chartType){

        var url = '<?php echo url_for('report/transactionReportByGatewayAndAmount'); ?>';
        $.ajax({
            type: "POST",
            url: url,
            data: "currency="+currency+"&fdate="+fdate+"&tdate="+tdate+"&chartType="+chartType,

            success: function(data) {
                if(data == 'logout'){
                    window.location.reload();
                }

                $('#transactionReportByGatewayAndAmountId').html(data);

            }


        });
    }

    function transactionReportByStatus(fdate,tdate,chartType){

        var url = '<?php echo url_for('report/transactionReportByStatus'); ?>';
        $.ajax({
            type: "POST",
            url: url,
            data: "currency="+currency+"&fdate="+fdate+"&tdate="+tdate+"&chartType="+chartType,
            success: function(data) {
                $('#transactionReportByStatusId').html(data);
            }
        });
    }
    
    function transactionReportByGatewayAndStatus(paymentStatus){

        if(paymentStatus == undefined){
            paymentStatus = 0;
        }
        
        var url = '<?php echo url_for('report/transactionReportByGatewayAndStatus'); ?>';
        $.ajax({
            type: "POST",
            url: url,
            data: "currency="+currency+"&fdate="+fdate+"&tdate="+tdate+"&chartType="+chartType+"&paymentStatus="+paymentStatus,

            success: function(data) {

                $('#transactionReportByGatewayAndStatusId').html(data);
                $('#radioButtonDiv').show();

            }

        });
    }
    function CountryWiseCollection(fdate,tdate,chartType){

        var url = '<?php echo url_for('report/countryWiseCollection'); ?>';
        $.ajax({
            type: "POST",
            url: url,
            data: "currency="+currency+"&fdate="+fdate+"&tdate="+tdate+"&chartType="+chartType,

            success: function(data) {

                $('#countryWiseCollectionId').html(data);

            }

        });
    }
    function countryWiseTransaction(fdate,tdate,chartType){

        var url = '<?php echo url_for('report/countryWiseTransaction'); ?>';
        $.ajax({
            type: "POST",
            url: url,
            data: "currency="+currency+"&fdate="+fdate+"&tdate="+tdate+"&chartType="+chartType,

            success: function(data) {
                $('#countryWiseTransactionId').html(data);

            }

        });
    }


</script>