<?php use_stylesheet('style.css');
use_javascript('calendar.js');
echo include_stylesheets(); 
echo include_javascripts(); ?>

<div class="clearfix">
  <div id="nxtPage">
    <form action='#' method="post" name="frm_tra_detail" id="frm_tra_detail" onsubmit="return false;">
      <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
        <?php if($showComments) {?>
        <tr>
          <td width="200px;">Support Request</td>
          <td>
            <?php echo $comments; ?>
          </td>
        </tr>
        <tr>
          <td width="200px;">Admin Comments</td>
          <td>
            <?php echo $adminComments; ?>
          </td>
        </tr>
        <?php }?>
        <tr>
          <td width="200px;"><?php echo $form['date']->renderLabel(); ?><span class="red"></span></td>
          <td><?php echo $form['date']->render(); ?>
            <br>
            <div class="red" id="date_error">
              <?php echo $form['date']->renderError(); ?>
            </div>
          </td>
        </tr>
        <?php if($status){ ?>
        <tr id="orderNumber_row" >
          <td ><?php echo $form['amount']->renderLabel(); ?><span class="red"></span></td>
          <td colspan="2"><?php echo $form['amount']->render(); ?>
            <br>
            <div class="red" id="amount_error">
              <?php echo $form['amount']->renderError(); ?>
            </div>
          </td>
        </tr>
        <tr id="orderNumber_row" >
          <td >Holiday</td>
          <td colspan="2">
            <input type="checkbox" name="chk_remark" id="chk_remark" value="H" <?php if($remark !='') echo 'checked' ?> onclick="getAmount('chk_remark')">
            <input type="hidden" name="hdn_id" id="hdn_id" value="<?php echo $revDate;?>">
            <input type="hidden" name="hdn_account" id="hdn_account" value="<?php echo $accountId;?>">
          </td>
        </tr>
        <?php } ?>
        <tr>
          <td align="center" colspan="2">
            <div class="divBlock">
              <center><?php if($status) {?><input style="" type="button" class="loginbutton"  id="multiFormSubmit" value="Save" onclick="validateForm();"><?php }?>
                &nbsp;&nbsp;
                <input style="" type="button" class="loginbutton"  id="multiFormSubmit" value="Close" onclick="parent.emailwindow.hide();">
              </center>
            </div>
          </td>
        </tr>
      </table>
    </form>
    <br>
  </div>
</div>

<script type="text/javascript">


  function validateForm(){

    var err  = 0;

    if($('#order_amount').val() == "")
    {
      $('#amount_error').html("Please enter amount");
      err = err+1;
    }else
    {
      $('#amount_error').html(" ");
    }

    if($('#order_date_day').val() == "" )
    {
      $('#date_error').html("Please enter transaction date");
      err = err+1;
    }else
    {
      $('#date_error').html(" ");
    }

    if($('#order_date_month').val() == "" )
    {
      $('#date_error').html("Please enter transaction month");
      err = err+1;
    }else
    {
      $('#date_error').html(" ");
    }

    if($('#order_date_year').val() == "" )
    {
      $('#date_error').html("Please enter transaction year");
      err = err+1;
    }else
    {
      $('#date_error').html(" ");
    }


    if(err == 0)
    {

      var chkId = '' ;
      if($("input:checked").length == 1){
        chkId = $('#chk_remark').val();
      }

      // Call ajax
      var hdn_id = $('#hdn_id').val();
      var hdn_account = $('#hdn_account').val();
      var amount = $('#order_amount').val();

      var dayVal = $('#order_date_day').val();
      var monthVal = $('#order_date_month').val();
      var yearVal = $('#order_date_year').val();
      $.post("<?php echo url_for('report/updateRevDetail') ?>", {hdn_id: hdn_id,amount: amount, chkId: chkId,dayVal: dayVal,monthVal: monthVal,yearVal: yearVal,hdn_account: hdn_account },
      function(data){
        parent.reloadWindow();
        parent.emailwindow.hide();
        parent.document.getElementById('flash_notice').style.display = 'block';
        parent.document.getElementById('flash_notice').innerHTML = data;
      }
    );
    }else{
      return false;
    }
  }

  function getAmount(chkId){

    if(document.getElementById(chkId).checked == true){
      document.getElementById('order_amount').value = '0';
      document.getElementById('order_amount').readOnly = true;

    }else{
      document.getElementById('order_amount').value = '';
      document.getElementById('order_amount').readOnly = false;
    }

  }

</script>