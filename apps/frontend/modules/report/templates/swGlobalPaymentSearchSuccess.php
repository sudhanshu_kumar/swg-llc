<?php use_helper('Pagination');

?>
<div class="clearfix">
    <div id="nxtPage">
        <table class="innerTable_content">
            <?php if($count>0){?>
            <tr>
                <td class="blbar"  colspan="2" align="left"><div style="float:left">Summary Report</div>

                </td>
            </tr>
            <tr>
                <td>Transaction Fee</td>
                <td><?php echo $transactionCharge ;?>%</td>
            </tr>
            <tr>
                <td>Total Transactions</td>
                <td><?php echo $totalTransaction?></td>
            </tr>
            <tr>
                <td>Total Amount</td>
                <td><?php echo format_amount($totalAmount/100,1);?></td>
            </tr>
            <tr>
                <td>Total Payment</td>
                <td><?php echo format_amount($totalPayment/100,1);?></td>
            </tr>
            <tr>
                <td>Total Fee</td>
                <td><?php echo format_amount($totalFee/100,1);?></td>
            </tr>
            <tr>
                <td  colspan="2" align="left">
                    <div style="float:right">
                        <form name="detail" id="detail" method="post">
                            <input type="hidden" name="startdate" id="startdate" value="<?php echo $fromDate?>">
                            <input type="hidden" name="enddate" id="enddate" value="<?php echo $toDate?>">

                            <div class="lblButton">
                                <input type="button" name="show" id="show" style="display:block; _padding-left:0px; *padding-left:0px;" class='normalbutton' onclick="validate_form();" value="Detailed Report(Click to Open)">
                                <input type="button" name="hide" id="hide" style="display:none" class='normalbutton' onclick="hide_form();" value="Hide Detailed Report">
                            </div>
                            <div class="lblButtonRight">
                                <div class="btnRtCorner"></div>
                            </div>
                        </form>
                    </div>
                </td>
            </tr>
            <?php }else{?>
            <tr>
                <td class="blbar"  colspan="2" align="left"><div style="float:left">Summary</div>
                </td>
            </tr>
            <tr><td  colspan="2" align='center' ><div style="color:red"> No record found</div></td></tr>
            <?php }?>

        </table>
    </div>
    <div>&nbsp;</div>
    <div id="detail_div"  class="clearfix"></div>
    <div style="display:none;padding-top:20px;" align="center" id="loader"><?php echo image_tag('/images/ajax-loader.gif',array()); ?></div>
</div>
<script>
    function validate_form(){
        $('#detail_div').html('<?php echo image_tag('../images/ajax-loader.gif'); ?>');
        var err = 0;
        if(err == 0){          
            $.post('swGlobalReportdetail',$("#detail").serialize(), function(data){
                if(data == "logout"){
                    location.reload();
                }else{                   
                    document.getElementById('show').style.display = "none";
                    document.getElementById('hide').style.display = "block";
                    $("#detail_div").html(data);
                }
            });
        }
    }
    function hide_form(){
        document.getElementById('detail_div').innerHTML = "";
        document.getElementById('show').style.display = "block";
        document.getElementById('hide').style.display = "none";
    }
</script>