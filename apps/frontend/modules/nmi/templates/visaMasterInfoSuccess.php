<html>
<head>
<title></title>
<link rel="stylesheet" type="text/css" media="screen" href="<?php  echo stylesheet_path('style');?>" />
</head>
<body>
 <div class="comBox">
  <h3><a name="PayOption">What is Verified by Visa?</a></h3>
  <div class="clear">&nbsp;</div>
  <p>
  Verified by Visa is a new way to add safety when you buy online. With added security to your existing Visa card, Verified by Visa ensures that only you can use your Visa card online. It's easy to activate for your existing Visa card, and it's free. </p>
    <div class="clear">&nbsp;</div>
  <p><strong>For more information:</strong> <br/><a href = "http://www.visa-asia.com/ap/sea/cardholders/security/vbv.shtml" class="bluelink" target="_blank">http://www.visa-asia.com/ap/sea/cardholders/security/vbv.shtml</a></p>
</div>
<div class="comBox">
    <h3><a name="PayDo">What is MasterCard  SecureCode?</a></h3>
    <div class="clear">&nbsp;</div>
  <p>MasterCard SecureCode is a simple and secure way to pay at thousands of online stores. A private code known only to you and your bank, your SecureCode enhances your existing MasterCard account by protecting you against unauthorized use of your card when shopping online at participating online retailers. </p>
    <div class="clear">&nbsp;</div>
  <p><strong>For more information:</strong> <br/><a href class="bluelink" ="http://www.mastercard.com/us/personal/en/cardholderservices/securecode/index.html" target="_blank" >http://www.mastercard.com/us/personal/en/cardholderservices/securecode/index.html.</a></p>
  </div>
  <center>
  <input type="button" value="Close" class="normalbutton" onClick="window.close()">
  </center>
</body>
</html>
