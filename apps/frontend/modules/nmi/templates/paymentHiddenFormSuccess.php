<table width="100%" cellspacing="0" cellpadding="0" align="center" style="border:none;" >
    <tr>
        <td style="padding-top:100px;border:none;"> <!-- border: 2px solid rgb(51, 102, 255); margin: 50px 35%; width:200px;height:300px; padding: 20px; color: rgb(0, 51, 153); -->
            <div style=" font-size: 20px; font-weight: bold; text-align: center;">
Processing, Please Wait..
            </div>
        </td>
    </tr>
    <tr>
        <td align="center" style="border:none;">[Note: Please do not press back and refresh button during payment process.]</td>
    </tr>
</table>
<form id="frm_billing_info"  name="frm_billing_info" action="<?php echo $formData->thirdPartyFormActionURL; ?>" method="POST" >
    <input type="hidden" name="billing-first-name" id="billing-first-name" value="<?php echo $formData->billing_first_name; ?>" />
	<input type="hidden" name="billing-last-name" id="billing-last-name" value="<?php echo $formData->billing_last_name; ?>" />
	<input type="hidden" name="billing-address1" id="billing-address1" value="<?php echo $formData->billing_address1; ?>" />
	<input type="hidden" name="billing-address2" id="billing-address2" value="<?php echo $formData->billing_address2; ?>" />
	<input type="hidden" name="billing-city" id="billing-city" value ="<?php echo $formData->billing_city; ?>" />
	<input type="hidden" name="billing-state" id="billing-state" value="<?php echo $formData->billing_state; ?>" />
	<input type="hidden" name="billing-postal" id="billing-postal" value="<?php echo $formData->billing_postal; ?>" />
	<input type="hidden" name="billing-country" id="billing-country" value="<?php echo $formData->billing_country; ?>" />
	<input type="hidden" name="billing-email" id="billing-email" value="<?php echo $formData->billing_email; ?>" />
	<input type="hidden" name="billing-phone" id="billing-phone" value="<?php echo $formData->billing_phone; ?>" />
	<input type="hidden" name="cardType" id="cardType" value="<?php echo $formData->cardType; ?>" />
	<input type="hidden" name="billing-cc-number" id="billing-cc-number" value="<?php echo $formData->billing_cc_number; ?>" />
	<input type="hidden" name="card_holder" id="card_holder" value="<?php echo $formData->card_holder; ?>" />
	<input type="hidden" name="billing-cc-exp[month]" id="billing-cc-exp_month" value="<?php echo $formData->billing_cc_exp_month; ?>" />
	<input type="hidden" name="billing-cc-exp[year]" id="billing-cc-exp_year" value="<?php echo $formData->billing_cc_exp_year; ?>" />
	<input type="hidden" name="billing-cvv" id="billing-cvv" value="<?php echo $formData->billing_cvv; ?>" />
	<input type="hidden" name="request_ip" id="request_ip" value="<?php echo $formData->request_ip; ?>" />
    <input type="hidden" name="requestId" id="requestId" value="<?php echo $formData->requestId; ?>" />
    <input type="hidden" name="billing-cc-exp" id="billing-cc-exp" value="<?php echo $formData->billing_cc_exp; ?>" />
    <input type="hidden" name="err-url" id="err-url" value="<?php echo $err_url;?>" />
    <input type="hidden" name="order-number" id="order-number" value="<?php echo $orderNumber;?>" />
</form>
<script>   
    $(document).ready(function (){        
        var url = "<?php echo url_for('nmi/getCartInfo');?>";
        $.post(url, { }, function (data){            
            if(jQuery.trim(data) == 1){
                document.frm_billing_info.submit();
            }else{
                <?php sfContext::getInstance()->getUser()->setFlash('notice', 'Applications has been already processed. '); ?>
                window.parent.location = "<?php echo url_for('cart/list');?>";
            }
        });
    })    
</script>