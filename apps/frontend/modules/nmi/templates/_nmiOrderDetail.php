
<table width="100%">
  <tr>
    <td class="blbar" colspan="3">Order Details - <?php //echo $request_details->getMerchant()->getAddress();?> </td>
  </tr>
  <tr>
    <td width="77%" class="txtBold">Item</td>
    <td width="23%" class="txtBold txtRight">Price</td>
  </tr>
  <tr>

    <td ><span class="txtBold"><?php  echo $request_details->getDescription();?> </span></td>

    <td class="txtRight"><?php echo format_amount($request_details->getAmount(),1);?></td>
  </tr>
  <tr class="blTxt">

    <td class="txtBold">&nbsp;</td>
    <td class="txtRight blTxt"><span class="bigTxt">Total: <?php echo format_amount($request_details->getAmount(),1);?></span></td>
  </tr>
</table>
