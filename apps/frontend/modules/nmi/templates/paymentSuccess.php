<?php
use_javascript('dhtmlwindow.js');
use_stylesheet('dhtmlwindow.css');
use_javascript('modal.js');
use_stylesheet('modal.css');

?>
<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">
<?php 

include_partial('global/innerHeading',array('heading'=>'Payment Confirmation'));?>
 
    <?php
   
//echo "<br/>";
$sf = sfContext::getInstance()->getUser();
$msgFlag = false;
if($sf->hasFlash('notice')){ $msgFlag = true;
  ?><div id="flash_notice" class="alertBox" ><?php
  echo nl2br($sf->getFlash('notice'));

  ?></div><?php
    ## If card type contains Mo then only this link will enable...
    $MoFlag = sfContext::getInstance()->getUser()->getAttribute('MoFlag');
    if($MoFlag){ ?>
    <!-- <div class="highlight yellow" id="flash_notice_content" style="margin-right: 0px;margin-left: 0px;"><b><span style="font-size: 15px;"><a href="<?php //echo url_for("mo_configuration/trackingProcess?requestId=".$requestId);?>">CLICK HERE</a> if you would like to attempt this transaction via Money Order.</span></b></div>-->
    <?php } 
}?>

<?php if($sf_request->hasParameter('display_msg')){echo "<div id='flash_notice' class='alertBox' >Only Visa Card Accepted</div>";}?>
<?php
include_partial('paymentGateway/orderDetail', array('request_details'=>$request_details, 'application_type' => $application_type))
?>
<?php //include_partial('orderDetail', array('request_details'=>$request_details)) ?>
<div class="clear">&nbsp;</div>
<div align="center">

      <!-- input type="hidden" name="request_id" id="request_id" value="<?php //echo $requestId ?>" reqeustId is being used from session variable...  -->
      <input type="hidden" name="paymentMode" id="paymentMode" value="<?php echo $paymentMode ?>">
      <?php
         $destiNationCurrencyId = CurrencyManager::getDestinationCurrency($processingCountry);
         $appType = Functions::getApplicationTypeByRequestId($requestId);
         if(1 != $destiNationCurrencyId && $appType != 'vap' ){
             $amount = CurrencyManager::getConvertedAmount($request_details->getAmount(),1,4);
         }else{
             $amount = $request_details->getAmount();
         }
      ?>
        <input type="hidden" name="amount" id="amount" value="<?php echo $amount; ?>">
<?php

$jna_service_disabled = sfConfig::get('app_jna_service_disabled');
if($jna_service_disabled){    
    $disabled_daterange =  sfConfig::get('app_jna_service_disabled_daterange');
    if($disabled_daterange){
        $startTime = sfConfig::get('app_jna_service_disabled_startdate');
        $endTime = sfConfig::get('app_jna_service_disabled_enddate');
    }else{
        $startTime = date('Y-m-d H:i:s');
        $endTime = date('Y-m-d H:i:s');
    }
    /**
     * [WP: 111] => CR: 157
     * Added condition of MasterCard SecureCode for JNA service...
     */
    if($paymentMode == '10,jna_nmi_vbv' || $paymentMode == '10,jna_nmi_mcs'){
        $currdate = date('Y-m-d H:i:s');        
        if($currdate >= $startTime && $currdate <= $endTime){
             $payStatus = 0;
             $message = sfConfig::get('app_jna_service_message');
             ?>
             <style>
                    .highlight.red {
                        background-color:RED;
                        border-color:RED;
                        color:#FFF;
                        }

                    .highlight {
                        border:1px dotted;
                        margin:10px 0;
                        padding:5px;
                        position:relative;
                    }
                    .highlight a {
                        color:#FFFFFF;
                    }
                </style>
                <div id="flash_notice" class="highlight red"><?php echo $message;?></div>
    <?php }
     }
} ?>


<?php
if (isset($payStatus) && 0 !=$payStatus)
    {?>
        <input type="button" name="btn_continue" id="btn_continue" value="Continue" class="normalbutton" onclick='continuePayment()'>
<?php } ?>
</div>
<br/>

<div id="payment_form" align="center">

</div>

<input type="hidden" id="refreshed" value="no">
<script>    
     function continuePayment(){
        $('#flash_notice_content').hide();
        $('#flash_notice').hide();        
        $('#btn_continue').hide();
        $('#payment_form').html('<?php echo image_tag('../images/ajax-loader.gif'); ?>');
        var url = "<?php echo url_for('nmi/nmiForm');?>";
        //var requestId = $('#request_id').val();
        var paymentMode = $('#paymentMode').val();
        $("#payment_form").load(url, {paymentMode:paymentMode }, function (data){
        });
    }
</script>
  </div>
</div>
<div class="content_wrapper_bottom"></div>
