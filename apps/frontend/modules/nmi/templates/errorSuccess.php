<?php include_partial('global/innerHeading',array('heading'=>'Order Status'));?>

<div class="global_content4 clearfix">
  <div class="wrapForm2">
    <?php
    $sf = sfContext::getInstance()->getUser();
    if($sf->hasFlash('notice')){ ?><br />
    <div id="flash_error" class="alertBox" >
      <?php
      echo nl2br($sf->getFlash('notice'));
      ?>
    </div><br />
    <?php }?>

    <div class="thanksArea">
      <p class="redBigMessage" >Payment Not successfull, please try again with another card.
        <h2>
          <center style="font-size:15px;font-weight:bold"><a href="<?php echo url_for('cart/list')?>" style="font-size:15px;font-weight:bold">Click here</a> to see your cart.</center>
        </h2>
      </p>
    </div>
  </div>
</div>