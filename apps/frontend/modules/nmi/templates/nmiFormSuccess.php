<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>
<?php use_helper('Form');?>

<?php
$vbvFlag = false;
$mcsFlag = false;
switch($paymentCardType){
    case 'jna_nmi_vbv':
        echo '<div align="center" class="redMediumMessage" >Only Verified by VISA cards are accepted. <a href="http://usa.visa.com/personal/security/index.html" target="_blank" style="font-size: 15px; font-weight: bold;">Click here </a>for more details.</div>';
        $vbvFlag = true;
        break;
    /**
     * [WP: 111] => CR: 157
     * Added condition for master card secure code for jna service...
     */
    case 'jna_nmi_mcs':
        echo '<div align="center" class="redMediumMessage" >Only MasterCard SecureCode cards are accepted.</div>';
        $mcsFlag = true;
        break;
    case 'nmi_mcs':
        case 'nmi_vbv':
            echo '<div align="center" class="redMediumMessage" >Note: Your card will be tested for eligibility of Verified by Visa or MasterCard SecureCode on submission .  <a href="javascript: void(0)" onclick=\'window.open("'.url_for('nmi/visaMasterInfo').'",
  "windowname1","width=500, height=500")\'>Know more</a></div>';
            break;
        default:
            break;
    }
    ?>
<form  id="frm_billing_info"  name="frm_billing_info" action="<?php echo $formActionURL; ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
    <?php if (!$form->getObject()->isNew()): ?>
    <input type="hidden" name="sf_method" value="put" />
    <?php endif; ?>
    <table border="0" width="100%">

        <tr>
            <td>
                <table width="100%">
                    <tr>
                        <td class="blbar" colspan="2" align="right">      <p>Billing information</p></td>
                    </tr>
                    <tr>
                        <td colspan="2" align="right">      <p class="red">*Required Information </p></td>
                    </tr>
                    <tr>
                        <td colspan="2" align="right">      <p class="red">Please enter the exact billing address which is printed on your Credit Card statement</p>

                        </td>
                    </tr>

                    <tr>
                        <td width="40%" align="left"><?php echo $form['billing-first-name']->renderLabel(); ?><span class="red">*</span></td>
                        <td align="left"><?php echo $form['billing-first-name']->render(); ?>
                            <br>
                            <div class="red" id="billing-first-name_error">
                                <?php echo $form['billing-first-name']->renderError(); ?>
                            </div>

                        </td>
                    </tr>
                    <tr>
                        <td align="left"><?php echo $form['billing-last-name']->renderLabel(); ?><span class="red">*</span></td>
                        <td align="left"><?php echo $form['billing-last-name']->render(); ?>
                            <br>
                            <div class="red" id="billing-last-name_error">
                                <?php echo $form['billing-last-name']->renderError(); ?>
                            </div>
                            <i><span class="small-text" style="font-family: Arial,Helvetica,sans-serif;font-size: 10.5px; color: #555555;">(Please avoid placing suffixes with your surname)</span></i>

                        </td>
                    </tr>

                    <tr>
                        <td align="left"><?php echo $form['billing-address1']->renderLabel(); ?><span class="red">*</span></td>
                        <td align="left"><?php echo $form['billing-address1']->render(); ?>
                            <br>
                            <div class="red" id="billing-address1_error">
                                <?php echo $form['billing-address1']->renderError(); ?>
                            </div>

                        </td>
                    </tr>

                    <tr>
                        <td align="left"><?php echo $form['billing-address2']->renderLabel(); ?></td>
                        <td align="left"><?php echo $form['billing-address2']->render(); ?>
                            <br>
                            <div class="red" id="billing-address2_error">
                                <?php echo $form['billing-address2']->renderError(); ?>
                            </div>

                        </td>
                    </tr>

                    <tr>
                        <td align="left"><?php echo $form['billing-city']->renderLabel(); ?><span class="red">*</span></td>
                        <td align="left"><?php echo $form['billing-city']->render(); ?>
                            <br>
                            <div class="red" id="billing-city_error">
                                <?php echo $form['billing-city']->renderError(); ?>
                            </div>

                        </td>
                    </tr>

                    <tr>
                        <td align="left"><?php echo $form['billing-state']->renderLabel(); ?></td>
                        <td align="left"><?php echo $form['billing-state']->render(); ?>
                            <br>
                            <div class="red" id="billing-state_error">
                                <?php echo $form['billing-state']->renderError(); ?>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td align="left"><?php echo $form['billing-postal']->renderLabel(); ?><span id="mandatoryZip" style="display:none" class="red">*</span></td>
                        <td align="left"><?php echo $form['billing-postal']->render(); ?>
                            <br>
                            <div class="red" id="billing-postal_error">
                                <?php echo $form['billing-postal']->renderError(); ?>
                            </div>

                        </td>
                    </tr>

                    <tr>
                        <td align="left"><?php echo $form['billing-country']->renderLabel(); ?><span class="red">*</span></td>
                        <td align="left"><?php echo $form['billing-country']->render(); ?>
                            <br>
                            <div class="red" id="billing-country_error">
                                <?php echo $form['billing-country']->renderError(); ?>
                            </div>

                        </td>
                    </tr>

                    <tr>
                        <td align="left"><?php echo $form['billing-email']->renderLabel(); ?><span class="red">*</span></td>
                        <td align="left"><?php echo $form['billing-email']->render(); ?>
                            <br>
                            <div class="red" id="billing-email_error">
                                <?php echo $form['billing-email']->renderError(); ?>
                            </div>

                        </td>
                    </tr>

                    <tr>
                        <td align="left"><?php echo $form['billing-phone']->renderLabel(); ?><span class="red">*</span></td>
                        <td align="left"><?php echo $form['billing-phone']->render(); ?>
                            <br>
                            <div class="red" id="billing-phone_error">
                                <?php echo $form['billing-phone']->renderError(); ?>
                            </div>

                        </td>
                    </tr>

                </table>
                <br/>
            </td>
            <td valign="top" width="50%">
                <table width="100%" style="margin:auto; margin-bottom:10px;">
                <tr>
                    <td class="blbar" colspan="2" align="right">      <p>Card information</p></td>
                </tr>

                <tr>
                    <td align="left"><?php echo $form['card_type']->renderLabel(); ?><span class="red">*</span></td>
                    <td align="left"><?php echo $paymentMode ; ?> <input type="hidden" name="cardType" id="cardType" value="<?php echo $cardType ; ?>" />
                        <br>
                        <div class="red" id="card_type_error">
                            <?php echo $form['card_type']->renderError(); ?>
                        </div>

                    </td>
                </tr>

                <tr>
                    <td align="left"><?php echo $form['billing-cc-number']->renderLabel(); ?><span class="red">*</span></td>
                    <td align="left"><?php echo $form['billing-cc-number']->render(); ?> <!-- <br> <img src="<?php // echo image_path('visa_new.gif'); ?>" alt="" class="pd4" /> -->

                        <br>
                        <div class="red" id="billing-cc-number_error">
                            <?php echo $form['billing-cc-number']->renderError(); ?>
                        </div>

                    </td>
                </tr>

                <tr>
                    <td align="left"><?php echo $form['card_holder']->renderLabel(); ?><span class="red">*</span></td>
                    <td align="left"><?php echo $form['card_holder']->render(); ?>
                        <br>
                        <div class="red" id="card_holder_error">
                            <?php echo $form['card_holder']->renderError(); ?>
                        </div>

                    </td>
                </tr>

                <tr>
                    <td align="left"><?php echo $form['billing-cc-exp']->renderLabel(); ?><span class="red">*</span></td>
                    <td align="left"><?php echo $form['billing-cc-exp']->render(); ?>
                        <br>
                        <div class="red" id="billing-cc-exp_error">
                            <?php echo $form['billing-cc-exp']->renderError(); ?>
                        </div>

                    </td>
                </tr>

                <tr>
                    <td align="left"><?php echo $form['billing-cvv']->renderLabel(); ?><span class="red">*</span>
                        <?php
                        $cvv_help = public_path('/images/',true).'question_mark.jpeg';
                        echo link_to(image_tag($cvv_help, array( 'title'=>'What is CVV?')), $this->getModuleName().'/cvvHelp', array('popup' => array('Agent Type Window', 'width=620,height=320,left=320,top=200,resizable=no,toolbar=no,location=no'), 'target' => '_blank'))
                        ?>
                    </td>
                    <td align="left"><?php echo $form['billing-cvv']->render(); ?>
                        <br>
                        <div class="red" id="billing-cvv_error">
                            <?php echo $form['billing-cvv']->renderError(); ?>
                        </div>
                        <?php echo $form['request_ip']->render(); ?>
                    </td>
                </tr>
                <?php
                  /**
                    * [WP: 104] => CR: 149
                    * If payment processor is JNA and visa card selected then only following image will be displayed...
                    */
                  if($vbvFlag){ ?>
                  <tr>
                    <td colspan="2" align="right" valign="bottom" height="221px;">
                        <div>
                        <?php
                            $vbv_logo = public_path('/images/',true).'vbv.jpg';
                            echo image_tag($vbv_logo, array( 'title'=>'Verified By Visa'));
                        ?>
                        </div>
                    </td>
                  </tr>
                  <?php } ?>

               <?php
                  /**
                    * [WP: 111] => CR: 157
                    * If payment processor is JNA and master card selected then only following image will be displayed...
                    */
                   if($mcsFlag){ ?>
                   
                  <tr>
                    <td colspan="2" align="right" valign="bottom" height="221px;">
                        <div>
                        <?php
                            $mcs_logo = public_path('/images/',true).'mcs.jpeg';
                            echo image_tag($mcs_logo, array( 'title'=>'MasterCard SecureCode'));
                        ?>
                        </div>
                    </td>
                  </tr>
                  <?php } ?>
                
            </td>
        </tr>
    </table>
    <tfoot>
        <tr>
            <td colspan="2" align="left">
                <div style="margin:auto; width:100%;" >
                    <?php echo $form['agreed']->render(); ?>
                    &nbsp;  I agree to <?php echo link_to('Terms and Conditions', 'cms/termsAndConditions', array(
                    'popup' => array('Window title', 'width=933,height=600,left=320,top=0,scrollbars=yes'))) ?>
                </div>
                <?php echo $form['agreed']->renderError(); ?>
                <div class="red" id="error_agreedterm">

                </div>
            </td>
        </tr>

        <tr>
            <td colspan="2" colspan="2">
                <!--input type="hidden" name="requestId" id="requestId" value="<?php //echo $requestId ?>" requestId is being used via session... -->
                <div style="margin:auto; width:90px;" id="payButDiv">
                    <input type="button" value="Make Payment" id="payBut" name="payBut" class="normalbutton" onclick="validateForm('1')" />
                </div>
                <div id="loader" align="center"></div>
            </td>
        </tr>
    </tfoot>
    <input type="hidden" name="billing-cc-exp" id="billing-cc-exp" value="" />
    <input type="hidden" name="thirdPartyFormActionURL" id="thirdPartyFormActionURL" value="" />
    <input type="hidden" name="paymentCardType" id="paymentCardType" value="<?php echo $paymentCardType; ?>" />
    </table>
</form>




<script>
    var found = new Array();
</script>
<?php
for($i=0;$i<count($arrCountry);$i++)
{
    ?>
<script>
    found[<?php echo $i; ?>] = '<?php echo $arrCountry[$i]; ?>';
</script>
<?php
}
?>

<script>
    function onDateChange()
    {
        var month = document.getElementById('billing-cc-exp_month').value;
        var year = document.getElementById('billing-cc-exp_year').value;
        var year1 = year.substring(2, 4);
        if(month <= 9)
        {
            var date = '0' + month + year1;
        }
        else
        {
            var date = month + year1;
        }
        document.getElementById('billing-cc-exp').value = date;

    }

    function validateForm(form)
    {
        $('#payBut').hide();
        $('#loader').html('<?php echo image_tag('../images/ajax-loader.gif'); ?>');
        var nameLen = jQuery.trim(document.getElementById('billing-first-name').value).length;
        var lastNameLen = jQuery.trim(document.getElementById('billing-last-name').value).length;
        var address1Len = jQuery.trim(document.getElementById('billing-address1').value).length;
        var address2Len = jQuery.trim(document.getElementById('billing-address2').value).length;
        var cityLen = jQuery.trim(document.getElementById('billing-city').value).length;
        var stateLen = jQuery.trim(document.getElementById('billing-state').value).length;
        var zipLen = document.getElementById('billing-postal').value.length;
        var emailLen = document.getElementById('billing-email').value.length;
        var cardHolderNameLen = jQuery.trim($('#card_holder').val()).length;

        var err  = 0;

        var firstName = jQuery.trim($('#billing-first-name').val());
        if(firstName == ""){
            $('#billing-first-name_error').html("Please enter First Name.");
            err = err+1;
        }else if(nameLen < $('#billing-first-name').val().length){
            $('#billing-first-name_error').html('Please remove blank spaces before and after First Name.');
            err = err +1;
        }else if(validateAppName(firstName) !=0){
            $('#billing-first-name_error').html("Please enter valid First Name.");
            err = err+1;
        }else if(nameLen < 2){
            $('#billing-first-name_error').html('First Name cannot be less than 2 characters.');
            err= err +1;
        }else if(nameLen > 32){
            $('#billing-first-name_error').html('First Name cannot be greater than 32 characters.');
            err= err +1;
        }else{
            $('#billing-first-name_error').html("");
        }

        var lastName = jQuery.trim($('#billing-last-name').val());
        if(lastName == ""){
            $('#billing-last-name_error').html("Please enter Last Name.");
            err = err+1;
        }else if(lastNameLen < $('#billing-last-name').val().length){
            $('#billing-last-name_error').html('Please remove blank spaces before and after Last Name.');
            err = err +1;
        }else if(validateAppName(lastName) != 0){
            $('#billing-last-name_error').html("Please enter valid Last Name.");
            err = err+1;
        }else if(lastNameLen < 2){
            $('#billing-last-name_error').html('Last Name cannot be less than 2 characters.');
            err= err +1;
        }else if(lastNameLen > 32){
            $('#billing-last-name_error').html('Last Name cannot be greater than 32 characters.');
            err= err +1;
        }else{
            $('#billing-last-name_error').html("");
        }

        if(jQuery.trim($('#billing-address1').val()) == "")
        {
            $('#billing-address1_error').html("Please enter Address 1.");
            err = err+1;
        }else if(validateString($('#billing-address1').val()) !=0)
        {
            $('#billing-address1_error').html("'Please enter correct Address 1.");
            err = err+1;
        }
        else if(address1Len < 2)
        {
            $('#billing-address1_error').html('Address1 cannot be less than 2 characters.');
            err= err +1;
        }
        else if(address1Len > 48)
        {
            $('#billing-address1_error').html('Address1 cannot be greater than 48 characters.');
            err= err +1;
        }
        else
        {
            $('#billing-address1_error').html("");
        }
        if(address2Len>0){
            if(validateString($('#billing-address2').val())!=0){
                $('#billing-address2_error').html('Please enter correct Address 2.');
                err = err+1;
            }
            else if(address2Len < 2)
            {
                $('#billing-address2_error').html('Address2 cannot be less than 2 characters.');
                err= err +1;
            }
            else if(address2Len > 48)
            {
                $('#billing-address2_error').html('Address2 cannot be greater than 48 characters.');
                err= err +1;
            }
            else{
                $('#billing-address2_error').html('');
            }
        }
        else{
            $('#billing-address2_error').html('');
        }
        if(jQuery.trim($('#billing-city').val()) == "")
        {
            $('#billing-city_error').html('Please enter City.');
            err =err+1;
        }
        else if(cityLen < 2)
        {
            $('#billing-city_error').html('City cannot be less than 2 characters.');
            err= err +1;
        }
        else if(cityLen > 48)
        {
            $('#billing-city_error').html('City cannot be greater than 48 characters.');
            err= err +1;
        }
        else if(validateString($('#billing-city').val())!=0){
            $('#billing-city_error').html('Please enter correct City.');
            err = err+1;
        }
        else{
            $('#billing-city_error').html('');
        }
        if( $('#billing-state').val() != "" && validateString($('#billing-state').val()) !=0)
        {
            $('#billing-state_error').html("Please enter valid State.");
            err = err+1;
        } else if(stateLen>0)
        {
            if(stateLen < 2)
            {
                $('#billing-state_error').html('State cannot be less than 2 characters.');
                err= err +1;
            }
            else if(stateLen > 32)
            {
                $('#billing-state_error').html('State cannot be greater than 32 characters.');
                err= err +1;
            } else {
                $('#billing-state_error').html("");
            }

        }
        else
        {
            $('#billing-state_error').html("");
        }

        if(zipLen>0){
            if(zipLen > 16)
            {
                $('#billing-postal_error').html('ZIP Code cannot be greater than 16 characters.');
                err= err +1;
            }
        }
        else{
            $('#billing-postal_error').html('');
        }
        if($('#billing-postal').val() == "")
        {
            if(inArray($("#billing-country").val(),found))
            {
                $('#billing-postal_error').html("ZIP(Postal Code) is compulsory.");
                err = err+1;
            }
            else
            {
                $('#billing-postal_error').html("");
            }
        }
        else if(validateZip($('#billing-postal').val()) !=0)
        {
            $('#billing-postal_error').html("Please enter Valid ZIP(Postal Code).");
            err = err+1;
        }
        else
        {
            $('#billing-postal_error').html("");
        }


        if($('#billing-country').val() == "")
        {
            $('#billing-country_error').html("Please enter Country.");
            err = err+1;
        }
        else
        {
            $('#billing-country_error').html("");
        }


        if(jQuery.trim($('#billing-email').val()) == "")
        {
            $('#billing-email_error').html('Please enter Email address.');
            err =err+1;
        }
        else if(validateEmail($('#billing-email').val()) !=0)
        {
            $('#billing-email_error').html("Please enter correct Email address.");
            err = err+1;
        }
        else if(emailLen < 8)
        {
            $('#billing-email_error').html('Email cannot be less than 8 characters.');
            err= err +1;
        }
        else if(emailLen > 66)
        {
            $('#billing-email_error').html('Email cannot be greater than 66 characters.');
            err= err +1;
        }
        else
        {
            $('#billing-email_error').html("");
        }


        if(jQuery.trim($('#billing-phone').val()) == "")
        {
            $('#billing-phone_error').html('Please enter Phone Number.');
            err =err+1;
        }else if(validatePhone($('#billing-phone').val()) !=0)
        {
            $('#billing-phone_error').html("Please enter valid Phone Number<br/>Phone Number should be between 8-16 digits.");
            err = err+1;
        }
        else
        {
            $('#billing-phone_error').html("");
        }


        if($('#billing-cc-number').val() == "")
        {
            $('#billing-cc-number_error').html("Please enter correct Card Number.");
            err = err+1;
        }else if(nmiCheckCardType($('#billing-cc-number').val()) !=0){
            $("#billing-cc-number_error").html('Please enter Correct Card Number.')
            err = err+1;
        }
        else
        {
            $('#billing-cc-number_error').html("");
        }


        var cardHolderName = jQuery.trim($('#card_holder').val());
        if(cardHolderName == ""){
            $('#card_holder_error').html("Please enter Card Holder Name.");
            err = err+1;
        }else if(cardHolderNameLen < $('#card_holder').val().length){
            $('#card_holder_error').html('Please remove blank spaces before and after Card Holder Name.');
            err = err +1;
        }else if(validateName(cardHolderName) != 0){
            $('#card_holder_error').html("Please enter valid Card Holder Name.");
            err = err+1;
        }else if(cardHolderNameLen < 2){
            $('#card_holder_error').html('Card Holder Name cannot be less than 2 characters.');
            err= err +1;
        }else if(cardHolderNameLen > 32){
            $('#card_holder_error').html('Card Holder Name cannot be greater than 32 characters.');
            err= err +1;
        }else{
            $('#card_holder_error').html("");
        }

        var billing_cvv = jQuery.trim($('#billing-cvv').val());
        if(billing_cvv == "")
        {
            $('#billing-cvv_error').html("Please enter CVV");
            err = err+1;
        }else{
            if($('#cardType').val() == "A" && billing_cvv.length != 4){
                $('#billing-cvv_error').html("CVV should be 4 digit long for American Express.");
                err = err+1;
            }else if($('#cardType').val() != "A" && billing_cvv.length != 3){
                $('#billing-cvv_error').html("CVV should be 3 digit long.");
                err = err+1;
            }else{
                $('#billing-cvv_error').html("");
            }
        }



        if(!ValidateExpDate($('#billing-cc-exp_year').val(),$('#billing-cc-exp_month').val()))
        {
            $('#billing-cc-exp_error').html("Invalid expiry date.");
            err = err+1;
        }
        else
        {
            $('#billing-cc-exp_error').html("");
        }


        if($('#agreed:checked').val() == null)
        {
            $('#error_agreedterm').html("Please agree to Terms & Conditions.");
            err = err+1;
        }
        else
        {
            $('#error_agreedterm').html("");
        }

        if(err == 0)
        {
            //      return true;
            var validateUrl = "<?php echo url_for('nmi/ValidateCard') ?>";

            $.post(validateUrl, $('#frm_billing_info').serialize(), function(data){

                if(jQuery.trim(data) == ''){
                    $('#loader').html('<div class="red">Connection problem found while connecting to payment server. Please try again later...</div>');
                    $('#payBut').show();
                }else{
                    if(data == "logout"){
                        window.location.reload();
                    }else{
                        var exp = data.split('#');

                        var type = exp[1].split('=');
                        if(type[1] == 'success')
                        {
                            //document.forms[0].action = exp[0];
                            $('#thirdPartyFormActionURL').val(exp[0]);
                            document.forms[0].submit();
                        }else{
                            window.location = exp[0];
                        }
                    }
                }
            });
        }else{
            $('#payBut').show();
            $('#loader').html('');
            return false;
        }



    }

    function validateZip(zip) {
        var reg = /^([A-Za-z0-9])+$/;
        if(reg.test(zip) == false) {
            return 1;
        }

        return 0;
    }

    function validateName(str)
    {
        var reg = /^[A-Za-z \-\.]*$/; //allow alphabet,spaces and hyphen only(as hyphen are allowed in passport & visa application form) ...
        if(reg.test(str) == false) {
            return 1;
        }

        return 0;
    }

    function validateAppName(str)
    {
        var reg = /^[A-Za-z \-\.]*$/; //allow alphabet,spaces and hyphen only(as hyphen are allowed in passport & visa application form) ...
        if(reg.test(str) == false) {
            return 1;
        }

        return 0;
    }

    function validateString(str)
    {
        var reg = /[^\s+]/; //allow special characters ` ' - . and space
        if(reg.test(str) == false) {
            return 1;
        }

        return 0;
    }
    function validateEmail(email) {

        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        if(reg.test(email) == false) {

            return 1;
        }

        return 0;
    }

    function validatePhone(phoneNumber) {
        //      var reg = /^((\+)?(\d{2}[-]))?(\d{10}){1}?$/;
        //var reg = /^((\+)?(\d{2}))?(\d{10-15}){1}?$/;
        var reg = /^[0-9[\]+()-]{8,16}?$/;
        if(reg.test(phoneNumber) == false) {
            return 1;
        }

        return 0;
    }

    function inArray(first,arrSecond)
    {
        for(i=0;i<arrSecond.length;i++)
        {
            if(first == arrSecond[i])
            {
                return true;
            }
        }
        return false;
    }
    // start - change by vineet for making ZIP mandatory
    $(document).ready(function()
    {
        if(inArray($("#billing-country").val(),found))
        {
            document.getElementById("mandatoryZip").style.display = '';
        }
        $("#billing-country").change(function(){
            if(inArray($("#billing-country").val(),found))
            {
                document.getElementById("mandatoryZip").style.display = '';
            }
            else
            {
                document.getElementById("mandatoryZip").style.display = 'none';
            }
        });
    });
    // end - change by vineet for making ZIP mandatory
</script>