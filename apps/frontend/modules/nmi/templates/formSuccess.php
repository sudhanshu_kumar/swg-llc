<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>
<?php use_helper('Form');?>
<?php

## Start here... ## Adding by ashwani...
$arrSameCardLimit = array();
$cartValidationObj = new CartValidation();
$arrCartCapacityLimit = $cartValidationObj->checkUserCartCapacity();
if(!empty($arrCartCapacityLimit) && count($arrCartCapacityLimit)){
      $arrSameCardLimit = $arrCartCapacityLimit;
} ## End here...
$hour = Settings::maxcard_time($arrSameCardLimit['transaction_period']);
$hour = str_replace(".","",$hour);

//if(isset($workType) && 'recharge'== $workType)
//    $action = url_for($this->getModuleName().'/rechargeProcess') ;
// else
//    $action = url_for($this->getModuleName().'/create') ;
?>
<div style="padding-right:118px;" class="redBigMessage">Due to regulations, we do not allow the use of same card more than once in <?php echo $hour; ?>, unless registered.</div>
<form  id="frm_billing_info"  name="frm_billing_info" action="<?php echo $postUrl; ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?> onsubmit="return validateForm(this)" target="vbv">
  <?php if (!$form->getObject()->isNew()): ?>
  <input type="hidden" name="sf_method" value="put" />
  <?php endif; ?>
  <table border="1" style="width:100%; margin-bottom:10px;">

    <tr>
      <td>
        <table width="100%" style="margin:auto; vertical-align:top;">
          <tr>
            <td class="blbar" colspan="2" align="right">      <p>Billing information</p></td>
          </tr>
          <tr>
            <td colspan="2" align="right">      <p class="red">*Required Information </p></td>
          </tr>
          <tr>
            <td colspan="2" align="right">      <p class="red">Please enter the exact billing address which is printed on your Credit Card statement</p>

            </td>
          </tr>

          <?php if(isset($allProfile) && count($allProfile)>0):

          echo "<tr><td>Profile</td><td>";
          foreach($allProfile as $key=>$value)
          {
            echo str_ireplace('P', 'Profile ', $value['profile']);
            if($radioChecked!=$value['id'])
            echo  radiobutton_tag("Profile", $value['id'],'',array('onclick' => 'getProfileActive(this)'));
            else
            echo  radiobutton_tag("Profile", $value['id'],1,array('onclick' => 'getProfileActive(this)'));
            echo '&nbsp;&nbsp;&nbsp;';
          }


          echo "</td></tr>";
          endif ?>



          <tr>
            <td><?php echo $form['billing-first-name']->renderLabel(); ?><span class="red">*</span></td>
            <td><?php echo $form['billing-first-name']->render(); ?>
              <br>
              <div class="red" id="billing-first-name_error">
                <?php echo $form['billing-first-name']->renderError(); ?>
              </div>

            </td>
          </tr>
          <tr>
            <td><?php echo $form['billing-last-name']->renderLabel(); ?><span class="red">*</span></td>
            <td><?php echo $form['billing-last-name']->render(); ?>
              <br>
              <div class="red" id="billing-last-name_error">
                <?php echo $form['billing-last-name']->renderError(); ?>
              </div>

            </td>
          </tr>

          <tr>
            <td><?php echo $form['billing-address1']->renderLabel(); ?><span class="red">*</span></td>
            <td><?php echo $form['billing-address1']->render(); ?>
              <br>
              <div class="red" id="billing-address1_error">
                <?php echo $form['billing-address1']->renderError(); ?>
              </div>

            </td>
          </tr>

          <tr>
            <td><?php echo $form['billing-address2']->renderLabel(); ?></td>
            <td><?php echo $form['billing-address2']->render(); ?>
              <br>
              <div class="red" id="billing-address2_error">
                <?php echo $form['billing-address2']->renderError(); ?>
              </div>

            </td>
          </tr>

          <tr>
            <td><?php echo $form['billing-city']->renderLabel(); ?><span class="red">*</span></td>
            <td><?php echo $form['billing-city']->render(); ?>
              <br>
              <div class="red" id="billing-city_error">
                <?php echo $form['billing-city']->renderError(); ?>
              </div>

            </td>
          </tr>

          <tr>
            <td><?php echo $form['billing-state']->renderLabel(); ?></td>
            <td><?php echo $form['billing-state']->render(); ?>
              <br>
              <div class="red" id="billing-state_error">
                <?php echo $form['billing-state']->renderError(); ?>
              </div>
            </td>
          </tr>

          <tr>
            <td><?php echo $form['billing-postal']->renderLabel(); ?><span id="mandatoryZip" style="display:none" class="red">*</span></td>
            <td><?php echo $form['billing-postal']->render(); ?>
              <br>
              <div class="red" id="billing-postal_error">
                <?php echo $form['billing-postal']->renderError(); ?>
              </div>

            </td>
          </tr>

          <tr>
            <td><?php echo $form['billing-country']->renderLabel(); ?><span class="red">*</span></td>
            <td><?php echo $form['billing-country']->render(); ?>
              <br>
              <div class="red" id="billing-country_error">
                <?php echo $form['billing-country']->renderError(); ?>
              </div>

            </td>
          </tr>

          <tr>
            <td><?php echo $form['billing-email']->renderLabel(); ?><span class="red">*</span></td>
            <td><?php echo $form['billing-email']->render(); ?>
              <br>
              <div class="red" id="billing-email_error">
                <?php echo $form['billing-email']->renderError(); ?>
              </div>

            </td>
          </tr>

          <tr>
            <td><?php echo $form['billing-phone']->renderLabel(); ?><span class="red">*</span></td>
            <td><?php echo $form['billing-phone']->render(); ?>
              <br>
              <div class="red" id="billing-phone_error">
                <?php echo $form['billing-phone']->renderError(); ?>
              </div>

            </td>
          </tr>

        </table>
        <br/>
      </td>
      <td style="width:50%; vertical-align:top;">
        <table width="100%" style="margin:auto; margin-bottom:10px;">
        <tr>
          <td class="blbar" colspan="2" align="right">      <p>Card information</p></td>
        </tr>

        <tr>
          <td><?php echo $form['card_type']->renderLabel(); ?><span class="red">*</span></td>
          <td><?php echo $paymentMode ; ?> <input type="hidden" name="cardType" id="cardType" value="<?php echo $cardType ; ?>" />
            <br>
            <div class="red" id="card_type_error">
              <?php echo $form['card_type']->renderError(); ?>
            </div>

          </td>
        </tr>

        <tr>
          <td><?php echo $form['billing-cc-number']->renderLabel(); ?><span class="red">*</span></td>
          <td><?php echo $form['billing-cc-number']->render(); ?> <!-- <br> <img src="<?php // echo image_path('visa_new.gif'); ?>" alt="" class="pd4" /> -->

            <br>
            <div class="red" id="billing-cc-number_error">
              <?php echo $form['billing-cc-number']->renderError(); ?>
            </div>

          </td>
        </tr>

        <tr>
          <td><?php echo $form['card_holder']->renderLabel(); ?><span class="red">*</span></td>
          <td><?php echo $form['card_holder']->render(); ?>
            <br>
            <div class="red" id="card_holder_error">
              <?php echo $form['card_holder']->renderError(); ?>
            </div>

          </td>
        </tr>

        <tr>
          <td><?php echo $form['billing-cc-exp']->renderLabel(); ?><span class="red">*</span></td>
          <td><?php echo $form['billing-cc-exp']->render(); ?>
            <br>
            <div class="red" id="billing-cc-exp_error">
              <?php echo $form['billing-cc-exp']->renderError(); ?>
            </div>

          </td>
        </tr>

        <tr>
          <td><?php echo $form['billing-cvv']->renderLabel(); ?><span class="red">*</span>
            <?php
            $cvv_help = public_path('/images/',true).'question_mark.jpeg';
            echo link_to(image_tag($cvv_help, array( 'title'=>'What is CVV?')), $this->getModuleName().'/cvvHelp', array('popup' => array('Agent Type Window', 'width=620,height=320,left=320,top=200,resizable=no,toolbar=no,location=no'), 'target' => '_blank'))
            ?>
          </td>
          <td><?php echo $form['billing-cvv']->render(); ?>
            <br>
            <div class="red" id="billing-cvv_error">
              <?php echo $form['billing-cvv']->renderError(); ?>
            </div>
            <?php echo $form['request_ip']->render(); ?>
          </td>
        </tr>

      </td>
    </tr>
  </table>
  <tfoot>
    <tr>
      <td colspan="2" colspan="2">
        <div style="margin:auto; width:100%;" >
          <?php echo $form['agreed']->render(); ?>
          &nbsp;  I agree to <?php echo link_to('Terms and Conditions', 'cms/termsAndConditions', array(
          'popup' => array('Window title', 'width=933,height=600,left=320,top=0,scrollbars=yes')
  )) ?>
        </div>
        <?php echo $form['agreed']->renderError(); ?>
        <div class="red" id="error_agreedterm">

        </div>
      </td>
    </tr>

    <tr>

      <td colspan="2" colspan="2">

        <?php if (!$form->getObject()->isNew()): ?>
        &nbsp;<?php //echo link_to('Delete', 'payeasy/delete?id='.$form->getObject()->getId(), array('method' => 'delete', 'confirm' => 'Are you sure?')) ?>

        <?php endif; ?>
        <input type="hidden" name="requestId" id="requestId" value="">
        <div style="margin:auto; width:90px;" id="payButDiv">
          <input type="submit" value="Make Payment" id="payBut" name="payBut" class="paymentbutton" onclick="validateForm('1')" />
        </div>
      </td>
    </tr>
  </tfoot>
  <input type="hidden" name="billing-cc-exp" id="billing-cc-exp" value="">

  </table>
</form>

 <script>
    var found = new Array();
  </script>
<?php
for($i=0;$i<count($arrCountry);$i++)
{
  ?>
  <script>
    found[<?php echo $i; ?>] = '<?php echo $arrCountry[$i]; ?>';
  </script>
  <?php
}
?>
<script>
  function onDateChange()
  {
    var month = document.getElementById('billing-cc-exp_month').value;
    var year = document.getElementById('billing-cc-exp_year').value;
    var year1 = year.substring(2, 4);
    if(month <= 9)
    {
      var date = '0' + month + year1;
    }
    else
    {
      var date = month + year1;
    }
    document.getElementById('billing-cc-exp').value = date;

  }
//  function noDisp()
//  {
//
//
//    var chkCard = nmiCheckCardType();
//
//    if(chkCard)
//      document.getElementById("payButDiv").style.display="none";
//      document.form[0].submit() ;
//  }
  
  function getProfileActive(val){


    var url = "<?php echo url_for('paymentGateway/getProfile');?>";
    var requestId = $(val).val();

    $.ajax({
      type: "POST",
      url: url,
      data: "requestId="+requestId,
      success: function(data) {

        var vars = data.split('##');
        var data_length = vars.length;
        var form_values = new Array();
        for(var i=0;i<data_length;i++) {
          var d = vars[i];
          var v = d.split('=>');
          var key = v[0];
          form_values[key] = v[1];
        }


        $("#ep_pay_easy_request_first_name").val(form_values['first_name']);
        $("#ep_pay_easy_request_last_name").val(form_values['last_name']);
        $("#ep_pay_easy_request_address1").val(form_values['address1']);
        $("#ep_pay_easy_request_address2").val(form_values['address2']);
        $("#ep_pay_easy_request_address2").val(form_values['address2']);
        $("#ep_pay_easy_request_town").val(form_values['town']);
        $("#ep_pay_easy_request_state").val(form_values['state']);
        $("#ep_pay_easy_request_zip").val(form_values['zip']);
        $("#ep_pay_easy_request_country").val(form_values['country']);
        $("#ep_pay_easy_request_email").val(form_values['email']);
        $("#ep_pay_easy_request_phone").val(form_values['phone']);
      }
    });


  }


  // start - change by vineet for making ZIP mandatory
    function inArray(first,arrSecond)
    {
      for(i=0;i<arrSecond.length;i++)
      {
        if(first == arrSecond[i])
        {
          return true;
        }
      }
      return false;
    }
    $(document).ready(function()
    {
        if(inArray($("#billing-country").val(),found))
        {
            document.getElementById("mandatoryZip").style.display = '';
        }
        $("#billing-country").change(function(){
            if(inArray($("#billing-country").val(),found))
            {
                document.getElementById("mandatoryZip").style.display = '';
            }
            else
            {
                document.getElementById("mandatoryZip").style.display = 'none';
            }
        });
    });
        // end - change by vineet for making ZIP mandatory

function validateZip(zip) {
    var reg = /^([A-Za-z0-9])+$/;
    if(reg.test(zip) == false) {
      return 1;
    }

    return 0;
  }
function validateForm(form)
{

   
    var err  = 0;
    if($('#billing-first-name').val() == "")
    {
        $('#billing-first-name_error').html("Please enter first name");
        err = err+1;
    }
    else
    {
        $('#billing-first-name_error').html("");
    }
    if($('#billing-last-name').val() == "")
    {
        $('#billing-last-name_error').html("Please enter last name");
        err = err+1;
    }
    else
    {
        $('#billing-last-name_error').html("");
    }

    if($('#billing-address1').val() == "")
    {
        $('#billing-address1_error').html("Please enter address1");
        err = err+1;
    }
    else
    {
        $('#billing-address1_error').html("");
    }

    if($('#billing-city').val() == "")
    {
        $('#billing-city_error').html("Please enter city");
        err = err+1;
    }
    else
    {
        $('#billing-city_error').html("");
    }

    if($('#billing-postal').val() == "")
      {
          if(inArray($("#billing-country").val(),found))
          {
              $('#billing-postal_error').html("ZIP(Postal Code) is compulsory.");
              err = err+1;
          }
          else
          {
              $('#billing-postal_error').html("");
          }
      }
      else if(validateZip($('#billing-postal').val()) !=0)
      {
          $('#billing-postal_error').html("Please enter Valid ZIP(Postal Code)");
          err = err+1;
      }
      else
      {
          $('#billing-postal_error').html("");
      }


    if($('#billing-country').val() == "")
    {
        $('#billing-country_error').html("Please enter country");
        err = err+1;
    }
    else
    {
        $('#billing-country_error').html("");
    }

    
    if($('#billing-email').val() == "")
    {
        $('#billing-email_error').html("Please enter Email");
        err = err+1;
    }
    else if(validateEmail($('#billing-email').val()) !=0)
    {
        $('#billing-email_error').html("Please enter Valid Email");
        err = err+1;
    }
    else
    {
        $('#billing-email_error').html("");
    }

    
    if($('#billing-phone').val() == "")
    {
        $('#billing-phone_error').html("Please enter Mobile number");
        err = err+1;
    }else if(validatePhone($('#billing-phone').val()) !=0)
    {
        $('#billing-phone_error').html("Please enter Valid Mobile number");
        err = err+1;
    }
    else
    {
        $('#billing-phone_error').html("");
    }

    
    if($('#billing-cc-number').val() == "")
    {
        $('#billing-cc-number_error').html("Please enter card  number");
        err = err+1;
    }else if(nmiCheckCardType($('#billing-cc-number').val()) !=0){
        $("#billing-cc-number_error").html('Please enter correct Card Number')
        err = err+1;
    }
    else
    {
        $('#billing-cc-number_error').html("");
    }

    
    if($('#card_holder').val() == "")
    {
        $('#card_holder_error').html("Please enter card  number");
        err = err+1;
    }
    else
    {
        $('#card_holder_error').html("");
    }

    
    if($('#card_holder').val() == "")
    {
        $('#card_holder_error').html("Please enter card name");
        err = err+1;
    }
    else
    {
        $('#card_holder_error').html("");
    }

    
    if($('#billing-cvv').val() == "")
    {
        $('#billing-cvv_error').html("Please enter cvv");
        err = err+1;
    }
    else
    {
        $('#billing-cvv_error').html("");
    }

    if(err == 0)
    {

        return true;
//        if(submit_form == 1)
//        {
//            $('#pfm_create_user_form').submit();
//        }
    }else{
        return false;
    }



}

function validateEmail(email) {
    
    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    if(reg.test(email) == false) {
        
      return 1;
    }

    return 0;
  }

function validatePhone(phoneNumber) {
  //      var reg = /^((\+)?(\d{2}[-]))?(\d{10}){1}?$/;
  //var reg = /^((\+)?(\d{2}))?(\d{10-15}){1}?$/;
  var reg = /^[0-9 [\]+()-]{8,20}?$/;
  if(reg.test(phoneNumber) == false) {
      return 1;
  }

  return 0;
}
</script>