<?php $sf = sfContext::getInstance()->getUser();
if($sf->hasFlash('notice')){
  ?>
<div id="flash_notice" class="alertBox" >
<?php
  echo nl2br($sf->getFlash('notice'));

  ?>
</div>
<?php }?>
<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">
    <?php
use_helper('Form');
use_helper('Pagination');

include_partial('global/innerHeading',array('heading'=>'Application Listing'));

 ?>
 <?php if($isFound) {
      $i = 1;
      ?>
    <div class="clear"></div>
    <div class="tmz-spacer"></div>
      <div id="nxtPage">
        <table width="99%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
          <tr>            
          <td class="blbar" colspan="10" align="right"><div style="float:left">Application Details</div>
            <span>Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total <b><?php echo $pager->getNbResults(); ?></b> results</span></td>
          </tr>
<tr>
  <td width="4%" ><span class="txtBold">S. No.</span></td>
  <td width="9%"><span class="txtBold">Application type</span></td>
  <td width="9%"><span class="txtBold">App Id</span></td>
  <td width="12%"><span class="txtBold">Reference No</span></td>
  <td width="14%"><span class="txtBold">Applicant Name</span></td>
  <td width="9%"><span class="txtBold">Date of Birth</span></td>
  <td width="6%"><span class="txtBold">NIS Payment Status</span></td>
  <td width="6%"><span class="txtBold">Paid On</span></td>
  <td width="10%"><span class="txtBold">Email Id</span></td>
  <td width="4%"><span class="txtBold">Action</span></td>
</tr>
           <tbody>
            <?php
            
            if($pager->getNbResults()>0)
            {
//              $finalStatus = $nisHelper->isAllReadyRefunded($sf_params->get('order_no'));
//              $finalStatus= $finalStatus->getRawValue();
//              $fStatus = null;
//              if(isset ($finalStatus) && is_array($finalStatus) && count($finalStatus)>0){
//                $fStatus = $finalStatus['action'];
//              }

              foreach ($pager->getResults() as $result):
              //            echo "<pre>";print_r(get_class_methods($result));die;
              $app['passport_id'] = $result['passport_id'];
              $app['visa_id'] = $result['visa_id'];
              $app['freezone_id'] = $result['freezone_id'];
              $app['visa_arrival_program_id'] = $result['visa_arrival_program_id'];
              $appDetails = $appObj->getApplicationDetails($app);

              $encryptAapplicationId = SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT($appDetails['id']));

              if($appDetails['app_type']=='passport')
              {
                  /**
                   * [WP: 096]
                   */
                  ##$printUrl = url_for("passport/showPrintSuccess?id=".SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT($item->getAppId())));
                  $printUrl = Functions::printApplicationURL('passport', $encryptAapplicationId);
              }
              if($appDetails['app_type'] =='visa')
              {
                  /**
                   * [WP: 096]
                   */
                  //$printUrl = url_for("visa/printVisaApp?id=".SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT($appDetails['id'])));
                  $printUrl = Functions::printApplicationURL('visa', $encryptAapplicationId);
              }
              if($appDetails['app_type'] =='vap')
              {
                  $printUrl = url_for("visaArrival/printVisaApp?id=".SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT($appDetails['id'])));
              }
              if($appDetails['app_type']=='freezone')
              {
                  /**
                   * [WP: 096]
                   */
                  if($appDetails['visacategory_id'] == 102){
                    //$printUrl = url_for("visa/printFreezone?id=".SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT($appDetails['id'])));
                    $printUrl = Functions::printApplicationURL('reentryfreezone', $encryptAapplicationId);
                  }else{
                    //$printUrl = url_for("visa/printVisaApp?id=".SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT($appDetails['id'])));
                    $printUrl = Functions::printApplicationURL('entryfreezone', $encryptAapplicationId);
                  }
              }
              //            echo "<pre>";print_R($appDetails);die;
              $finalStatus = $nisHelper->isAllAppReadyRefunded($sf_params->get('order_no'),$appDetails['id']);
              $finalStatus= $finalStatus->getRawValue();
              $fStatus = null;
              if(isset ($finalStatus) && is_array($finalStatus) && count($finalStatus)>0){
                  $fStatus = $finalStatus['action'];
              }
             
             
              ?>
            <tr>
              <td width="4%"><?php echo $i;?></td>
              <td width="9%"><span><?php echo ($appDetails['app_type'] == 'vap')?sfConfig::get('app_visa_arrival_title'):strtoupper($appDetails['app_type']);?></span></td>
              <td width="10%"><span><a href="<?php echo url_for('requestRefund/applicationDetailsInfo?appId='.Settings::encryptInput($appDetails['id']).'&type='.Settings::encryptInput($appDetails['app_type']).'&orderNumber='.Settings::encryptInput($orderNumber)) ?>"><?php echo $appDetails['id'];?></a></span></td>
              <td width="9%"><span><?php echo $appDetails['ref_no'];?></span></td>
              <td width="14%"><span><?php echo $appDetails['name'];?></span></td>
              <td width="9%"><span><?php echo date_format(date_create($appDetails['dob']), 'd-m-Y');?></span></td>
              <?php if(isset ($fStatus) && $fStatus!=null){ ?>
              <td width="9%"><span><?php echo strtoupper($fStatus);?></span></td>
              <?php } else{ ?>
              <td width="9%"><span><?php echo $appDetails['status'];?></span></td>
                    <?php  }  ?>
            <td width="9%"><span>
              <?php if($appDetails['status'] != 'New') echo date_format(date_create($ipay4meOrderDetails[0]['paid_at']), 'd-m-Y'); else echo "--"; ?>
              </span></td>
              <td width="10%"><span><?php echo $ipay4meOrderDetails[0]['email'];?></span></td>
            <td width="4%"><span onclick="openPopUp('<?php echo $printUrl; ?>');"><img src="<?php echo image_path('print_icon.png'); ?>" alt="Print" title="Print" class="hand" /></span></td>
            </tr>
            <?php
            $i++;
            endforeach; ?>
            <tr>
            <td class="blbar" colspan="10" height="25px" align="right"><div class="paging pagingFoot">
                  <?php
                  echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName()).'?orderNumber='.Settings::encryptInput($orderNumber));
                  ?>
              </div></td>
            </tr>
            <?php } else { ?>
          <tr>
            <td  align='center' class='red' colspan="10">No Results found</td>
          </tr>
            <?php } ?>
          </tbody>
        </table>
        <br>
      </div>
  <?php

      } ?>
    </div>
  </div>
<div class="content_wrapper_bottom"></div>
  <script>
   function openPopUp(url){
      url = url + '/mode/cart'
      window.open (url, "mywindow","status=1,toolbar=0,width=1000px,height=800px,location=0,menubar=0,scrollbars=1");
     // alert(url);
    }

  </script>
