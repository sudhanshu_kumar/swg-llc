<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">
<?php include_partial('global/innerHeading',array('heading'=>"Applicant's Details"));?>
    <div class="clear"></div>
    <div class="tmz-spacer"></div>
<?php if($sf_request->getParameter('p') !='i') {?>
  <?php  echo ePortal_popup("Please Keep This Safe","<p>You will need it later</p><h5>Application Id: ".$passport_application[0]['id']."</h5><h5> Reference No: ".$passport_application[0]['ref_no']."</h5>");  ?>
  <?php if(isset($chk)){ echo "<script>pop();</script>"; } ?>
  <?php }

?>
<!--div id="waitPayment" style="display:none;position:fixed;_position:absolute;top:20%;left:45%;padding:50px 40px;background:#fff;border:1px solid #999;z-index:99;line-height:30px;"> <?php echo image_tag('/images/ajax-loader.gif', array('alt' => 'Checking your Payment Attempts status.','border'=>0 )); ?><br/> Loading payment status...</div-->
</center>
<?php
$sf = sfContext::getInstance()->getUser();
if($sf->hasFlash('notice')){ ?>
<div id="flash_notice" class="alertBox" >
  <?php
  echo nl2br($sf->getFlash('notice'));
  ?>
    </div>
    <br/>
<?php }?>
<?php
$sf = sfContext::getInstance()->getUser();
if($sf->hasFlash('error')){ ?>
<div id="flash_error" class="alertBox" >
  <?php
  echo nl2br($sf->getFlash('error'));
  ?>
    </div>
    <br/>
<?php }?>
<form action="<?php echo url_for('cart/list');// echo secure_url_for('payments/ApplicationPayment') ?>" method="POST" class="dlForm multiForm">
<div  id="Reciept" >
        <table cellpadding="0" cellspacing="0" border="0" width="99%">
          <tr>
            <td class="blbar" colspan="2"><strong><?php echo ePortal_legend("Profile"); ?></strong></td>
          </tr>
          <tr>
            <td><label >Full name:</label ></td>
            <td><?php if($type=='passport'){ ?>
        <?php echo ePortal_displayName($passport_application[0]['title_id'],$passport_application[0]['first_name'],@$passport_application[0]['mid_name'],$passport_application[0]['last_name']); ?>
        <?php } else if($type=='visa'){?>
        <?php echo ePortal_displayName($passport_application[0]['title'],$passport_application[0]['other_name'],@$passport_application[0]['middle_name'],$passport_application[0]['surname']); ?>
        <?php } else if($type=='freezone'){ ?>
        <?php echo ePortal_displayName($passport_application[0]['title'],$passport_application[0]['other_name'],@$passport_application[0]['middle_name'],$passport_application[0]['surname']);  ?>
        <?php } else if($type=='vap'){ ?>
        <?php echo ePortal_displayName($passport_application[0]['title'],$passport_application[0]['first_name'],@$passport_application[0]['middle_name'],$passport_application[0]['surname']);  ?>
        <?php } ?>
    <div class="noPrint"><a href ="<?php echo url_for('requestRefund/refundPayment?orderNumber='.Settings::encryptInput($orderNumber).'&appId='.Settings::encryptInput($appId).'&type='.Settings::encryptInput($type).'&mode=name') ?>" class="green_link">Request to Edit Name</a></div></td>
          </tr>
          <tr>
            <td><label >Gender:</label ></td>
            <td><?php if($type=='passport'){ ?>
        <?php echo $passport_application[0]['gender_id']; ?>
        <?php } else if($type=='visa'){ ?>
        <?php echo $passport_application[0]['gender']; ?>
        <?php } else if($type=='freezone') { ?>
        <?php echo $passport_application[0]['gender']; ?>
        <?php } else if($type=='vap') { ?>
        <?php echo $passport_application[0]['gender']; ?>
              <?php } ?></td>
          </tr>
          <tr>
            <td><label >Date of birth:</label ></td>
            <td><?php $datetime = date_create($passport_application[0]['date_of_birth']); echo date_format($datetime, 'd/F/Y'); ?>
              <div class="noPrint"><a href ="<?php echo url_for('requestRefund/refundPayment?orderNumber='.Settings::encryptInput($orderNumber).'&appId='.Settings::encryptInput($appId).'&type='.Settings::encryptInput($type).'&mode=dob') ?>" class="green_link">Request to Edit DOB</a></div></td>
          </tr>
          <tr>
            <td><label >Email:</label ></td>
            <td><?php if($passport_application[0]['email']!='') echo $passport_application[0]['email']; else echo "--"?></td>
          </tr>
  <?php if($type=='passport'){?>
          <tr>
            <td><label >Country Of Origin:</label ></td>
            <td><?php echo $passport_application[0]['cName']; ?></td>
          </tr>
  <?php if($passport_application[0]['sName']!='') { ?>
          <tr>
            <td><label >State of origin:</label ></td>
            <td><?php echo $passport_application[0]['sName']; ?></td>
          </tr>
  <?php } ?>
          <tr>
            <td><label >Occupation:</label ></td>
            <td><?php if($passport_application[0]['occupation']!=""){ echo $passport_application[0]['occupation'];}else{ echo '--';} ?></td>
          </tr>
  <?php  } ?>
          <tr>
            <td class="blbar" colspan="2"><strong><?php echo ePortal_legend("Application Information"); ?></strong>
              <?php if($type=='passport'){?></td>
          </tr>
          <tr>
            <td><label >Passport type:</label ></td>
            <td><?php echo $passport_application[0]['passportType']; ?></td>
          </tr>
  <?php  } ?>
<?php if(isset($passport_application[0]['PassportApplicationDetails'])) { ?>
          <tr>
            <td><label >Request Type:</label ></td>
            <td><?php echo $passport_application[0]['PassportApplicationDetails']['request_type_id']; ?></td>
          </tr>
  <?php } ?>
          <tr>
            <td><label >Date:</label ></td>
            <td><?php $applicationDatetime = date_create($passport_application[0]['created_at']); echo date_format($applicationDatetime, 'd/F/Y');?></td>
          </tr>
          <tr>
            <td><label >Application Id:</label ></td>
            <td><?php echo $passport_application[0]['id']; ?></td>
          </tr>
          <tr>
            <td><label >Reference No:</label ></td>
            <td><?php echo $passport_application[0]['ref_no']; ?></td>
          </tr>
          <tr>
            <td class="blbar" colspan="2"><strong><?php echo ePortal_legend("Processing Information"); ?></strong></td>
          </tr>
          <tr>
            <td><label>
              <?php if($type =='passport'){ echo 'Country:'; } else{ echo 'Applying Country:';} ?>
              </label ></td>
            <td><?php if($type=='passport'){echo $passport_application[0]['passportPCountry']; }else {echo
    $passport_application[0]['CurrentCountry']['country_name']; }?></td>
          </tr>
  <?php if($type=='passport'){?>
          <tr>
            <td><label >State:</label ></td>
            <td><?php if($passport_application[0]['passportPState']!='')echo $passport_application[0]['passportPState']; else echo 'Not Applicable'; ?></td>
          </tr>
  <?php }?>
  <tr>
            <td><label >
              <?php if($type =='passport'){ echo 'Embassy:'; } else if($type == "visa") { echo 'Applying Embassy:';}else {echo "Center:" ;} ?>
              </label ></td>
            <td><?php if($type=='passport'){
                        if($passport_application[0]['passportPEmbassy']==''){
                            echo 'Not Applicable';
                        }else{
                            echo $passport_application[0]['passportPEmbassy'];
                        }
                    }else if($type == 'freezone'){
                        if($passport_application[0]['center'] == ''){
                            echo 'Not Applicable';
                        }else{
                            echo $passport_application[0]['center'];
                        }
                } elseif($type=='vap'){
                 $processingCenter = Doctrine::getTable('GlobalMaster')->getDetail($passport_application[0]['processing_center_id']);   
                 if($processingCenter!='')
                 {
                     echo $processingCenter[0]['var_value'];
                 } else {
                     echo 'Not Applicable';
                 }
                }
                else{
                        echo $passport_application[0]['VisaApplicantInfo']['EmbassyMaster']['embassy_name'];
               }?>
              <div class="noPrint"><a href ="<?php echo url_for('requestRefund/refundPayment?orderNumber='.Settings::encryptInput($orderNumber).'&appId='.Settings::encryptInput($appId).'&type='.Settings::encryptInput($type).'&mode=office') ?>" class="green_link" >Request to Edit Processing Information</a></div></td>
          </tr>
          <?php if($type=='passport'){?>
  <tr>
            <td><label >Passport Office:</label ></td>
            <td><?php if($passport_application[0]['passportPOffice']!='') echo $passport_application[0]['passportPOffice']; else echo 'Not Applicable'; ?></td>
  </tr>
          <?php }?>

  <?php if($type != 'vap'){?>
  <tr>
    <td><label >Interview Date:</label ></td>
            <td><?php
      if($passport_application[0]['ispaid'] == 1)
      {
          if($type =='passport'){
              if(($passport_application[0]['paid_local_currency_amount'] == 0) && ($passport_application[0]['paid_dollar_amount'] == 0))
              {
                  echo "Check the Passport Office / Embassy / High Commission";
              }else
              {
                  $interviewDatetime = date_create($passport_application[0]['interview_date']); echo date_format($interviewDatetime, 'd/F/Y');
              }
          }
         else{
             if(($passport_application[0]['paid_naira_amount'] == 0) && ($passport_application[0]['paid_dollar_amount'] == 0))
              {
                  echo "Check the Passport Office / Embassy / High Commission";
              }else
              {
                  $interviewDatetime = date_create($passport_application[0]['interview_date']); echo date_format($interviewDatetime, 'd/F/Y');
              }
          }
      }else{
        echo "Available after Payment";
      }
      ?></td>
  </tr>
  <?php } ?>
<?php
$isPaid = $passport_application[0]['ispaid'];
?>
        </table>
        <div class="clear">&nbsp;</div>
<div class="pixbr XY20">
<center id="multiFormNav">
<div align="center">
              <table align="center" class="nobrd">
                <tr>
                    <td align="center"><input type="button" class="normalbutton"  onclick="window.location.href='<?php  echo url_for('requestRefund/refundPayment?&orderNumber='.Settings::encryptInput($orderNumber)) ?>'"  value="Contact Support">
      </td>
                  <td align="center"><input type="button" class="normalbutton"  onclick="javascript:window.open('<?= $ackUrl ?>','MyPage','width=750,height=700,scrollbars=1,resizable=yes');"  value="Print Acknowledgement">
      </td>
                  <td align="center"><input type="button" class="normalbutton"  onclick="javascript:window.open('<?= $payReceiptUrl ?>','MyPage','width=750,height=700,scrollbars=1,resizable=yes');"  value="Print Receipt">
      </td>
    </tr>
              </table>
            </div>
</center>
</div>
</div>
<input type ="hidden" value="<?php echo $getAppIdToPaymentProcess;?>" name="appDetails" id="appDetails"/>
</form>
</div>
</div>
<div class="content_wrapper_bottom"></div>
