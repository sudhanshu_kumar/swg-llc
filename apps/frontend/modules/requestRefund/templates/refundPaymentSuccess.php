  <?php
  include_partial('orderDetail', array('orderNumber'=>$orderNumber,'amount'=>$amount, 'currency' => $currency)) ?>
  <form id='refundForm' name="refundForm" action="<?php echo url_for('requestRefund/requestToAdmin') ?>" method="post" enctype="multipart/form-data" >
    <?php if($isFound) {
      $i = 1;
      ?>
  <div class="tmz-spacer"></div>
      <div id="nxtPage">
    <table width="99%" border="0" cellpadding="0" cellspacing="0" class="dataTable" align="center">
      <br/>
          <tr class="blbar" colspan="3">
            <td class="blbar" align="left" colspan="2">Please Select All applicable applications: </td>
        <td class="blbar" colspan="1" align="right"><span>Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total <b><?php echo $pager->getNbResults(); ?></b> results</span></td>
          </tr>
          <tr>
        <td width="1%" ><span class="txtBold">
          <input type="checkbox" name="chk_all" id="chk_all" onclick="checkAll(document.refundForm,'chk_fee[]');">
          </span></td>
            <td width="9%"><span class="txtBold">Application type</span></td>
            <td width="9%"><span class="txtBold">App Id</span></td>
          </tr>
          <tbody>
            <?php
            if($pager->getNbResults()>0)
            {

              foreach ($pager->getResults() as $result):
              //                          echo "<pre>";print_r($result['freezone_id']);die;
              $app['passport_id'] = $result['passport_id'];
              $app['visa_id'] = $result['visa_id'];
              $app['freezone_id'] = $result['freezone_id'];
              $app['visa_arrival_program_id'] = $result['visa_arrival_program_id'];

              $appDetails = $appObj->getApplicationDetails($app);
              //            echo "<pre>";print_R($appDetails);die;
              $finalStatus = $nisHelper->isAllAppReadyRefunded($sf_params->get('orderNumber'),$appDetails['id']);
              $finalStatus= $finalStatus->getRawValue();
              $fStatus = null;
              if(isset ($finalStatus) && is_array($finalStatus) && count($finalStatus)>0){
                $fStatus = $finalStatus['action'];
              }
              ?>
            <tr>
          <td width="4%"><?php
                $value = $appDetails['id']."_".$appDetails['app_type'];
                if($value == $selectedApp){ ?>
                <input type="checkbox" name="chk_fee[]" id="<?= "chk_fee".$i; ?>" value="<?= $value;?>" checked  OnClick="UncheckMain(document.refundForm,document.refundForm.chk_all,'chk_fee');getAppDetail(<?php echo $appDetails['id']; ?>,'<?php echo $appDetails['app_type'];?>',<?php echo "chk_fee".$i; ?>);">
                <?php }else{ ?>
                <input type="checkbox" name="chk_fee[]" id="<?= "chk_fee".$i; ?>" value="<?= $value;?>" OnClick="UncheckMain(document.refundForm,document.refundForm.chk_all,'chk_fee');getAppDetail(<?php echo $appDetails['id']; ?>,'<?php echo $appDetails['app_type'];?>',<?php echo "chk_fee".$i; ?>);">
                      <?php } ?>
              </td>
              <td width="9%"><span><?php echo ($appDetails['app_type'] == 'vap')?sfConfig::get('app_visa_arrival_title'):strtoupper($appDetails['app_type']);?></span></td>
              <td width="10%"><span><?php echo $appDetails['id'];?></span></td>
            </tr>
            <?php
            $i++;
            endforeach; ?>
            <tr>
          <td class="blbar" colspan="11" height="25px" align="right"><div class="paging pagingFoot">
                  <?php
                  echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName()).'?orderNumber='.$sf_params->get('orderNumber'));
                  ?>
            </div></td>
            </tr>
            <?php } else { ?>
        <tr>
          <td  align='center' class='red' colspan="11">No Results found</td>
        </tr>
            <?php } ?>
          </tbody>
        </table>
        <br>
      </div>
        <?php

      } ?>
  <table width="99%" cellpadding="0" cellspacing="0" border="0">
      <tr id="supportCategory_row" >
      <td><?php echo $form['reason']->renderLabel(); ?><span class="red"></span></td>
      <td><?php echo $form['reason']->render(); ?> <br>
        <div class="red" id="reason_error"> <?php echo $form['reason']->renderError(); ?> </div></td>
      <td height="100" style="border-left:0px;"><div class="" id="description_div" align="justify"> </div></td>
      <input type="hidden" name="hdn_description" id="hdn_description" value="">
      </tr>
    <tr id="orderNumber_row" class="no_display">
        <td ><?php echo $form['order_number']->renderLabel(); ?><span class="red"></span></td>
      <td colspan="2"><?php echo $form['order_number']->render(); ?> <br>
        <div class="red" id="order_number_error"> <?php echo $form['order_number']->renderError(); ?> </div></td>
      </tr>
    <tr id="orderApplication_row" class="no_display"> </tr>
    <tr id='tr_reason' class="no_display"> </tr>
    <tr id='tr_duplicate' class="no_display"> </tr>
    <tr id="tr_detail" class="no_display">
      <td colspan="3" ><table  width="99%">
            <tr>
              <td width="24%">Title<font color=red>*</font></td>
              <td colspan="2"><select name="title" id="sel_title">
                  <option value="">Select Title</option>
                  <option value="MR">Mr</option>
                  <option value="MRS">Mrs</option>
                  <option value="MISS">Miss</option>
                  <option value="DR">Dr</option>
                </select>
              <div class="red" id="sel_title_error"> </div></td>
            </tr>
            <tr>
              <td>First Name<font color=red>*</font></td>
              <td colspan="2"><input id="firstname" class="txt-input" type="text" maxlength="255" value="" name="first_name"/>
              <div class="red" id="firstname_error"> </div></td>
            </tr>
            <tr>
              <td>Middle Name</td>
              <td colspan="2"><input id="middlename" class="txt-input" type="text" maxlength="255" value="" name="middle_name"/>
              <div class="red" id="middlename_error"> </div></td>
            </tr>
            <tr>
              <td>Last Name<font color=red>*</font></td>
              <td colspan="2"><input id="lastname" class="txt-input" type="text" maxlength="255" value="" name="last_name"/>
              <div class="red" id="lastname_error"> </div></td>
            </tr>
        </table></td>
    </tr>
    <tr id="tr_dob" class="no_display">
        <td ><?php echo $form['date_of_birth']->renderLabel(); ?><span class="red"></span></td>
      <td colspan="2"><?php echo $form['date_of_birth']->render(); ?> <br>
        <div class="red" id="date_of_birth_error"> <?php echo $form['date_of_birth']->renderError();?> </div></td>
      </tr>
      <?php if($result['freezone_id'] && $mode =='office') { ?>
    <tr id=tr_center class="no_display">
        <td><?php echo $form['processing_center_id']->renderLabel(); ?><span class="red"><font color=red>*</font></span></td>
      <td colspan="2"><?php echo $form['processing_center_id']->render(); ?> <br>
        <div class="red" id="processing_center_id_error"> <?php echo $form['processing_center_id']->renderError();?> </div></td>
    </tr>
      <?php } else { ?>
    <tr id="tr_embassy" class="no_display">
      <td colspan="3" ><table width="99%">
            <tr>
              <td width="24%"><?php echo $form['processing_country_id']->renderLabel(); ?><span class="red"><font color=red>*</font></span></td>
            <td colspan="2"><?php echo $form['processing_country_id']->render(); ?> <br>
              <div class="red" id="processing_country_id_error"> <?php echo $form['processing_country_id']->renderError();?> </div></td>
            </tr>
            <tr>
              <td>Processing Embassy / Center<span class="red"><font color=red>*</font></span></td>
              <td colspan="2"><select id="order_processing_embassy_id" name="processing_embassy_id" style="width:210px;">
                  <option value="">Please Select Processing Embassy / Center</option>
                </select>
                <br>
                <div class="red" id="processing_embassy_id_error">
                  <?php //echo $form['processing_embassy_id']->renderError();?>
              </div></td>
          </tr>
        </table></td>
    </tr>
      <?php } ?>
    <tr id="duplicate_app" class="no_display">
        <td id="duplicate_app_1" colspan="3">
      </tr>
      <tr>
        <td><?php echo $form['Message']->renderLabel(); ?><span class="red"></span></td>
      <td colspan="2"><?php echo $form['Message']->render(); ?> <br>
        <div class="red" id="Message_error"> <?php echo $form['Message']->renderError(); ?> </div></td>
      </tr>
      <input type="hidden" name="hdn_order" id="hdn_order" value="<?php echo $orderNumber ; ?>">
      <input type="hidden" name="hdn_amount" id="hdn_amount" value="<?php echo $amount ; ?>">
      <input type="hidden" name="duplicate_order_num" id="duplicate_order_num" value="">
      <input type="hidden" name="hdn_mode" id="hdn_mode" value="<?php echo $mode ; ?>">
      <input type="hidden" name="hdn_app" id="hdn_app" value="<?php echo $appType ; ?>">
      <input type="hidden" name="hdn_app_id" id="hdn_app_id" value="<?php echo $appId ; ?>">
      <input type="hidden" name="hdn_selectedCat" id="hdn_selectedCat" value="<?php echo $selectedCat ; ?>">
      <input type="hidden" name="hdn_selectedSubCat" id="hdn_selectedSubCat" value="<?php echo $selectedSubCat ; ?>">
    <tr id="user_div"> <?php echo $form->renderGlobalErrors(); ?>
      <td width="24%">Upload Proof<span id="user_proof_mandatory" class="no_display"><font color="red">*</font></span></td>
      <td colspan="2">
        <div id ="uploadFile_div" >
            <input type="file" id="order_user_proof" name="order_user_proof" onchange="submitIframe();" >
        </div>        
        <br>
          <font color="red"> NOTE: Please provide proof to justify above reason.</font><br>
          -Please upload JPG/GIF/PNG images only. <br>
          -File size upto 400 KB.
          <div class="red" id="user_proof_error"> <?php echo $form['user_proof']->renderError(); ?> </div>
        <iframe name="uploadIframe" class="no_display" frameborder="0"></iframe></td>
      </tr>
      <tr>
        <td colspan="3"><div align="center">
          <p class="fnt_size_large"> We encourage you to submit your support queries using the form above as it provides all relevant information regarding your application.
              If you have have any other query regarding your application which is not covered above,
            please drop an email to <a href="mailto:<?php echo sfConfig::get('app_support_email');?>"><?php echo sfConfig::get('app_support_email');?></a> </p>
            <br />
          <p class="fnt_size_large"><b>Disclaimer</b>: Your request is subjected to further analysis by our support team (including eligibility for refund if applicable).</p>
          <br />
          <div class="fnt_size_large">
              <input type="checkbox" name="chk_policy" id="chk_policy" value="P">
            <b>I have read & agree with <?php echo link_to('SW GLOBAL LLC Refund Policy',url_for('cms/refundPolicy'));  ?>.</b> </div>
          <div class="red" id="chk_policy_error"> <?php echo $form['Message']->renderError(); ?> </div>
        </div></td>
      </tr>
      <input type="hidden" value="<?php echo $selectedApp?>" name="hdn_seleApp" id="hdn_seleApp">
      <input type="hidden" value="<?php echo $selectedSubCat?>" name="hdn_seleSubCat" id="hdn_seleSubCat">
      <tr>
        <td colspan="3"><div align="center">
            <?php   //echo submit_tag('Submit',array('class' => 'paymentbutton','id'=>'smb_frm')); ?>
          <input type="button" value="Submit" id="smb_frm" name="smb_frm" class = "normalbutton" onclick="validateForm(document.refundForm);">
        </div></td>
      </tr>
    </table>
  </form>
</div>
</div>
<div class="content_wrapper_bottom"></div>

<script type="text/javascript">

  function submitIframe(){
      $("#smb_frm").val('Please wait...');
      $("#smb_frm").attr('disabled', "disabled");	
      document.forms[0].action = "<?php echo url_for($sf_context->getModuleName().'/uploadImage');?>";
      document.forms[0].target = "uploadIframe";
      document.forms[0].submit();
  }

  function validateForm(fmobj){
    var err  = 0;
    if($('#order_reason').val() == "Request for change Application Details"){
      var j= 0;
      $("#nxtPage :checked").each(function() {

        if($(this).attr('id') != 'chk_all'){
          j++;
        }

      });
      if(j>1){
        alert('Please select only one application at a time for changing details of the application.');
        err = err+1;
      }
    }
    if($('#order_reason').val() == "")
    {
      $('#reason_error').html("Please enter reason");
      err = err+1;
    }else
    {
      $('#reason_error').html(" ");
    }
   if($('#order_reason').val() == "Request for change Application Details" || $('#order_reason').val() == "Request for Refund"){
       if(!$('#req').length > 0){
         alert('Tampering URL is Not Allowed');
        window.location.href= '<?php echo url_for('requestRefund/refundPayment?orderNumber='.$orderNumber);?>';
        return false;
       }
     }
    if($('#req').val() == "")
    {
      $('#req_error').html("Please Select Sub Support Category");
      err = err+1;
    }else
    {
      $('#erq_error').html(" ");
    }


    if($('#req').val() == "Date Of Birth"){
      if($('#order_date_of_birth_day').val() == "" )
      {
        $('#date_of_birth_error').html("Please enter Date Of Birth");
        err = err+1;
      }else
      {
        $('#date_of_birth_error').html(" ");
      }


      if($('#order_date_of_birth_month').val() == "" )
      {
        $('#date_of_birth_error').html("Please enter Date of Birth");
        err = err+1;
      }else
      {
        $('#date_of_birth_error').html(" ");
      }

      if($('#order_date_of_birth_year').val() == "" )
      {
        $('#date_of_birth_error').html("Please enter Date of Birth");
        err = err+1;
      }else
      {
        $('#date_of_birth_error').html(" ");
      }
      /* Getting current year */
      var current_date = new Date();
      /* Converting user input date of birth to javascript data format... */
      var date_of_birth = new Date($('#order_date_of_birth_year').val(),$('#order_date_of_birth_month').val()-1,$('#order_date_of_birth_day').val());
      if(date_of_birth.getTime()>current_date.getTime()) {
        $('#date_of_birth_error').html("Date of Birth cannot be a future date");
        err = err+1;
        return false;
      }else {
          $('#date_of_birth_error').html(" ");
      }

    }
    if($('#req').val() == "Name"){
      if($('#sel_title').val() == "")
      {
        $('#sel_title_error').html("Please select title");
        err = err+1;
      }else
      {
        $('#sel_title_error').html(" ");
      }


      if($('#firstname').val() == "")
      {
        $('#firstname_error').html("Please select First Name");
        err = err+1;
      }else
      {
        $('#firstname_error').html(" ");
      }

      if($('#lastname').val() == "")
      {
        $('#lastname_error').html("Please select Last Name");
        err = err+1;
      }else
      {
        $('#lastname_error').html(" ");
      }


    }


    if($('#order_Message').val() == "")
    {
      $('#Message_error').html("Please enter message");
      err = err+1;
    }else
    {
      $('#Message_error').html(" ");
    }

    if($('#order_Message').val().length > 1000)
    {
      $('#Message_error').html("Message should be of maximum 1000 characters.");
      err = err+1;
    }
    if($('#req').val() == "Processing Office"){
      if($('#order_processing_country_id').val() == "")
      {
        $('#processing_country_id_error').html("Please select Country");
        err = err+1;
      }else
      {
        $('#processing_country_id_error').html(" ");
      }


      if($('#order_processing_embassy_id').val() == "")
      {
        $('#processing_embassy_id_error').html("Please select Embassy / Center");
        err = err+1;
      }else
      {
        $('#processing_embassy_id_error').html(" ");
      }
    }



    if($('#req').val() == "Duplicate Payment"){
      if($('#duplicate_order_number_list').val() == "")
      {
        $('#tr_duplicate_error').html("Please select order number");
        err = err+1;
      }else
      {
        $('#tr_duplicate_error').html(" ");
      }
      if($('#no_duplicate_order').val() == "No Duplicate Order Number")
      {
        $('#tr_duplicate_error').html("There is no any duplicate order");
        err = err+1;
      }else
      {
        $('#tr_duplicate_error').html(" ");
      }
    }
    var des_key = $('#order_reason').val();
    if(des_key =='Request for Refund'){
      if(jQuery.trim($('#order_user_proof').val()) == "")
      {
        $('#user_proof_error').html('Please upload user proof.');
        err= err +1;
      }
    }

    else{
      $('#user_proof_error').html("");
    }
    // end

    if($('#chk_policy:checked').val() == null){
      $('#chk_policy_error').html("Please agree with refund policy");
      err = err+1;
    }else{
      $('#chk_policy_error').html(" ");
    }
    if(err != 0)
    {
      return false;
    }

    var sel= false;
    for (var i=0;i<fmobj.elements.length;i++)
    {
      var e = fmobj.elements[i];

      if ((e.type=='checkbox') && (!e.disabled) && (e.name == 'chk_fee[]'))
      {
        if(e.checked == true)
        {
          sel= true;
          break;
        }
      }
    }

    if(sel)
    {
      document.forms[0].action = "<?php echo url_for($sf_context->getModuleName().'/requestToAdmin');?>";
      //document.forms[0].action = "<?php //echo url_for($sf_context->getModuleName().'/uploadImage');?>";
      document.forms[0].target = "";
      document.forms[0].submit();
      return true;
    }
    else
    {
      alert('Please select at least one applicable Application');
      return false;
    }





  }


  function getDescription(){
    if($('#order_reason').val() == "Duplicate payment"){
      $('#orderNumber_row').show();
    }else{
      $('#orderNumber_row').hide();
    }
    var des_key = $('#order_reason').val();
    var url = "<?php echo url_for('requestRefund/getDescription'); ?>";
    $.post(url, {des_key: des_key},
    function(data){
      $('#description_div').addClass('infoBox1')
      $('#description_div').html(data)
      $('#hdn_description').html(data)

    });
  }

  function getSubSupportCategory(){

    var des_key = $('#order_reason').val();

    var url = "<?php echo url_for('requestRefund/getSubCategory'); ?>";
    $.post(url, {des_key: des_key},
    function(data){
      $('#tr_reason').show();
      $('#tr_reason').html(data)
      //      alert(des_key);
    });
    if(des_key == 'Request for Refund')
    {
      $('#tr_detail').hide();
      $('#tr_embassy').hide();
      $('#tr_center').hide();
      $('#tr_dob').hide();
      $('#user_proof_mandatory').show();
    }
    else if(des_key == 'Request for Re-scheduling Interview date')
    {
      $('#tr_duplicate').hide()
      $('#duplicate_app').hide();
      $('#duplicate_app_1').hide();
      $('#tr_detail').hide();
      $('#tr_embassy').hide();
      $('#tr_center').hide();
      $('#tr_dob').hide();
      $('#user_proof_mandatory').hide();
    }
    else if(des_key == 'Request for change Application Details')
    {
      $('#tr_duplicate').hide()
      $('#duplicate_app').hide();
      $('#duplicate_app_1').hide();
      $('#tr_detail').hide();
      $('#tr_embassy').hide();
      $('#tr_center').hide();
      $('#tr_dob').hide();
      $('#user_proof_mandatory').hide();

    }
    else if(des_key == 'Request for Application Details')
    {
      $('#tr_duplicate').hide()
      $('#duplicate_app').hide();
      $('#duplicate_app_1').hide();
      $('#tr_detail').hide();
      $('#tr_embassy').hide();
      $('#tr_center').hide();
      $('#tr_dob').hide();
      $('#user_proof_mandatory').hide();
    }


  }

  function getOrder(DrpVAlue){
    if(DrpVAlue != '' && DrpVAlue != des_key){
      var des_key = DrpVAlue;
    }
    else{
      var des_key = $('#req').val();
    }

    if(des_key == 'Duplicate Payment'){

      var orderNumber = <?php echo $orderNumber;?>;
      var url = "<?php echo url_for('requestRefund/getDuplicateOrder'); ?>";
      $.post(url, {des_key: des_key,orderNumber: orderNumber},
      function(data){
        $('#tr_duplicate').show();
        $('#tr_duplicate').html(data)
        $('#duplicate_app').show();
        $('#duplicate_app_1').show();
        $('#tr_detail').hide();
        $('#tr_embassy').hide();
        $('#tr_center').hide();
        $('#tr_dob').hide();
      });
    }else if(des_key == 'Others'){
      $('#tr_duplicate').hide();
      $('#duplicate_order_number_list').html('');
      $('#tr_detail').hide();
      $('#tr_embassy').hide();
      $('#tr_center').hide();
      $('#tr_dob').hide();
      $('#tr_dob').html('');
      $('#duplicate_app').hide();
      $('#duplicate_app_1').hide();
      $('#duplicate_app').html('');
    }else if(des_key == 'Name'){
      $('#tr_dob').hide();
      $('#tr_detail').show();
      $('#tr_embassy').hide();
      $('#tr_center').hide();
      $('#tr_duplicate').hide();
      $('#duplicate_app').hide();
      $('#duplicate_app_1').hide();
    }else if(des_key == 'Date Of Birth'){
      $('#tr_detail').hide();
      $('#tr_dob').show();
      $('#tr_embassy').hide();
      $('#tr_center').hide();
      $('#tr_duplicate').hide();
      $('#duplicate_app').hide();
      $('#duplicate_app_1').hide();
    }else if(des_key == 'Processing Office'){
      $('#tr_detail').hide();
      $('#tr_embassy').show();
      $('#tr_center').show();
      $('#tr_dob').hide();
      $('#tr_duplicate').hide();
      $('#duplicate_app').hide();
      $('#duplicate_app_1').hide();
    }else if(des_key == 'Others'){
      $('#tr_detail').hide();
      $('#tr_embassy').hide();
      $('#tr_center').hide();
      $('#tr_dob').hide();
      $('#duplicate_app').hide();
      $('#duplicate_app_1').hide();
      $('#tr_duplicate').hide();
    }else{
      $('#tr_detail').hide();
      $('#tr_embassy').hide();
      $('#tr_center').hide();
      $('#tr_dob').hide();
      $('#tr_duplicate').hide();
      $('#duplicate_app').hide();
      $('#duplicate_app_1').hide();
    }


  }



  function getDuplicateOrderDetails()
  {

    var des_key = $('#duplicate_order_number_list').val();
    if(des_key){

      var orderNumber = des_key;
      var url = "<?php echo url_for('requestRefund/getDuplicateOrderNumberDetails'); ?>";
      $.post(url, {orderNumber: orderNumber},function(data){
        $("#duplicate_app_1").html(data);
        $("#duplicate_app").show();
      });
    }

  }

  function isNumeric(value) {
    if (value == null || !value.toString().match(/^[-]?\d*\.?\d*$/)) return false;
    return true;
  }


  function checkAll(fmobj, chkAll)
  {

    for (var i=0;i<fmobj.elements.length;i++)
    {
      var e = fmobj.elements[i];
      if ((e.name == chkAll) && (e.type=='checkbox') && (!e.disabled))
      {
        e.checked = fmobj.chk_all.checked;


      }
    }
  }

  function UncheckMain(fmobj, objChkAll, chkElement)
  {

    var boolCheck = true;
    if(objChkAll.checked==false)
    {
      for(var i=0;i<fmobj.elements.length;i++)
      {
        if(fmobj.elements[i].name.indexOf(""+chkElement+"")!="-1" && fmobj.elements[i].checked==false)
        {
          boolCheck = false;
          break;
        }
      }
      if(boolCheck==true)
        objChkAll.checked=true;
    }
    else
    {
      objChkAll.checked=false;
    }
  }

  function getEmbassy(coutnryId,embassyId,appType){
    var country = '';
    var embassy = '';
    if(coutnryId){
      country = coutnryId;
    }else{
      country = $("#order_processing_country_id").val();
    }

    if(embassyId){
      embassy = embassyId;
    }

    if(country=='') {alert('please select a country');return;}
    if(appType=='vap')
        {
     var url = "<?php echo url_for('requestRefund/GetProcessingCenter/'); ?>";
        } else {
    var url = "<?php echo url_for('requestRefund/GetEmbassy/'); ?>";
    }


    $("#order_processing_embassy_id").load(url, {country_id: country,embassy: embassy, appType:appType },function(){});



  }



  $(document).ready(function() {
    var HdnValue = $('#hdn_seleApp').val();
    var HdnSubCat = $('#hdn_seleSubCat').val();
    if(HdnValue !=''){
      $("#nxtPage :checked").each(function() {
        var chkId = $(this).attr('id');
        getAppDetail('<?php echo $appId;?>','<?php echo $appType;?>',document.getElementById(chkId));
      });

      getDescription();getSubSupportCategory();getOrder(HdnSubCat);
    }
  });






  function getAppDetail(appId,appType,chkId){

    var url = "<?php echo url_for('requestRefund/getAppDetail'); ?>";
    $.post(url, {appId: appId,appType: appType},function(data){
      var exp = data.split('-');
      if(chkId.checked == true){
        if(exp[7] == 'NG' && exp[9] == 'freezone'){
          $("#tr_center").show();
          $('#tr_embassy').hide();
          $("#order_processing_center_id").val(exp[8]);
        }
         order_processing_country_id = jQuery.trim($("#order_processing_country_id").val());
        if(order_processing_country_id=="")
            {
        $("#order_processing_country_id").val(exp[7]);
        if(exp[8] !=''){
          getEmbassy(exp[7],exp[8],exp[9]);
        }
            }
        $("#order_processing_embassy_id").val(exp[8]);
         order_date_of_birth_day = jQuery.trim($("#order_date_of_birth_day").val());
        if(order_date_of_birth_day=="")
            {
        $("#order_date_of_birth_day").val(exp[6]);
            }
        order_date_of_birth_month = jQuery.trim($("#order_date_of_birth_month").val());
        if(order_date_of_birth_month=="")
            {
        $("#order_date_of_birth_month").val(exp[5]);
            }
        order_date_of_birth_year = jQuery.trim($("#order_date_of_birth_year").val());
        if(order_date_of_birth_year=="")
            {
        $("#order_date_of_birth_year").val(exp[4]);
            }
        sel_title = jQuery.trim($("#sel_title").val());
        if(sel_title=="")
            {
        $("#sel_title").val(exp[0]);
            }
        firstname = jQuery.trim($("#firstname").val());
        if(firstname=="")
            {
        $("#firstname").val(exp[1]);
            }
        middlename = jQuery.trim($("#middlename").val());
        if(middlename=="")
            {
        $("#middlename").val(exp[2]);
            }
        lastname = jQuery.trim($("#lastname").val());
        if(lastname=="")
            {
        $("#lastname").val(exp[3]);
            }
      }else {
        $("#order_processing_country_id").val('');
        $("#order_processing_embassy_id").val('');
        $("#order_date_of_birth_day").val('');
        $("#order_date_of_birth_month").val('');
        $("#order_date_of_birth_year").val('');
        $("#sel_title").val('');
        $("#firstname").val('');
        $("#middlename").val('');
        $("#lastname").val('');
      }

      //$("#lastname").val(exp[3]);


    });
  }

</script>