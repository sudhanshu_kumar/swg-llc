<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">
<?php include_partial('global/innerHeading',array('heading' =>'Pending Support Request Report'));
       use_helper('Pagination'); ?>
    <div class="clear"></div>
    <div class="tmz-spacer"></div>
    <div id="nxtPage">
      <table width="99%" cellpadding="0" cellspacing="0" class="dataTable" id="hdn_datatable">
    <tr>
          <td class="blbar" colspan="6" align="right"><div class="float_left">Pending Support Request Report</div>
            <span>Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total <b><?php echo $pager->getNbResults(); ?></b> results</span></td>
    </tr>
    <tr>
          <td width = "5%"><span class = "txtBold"> Sr No </span></td>
          <td width = "30%"><span class = "txtBold"> Support Category </span></td>
          <td width = "20%"><span class = "txtBold"> Sub Support Category </span></td>
          <td width = "10%"><span class = "txtBold"> Order Number </span></td>
          <td width = "5%"><span class = "txtBold"> Status </span></td>
          <td width = "10%"><span class = "txtBold">Request Date</span></td>
   <!-- <td width = "5%"> <span class = "txtBold"> Action </span></td> -->
    </tr>
<?php  $i = $pager->getFirstIndice();

if($pager->getNbResults()>0){
    foreach($pager->getResults() as $list): ?>
        <tr>
          <td width = "5%"><?php echo $i;?> </td>
          <td width = "15%"><?php echo nl2br($list['support_category']);?> </td>
        <?php if(isset ($list['sub_support_category']) && $list['sub_support_category'] != '') { ?>
          <td width = "15%"><?php echo nl2br($list['sub_support_category']);?> </td>
        <?php } else { ?>
          <td width = "15%"><?php echo 'N/A'; ?> </td>
        <?php  } ?>
          <td width = "10%"><?php echo $list['order_number'];?> </td>
          <td width = "5%"><?php echo $list['status'];?> </td>
          <td width = "5%"><?php echo date_format(date_create($list['created_at']),'Y-m-d');?> </td>
        <!-- <td width = "5%"><span><?//= link_to("Details", url_for("requestRefund/supportRequestDetail?request_id=".$list['id']),array('title'=>'See Requst Details'))?></span></td> -->
        </tr>
    <?php $i++;
    endforeach;?>
    <tr>
          <td class="blbar" colspan="6" height="25px" align="right"><div class = "paging pagingFoot"> <?php echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName())) ?></div></td>
    </tr>
    <?php }else {?>
        <tr>
          <td  align='center' class='red' colspan="6">No Pending Support Request </td>
        </tr>
   <?php } ?>
</table>
<br/>
<br/>
 <?php if($pager->getNbResults()>0){ ?>
<center class='multiFormNav'>
        <input type='button' value='Export to Excel' class="normalbutton" onclick="window.open('<?php echo _compute_public_path($filename, 'uploads/excel', '', true); ?>');return false;">
        &nbsp;
 </center>
  <?php } ?>
</div>
</div>
</div>
<div class="content_wrapper_bottom"></div>
