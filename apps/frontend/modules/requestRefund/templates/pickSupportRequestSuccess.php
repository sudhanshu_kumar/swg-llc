

<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">
  <?php
use_javascript('supportRequestValidate.js');
use_helper('Form');
include_partial('global/innerHeading',array('heading'=>'Support Request Details'));?>
<div class="clear"></div>
<div class="tmz-spacer"></div>
    <?php $sf = sfContext::getInstance()->getUser();
      if($sf->hasFlash('notice')){ ?>
       <div id="flash_notice" class="alertBox" ><?php
      echo nl2br($sf->getFlash('notice'));
      ?>
      </div>
    <?php } ?>
    <?php echo form_tag('requestRefund/saveSupportUserActivities',array('name'=>'order_search_form','class'=>'', 'method'=>'post','id'=>'order_search_form')) ?>
    
    <table width="99%" cellpadding="0" cellspacing="0" border="0">

      <tr>
        <td width="25%" class="txtBold">Customer Name</td>
        <td width="50%" class="txtLeft"><?php echo $reuqestDetailsArr['first_name'] ." " . $reuqestDetailsArr['last_name']; ?></td>
      </tr>
      <tr>

        <td ><span class="txtBold"> Support Category</span></td>

        <td class="txtLeft"><?php echo $reuqestDetailsArr['support_category'];?></td>
      </tr>


      <tr>
      <table>
      <tr>
        <td class="blbar" colspan="5" height="25px" align="left">
          Request made for :
        </td>
      </tr>
        
        <tr>
          <td colspan="5"><span class="txtBold">Order Number:</span> <?php echo $reuqestDetailsArr['order_number']?></td>
          
        </tr>
        <tr bgcolor="#eeeeee">
          <td width="15%"><span class="txtBold">Application Id</span></td>
          <td width="14%"><span class="txtBold">Application Type</span></td>
          <td width="14%"><span class="txtBold">Reference Number</span></td>
          <td width="30%"><span class="txtBold">Applicant Name</span></td>
          <td width="12%"><span class="txtBold">Application Status</span></td>
        </tr>
        <?php
        $arrayApp = $reuqestDetailsArr['app_detail'];
        for($i=0;$i<count($arrayApp);$i++){
          if($arrayApp[$i]['app_type'] == 'passport'){
            $appId['passport_id'] = $arrayApp[$i]['app_id'];
          }else if($arrayApp[$i]['app_type'] == 'visa'){
            $appId['visa_id'] = $arrayApp[$i]['app_id'];
          }
          else if($arrayApp[$i]['app_type'] == 'freezone'){
            $appId['freezone_id'] = $arrayApp[$i]['app_id'];
          }
          else if($arrayApp[$i]['app_type'] == 'vap'){
            $appId['visa_arrival_program_id'] = $arrayApp[$i]['app_id'];
          }

          $appDetails = $objApp->getApplicationDetails($appId);
          ?>

        <tr>
          <td><?php echo $appDetails['id']?></td>
          <td><?php if($appDetails['app_type'] == 'vap'){echo sfConfig::get('app_visa_arrival_title');}else{echo ucfirst($appDetails['app_type']);}?></td>
          <td><?php echo $appDetails['ref_no']?></td>
          <td><?php echo $appDetails['name']?></td>
          <td><?php echo $appDetails['status']?></td>
        </tr>
        <?php } ?>
      </table>
       
        
     
      </tr>
      <tr>
        <table width="100%">
          <tr>
            <td class="blbar" colspan="10" height="25px" align="left">
              Changes To Be Done:
            </td>
          </tr>
          <tr>
            <td width="25%" class="txtBold" colspan="5">Sub Support Category:</td>
            <?php if(isset ($reuqestDetailsArr['sub_support_category']) && $reuqestDetailsArr['sub_support_category'] != '') {?>
            <td width="50%" class="txtLeft"><?php echo $reuqestDetailsArr['sub_support_category'];?></td>
            <?php } else { ?>
            <td width="50%" class="txtLeft"><?php echo 'N/A';?></td>
           <?php  } ?>
          </tr>
           <?php if(isset ($reuqestDetailsArr['app_detail'][0]['request_for']) &&  $reuqestDetailsArr['app_detail'][0]['request_for'] != '') {?>
          <tr>
            <td width="25%" class="txtBold" colspan="5" valign="top">Request for change:</td>
            <td width="50%" class="txtLeft" ><?php echo nl2br($reuqestDetailsArr['app_detail'][0]['request_for']);?></td>
          </tr>
          <?php } ?>

        </table>

      </tr>
        


    </table>
    <?php foreach($arrRequestComments as $value) {?>


    <div class="Sap"></div>
    <table class="CommentNew" width="100%">
      <tr>
        <td  valign="top" width="25%" class="txtBold">
          Customer Comments  On <?php if($value['created_at']){ echo date_format(date_create($value['created_at']),'Y-m-d'); } ?> :
        </td>
        <td valign="top" width="50%"><?php if($value['customer_comments']){ echo $value['customer_comments'];}else{ echo "N/A";}?></td>
      </tr>
      <tr>
          <td valign="top" class="txtBold" width="25%">Support User Comments  On <?php if(isset ($value['updated_at'])){ echo date_format(date_create($value['updated_at']),'Y-m-d'); }?> :</td>
        <td  valign="top" width="50%"><?php if($value['support_user_comments']){ echo $value['support_user_comments'];}else{ echo "N/A";}?></td>

      </tr>
      <tr>
        <td valign="top" class="txtBold" width="25%">Uploaded Document:</td>

        <td align="left">


          <table width="100%" style="border:none !important">
            <tr>
              <?php
              $width = '';
              $height = '';
              $finalImageNameArr = array();
              if(!empty($value['upload_document'])){
                $finalStr = substr($value['upload_document'],0,-4);
                $finalImageNameArr = explode("####",$finalStr);
              }
              if(!empty($finalImageNameArr)){
                for($i=0;$i<count($finalImageNameArr);$i++){
                  $filename = sfConfig::get('sf_upload_dir').'/support_request/'.$finalImageNameArr[$i];
                  if(is_file($filename)){
                    $fileArray = getimagesize($filename);
                    $width = $fileArray[0]+50;
                    $height = $fileArray[1]+50;
                  }else{
                    $width = '500';
                    $height = '625';
                  }
                  ?>
              <td valign="top"><a href="javascript:void(0);" onclick="openWin('<?= _compute_public_path($finalImageNameArr[$i], 'uploads/support_request/', '', true);?>');">View uploaded document</a></td>
              <?php }}else{ ?>
              <td  align="left" width="50%" >N/A</td>
            <?php } ?>
          </tr>
          </table>
        </td>
      </tr>
    </table>
    <div class="Sap"></div>

      <?php } ?>
    <script>
      function openWin(url){
        var win = window.open(url,'MyPage','width=<?php echo $width;?>,height=<?php echo $height;?>,scrollbars=1,resizable=yes');
        win.moveTo(400,400);
      }
    </script>
    <table width="100%" class="CommentNew">
      <tr>
        <td colspan="4" align="center"> <div>
          <input type="button" value="Close" class="loginbutton" onclick="openCloseDiv('closeDivId','assignToDivId','respondDivId');">&nbsp;&nbsp;
          <input type="button" value="Assign To" class="loginbutton" onclick="openCloseDiv('assignToDivId','closeDivId','respondDivId');">&nbsp;&nbsp;
          <input type="button" value="Respond" class="loginbutton" onclick="openCloseDiv('respondDivId','closeDivId','assignToDivId');">&nbsp;&nbsp;
          <input type="hidden" name="hdn_page" value="<?php echo $page;?>">
          <input type="hidden" name="hdn_mode" value="<?php echo $mode;?>">
         <?php if($mode == 'pick')
                {
                  $url = "requestRefund/supportRequestPendingList?page=".$page;
                }else if($mode == 'reply'){
                  $url = "requestRefund/supportRequestAssignedList?page=".$page;
                }
          ?>
          <input type="button" value="Back" class="loginbutton" onclick="back('<?php echo url_for($url);?>')">&nbsp;&nbsp;
                     
          
          <input type ="hidden" value="<?php echo $requestId; ?>" name="hdn_requestId" id="hdn_requestId">
          <input type ="hidden" value="" name="mode" id="mode"></div>
        </td>
      </tr>
    </table>
    <br />


    <div id="closeDivId" style="display:none">
      <table width="99%">
        <tr>
          <td class="blbar" colspan="3" >Close Support Request <span style="margin-left: 700px;cursor:pointer" onclick="jQuery:$('#closeDivId').hide()"><u>X</u></span></td>
        </tr>
        <tr>
          <td width="25%" class="txtBold">Support User Comments</td>
          <td width="50%" class="">
            <textarea class="textarea" cols="50" rows="5" name="txt_support_close_cmt" id="txt_support_close_cmt"></textarea>
            <div class="red" id="error_txt_support_close_cmt"></div>
          </td>

        </tr>
        <tr>
          <td colspan="2" align="center">
            <?php echo submit_tag('Save',array('class' => 'loginbutton','onclick'=>'return validateRequestForm()')); ?>
          </td>
        </tr>

      </table>
    </div>
    <div id="assignToDivId"  style="display:none">
      <table width="99%">
        <tr>
          <td class="blbar" colspan="3">Assign Support Request<span style="margin-left: 700px;cursor:pointer" onclick="jQuery:$('#assignToDivId').hide()"><u>X</u></span></td>
        </tr>
        <tr>
          <td width="25%" class="txtBold">Support User List</td>
          <td width="50%" class="">
            <?php echo select_tag('slt_user', options_for_select($arrSupportUser,'',array('include_custom'=>'----Please Select User----')))?>
            <div class="red" id="error_slt_user"></div>
          </td>

        </tr>
        <tr>
          <td width="25%" class="txtBold">Support User Comments</td>
          <td width="50%" class="">
            <textarea class="textarea" cols="50" rows="5" name="txt_support_assign_cmt" id="txt_support_assign_cmt"></textarea>
            <div class="red" id="error_txt_support_assign_cmt"></div>
          </td>
        </tr>
        <tr>
          <td colspan="2" align="center">
            <?php echo submit_tag('Save',array('class' => 'loginbutton','onclick'=>'return validateRequestForm()')); ?>
          </td>
        </tr>
      </table>
    </div>
    <div id="respondDivId" style="display:none">
      <table width="99%">
        <tr>
          <td class="blbar" colspan="3">Respond Support Request <span style="margin-left: 685px;cursor:pointer" onclick="jQuery:$('#respondDivId').hide()"><u>X</u></span></td>
        </tr>
        <tr>
          <td width="25%" class="txtBold">Support User Comments</td>
          <td width="50%" class="">
            <textarea class="textarea" cols="50" rows="5" name="txt_support_respond_cmt" id="txt_support_respond_cmt"></textarea>
            <div class="red" id="error_txt_support_respond_cmt"></div>
          </td>

        </tr>
        <tr>
          <td colspan="2" align="center">
            <?php echo submit_tag('Save',array('class' => 'loginbutton','onclick'=>'return validateRequestForm()')); ?>
          </td>
        </tr>
      </table>

    </div>
    </form>

                </div>
</div>
<div class="content_wrapper_bottom"></div>
<script>
    function back(url)
    {
        var url = url;
        window.parent.location =  url;
    }
</script>