<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">
<?php use_helper('Pagination');
  use_helper('Form');
  include_partial('global/innerHeading',array('heading'=>'Assigned Support Request'));?>
  <div class="clear"></div>
  <div class="tmz-spacer"></div>
  <div id="nxtPage">
  <?php $sf = sfContext::getInstance()->getUser();
      if($sf->hasFlash('notice')){ ?>
       <div id="flash_notice" class="alertBox" ><?php
      echo nl2br($sf->getFlash('notice'));
      ?>
      </div>
    <?php } ?>
    <?php echo form_tag('requestRefund/supportRequestAssignedList',array('name'=>'order_search_form','class'=>'', 'method'=>'post','id'=>'order_search_form')) ?>
        <table width="100%" cellpadding="0" cellspacing="0" border="0">
          <?php
          $fname = (isset($_REQUEST['fname'])) ? $_REQUEST['fname'] : '';
          $lname = (isset($_REQUEST['lname'])) ? $_REQUEST['lname'] : '';
          $email = (isset($_REQUEST['email'])) ? $_REQUEST['email'] : '';
          $order_number = (isset($_REQUEST['order_number'])) ? $_REQUEST['order_number'] : '';

          echo formRowComplete('First Name',input_tag('fname',$fname , array('size' => 27, 'maxlength' => 50,'class'=>'txt-input')));
          echo "</tr>";
          echo formRowComplete('Last Name',input_tag('lname', $lname, array('size' => 27, 'maxlength' => 50,'class'=>'txt-input')));
          echo "</tr>";
          echo formRowComplete('Email',input_tag('email', $email, array('size' => 27, 'maxlength' => 50,'class'=>'txt-input')));
          echo "</tr>";
          echo formRowComplete('Order Number',input_tag('order_number', $order_number, array('size' => 27, 'maxlength' => 50,'class'=>'txt-input')));
          echo "</tr>";
          ?>

          <tr valign="top" >
            <td height="30" valign="top" style="border-right-style:none; vertical-align:top;">&nbsp;</td>
            <td height="30" valign="top" style="border-left-style:none; vertical-align:top;">
              <?php echo submit_tag('Search',array('class' => 'normalbutton')); ?>
              <input type="button" value="Reset" onclick="resetAll();" class="normalbutton" >
            </td>
          </tr>
        </table>
    </form>
    <div class="clear">&nbsp;</div>
          <table>
            <tr>
              <td class="blbar" colspan="10" align="right">
              <span>Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total  <b><?php echo $pager->getNbResults(); ?></b>  results</span></td>
            </tr>
            <tr>
              <td width="5%"><span class="txtBold">S. No.</span></td>
              <td width="10%"><span class="txtBold">First Name</span></td>
              <td width="10%"><span class="txtBold">Last Name</span></td>
              <td width="10%"><span class="txtBold">Email</span></td>
              <td width="20%"><span class="txtBold">Request Type</span></td>
              <td width="15%"><span class="txtBold">Order Number</span></td>
              <td width="15%"><span class="txtBold">Date Of Request</span></td>
              <td width="15%"><span class="txtBold">Pick Up Date</span></td>
              <td width="15%"><span class="txtBold">Last Action Date</span></td>
              <td width="10%"><span class="txtBold">Action</span></td>
            </tr>
            <?php
            if(($pager->getNbResults())>0) {
              $limit = sfConfig::get('app_records_per_page');
              $page = $sf_context->getRequest()->getParameter('page',0);
              $j = max(($page-1),0)*$limit ;

              ?>
              <?php
              foreach ($pager->getResults() as $result):
              $j++;
              ?>
            <tr>
              <td><?php echo $j;?></td>
              <td ><span><?php if($result->getSupportRequestList()->getsfGuardUser()->getUserDetail()->getFirstName() !='') {echo $result->getSupportRequestList()->getsfGuardUser()->getUserDetail()->getFirstName();}else{echo "--";}?></span></td>
              <td><span ><?php if($result->getSupportRequestList()->getsfGuardUser()->getUserDetail()->getLastName() !=''){echo $result->getSupportRequestList()->getsfGuardUser()->getUserDetail()->getLastName();}else{echo "--";}?></span></td>
              <td><span ><?php if($result->getSupportRequestList()->getsfGuardUser()->getUserDetail()->getEmail() !=''){echo $result->getSupportRequestList()->getsfGuardUser()->getUserDetail()->getEmail();}else{echo "--";}?></span></td>
              <td ><span ><?php if($result->getSupportRequestList()->getSupportCategory() !=''){echo $result->getSupportRequestList()->getSupportCategory();}else{echo "--";}?></span></td>
              <td><span> <?php echo $result->getSupportRequestList()->getOrderNumber();?></span></td>
              <td><span ><?php if($result->getSupportRequestList()->getCreatedAt() !=''){
                  echo date_format(date_create($result->getSupportRequestList()->getCreatedAt()),'Y-m-d');}else{echo "--";}?></span></td>
              <td>
                <?php if($result->getCreatedAt() !=''){
                    echo date_format(date_create($result->getCreatedAt()),'Y-m-d');}else{echo "--";}?>
              </td>
              <td>
                 <?php if($result->getUpdatedAt() !=''){
                     echo date_format(date_create($result->getUpdatedAt()),'Y-m-d');}else{echo "--";}?>
              </td>
              <td> <a  href="<?php echo url_for('requestRefund/pickSupportRequest?requestId='.Settings::encryptInput($result->getRequestId()).'&mode=reply&page='.$page)?>" title="Reply" >Reply</a></td>

            </tr>
            <?php endforeach; ?>
            <tr>
              <td class="blbar" colspan="10" height="25px" align="right">
              <?php echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName()).'?fname='.$fname.'&lname='.$lname.'&email='.$email.'&usePager=true') ?></td>
            </tr>
            <?php
          }else { ?>
            <tr><td  colspan="10" align='center' ><div class="red"> No record found</div></td></tr>
            <?php } ?>
          </table>

  </div>
</div>
</div>
<div class="content_wrapper_bottom"></div>
<script>
  function resetAll(){
    $('#fname').val('');
    $('#lname').val('');
    $('#email').val('');
    $('#order_number').val('');
  }
</script>
