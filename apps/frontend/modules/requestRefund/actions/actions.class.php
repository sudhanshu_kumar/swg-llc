<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class requestRefundActions extends sfActions {

    private $errorsObj;

    public function executeRefundMessage(sfWebRequest $request) {
        $this->orderNumber = $request->getParameter('requestId');
    }

    public function executeRefundPayment(sfWebRequest $request) {

        $params = $request->getPostParameters();
        if (!$request->getParameter('orderNumber')) {
            $orderNumber = $params['hdn_order'];
        } else {
            $orderNumber = $request->getParameter('orderNumber');
        }
        if(!is_numeric($orderNumber)){
            $orderNumber = Settings::decryptInput($orderNumber);
        }

        if (!$request->getParameter('type') && isset($params['hdn_app'])) {
            $this->appType = $params['hdn_app'];
        } else {
            $this->appType = Settings::decryptInput($request->getParameter('type'));
        }

        if (!$request->getParameter('appId') && isset($params['hdn_app_id'])) {
            $this->appId = $params['hdn_app_id'];
        } else {
            $this->appId = Settings::decryptInput($request->getParameter('appId'));
        }

        if (!$request->getParameter('mode') && isset($params['hdn_mode'])) {
            $this->mode = $params['hdn_mode'];
        } else {
            $this->mode = $request->getParameter('mode');
        }

        if (!empty($this->appType) && !empty($this->appId)) {
            $this->selectedApp = $this->appId . '_' . $this->appType;
        } else {
            $this->selectedApp = '';
        }

        // $usrId = $this->getUser()->getGuarduser()->getUserDetail()->getUserId();die;
        $orderRequestDetailId = Doctrine::getTable('ipay4meOrder')->getOrderRequestDetailId($orderNumber);
        $requestDetails = Doctrine::getTable('OrderRequestDetails')->find($orderRequestDetailId);
        $userId = $requestDetails->getUserId();
        if ($this->mode == "name") {
            $this->selectedCat = "Request for change Application Details";
            $this->selectedSubCat = "Name";
        } elseif ($this->mode == "dob") {
            $this->selectedCat = "Request for change Application Details";
            $this->selectedSubCat = "Date Of Birth";
        } elseif ($this->mode == "office") {
            $this->selectedCat = "Request for change Application Details";
            $this->selectedSubCat = "Processing Office";
        } else {
            $this->selectedCat = "";
            $this->selectedSubCat = "";
        }


        $userDetailobj = $requestDetails->getSfGuardUser()->getUserDetail();
        unset($_SESSION['select_value']);
        if (isset($this->mode) && $this->mode != '') {
            $request->setAttribute('name', $this->selectedCat);
            $_SESSION['select_value'] = $this->selectedSubCat;
        }

        $this->orderNumber = $orderNumber;
        $this->currency = $requestDetails->getCurrency();
        if($this->currency == '1'){
            $this->amount = $requestDetails->getAmount();
        }else{
            $this->amount = $requestDetails->getConvertAmount();
        }


        $this->form = new RefundPaymentForm();
        $formVal = $request->getParameter($this->form->getName());



        $this->ipay4meOrderDetails = ipay4meOrderTable::getInstance()->getIpayOrderDetails($orderNumber);

        $ipayNisDetailsQuery = CartItemsTransactionsTable::getInstance()->getCartDetailsQuery($orderNumber);

        $this->nisHelper = new NisHelper();
        $page = 1;
        if ($request->hasParameter('page')) {
            $page = $request->getParameter('page');
        }
        $this->pager = new sfDoctrinePager('PassportApplication', sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($ipayNisDetailsQuery);
        $this->pager->setPage($this->getRequestParameter('page', $page));
        $this->pager->init();
        $this->appObj = CartItemsTransactionsTable::getInstance();

        $this->isFound = true;
    }

    public function executeRequestToAdmin(sfWebRequest $request) {
        $param = $request->getPostParameters();
        $orderNumber = $param['hdn_order'];
        $concatinatedMsg = '';
        $concatinatedMsg1 = '';
        $concatinatedMsg2 = '';
        $concatinatedMsg3 = '';
        $concatinatedMsg .= $param['hdn_order'] . "\n";
        if ($this->getUser() && $this->getUser()->isAuthenticated()) {
            $userId = $this->getUser()->getGuardUser()->getId();
        } else {
            return sfContext::getInstance()->getController()->redirect('@adminhome');
        }

        $file_name = '';
        if ($_FILES['order_user_proof']['tmp_name'] != '') {
            //upload image
            $path = '/support_request';
            $final_path = sfConfig::get('sf_upload_dir') . $path;
            if (is_dir($final_path) == '') {
                mkdir($final_path, 0777, true);
                chmod($final_path, 0777);
            }
            $tmpfileName = $_FILES['order_user_proof']['tmp_name'];
            $file_name = $userId . '_' . time() . '_' . $_FILES['order_user_proof']['name'];
            $path_to_upload = $final_path . '/' . $file_name;
            $upload = move_uploaded_file($tmpfileName, $path_to_upload);
            chmod($path_to_upload, 0755);
            //end upload
        }


        $amount = $param['hdn_amount'];
        if (isset($param['order']['reason']) && $param['order']['reason'] != '') {
            $concatinatedMsg .= "\n Sub Category : " . $param['order']['reason'];
            $supportCategory = $param['order']['reason'];
        } else {
            $concatinatedMsg .= "\n Sub Category : " . $param['hdn_selectedCat'];
            $supportCategory = $param['hdn_selectedCat'];
        }
        $this->userId = $userId;
        $message = $param['order']['Message'];

        $concatinatedMsg .= "\n  Message : " . $param['order']['Message'];
        //    $userProof = $param['order']['user_proof'];
        $mode = $param['hdn_mode'];

        if (isset($param['req']) && $param['req'] != '') {
            $subCategory = $param['req'];
            if(isset ($param['req']) && $param['req'] != ''){
            $concatinatedMsg .= "\n Sub Support Category : " . $param['req'];
            }else {
                $concatinatedMsg .= "\n Sub Support Category : " . 'N/A';
            }
        } else {
            if(isset ($param['hdn_selectedSubCat']) &&  $param['hdn_selectedSubCat'] != '') {
            $concatinatedMsg .= "\n Sub Support Category : " . $param['hdn_selectedSubCat'];
            } else {
               $concatinatedMsg .= "\n Sub Support Category : " . 'N/A';
            }
            $subCategory = $param['hdn_selectedSubCat'];
        }
        $concatinatedMsg1 = array('Order Number' => $orderNumber, 'User Id' => $userId, 'Support Category' => $supportCategory, 'Sub Support Category' => $subCategory); //."\n".'Amount : '.$amount."\n".'Message : '.$message;
        $concatinatedMsg2 = array('Message' => $message, 'User Id' => $userId, 'filename' => $file_name);



        if (isset($param['chk_fee']) && $param['chk_fee'] != '') {
            $concatinatedMsg3['chk_fee'] = $param['chk_fee'];
            $appDetail = $param['chk_fee'];

            $str = '';
            foreach ($appDetail as $key => $val) {
                $exp = explode('_', $val);
                if($exp[1]=='vap'){
                $concatinatedMsg .= "\n Application Id : " . $exp[0] . ", Type : " . "Visa On Arrival";
                } else {
                $concatinatedMsg .= "\n Application Id : " . $exp[0] . ", Type : " . ucwords($exp[1]);
                }
            }
        }

        if (isset($param['duplicate_order_number_list']) && $param['duplicate_order_number_list'] != '') {
            $concatinatedMsg3['Duplicate Order Number'] = "\n Duplicate Order Number : " . $param['duplicate_order_number_list'];
            $concatinatedMsg .= "\n Duplicate Order Number : " . $param['duplicate_order_number_list'];
        }
        if (isset($param['chk_dfee']) && $param['chk_dfee'] != '') {
            $concatinatedMsg3['chk_dfee'] = $param['chk_dfee'];
            $appDetail = $param['chk_dfee'];
            $appArray = '';
            foreach ($appDetail as $key => $val) {
                $exp = explode('_', $val);
                $appArray .= "\n Application Id : " . $exp[0] . ", Type : " . ucwords($exp[1]);
                if($exp[1]=='vap'){
                $concatinatedMsg .= "\n Application Id : " . $exp[0] . ", Type : " . "Visa On Arrival";
                } else {
                $concatinatedMsg .= "\n Application Id : " . $exp[0] . ", Type : " . ucwords($exp[1]);
                }
            }
            $concatinatedMsg3['AppIdType'] = $appArray;
        }

        if (isset($param['chk_dfee']) && $param['chk_dfee'] != '') {
            $concatinatedMsg3['Duplicate Application Detail'] = $concatinatedMsg3['chk_dfee'];
        }
        if ($mode == 'name') {
            if (isset($param['title']) && $param['title'] != '') {
                $concatinatedMsg3['title'] = "\nName to be changed : " . $param['title'];
            }

            if (isset($param['first_name']) && $param['first_name'] != '') {
                $concatinatedMsg3['first_name'] = " " . $param['first_name'];
            }

            if (isset($param['middle_name']) && $param['middle_name'] != '') {
                $concatinatedMsg3['middle_name'] = " " . $param['middle_name'];
            }

            if (isset($param['last_name']) && $param['last_name'] != '') {
                $concatinatedMsg3['last_name'] = " " . $param['last_name'];
            }
            $concatinatedMsg3['name'] = $concatinatedMsg3['title'] . $concatinatedMsg3['first_name'] . $concatinatedMsg3['middle_name'] . $concatinatedMsg3['last_name'];
            $concatinatedMsg .= "\nName to be changed : " . $param['title'] . " " . $param['first_name'] . " " . $param['middle_name'] . " " . $param['last_name'];
        } else if ($mode == 'dob') {
            if (isset($param['order']['date_of_birth']['day']) && $param['order']['date_of_birth']['day'] != '') {
                $concatinatedMsg3['day'] = "\n New Date of Birth(dd-mm-yyyy) : " . $param['order']['date_of_birth']['day'];
            }

            if (isset($param['order']['date_of_birth']['month']) && $param['order']['date_of_birth']['month'] != '') {
                $concatinatedMsg3['month'] = "-" . $param['order']['date_of_birth']['month'];
            }

            if (isset($param['order']['date_of_birth']['year']) && $param['order']['date_of_birth']['year'] != '') {
                $concatinatedMsg3['year'] = "-" . $param['order']['date_of_birth']['year'];
            }
            $concatinatedMsg3['dob'] = $concatinatedMsg3['day'] . $concatinatedMsg3['month'] . $concatinatedMsg3['year'];
            $concatinatedMsg .= $concatinatedMsg3['dob'];
        } else if ($mode == 'office') {

            if (isset($param['order']['processing_country_id']) && $param['order']['processing_country_id'] != '') {
                $countryId = $param['order']['processing_country_id'];
                $concatinatedMsg3['country'] = "\n Country Name : " . Doctrine::getTable('Country')->find($countryId)->getCountryName();
            }

            if (isset($param['processing_embassy_id']) && $param['processing_embassy_id'] != '') {
                $embassyId = $param['processing_embassy_id'];
                if($exp[1]=='vap')
                   {
                       $concatinatedMsg3['embassy'] = "\nCenter Name : " . Doctrine::getTable('GlobalMaster')->find($embassyId)->getVarValue();
                   }else {
                    $concatinatedMsg3['embassy'] = "\nEmbassy Name : " . Doctrine::getTable('EmbassyMaster')->find($embassyId)->getEmbassyName();
                   }
            }
            $concatinatedMsg3['office'] = $concatinatedMsg3['country'] . $concatinatedMsg3['embassy'];
            $concatinatedMsg .= $concatinatedMsg3['office'];
        } else {

            if (isset($subCategory) && $subCategory == 'Name') {
                if (isset($param['title']) && $param['title'] != '') {
                    $concatinatedMsg3['title'] = "\n Name to be changed : " . $param['title'];
                    $concatinatedMsg .= "\n Name to be changed : \n Title :" . $param['title'];
                }

                if (isset($param['first_name']) && $param['first_name'] != '') {
                    $concatinatedMsg3['first_name'] = " " . $param['first_name'];
                    $concatinatedMsg .= "\n First Name :" . $param['first_name'];
                }

                if (isset($param['middle_name']) && $param['middle_name'] != '') {
                    $concatinatedMsg3['middle_name'] = " " . $param['middle_name'];
                    $concatinatedMsg .= "\n Middle Name :" . $param['middle_name'];
                }
                else
                {
                    $concatinatedMsg3['middle_name'] = " " ;
                    $concatinatedMsg .= "\n Middle Name : " . $param['middle_name'];
                }

                if (isset($param['last_name']) && $param['last_name'] != '') {
                    $concatinatedMsg3['last_name'] = " " . $param['last_name'];
                    $concatinatedMsg .= "\n Last Name : " . $param['last_name'];
                }
                $concatinatedMsg3['name'] = $concatinatedMsg3['title'] . $concatinatedMsg3['first_name'] . $concatinatedMsg3['middle_name'] . $concatinatedMsg3['last_name'];
            } else if (isset($subCategory) && $subCategory == 'Date Of Birth') {
                if (isset($param['order']['date_of_birth']['day']) && $param['order']['date_of_birth']['day'] != '') {
                    $concatinatedMsg3['day'] = "\n New Date of Birth(dd-mm-yyyy) : " . $param['order']['date_of_birth']['day'];
                }

                if (isset($param['order']['date_of_birth']['month']) && $param['order']['date_of_birth']['month'] != '') {
                    $concatinatedMsg3['month'] = "-" . $param['order']['date_of_birth']['month'];
                }

                if (isset($param['order']['date_of_birth']['year']) && $param['order']['date_of_birth']['year'] != '') {
                    $concatinatedMsg3['year'] = "-" . $param['order']['date_of_birth']['year'];
                }
                $concatinatedMsg3['dob'] = $concatinatedMsg3['day'] . $concatinatedMsg3['month'] . $concatinatedMsg3['year'];
                $concatinatedMsg .= $concatinatedMsg3['dob'];
            } else if (isset($subCategory) && $subCategory == 'Processing Office') {

                if (isset($param['order']['processing_country_id']) && $param['order']['processing_country_id'] != '') {
                    $countryId = $param['order']['processing_country_id'];
                    $concatinatedMsg3['country'] = "\n Country Name : " . Doctrine::getTable('Country')->find($countryId)->getCountryName();
                }

                if (isset($param['processing_embassy_id']) && $param['processing_embassy_id'] != '') {
                   $embassyId = $param['processing_embassy_id'];
                    if($exp[1]=='vap')
                   {
                       $concatinatedMsg3['embassy'] = "\nCenter Name : " . Doctrine::getTable('GlobalMaster')->find($embassyId)->getVarValue();
                   }else {
                    $concatinatedMsg3['embassy'] = "\nEmbassy Name : " . Doctrine::getTable('EmbassyMaster')->find($embassyId)->getEmbassyName();
                   }
                }
                $concatinatedMsg3['office'] = $concatinatedMsg3['country'] . $concatinatedMsg3['embassy'];
                $concatinatedMsg .= $concatinatedMsg3['office'];
            }
        }
        //    save data into support refund list table
        //    $objsupportList = new SupportRefundList();
        $supportRequestObj = new SupportRequestManager();
        $supportRequestId = $supportRequestObj->setSupportRequest($concatinatedMsg1, $concatinatedMsg2, $concatinatedMsg3);


        $subject = "Refund Request Mail";
        $partialName = 'mailTemplate';
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));

        $notificationUrl = "notification/paymentRefundRequestMailUser";
        $senMailUrl = "notification/paymentRefundRequestMailAdmin";
        //mail to user
        $taskId = EpjobsContext::getInstance()->addJob('paymentRefundRequestMailUser', $notificationUrl, array('concatinatedMsg' => $concatinatedMsg, 'orderNumber' => $orderNumber));
        sfContext::getInstance()->getLogger()->debug("sceduled job with id: $taskId");
        //mail to admin
        $mailTaskId = EpjobsContext::getInstance()->addJob('paymentRefundRequestMailAdmin', $senMailUrl, array('concatinatedMsg' => $concatinatedMsg, 'orderNumber' => $orderNumber));
        sfContext::getInstance()->getLogger()->debug("sceduled job with id: $mailTaskId");
        $this->redirect('requestRefund/thanks');
    }

    public function executeGetDescription(sfWebRequest $request) {
        $this->setTemplate(null);
        $desKey = $request->getParameter('des_key');
        if ($desKey != '') {
            $arrDescription = Doctrine::getTable('SupportReason')->findByKey($desKey);
            $str = $arrDescription->getFirst()->getReason();
            return $this->renderText($str);
        }
    }

    public function executeGetSubCategory(sfWebRequest $request) {
        $this->setTemplate(null);
        $desKey = $request->getParameter('des_key');
        if ($desKey != '') {
            $arrDescription = Doctrine::getTable('SupportReason')->findByKey($desKey);
            $arrChild = Doctrine::getTable('SupportReason')->findByParentId($arrDescription->getFirst()->getId())->toArray();
            if (count($arrChild) > 0) {
                if (isset($_SESSION['select_value']) && !empty($_SESSION['select_value'])) {
                    $disabled = "disabled = disabled";
                } else {
                    $disabled = "";
                }
                $str = '';

                $str .="<td>Sub Support Category<font color=red>*</font></td><td colspan=2><select id='req' name='req' onchange='getOrder()' $disabled>";
                $str .="<option value=''>--Please Select Sub Support Category--</option>";
                foreach ($arrChild as $key => $value) {
                    if (isset($_SESSION['select_value']) && !empty($_SESSION['select_value'])) {
                        if ($value['key'] == $_SESSION['select_value']) {
                            $str .="<option value='" . $value['key'] . "' selected >" . $value['key'] . "</option>";
                        } else {
                            $str .="<option value='" . $value['key'] . "'>" . $value['key'] . "</option>";
                        }
                    } else {
                        $str .="<option value='" . $value['key'] . "'>" . $value['key'] . "</option>";
                    }
                }

                $str .="</select><div class='red' id='req_error'></div></td>";
                return $this->renderText($str);
            } else {
                $str = '';
                return $this->renderText($str);
            }
        }
    }

    public function executeGetDuplicateOrder(sfWebRequest $request) {
        $this->setTemplate(null);
        $orderNumber = $request->getParameter('orderNumber');

        if ($orderNumber != '') {
            $orderRequestDetailId = Doctrine::getTable('ipay4meOrder')->getOrderRequestDetailId($orderNumber);
            $requestDetails = Doctrine::getTable('OrderRequestDetails')->find($orderRequestDetailId);
            $userId = $requestDetails->getUserId();
            $allOrder = ipay4meOrderTable::getInstance()->getAllDuplicateOrder($userId);
            if($allOrder){
            $str = '';
            $str .="<td>Duplicate Order<font color=red>*</font></td><td colspan=2><select name='duplicate_order_number_list' id='duplicate_order_number_list' onChange='getDuplicateOrderDetails()'>";
            $str .="<option value=''>Please Select order number</option>";
            for($i=0;$i<count($allOrder);$i++) {
                if ($allOrder[$i] != $orderNumber) {
                    $str .="<option value=" . $allOrder[$i] . ">" . $allOrder[$i] . "</option>";
                }
            }

            $str .="</select><div class='red' id='tr_duplicate_error'></div></td>";
            return $this->renderText($str);
            }else{
                $str = '<td>Duplicate Order<font color=red>*</font></td><td colspan=2><input type = "text" size ="50" id = "no_duplicate_order" readonly = "readonly" value = "No Duplicate Order Number">';
                $str .="<div class='red' id='tr_duplicate_error'></div></td>";
                return $this->renderText($str);
            }
        }
    }

    public function executeOpenInfoBox(sfWebRequest $request) {
        //        $this->broadcast_message = $this->getBroadcastMessage();
        $this->orderNumber = $request->getParameter('requestId');
        if(!is_numeric($this->orderNumber)){
            $this->orderNumber = Settings::decryptInput($this->orderNumber);
        }
        $this->setTemplate('openInfoBox');
        //    $this->setLayout(false);
    }

    public function executeRefundPolicy(sfWebRequest $request) {
        //        $this->broadcast_message = $this->getBroadcastMessage();
        $this->orderNumber = $request->getParameter('requestId');
        $this->setTemplate('refundPolicy');
        $this->setLayout(false);
    }

    public function executeGetEmbassy(sfWebRequest $request) {
        $this->setLayout(false);
        $q = array();
        $selectedOfficeId = $request->getParameter('embassy');
        if (trim($request->getParameter('country_id')) != "") {
            $q = Doctrine::getTable('EmbassyMaster')->createQuery('st')->
                            addWhere('embassy_country_id = ?', $request->getPostParameter('country_id'))->execute()->toArray();
        }


        /**
         * [WP:081] => CR:119
         * Exclude Bonn embassy for Germany processing country.
         * Getting excluded embassy defined in app.yml
         */
        $arrExcludeEmbassy = sfConfig::get('app_excludeEmbassy');

        $str = '<option value="">-- Please Select --</option>';

        if (count($q) > 0)
            foreach ($q as $key => $value) {
                if (!empty($arrExcludeEmbassy)) {
                        if (!in_array(trim($value['embassy_name']), $arrExcludeEmbassy)) {
                            $str .= '<option value="' . $value['id'] . '" '.(($value['id'] == $selectedOfficeId) ? ' selected="selected"' : '').' >' . $value['embassy_name'] . '</option>';
                        }
                }else{
                    $str .= '<option value="' . $value['id'] . '"' . (($value['id'] == $selectedOfficeId) ? ' selected="selected"' : '') . '>' . $value['embassy_name'] . '</option>';
                }
            }

        return $this->renderText($str);
    }
    public function executeGetProcessingCenter(sfWebRequest $request) {
        $this->setLayout(false);
        $selectedOfficeId = $request->getParameter('embassy');
                $str = '<option value="">-- Please Select --</option>';
                $str .= '<option value="' . '135' . '"' . (('135' == $selectedOfficeId) ? ' selected="selected"' : '') . '>' . 'Nnamdi Azikiwe international Airport,Abuja' . '</option>';
                $str .= '<option value="' . '136' . '"' . (('136' == $selectedOfficeId) ? ' selected="selected"' : '') . '>' . 'Mallam Aminu Kano Airport, Kano' . '</option>';
                $str .= '<option value="' . '137' . '"' . (('137' == $selectedOfficeId) ? ' selected="selected"' : '') . '>' . 'Murtala Mohammed Airport, Lagos' . '</option>';
                $str .= '<option value="' . '138' . '"' . (('138' == $selectedOfficeId) ? ' selected="selected"' : '') . '>' . 'Margret Ekpo Airport,Calabar' . '</option>';

        return $this->renderText($str);
    }

    public function executeGetDuplicateOrderNumberDetails(sfWebRequest $request) {
        $orderNumber = $request->getParameter('orderNumber');
        $this->orderNumber = $orderNumber;


        $this->ipay4meOrderDetails = ipay4meOrderTable::getInstance()->getIpayOrderDetails($orderNumber);

        $ipayNisDetailsQuery = CartItemsTransactionsTable::getInstance()->getCartDetailsQuery($orderNumber);

        $this->nisHelper = new NisHelper();
        $page = 1;
        if ($request->hasParameter('page')) {
            $page = $request->getParameter('page');
        }
        $this->pager = new sfDoctrinePager('PassportApplication', sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($ipayNisDetailsQuery);
        $this->pager->setPage($this->getRequestParameter('page', $page));
        $this->pager->init();
        $this->isFound = true;
        $this->nisHelper = new NisHelper();
        $this->appObj = CartItemsTransactionsTable::getInstance();
        echo $this->getPartial("orderAddDetails", array("pager" => $this->pager, "isFound" => $this->isFound, "nisHelper" => $this->nisHelper, "appObj" => $this->appObj));
        die();
        //    $this->renderPartial($templateName, $vars)
    }

    public function executeGetAppDetail(sfWebRequest $request) {
        $appId = $request->getParameter('appId');
        $appType = $request->getParameter('appType');
        if ($appType == 'passport') {
            $arrAppDetail = Doctrine::getTable('PassportApplication')->find($appId)->toArray();
            $title = $arrAppDetail['title_id'];
            $fname = $arrAppDetail['first_name'];
            $lname = $arrAppDetail['last_name'];
            $mname = $arrAppDetail['mid_name'];
            $dob = explode(' ', $arrAppDetail['date_of_birth']);
            $dob = $dob[0];
            $country = $arrAppDetail['processing_country_id'];
            $embassy = $arrAppDetail['processing_embassy_id'];
        } elseif ($appType == 'visa') {
            $arrAppDetail = Doctrine::getTable('VisaApplication')->find($appId)->toArray();
            $title = $arrAppDetail['title'];
            $lname = $arrAppDetail['surname'];
            $mname = $arrAppDetail['middle_name'];
            $fname = $arrAppDetail['other_name'];
            $dob = explode(' ', $arrAppDetail['date_of_birth']);
            $dob = $dob[0];
            $arrProcessingCountry = Doctrine::getTable('VisaApplicantInfo')->findByApplicationId($appId)->toArray();
            $country = $arrProcessingCountry[0]['applying_country_id'];
            $embassy = $arrProcessingCountry[0]['embassy_of_pref_id'];
        } elseif ($appType == 'freezone') {
            $arrAppDetail = Doctrine::getTable('VisaApplication')->find($appId)->toArray();
            $title = $arrAppDetail['title'];
            $lname = $arrAppDetail['surname'];
            $mname = $arrAppDetail['middle_name'];
            $fname = $arrAppDetail['other_name'];
            $dob = explode(' ', $arrAppDetail['date_of_birth']);
            $dob = $dob[0];
            $country = 'NG';
            $embassy = Doctrine::getTable('ReEntryVisaApplication')->getProcessingCountry($appId);
        } elseif ($appType == 'vap') {
            $arrAppDetail = Doctrine::getTable('VapApplication')->find($appId)->toArray();
            $title = $arrAppDetail['title'];
            $lname = $arrAppDetail['surname'];
            $mname = $arrAppDetail['middle_name'];
            $fname = $arrAppDetail['first_name'];
            $dob = explode(' ', $arrAppDetail['date_of_birth']);
            $dob = $dob[0];
            $country = $arrAppDetail['processing_country_id'];
            $embassy = $arrAppDetail['processing_center_id'];
        }

        $arrDetail = $title . '-' . $fname . '-' . $mname . '-' . $lname . '-' . $dob . '-' . $country . '-' . $embassy . '-' . $appType;

        return $this->renderText($arrDetail);
    }

    public function executeApplicationDetails(sfWebRequest $request) {
        //        $this->broadcast_message = $this->getBroadcastMessage();

        $this->setTemplate('applicationDetails');

        $orderNumber = $request->getParameter('orderNumber');
        if(!is_numeric($orderNumber)){
            $orderNumber = Settings::decryptInput($orderNumber);
        }


        // $usrId = $this->getUser()->getGuarduser()->getUserDetail()->getUserId();die;
        $orderRequestDetailId = Doctrine::getTable('ipay4meOrder')->getOrderRequestDetailId($orderNumber);
        $requestDetails = Doctrine::getTable('OrderRequestDetails')->find($orderRequestDetailId);
        $userId = $requestDetails->getUserId();

        $userDetailobj = $requestDetails->getSfGuardUser()->getUserDetail();

        $this->orderNumber = $orderNumber;
        $this->amount = $requestDetails->getAmount();
        $this->form = new RefundPaymentForm();

        $this->ipay4meOrderDetails = ipay4meOrderTable::getInstance()->getIpayOrderDetails($orderNumber);

        $ipayNisDetailsQuery = CartItemsTransactionsTable::getInstance()->getCartDetailsQuery($orderNumber);

        $this->nisHelper = new NisHelper();
        $page = 1;
        if ($request->hasParameter('page')) {
            $page = $request->getParameter('page');
        }
        $this->pager = new sfDoctrinePager('PassportApplication', sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($ipayNisDetailsQuery);
        $this->pager->setPage($this->getRequestParameter('page', $page));
        $this->pager->init();
        $this->appObj = CartItemsTransactionsTable::getInstance();

        $this->isFound = true;

        //    $this->setLayout(false);
    }

    public function executeApplicationDetailsInfo(sfWebRequest $request) {


        $orderNumber = Settings::decryptInput($request->getParameter('orderNumber'));
        $appId = Settings::decryptInput($request->getParameter('appId'));
        $type = Settings::decryptInput($request->getParameter('type'));
        // $appId='2934194';$type='visa';
        $this->orderNumber = $orderNumber;
        $this->appId = $appId;
        $this->type = $type;
        $nisUrl = sfConfig::get("app_nis_portal_url");
        if ($type == 'passport') {
            $passport_application = $this->getPassportRecord($appId);
            $this->passport_application = $passport_application;

            $encriptedAppId = SecureQueryString::ENCRYPT_DECRYPT($appId);
            $encriptedAppId = SecureQueryString::ENCODE($encriptedAppId);
            $this->payReceiptUrl = $nisUrl . 'passport/passportPaymentSlip?id=' . $encriptedAppId;
            $this->ackUrl = $nisUrl . 'passport/passportAcknowledgmentSlip?id=' . $encriptedAppId;
        } else if ($type == 'visa') {
            $visa_application = $this->getVisaFreshRecord($appId);

            $refNo = $visa_application[0]["ref_no"];
            $encriptedAppId = SecureQueryString::ENCRYPT_DECRYPT($appId);
            $encriptedAppId = SecureQueryString::ENCODE($encriptedAppId);
            $encriptedRefId = SecureQueryString::ENCRYPT_DECRYPT($refNo);
            $encriptedRefId = SecureQueryString::ENCODE($encriptedRefId);

            $this->payReceiptUrl = $nisUrl . 'visa/PrintRecipt?visa_app_id=' . $encriptedAppId . '&visa_app_refId=' . $encriptedRefId;
            $this->ackUrl = $nisUrl . 'visa/visaAcknowledgmentSlip?id=' . $encriptedAppId;
            $this->passport_application = $visa_application;
        } else if ($type == 'freezone') {
            $visa_application = $this->getVisaFreshRecord($appId);

            $visa_application[0]['country'] = 'Nigeria';
            $country = 'NG';
            $embassy = Doctrine::getTable('ReEntryVisaApplication')->getProcessingCountry($appId);
            if (!empty($embassy)) {
                $embassyId = $embassy;
            } else {
                $embassyId = $visa_application[0]['VisaApplicantInfo']['authority_id'];
            }

            $embassyName = Doctrine::getTable('VisaProcessingCentre')->findById($embassyId)->toArray();
            $visa_application[0]['center'] = $embassyName[0]['centre_name'];

            $refNo = $visa_application[0]["ref_no"];
            $zoneType = $visa_application[0]["zone_type_id"];
            if ($zoneType == 67) {
                $encriptedAppId = SecureQueryString::ENCRYPT_DECRYPT($appId);
                $encriptedAppId = SecureQueryString::ENCODE($encriptedAppId);
                $encriptedRefId = SecureQueryString::ENCRYPT_DECRYPT($refNo);
                $encriptedRefId = SecureQueryString::ENCODE($encriptedRefId);

                $this->payReceiptUrl = $nisUrl . 'visa/PrintRecipt?visa_app_id=' . $encriptedAppId . '&visa_app_refId=' . $encriptedRefId;
                $this->ackUrl = $nisUrl . 'visa/visaAcknowledgmentSlip?id=' . $encriptedAppId;
            } else {
                $encriptedAppId = SecureQueryString::ENCRYPT_DECRYPT($appId);
                $encriptedAppId = SecureQueryString::ENCODE($encriptedAppId);
                $encriptedRefId = SecureQueryString::ENCRYPT_DECRYPT($refNo);
                $encriptedRefId = SecureQueryString::ENCODE($encriptedRefId);

                $this->payReceiptUrl = $nisUrl . 'visa/PrintRecipt?visa_app_id=' . $encriptedAppId . '&visa_app_refId=' . $encriptedRefId;
                $this->ackUrl = $nisUrl . 'visa/visaReEntryAcknowledgmentSlip?id=' . $encriptedAppId;
            }
            $this->passport_application = $visa_application;
        }   else if ($type == 'vap') {
            $visa_application = $this->getVisaOnArrivalFreshRecord($appId);

            $refNo = $visa_application[0]["ref_no"];
            $encriptedAppId = SecureQueryString::ENCRYPT_DECRYPT($appId);
            $encriptedAppId = SecureQueryString::ENCODE($encriptedAppId);
            $encriptedRefId = SecureQueryString::ENCRYPT_DECRYPT($refNo);
            $encriptedRefId = SecureQueryString::ENCODE($encriptedRefId);

            $this->payReceiptUrl = $nisUrl . 'VisaArrivalProgram/visaArrivalPrintReceipt?visa_app_id=' . $encriptedAppId . '&visa_app_refId=' . $encriptedRefId;
            $this->ackUrl = $nisUrl . 'VisaArrivalProgram/visaArrivalAcknowledgmentSlip?id=' . $encriptedAppId;
            $this->passport_application = $visa_application;
        }
    }

    protected function getPassportRecord($id) {
        $passport_application = Doctrine_Query::create()
                        ->select("pa.*,pa.processing_country_id as processing_passport_id, pd.*, pci.*, c.country_name as cName, s.state_name as sName, pc.country_name as passportPCountry, ps.state_name as passportPState, em.embassy_name as passportPEmbassy, po.office_name as passportPOffice, pt.id, pt.var_value as passportType,cs.state_name as contact_state")
                        ->from('PassportApplication pa')
                        ->leftJoin('pa.PassportApplicationDetails pd')
                        ->leftJoin('pa.PassportApplicantContactinfo pci')
                        ->leftJoin('pci.Country c', 'c.id=pci.nationality_id')
                        ->leftJoin('pci.State cs', 'cs.id=pci.state_of_origin')
                        ->leftJoin('pd.State s', 's.id=pd.stateoforigin')
                        ->leftJoin('pa.Country pc', 'pc.id=pa.processing_country_id')
                        ->leftJoin('pa.State ps', 'ps.id=pa.processing_state_id')
                        ->leftJoin('pa.EmbassyMaster em', 'em.id=pa.processing_embassy_id')
                        ->leftJoin('pa.PassportOffice po', 'po.id=pa.processing_passport_office_id')
                        ->leftJoin('pa.PassportAppType pt')
                        ->Where('pa.id=' . $id)
                        ->execute()->toArray(true);

        if (isset($passport_application) && is_array($passport_application) && count($passport_application) > 0) {
            if ($passport_application[0]['sName'] == '') {
                $passport_application[0]['sName'] = $passport_application[0]['contact_state'];
            }
        }
        return $passport_application;
    }

    protected function getVisaFreshRecord($id) {
        $visa_application = Doctrine::getTable('VisaApplication')->getVisaFreshRecord($id);
        return $visa_application;
    }

    protected function getVisaOnArrivalFreshRecord($id) {
        $visa_application = Doctrine::getTable('VapApplication')->getVisaFreshRecord($id);
        return $visa_application;
    }

    protected function getFreezoneFreshRecord($id) {
        $visa_application = Doctrine::getTable('VisaApplication')->getFreezoneFreshRecord($id);
        return $visa_application;
    }

    //Start: kuldeep for support user request handling

    public function executeSupportRequestPendingList(sfWebRequest $request) {


        $fname = $request->getParameter("fname");
        $lname = $request->getParameter("lname");
        $email = $request->getParameter("email");
        $order_number = $request->getParameter("order_number");

        //get all pending support request
        $supportReqAllQuery = Doctrine::getTable('SupportRequestList')->getAllPendingRequest($fname, $lname, $email, $order_number);
        $page = 1;
        if ($request->hasParameter('page')) {
            $page = $request->getParameter('page');
        }
        $this->pager = new sfDoctrinePager('SupportRequestList', sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($supportReqAllQuery);
        $this->pager->setPage($this->getRequestParameter('page', $page));
        $this->pager->init();
        //end of pending list
    }

    public function executeSupportRequestAssignedList(sfWebRequest $request) {

        $fname = $request->getParameter("fname");
        $lname = $request->getParameter("lname");
        $email = $request->getParameter("email");
        $order_number = $request->getParameter("order_number");
        //get assigned support reuqest
        $supportReqAssignedQuery = Doctrine::getTable('SupportRequestAssignment')->getAllAssignedRequest($this->getUser()->getGuardUser()->getId(), $fname, $lname, $email,$order_number);

        $page = 1;
        if ($request->hasParameter('page')) {
            $page = $request->getParameter('page');
        }
        $this->pager = new sfDoctrinePager('SupportRequestList', sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($supportReqAssignedQuery);
        $this->pager->setPage($this->getRequestParameter('page', $page));
        $this->pager->init();
        //end of assigned list
    }

    public function executePickSupportRequest(sfWebRequest $request) {
        $this->requestId = Settings::decryptInput($request->getParameter("requestId"));
        $this->mode = $request->getParameter("mode");
        $this->page = $request->getParameter("page");
        $objSupportReq = new SupportRequestManager();

        if ($this->mode == "pick") {
            //update request status Pending -> Assigned
            SupportRequestListTable::getInstance()->updateRequestStatus($this->requestId, 'Assigned');
            //save assignment
            $objSupportReq->saveRequestAssignment($this->getUser()->getGuardUser()->getId(), $this->requestId);
        }
        //get request details
        $arrReuqestDetails = SupportRequestListTable::getInstance()->getRequestDetails($this->requestId);
        $arrNormalization = $objSupportReq->processReuqestData($arrReuqestDetails);
        $this->objApp = CartItemsTransactionsTable::getInstance();
        //customer/support comments
        $this->reuqestDetailsArr = $arrNormalization['reuqestDetailsArr'];
        $this->arrRequestComments = $arrNormalization['arrRequestComments'];


        //get All Support User List
        $loggedInUserName = $this->getUser()->getGuardUser();
        $this->arrSupportUser = $objSupportReq->getAllSupportUser($loggedInUserName);
        $this->setTemplate('pickSupportRequest');
    }

    public function executeSaveSupportUserActivities(sfWebRequest $request) {
        $param = $request->getPostParameters();

        $objSupportReq = new SupportRequestManager();
        $userId = $this->getUser()->getGuardUser()->getId();

        if ($param['mode'] == 'close') {
           $msg = "Closed";
            //update request status
            $updateRequestStatus = SupportRequestListTable::getInstance()->updateRequestStatus($param['hdn_requestId'], 'Approved');
            //update comments
            $isUpdated = $objSupportReq->setComments($param, $userId);
            $senMailUrl = "notification/supportCloseMail";

            $this->auditPaymentTransaction($param['hdn_requestId']);
            sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
            $mailTaskId = EpjobsContext::getInstance()->addJob('SupportCloseMail', $senMailUrl, array('request_id' => $param['hdn_requestId']));
            sfContext::getInstance()->getLogger()->debug("scheduled support request response successful mail job with id: $mailTaskId");
        }
        if ($param['mode'] == 'assign') {
            $msg = "Assigned";
            //update request status
            $updateRequestStatus = SupportRequestListTable::getInstance()->updateRequestStatus($param['hdn_requestId'], 'Assigned');
            //update comments
            $isUpdated = $objSupportReq->setComments($param, $userId);
        }
        if ($param['mode'] == 'respond') {
            $msg = "Responed";
            //update request status
            $updateRequestStatus = SupportRequestListTable::getInstance()->updateRequestStatus($param['hdn_requestId'], 'Responded');
            //update comments
            $isUpdated = $objSupportReq->setComments($param, $userId);
            sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
            $url = url_for("requestRefund/supportRequestDetail", true);
            $senMailUrl = "notification/supportResponseMail";

            $this->auditPaymentTransaction($param['hdn_requestId']);
            $mailTaskId = EpjobsContext::getInstance()->addJob('SupportResponseMail', $senMailUrl, array('request_id' => $param['hdn_requestId'], 'url' => $url));
            sfContext::getInstance()->getLogger()->debug("scheduled support request response successful mail job with id: $mailTaskId");
        }
        if($param['hdn_mode'] == 'pick'){
          $redirecturl = 'supportRequestPendingList';
        }else if($param['hdn_mode'] == 'reply'){
          $redirecturl = 'supportRequestAssignedList';
        }

        $this->getUser()->setFlash('notice', "Request has been ".$msg." successfully");
        $this->redirect('requestRefund/'.$redirecturl.'?page='.$param['hdn_page']);
    }

    public function auditPaymentTransaction($param) {
        //Log audit Details
        $supportReqList = Doctrine::getTable('SupportRequestList')->find($param);

        $order_number = $supportReqList->getOrderNumber();
        $requestId = $supportReqList->getId();
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('EPortal'));
        $applicationArr = array(new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_P4MTXNID, $requestId, $requestId));
        $eventHolder = new pay4meAuditEventHolder(
                        EpAuditEvent::$CATEGORY_TRANSACTION,
                        EpAuditEvent::$SUBCATEGORY_TRANSACTION_PAYMENT,
                        EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_TRANSACTION_PAYMENT, array('request_id' => $requestId, 'order_number' => $order_number)),
                        $applicationArr);

        sfContext::getInstance()->getEventDispatcher()->notify(new sfEvent($eventHolder, 'epAuditEvent'));
    }

    //End:Kuldeep for Support user request handling
    //Start : Amit

    public function executeSupportRequestList(sfWebRequest $request) {

        $supReqListQuery = Doctrine::getTable('SupportRequestList')->searchSupReqList($this->getUser()->getGuardUser()->getId());

        $page = 1;
        if ($request->hasParameter('page')) {
            $page = $request->getParameter('page');
        }
        $this->pager = new sfDoctrinePager('SupportRequestList', sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($supReqListQuery);
        $this->pager->setPage($this->getRequestParameter('page', $page));
        $this->pager->init();
    }

    public function executePendingReqReport(sfWebRequest $request) {
        $time = mktime(0, 0, 0, date("m"), date("d") - 4, date("Y"));
        $prev4DaysDate = date("Y/m/d", $time);

        $pendingReqRep = Doctrine::getTable('SupportRequestList')->pendingReqReport($prev4DaysDate);
        $page = 1;
        if ($request->hasParameter('page')) {
            $page = $request->getParameter('page');
        }
        $this->pager = new sfDoctrinePager('SupportRequestList', sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($pendingReqRep);
        $this->pager->setPage($this->getRequestParameter('page', $page));
        $this->pager->init();

        $pendingReport = $pendingReqRep->execute()->toArray();
//              echo "<pre>";print_r($pendingReport);
        $this->filename = 'Pending Support Request Report.xls';
        $excel = new ExcelWriter('uploads/excel/' . $this->filename);
        if ($excel == false) {
            echo $excel->error;
        }
        $myArr = array("<b>S.No.</b>", "<b>Support Request Category</b>", "<b>Sub Support Category</b>", "<b>Order Number</b>", "<b>Status</b>", "<b>Request Date</b>");
        $excel->writeLine($myArr);
        $count = count($pendingReport);
        for ($i = 0; $i < $count; $i++) {
            $pendingReport[$i]['created_at'] = date_format(date_create($pendingReport[$i]['created_at']),'d-m-Y');
            if(isset ($pendingReport[$i]['sub_support_category']) && $pendingReport[$i]['sub_support_category'] != ''){
                $subCategory = $pendingReport[$i]['sub_support_category'];
            }else {
                $subCategory = 'N/A';
            }
            $arrayData = array("'".($i + 1), "'".$pendingReport[$i]['support_category'], "'".$subCategory, "'".$pendingReport[$i]['order_number'], "'".$pendingReport[$i]['status'], "'".$pendingReport[$i]['created_at']);
            $excel->writeLine($arrayData);
        }
        $excel->close();
    }

    public function executeSupportRequestDetail(sfWebRequest $request) {

        $this->setTemplate('supportRequestDetail');
        $this->requestId = Settings::decryptInput($request->getParameter('request_id'));
        $this->requestDetails = Doctrine::getTable('SupportRequestList')->findById($this->requestId);
        $this->requestCommentsDetails = Doctrine::getTable('SupportRequestComments')->getRequestDetails($this->requestId);
        $request->setParameter('request_id', $this->requestId);

        $this->form = new SupportRequestCommentsForm(null, array('req_id' => $this->requestId, 'user_id' => $this->getUser()->getGuardUser()->getId()));

        if ($request->isMethod('post')) {
            $this->form->bind($request->getPostParameter($this->form->getName()), $request->getFiles($this->form->getName()));
            if ($this->form->isValid()) {
                $params = $request->getPostParameters();
                $customerComment = $params['support_request_comments']['customer_comments'];
                $requestId = $params['support_request_comments']['request_id'];
                $userId = $params['support_request_comments']['user_id'];

                $documentArray = $request->getFiles($this->form->getName());
                // echo "<pre>";print_r($documentArray);die;
                //upload image
                $path = '/support_request';
                $final_path = sfConfig::get('sf_upload_dir') . $path;
                if (is_dir($final_path) == '') {
                    mkdir($final_path, 0777, true);
                    chmod($final_path, 0777);
                }
                $strFileName = '';
                foreach ($documentArray as $val) {
                    if (!empty($val['name']) && $val['name'] != '') {
                        $tmpfileName = $val['tmp_name'];
                        $file_name = $userId . '_' . time() . '_' . $val['name'];
                        $path_to_upload = $final_path . '/' . $file_name;
                        $strFileName .= $file_name . "$$$$";
                        $upload = move_uploaded_file($tmpfileName, $path_to_upload);
                        chmod($path_to_upload, 0755);
                    }
                }
                $finalImageNameArr = array();
                if (!empty($strFileName)) {
                    $finalStr = substr($strFileName, 0, -4);
                    $finalImageNameArr = explode("$$$$", $finalStr);
                }

                $supportRequestManagerObj = new SupportRequestManager();
                $supportRequestManagerObj->setMultipleComment($customerComment, $requestId, $userId, $finalImageNameArr);


                $custComment = $params['support_request_comments']['customer_comments'];
                //          echo $custComment ;
                $subject = "Support Request Mail";
                $partialName = 'mailTemplate';
                sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));

                $notificationUrl = "notification/custCommentMailToAdmin";

                //mail to user
                $taskId = EpjobsContext::getInstance()->addJob('custCommentMailToAdmin', $notificationUrl, array('cust_comment' => $custComment, 'request_id' => $this->requestId));
                sfContext::getInstance()->getLogger()->debug("sceduled job with id: $taskId");
                $this->getUser()->setFlash('notice', "Request responded Successfully");
                $this->redirect('requestRefund/supportRequestList');
            } else {
                echo $this->form->renderGlobalErrors();
            }
        }
    }

    public function executeSendCustomerResponse(sfWebRequest $request) {
        $userId = $this->getUser()->getGuardUser()->getId();
        $requestId = $request->getParameter('request_id');
        $custComment = trim($request->getParameter('custmerResText'));
        $supReqManager = new supportRequestManager();
        $commentInserted = $supReqManager->setCustomerComment($custComment, $requestId, $userId);
        if ($commentInserted) {

            $subject = "Support Request Mail";
            $partialName = 'mailTemplate';
            sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));

            $notificationUrl = "notification/custCommentMailToAdmin";
            //            $senMailUrl = "notification/paymentRefundRequestMailAdmin" ;
            //mail to user
            $taskId = EpjobsContext::getInstance()->addJob('custCommentMailToAdmin', $notificationUrl, array('cust_comment' => $custComment, 'request_id' => $requestId));
            sfContext::getInstance()->getLogger()->debug("sceduled job with id: $taskId");

            //mail to admin
            //            $mailTaskId = EpjobsContext::getInstance()->addJob('paymentRefundRequestMailAdmin',$senMailUrl, array('concatinatedMsg' =>$concatinatedMsg,'orderNumber'=>$orderNumber));
            //            sfContext::getInstance()->getLogger()->debug("sceduled job with id: $mailTaskId");
        }
    }

    public function executeThanks() {
        $this->setTemplate("thanks");
    }
    // END : Amit

    public function executeUploadImage(sfWebRequest $request) {


        if (isset($_FILES['order_user_proof']) && isset($_FILES['order_user_proof']['tmp_name']) && $_FILES['order_user_proof']['tmp_name'] != "") {
            if ($_FILES['order_user_proof']['type'] != "image/jpeg" && $_FILES['order_user_proof']['type'] != "image/jpg" && $_FILES['order_user_proof']['type'] != "image/pjpeg" && $_FILES['order_user_proof']['type'] != "image/pjpg" && $_FILES['order_user_proof']['type'] != "image/png" && $_FILES['order_user_proof']['type'] != "image/gif") {
                $error = 'File type not supported.';
            } else if ($_FILES['order_user_proof']['size'] / 1024 > 400) {
                $error = 'The uploaded image is bigger than the allowed image size.';
            } else {
                $error = '';
            }
        }

        if (!empty($error)) {
            echo "<script>window.parent.document.getElementById('user_proof_error').innerHTML='" . $error . "'</script>";
            //echo "<script>window.parent.document.getElementById('order_user_proof').value  = ''</script>";
            ?>
               <script>
                    window.parent.document.getElementById('uploadFile_div').innerHTML = window.parent.document.getElementById('uploadFile_div').innerHTML;
               </script>
            <?php
        } else {
            echo "<script>window.parent.document.getElementById('user_proof_error').innerHTML=''</script>";
        }
        echo "<script>window.parent.document.getElementById('smb_frm').disabled  = false;</script>";
        echo "<script>window.parent.document.getElementById('smb_frm').value  = 'Submit'</script>";
        exit;
    }
}
?>
