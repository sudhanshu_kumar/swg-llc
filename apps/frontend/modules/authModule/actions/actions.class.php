<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class authModuleActions extends sfActions {

    public function executeOpenIdLogin(sfWebRequest $request) {

    }

    public function executeOpenIdDetails(sfWebRequest $request) {

        $screenName = null;
        $obj = new EpOpenIDExt();
        $identType = $request->getParameter('openid_identifier');
        $appVars = $request->getParameter('appVars');        
        $this->getUser()->getAttributeHolder()->remove('appVars');
        $this->getUser()->setAttribute('appVars', $appVars);
        $referer = $request->getReferer();
        if (strpos($referer, 'report/getReceipt') !== false) {            
            $this->getUser()->setAttribute('login_direct', base64_encode($referer));
        }else if (strpos($referer, 'visaArrival/searchFlightDetail') !== false) {
            $this->getUser()->setAttribute('login_direct', base64_encode($referer));
        }

        if ($request->getParameter('srcname') != "") {
            $screenName = $request->getParameter('srcname');
        }
        if (!isset($_GET['openid_mode'])) {
            EpOpenIDExt::getOpenidProvider($identType, $screenName);
        }

        try {
            if (isset($_GET['openid_mode']) && $_GET['openid_mode'] != "cancel") {
                //if(isset($_GET['openid_mode'])) {

                $userDetails = EpOpenIDExt::getOpenidProviderDetails($_GET, $identType);                
                $this->getUser()->setAttribute('data', $userDetails);
                //if($identType != 'pay4me') {
?>
                <script>
                    window.opener.location = 'authModule/authUserDetails';
                    self.close();
                </script>
<?php

                exit;
                //}
                // $this->redirect('authModule/preAuthentication');
                //$this->redirect('authModule/authUserDetails');
            }
            if (isset($_GET['openid_mode']) && $_GET['openid_mode'] == "cancel") {
                //if($identType != 'pay4me') {
?>
                <script>
                    self.close();
                </script>
<?php

                exit;
                //}
                //throw new Exception("User has canceled authentication!");
            }
        } catch (Exception $e) {
            // throw new Exception("your authentication has been failed due to some network problem !");
            // $this->getUser()->setFlash('notice','<b><font color=red>'.$e->getMessage().'</font></b>');
            $this->redirect('authModule/openIdLogin');
        }
        exit;
    }

    public function executePreAuthentication(sfWebRequest $request) {
        if ($request->getParameter('email')) {
            $data = $this->getUser()->getAttribute('data');
            if ($request->getParameter('email') == $data[DbtoOpendidAttrMap::$EMAIL_KEY]) {
                $request->setParameter('authStatus', 'pass');
                $this->forward($this->moduleName, 'authUserDetails');
            } else {
//        $this->getUser()->setFlash('notice','<b><font color=red>You are not authorized, Please try again !</font></b>');
                $errorMsgObj = new ErrorMsg();
                $errorMsg = $errorMsgObj->displayErrorMessage("E044", '001000');
                $this->getUser()->setFlash('notice', $errorMsg);
                $this->redirect('authModule/openIdLogin');
            }
        }
    }

    /**
     *
     * @param <type> $request
     * KYC form open...
     */

    public function executeAuthUserDetails(sfWebRequest $request) {
        $authStatus = $request->getParameter('authStatus');        
        $authStatus = 'pass';
        if ($authStatus == 'pass') {
            $data = $this->getUser()->getAttribute('data');
            
            if (empty($data)) {
                $this->redirect('welcome/index');
            }            
            $appVars = $this->getUser()->getAttribute('appVars', '0');            

            /**
             * Start Here
             * [WP:069] => CR: 102
             * First check email exist or not...
             * If email exist then check username and do accordingly...
             * If not match open KYC form...
             */
            ## Getting all payment user associated with given email id...
            $userArray = Doctrine::getTable('UserDetail')->getPaymentUserByEmail($data[DbtoOpendidAttrMap::$EMAIL_KEY]);
            
            $usersCount = count($userArray);
            if($usersCount >= 1){
               /**
                * [WP: 081] => CR:117
                * If Email exists more than one then if condition will work...
                * A page will be open with all login account information...
                * User will choose one account accordingly...
                */
               if($usersCount > 1){
                   $this->email = $data[DbtoOpendidAttrMap::$EMAIL_KEY];
                   $this->userArray = $userArray;
                   $this->setTemplate('duplicateAccountList');
               }else{
                   ## Fetching user data using the userId of first row..
                   $userData = Doctrine::getTable('sfGuardUser')->find($userArray[0]['user_id']);                   
                   if(!empty($userData)){
                       ## Matching given userkey match with the key stored in database...
                       if($userData->getUsername() != $data[DbtoOpendidAttrMap::$USER_KEY]){
                           $userData = Doctrine::getTable('sfGuardUser')->find($userArray[0]['user_id']);
                           $userData->setUsername($data[DbtoOpendidAttrMap::$USER_KEY]);
                           $userData->save();

                           ## Fetching login type...
                           $loginType = Settings::getLoginType($data[DbtoOpendidAttrMap::$USER_KEY]);

                           ## Inserting user login log if user key is different...
                           $this->insertUserLoginLogs($loginType, $userArray[0]['user_id']);
                       }
                   }//End of if(!empty($userData)){...

                   $this->redirect('authModule/integSfUserDetails');
               }
              
            }
            /**
             * End Here
             */
            
            $nameArr = explode(" ", $data[DbtoOpendidAttrMap::$FULL_NAME_KEY]);
            
            if (count($nameArr) > 1) {
                $this->lname = $nameArr[count($nameArr) - 1];
                unset($nameArr[count($nameArr) - 1]);
                $this->fname = implode(" ", $nameArr);
            } else {
                $this->fname = $data[DbtoOpendidAttrMap::$FULL_NAME_KEY];
                $this->lname = " ";
            }
            $this->email = $data[DbtoOpendidAttrMap::$EMAIL_KEY];
        } else {
            $this->getUser()->setFlash('notice', '<b><font color=red>You are not authorized, Please try again !</font></b>');
            $this->redirect('authModule/openIdLogin');
        }
    }

    /**
     *
     * @param <type> $request
     * @return <type>
     * After KYC form submit following action performs...
     */

    public function executeIntegSfUserDetails(sfWebRequest $request) {
        $data = $this->getUser()->getAttribute('data');

        $fname = $request->getParameter('fname');
        $lname = $request->getParameter('lname');
        $address = $request->getParameter('address');
        $mobile_frefix = $request->getParameter('mobile_frefix');
        $mobile = $request->getParameter('mobile');
        $mobileNo = $mobile_frefix . $mobile;
        $uname = $data[DbtoOpendidAttrMap::$USER_KEY];
        $email = $request->getParameter('email');

        if ($request->isMethod('post')) {
            unset($_SESSION['address']);
            $_SESSION['address'] = $address;
        } else {
            $_SESSION['address'] = NULL;
        }
        $appVars = $this->getUser()->getAttribute('appVars', '0');
        //checking if mobile number already exists
        if ($mobile != null) {
            $mobile = trim($mobile); //[\]+()-]
            $mobile = str_replace("[", "", $mobile);
            $mobile = str_replace("]", "", $mobile);
            $mobile = str_replace("+", "", $mobile);
            $mobile = str_replace("(", "", $mobile);
            $mobile = str_replace(")", "", $mobile);
            $mobile = str_replace("-", "", $mobile);
            $mobile = str_replace(" ", "", $mobile);
            $mobile = "+" . $mobile;
        }

        $countMobile = Doctrine::getTable('UserDetail')->checkUniqueMobileNumber('', $mobile);
        $countEmail = Doctrine::getTable('UserDetail')->checkUniqueEmail('', $email);

        if ($countMobile > 0) {

            $errorMsgObj = new ErrorMsg();
            $errorMsg = $errorMsgObj->displayErrorMessage("E045", '001000');

            $this->getUser()->setFlash('notice', $errorMsg);
            $this->redirect('authModule/authUserDetails');
        }


        if ($countEmail > 0) {

            $userDetail = Doctrine::getTable('UserDetail')->findByEmail($email)->toArray();
            $sfGuardUser = Doctrine::getTable('SfGuardUser')->find($userDetail[0]['user_id']);
            
            ## Fetching login type...            
            $loginType = Settings::getLoginType($sfGuardUser->getUserName());
            
            $errorMsgObj = new ErrorMsg();
            $errorMsg = $errorMsgObj->displayErrorMessage("E055", '001000');

            $this->getUser()->setFlash('notice', $errorMsg. "<br/><b><font color=red>Note:</font> Your email addres is associated with ".$loginType." account in our system.</b>");
            $this->redirect('authModule/authUserDetails');
        }

        if (!isset($uname) || is_null($uname)) {
            $userCount = 1;
            if ($this->getUser() && $this->getUser()->isAuthenticated()) {
                $userId = $this->getUser()->getGuardUser()->getId();
            } else {

                return sfContext::getInstance()->getController()->redirect('@adminhome');
            }
        } else {
            $userCount = Doctrine::getTable('sfGuardUser')->chk_username($data[DbtoOpendidAttrMap::$USER_KEY]);
            if ($userCount == 0) {
                $userId = Doctrine::getTable('UserDetail')->createUser($data[DbtoOpendidAttrMap::$USER_KEY], $data[DbtoOpendidAttrMap::$EMAIL_KEY], $fname, $lname, $mobile, $address);
                
                ## Fetching login type...                
                $loginType = Settings::getLoginType($data[DbtoOpendidAttrMap::$USER_KEY]);

                ## Inserting user login log if user key is different...
                $this->insertUserLoginLogs($loginType, $userId);
                
            } else {
                $userDet = Doctrine::getTable('sfGuardUser')->findBy('username', $data[DbtoOpendidAttrMap::$USER_KEY]);                
                $userId = $userDet->getFirst()->getId();
            }
        }


        ## update last login...
        $this->updateLastLogin($userId);

        //User sigin functionality
        $userObj = Doctrine::getTable('sfGuardUser')->find($userId);

        //start:: Vineet validate payment rights of user
        if ($userObj->getUserDetail()->getPaymentRights() == '1') {
            $this->redirect("paymentProcess/blockUserMsg");
        }
        //end:: Vineet validate payment rights of user
        //start: Kuldeep
        if ($userObj->getIsActive() == 0) {
            $errorMsg = 'You are not authorize to login!!';
            $this->getUser()->setFlash('notice', $errorMsg);
            $this->redirect('pages/index');
        }
        //End:Kuldeep
        $this->getUser()->signin($userObj, false);

        $this->logOpenIDUserLoginAuditDetails($userObj->getUsername(), $userId);

        $this->getUser()->getAttributeHolder()->remove('data');
        if (!$this->getUser()->getAttributeHolder()->has('loginOnOrderRequest')) {
            $this->getUser()->getAttributeHolder()->remove('requestId');
        }

        //GETTING REQUEST ID SESSION
        $requestId = $this->getUser()->getAttribute('requestId');
        

        if ($requestId) {
            //check if cart empty
            //add application if cart is coming from NIS
            $orderRequest = Doctrine::getTable('OrderRequestDetails')->find($requestId);
            
            //get Application on NIS
            $appDetails = CartItemsTransactionsTable::getInstance()->getRetryIdDetailsOnNis($orderRequest->getRetryId());

            $application_type = $appDetails[0]['app_type'];
            if ($application_type == 'passport') {
                $application_type = 'p';
            } else if ($application_type == 'visa') {
                $application_type = 'v';
            } else if ($application_type == 'freezone') {
                $application_type = 'v';
            } else if ($application_type == 'vap') {
                $application_type = 'vap';
            }

            $application_id = $appDetails[0]['id'];

            $errorMsgObj = new ErrorMsg();

            ## If application is already processed for refund or chargeback
            $isRefunded = RollbackPaymentDetailsTable::getInstance()->isRefunded($application_id, $application_type);
            if (count($isRefunded) > 0) {
                $errorMsg = $errorMsgObj->displayErrorMessage("E049", '001000', array('isRefunded' => $isRefunded[0]['action']));
                $this->getUser()->setFlash('notice', $errorMsg);
                $this->redirect('cart/list');
            }
            
            $cartObj = new cartManager();

            /**
             * [WP: 102] => CR: 144
             * Clear cart while coming from nis merchant...
             */
            //$this->getUser()->clearCart();
            
            $arrCartItems = $this->getUser()->getCartItems();
            
            if (count($arrCartItems) == 0) {
                $addToCart = $cartObj->addToCart($application_type, $application_id);
            } else {
                $flg = false;
                foreach ($arrCartItems as $items) {
                    if ($items->getAppId() != $application_id) {
                        $flg = true;
                    }
                    if ($flg) {
                        $cartValidationObj = new CartValidation();
                        $addToCart = $cartObj->addToCart($application_type, $application_id);
                        if ($addToCart == 'common_opt_not_available') {
                            $this->getUser()->setFlash("notice", sfConfig::get("app_cart_err_message_add"), true);
                            $this->redirect('cart/list?#msg');
                        } else if ($addToCart == 'capacity_full') {
                            //start:Kuldeep for check cart capacity
                            $arrCartCapacity = array();
                            $arrCartCapacityLimit = $cartValidationObj->checkUserCartCapacity();
                            if (!empty($arrCartCapacityLimit) && count($arrCartCapacityLimit)) {
                                $arrCartCapacity = $arrCartCapacityLimit;
                            }
                            //end:Kuldeep
                            $msg = $arrCartCapacity['cart_capacity'] . " application";
                            $errorMsg = $errorMsgObj->displayErrorMessage("E047", '001000', array("msg" => $msg));
                            $this->getUser()->setFlash('notice', $errorMsg);
                            $this->redirect('cart/list');
                        } else if ($addToCart == 'amount_capacity_full') {
                            //start:Kuldeep for check cart capacity
                            $arrCartCapacity = array();
                            $arrCartCapacityLimit = $cartValidationObj->checkUserCartCapacity();
                            if (!empty($arrCartCapacityLimit) && count($arrCartCapacityLimit)) {
                                $arrCartCapacity = $arrCartCapacityLimit;
                            }
                            //end:Kuldeep
                            $errorMsg = $errorMsgObj->displayErrorMessage("E048", '001000', array("msg" => $arrCartCapacity['cart_amount_capacity']));
                            $this->getUser()->setFlash("notice", $errorMsg);
                            $this->redirect('cart/list');
                        } else if ($addToCart == "application_edit") {
                            $errorMsg = $errorMsgObj->displayErrorMessage("E058", '001000', array("msg" => ''));
                            $this->getUser()->setFlash("notice", $errorMsg);
                        }else if ($addToCart == "application_rejected") {
                            $errorMsg = $errorMsgObj->displayErrorMessage("E060", '001000', array("msg" => ''));
                            $this->getUser()->setFlash("notice", $errorMsg);
                        } else if($addToCart == "only_vap_allowed"){
                            if(strtolower($appDetails[0]['app_type']) != 'vap'){
                                $msg = ucfirst($appDetails[0]['app_type']).' application can not be added into the cart with Visa on Arrival application.';
                            }else{
                                $msg = 'Visa on Arrival application can not be added into the cart with another type of application such as passport, visa, .. etc.';
                            }
                            $this->getUser()->setFlash('notice', sprintf($msg));
                            $this->redirect('cart/list');
                        }

                        break;
                    }
                }
            }

            //generating bill
            $orderRequest = Doctrine::getTable('OrderRequestDetails')->find($requestId);
            $billObj = new billManager();
            $billObj->addBill($orderRequest->getOrderRequestId());
            if ($appVars) {
                $redirectObj = new NisManager();
                $redirectObj->redirectoNisPage($appVars);
                //$this->redirectoNisPage($appVars);
            }
            
            $this->redirect('paymentGateway/gotoApplicantVault');
//      switch(sfConfig::get('app_active_paymentgateway')) {
//        case 'authorizeNet':
//          $this->redirect('paymentProcess/orderConfirmation?requestId='.$requestId);
//          break;
//        case 'vbv':
//          $this->redirect('paymentGateway/gotoApplicantVault?requestId='.$requestId);
//          break;
//        case 'payEasy':
//        case 'grayPay':
//          $vbv_active  = settings::isVbvActive();
//          if($vbv_active) {
//            $this->redirect('paymentGateway/gotoApplicantVault?requestId='.$requestId);
//          }
//          else {
//            $this->redirect('paymentGateway/new?requestId='.$requestId);
//          }
//          break;
//        default:
//          throw new Exception("Unable to handle request !");
//          break;
//      }
        }
        //    $this->redirect('paymentProcess/orderConfirmation?requestId='.$requestId);
        //$this->redirect('vbv_configuration/vbvForm?requestId='.$requestId);
        else {            
            if ($appVars) {
                //commented by ashwani...
                //$redirectObj = new NisManager();
                //$redirectObj->redirectoNisPage($appVars);                
                ## Redirected to NIS page...
                $this->redirect('nis/index?appVars='.$appVars);
            }            
            if (($this->getUser()->hasAttribute('login_direct')) && ($this->getUser()->getAttribute('login_direct') != '')) {//print "fdfdf";exit;
                $url = base64_decode($this->getUser()->getAttribute('login_direct'));
                $this->getUser()->setAttribute('login_direct', '');             
                $this->redirect($url);                 
            }            
            $this->redirect('welcome/index');
        }
    }

    public function logOpenIDUserLoginAuditDetails($uname, $id) {
        //Log audit Details
        $eventHolder = new pay4meAuditEventHolder(
                        EpAuditEvent::$CATEGORY_SECURITY,
                        EpAuditEvent::$SUBCATEGORY_SECURITY_LOGIN,
                        EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_SECURITY_FIRST_LOGIN, array('username' => $uname)),
                        null);
        $this->dispatcher->notify(new sfEvent($eventHolder, 'epAuditEvent'));
    }

    /**
     * @Author: Vineet Malhotra
     * @param: null
     * @return: logout/session path
     * @desc: reset session
     */
    public function executeResetSession() {
        echo sfUser::ATTRIBUTE_NAMESPACE;
        $this->settemplate(null);
        die;
    }

    /**
     * [WP:069] => CR:102
     * @param <type> $request
     * @return <type>
     * This function create data for logged in...
     * Calls when facebook FB code uses...     
     */

    public function executeMakeFacebookData(sfWebRequest $request){        
        $uid = $request->getPostParameter('uid');
        $accessToken = $request->getPostParameter('accessToken');
        $email = $request->getPostParameter('email');
        $username = $request->getPostParameter('username');
        $fullname = $request->getPostParameter('fullname');

        if($uid == '' && $email =='' && $username == '' && $fullname == ''){
            $signIn = false;
        }else{
            $userDetails = array();
            $userDetails['username'] = 'https://facebook.com/uid/'.$uid;
            $userDetails['fullname'] = $fullname;
            $userDetails['email'] = $email;
            $this->getUser()->setAttribute('facebookAccessToken', $accessToken);
            $this->getUser()->setAttribute('data', $userDetails);

            ## This code requires when user comes from NIS end...
            $appVars = $request->getPostParameter('appVars');
            $this->getUser()->getAttributeHolder()->remove('appVars');
            $this->getUser()->setAttribute('appVars', $appVars);

            ## This code requires when user comes from email click...
            $referer = $request->getReferer();
            if (strpos($referer, 'report/getReceipt') !== false) {
                $this->getUser()->setAttribute('login_direct', base64_encode($referer));
            }else if (strpos($referer, 'visaArrival/searchFlightDetail') !== false) {
                $this->getUser()->setAttribute('login_direct', base64_encode($referer));
            }
            
            $signIn = true;
        }
        return $this->renderText($signIn);
    }

    /**
     * [WP:069] => CR:102
     * @param <type> $request
     * This function call after logged in from facebook...
     * Calling when window.open method calls...     
     */
    public function executeFacebookReturnLogin(sfWebRequest $request){
        
        ## Getting application http path...
        $sitePath = Settings::getHTTPpath();

        ## Create our Application instance...
        $facebook = new FacebookApi($sitePath);
        $user = $facebook->getUser();
        
        if ($user) {
          try {
            // Proceed knowing you have a logged in user who's authenticated.
            $user_profile = $facebook->api('/me');
            $uid = $user_profile['id'];
            $accessToken = $facebook->getAccessToken();
            $email = $user_profile['email'];
            //$username = $request->getPostParameter('username');
            $fullname = $user_profile['name'];

            if($uid == '' && $email =='' && $fullname == ''){
                $this->getUser()->setFlash("notice", "Unable to receive user information from facebook. Please try again.");
                ?>
                    <script>
                        window.opener.location = '<?php echo $sitePath; ?>';
                        self.close();
                    </script>
                <?php
                
            }else{ 
                $userDetails = array();
                $userDetails['username'] = 'https://facebook.com/uid/'.$uid;
                $userDetails['fullname'] = $fullname;
                $userDetails['email'] = $email;
                $this->getUser()->setAttribute('facebookAccessToken', $accessToken);
                $this->getUser()->setAttribute('data', $userDetails);

                ## This code requires when user comes from NIS end...                
                $appVars = $request->getParameter('appVars');        
                $this->getUser()->getAttributeHolder()->remove('appVars');
                $this->getUser()->setAttribute('appVars', $appVars);

                ## This code requires when user comes from email click...
                //$referer = $request->getReferer();
                $referer = $this->getUser()->getAttribute('redirectEmailUri');

                if(!empty($referer)){
                    if (strpos($referer, 'report/getReceipt') !== false) {
                        $this->getUser()->setAttribute('login_direct', base64_encode($referer));
                    }else if (strpos($referer, 'visaArrival/searchFlightDetail') !== false) {
                        $this->getUser()->setAttribute('login_direct', base64_encode($referer));
                    }
                }
                
                ## Destroy redirectEmailUri which set on home page...
                $this->getUser()->getAttributeHolder()->remove('redirectEmailUri');
                
                ## Destroy facebook session from application after successfully logged in...
                ## Session destroy if exists...
                $facebook->destroySession();
                ?>
                <script>
                    window.opener.location = '<?php echo $sitePath; ?>/authModule/authUserDetails';
                    self.close();
                </script>
                <?php                
                exit;
            }

          } catch (FacebookApiException $e) {
            error_log($e);
            $user = null;
          }
        }
        ## Session destroy if exists...
        $facebook->destroySession();        
        ?>
        <script>
            window.opener.location = '<?php echo $sitePath; ?>';
            self.close();
        </script>
        <?php
        exit;
    }

    
    

    /**
     * [WP: 069] => CR:102
     * @param <type> $loginType
     * @param <type> $userId
     * inserting login user logs...
     */
    private function insertUserLoginLogs($loginType = '', $userId = ''){
       if($loginType != '' && $userId != ''){
           $userLoginLogsObj = new UserLoginLogs();
           $userLoginLogsObj->setUserId($userId);
           $userLoginLogsObj->setLoginMode($loginType);
           $userLoginLogsObj->save();           
       }
    }

    /**
     * [WP: 081] => CR:117
     * @param <type> $request
     * This function activate user account...
     * Required Userid, email...
     */
    public function executeActivateAccount(sfWebRequest $request){

        
        $email = $request->getPostParameter('email');
        $activeAccountId = $request->getPostParameter('activeAccountId');
        $errMsg = false;
        if(!empty($activeAccountId)){
            $userDetails = Doctrine::getTable('UserDetail')->findByUserId($activeAccountId);
            if(!empty($userDetails)){
                if($email == $userDetails->getFirst()->getEmail()){

                    ## Getting all payment user associated with given email id...
                    $userArray = Doctrine::getTable('UserDetail')->getPaymentUserByEmail($email);                    
                    $usersCount = count($userArray);
                    if($usersCount >= 1){

                        foreach($userArray AS $userObj){
                            if($userObj->getUserId() != $activeAccountId){                                
                                $userAccountObj = Doctrine::getTable('UserDetail')->find($userObj->getId());
                                if(!empty($userAccountObj)){
                                    $deactivateEmail = 'unused_'.$userObj->getEmail();
                                    $userAccountObj->setEmail($deactivateEmail);
                                    $userAccountObj->save();
                                }
                            }                            
                        }                        
                        $this->redirect('authModule/authUserDetails');
                    }else{
                        $errMsg = true;
                    }
                }else{
                    $errMsg = true;
                }
            }else{
                $errMsg = true;
            }
        }else{
            $errMsg = true;
        }

        ## If any error occurs then redirect to home page...
        if($errMsg){
            $this->getUser()->setFlash("notice", "Unable to receive user information from facebook. Please try again.");
            $this->redirect('@homepage');
        }
        
    }

    /**
     * [WP:081] => CR:117
     * @param <type> $userId
     * @return <type>
     * updating last_login field...
     */
    private function updateLastLogin($userId){
        $userObj = Doctrine::getTable('sfGuardUser')->find($userId);
        if(!empty($userObj)){
            $userObj->setLastLogin(date('Y-m-d H:i:s'));
            $userObj->save();
        }
        return ;
    }

}
