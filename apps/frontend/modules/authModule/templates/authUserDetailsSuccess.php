<script>
  function imposeMaxLength(Object, MaxLen,eve)
    {
        if(Object.value.length <= MaxLen)
            {
                return true;
            }
            else
                {
                    var keynum;
                    if(window.event) // IE
                    {
                        keynum = eve.keyCode;
                    }
                    else if(eve.which) // Netscape/Firefox/Opera
                    {
                        keynum = eve.which;
                    }
                    if(keynum == 8 || keynum ==undefined)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                }
    }
</script>
<?php use_helper('Form');?>
<?php use_helper('DateForm');?>
<?php include_javascripts('jquery-1.2.6.js') ?>
<?php include_javascripts('jquery-ui.min.js') ?>


    <?php //echo ePortal_pagehead('User Details'); ?>
<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">
<?php
include_partial('global/innerHeading',array('heading'=>'User Details'));
?>
 <div class="clear"></div>
<?php echo ePortal_pagehead(''); ?>
 <div class="tmz-spacer"></div> 
    <table style="width: 100%;">
      <tbody>
        <tr>
          <td width="27%">Email Address:</td>
          <td style="color:#8A8672"><?php echo $email; ?></td>
        </tr>
      </tbody>
    </table>
    <br/>


    <?php

    $arrCountry = array('');
    ?>
    <?php echo form_tag(url_for('authModule/integSfUserDetails'),array('name'=>'openid','id'=>'openid', 'onSubmit'=>'return validateSignForm("1")')); ?>
    <input type="hidden" name="email" id="email" value="<?php echo $email; ?>">


    <table style="width: 100%;">
      <tbody>
          <tr><td class="blbar" colspan="2">Please provide the following details to complete authentication process.</td></tr>

        <tr>
          <?php echo formRowComplete('First Name<span class="red">*</span>',input_tag('fname', $fname, array('style' => 'color:#8A8672;', 'readonly' => true),'class=txt-input maxlength=30'),'','fname','err_name','username_row'); ?>
        </tr>
        <tr>
          <?php echo formRowComplete('Last Name<span class="red">*</span>',input_tag('lname', $lname, array('style' => 'color:#8A8672;', 'readonly' => true),'class=txt-input maxlength=30'),'','lname','err_lastname','username_row'); ?>
        </tr>
        <tr>
          <?php echo formRowComplete('Contact Address<span class="red">*</span>',textarea_tag('address', '', 'class=txt-input maxlength=30 onkeypress="return imposeMaxLength(this, 255,event);"'),'(upto 255 characters only)','address','err_address','address_row'); ?>
        </tr>
        <tr>
        <td  width=15%' class='ltgray'>Country<span class="red">*</span></td>
        <td>
          <select id="country_into" name="country_into">

                <option ringto="" value="">Please Select Country</option>
                <option ringto="+93-" value="93">Afghanistan</option>

                <option ringto="+355-" value="355">Albania</option>

                <option ringto="+213-" value="213">Algeria</option>

                <option ringto="+1684-" value="1684">American Samoa</option>

                <option ringto="+376-" value="376">Andorra</option>

                <option ringto="+244-" value="244">Angola</option>

                <option ringto="+1264-" value="1264">Anguilla</option>

                <option ringto="+672-" value="672">Antarctica</option>

                <option ringto="+1268-" value="1268">Antigua &amp; Barbuda</option>

                <option ringto="+54-" value="54">Argentina</option>

                <option ringto="+374-" value="374">Armenia</option>

                <option ringto="+297-" value="297">Aruba</option>

                <option ringto="+247-" value="247">Ascension Island</option>

                <option ringto="+61-" value="61">Australia</option>

                <option ringto="+43-" value="43">Austria</option>

                <option ringto="+994-" value="994">Azerbaijan</option>

                <option ringto="+1242-" value="1242">Bahamas</option>

                <option ringto="+973-" value="973">Bahrain</option>

                <option ringto="+880-" value="880">Bangladesh</option>

                <option ringto="+1246-" value="1246">Barbados</option>

                <option ringto="+375-" value="375">Belarus</option>

                <option ringto="+32-" value="32">Belgium</option>

                <option ringto="+501-" value="501">Belize</option>

                <option ringto="+229-" value="229">Benin</option>

                <option ringto="+1441-" value="1441">Bermuda</option>

                <option ringto="+975-" value="975">Bhutan</option>

                <option ringto="+591-" value="591">Bolivia</option>

                <option ringto="+387-" value="387">Bosnia/Herzegovina</option>

                <option ringto="+267-" value="267">Botswana</option>

                <option ringto="+55-" value="55">Brazil</option>

                <option ringto="+1284-" value="1284">British Virgin Islands</option>

                <option ringto="+673-" value="673">Brunei</option>

                <option ringto="+359-" value="359">Bulgaria</option>

                <option ringto="+226-" value="226">Burkina Faso</option>

                <option ringto="+257-" value="257">Burundi</option>

                <option ringto="+855-" value="855">Cambodia</option>

                <option ringto="+237-" value="237">Cameroon</option>

                <option ringto="1-" value="1">Canada</option>

                <option ringto="+238-" value="238">Cape Verde Islands</option>

                <option ringto="+1345-" value="1345">Cayman Islands</option>

                <option ringto="+236-" value="236">Central African Republic</option>

                <option ringto="+235-" value="235">Chad Republic</option>

                <option ringto="+56-" value="56">Chile</option>

                <option ringto="+86-" value="86">China</option>

                <option ringto="+6724-" value="6724">Christmas Island</option>

                <option ringto="+6722-" value="6722">Cocos Keeling Island</option>

                <option ringto="+57-" value="57">Colombia</option>

                <option ringto="+269-" value="269">Comoros</option>

                <option ringto="+243-" value="243">Congo Democratic Republic</option>

                <option ringto="+242-" value="242">Congo, Republic of</option>

                <option ringto="+682-" value="682">Cook Islands</option>

                <option ringto="+506-" value="506">Costa Rica</option>

                <option ringto="+225-" value="225">Cote D'Ivoire</option>

                <option ringto="+385-" value="385">Croatia</option>

                <option ringto="+53-" value="53">Cuba</option>

                <option ringto="+357-" value="357">Cyprus</option>

                <option ringto="+420-" value="420">Czech Republic</option>

                <option ringto="+45-" value="45">Denmark</option>

                <option ringto="+253-" value="253">Djibouti</option>

                <option ringto="+1767-" value="1767">Dominica</option>

                <option ringto="+1809, 1829-" value="1809, 1829">Dominican Republic</option>

                <option ringto="+593-" value="593">Ecuador</option>

                <option ringto="+20-" value="20">Egypt</option>

                <option ringto="+503-" value="503">El Salvador</option>

                <option ringto="+240-" value="240">Equatorial Guinea</option>

                <option ringto="+291-" value="291">Eritrea</option>

                <option ringto="+372-" value="372">Estonia</option>

                <option ringto="+251-" value="251">Ethiopia</option>

                <option ringto="+500-" value="500">Falkland Islands</option>

                <option ringto="+298-" value="298">Faroe Island</option>

                <option ringto="+679-" value="679">Fiji Islands</option>

                <option ringto="+358-" value="358">Finland</option>

                <option ringto="+33-" value="33">France</option>

                <option ringto="+596-" value="596">French Antilles/Martinique</option>

                <option ringto="+594-" value="594">French Guiana</option>

                <option ringto="+689-" value="689">French Polynesia</option>

                <option ringto="+241-" value="241">Gabon Republic</option>

                <option ringto="+220-" value="220">Gambia</option>

                <option ringto="+995-" value="995">Georgia</option>

                <option ringto="+49-" value="49">Germany</option>

                <option ringto="+233-" value="233">Ghana</option>

                <option ringto="+350-" value="350">Gibraltar</option>

                <option ringto="+30-" value="30">Greece</option>

                <option ringto="+299-" value="299">Greenland</option>

                <option ringto="+1473-" value="1473">Grenada</option>

                <option ringto="+590-" value="590">Guadeloupe</option>

                <option ringto="+1671-" value="1671">Guam</option>

                <option ringto="+502-" value="502">Guatemala</option>

                <option ringto="+224-" value="224">Guinea Republic</option>

                <option ringto="+245-" value="245">Guinea-Bissau</option>

                <option ringto="+592-" value="592">Guyana</option>

                <option ringto="+509-" value="509">Haiti</option>

                <option ringto="+504-" value="504">Honduras</option>

                <option ringto="+852-" value="852">Hong Kong</option>

                <option ringto="+36-" value="36">Hungary</option>

                <option ringto="+354-" value="354">Iceland</option>

                <option ringto="+91-" value="91">India</option>

                <option ringto="+62-" value="62">Indonesia</option>

                <option ringto="+964-" value="964">Iraq</option>

                <option ringto="+353-" value="353">Ireland</option>

                <option ringto="+972-" value="972">Israel</option>

                <option ringto="+39-" value="39">Italy</option>

                <option ringto="+1876-" value="1876">Jamaica</option>

                <option ringto="+81-" value="81">Japan</option>

                <option ringto="+962-" value="962">Jordan</option>

                <option ringto="+254-" value="254">Kenya</option>

                <option ringto="+686-" value="686">Kiribati</option>

                <option ringto="+3774-" value="3774">Kosovo</option>

                <option ringto="+965-" value="965">Kuwait</option>

                <option ringto="+996-" value="996">Kyrgyzstan</option>

                <option ringto="+856-" value="856">Laos</option>

                <option ringto="+371-" value="371">Latvia</option>

                <option ringto="+961-" value="961">Lebanon</option>

                <option ringto="+266-" value="266">Lesotho</option>

                <option ringto="+231-" value="231">Liberia</option>

                <option ringto="+218-" value="218">Libya</option>

                <option ringto="+423-" value="423">Liechtenstein</option>

                <option ringto="+370-" value="370">Lithuania</option>

                <option ringto="+352-" value="352">Luxembourg</option>

                <option ringto="+853-" value="853">Macau</option>

                <option ringto="+389-" value="389">Macedonia</option>

                <option ringto="+261-" value="261">Madagascar</option>

                <option ringto="+265-" value="265">Malawi</option>

                <option ringto="+60-" value="60">Malaysia</option>

                <option ringto="+960-" value="960">Maldives</option>

                <option ringto="+223-" value="223">Mali Republic</option>

                <option ringto="+356-" value="356">Malta</option>

                <option ringto="+692-" value="692">Marshall Islands</option>

                <option ringto="+222-" value="222">Mauritania</option>

                <option ringto="+230-" value="230">Mauritius</option>

                <option ringto="+52-" value="52">Mexico</option>

                <option ringto="+691-" value="691">Micronesia</option>

                <option ringto="+373-" value="373">Moldova</option>

                <option ringto="+377-" value="377">Monaco</option>

                <option ringto="+976-" value="976">Mongolia</option>

                <option ringto="+382-" value="382">Montenegro</option>

                <option ringto="+1664-" value="1664">Montserrat</option>

                <option ringto="+212-" value="212">Morocco</option>

                <option ringto="+258-" value="258">Mozambique</option>

                <option ringto="+95-" value="95">Myanmar (Burma)</option>

                <option ringto="+264-" value="264">Namibia</option>

                <option ringto="+674-" value="674">Nauru</option>

                <option ringto="+977-" value="977">Nepal</option>

                <option ringto="+31-" value="31">Netherlands</option>

                <option ringto="+599-" value="599">Netherlands Antilles</option>

                <option ringto="+687-" value="687">New Caledonia</option>

                <option ringto="+64-" value="64">New Zealand</option>

                <option ringto="+505-" value="505">Nicaragua</option>

                <option ringto="+227-" value="227">Niger Republic</option>

                <option ringto="+234-" value="234">Nigeria</option>

                <option ringto="+683-" value="683">Niue Island</option>

                <option ringto="+6723-" value="6723">Norfolk</option>

                <option ringto="+850-" value="850">North Korea</option>

                <option ringto="+47-" value="47">Norway</option>

                <option ringto="+968-" value="968">Oman Dem Republic</option>

                <option ringto="+92-" value="92">Pakistan</option>

                <option ringto="+680-" value="680">Palau Republic</option>

                <option ringto="+970-" value="970">Palestine</option>

                <option ringto="+507-" value="507">Panama</option>

                <option ringto="+675-" value="675">Papua New Guinea</option>

                <option ringto="+595-" value="595">Paraguay</option>

                <option ringto="+51-" value="51">Peru</option>

                <option ringto="+63-" value="63">Philippines</option>

                <option ringto="+48-" value="48">Poland</option>

                <option ringto="+351-" value="351">Portugal</option>

                <option ringto="+1787-" value="1787">Puerto Rico</option>

                <option ringto="+974-" value="974">Qatar</option>

                <option ringto="+262-" value="262">Reunion Island</option>

                <option ringto="+40-" value="40">Romania</option>

                <option ringto="+7-" value="7">Russia</option>

                <option ringto="+250-" value="250">Rwanda Republic</option>

                <option ringto="+1670-" value="1670">Saipan/Mariannas</option>

                <option ringto="+378-" value="378">San Marino</option>

                <option ringto="+239-" value="239">Sao Tome/Principe</option>

                <option ringto="+966-" value="966">Saudi Arabia</option>

                <option ringto="+221-" value="221">Senegal</option>

                <option ringto="+381-" value="381">Serbia</option>

                <option ringto="+248-" value="248">Seychelles Island</option>

                <option ringto="+232-" value="232">Sierra Leone</option>

                <option ringto="+65-" value="65">Singapore</option>

                <option ringto="+421-" value="421">Slovakia</option>

                <option ringto="+386-" value="386">Slovenia</option>

                <option ringto="+677-" value="677">Solomon Islands</option>

                <option ringto="+252-" value="252">Somalia Republic</option>

                <option ringto="+685-" value="685">Somoa</option>

                <option ringto="+27-" value="27">South Africa</option>

                <option ringto="+82-" value="82">South Korea</option>

                <option ringto="+34-" value="34">Spain</option>

                <option ringto="+94-" value="94">Sri Lanka</option>

                <option ringto="+290-" value="290">St. Helena</option>

                <option ringto="+1869-" value="1869">St. Kitts</option>

                <option ringto="+1758-" value="1758">St. Lucia</option>

                <option ringto="+508-" value="508">St. Pierre</option>

                <option ringto="+1784-" value="1784">St. Vincent</option>

                <option ringto="+249-" value="249">Sudan</option>

                <option ringto="+597-" value="597">Suriname</option>

                <option ringto="+268-" value="268">Swaziland</option>

                <option ringto="+46-" value="46">Sweden</option>

                <option ringto="+41-" value="41">Switzerland</option>

                <option ringto="+963-" value="963">Syria</option>

                <option ringto="+886-" value="886">Taiwan</option>

                <option ringto="+992-" value="992">Tajikistan</option>

                <option ringto="+255-" value="255">Tanzania</option>

                <option ringto="+66-" value="66">Thailand</option>

                <option ringto="+228-" value="228">Togo Republic</option>

                <option ringto="+690-" value="690">Tokelau</option>

                <option ringto="+676-" value="676">Tonga Islands</option>

                <option ringto="+1868-" value="1868">Trinidad &amp; Tobago</option>

                <option ringto="+216-" value="216">Tunisia</option>

                <option ringto="+90-" value="90">Turkey</option>

                <option ringto="+993-" value="993">Turkmenistan</option>

                <option ringto="+1649-" value="1649">Turks &amp; Caicos Island</option>

                <option ringto="+688-" value="688">Tuvalu</option>

                <option ringto="+256-" value="256">Uganda</option>

                <option ringto="+380-" value="380">Ukraine</option>

                <option ringto="+971-" value="971">United Arab Emirates</option>

                <option ringto="+44-" value="44">United Kingdom</option>

                <option ringto="+598-" value="598">Uruguay</option>

                <option ringto="1-" value="1">USA/Canada</option>

                <option ringto="+998-" value="998">Uzbekistan</option>

                <option ringto="+678-" value="678">Vanuatu</option>

                <option ringto="+3966-" value="3966">Vatican City</option>

                <option ringto="+58-" value="58">Venezuela</option>

                <option ringto="+84-" value="84">Vietnam</option>

                <option ringto="+1340-" value="1340">Virgin Islands (US)</option>

                <option ringto="+681-" value="681">Wallis/Futuna Islands</option>

                <option ringto="+967-" value="967">Yemen Arab Republic</option>

                <option ringto="+260-" value="260">Zambia</option>

                <option ringto="+263-" value="263">Zimbabwe</option>

</select><div id="err_country" class="red"></div>
        </td>
        </tr>
        <tr>
            <td class="ltgray" width="15%">
                Mobile Number
                <span class="red">*</span>
            </td>
            <td width="41%">
                <input type="text" name="mobile_frefix" id="mobile_frefix" style="width:38px;border:1px solid #A3ABB8;padding:2px;" readonly>
                <input id="mobile" class="txt-input" type="text" maxlength="30" value="" name="mobile"/>
                <div id="err_mobile_no" class="red"></div>
            </td>
        </tr>
        <tr><td colspan="2"><p class="leftFl">*Mandatory Information</p></td></tr>

       <!-- <tr>
          <?php //echo formRowComplete('Mobile Number<span class="red">*</span>',input_tag('mobile', $sf_params->get('mobile'), 'class=txt-input maxlength=30'),'','mobile','err_mobile_no','username_row'); ?>
        </tr>-->
        <tr><td colspan="2" align="center">
          <input type="submit" onclick="" value="Submit" class="loginbutton">
        </td>
        </tr>
      </tbody>
    </table>


  </div>
</div>
<div class="content_wrapper_bottom"></div>
<script>
    $(document).ready(function() {
        $("#country_into").change(function() {
            var phone_prefix = $('#country_into option:selected').attr('ringto');
            $('#mobile_frefix').val(phone_prefix);

        });
    });
  function validateSignForm(submit_form)
  {
        var err  = 0;
//        if($('#fname').val() == "")
//        {
//            $('#err_name').html("Please enter First Name");
//            err = err+1;
//        }
//        else
//        {
//            $('#err_name').html("");
//        }
//        if($('#fname').val() != ""){
//            if(validateString($('#fname').val()))
//            {
//                $('#err_name').html("Please enter Valid First Name");
//                err = err+1;
//            }
//            else
//            {
//                $('#err_name').html("");
//            }
//        }
//        if($('#lname').val() == "")
//        {
//            $('#err_lastname').html("Please enter Last Name");
//            err = err+1;
//        }
//        else
//        {
//            $('#err_lastname').html("");
//        }
//        if($('#lname').val() != ""){
//            if(validateString($('#lname').val()))
//            {
//                $('#err_lastname').html("Please enter Valid Last Name");
//                err = err+1;
//            }
//            else
//            {
//                $('#err_lastname').html("");
//            }
//        }

        if(jQuery.trim($('#address').val()) == "")
        {
            $('#err_address').html("Please enter Address");
            err = err+1;
        }
        if(jQuery.trim($('#address').val()) != "")
        {
            if(validateAddress($('#address').val()))
            {
                $('#err_address').html(invalid_addr_msg);
                err = err+1;
            }
            else
            {
                $('#err_address').html("");
            }
        }
        if($('#country_into').val() == "")
                {

                    $('#err_country').html("Please select Country");
                    err = err+1;
                }
                else
                {
                    $('#err_country').html("");
                }

        if(jQuery.trim($('#mobile').val()) == "")
        {
            if(validatePhone($('#mobile').val()))
            {
                $('#err_mobile_no').html("Please enter Mobile number");
                err = err+1;
            }
            else
            {
                $('#err_mobile_no').html("");
            }
        }
        if($('#mobile').val() != "")
        {
            if(validatePhone($('#mobile').val()))
            {

                $('#err_mobile_no').html("Please enter Valid Mobile number <br/>Mobile Number should be between 8-20 digits");
                err = err+1;
            }
            else
            {
                $('#err_mobile_no').html("");
            }
        }

        if(err == 0)
        {
            if(submit_form == 1)
            {
                return true;
            }
        }else{
            return false;
        }

  }
  function validateString(str) {
    var reg = /^([A-Za-z0-9])+$/;
    if(reg.test(str) == false) {
      return true;
    }
    return false;
  }
</script>
