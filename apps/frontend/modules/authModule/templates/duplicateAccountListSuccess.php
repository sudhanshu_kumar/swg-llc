<!-- THIS FILE CONTAINS DUPLICATE ACCOUNT LIST -->
<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
    <div class="content_container">
        <?php include_partial('global/innerHeading',array('heading'=>'Account verification for SW Global LLC'));?>
        <div class="clear"></div>
        <div>

            <!--h2 class="successBox">Email verification for <em style="font-size:23px;">SW Global LLC Account.</em></h2-->
            <br/>
            <div class="clearfix"></div>

            <div class="inst_bg_step_box" id="inst_bg_step1_id" style="display:block;height:80px;width:900px;padding:10px;">
                <div class="info_box">
                    There are the multiple accounts on SW Global LLC registered with the your email <strong>'<?php echo $email; ?>'</strong>.
                    <br /><br />
                    There can only be one account active with an email id. Please select one of the following account which shall be your active account. On submission of the activation request, the other accounts will be deactivated and will no longer be accessible to you.
                    <br /><br />
                    The further transactions can be made from the active account.
                </div>
            </div>            
            <div class="clear"></div>
            <br />
            <?php echo form_tag(url_for('authModule/activateAccount'),array('name'=>'activateFrm','id'=>'activateFrm')); ?>
            <table width="100%" cellspacing="0" cellpadding="0" style="padding-top:20px;"  >
                <input type="hidden" name="email" id="email" value="<?php echo $email; ?>" />
               <?php foreach($userArray AS $userObj) { ?>
                   <?php
                        ## Fetching login type...
                        $loginType = Settings::getLoginType($userObj->getSfGuardUser()->getUsername());
                        
                        ## Fetching logged in user id...
                        $userId = $userObj->getUserId();
                        
                        ## Fetching user registered card...
                        $userCardDetails = Doctrine::getTable('ApplicantVault')->getUserCardDetails($userId);
                        
                        ## Fetching user success transactions...
                        $userPaidTransactions = Doctrine::getTable('ipay4meOrder')->userPaidApplication($userId, 0, 'desc');
                        
                        $totalTransactions = count($userPaidTransactions);
                        if($totalTransactions > 0){                            
                            $lastLogin = date_format(date_create($userPaidTransactions->getFirst()->getUpdatedAt()), 'd/F/Y');
                        }else{
                            $lastLogin = 'N/A';
                        }
                   ?>


                   <tr>
                    <td class="grey_bg_table">
                        <table width="100%" cellspacing="0" cellpadding="0" >
                            <tr>
                                <td>
                                    <table width="100%" cellspacing="0" cellpadding="0" >
                                        <tr>
                                            <td width="20"><input type="radio" name="activeAccountId" value="<?php echo $userId; ?>" /></td>
                                            <td>
                                                <?php
                                                switch(strtolower($loginType)){
                                                  case 'google':
                                                      echo image_tag('google-active.png', array('alt' => 'GOOGLE', 'title' => 'Google OpenID'));
                                                      break;
                                                  case 'openid':
                                                      echo image_tag('openid-active.png', array('alt' => 'PAY4ME', 'title' => 'SW Global LLC OpenID'));
                                                      break;
                                                  case 'yahoo':
                                                      echo image_tag('yahoo-active.png', array('alt' => 'YAHOO', 'title' => 'Yahoo OpenID'));
                                                      break;
                                                  case 'facebook':
                                                      echo image_tag('facebook-active.png', array('alt' => 'FACEBOOK', 'title' => 'Facebook'));
                                                      break;
                                                }
                                                ?>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="100%" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="20%" class="blbar">First Name</td>
                                            <td width="20%" class="blbar">Last Name</td>
                                            <td width="15%" class="blbar">Success Transactions</td>
                                            <td width="15%" class="blbar">Registered Card</td>
                                            <td width="20%" class="blbar">Last Login</td>
                                        </tr>
                                        <tr>
                                            <td><?php echo ucfirst($userObj->getFirstName()); ?></td>
                                            <td><?php echo ucfirst($userObj->getLastName()); ?></td>
                                            <td><?php echo count($userPaidTransactions); ?></td>
                                            <td><?php echo count($userCardDetails); ?></td>
                                            <td><?php echo $lastLogin; ?></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                   </tr>


                
                <tr>
                    <td>&nbsp;</td>
                </tr>
                
                <?php } ?>
                <tr>
                    <td><input type="checkbox" name="btnCheck" id="btnCheck" value="1" />&nbsp;I have read above instructions.</td>
                </tr>
                <tr>
                    <td align="center" style="padding:20px;">
                        <input type="button" value="Activate" id="activateButton" class="loginbutton" onclick="activateAccount()" />
                        <div id="loading" class="no_display"><?php echo image_tag('/images/ajax-loader_1.gif',array('alt'=>'Loading ...', 'align' => 'absmiddle'));?><br/>&nbsp;&nbsp;Please wait...</div>
                    </td>
                </tr>
            </table>
            </form>            
        </div>
    </div>
</div>
<div class="content_wrapper_bottom"></div>
<script>
    function activateAccount(){
        var checkValues = validateMultipleRadio('activateFrm');
        if(checkValues == ''){
            alert("Please select any one account to login.");
        }else{
            if($('#btnCheck').is(':checked')){
                if(confirm('Are you sure?')){
                    $('#loading').show();
                    $('#activateButton').hide();
                    $('#activateFrm').submit();
                }
            }else{
                alert("Please check 'I have read above instructions'.");
            }            
        }
    }
</script>