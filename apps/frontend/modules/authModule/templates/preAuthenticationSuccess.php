<?php use_helper('Form');?>
<?php use_helper('DateForm');?>
<?php include_javascripts('jquery-1.2.6.js') ?>
<?php include_javascripts('jquery-ui.min.js') ?>
<div class="global_content4 clearfix">
  <div class="brdBox">
    <?php echo ePortal_pagehead('Email Verification'); ?>
    <p class="flRight">*Required Information</p><br/>
    <h2 class="successBox">Email verification for <em style="font-size:23px;">SW Global LLC Account.</h2>
    <br/>
    <div class="clearfix"></div>
    <?php echo form_tag(url_for('authModule/preAuthentication'),array('name'=>'pauth','id'=>'pauth', 'onSubmit'=>'return validateSignForm("1")')); ?>
    <table style="width: 100%;">
      <tbody>
        <tr>
          <?php echo formRowComplete('Email Address<span class="red">*</span>',input_tag('email', '', 'class=txt-input maxlength=30'),'','email','err_email','username_row'); ?>
        </tr>
        <tr><td colspan="2" align="right">
          <input type="submit" onclick="" value="Submit" class="button_new">
        </td>
        </tr>
      </tbody>
    </table>
  </div>
</div>
<script>

  function validateSignForm(submit_form)
  {
        var err  = 0;
        if($('#email').val() == "")
        {
            $('#err_email').html("Please enter Email Address");
            err = err+1;
        }
        else
        {
            $('#err_email').html("");
        }

        if(err == 0)
        {
            if(submit_form == 1)
            {
                return true;
            }
        }else{
            return false;
        }

  }
  function validateString(str) {
    var reg = /^([A-Za-z0-9])+$/;
    if(reg.test(str) == false) {
      return true;
    }
    return false;
  }
</script>