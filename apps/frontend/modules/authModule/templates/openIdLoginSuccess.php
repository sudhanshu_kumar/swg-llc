<?php use_helper('Form');?>
<?php use_helper('DateForm');?>
<?php include_javascripts('jquery-1.2.6.js') ?>
<?php include_javascripts('jquery-ui.min.js') ?>
<div class="global_content4 clearfix">
  <div class="brdBox">
  <?php echo ePortal_pagehead('OpenID Login'); ?>
  <br/>
    <div class="clearfix"></div>
    <table style="width: 100%;">
      <tbody>
        <tr>
        <!--<td><?php //echo link_to(image_tag('google.png'), 'authModule/openIdDetails?openid_identifier=google','title = Login with google openId'); ?></td>
        <td><?php //echo link_to(image_tag('yahoo.png'), 'authModule/openIdDetails?openid_identifier=yahoo','title = Login with yahoo openId'); ?></td>
        <td> <a href="#" id="aol"> <?php //echo image_tag('aol.png'); ?></a></td>
        <td> <a href="#" id="aol"> <?php //echo image_tag('myopenid.png'); ?></a></td> -->
        <td><?php echo link_to(image_tag('myopenid.png'), 'authModule/openIdDetails?openid_identifier=pay4me','title = Login with SW Global LLC openId'); ?></td>
        <!--<td><?php //echo link_to(image_tag('myspace.png'), 'authModule/openIdDetails?openid_identifier=myspace','title = Login with myspace openId'); ?></td>
        <td><?php //echo link_to(image_tag('myopenid.png'), 'authModule/openIdDetails?openid_identifier=openid','title = Login with myOpenId'); ?></td> -->
        </tr>
      </tbody>
    </table>
    <br/><br/>
    <?php echo form_tag(url_for('authModule/openIdDetails?openid_identifier=aol'),array('name'=>'openid','id'=>'openid', 'onSubmit'=>'return validateSignForm("1")')); ?>
    <div id="divAol" style="display:none">
    <input type="hidden" name="openid_identifier" value="aol">
    <table><tr>
    <?php echo formRowComplete('Username<span class="red">*</span>',input_tag('srcname', $sf_params->get('srcname'), 'class=txt-input maxlength=30'),'','srcname','err_name','username_row'); ?>
    <td><input type="submit" onclick="" value="Submit" class="button"></td>
    </tr></table>
    </div>
  </div>
</div>

<script type="text/javascript">
  $("#aol").click(function()
    {
      $('#divAol').show();
    });

  function validateSignForm(submit_form)
  {
        var err  = 0;
        if($('#srcname').val() == "")
        {
            $('#err_name').html("Please enter Username");
            err = err+1;
        }
        else
        {
            $('#err_name').html("");
        }
        if(err == 0)
        {
            if(submit_form == 1)
            {
                return true;
            }
        }else{
            return false;
        }

  }
</script>
