<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

function pager_navigation($pager, $uri)
{
  $navigation = '';

  if ($pager->haveToPaginate())
  {
    $uri .= (preg_match('/\?/', $uri) ? '&' : '?').'page=';

    // First and previous page
    if ($pager->getPage() != 1)
    {
      $navigation .= link_to(image_tag('/sf/sf_admin/images/first.png', 'align=absmiddle'), $uri.'1');
      $navigation .= link_to(image_tag('/sf/sf_admin/images/previous.png', 'align=absmiddle'), $uri.$pager->getPreviousPage()).' ';
    }

    // Pages one by one
    $links = array();
    foreach ($pager->getLinks() as $page)
    {
      $links[] = link_to_unless($page == $pager->getPage(), $page, $uri.$page);
    }
    $navigation .= join('  ', $links);

    // Next and last page
    if ($pager->getPage() != $pager->getLastPage())
    {
      $navigation .= ' '.link_to(image_tag('/sf/sf_admin/images/next.png', 'align=absmiddle'), $uri.$pager->getNextPage());
      $navigation .= link_to(image_tag('/sf/sf_admin/images/last.png', 'align=absmiddle'), $uri.$pager->getLastPage());
    }

  }

  return $navigation;
}

function ajax_pager_navigation($pager, $uri, $divId)
{
  $navigation = '';

  if ($pager->haveToPaginate())
  {
    $uri .= (preg_match('/\?/', $uri) ? '&' : '?').'page=';

    // First and previous page
    if ($pager->getPage() != 1)
    {
      $FirstUri = $uri . '1' ;
      $PreviousUri = $uri.$pager->getPreviousPage() ;
      $navigation .=  "<span style='cursor:pointer;color:blue;' onclick=\"ajax_paginator('$divId', '$FirstUri')\" ><img src='../../sf/sf_admin/images/first.png' /></span>" ;
      $navigation .=  "<span style='cursor:pointer;color:blue;' onclick=\"ajax_paginator('$divId', '$PreviousUri')\" ><img src='../../sf/sf_admin/images/previous.png' /></span>" ;
    }

    // Pages one by one
    $links = array();
    $currPage = $pager->getPage() ;
    foreach ($pager->getLinks() as $page)
    {
      $styleUnderscore = "" ;
      ($currPage == $page)? "" : $styleUnderscore = "text-decoration:underline" ; // For style underscore property
      $pagenoUri = $uri.$page ;
      $links[] =  "<span style='cursor:pointer;color:blue;$styleUnderscore' onclick=\"ajax_paginator('$divId', '$pagenoUri')\" >$page</span>" ;

     // link_to_unless($page == $pager->getPage(), $page, "#", array('onclick' => "ajax_paginator('$divId', '$pagenoUri')"));


     // $links[] = link_to_unless($page == $pager->getPage(), $page, $uri.$page);
    }


    $navigation .= join('  ', $links);

    // Next and last page
    if ($pager->getPage() != $pager->getLastPage())
    {
      $lastUri = $uri.$pager->getLastPage() ;
      $nextUri = $uri.$pager->getNextPage() ;
      //$navigation .= ' '.link_to(image_tag('/sf/sf_admin/images/next.png', 'align=absmiddle'), $uri.$pager->getNextPage());
      $navigation .= "<span style='cursor:pointer;color:blue;' onclick=\"ajax_paginator('$divId', '$nextUri')\" ><img src='../../sf/sf_admin/images/next.png' /></span>" ;
     // $navigation .= link_to(image_tag('/sf/sf_admin/images/last.png', 'align=absmiddle'), $uri.$pager->getLastPage());
      $navigation .= "<span style='cursor:pointer;color:blue;' onclick=\"ajax_paginator('$divId', '$lastUri')\" ><img src='../../sf/sf_admin/images/last.png' /></span>" ;
    }

  }

  return $navigation;
}
