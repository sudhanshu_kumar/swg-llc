<?php

class PaymentGatewayHelper {

    public static function isMaxIPaddreslimitExcedded($ipAddress, $maxNumReq, $timeInMinutes) {

        $TotalnumReq = Doctrine::getTable('FpsDetail')->getRequestbetTimeDiff($ipAddress, $timeInMinutes);
        // check max count
        if ($TotalnumReq && $TotalnumReq >= $maxNumReq)
            return true;
        else
            return false;
    }

    public static function isSameCardlimitExcedded($card_Num, $maxNumReq, $timeInMinutes) {
        // get request obj
        $TotalnumReq = Doctrine::getTable('FpsDetail')->getNoOfCardUsed($card_Num, $timeInMinutes);

        // check max count
        if (($TotalnumReq >= 0) && ($TotalnumReq >= $maxNumReq))
            return true;
        else
            return false;
    }

    public static function isSameEmailWithCardlimitExcedded($card_Num, $maxNumReq, $timeInMinutes) {

        // get request obj
        $fpsDetail = Doctrine::getTable('FpsDetail')->getNoEmailUsed($card_Num, $timeInMinutes);

        foreach ($fpsDetail as $fpsRecord) {
            $emailArr[] = $fpsRecord['email'];
        }

        if (isset($emailArr)) {
            $distinctEmailArr = array_unique($emailArr);
            $TotalnumReq = count($distinctEmailArr);
        } else {
            $TotalnumReq = 0;
        }
        // get Card first number array  get Card first number array
        // get count of both array
        // check max count

        if ($TotalnumReq && $TotalnumReq >= $maxNumReq)
            return true;
        else
            return false;
    }

    public static function isCardBlackListed($card_Num, $ruleId) {
        $TotalnumRec = Doctrine::getTable('FpsDetail')->getBlackListCard($card_Num, $ruleId);
        if ($TotalnumRec > 0)
            return true;
        else
            return false;
    }

    public static function generateTrackingNumber($trackingId) {
        return "T" . rand(10000, 1000000) . $trackingId;
    }

    public static function generateWtTrackingNumber($trackingId) {
        return "W" . rand(10000, 1000000) . $trackingId;
    }
    
    /**
     * [WP: 086] => CR:125
     * @param <type> $cardFirstSixDigit will contains credit card first 6 digit...
     * @return <type> true if country match else false...
     */
    public static function isCreditCardCountryNotExist($cardFirstSixDigit, $orderNumber='') {

        $cardCountryObj = Doctrine::getTable('CardCountryList')->findByBin($cardFirstSixDigit);
        if(count($cardCountryObj) > 0){
            $cardCountry = $cardCountryObj[0]['country'];

            ## Getting processing country...
            $paymentOptionsObj = new paymentOptions();
            $applicantProcessingCountry = $paymentOptionsObj->getProcessingCountry();

            if(strtolower($applicantProcessingCountry) == strtolower($cardCountry)){
                ## return false means Passed by BIN rule...
                return false;
            }else{
                /**
                 * [WP: 095] => CR: 135
                 * Checking processing country and bin country exists in a group or not and return true or false...
                 * return true means both country belong to single group...
                 * return false means both country do not belong to single group...
                 */
                $groupFlag = PaymentGatewayHelper::checkCountryInGroupList($applicantProcessingCountry, $cardCountry);
                if($groupFlag){
                    ## return false means Passed by BIN rule...
                    return false;
                }else{
                    ## return true means Stopped by BIN rule...
                    return true;
                }
            }
        }else{            
            PaymentGatewayHelper::insertCreditCardNumberInDb($cardFirstSixDigit, $orderNumber);
            ## return false means Passed by BIN rule...
            return false;
        }
    }

    /**
     * [WP: 086] => CR:125
     * @param <type> $cardFirstSixDigit will contains credit card first 6 digit...     * 
     */
    public static function insertCreditCardNumberInDb($cardFirstSixDigit, $orderNumber=''){
            $cardNotInCountryList = new CardNotInCountryList();
            /*
             * OrderNumber will be blank in case of Marine Gateway i.e, 1 OR 8
             */
            if($orderNumber != ''){
                $cardNotInCountryList->setOrderNumber($orderNumber);
            }
            $cardNotInCountryList->setBin($cardFirstSixDigit);
            $cardNotInCountryList->save();
            $lastBinId = $cardNotInCountryList->getId();
            sfContext::getInstance()->getUser()->setAttribute('lastBinId', $lastBinId);
    }

    /**
     * [WP: 086] => CR:125
     * @param <type> $cardFirstSixDigit
     * @param <type> $orderNumber
     * Updated cardNotInCountryList table only on Marine/Graypay i.e, 1 OR 8
     */

    public static function updateCreditCardNumberInDb($orderNumber=0){
        $lastBinId = sfContext::getInstance()->getUser()->getAttribute('lastBinId');
        $cardNotInCountryObj = Doctrine::getTable('CardNotInCountryList')->find($lastBinId);        
        if($cardNotInCountryObj != ''){
            $cardNotInCountryObj->setOrderNumber($orderNumber);
            $cardNotInCountryObj->save();
        }
    }

    /**
     * [WP: 095] => CR: 135
     * @param <type> $applicantProcessingCountry
     * @param <type> $cardCountry
     * If both country group will match then return true else false...
     */

    public static function checkCountryInGroupList($applicantProcessingCountry, $cardCountry){
        
        ## Finding processing country group id...
        $processingCountryGroup = 0;
        $processingCountryObj = Doctrine::getTable('CardCountryGroupList')->findByCountry($applicantProcessingCountry);
        if(count($processingCountryObj) > 0){
            $processingCountryGroup = $processingCountryObj->getFirst()->getGroupId();
        }

        ## Finding card country group id...
        $cardCountryGroup = 0;
        $cardCountryObj = Doctrine::getTable('CardCountryGroupList')->findByCountry($cardCountry);
        if(count($cardCountryObj) > 0){
            $cardCountryGroup = $cardCountryObj->getFirst()->getGroupId();
        }

        if($processingCountryGroup != 0 && $cardCountryGroup != 0){
            if($processingCountryGroup == $cardCountryGroup){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }

    }//End of private function checkCountryInGroupList($applicantProcessingCountry, $cardCountry){...

    
}
?>
