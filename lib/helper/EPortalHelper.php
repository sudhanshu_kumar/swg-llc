<?php

function format_amount($amount, $currency="",$convert_to_cents=0, $currencySymbol='$'){
  if($convert_to_cents) {$amount=$amount/100;}
    if($currency){
       #sfLoader::loadHelpers('Asset');
       //sfContext::getInstance()->getConfiguration()->loadHelpers(array('Asset'));
       #sfLoader::loadHelpers('Tag');
       //sfContext::getInstance()->getConfiguration()->loadHelpers(array('Tag'));
       //$filepath_naira = image_tag('../img/naira.gif',array('alt'=>'Naira'));
        return html_entity_decode($currencySymbol.number_format($amount, 2, '.', ''));
    }
    return number_format($amount, 2, '.', '');

}

function convertToCents($dollarAmount) {
    return number_format(($dollarAmount*100), 0, '.', '');
  }


function format_address ($address) {
  $address_array = explode( "~", $address);
  return $address = $address_array['0']."<br />".$address_array['2']."<br />".$address_array['3']."<br />";
      /*[0] => address1
    [1] => address2
    [2] => town
    [3] => state
    [4] => zip
    [5] => IN*/

}


function cutText($string, $setlength) {
    $length = $setlength;
    if($length<strlen($string)){
        while (($string{$length} != " ") AND ($length > 0)) {
            $length--;
        }
        if ($length == 0) return substr($string, 0, $setlength);
        else return substr($string, 0, $length)."...";
    }else return $string;
}

function formatButton($button){
    $html = "<div class='lblButton'>";
    $html .= $button;
    $html .= "</div>
                 <div class='lblButtonRight'>
             <div class='btnRtCorner'></div>";
    return $html;
}


function formRowComplete($key,$val,$addtext='',$label='',$err='',$dlid=''){
 // $html =  "<dl id=$dlid><div class=\"dsTitle4Fields\"><label for=$label>$key</label></div><div class=\"dsInfo4Fields\">{$val}<br />$addtext<br /><div id=$err class=\"cRed\"></div></div></dl>";

  $html = "<tr id='$dlid'><td width=20%' class='ltgray'>$key</td><td width='41%' >$val";

  if($addtext != '')
     $html .="<br />$addtext";

  if($err != '')
    $html .="<br /><div id=$err class=\"red\"></div>";

  $html .="</td>";

  return $html;
}


function formRowCompleteNoCSS($key,$val,$addtext='',$label='',$err='',$dlid=''){
 // $html =  "<dl id=$dlid><div class=\"dsTitle4Fields\"><label for=$label>$key</label></div><div class=\"dsInfo4Fields\">{$val}<br />$addtext<br /><div id=$err class=\"cRed\"></div></div></dl>";

  $html = "<tr id='$dlid'><td >$key</td><td >$val";

  if($addtext != '')
     $html .="<br />$addtext";

  if($err != '')
    $html .="<br /><div id=$err class=\"red\"></div>";

  $html .="</td>";

  return $html;
}


function ePortal_legend($content,array $attr= array(), $empty =false, $class=false) {
  $html ='';
  $attributes ='';
  if(!$class)
    $class= 'formHead';
  if(empty($content) && !$empty) {return;}
  if(!empty($attr)){
    foreach($attr as $k=>$v){
      if($k=='class'){$class .=' '.$v ;continue;}
      $attributes .="{$k}='{$v}' ";
    }
  }
  $attributes .='class=" '.$class.'"';
  $html .= "<legend {$attributes}> {$content}</legend>";
  return $html;
}

 function getNoOfX($length)
    {
        $text = "";
        for($i=0;$i<$length;$i++){
            $text.="x";
        }
        return $text;
    }

function ePortal_pagehead($content) {
  $html ='';
    
   $html .= "<p class='page_header'>{$content}</p><div class='clear'></div>";
  
  $sfU =sfContext::getInstance()->getUser();
  if ($sfU->hasFlash('notice')){
    $html.='<div id="flash_notice" class="alertBox" ><span>';
    $html .= nl2br($sfU->getFlash('notice'));
    $html .='</span></div>';
  }
  if ($sfU->hasFlash('error')){
    $html .= '<div id="flash_error" class="alertBox">
          <span>'.nl2br($sfU->getFlash('error')).
          '</span>
        </div>';
  }

  return $html;
}
/**
 * Display hiligheted blocks
 * @param <type> $heading
 * @param <type> $content
 * @param <type> $attr
 * @param <type> $empty
 * @return <type>
 */
function ePortal_highlight($content, $heading='', array $attr= array(), $empty=false) {
  $html ='';
  $attributes ='';
  $class= 'highlight';
  if(empty($content) && $empty) {return;}
  if(!empty($attr)){
    foreach($attr as $k=>$v){
      if($k=='class'){$class .=' '.$v ;continue;}
      $attributes .="{$k}='{$v}' ";
    }
  }
  $attributes .='class="'.$class.'"' ;
  $html .= "<div {$attributes}>";
  $html .= (!empty($heading))?"<h3>{$heading}</h3>":'' ;
  $html .= "<p>{$content}</p></div>";

  return $html;
}
function ePortal_displayName($title="", $fName="",$mName="",$lName=""){

  return ucwords (strtolower($title." ".$lName." ".$fName." ".$mName));
}

function ePortal_popup($title,$content,$attr= array()) {
  $width = '400px';
  $height = '';
  if(isset($attr['width']) && !empty($attr['width']) ){ $width = $attr['width'];}
  if(isset($attr['height']) && !empty($attr['height']) ){ $height = $attr['height'];}
  $popBlock = <<<EOF
<div class='popWrapper'><div id='mainDiv' class='parentDisable'><table border='0' id='popup' width='{$width}' height='{$height}'><tr><td><div style='padding:10px;'><h2>{$title}</h2><p>{$content}</p><p><center id='printHidebuttonID'><a id='popPrint' href='#'><b>[Print]</b></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a id='popClose' href='#'  onClick='return hidePop()'><b>[Close]</b></a></center></p></div></td></tr></table></div></div>
EOF;
  $html = <<<EOF
<style>

.pageWrapper {z-index:0}
.popWrapper {position:absolute;z-index:999;left:0px;top:0px;}
#mainDiv {display:none;}
.parentDisable {
z-index:99;
width:100%;
height:100%;
display:block;
position:fixed;
top:0px;
left:0px;
background-color: #ccc;
color: #aaa;
opacity: .98;
filter: alpha(opacity=98);
}
#popup {
width:{$width};
height:{$height};
position:relative;
margin:0px auto;
margin-top:50px;
color: #000;
background-color: #fff;
}
.parentDisable { /*\*/_position: absolute; _top: expression(((ignoreMe = document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop)) + 'px'); _right: expression((20 + (ignoreMe2 = document.documentElement.scrollLeft ? document.documentElement.scrollLeft : document.body.scrollLeft)) + 'px');/**/}




</style>
<script>
showPop = false ;
function pop() {
showPop =true ;
$('#mainDiv').css('display','block');
$('.pageWrapper select').hide();
  return false
}

function pop2() {
showPop =true ;
$('#mainDiv').css('display','block');
$('#popClose').css('display','none');
$('.pageWrapper select').hide();
  return false;
}

function hidePop() {
$('.pageWrapper select').show();
  $('#mainDiv').css('display','none');
  return false
}
$('body').prepend("{$popBlock}");
/*$('document').ready(function(){
  $('body').prepend("{$popBlock}");
 if(showPop){ pop();}
});*/
wHeight =$(document).height() ;
wWidth = $(document).width() ;

$('.popWrapper #mainDiv').css('height',wHeight).css('width',wWidth);

$(window).resize(function(){
wHeight =$(document).height() ;
wWidth = $(document).width() ;
//alert(wHeight+':'+wWidth);
$('.popWrapper #mainDiv').css('height',wHeight).css('width',wWidth);
});
</script>
EOF;

  return $html;
}

//Function for making http url to https url
//its,useful when you want to make forcefully your url with ssl(like http://example.com to https://example.com)
function secure_url_for($internal_uri) {
  if(!sfConfig::get('app_use_https_for_payment')) {
    return url_for($internal_uri);
  }
  $context = sfContext::getInstance();
  $request = $context->getRequest();
  $host = $request->getHost();
  $uri = url_for($internal_uri);
  $surl = sprintf('%s://%s%s', 'https', $host, $uri);
  return $surl;
}

function ePortal_PaymentSuccess_popup($content,$attr= array()) {
  $width = '400px';
  $height = '';
  if(isset($attr['width']) && !empty($attr['width']) ){ $width = $attr['width'];}
  if(isset($attr['height']) && !empty($attr['height']) ){ $height = $attr['height'];}

//  $popBlock = "<div class='popWrapper'><div id='mainDiv' class='parentDisable'>";
//  $popBlock .= "<table border='0' id='popup' width='{$width}' height='{$height}'>";
//  $popBlock .= "<tr><td valign='top'><div style='padding:10px;'><h2>{$title}</h2><p>{$content}</p><p>";
//  $popBlock .= "<a href='javascript:void(0);' onClick='cancelTransaction();'><b>[Cancel Transaction]</b></a></p></div></td></tr>";
//  $popBlock .= "<tr><td  valign='top'><div id='clockwrapper'><div id='clock1'></div></div></td></tr>";
//  $popBlock .= "</table></div></div>";

    $popBlock = '<div class="popWrapper"><div id="mainDiv" class="parentDisable">';
    $popBlock .= '<table border="1" id="popup" width="'.$width.'" height="'.$height.'">';
    $popBlock .= '<tr><td valign="top">'.$content.'</td></tr>';
    $popBlock .= '<tr><td><div id="clockwrapper"><div id="clock1"></div></div></td></tr>';
    $popBlock .= '</table></div></div>';

  $html = <<<EOF
<style>

.pageWrapper {z-index:0}
.popWrapper {position:absolute;z-index:999;left:0px;top:0px;}
#mainDiv {display:none;}
.parentDisable {
z-index:99;
width:100%;
height:100%;
display:block;
position:fixed;
top:0px;
left:0px;
background-color: #ccc;
color: #aaa;
opacity: .98;
filter: alpha(opacity=98);
}
#popup {
width:{$width};
height:{$height};
position:relative;
margin:0px auto;
margin-top:50px;
color: #000;
background-color: #fff;
}
.parentDisable { /*\*/_position: absolute; _top: expression(((ignoreMe = document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop)) + 'px'); _right: expression((20 + (ignoreMe2 = document.documentElement.scrollLeft ? document.documentElement.scrollLeft : document.body.scrollLeft)) + 'px');/**/}




</style>
<script>
showPop = false ;
function pop() {
showPop =true ;
$('#mainDiv').css('display','block');
$('.pageWrapper select').hide();
  return false
}

function pop2() {
showPop =true ;
$('#mainDiv').css('display','block');
$('#popClose').css('display','none');
$('.pageWrapper select').hide();
  return false;
}

function hidePop() {
$('.pageWrapper select').show();
  $('#mainDiv').css('display','none');
  return false
}
$('body').prepend('{$popBlock}');
/*$('document').ready(function(){
  $('body').prepend('{$popBlock}');
 if(showPop){ pop();}
});*/
wHeight =$(document).height() ;
wWidth = $(document).width() ;

$('.popWrapper #mainDiv').css('height',wHeight).css('width',wWidth);

$(window).resize(function(){
wHeight =$(document).height() ;
wWidth = $(document).width() ;
//alert(wHeight+':'+wWidth);
$('.popWrapper #mainDiv').css('height',wHeight).css('width',wWidth);
});
</script>
EOF;

  return $html;
}

?>
