<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class PosHelper{

    public static function getPaymentStatusThroughPos ($application_id, $from_date, $to_date, $appAmount){

        try{

      /*
      $header[] = "Content-Type: application/soap+xml; charset=utf-8";
      $url =  sfConfig::get('app_pos_form_request_url');
      $requestXml = self::generatePosFormUrlXml($application_id, $from_date, $to_date);

      ## Creating Form URL Response Log...
      self::createLogData($requestXml, 'PosFormUrlRequestXml', $parentLogFolder);

      $curl = curl_init();
      curl_setopt($curl, CURLOPT_URL, $url);
      curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
      curl_setopt($curl, CURLOPT_POST, 1);
      curl_setopt($curl, CURLOPT_POSTFIELDS, $requestXml);
      curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);

      $responseXml = curl_exec($curl);

      ## Creating Form URL Response Log...
      self::createLogData($responseXml, 'PosFormUrlResponseXml', $parentLogFolder);
      $respObj = simplexml_load_string($responseXml);
       *
       */

            $test_application_id = sfConfig::get('app_pos_test_application_id');

            $application_id = (trim($test_application_id) != '')?$test_application_id:$application_id;
            $parentLogFolder = 'posLogs';


            $url =  sfConfig::get('app_pos_form_request_url');
            $username = sfConfig::get('app_pos_query_username');
            $password = sfConfig::get('app_pos_query_password');

            $client = new SoapClient($url.'?WSDL');
            $res = $client->TransactionStatus(array('userName' => $username, 'password' => $password, 'applicationID' => $application_id, 'fromDate' => '', 'toDate' => ''));

            $xmlHeader = '<?xml version="1.0" encoding="UTF-8"?><string xmlns="https://online.valucardnigeria.com/ ">';
            $xmlFooter = '</string>';

            $pos1 = stripos($res->TransactionStatusResult, '<?xml');
            if ($pos1 === false) {
                $responseXml = $xmlHeader.$res->TransactionStatusResult.$xmlFooter;
            }else{
                $responseXml = $res->TransactionStatusResult;
            }

            ## Creating Form URL Response Log...
            self::createLogData($responseXml, $application_id.'_PosFormUrlResponseXml', $parentLogFolder);

            $respObj = @simplexml_load_string($responseXml);

            $respArr = array();
            if(!isset($responseXml) || ''==$responseXml || !$respObj || ''===$respObj){
                $crash = 1;
            }else{
                $respArr = self::getArrayfrmXML($respObj,array());
            }

            /**
             * [WP: 106] => CR: 151
             * Commented below lines and return $respArr array in case of success
             */

            if(in_array('1', $respArr['status'])){
                return $respArr;

                //return true;
            }else{
                return false;
            }

        }catch(Exception $e){
            echo $e->getMessage();
            exit();
        }
    }

    private static function generatePosFormUrlXml($applicationID = '', $fromDate = '', $toDate = ''){

        $username = sfConfig::get('app_pos_query_username');
        $password = sfConfig::get('app_pos_query_password');
        $doc = new DomDocument('1.0', 'utf-8');
        $doc->formatOutput = true;

        $soap_root_element = $doc->createElement("soap12:Envelope");
        $root = $doc->appendChild($soap_root_element);
        $soap_root_element->setAttributeNodeNS(new DOMAttr('xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance'));
        $soap_root_element->setAttributeNodeNS(new DOMAttr('xmlns:xsd', 'http://www.w3.org/2001/XMLSchema'));
        $soap_root_element->setAttributeNodeNS(new DOMAttr('xmlns:soap12', 'http://www.w3.org/2003/05/soap-envelope'));

        $soap_body_root_element = $doc->createElement("soap12:Body");
        $soap_body_root = $root->appendChild($soap_body_root_element);

        $element = $doc->createElement('TransactionStatus');
        $domAttribute = $doc->createAttribute('xmlns');
        // Value for the created attribute
        $domAttribute->value = 'https://online.valucardnigeria.com/';

        // Don't forget to append it to the element
        $element->appendChild($domAttribute);
        $soap_body_root = $soap_body_root->appendChild($element);

        $structElm = $doc->createElement("userName");
        $soap_body_root->appendChild($structElm);
        $structElm->appendChild($doc->createTextNode($username));

        $structElm = $doc->createElement("password");
        $soap_body_root->appendChild($structElm);
        $structElm->appendChild($doc->createTextNode($password));

        $structElm = $doc->createElement("applicationID");
        $soap_body_root->appendChild($structElm);
        $structElm->appendChild($doc->createTextNode($applicationID));

        //    $structElm = $doc->createElement("fromDate");
        //    $soap_body_root->appendChild($structElm);
        //    $structElm->appendChild($doc->createTextNode($fromDate));
        //
        //    $structElm = $doc->createElement("toDate");
        //    $soap_body_root->appendChild($structElm);
        //    $structElm->appendChild($doc->createTextNode($toDate));

        $xmldata = $doc->saveXML();
        return $xmldata;
    }

    private static function createLogData($xmlData,$nameFormate,$parentLogFolder='', $appendTime=true, $append = false){
        $path = self::getLogPath($parentLogFolder);
        if($appendTime){
            $file_name = $path."/".($nameFormate.'-'.date('Y_m_d_H:i:s')).".txt";
        }else{
            $file_name = $path."/".$nameFormate.".txt";
        }
        if($append){
            @file_put_contents($file_name, $xmlData, FILE_APPEND);
        }else{
            $i=1;
            while(file_exists($file_name)) {
                $file_name = $path."/".($nameFormate.'-'.date('Y_m_d_H:i:s-')).$i.".txt";
                $i++;
            }
            @file_put_contents($file_name, $xmlData);
        }
    }

    private static function getLogPath($parentLogFolder){
        $logPath =  sfConfig::get('sf_log_dir').'/'.$parentLogFolder.'/'.date('Y-m-d');

        if(is_dir($logPath)==''){
            $dir_path=$logPath."/";
            mkdir($dir_path,0777,true);
            chmod($dir_path, 0777);
        }
        return $logPath;
    }

    private static function getArrayfrmXML($xml,$arr){
        $iter = 0;
        $amount = '';
        foreach($xml->children() as $b){
            foreach($b->DATA as $c){
                $a = $b->getName();
                if(!$b->children()){
                    $arr[$a] = trim($b[0]);
                }else{
                    $arr[$a][$iter] = array();
                    if(isset($b->DATA[$iter]->RESPCODE) && $b->DATA[$iter]->RESPCODE !=''){
                        $arr[$a][$iter] = (string) $b->DATA[$iter]->RESPCODE;
                        if($b->DATA[$iter]->RESPCODE == '1'){
                            $amount = $b->DATA[$iter]->AMOUNT;
                        }
                    }else{
                        $arr[$a][$iter] = (string) $b->DATA[$iter]->ERRORCODE;
                    }
                    $iter++;
                }
            }
        }

        return array('status' => $arr[$xml->children()->getName()],'amount' => $amount);
    }
}
?>