<?php
use_helper('Form');
/**
 * Project Ordered List of menu.
 * @package    Menu Render Formating
 * @path       /lib/Widget/MenuHelpers.php
 */

class menuHelper {
    public function setMenuSession(){
        $sessionMenu=sfContext::getInstance()->getUser()->getAttribute('menu');
        if(!isset($sessionMenu) || ''!=$sessionMenu){
            $MenuPluginObj =new menuManager();
            $menuArr = $MenuPluginObj->getUserMenu();

            $sessionMenu=sfContext::getInstance()->getUser()->setAttribute('menu', $menuArr);
        }
    }

    public function renderMenuYAML($elm, $homeMenuFlag = true){
        $permissions = $_SESSION['symfony/user/sfUser/credentials']; //sfContext::getInstance()->getUser()->getAllPermissionNames();
            echo "<ul>";
        if(count($elm)){
            foreach($elm as $node)
            {
                //print_r($permissions);echo"<hr>";continue;
                $perm = @$node['perm'];
                if(!in_array($perm,$permissions) && !empty($perm)) continue;
                $url = @$node['url'];
                
                if(sfContext::getInstance()->getUser()->getGuardUser()->hasGroup("payment_group") && $homeMenuFlag){
                    echo "<li>";
                    echo link_to('Home', url_for('welcome/index'))."</a>";
                    echo "</li>";
                    $homeMenuFlag = false;
                }


                echo "<li>";

                if (isset($node['child'])&& count($node['child'])){
                    echo (!empty($url) && $url !='#')? link_to($node['label'],$url):"<a href='#'>".$node['label']."</a>";
                    //print_r(count($node['child']));
                    $this->renderMenuYAML($node['child'], $homeMenuFlag);
                }
                else{
                    echo (!empty($url))? link_to($node['label'],$url):$node['label']."";
                }
                echo "</li>";                
            }
        }
        echo "</ul>";        
    }#end of function
    public function renderMenuYAMLPerms ($elem) {

        $userPerms = $_SESSION['symfony/user/sfUser/credentials'] ;// sfContext::getInstance()->getUser()->getAllPermissionNames();
        foreach ($elem as $k=>$aElem) {
            //     / echo "$k ";
            $res = $this->isValidMenuItem($aElem, $userPerms);
            if (!$res) {
                unset($elem[$k]);
            } else {
                $elem[$k] = $res;
            }
        }
        return $elem;
    }
    public function isValidMenuItem ($elem, $uperms) {
        if(!array_key_exists('perm', $elem)) {
            $myperm = '';
        } else {
            $myperm = $elem['perm'];
        }
        // check if my permission is there in current user permissions list
        if (!empty($myperm)){
            if (!in_array($myperm, $uperms)) {
                return false;
            }
            else{
                if(array_key_exists('eval', $elem)) {
                    if($elem['eval'] == 'menu/display'){
                        $valid_user = $this->chk_merchant_bank_association($elem['url']);
                        if(!$valid_user){
                            return false;
                        }
                    }
                    else if($elem['eval'] == 'menu/recharge'){
                        $valid_user = $this->chk_payment_association($elem['url']);
                        if(!$valid_user){
                            return false;
                        }
                    }
                    else if($elem['eval'] == 'menu/KycApplied'){
                        $userId = sfContext::getInstance()->getUser()->getAttribute('user_id', null, 'sfGuardSecurityUser');
                        $user_detail = Doctrine::getTable('UserDetail')->findByUserId($userId);
                        $kycStatus = $user_detail->getFirst()->getKycStatus();

                        if(!Settings::isEwalletPinActive() || 2!= $kycStatus )
                           return false;
                    }
                    else if($elem['eval'] == 'menu/KycApproved'){
                        $userId = sfContext::getInstance()->getUser()->getAttribute('user_id', null, 'sfGuardSecurityUser');
                        $user_detail = Doctrine::getTable('UserDetail')->findByUserId($userId);
                        $kycStatus = $user_detail->getFirst()->getKycStatus();

                        if(1!= $kycStatus )
                           return false;
                    }
                }

            }
        }
        // I do have permission for myself at least
        // find out if I have child
        if (!array_key_exists('child', $elem)) {
            // no child - find out if I have url
            if (array_key_exists('url', $elem) && (!empty($elem['url']))) {
                // at least I have url - I am valid entity
                return $elem;;
            }
            // I don't have url
        }
        // I do have children
        if(isset($elem['child'])){
            foreach ($elem['child'] as $k=>$aChild) {
                $retVal = $this->isValidMenuItem($aChild,$uperms);
                if (!$retVal) {
                    // not a valid menu item - remove it
                    unset($elem['child'][$k]);
                } else {
                    $elem['child'][$k] = $retVal;
                }
            }

            // All children validated - see if I have any valid child
            if (count($elem['child'])) {
                // I have valid child(ren)
                return $elem;
            }
        }
        // No valid child!!
        return false;
    }

    public function  chk_merchant_bank_association($url) {
        $bank_id = $this->getSfUserBank();
        if($bank_id){
            $url  = explode('=',$url);
            $suburl  = explode('&',$url[1]);
            $merchant_name = $suburl['0'];
            $merchantObj = Doctrine::getTable('Merchant')->findByName($merchant_name);

            return $count = Doctrine::getTable('BankMerchant')->chkAssociation($merchant_name,$bank_id);
        }
        else{
            return 0;
        }


    }


    public function getSfUserBank() {
        $user = null;
        try {
            $user = sfContext::getInstance()->getUser();
        } catch (Exception $exe) {
            return 0;
        }
        if (!$user->isAuthenticated()) {
            return 0;
        }
        return $user->getGuardUser()->getBankUser()->getFirst()->getBankId();
        return true;
    }

    public function  chk_payment_association($url) {
        $bank_id = $this->getSfUserBank();
        if($bank_id){
            // $url  = explode('=',$url);
            // $merchant_name = $url['1'];
            return $count = Doctrine::getTable('ServiceBankConfiguration')->chkAssociation($bank_id);
        }
        else{
            return 0;
        }


    }
}
