<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class sfTrackableListener extends Doctrine_Record_Listener {
  /**
   * Array of timestampable options
   *
   * @var string
   */
  protected $_options = array();

  /**
   * __construct
   *
   * @param string $options
   * @return void
   */
  public function __construct(array $options)
  {
      $this->_options = $options;
  }

  public function preInsert(Doctrine_Event $event) {
    $user = $this->getSfUser();
    if( ! $this->_options['created']['disabled']) {
      $event->getInvoker()->created_by = $user;
    }
    if( ! $this->_options['updated']['disabled']) {
      $event->getInvoker()->updated_by = $user;
    }
  }

  public function preUpdate(Doctrine_Event $event) {
    $event->getInvoker()->updated_by = $this->getSfUser();
  }

  public function preDqlUpdate(Doctrine_Event $event) {
    $params = $event->getParams();
    $table = $event->getInvoker()->getTable();
    if( ! $this->_options['updated']['disabled']) {
      $updatedName = $table->getFieldName('updated_by');
      $field = $params['alias'] . '.' . $updatedName;
      $query = $event->getQuery();

      if ( ! $query->contains($field)) {
          $query->set($field, '?', $this->getSfUser());
      }
    }
    if( ! $this->_options['created']['disabled']) {
      $createdName = $table->getFieldName('created_by');
      $field = $params['alias'] . '.' . $createdName;
      $query = $event->getQuery();

      if ( ! $query->contains($field)) {
          $query->set($field, '?', $this->getSfUser());
      }
    }


  }

  protected function getSfUser() {
    $user = sfContext::getInstance()->getUser();
    if (!$user->isAuthenticated()) {
      return 0;
    }
    return $user->getGuardUser()->getId();

   // return $user->getGuardUser()->getId();
   //return true;
  }
}