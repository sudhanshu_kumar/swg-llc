<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of sessionAuditManager
 *
 * @author
 */
class sessionAuditManager{
    //method to create the query
    public function getDataForReport( $userDetails, $bank="", $branch="", $username="",$logoutTime="",$sessionId="",$ignoreUsers="") {
        //for portal admin get all data
        $getArrayPaging = '';
        if($userDetails['groupName'] == 'portal_admin'){
            $getArrayPaging = Doctrine::getTable('AppSessions')->getSearchData( $bank, $branch, $username,$logoutTime,$sessionId,$ignoreUsers);
        }
        elseif($userDetails['groupName'] == 'bank_admin'){
            //get the bank and id for the user
            $getBankArray = Doctrine::getTable('BankUser')->getBankIdName($userDetails['id']);
            $bankId = $getBankArray[0]['id'];

            $getArrayPaging = Doctrine::getTable('AppSessions')->getSearchData( $bank, $branch, $username,$logoutTime,$sessionId,$ignoreUsers);
        }
        return $getArrayPaging;
    }
    public function doLogoutForcefully($bankId,$bankBranchId=''){           
        $sessObj = new EpDBSessionDetail();
        $logoutTime = $sessObj->getLogoutTimeFromCurrentTimestamp();
        $userList = Doctrine::getTable('sfGuardUser')->getUserList($bankId,$bankBranchId);
        $EpDBSessionDetailObj =  new EpDBSessionDetail();
        $EpDBSessionDetailObj->deleteSessionData($userList);
    }
}
?>
