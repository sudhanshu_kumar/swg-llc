<?php


class TransactionChargesTable extends Doctrine_Table
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('TransactionCharges');
    }

    public function getTransactionCharges($gateway_id) {
      $q = $this->createQuery('t')
           ->where('gateway_id = ?',$gateway_id);

           $res =  $q->execute();
           if($res->count()) {
             return $res;
           }
           return 0;
    }
}