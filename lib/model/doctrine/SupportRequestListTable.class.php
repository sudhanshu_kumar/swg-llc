<?php


class SupportRequestListTable extends Doctrine_Table
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('SupportRequestList');
    }

    public function getAllPendingRequest($fname='',$lname='',$email='', $order_number='')
    {
      try{
        $q = $this->createQuery('pp')
        ->select('pp.*,sra.*,sf.id,ud.*')
        ->leftJoin('pp.sfGuardUser sf')
        ->leftJoin('sf.UserDetail ud')
        ->leftJoin('pp.SupportRequestAssignment sra')
        ->where('pp.status =?','Pending');
        if(!empty($fname) && isset($fname)){
          $q->andWhere('ud.first_name=?',$fname);
        }
        if(!empty($lname) && isset($lname)){
          $q->andWhere('ud.last_name=?',$lname);
        }
        if(!empty($email) && isset($email)){
          $q->andWhere('ud.email=?',$email);
        }
        if(!empty($order_number) && isset($order_number)){
          $q->andWhere('pp.order_number=?',$order_number);
        }
        return $q;
      }catch(Exception $e){
        echo("Problem found". $e->getMessage());die;
      }
    }

    public function getRequestDetails($requestId)
    {
      try{
        $q = $this->createQuery('srl')
        ->select('srl.*,src.*,sf.id,ud.*,srad.*')
        ->leftJoin('srl.sfGuardUser sf')
        ->leftJoin('sf.UserDetail ud')
        ->leftJoin('srl.SupportRequestComments src')
        ->leftJoin('srl.SupportRequestApplicationDetails srad')
        ->where('srl.id=?',$requestId)
        ->execute(array(),Doctrine::HYDRATE_ARRAY);
        return $q;

      }catch(Exception $e){
        echo("Problem found". $e->getMessage());die;
      }
    }

    public function updateRequestStatus($requestId,$status)
    {
      try {
        $q = Doctrine_Query::create()
        ->update('SupportRequestList')
        ->set('status', '?', $status)
        ->where("id =?", $requestId)
        ->execute();
        return true;
      }catch(Exception $e){
        echo("Problem found". $e->getMessage());die;
      }
      
    }

        public function searchSupReqList($userId) {
       try {
            $query = $this->createQuery('a')
              ->select('*')
              ->where('a.user_id = ?',$userId)
              ->orderBy('a.created_at desc');
              return $query;
       }catch(Exception $e){
            echo ("Problem Found ".$e.getError());die;
       }

    }
    public function pendingReqReport($prev4DaysDate){
        try {
        $query = $this->createQuery('a')
                ->select('*')
                ->where('a.status = ?','pending')
                ->andWhere('a.created_at < ?',$prev4DaysDate);
//                echo $query->getSqlQuery();die;
                return $query;
        }catch(Exception $e){
            echo ("Problem Found ".$e.getError());die;

        }
     }
    
}