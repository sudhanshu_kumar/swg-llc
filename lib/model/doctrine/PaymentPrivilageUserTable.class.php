<?php


class PaymentPrivilageUserTable extends Doctrine_Table
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('PaymentPrivilageUser');
    }

   /**function getAllActivePaymentUser()
   *@purpose :get all active user from db
   *@param : N/A
   *@return :  list of all active users
   *@author : KSingh
   *@date : 02-05-2011
   */

    public function getAllActivePaymentUser(){
        
        $q = $this->createQuery('pp')
        ->select('pp.*')
        ->where('pp.status =?','active')
        ->execute(array(),Doctrine::HYDRATE_ARRAY);
        
        return $q;
    }

    /**function updateStatus()
   *@purpose :update user status
   *@param : $id,$status
   *@return :  boolean
   *@author : KSingh
   *@date : 02-05-2011
   */
   public function updateStatus($id,$status,$deleted){
       
       $q = Doctrine_Query::create()
        ->update('PaymentPrivilageUser')
        ->set('status',"'$status'")
        ->where('user_id =?',$id);
        $res = $q->execute();
        if($res){
            return true;
        }else{
            return false;
        }
       
   }
    /**function getUserPreviousDetail()
   *@purpose :get user status
   *@param : $id
   *@return :  boolean
   *@author : KSingh
   *@date : 02-05-2011
   */
   public function getUserPreviousDetail($id){

       $q = $this->createQuery('pp')
       ->select('pp.*')
       ->where('pp.user_id =?',$id)
       ->execute(array(),Doctrine::HYDRATE_ARRAY);
       return $q;

   }
   /**
    * [WP: 107] => CR: 152
    * @return <type>
    * This function will return query for all active payment users...
    */
   public function getAllActivePaymentUserQuery($fname='', $lname='', $email='', $userStatus=''){

        $q = $this->createQuery('pp')
        ->select('ud.*,pp.*, u.id')
        ->leftJoin('pp.sfGuardUser u')
        ->leftJoin('u.UserDetail ud');

        if($userStatus != ''){
            $q->andWhere('pp.status =?',$userStatus);
        }

        if(isset($fname) && !empty($fname)){
          $q->andWhere('ud.first_name=?',$fname);
        }
        if(isset($lname) && !empty($lname)){
          $q->andWhere('ud.last_name=?',$lname);
        }
        if(isset($email) && !empty($email)){
          $q->andWhere('ud.email=?',$email);
        }
        if(isset($status) && !empty($status)){
          $q->andWhere('pp.status=?',$status);
        }        
        return $q;
        
    }//End of public function getAllActivePaymentUserQuery($fname='', $lname='', $email='', $userStatus=''){...
    
}