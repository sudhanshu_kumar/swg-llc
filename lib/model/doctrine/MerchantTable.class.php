<?php


class MerchantTable extends Doctrine_Table
{

    public static function getInstance()
    {
        return Doctrine_Core::getTable('Merchant');
    }


    public function isValidMerchant($merchant_code)
    {
        $q =  Doctrine_Query::create()
        ->select('*')
        ->from('Merchant')
        ->where('merchant_code=?',$merchant_code)->count() ;
        return $q ;

    }

    public function getMerchantDetails($merchant_code){
        $q =  Doctrine_Query::create()
        ->select('*')
        ->from('Merchant')
        ->where('merchant_code=?',$merchant_code)->fetchOne()->toArray() ;
        return $q ;
    }
    
    public function getMerchantDetailsById($merchant_id)
    {
        $query = $this->createQuery('a')
        ->where('a.id = ?',$merchant_id);

        $rs = $query->execute(array(), DOCTRINE::HYDRATE_ARRAY);
        if($rs) {
            $record = $rs ;
        }else {
            $record = false ;
        }
        return $record;
    }

}