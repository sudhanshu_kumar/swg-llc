<?php


class EpOisResponseTable extends Doctrine_Table
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('EpOisResponse');
    }

    public function setResponse($ResponseDetail,$requestId,$orderId,$tokenId)
      {
        $obj = new EpOisResponse();
        $obj->setOisReqId($requestId);
        $obj->setOrderId($orderId);
        $obj->setTokenId($tokenId);
        $obj->setResult($ResponseDetail['result']);
        $obj->setResultText($ResponseDetail['result-text']);
        $obj->setTransactionId($ResponseDetail['transaction-id']);
        $obj->setResultCode($ResponseDetail['result-code']);
        $obj->setAuthorizationCode($ResponseDetail['authorization-code']);
        $obj->setAvsResult($ResponseDetail['avs-result']);
        $obj->setActionType($ResponseDetail['action-type']);
        $obj->setAmount($ResponseDetail['amount']);
        $obj->setIpAddress($ResponseDetail['ip-address']);
        $obj->setIndustry($ResponseDetail['industry']);
        $obj->setProcessorId($ResponseDetail['processor-id']);
        $obj->setCurrency($ResponseDetail['currency']);
        $obj->setTaxAmount($ResponseDetail['tax-amount']);
        $obj->setShippingAmount($ResponseDetail['shipping-amount']);
        //$obj->setBilling($ResponseDetail['billing']);
        $obj->setFirstName($ResponseDetail['billing'][0]['first-name']);
        $obj->setLastName($ResponseDetail['billing'][0]['last-name']);
        $obj->setAddress1($ResponseDetail['billing'][0]['address1']);
        if(key_exists('address2', $ResponseDetail['billing'][0]))
            $obj->setAddress2($ResponseDetail['billing'][0]['address2']);
        $obj->setCity($ResponseDetail['billing'][0]['city']);
        if(key_exists('state',$ResponseDetail['billing'][0]))
            $obj->setState($ResponseDetail['billing'][0]['state']);
        if(key_exists('postal', $ResponseDetail['billing'][0]))
            $obj->setPostal($ResponseDetail['billing'][0]['postal']);
        $obj->setCountry($ResponseDetail['billing'][0]['country']);
        $obj->setPhone($ResponseDetail['billing'][0]['phone']);
        $obj->setEmail($ResponseDetail['billing'][0]['email']);
        $obj->setCcNumber($ResponseDetail['billing'][0]['cc-number']);
        $obj->setCcExp($ResponseDetail['billing'][0]['cc-exp']);
        $obj->save();
        return $obj;
      }
}