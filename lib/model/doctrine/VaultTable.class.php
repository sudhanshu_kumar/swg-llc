<?php


class VaultTable extends Doctrine_Table
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('Vault');
    }

    public function getAllVaultOrder($arrRefundOrderNumber){
        try {
            $q = $this->createQuery('s')
            ->select('*')
            //->where('s.payment_status = 0 ')
            ->andWhereNotIn('s.order_number',$arrRefundOrderNumber);

            return  $q->execute()->toArray();
        }catch(Exception $e) {
            echo("Problem found". $e->getMessage());die;
        }
    }

    public function updateVaultOrder($vaultNumber,$orderNumber,$paymentStatus){
        try{
            $q = $this->createQuery('e')
            ->update('Vault')
            ->set('payment_status', '?', $paymentStatus)
            ->where('order_number=?',$orderNumber)
            ->andWhere("vault_num=?",$vaultNumber);
           $res = $q->execute();
           if($res){
                return true;
            }else{
                return false;
            }
        }catch(Exception $e) {
            echo("Problem found". $e->getMessage());die;
        }
    }

    public function getVaultOrder(){
        try {
            $q = $this->createQuery('s')
            ->select('*')
            ->where('payment_status=?','0');
            //->andWhereNotIn('s.order_number',$arrRefundOrderNumber);

            return  $q->execute()->toArray();
        }catch(Exception $e) {
            echo("Problem found". $e->getMessage());die;
        }
    }

    
}