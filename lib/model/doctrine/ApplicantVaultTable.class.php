<?php


class ApplicantVaultTable extends Doctrine_Table
{

  public static function getInstance()
  {
    return Doctrine_Core::getTable('ApplicantVault');
  }

  public function getUserAuthList($email='', $fdate='', $tdate='')
  {
    $query = Doctrine_Query::create()
    ->select("t.id,t.card_holder,t.card_first,t.card_last,t.card_len,sf.id,ud.id,ud.email")
    ->from("ApplicantVault t")
    ->leftJoin('t.sfGuardUser sf')
    ->leftJoin('sf.UserDetail ud')
    ->where("t.status='New'");
    if($email != '')
    {
      $query->andWhere("ud.email='". $email."' or t.email = '".$email."'");
    }
    if($fdate != '')
    $query->andWhere('date(t.created_at) >=?', $fdate);
    if($tdate != '')
    $query->andWhere('date(t.created_at) <=?', $tdate);
    $query->orderBy("t.id desc");
    //      echo "<pre>";print_r($query->execute()->toArray());die;
    return $query;
  }

  public function getFilteredUserAuthList($email='', $fdate='', $tdate='')
  {
    $query1 = Doctrine_Query::create()
    ->SELECT("max(t.id) ")
    ->from("ApplicantVault t");
    if($email != '')
    $query1->andWhere('t.email=?', $email);
    if($fdate != '')
    $query1->andWhere('date(t.created_at) >=?', $fdate);
    if($tdate != '')
    $query1->andWhere('date(t.created_at) <=?', $tdate);
    $query1->groupBy("t.card_hash")
    ->having("GROUP_CONCAT(status) NOT LIKE '%Approved%' AND GROUP_CONCAT(status) NOT LIKE '%Reject%'");
    $query = $query1->execute(array(),Doctrine::HYDRATE_ARRAY);
    $chkVal='';
    $i=0;
    foreach($query as $val)
    {
      if($i==0)
      $chkVal .= $val['max'];
      else
      $chkVal .= ','.$val['max'];
      $i++;
    }

    $chkVal = ($chkVal == '') ? 0 : $chkVal;
    $query = Doctrine_Query::create()
    ->select("t.card_first,t.card_last,t.first_name,t.last_name,t.card_hash,t.card_holder,t.card_len,t.created_at")
    ->from("ApplicantVault t")
    ->where("t.id IN (".$chkVal.")");
    if($email != '')
    $query->andWhere('t.email=?', $email);
    if($fdate != '')
    $query->andWhere('date(t.created_at) >=?', $fdate);
    if($tdate != '')
    $query->andWhere('date(t.created_at) <=?', $tdate);
    $query->orderBy("t.created_at desc");

    //    ->whereIn("t.id","select max(id) from applicant_vault where status = 'New' group by card_hash");
    //    ->groupBy("t.card_hash")
    //    ->orderBy("t.created_at desc");
    //    echo $query->getSqlQuery();

    //select * from applicant_vault where id in (select max(id) from applicant_vault where status = 'New' group by card_hash)


    //    die;
    //          echo "<pre>";print_r($query->execute()->toArray());die;
    return $query;
  }

  public function getCardsFromVault($userId)
  {
    $query = Doctrine_Query::create()
    ->select("t.id,t.card_first,t.card_last,t.card_len,t.first_name,t.last_name,t.expiry_year,t.expiry_month,t.card_holder")
    ->from("ApplicantVault t")
    ->where('t.status =?', 'Approved')
    ->andWhere("t.created_by = ?", $userId)
    ->orderBy("t.id desc")
    ->fetchArray();
    //    echo "<pre>";print_r($query);die;
    return $query;
  }

  public function validateCardInApplicantVault($cardNumber)
  {
    $query = Doctrine_Query::create()
    ->select("count(*)")
    ->from("ApplicantVault t")
    ->where('t.status =?', 'Approved')
    ->andWhere("t.card_hash  = ?", md5($cardNumber))
    ->orderBy("t.id desc")
    ->count();
    //    echo "<pre>";print_r($query);die;
    return $query;
  }


  public function getUserCardDetails($userId = null){
    $query = Doctrine_Query::create()
    ->select("t.id,t.card_holder,t.card_first,t.card_last,t.card_len")
    ->from("ApplicantVault t")
    ->where("t.status='Approved'");
    $query->andWhere("t.created_by=?",$userId);
    return $query;
  }

  public function getDeplicateCardDetails($credirCardNumber = null, $userId=''){
    $query = Doctrine_Query::create()
    ->select("count(t.id)")
    ->from("ApplicantVault t")
    ->where("t.status='Approved'");
    $query->andWhere("t.card_hash='".$credirCardNumber."'");
    if(!empty ($userId))
    {
      $query->andWhere("t.created_by=?", $userId);
    }
    //    $query->andWhere("t.created_by=?", $userId);
    $query = $query->execute(array(),Doctrine::HYDRATE_ARRAY);
    return $query[0]["count"];
  }
  // Function to show credit card list search after getting input of credit card number / credit car first or last four digits
  public function getDetailsByCardNumber($cardFirst,$cardLast,$creditCard,$name, $email='',$user_id = '', $status = ''){

    $query = Doctrine_Query::create()
    ->select("t.*")
    ->from("ApplicantVault t");
    if($cardFirst != ""){

      $query->addWhere("t.card_first LIKE '$cardFirst%'");
    }
    if($cardLast !=""){
      $query->addWhere("t.card_last LIKE '$cardLast%'");
    }
    if($creditCard !=""){
      $query->addWhere("t.card_hash =?",$creditCard);
    }
    if($name !=""){
      $query->addWhere("t.card_holder LIKE '$name%'");
    }    
    if(!empty($email) && trim($user_id) == ''){
        $query->addWhere("t.email = ?",$email);
    }
    if(!empty($email) && !empty($user_id)){
        $query->addWhere("(t.user_id = ? OR t.email = ?)",array($user_id, $email));
    }
    if(!empty($status)){
        $query->addWhere("t.status = ?",$status);
    }
    
    $query->orderBy("t.created_at desc");

    return $query;

  }

  public function getApprovedCardByHash($cardHashCode,$userId='')
  {
    $query = Doctrine_Query::create()
    ->select("*")
    ->from("ApplicantVault t")
    ->where('t.status =?', 'Approved')
    ->andWhere("t.card_hash  = ?", $cardHashCode);
    if($userId !=''){
      $query->andWhere("t.created_by  = ?", $userId);
    }
    $query->orderBy("t.id desc");
    $res = $query->execute(array(),Doctrine::HYDRATE_ARRAY);
    return $res;
  }


/**
 * Function created to get card details by card first , card last and card hash for Approved status
 *
*/
  public function getDetailsByCardHash($cardHash,$cardFirst,$cardLast)
  {
    try
    {
      $query = Doctrine_Query::create()

      ->select("t.*")
      ->from("ApplicantVault t")
      ->where('t.card_first=?',$cardFirst)
      ->andWhere('t.card_last=?',$cardLast)
      ->andWhere('t.card_hash=?',$cardHash)
      ->andWhere('t.status=?','Approved');
      $res = $query->execute(array(),Doctrine::HYDRATE_ARRAY);

      if(count($res) == 0) {
        return 0;
      }
      return $res;
    }
    catch (Exception $e) {
      throw New Exception ($e->getMessage());
    }
  }

  public function getCardsAmexCard($userId)
  {
    $query = Doctrine_Query::create()
    ->select("t.id,t.card_first,t.card_last,t.card_len,t.first_name,t.last_name,t.expiry_year,t.expiry_month,t.card_holder,t.card_type")
    ->from("ApplicantVault t")
    ->where('t.status =?', 'Approved')
    ->andWhere("t.created_by = ?", $userId)
    ->andWhere("t.card_type = ?",'A' )
    ->orderBy("t.id desc")
    ->fetchArray();
    //    echo "<pre>";print_r($query);die;
    return $query;
  }

  public function getFilterCardsFromVault($userId,$paymentModeArray)
  {
    $query = Doctrine_Query::create()
    ->select("t.id,t.card_type,t.card_first,t.card_last,t.card_len,t.first_name,t.last_name,t.expiry_year,t.expiry_month,t.card_holder")
    ->from("ApplicantVault t")
    ->where('t.status =?', 'Approved')
    ->andWhere("t.created_by = ?", $userId)
    ->andWhereIn("t.card_type", $paymentModeArray)
    ->orderBy("t.id desc")
    ->fetchArray();
    //    echo "<pre>";print_r($query);die;
    return $query;
  }

  public function isValid4Registration($cardnumber)
  {
    $query = Doctrine_Query::create()
    ->select("t.id,t.status")
    ->from("ApplicantVault t")
    ->where('t.card_hash =?', md5($cardnumber))
    ->fetchArray();

    $value = "";
    if(count($query))
    {
      $value = $query[0]['status'];
    }
    return $value;
  }
  public function getCardDetailsByFirstAndSecond($cardFirst,$cardLast){
      $result = Doctrine_Query::create()
      ->select('*')
      ->from('ApplicantVault t')
      ->where('t.card_first =?',$cardFirst)
      ->andWhere('t.card_last =?',$cardLast)
      ->andWhere('t.status =?','approved')
      ->orderBy('t.id DESC')
      ->execute(array(),Doctrine::HYDRATE_ARRAY);
      return $result;
//      echo "<pre>";print_r($result);die;
  }

  /**
   * [WP: 102] => CR: 146
   * @param <type> $cardFirst
   * @param <type> $cardLast
   * @param <type> $creditCard
   * @param <type> $name
   * @param <type> $email
   * @return <type> 
   */
  public function getRegisteredCardDetails($cardFirst, $cardLast, $creditCard, $name, $email=''){

    $userId = '';
    
    if ($email != '') {
        //$cardDetailObj = Doctrine::getTable('ApplicantVault')->findByEmail(trim($email));
        

        $userDetailObj = Doctrine::getTable('UserDetail')->getPaymentUserByEmail(trim($email));
        if(count($userDetailObj) < 1){

        }else{
            $userId = $userDetailObj->getFirst()->getUserId();
        }
    }
    $query = $this->getDetailsByCardNumber($cardFirst, $cardLast, $creditCard, $name, $email, $userId, 'Approved');
    return $query;
  }

      
}
