<?php


class EpGrayPayCaptureDataTable extends PluginEpGrayPayCaptureDataTable
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('EpGrayPayCaptureData');
    }

    public function update($captureDataId, $status) {

        if(!empty($captureDataId)){
             $q = Doctrine_Query::create()
                ->update('EpGrayPayCaptureData')
                ->set('is_captured','?',$status)
                ->Where('id = ?',$captureDataId);
             $rows = $q->execute();
        }
    }
}