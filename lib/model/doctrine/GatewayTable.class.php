<?php


class GatewayTable extends Doctrine_Table
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('Gateway');
    }

    public function getGatewayMerchantDetails($gateway_id) {
      $q = $this->createQuery('g')
      ->leftJoin('g.Merchant m')
      ->where('g.id =?',$gateway_id);

      return $q->execute();
    }
     public function findGateway() {
      $q = $this->createQuery('g')
      ->select('g.name')
      ->where('g.merchant_id !=?',0);

      return $q->execute(array(), DOCTRINE::HYDRATE_ARRAY);
    }

      public function getAllGatewayDetails() {
      $q = $this->createQuery('g');
      $q->select('g.id,g.name');
      return $q->execute()->toArray();
    }

    public function getDisplayName($id)
    {
        $q = Doctrine_Query::create()
         ->select('g.display_name')
         ->from('Gateway g')
         ->where('g.id = ?',$id)
         ->execute(array(), DOCTRINE::HYDRATE_ARRAY);
         
         if(!empty($q))
         {
             return $q[0]['display_name'];
         }
         else
         {
             return false;
         }
    }

    
}