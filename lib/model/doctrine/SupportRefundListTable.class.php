<?php


class SupportRefundListTable extends Doctrine_Table
{

  public static function getInstance()
  {
    return Doctrine_Core::getTable('SupportRefundList');
  }

  public function getAllRecords()
  {
    $q = Doctrine_Query::create()
    ->select('order_number, amount, refund_type, refund_desc, message, is_read')
    ->from('SupportRefundList')
    ->orderBy('created_at');
    return $q;
  }

  public function getAllRecordsByOrderNumber($orderNumber)
  {
    $q = Doctrine_Query::create()
    ->select('order_number, amount, refund_type, refund_desc, message, is_read')
    ->from('SupportRefundList')
    ->where('order_number = ?', $orderNumber)
    ->execute()->toArray();

    $this->markRead($orderNumber);
    return $q;
  }

  function markRead($orderNumber)
  {
    $mark = Doctrine_Query::create()
    ->update('SupportRefundList')
    ->set('is_read', '?' , 'yes')
    ->where('order_number = ?', $orderNumber)
    ->execute();
  }
}