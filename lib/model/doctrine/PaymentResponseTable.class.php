<?php


class PaymentResponseTable extends Doctrine_Table
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('PaymentResponse');
    }

  public function savePaymentResponse($orderNumber, $paymentResponse) {//echo "->>>>>>>>>".$paymentResponse['response_reason_text'];die;
    $response = new PaymentResponse();
    $response->setRequestOrderNumber($orderNumber);
    $response->setResponseCode($paymentResponse['response_code']);
    $response->setResponseReason($paymentResponse['response_reason']);
    $response->setResponseReasonTxt($paymentResponse['response_reason_text']);
    $response->setResponseReasonSubCode($paymentResponse['response_subcode']);
    $response->setResponseTransactionCode($paymentResponse['transaction_id']);
    $response->setResponseAuthorizationCode($paymentResponse['authorization_code']);
    $response->setResponseTransactionDate(date("Y-m-d H:m:s"));
    $response->save();
    return true ;
  }
}