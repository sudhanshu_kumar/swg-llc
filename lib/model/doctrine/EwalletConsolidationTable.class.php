<?php


class EwalletConsolidationTable extends Doctrine_Table {

  public static function getInstance() {
    return Doctrine_Core::getTable('EwalletConsolidation');
  }

  public function fetchDetails($user_id, $gatewayId,$serviceCharge) {
    $q = $this->createQuery('w')
        ->select('w.*')
        ->where('w.user_id  = ?', $user_id)
        ->andWhere('w.gateway_id  = ?', $gatewayId)
        ->andWhere('w.service_charge  = ?', $serviceCharge);

    $set = $q->execute();
    if ($set->count() == 0) return 0;
    return $set;
  }

  public function getCollectionAccounts($user_id) {
    $q = $this->createQuery('w')
        ->select('w.*')
        ->where('user_id=?',$user_id)
        ->orderBy('wallet_amount DESC');

    return $q->execute();
  }

  public function getPayorAcct($amount_paid,$user_id) {
    $q = $this->createQuery('w')
        ->select('w.*')
        ->where('user_id=?',$user_id)
        ->andWhere('wallet_amount >='.$amount_paid)
        ->orderBy('wallet_amount DESC')
        ->LIMIT(1);
    //$q->getQuery();
    $result = $q->execute();
    if($result->count() == 0) {
      return 0;
    }
    return $result;
  }

  public function updatePayment($service_charge, $gateway_id, $amount, $user_id) {

    $q = $this->createQuery('e')
        ->update('EwalletConsolidation')
        ->set('wallet_amount','wallet_amount - '."'".$amount."'" )
        ->set('actual_amount','actual_amount - '."'".$amount."'" )
        ->where('service_charge=?',$service_charge)
        ->andWhere('gateway_id=?',$gateway_id)
        ->andWhere("user_id=?",$user_id);
    //print $q->getSqlQuery();
    $q->execute();
  }
}