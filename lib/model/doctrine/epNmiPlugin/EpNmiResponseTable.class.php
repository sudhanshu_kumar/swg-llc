<?php


class EpNmiResponseTable extends PluginEpNmiResponseTable
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('EpNmiResponse');
    }

    public function setResponse($ResponseDetail,$requestId,$orderId,$tokenId)
      {
        $obj = new EpNmiResponse();
        $obj->setNmiReqId($requestId);
        $obj->setOrderId($orderId);
        $obj->setTokenId($tokenId);
        $obj->setResult($ResponseDetail['result']);
        $obj->setResultText($ResponseDetail['result-text']);
        $obj->setTransactionId($ResponseDetail['transaction-id']);
        $obj->setResultCode($ResponseDetail['result-code']);
        $obj->setAuthorizationCode($ResponseDetail['authorization-code']);
        $obj->setAvsResult($ResponseDetail['avs-result']);
        $obj->setActionType($ResponseDetail['action-type']);
        $obj->setAmount($ResponseDetail['amount']);
        $obj->setIpAddress($ResponseDetail['ip-address']);
        $obj->setIndustry($ResponseDetail['industry']);
        $obj->setProcessorId($ResponseDetail['processor-id']);
        $obj->setCurrency($ResponseDetail['currency']);
        $obj->setTaxAmount($ResponseDetail['tax-amount']);
        $obj->setShippingAmount($ResponseDetail['shipping-amount']);
        //$obj->setBilling($ResponseDetail['billing']);
        $obj->setFirstName($ResponseDetail['billing'][0]['first-name']);
        $obj->setLastName($ResponseDetail['billing'][0]['last-name']);
        $obj->setAddress1($ResponseDetail['billing'][0]['address1']);
        if(key_exists('address2', $ResponseDetail['billing'][0]))
        $obj->setAddress2($ResponseDetail['billing'][0]['address2']);
        $obj->setCity($ResponseDetail['billing'][0]['city']);
        if(key_exists('state',$ResponseDetail['billing'][0]))
        $obj->setState($ResponseDetail['billing'][0]['state']);
        if(key_exists('postal', $ResponseDetail['billing'][0]))
        $obj->setPostal($ResponseDetail['billing'][0]['postal']);
        $obj->setCountry($ResponseDetail['billing'][0]['country']);
        $obj->setPhone($ResponseDetail['billing'][0]['phone']);
        $obj->setEmail($ResponseDetail['billing'][0]['email']);
        $obj->setCcNumber($ResponseDetail['billing'][0]['cc-number']);
        $obj->setCcExp($ResponseDetail['billing'][0]['cc-exp']);
        $obj->save();
        return $obj;
      }

      public function getTransactionId($orderid)
      {
        $orview = $this->createQuery('per')
        ->select('per.transaction_id')
        ->Where('per.order_id= ?',$orderid)
        ->execute(array(), Doctrine::HYDRATE_ARRAY);

        if(count($orview) > 0)
        {

          return $orview[0]['transaction_id'];
        }

        return false;
      }

      public function setCapture($order_id, $capture)
      {
        try {
          $q = Doctrine_Query::create()
          ->update('EpNmiResponse')
          ->set('action_type ', '?', $capture)
          ->where("order_id  =?", $order_id)
          ->execute();
          return true;
        }catch(Exception $e){
          echo("Problem found". $e->getMessage());die;
        }
      }
}