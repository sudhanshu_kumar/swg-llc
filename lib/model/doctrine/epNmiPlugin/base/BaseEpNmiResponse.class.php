<?php

/**
 * BaseEpNmiResponse
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $nmi_req_id
 * @property string $token_id
 * @property string $order_id
 * @property integer $result
 * @property string $result_text
 * @property bigint $transaction_id
 * @property string $result_code
 * @property double $authorization_code
 * @property string $avs_result
 * @property string $action_type
 * @property float $amount
 * @property string $ip_address
 * @property string $industry
 * @property string $city
 * @property double $processor_id
 * @property string $currency
 * @property string $tax_amount
 * @property double $shipping_amount
 * @property string $billing
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $phone
 * @property string $address1
 * @property string $address2
 * @property string $town
 * @property string $postal
 * @property string $country
 * @property string $state
 * @property string $cc_number
 * @property timestamp $cc_exp
 * @property EpNmiRequest $EpNmiRequest
 * 
 * @method integer       getNmiReqId()           Returns the current record's "nmi_req_id" value
 * @method string        getTokenId()            Returns the current record's "token_id" value
 * @method string        getOrderId()            Returns the current record's "order_id" value
 * @method integer       getResult()             Returns the current record's "result" value
 * @method string        getResultText()         Returns the current record's "result_text" value
 * @method bigint        getTransactionId()      Returns the current record's "transaction_id" value
 * @method string        getResultCode()         Returns the current record's "result_code" value
 * @method double        getAuthorizationCode()  Returns the current record's "authorization_code" value
 * @method string        getAvsResult()          Returns the current record's "avs_result" value
 * @method string        getActionType()         Returns the current record's "action_type" value
 * @method float         getAmount()             Returns the current record's "amount" value
 * @method string        getIpAddress()          Returns the current record's "ip_address" value
 * @method string        getIndustry()           Returns the current record's "industry" value
 * @method string        getCity()               Returns the current record's "city" value
 * @method double        getProcessorId()        Returns the current record's "processor_id" value
 * @method string        getCurrency()           Returns the current record's "currency" value
 * @method string        getTaxAmount()          Returns the current record's "tax_amount" value
 * @method double        getShippingAmount()     Returns the current record's "shipping_amount" value
 * @method string        getBilling()            Returns the current record's "billing" value
 * @method string        getFirstName()          Returns the current record's "first_name" value
 * @method string        getLastName()           Returns the current record's "last_name" value
 * @method string        getEmail()              Returns the current record's "email" value
 * @method string        getPhone()              Returns the current record's "phone" value
 * @method string        getAddress1()           Returns the current record's "address1" value
 * @method string        getAddress2()           Returns the current record's "address2" value
 * @method string        getTown()               Returns the current record's "town" value
 * @method string        getPostal()             Returns the current record's "postal" value
 * @method string        getCountry()            Returns the current record's "country" value
 * @method string        getState()              Returns the current record's "state" value
 * @method string        getCcNumber()           Returns the current record's "cc_number" value
 * @method timestamp     getCcExp()              Returns the current record's "cc_exp" value
 * @method EpNmiRequest  getEpNmiRequest()       Returns the current record's "EpNmiRequest" value
 * @method EpNmiResponse setNmiReqId()           Sets the current record's "nmi_req_id" value
 * @method EpNmiResponse setTokenId()            Sets the current record's "token_id" value
 * @method EpNmiResponse setOrderId()            Sets the current record's "order_id" value
 * @method EpNmiResponse setResult()             Sets the current record's "result" value
 * @method EpNmiResponse setResultText()         Sets the current record's "result_text" value
 * @method EpNmiResponse setTransactionId()      Sets the current record's "transaction_id" value
 * @method EpNmiResponse setResultCode()         Sets the current record's "result_code" value
 * @method EpNmiResponse setAuthorizationCode()  Sets the current record's "authorization_code" value
 * @method EpNmiResponse setAvsResult()          Sets the current record's "avs_result" value
 * @method EpNmiResponse setActionType()         Sets the current record's "action_type" value
 * @method EpNmiResponse setAmount()             Sets the current record's "amount" value
 * @method EpNmiResponse setIpAddress()          Sets the current record's "ip_address" value
 * @method EpNmiResponse setIndustry()           Sets the current record's "industry" value
 * @method EpNmiResponse setCity()               Sets the current record's "city" value
 * @method EpNmiResponse setProcessorId()        Sets the current record's "processor_id" value
 * @method EpNmiResponse setCurrency()           Sets the current record's "currency" value
 * @method EpNmiResponse setTaxAmount()          Sets the current record's "tax_amount" value
 * @method EpNmiResponse setShippingAmount()     Sets the current record's "shipping_amount" value
 * @method EpNmiResponse setBilling()            Sets the current record's "billing" value
 * @method EpNmiResponse setFirstName()          Sets the current record's "first_name" value
 * @method EpNmiResponse setLastName()           Sets the current record's "last_name" value
 * @method EpNmiResponse setEmail()              Sets the current record's "email" value
 * @method EpNmiResponse setPhone()              Sets the current record's "phone" value
 * @method EpNmiResponse setAddress1()           Sets the current record's "address1" value
 * @method EpNmiResponse setAddress2()           Sets the current record's "address2" value
 * @method EpNmiResponse setTown()               Sets the current record's "town" value
 * @method EpNmiResponse setPostal()             Sets the current record's "postal" value
 * @method EpNmiResponse setCountry()            Sets the current record's "country" value
 * @method EpNmiResponse setState()              Sets the current record's "state" value
 * @method EpNmiResponse setCcNumber()           Sets the current record's "cc_number" value
 * @method EpNmiResponse setCcExp()              Sets the current record's "cc_exp" value
 * @method EpNmiResponse setEpNmiRequest()       Sets the current record's "EpNmiRequest" value
 * 
 * @package    ama
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseEpNmiResponse extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('ep_nmi_response');
        $this->hasColumn('nmi_req_id', 'integer', null, array(
             'type' => 'integer',
             'notnull' => true,
             ));
        $this->hasColumn('token_id', 'string', 50, array(
             'type' => 'string',
             'notnull' => true,
             'length' => 50,
             ));
        $this->hasColumn('order_id', 'string', 100, array(
             'type' => 'string',
             'notnull' => true,
             'length' => 100,
             ));
        $this->hasColumn('result', 'integer', 4, array(
             'type' => 'integer',
             'notnull' => false,
             'length' => 4,
             ));
        $this->hasColumn('result_text', 'string', 20, array(
             'type' => 'string',
             'length' => 20,
             ));
        $this->hasColumn('transaction_id', 'bigint', 11, array(
             'type' => 'bigint',
             'notnull' => false,
             'length' => 11,
             ));
        $this->hasColumn('result_code', 'string', 20, array(
             'type' => 'string',
             'notnull' => false,
             'length' => 20,
             ));
        $this->hasColumn('authorization_code', 'double', null, array(
             'type' => 'double',
             'notnull' => false,
             ));
        $this->hasColumn('avs_result', 'string', 30, array(
             'type' => 'string',
             'notnull' => true,
             'length' => 30,
             ));
        $this->hasColumn('action_type', 'string', 30, array(
             'type' => 'string',
             'notnull' => false,
             'default' => 'sale',
             'length' => 30,
             ));
        $this->hasColumn('amount', 'float', null, array(
             'type' => 'float',
             'notnull' => false,
             ));
        $this->hasColumn('ip_address', 'string', 20, array(
             'type' => 'string',
             'notnull' => false,
             'length' => 20,
             ));
        $this->hasColumn('industry', 'string', 50, array(
             'type' => 'string',
             'notnull' => false,
             'length' => 50,
             ));
        $this->hasColumn('city', 'string', 50, array(
             'type' => 'string',
             'notnull' => false,
             'length' => 50,
             ));
        $this->hasColumn('processor_id', 'double', null, array(
             'type' => 'double',
             'notnull' => false,
             ));
        $this->hasColumn('currency', 'string', 20, array(
             'type' => 'string',
             'notnull' => false,
             'length' => 20,
             ));
        $this->hasColumn('tax_amount', 'string', 50, array(
             'type' => 'string',
             'notnull' => false,
             'length' => 50,
             ));
        $this->hasColumn('shipping_amount', 'double', null, array(
             'type' => 'double',
             'notnull' => false,
             ));
        $this->hasColumn('billing', 'string', 255, array(
             'type' => 'string',
             'notnull' => false,
             'length' => 255,
             ));
        $this->hasColumn('first_name', 'string', 30, array(
             'type' => 'string',
             'notnull' => false,
             'length' => 30,
             ));
        $this->hasColumn('last_name', 'string', 30, array(
             'type' => 'string',
             'notnull' => false,
             'length' => 30,
             ));
        $this->hasColumn('email', 'string', 100, array(
             'type' => 'string',
             'notnull' => false,
             'length' => 100,
             ));
        $this->hasColumn('phone', 'string', 20, array(
             'type' => 'string',
             'notnull' => false,
             'length' => 20,
             ));
        $this->hasColumn('address1', 'string', 255, array(
             'type' => 'string',
             'notnull' => false,
             'length' => 255,
             ));
        $this->hasColumn('address2', 'string', 255, array(
             'type' => 'string',
             'notnull' => false,
             'length' => 255,
             ));
        $this->hasColumn('town', 'string', 255, array(
             'type' => 'string',
             'notnull' => false,
             'length' => 255,
             ));
        $this->hasColumn('postal', 'string', 55, array(
             'type' => 'string',
             'notnull' => false,
             'length' => 55,
             ));
        $this->hasColumn('country', 'string', 55, array(
             'type' => 'string',
             'notnull' => false,
             'length' => 55,
             ));
        $this->hasColumn('state', 'string', 255, array(
             'type' => 'string',
             'notnull' => false,
             'length' => 255,
             ));
        $this->hasColumn('cc_number', 'string', 255, array(
             'type' => 'string',
             'notnull' => false,
             'length' => 255,
             ));
        $this->hasColumn('cc_exp', 'timestamp', null, array(
             'type' => 'timestamp',
             'notnull' => false,
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasOne('EpNmiRequest', array(
             'local' => 'nmi_req_id',
             'foreign' => 'id',
             'onDelete' => 'CASCADE'));

        $timestampable0 = new Doctrine_Template_Timestampable();
        $sftrackable0 = new sfTrackable();
        $this->actAs($timestampable0);
        $this->actAs($sftrackable0);
    }
}