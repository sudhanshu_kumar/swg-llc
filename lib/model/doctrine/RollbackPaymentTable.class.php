<?php


class RollbackPaymentTable extends Doctrine_Table
{

  public static function getInstance()
  {
    return Doctrine_Core::getTable('RollbackPayment');
  }

  public function setRollback($order_number,$comments){
    $response = new RollbackPayment();
    $response->setOrderNumber($order_number);
    //$response->setAction($action);
    $response->setComments($comments);
    $response->save();
    return $response->getIncremented();

  }
  public function getAllRecordsByOrderNumber($orderNumber)
  {
    $q = Doctrine_Query::create()
    ->select('rp.id, rd.action as action, rp.comments as comments, rp.order_number as order_number')
    ->from('RollbackPayment rp')
    ->leftJoin('rp.RollbackPaymentDetails rd')
    ->where('rp.order_number = ?', $orderNumber)
    ->execute()->toArray();
    
    return $q;
  }

  public function getOrderNumber($orderNumber){
    $q = Doctrine_Query::create()
    ->select('rp.*,rpd.*')
    ->from('RollbackPayment rp')
    ->leftJoin('rp.RollbackPaymentDetails rpd')
    ->where('rp.order_number = ?', $orderNumber)
    //->andWhere('rpd.app_id = ?', $appId)
    ->execute()->toArray();

    return $q;
    
  }

  public function getOrderAppNumber($orderNumber,$appId){
    $q = Doctrine_Query::create()
    ->select('rp.*,rpd.*')
    ->from('RollbackPayment rp')
    ->leftJoin('rp.RollbackPaymentDetails rpd')
    ->where('rp.order_number = ?', $orderNumber)
    ->andWhere('rpd.app_id = ?', $appId)
    ->execute()->toArray();

    return $q;

  }

  public function getNisApplicationStatus($appId,$appType,$orderNumber){
    $q = Doctrine_Query::create()
    ->select('rp.*,rpd.*,rpd.action as app_status')
    ->from('RollbackPayment rp')
    ->leftJoin('rp.RollbackPaymentDetails rpd')
    ->where('rp.order_number = ?', $orderNumber)
    ->andWhere('rpd.app_id = ?', $appId)
    ->andWhere("rpd.app_type = '$appType'");
    $q = $q->execute()->toArray();
    if(isset ($q) && is_array($q) && count($q)>0){
      return $q[0]['app_status'];
    }
    return "paid";
  }
}