<?php

/**
 * BaseRequest
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $order_detail_id
 * @property integer $request_order_number
 * @property float $amount
 * @property integer $card_details_first
 * @property integer $card_details_last
 * @property OrderRequestDetails $OrderRequestDetails
 * @property Doctrine_Collection $Response
 * @property Doctrine_Collection $PaymentResponse
 * 
 * @method integer             getOrderDetailId()        Returns the current record's "order_detail_id" value
 * @method integer             getRequestOrderNumber()   Returns the current record's "request_order_number" value
 * @method float               getAmount()               Returns the current record's "amount" value
 * @method integer             getCardDetailsFirst()     Returns the current record's "card_details_first" value
 * @method integer             getCardDetailsLast()      Returns the current record's "card_details_last" value
 * @method OrderRequestDetails getOrderRequestDetails()  Returns the current record's "OrderRequestDetails" value
 * @method Doctrine_Collection getResponse()             Returns the current record's "Response" collection
 * @method Doctrine_Collection getPaymentResponse()      Returns the current record's "PaymentResponse" collection
 * @method Request             setOrderDetailId()        Sets the current record's "order_detail_id" value
 * @method Request             setRequestOrderNumber()   Sets the current record's "request_order_number" value
 * @method Request             setAmount()               Sets the current record's "amount" value
 * @method Request             setCardDetailsFirst()     Sets the current record's "card_details_first" value
 * @method Request             setCardDetailsLast()      Sets the current record's "card_details_last" value
 * @method Request             setOrderRequestDetails()  Sets the current record's "OrderRequestDetails" value
 * @method Request             setResponse()             Sets the current record's "Response" collection
 * @method Request             setPaymentResponse()      Sets the current record's "PaymentResponse" collection
 * 
 * @package    ama
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseRequest extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('request');
        $this->hasColumn('order_detail_id', 'integer', null, array(
             'type' => 'integer',
             'notnull' => true,
             ));
        $this->hasColumn('request_order_number', 'integer', null, array(
             'type' => 'integer',
             'notnull' => true,
             ));
        $this->hasColumn('amount', 'float', null, array(
             'type' => 'float',
             ));
        $this->hasColumn('card_details_first', 'integer', 2, array(
             'type' => 'integer',
             'length' => 2,
             ));
        $this->hasColumn('card_details_last', 'integer', 2, array(
             'type' => 'integer',
             'length' => 2,
             ));


        $this->index('unique_parameter', array(
             'fields' => 
             array(
              0 => 'request_order_number',
             ),
             'type' => 'unique',
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasOne('OrderRequestDetails', array(
             'local' => 'order_detail_id',
             'foreign' => 'id',
             'onDelete' => 'CASCADE'));

        $this->hasMany('Response', array(
             'local' => 'id',
             'foreign' => 'request_id'));

        $this->hasMany('PaymentResponse', array(
             'local' => 'request_order_number',
             'foreign' => 'request_order_number'));

        $timestampable0 = new Doctrine_Template_Timestampable();
        $softdelete0 = new Doctrine_Template_SoftDelete();
        $sftrackable0 = new sfTrackable();
        $this->actAs($timestampable0);
        $this->actAs($softdelete0);
        $this->actAs($sftrackable0);
    }
}