<?php

/**
 * BaseUserTransactionLimit
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $id
 * @property integer $user_id
 * @property integer $cart_capacity
 * @property double $cart_amount_capacity
 * @property integer $number_of_transaction
 * @property integer $transaction_period
 * @property sfGuardUser $sfGuardUser
 * 
 * @method integer              getId()                    Returns the current record's "id" value
 * @method integer              getUserId()                Returns the current record's "user_id" value
 * @method integer              getCartCapacity()          Returns the current record's "cart_capacity" value
 * @method double               getCartAmountCapacity()    Returns the current record's "cart_amount_capacity" value
 * @method integer              getNumberOfTransaction()   Returns the current record's "number_of_transaction" value
 * @method integer              getTransactionPeriod()     Returns the current record's "transaction_period" value
 * @method sfGuardUser          getSfGuardUser()           Returns the current record's "sfGuardUser" value
 * @method UserTransactionLimit setId()                    Sets the current record's "id" value
 * @method UserTransactionLimit setUserId()                Sets the current record's "user_id" value
 * @method UserTransactionLimit setCartCapacity()          Sets the current record's "cart_capacity" value
 * @method UserTransactionLimit setCartAmountCapacity()    Sets the current record's "cart_amount_capacity" value
 * @method UserTransactionLimit setNumberOfTransaction()   Sets the current record's "number_of_transaction" value
 * @method UserTransactionLimit setTransactionPeriod()     Sets the current record's "transaction_period" value
 * @method UserTransactionLimit setSfGuardUser()           Sets the current record's "sfGuardUser" value
 * 
 * @package    ama
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseUserTransactionLimit extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('tbl_user_transaction_limit');
        $this->hasColumn('id', 'integer', null, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => true,
             'autoincrement' => true,
             ));
        $this->hasColumn('user_id', 'integer', 4, array(
             'type' => 'integer',
             'notnull' => true,
             'length' => 4,
             ));
        $this->hasColumn('cart_capacity', 'integer', 10, array(
             'type' => 'integer',
             'notnull' => true,
             'length' => 10,
             ));
        $this->hasColumn('cart_amount_capacity', 'double', null, array(
             'type' => 'double',
             'notnull' => true,
             ));
        $this->hasColumn('number_of_transaction', 'integer', 10, array(
             'type' => 'integer',
             'notnull' => true,
             'length' => 10,
             ));
        $this->hasColumn('transaction_period', 'integer', 10, array(
             'type' => 'integer',
             'notnull' => true,
             'length' => 10,
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasOne('sfGuardUser', array(
             'local' => 'user_id',
             'foreign' => 'id',
             'onDelete' => 'CASCADE'));

        $timestampable0 = new Doctrine_Template_Timestampable();
        $softdelete0 = new Doctrine_Template_SoftDelete();
        $sftrackable0 = new sfTrackable();
        $this->actAs($timestampable0);
        $this->actAs($softdelete0);
        $this->actAs($sftrackable0);
    }
}