<?php

/**
 * BaseCountryBasedPayment
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property string $country_code
 * @property string $service
 * 
 * @method string              getCountryCode()  Returns the current record's "country_code" value
 * @method string              getService()      Returns the current record's "service" value
 * @method CountryBasedPayment setCountryCode()  Sets the current record's "country_code" value
 * @method CountryBasedPayment setService()      Sets the current record's "service" value
 * 
 * @package    ama
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseCountryBasedPayment extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('country_based_payment');
        $this->hasColumn('country_code', 'string', 20, array(
             'type' => 'string',
             'notnull' => false,
             'length' => 20,
             ));
        $this->hasColumn('service', 'string', 255, array(
             'type' => 'string',
             'notnull' => true,
             'length' => 255,
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $timestampable0 = new Doctrine_Template_Timestampable();
        $softdelete0 = new Doctrine_Template_SoftDelete();
        $sftrackable0 = new sfTrackable();
        $this->actAs($timestampable0);
        $this->actAs($softdelete0);
        $this->actAs($sftrackable0);
    }
}