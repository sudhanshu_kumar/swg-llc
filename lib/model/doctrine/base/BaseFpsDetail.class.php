<?php

/**
 * BaseFpsDetail
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $order_number
 * @property integer $order_request_detail_id
 * @property integer $gateway_id
 * @property string $ip_address
 * @property integer $user_id
 * @property varchar $card_num
 * @property string $email
 * @property timestamp $trans_date
 * @property enum $payment_status
 * @property integer $fps_rule_id
 * @property enum $isverified
 * @property OrderRequestDetails $OrderRequestDetails
 * @property Gateway $Gateway
 * 
 * @method integer             getOrderNumber()             Returns the current record's "order_number" value
 * @method integer             getOrderRequestDetailId()    Returns the current record's "order_request_detail_id" value
 * @method integer             getGatewayId()               Returns the current record's "gateway_id" value
 * @method string              getIpAddress()               Returns the current record's "ip_address" value
 * @method integer             getUserId()                  Returns the current record's "user_id" value
 * @method varchar             getCardNum()                 Returns the current record's "card_num" value
 * @method string              getEmail()                   Returns the current record's "email" value
 * @method timestamp           getTransDate()               Returns the current record's "trans_date" value
 * @method enum                getPaymentStatus()           Returns the current record's "payment_status" value
 * @method integer             getFpsRuleId()               Returns the current record's "fps_rule_id" value
 * @method enum                getIsverified()              Returns the current record's "isverified" value
 * @method OrderRequestDetails getOrderRequestDetails()     Returns the current record's "OrderRequestDetails" value
 * @method Gateway             getGateway()                 Returns the current record's "Gateway" value
 * @method FpsDetail           setOrderNumber()             Sets the current record's "order_number" value
 * @method FpsDetail           setOrderRequestDetailId()    Sets the current record's "order_request_detail_id" value
 * @method FpsDetail           setGatewayId()               Sets the current record's "gateway_id" value
 * @method FpsDetail           setIpAddress()               Sets the current record's "ip_address" value
 * @method FpsDetail           setUserId()                  Sets the current record's "user_id" value
 * @method FpsDetail           setCardNum()                 Sets the current record's "card_num" value
 * @method FpsDetail           setEmail()                   Sets the current record's "email" value
 * @method FpsDetail           setTransDate()               Sets the current record's "trans_date" value
 * @method FpsDetail           setPaymentStatus()           Sets the current record's "payment_status" value
 * @method FpsDetail           setFpsRuleId()               Sets the current record's "fps_rule_id" value
 * @method FpsDetail           setIsverified()              Sets the current record's "isverified" value
 * @method FpsDetail           setOrderRequestDetails()     Sets the current record's "OrderRequestDetails" value
 * @method FpsDetail           setGateway()                 Sets the current record's "Gateway" value
 * 
 * @package    ama
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseFpsDetail extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('fps_detail');
        $this->hasColumn('order_number', 'integer', null, array(
             'type' => 'integer',
             'notnull' => true,
             ));
        $this->hasColumn('order_request_detail_id', 'integer', null, array(
             'type' => 'integer',
             'notnull' => true,
             ));
        $this->hasColumn('gateway_id', 'integer', 2, array(
             'type' => 'integer',
             'notnull' => true,
             'length' => 2,
             ));
        $this->hasColumn('ip_address', 'string', 20, array(
             'type' => 'string',
             'length' => 20,
             ));
        $this->hasColumn('user_id', 'integer', null, array(
             'type' => 'integer',
             ));
        $this->hasColumn('card_num', 'varchar', 40, array(
             'type' => 'varchar',
             'length' => 40,
             ));
        $this->hasColumn('email', 'string', 50, array(
             'type' => 'string',
             'length' => 50,
             ));
        $this->hasColumn('trans_date', 'timestamp', null, array(
             'type' => 'timestamp',
             'notnull' => false,
             ));
        $this->hasColumn('payment_status', 'enum', null, array(
             'type' => 'enum',
             'values' => 
             array(
              0 => 0,
              1 => 1,
             ),
             'default' => 1,
             ));
        $this->hasColumn('fps_rule_id', 'integer', null, array(
             'type' => 'integer',
             'notnull' => true,
             ));
        $this->hasColumn('isverified', 'enum', null, array(
             'type' => 'enum',
             'values' => 
             array(
              0 => 'no',
              1 => 'yes',
             ),
             'default' => 'no',
             'notnull' => false,
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasOne('OrderRequestDetails', array(
             'local' => 'order_request_detail_id',
             'foreign' => 'id',
             'onDelete' => 'CASCADE'));

        $this->hasOne('Gateway', array(
             'local' => 'gateway_id',
             'foreign' => 'id'));

        $timestampable0 = new Doctrine_Template_Timestampable();
        $softdelete0 = new Doctrine_Template_SoftDelete();
        $sftrackable0 = new sfTrackable();
        $this->actAs($timestampable0);
        $this->actAs($softdelete0);
        $this->actAs($sftrackable0);
    }
}