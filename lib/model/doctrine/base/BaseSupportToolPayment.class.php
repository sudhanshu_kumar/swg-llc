<?php

/**
 * BaseSupportToolPayment
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $id
 * @property integer $order_id
 * @property integer $gateway_id
 * 
 * @method integer            getId()         Returns the current record's "id" value
 * @method integer            getOrderId()    Returns the current record's "order_id" value
 * @method integer            getGatewayId()  Returns the current record's "gateway_id" value
 * @method SupportToolPayment setId()         Sets the current record's "id" value
 * @method SupportToolPayment setOrderId()    Sets the current record's "order_id" value
 * @method SupportToolPayment setGatewayId()  Sets the current record's "gateway_id" value
 * 
 * @package    ama
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseSupportToolPayment extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('support_tool_payment');
        $this->hasColumn('id', 'integer', null, array(
             'type' => 'integer',
             'notnull' => true,
             ));
        $this->hasColumn('order_id', 'integer', null, array(
             'type' => 'integer',
             'notnull' => true,
             ));
        $this->hasColumn('gateway_id', 'integer', 2, array(
             'type' => 'integer',
             'notnull' => true,
             'length' => 2,
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $timestampable0 = new Doctrine_Template_Timestampable();
        $sftrackable0 = new sfTrackable();
        $this->actAs($timestampable0);
        $this->actAs($sftrackable0);
    }
}