<?php

/**
 * BaseCurrencyValue
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property float $value
 * @property timestamp $currency_date
 * @property enum $conversion_type
 * @property string $description
 * 
 * @method float         getValue()           Returns the current record's "value" value
 * @method timestamp     getCurrencyDate()    Returns the current record's "currency_date" value
 * @method enum          getConversionType()  Returns the current record's "conversion_type" value
 * @method string        getDescription()     Returns the current record's "description" value
 * @method CurrencyValue setValue()           Sets the current record's "value" value
 * @method CurrencyValue setCurrencyDate()    Sets the current record's "currency_date" value
 * @method CurrencyValue setConversionType()  Sets the current record's "conversion_type" value
 * @method CurrencyValue setDescription()     Sets the current record's "description" value
 * 
 * @package    ama
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseCurrencyValue extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('currency_value');
        $this->hasColumn('value', 'float', null, array(
             'type' => 'float',
             'notnull' => true,
             ));
        $this->hasColumn('currency_date', 'timestamp', null, array(
             'type' => 'timestamp',
             'notnull' => true,
             ));
        $this->hasColumn('conversion_type', 'enum', null, array(
             'type' => 'enum',
             'values' => 
             array(
              0 => 'naira_to_dollor',
              1 => 'dollor_to_naira',
             ),
             'default' => 'dollor_to_naira',
             'notnull' => true,
             ));
        $this->hasColumn('description', 'string', 128, array(
             'type' => 'string',
             'notnull' => false,
             'length' => 128,
             ));
    }

    public function setUp()
    {
        parent::setUp();
        
    }
}