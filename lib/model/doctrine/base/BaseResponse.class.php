<?php

/**
 * BaseResponse
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $request_id
 * @property integer $response_code
 * @property string $response_reason
 * @property string $response_reason_txt
 * @property integer $response_reason_sub_code
 * @property integer $response_transaction_code
 * @property string $response_authorization_code
 * @property timestamp $response_transaction_date
 * @property Request $Request
 * 
 * @method integer   getRequestId()                   Returns the current record's "request_id" value
 * @method integer   getResponseCode()                Returns the current record's "response_code" value
 * @method string    getResponseReason()              Returns the current record's "response_reason" value
 * @method string    getResponseReasonTxt()           Returns the current record's "response_reason_txt" value
 * @method integer   getResponseReasonSubCode()       Returns the current record's "response_reason_sub_code" value
 * @method integer   getResponseTransactionCode()     Returns the current record's "response_transaction_code" value
 * @method string    getResponseAuthorizationCode()   Returns the current record's "response_authorization_code" value
 * @method timestamp getResponseTransactionDate()     Returns the current record's "response_transaction_date" value
 * @method Request   getRequest()                     Returns the current record's "Request" value
 * @method Response  setRequestId()                   Sets the current record's "request_id" value
 * @method Response  setResponseCode()                Sets the current record's "response_code" value
 * @method Response  setResponseReason()              Sets the current record's "response_reason" value
 * @method Response  setResponseReasonTxt()           Sets the current record's "response_reason_txt" value
 * @method Response  setResponseReasonSubCode()       Sets the current record's "response_reason_sub_code" value
 * @method Response  setResponseTransactionCode()     Sets the current record's "response_transaction_code" value
 * @method Response  setResponseAuthorizationCode()   Sets the current record's "response_authorization_code" value
 * @method Response  setResponseTransactionDate()     Sets the current record's "response_transaction_date" value
 * @method Response  setRequest()                     Sets the current record's "Request" value
 * 
 * @package    ama
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseResponse extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('response');
        $this->hasColumn('request_id', 'integer', null, array(
             'type' => 'integer',
             'notnull' => true,
             ));
        $this->hasColumn('response_code', 'integer', 1, array(
             'type' => 'integer',
             'notnull' => true,
             'length' => 1,
             ));
        $this->hasColumn('response_reason', 'string', 10, array(
             'type' => 'string',
             'length' => 10,
             ));
        $this->hasColumn('response_reason_txt', 'string', 255, array(
             'type' => 'string',
             'length' => 255,
             ));
        $this->hasColumn('response_reason_sub_code', 'integer', 1, array(
             'type' => 'integer',
             'length' => 1,
             ));
        $this->hasColumn('response_transaction_code', 'integer', null, array(
             'type' => 'integer',
             ));
        $this->hasColumn('response_authorization_code', 'string', 10, array(
             'type' => 'string',
             'length' => 10,
             ));
        $this->hasColumn('response_transaction_date', 'timestamp', null, array(
             'type' => 'timestamp',
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasOne('Request', array(
             'local' => 'request_id',
             'foreign' => 'id',
             'onDelete' => 'CASCADE'));

        $timestampable0 = new Doctrine_Template_Timestampable();
        $softdelete0 = new Doctrine_Template_SoftDelete();
        $sftrackable0 = new sfTrackable();
        $this->actAs($timestampable0);
        $this->actAs($softdelete0);
        $this->actAs($sftrackable0);
    }
}