<?php

/**
 * BaseVault
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $gateway_id
 * @property integer $vault_num
 * @property integer $order_number
 * @property enum $payment_status
 * 
 * @method integer getGatewayId()      Returns the current record's "gateway_id" value
 * @method integer getVaultNum()       Returns the current record's "vault_num" value
 * @method integer getOrderNumber()    Returns the current record's "order_number" value
 * @method enum    getPaymentStatus()  Returns the current record's "payment_status" value
 * @method Vault   setGatewayId()      Sets the current record's "gateway_id" value
 * @method Vault   setVaultNum()       Sets the current record's "vault_num" value
 * @method Vault   setOrderNumber()    Sets the current record's "order_number" value
 * @method Vault   setPaymentStatus()  Sets the current record's "payment_status" value
 * 
 * @package    ama
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseVault extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('vault');
        $this->hasColumn('gateway_id', 'integer', null, array(
             'type' => 'integer',
             'notnull' => true,
             'default' => 1,
             ));
        $this->hasColumn('vault_num', 'integer', null, array(
             'type' => 'integer',
             'notnull' => true,
             ));
        $this->hasColumn('order_number', 'integer', null, array(
             'type' => 'integer',
             'notnull' => true,
             ));
        $this->hasColumn('payment_status', 'enum', null, array(
             'type' => 'enum',
             'values' => 
             array(
              0 => 0,
              1 => 1,
              2 => 2,
              3 => 3,
             ),
             'default' => 0,
             ));
    }

    public function setUp()
    {
        parent::setUp();
        
    }
}