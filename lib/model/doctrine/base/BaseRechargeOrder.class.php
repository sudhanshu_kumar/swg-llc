<?php

/**
 * BaseRechargeOrder
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $user_id
 * @property integer $gateway_id
 * @property integer $account_no
 * @property float $amount
 * @property float $item_fee
 * @property float $service_charge
 * @property enum $payment_status
 * @property timestamp $paid_date
 * @property sfGuardUser $sfGuardUser
 * @property Gateway $Gateway
 * @property Doctrine_Collection $ipay4meRechargeOrder
 * 
 * @method integer             getUserId()               Returns the current record's "user_id" value
 * @method integer             getGatewayId()            Returns the current record's "gateway_id" value
 * @method integer             getAccountNo()            Returns the current record's "account_no" value
 * @method float               getAmount()               Returns the current record's "amount" value
 * @method float               getItemFee()              Returns the current record's "item_fee" value
 * @method float               getServiceCharge()        Returns the current record's "service_charge" value
 * @method enum                getPaymentStatus()        Returns the current record's "payment_status" value
 * @method timestamp           getPaidDate()             Returns the current record's "paid_date" value
 * @method sfGuardUser         getSfGuardUser()          Returns the current record's "sfGuardUser" value
 * @method Gateway             getGateway()              Returns the current record's "Gateway" value
 * @method Doctrine_Collection getIpay4meRechargeOrder() Returns the current record's "ipay4meRechargeOrder" collection
 * @method RechargeOrder       setUserId()               Sets the current record's "user_id" value
 * @method RechargeOrder       setGatewayId()            Sets the current record's "gateway_id" value
 * @method RechargeOrder       setAccountNo()            Sets the current record's "account_no" value
 * @method RechargeOrder       setAmount()               Sets the current record's "amount" value
 * @method RechargeOrder       setItemFee()              Sets the current record's "item_fee" value
 * @method RechargeOrder       setServiceCharge()        Sets the current record's "service_charge" value
 * @method RechargeOrder       setPaymentStatus()        Sets the current record's "payment_status" value
 * @method RechargeOrder       setPaidDate()             Sets the current record's "paid_date" value
 * @method RechargeOrder       setSfGuardUser()          Sets the current record's "sfGuardUser" value
 * @method RechargeOrder       setGateway()              Sets the current record's "Gateway" value
 * @method RechargeOrder       setIpay4meRechargeOrder() Sets the current record's "ipay4meRechargeOrder" collection
 * 
 * @package    ama
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseRechargeOrder extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('recharge_order');
        $this->hasColumn('user_id', 'integer', 4, array(
             'type' => 'integer',
             'notnull' => true,
             'length' => 4,
             ));
        $this->hasColumn('gateway_id', 'integer', 2, array(
             'type' => 'integer',
             'notnull' => true,
             'length' => 2,
             ));
        $this->hasColumn('account_no', 'integer', 4, array(
             'type' => 'integer',
             'notnull' => true,
             'length' => 4,
             ));
        $this->hasColumn('amount', 'float', null, array(
             'type' => 'float',
             'notnull' => true,
             ));
        $this->hasColumn('item_fee', 'float', null, array(
             'type' => 'float',
             'notnull' => true,
             ));
        $this->hasColumn('service_charge', 'float', null, array(
             'type' => 'float',
             'notnull' => true,
             ));
        $this->hasColumn('payment_status', 'enum', null, array(
             'type' => 'enum',
             'values' => 
             array(
              0 => 0,
              1 => 1,
             ),
             'default' => 1,
             ));
        $this->hasColumn('paid_date', 'timestamp', null, array(
             'type' => 'timestamp',
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasOne('sfGuardUser', array(
             'local' => 'user_id',
             'foreign' => 'id',
             'onDelete' => 'CASCADE'));

        $this->hasOne('Gateway', array(
             'local' => 'gateway_id',
             'foreign' => 'id'));

        $this->hasMany('ipay4meRechargeOrder', array(
             'local' => 'id',
             'foreign' => 'recharge_order_id'));

        $timestampable0 = new Doctrine_Template_Timestampable();
        $softdelete0 = new Doctrine_Template_SoftDelete();
        $sftrackable0 = new sfTrackable();
        $this->actAs($timestampable0);
        $this->actAs($softdelete0);
        $this->actAs($sftrackable0);
    }
}