<?php


class SupportReasonTable extends Doctrine_Table
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('SupportReason');
    }
    
    public function getParentReason()
    {
        $var ='0';
       $q = Doctrine_Query::create()
        ->select('key, 	parent_id')
        ->from('SupportReason')
        ->where('parent_id =?',$var)
        ->orderBy('created_at')
        ->execute(array(), DOCTRINE::HYDRATE_ARRAY);
       
        return $q;
    }

}