<?php


class sfGuardUserGroupTable extends PluginsfGuardUserGroupTable
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('sfGuardUserGroup');
    }
   public function getUserGroupDetails($userId){
     $q = Doctrine_Query::create()
            ->select('ug.user_id,g.name')
            ->from('sfGuardUserGroup ug')
            ->leftJoin("ug.sfGuardGroup g")
            ->where('ug.user_id =?',$userId)
            ->execute(array(),Doctrine::HYDRATE_ARRAY);
    if( count($q)>0){
         return $q[0];
     }
     else
        return false;
  }
}