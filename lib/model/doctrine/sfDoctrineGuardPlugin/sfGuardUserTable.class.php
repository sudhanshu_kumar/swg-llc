<?php


class sfGuardUserTable extends PluginsfGuardUserTable
{

  public static function getInstance()
  {
    return Doctrine_Core::getTable('sfGuardUser');
  }

  public function retrieveByUsername($username, $isActive = true)
  {
    $query = Doctrine::getTable('sfGuardUser')->createQuery('u')
    ->where('u.username = ?', $username)
    ->addWhere('u.is_active = ?', $isActive)
    ;

    return $query->fetchOne();
  }


  public function chk_username($username){
    $manager = Doctrine_Manager::getInstance();
    $callBackValue = $manager->getAttribute(Doctrine::ATTR_USE_DQL_CALLBACKS);
    $manager->setAttribute(Doctrine::ATTR_USE_DQL_CALLBACKS, false);
    $username_count = Doctrine::getTable('sfGuardUser')->createQuery('a')->where('username=?',$username)->count();
    $manager->setAttribute(Doctrine::ATTR_USE_DQL_CALLBACKS, $callBackValue );
    return $username_count ;
  }


  public function getActivationToken($hashAlgo,$userPwdSalt,$updatedAt){
    # Get the timestamp of updatedAt datetime
    list($date, $time) = explode(' ', $updatedAt);
    list($year, $month, $day) = explode('-', $date);
    list($hour, $minute, $second) = explode(':', $time);

    $timestamp = mktime($hour, $minute, $second, $month, $day, $year);

    $strPwdUpdatedAt = $userPwdSalt.$timestamp;

    $token = hash($hashAlgo,$strPwdUpdatedAt);
    return $token;
  }

  public function getAllUsers($userType, $email=""){
    if(strtolower($userType) == 'admin')
      $group_id = 2;
    else if(strtolower($userType) == 'payment')
      $group_id = 3;
    $q = $this->createQuery('u')
    ->select('u.*,ud.first_name as first_name,ud.last_name as last_name,ud.email as email,ud.failed_attempt as failed_attempt,g.*,gg.*')
    ->leftJoin('u.sfGuardUserGroup g')
    ->leftJoin('g.sfGuardGroup gg')
    ->leftJoin('u.UserDetail ud')
    ->andWhere('g.group_id = ?',$group_id);
    if($email!="")
    {   
    $q->andWhere("ud.email LIKE '%".$email."%'");
    }

    //        if($status_option != '' && $uname=='' ){
    //           if($status_option == 1){
    //             $q->andWhere('ud.failed_attempt  >= ?',sfConfig::get('app_number_of_failed_attempts_for_blocked_user'));
    //           }elseif($status_option == 2){
    //             $q->andWhere(' ud.failed_attempt is NULL or ud.failed_attempt  < ?',array(sfConfig::get('app_number_of_failed_attempts_for_blocked_user')));
    //
    //           }
    //        }

        return $q;

  }
  public function getReportUsers($group_id="") {
    $q = $this->createQuery('u')
        ->select('u.*,ud.email')
        ->leftJoin('u.UserDetail ud')
        ->leftJoin('u.sfGuardUserGroup g')
        ->where('g.group_id = ?', $group_id);

      $q->orderBy("u.username");

    return $q;
  }
  //Start Function  by Kuldeep
  public function getAllSupportUser($group_id="") {
    try{
      $q = $this->createQuery('u')
      ->select('u.*,ud.email')
      ->leftJoin('u.UserDetail ud')
      ->leftJoin('u.sfGuardUserGroup g')
      ->where('g.group_id = ?', $group_id)
      ->orderBy("u.username")
      ->execute()
      ->toArray(array(),Doctrine::HYDRATE_ARRAY);

      return $q;
    }catch(Exception $e){
      echo "Problem found :" .$e->getMessage();
    }
  }
  //END Function  by Kuldeep

}