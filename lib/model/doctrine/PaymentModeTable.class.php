<?php


class PaymentModeTable extends Doctrine_Table
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('PaymentMode');
    }

    public function getCardGroup($cardTypeArray){
        $query = Doctrine_Query::create()
        ->select('pm.*,g.*')
        ->from('PaymentMode pm')
        ->leftJoin('pm.Gateway g')
        ->whereIN('pm.card_type',$cardTypeArray)
        ->execute()->toArray();
       return $query;
    }
}