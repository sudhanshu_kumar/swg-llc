<?php


class EpMasterLedgerTable extends PluginEpMasterLedgerTable
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('EpMasterLedger');
    }
}