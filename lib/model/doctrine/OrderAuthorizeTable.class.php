<?php


class OrderAuthorizeTable extends Doctrine_Table
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('OrderAuthorize');
    }
     public function  getReport($gatewayId,$oder_no='',$from_date='',$to_date='',$status=''){
     $phis = $this->createQuery('O')
        ->select('*')       
        ->Where('O.gateway_id = ?',$gatewayId);
       
         if(''!=$oder_no){
            $phis->andWhere('O.order_number = ?',$oder_no);
        }

        if(''!=$from_date){
            $phis->andWhere('O.trans_date  >= ?',$from_date);
        }
        if(''!=$to_date){
            $phis->andWhere('O.trans_date <= ?',$to_date);

        }
        if(''!=$status){
            $phis->andWhere("O.authorize_status = ?",$status);

        }

// $result = $phis->execute(array(),Doctrine::HYDRATE_ARRAY);
// echo "<pre>";print_r($result);die;
//         print $phis->getSqlQuery();die;
//        $phis->execute();exit;
        return $phis;
     }
}