<?php

class SupportRequestApplicationDetailsTable extends Doctrine_Table {

    public static function getInstance() {
        return Doctrine_Core::getTable('SupportRequestApplicationDetails');
    }

    public function getRequestId($requestId) {
        try {
            $query = $this->createQuery('a')
                            ->select('*')
                            ->where('a.request_id = ?', $requestId);
            $assignedArray = $query->execute()->toArray();
         return $assignedArray;
        } catch (Exception $e) {
            echo ("Problem Found " . $e . getError());
            die;
        }
    }

}