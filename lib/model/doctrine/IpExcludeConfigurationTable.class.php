<?php


class IpExcludeConfigurationTable extends Doctrine_Table
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('IpExcludeConfiguration');
    }

    public function getTotalIpRecordQuery(){
        $query = $this->createQuery('ip')
               ->select('*')
               ->orderBy('id DESC');
        return $query;
    }
}