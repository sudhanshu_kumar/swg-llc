<?php


class SupportRequestAssignmentTable extends Doctrine_Table
{

  public static function getInstance()
  {
    return Doctrine_Core::getTable('SupportRequestAssignment');
  }

/**function getAllAssignedRequest()
   *@purpose :get all assigned request from db
   *@param : $userId,$fname,$lname,$email
   *@return :  list of all assign request
   *@author : KSingh
   *@date : 29-08-2011
   */
  public function getAllAssignedRequest($userId,$fname='',$lname='',$email='', $order_number=''){
    $statusArr = array("0"=>"Pending","1"=>"Approved");
    $q = $this->createQuery('sra')
    ->select('sra.*,srl.*,sf.id,ud.*')
    ->leftJoin('sra.SupportRequestList srl')
    ->leftJoin('srl.sfGuardUser sf')
    ->leftJoin('sf.UserDetail ud')
    ->whereNotIn('srl.status',$statusArr)
    ->andWhere('sra.assigned_to =?',$userId)
    ->andWhere('sra.assign_active =?','Yes');
    if(!empty($fname) && isset($fname)){
      $q->andWhere('ud.first_name=?',$fname);
    }
    if(!empty($lname) && isset($lname)){
      $q->andWhere('ud.last_name=?',$lname);
    }
    if(!empty($email) && isset($email)){
      $q->andWhere('ud.email=?',$email);
    }
    if(!empty($order_number) && isset($order_number)){
      $q->andWhere('srl.order_number=?',$order_number);
    }
    

    return $q;

  }
  /*  END  // Functions by Kuldeep*/

  public function getRequestId($requestId)
  {

    $query = $this->createQuery('a')
    ->select('*')
    ->orderBy('a.id DESC')
    ->where('a.request_id = ?', $requestId)
    ->limit(1);

    $requestDetails = $query->execute()->toArray();
    return $requestDetails;
  }

  public function getAssignedSupUser($requestId){

    try {
      $query = $this->createQuery('a')
      ->select('*')
      ->where('a.request_id = ?',$requestId);
      $assignedArray = $query->execute()->toArray();
      if(count($assignedArray)>0)
      return $assignedArray[0]['assigned_to'];
      else
      return 0;

    } catch(Exception $e){
      echo ("Problem Found ".$e.getError());die;
    }

  }

/**function isRequestAlreadyAssigned()
   *@purpose :check if request already assigned
   *@param : $requestId
   *@return :  list of all assign request
   *@author : KSingh
   *@date : 23-09-2011
   */
  public function isRequestAlreadyAssigned($requestId)
  {
    $query = Doctrine_Query::create()
    ->select('sra.*')
    ->from('SupportRequestAssignment sra')
    ->where('sra.request_id=?',$requestId)
    ->andWhere('sra.assign_active=?','Yes')
    ->execute()->toArray();
    return $query;
  }
/**function updatePreviousAssignment()
   *@purpose :update previous assignment
   *@param : $assignedId
   *@return :  update assignmernt
   *@author : KSingh
   *@date : 23-09-2011
   */
  public function updatePreviousAssignment($assignedId)
  {
    $q = Doctrine_Query::create()
    ->update('SupportRequestAssignment')
    ->set('assign_active','?','No')
    ->Where('id = ?',$assignedId);
    $rows = $q->execute();
  }

}