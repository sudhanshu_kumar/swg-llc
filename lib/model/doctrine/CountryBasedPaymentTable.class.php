<?php


class CountryBasedPaymentTable extends Doctrine_Table
{

  public static function getInstance()
  {
    return Doctrine_Core::getTable('CountryBasedPayment');
  }

  public function isCountryHas3dCheck($card_type, $processingCountries)
  {
    $q = Doctrine_Query::create()
    ->select('count(*)')
    ->from('CountryBasedPayment')
    ->where('service =?', $card_type)
    ->andWhereIn('country_code', $processingCountries)
    ->count();
    return $q;
  }
  
  public function getCardType($processingCountries)
  {
    $q = Doctrine_Query::create()
    ->select('*')
    ->from('CountryBasedPayment')
    //->where('service =?', $card_type)
    ->where('country_code=?', $processingCountries)
    ->execute()->toArray();
    return $q;
  }


}