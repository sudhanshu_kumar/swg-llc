<?php


class ServiceChargeTable extends Doctrine_Table
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('ServiceCharge');
    }

    public function getServiceCharge($gateway_id, $type){
        try {
            $q = $this->createQuery('sc')
            ->select('*')
            ->Where('sc.gateway_id= ?',$gateway_id)
            ->andWhere('sc.type = ?', $type);
            $result = $q->fetchArray();
            if(count($result)) {
                return $result[0];
            }
            return 0;

        }catch(Exception $e) {
            echo("Problem found". $e->getMessage());die;
        }
    }
}