<?php


class CountryIpTable extends Doctrine_Table
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('CountryIp');
    }

    function getCountryCode($ip)
    {
        // Make IP numerical
        $ip_num = sprintf("%u", ip2long($ip));
        
        // lookup IP address
        $q = $this->createQuery('c')
        ->select('country_code')
        ->where('begin_ip_num <= ?', $ip_num)
        ->andWhere('end_ip_num >= ?', $ip_num)
        ->execute(array(), Doctrine::HYDRATE_ARRAY);
        if(count($q)>0){
            return $q[0]['country_code'] ;
        }
        return false;
         
    }

}