<?php


class ipay4meOrderTable extends Doctrine_Table {

  public static function getInstance() {
    return Doctrine_Core::getTable('ipay4meOrder');
  }

  public function checkDuplicacy($order_number) {
    try {
      $q = $this->createQuery('s')
      ->select('count(*)')
      ->where('s.order_number = ?',$order_number);

      return  $q->count();
    }catch(Exception $e) {
      echo("Problem found". $e->getMessage());die;
    }
  }


  public function getOrderRequestDetails($order_number, $gateway_id ='') {
    try {
      $q = $this->createQuery('s')
      ->select('*')
      ->where('s.order_number = ?',$order_number);
      if($gateway_id!='')
      $q->andWhere('s.gateway_id = ?',$gateway_id);
      $result = $q->fetchArray();
      if(count($result) > 0)
      return $result[0]['order_request_detail_id'];
      else
      return 0;
    }catch(Exception $e)  {
      echo("Problem found". $e->getMessage());die;
    }
  }

  public function getReceipt($receiptId) {
    $q = $this->createQuery('ipo')
    ->select('*, ORD.*,or.*, mr.*')
    ->leftJoin('ipo.OrderRequestDetails ORD')
    ->leftJoin('ORD.OrderRequest or')
    ->leftJoin('or.Merchant mr')
    ->Where('ipo.order_number= ?',$receiptId)
    ->andWhere('ORD.payment_status = 0')
    ->execute();

    if($q->count()) {
      return $q;
    }
    return 0;
  }

  public function getPurchaseHistory($userId) {
    $phis = $this->createQuery('ph')
    ->select('ph.id, ph.order_number as request_order_number, ORD.amount as amount, ORD.convert_amount as convert_amount, ORD.currency as currency, ORD.description as description,
                    ORD.paid_date as response_transaction_date,
                    mr.name as mname,B.id,B.bill_number,OR.id,ORD.id')
    ->leftJoin('ph.OrderRequestDetails ORD')
    ->leftJoin('ORD.OrderRequest OR')
    ->leftJoin('OR.Bill B')
    ->leftJoin('ORD.Merchant mr')
    ->where('ORD.user_id = ?',$userId)
    ->andWhere('ORD.payment_status = 0')
    ->andWhere('ph.payment_status = "0"')
    ->OrderBy('ORD.paid_date DESC');

    // print $phis->getSqlQuery();
    //$phis->execute();exit;
    return $phis;
  }
  public function getPaymentReport($from_date,$to_date,$oder_no='',$bill_no='') {

    $phis = $this->createQuery('ph')
    ->select('ph.id, ph.order_number as request_order_number,ph.payment_status as payment_status,ph.updated_at as date, ORD.amount as amount, ORD.description as description,
                    ORD.paid_date as response_transaction_date,
                    mr.name as mname,B.id,B.bill_number,OR.id,ORD.id')
    ->leftJoin('ph.OrderRequestDetails ORD')
    ->leftJoin('ORD.OrderRequest OR')
    ->leftJoin('OR.Bill B')
    ->leftJoin('ORD.Merchant mr');
    if(''!=$bill_no){
      $phis ->andWhere('B.bill_number = ?',$bill_no);
    }
    if(''!=$oder_no){
      $phis->andWhere('ph.order_number = ?',$oder_no);
    }

    if(''!=$from_date){
      $phis->andWhere('updated_at  >= ?',$from_date);
    }
    if(''!=$to_date){
      $phis->andWhere('updated_at <= ?',$to_date);

    }
    // $result = $phis->execute(array(),Doctrine::HYDRATE_ARRAY);
    // echo "<pre>";print_r($result);die;
    //         print $phis->getSqlQuery();
    //        $phis->execute();exit;
    return $phis;
  }

  public function findSuccessfulPaymentDetail($orderId) {
    try{
      $q = Doctrine_Query::create()
      ->select('peo.id,peo.order_number as order_number,peo.payor_name as payor_name,peo.card_last as card_last,peo.card_len as card_len,peo.card_type as card_type,peo.gateway_id as gateway_id,peo.created_by as created_by')
      ->from('ipay4meOrder peo')
      ->where('peo.order_request_detail_id = ?',$orderId)
      ->andWhere("peo.payment_status = '0'");


      $result = $q->execute(array(),Doctrine::HYDRATE_ARRAY);


      if(count($result) > 0)
      return $result[0];
      else
      return false;
    }catch(Exception $e) {
      echo("Problem found". $e->getMessage());die;
    }
  }
  public function paymentDetails($orderId)
  {
    try{
      $q = Doctrine_Query::create()
      ->select('pe.id, ord.id as ordid, pe.payment_status as paymentStatus, m.id as mid, m.name as mername, m.homepage_url as merurl, m.abbr as mabbr, sfu.id as uid, sfd.first_name as fname, sfd.last_name as lname')
      ->from('ipay4meOrder pe')
      ->leftJoin('pe.OrderRequestDetails ord')
      ->leftJoin('ord.sfGuardUser sfu')
      ->leftJoin('sfu.UserDetail sfd')
      ->leftJoin('ord.Merchant  m')
      ->where('pe.order_number = ?',$orderId);
      $result = $q->execute(array(),Doctrine::HYDRATE_ARRAY);
      if(count($result) > 0)
      return $result[0];
      else
      return 0;
    }catch(Exception $e) {
      echo("Problem found". $e->getMessage());die;
    }
  }

  public function getNoOfCardUsed($card_Num, $timeInMinutes){
    try{

      $card_first = substr($card_Num, 0, 4);
      $card_last =  substr($card_Num, -4, 4);

      $query= $this->createQuery('ip')
      ->select('ip.id')
      ->where('ip.card_Num = ?',$card_Num)
      ->andWhere('ip.created_at < ?',date('Y-m-d H:i:s'))
      ->andWhere('ip.created_at > ?',date('Y-m-d H:i:s',mktime(date('H'),date('i') - $timeInMinutes, date('s'), date('m'), date('d'), date('Y'))))
      ->execute(array(), Doctrine::HYDRATE_ARRAY);

      return count($query);
    }catch(Exception $e) {
      echo("Problem found". $e->getMessage());die;
    }
  }


  public function getAllGatewayPaymentOrderDetails($gateway_id,$from_date,$to_date) {
    try{

      $q = $this->createQuery('i');

      $q->select('count(*) as count_response, i.response_code,t.id')
      ->leftJoin('i.Gateway t')
      ->where('i.gateway_id =?',$gateway_id)
      ->andWhere('date(i.updated_at)  >= ?',$from_date)
      ->andWhere('date(i.updated_at) <= ?',$to_date)
      ->groupBy('i.response_code');

      //die($q->getSqlQuery());

      return  $q;

    }catch(Exception $e) {
      echo("Problem found". $e->getMessage());die;
    }
  }



  public function getAllGatewayPaymentHistory($gateway_id,$from_date,$to_date,$responseCode) {
    try{

      $q = $this->createQuery('i');

      $q->select('i.response_code,i.order_number,i.card_type,i.updated_at,o.amount,o.user_id,or.transaction_number,s.username')
      ->innerJoin('i.OrderRequestDetails o')
      ->innerJoin('o.OrderRequest or')
      ->innerJoin('o.sfGuardUser  s')
      ->where('i.gateway_id =?',$gateway_id)
      ->andWhere('i.updated_at  >= ?',$from_date)
      ->andWhere('i.updated_at <= ?',$to_date)
      ->andWhere('i.response_code <= ?',$responseCode);
      return  $q;

    }catch(Exception $e) {
      echo("Problem found". $e->getMessage());die;
    }
  }


  public function getOrderDetails($request_order_number) {
    try {
      $q = $this->createQuery('o')
      ->select('o.order_number, d.id, i.transaction_number as item_number, g.name  as gateway, i.amount, u.username')
      ->innerJoin('o.OrderRequestDetails d')
      ->innerJoin('d.OrderRequest i')
      ->innerJoin('o.Gateway g')
      ->innerJoin('d.sfGuardUser u')
      ->where('o.order_number=?',$request_order_number );
      // print $q->getSqlQuery();


      $res = $q->execute();

      if($res->count() == 0) {
        return 0;
      }
      return $res;
    }
    catch (Exception $e) {
      throw New Exception ($e->getMessage());
    }
  }

  public function getBatchRecords($startDate, $endDate, $max_amount, $status) {
    $q = $this->createQuery('i')
    ->select('sum(o.amount) as total_amount, i.order_number')
    ->innerJoin('i.Vault v')
    ->innerJoin('i.OrderRequestDetails o')
    ->where('i.payment_status=?','0')
    ->andWhere('v.payment_status=?','0')
    ->andWhere('i.gateway_id = "1"')
    ->andWhere('i.updated_at  >= ?',$startDate)
    ->andWhere('i.updated_at <= ?',$endDate)
    ->groupBy("date(i.updated_at)");
    //  ->having('sum(o.amount) <= ?',$max_amount);

    //   print $q->getSqlQuery();

    return $q->execute();
  }



  public function processBatchRecords($startDate, $endDate, $status) {
    $q = $this->createQuery('i')
    ->select('i.order_number, v.vault_num, o.amount')
    ->innerJoin('i.Vault v')
    ->innerJoin('i.OrderRequestDetails o')
    ->where('i.payment_status=?','0')
    ->andWhere('v.payment_status=?','0')
    ->andWhere('i.gateway_id = "1"')
    ->andWhere('i.updated_at  >= ?',$startDate)
    ->andWhere('i.updated_at <= ?',$endDate);
    //  ->having('sum(o.amount) <= ?',$max_amount);

    //                print $q->getSqlQuery();

    return $q->execute();
  }

  public function getIpayOrderDetails($request_order_number) {
    try {
      $q = $this->createQuery('o')
      ->select('o.order_number as order_number,o.gateway_id,o.order_request_detail_id,o.created_at as paid_at,o.id, d.id, i.transaction_number as item_number, ud.email as email, o.card_first as card_first, o.card_last as card_last ')
      ->innerJoin('o.OrderRequestDetails d')
      ->innerJoin('d.OrderRequest i')
      ->innerJoin('d.sfGuardUser u')
      ->innerJoin('u.UserDetail ud')
      ->where('o.order_number=?',$request_order_number );
      $res = $q->execute(array(),Doctrine::HYDRATE_ARRAY);

      if(count($res) == 0) {
        return 0;
      }
      return $res;
    }
    catch (Exception $e) {
      throw New Exception ($e->getMessage());
    }
  }
/**
 *
 * @param <type> $request_order_number
 * @return <type>
 * Get Details from the table According to the order number passed
 */
  public function getIpayOrderDetailsChild($request_order_number) {
    try {
      $q = $this->createQuery('o')
      ->select('o.order_number as order_number,o.created_at as paid_at,o.id, d.id, i.transaction_number as item_number, ud.email as email, o.card_first as card_first, o.card_last as card_last, o.card_hash as card_hash ')
      ->leftJoin('o.OrderRequestDetails d')
      ->leftJoin('d.OrderRequest i')
      ->leftJoin('d.sfGuardUser u')
      ->leftJoin('u.UserDetail ud')
      ->where('o.order_number=?',$request_order_number );
      $res = $q->execute(array(),Doctrine::HYDRATE_ARRAY);
      if(count($res) == 0) {
        return 0;
      }
      return $res;
    }
    catch (Exception $e) {
      throw New Exception ($e->getMessage());
    }
  }

  /**
 *
 * @param <type> $request_order_number
 * @return <type>
 * Get Details from the table According to the order number passed
 */
  public function getIpayOrderDetailsChildFPS($request_order_number) {
    try {
      $q = $this->createQuery('o')
      ->select('o.order_number as order_number,o.created_at as paid_at,o.id, d.id, i.transaction_number as item_number, f.email as email, o.card_first as card_first, o.card_last as card_last, o.card_hash as card_hash ')
      ->leftJoin('o.OrderRequestDetails d')
      ->leftJoin('d.FpsDetail f')
      ->leftJoin('d.OrderRequest i')
      ->leftJoin('d.sfGuardUser u')
      ->leftJoin('u.UserDetail ud')
      ->where('o.order_number=?',$request_order_number );
      $res = $q->execute(array(),Doctrine::HYDRATE_ARRAY);
      if(count($res) == 0) {
        return 0;
      }
      return $res;
    }
    catch (Exception $e) {
      throw New Exception ($e->getMessage());
    }
  }

  public function getPurchaseHistoryByEmailAddress($userId) {
   return  $query = Doctrine_Query::create()
    ->select("ord.id,io.order_number as order_no,io.gateway_id as gatewayId,io.response_code as response_code,ord.amount as amount,ord.created_at as trans_date,io.payment_status as pay_status,ord.currency,ord.convert_amount")
    ->from("OrderRequestDetails ord")
    ->innerJoin("ord.ipay4meOrder io")
    ->where("ord.user_id=$userId")
    ->orderBy("io.id desc");
//    ->execute(array(),Doctrine::HYDRATE_ARRAY);
//    echo "<pre>";print_r($query);die;
//            print $query->getSqlQuery();die;
    //$phis->execute();exit;

  }


  public function getSuccefullPayment($gateway_id,$paymentStatus) {
    try {

      $q = $this->createQuery('s')
      ->select('*')
      ->where('s.gateway_id = ?',$gateway_id)
      ->andWhere('s.payment_status = ?',$paymentStatus)
      ->execute(array(), Doctrine::HYDRATE_ARRAY);
      return $q;
    }catch(Exception $e)  {
      echo("Problem found". $e->getMessage());die;
    }
  }


  public function setBlockStatus($orderNumber){
    $q = Doctrine_Query::create()
    ->update('ipay4meOrder')
    ->set('payment_status','?',2)
    ->where('order_number =?',$orderNumber);
    $res = $q->execute();
    if($res){
      return true;
    }else{
      return false;
    }

  }

  public function setChargebackStatus($orderNumber){
    $q = Doctrine_Query::create()
    ->update('ipay4meOrder')
    ->set('payment_status',3)
    ->where('order_number =?',$orderNumber);
    $res = $q->execute();
    if($res){
      return true;
    }else{
      return false;
    }

  }

  public function getGatewayId($orderNumber){

    try {

      $q = $this->createQuery('s')
      ->select('s.gateway_id')
      ->where('s.order_number = ?',$orderNumber)
      ->execute(array(), Doctrine::HYDRATE_ARRAY);
      return $q[0]['gateway_id'];
    }catch(Exception $e)  {
      echo("Problem found". $e->getMessage());die;
    }

  }

  public function getOrderNumberPrice($orderNumber){

    try {

      $q = $this->createQuery('s')
      ->select('s.id,ord.amount as amount,ord.convert_amount as convert_amount,ord.currency as currency')
      ->leftJoin("s.OrderRequestDetails ord")
      ->where('s.order_number = ?',$orderNumber)
      ->execute(array(), Doctrine::HYDRATE_ARRAY);
      return $q;
    }catch(Exception $e)  {
      echo("Problem found". $e->getMessage());die;
    }

  }

  public function getOrderNumberStatus($request_order_number) {
    try {
      $q = $this->createQuery('o')
      ->select('o.order_number,o.created_at, d.id, i.transaction_number as item_number,d.payment_status as status')
      ->innerJoin('o.OrderRequestDetails d')
      ->innerJoin('d.OrderRequest i')
      ->where('o.order_number=?',$request_order_number);
      // print $q->getSqlQuery();


      $res = $q->execute();

      if($res->count() == 0) {
        return 0;
      }
      return $res->toArray();
    }
    catch (Exception $e) {
      throw New Exception ($e->getMessage());
    }
  }

  public function getOrderRequestDetailId($orderNumber){
    try {
      $q = $this->createQuery('s')
      ->select('s.order_request_detail_id')
      ->where('s.order_number = ?',$orderNumber);
      $res = $q->execute(array(), Doctrine::HYDRATE_ARRAY);
      return  $res[0]['order_request_detail_id'] ;
    }catch(Exception $e) {
      echo("Problem found". $e->getMessage());die;
    }
  }

  public function getOrderRequestDetailByOrder($orderNumber){
    try {
      $q = $this->createQuery('s')
      ->select('s.*')
      ->where('s.order_number = ?',$orderNumber);
      $res = $q->execute(array(), Doctrine::HYDRATE_ARRAY);
      return  $res;
    }catch(Exception $e) {
      echo("Problem found". $e->getMessage());die;
    }
  }

  public function getOrderNumberDetailsByCardDetails($firstDigit,$lastDigit){
    if(isset ($firstDigit) && $firstDigit!='' && isset ($lastDigit) && $lastDigit!='')
    {
      return $query = "SELECT ip.order_number as order_no, ip.card_holder as holder_name,ip.card_first,ip.card_last, ip.created_at as trans_date, ord.amount as amount, ip.payment_status as pay_status,
                       ord.`retry_id` as retry_id,ord.currency,ord.convert_amount FROM `ipay4me_order` ip left join `order_request_details` ord on (ord.`id` = ip.`order_request_detail_id`)
                       WHERE `card_first` LIKE '".$firstDigit."%' AND `card_last` = '".$lastDigit."' ORDER BY ip.id DESC ";
    }
  }

  public function getAllDuplicateOrder($userId)
  {
    try{
      $q = Doctrine_Query::create()
      ->select('pe.*,ord.*')
      ->from('ipay4meOrder pe')
      ->leftJoin('pe.OrderRequestDetails ord')
      //->leftJoin('ord.sfGuardUser sfu')
      //->leftJoin('sfu.UserDetail sfd')
      //->leftJoin('ord.Merchant  m')
      ->where('ord.user_id = ?',$userId)
      ->andWhere('pe.payment_status != ?','1');
      $result = $q->execute(array(),Doctrine::HYDRATE_ARRAY);

     if(count($result) > 0) {
      $orderNumberArray = array();

      for($i=0;$i<count($result);$i++){
          $order = $result[$i]['order_number'];
          $nisDetailArray = Doctrine::getTable('CartItemsTransactions')->getOrderDetailsOnNis($order);
//          echo "<pre>";print_r($nisDetailArray);echo "</pre>";

          if($nisDetailArray){
            $paidApplicationArray = array();
            $statusArray = array();
            
            for($j=0;$j<count($nisDetailArray);$j++){
              if($nisDetailArray[$j]['status'] != 'New'){
                  $paidApplicationArray[] = $nisDetailArray[$j];
              }else{
                  $statusArray[] = 'Not Paid';
              }
            }
            if(count($statusArray) != count($nisDetailArray)){
                 $orderNumberArray[] = $result[$i]['order_number'];
            }           
          }
      } 
   if(count($orderNumberArray) > 0)
      return $orderNumberArray;
   else
      return false;
  }else{
      return false;
    }
    }catch(Exception $e) {
      echo("Problem found". $e->getMessage());die;
    }
}

  public function getDailyProcessedAmount($cardType, $thresholdDateTime ,$activeGatewayId){
    try {
      if(!$thresholdDateTime)
      $thresholdDateTime = date("Y-m-d G:i:s") ;
      
      $q = $this->createQuery('o')
      ->select('SUM(d.amount) as total_amount')
      ->innerJoin('o.OrderRequestDetails d')
      ->innerJoin('d.OrderRequest i')
      ->innerJoin('d.sfGuardUser u')
      ->innerJoin('u.UserDetail ud')
      ->where('o.payment_status=?', '0')
      ->andWhere('o.card_type=?', $cardType)
      ->andWhere('o.gateway_id=?', $activeGatewayId)
      ->andWhere('d.paid_date > ?', $thresholdDateTime);
      $res = $q->execute(array(),Doctrine::HYDRATE_ARRAY);
      if(count($res[0]['total_amount']) == 0) {
        return 0;
      }
      return $res[0]['total_amount'];
    }
    catch (Exception $e) {
      throw New Exception ($e->getMessage());
    }
  }


  public function getOrderDetailByApprovalCode($orderId)
  {
    try{
      $q = Doctrine_Query::create()
      ->select('pe.*, ord.id as ordid,ord.paid_date as paid_date,ord.amount as amount,ord.name as name, m.id as mid, m.name as mername, m.description as description, m.abbr as mabbr')
      ->from('ipay4meOrder pe')
      ->leftJoin('pe.OrderRequestDetails ord')
      ->leftJoin('ord.Merchant  m')
      ->where('pe.order_number = ?',$orderId);
      $result = $q->execute(array(),Doctrine::HYDRATE_ARRAY);
      if(count($result) > 0)
      return $result[0];
      else
      return 0;
    }catch(Exception $e) {
      echo("Problem found". $e->getMessage());die;
    }
  }
/**
 *
 * @param <type> $orderDetailId
 * @return <type>
 * Ipay4me order detials corresponding to the order request detail id
 */
  public function getIpayOrderDetailbyId($orderDetailId) {

    if(isset ($orderDetailId) && $orderDetailId!='')
    {
      $query = Doctrine_Query::create()
      ->select("t.*,g.*")
      ->from("iPay4meOrder t")
      ->leftJoin('t.Gateway g')
      ->where("t.order_request_detail_id =?",$orderDetailId);
      return $query;
    }
  }
   /**
   *
   *
   * @param <type> $orderNumber
   * @param <type> $cardFirst
   * @param <type> $cardLast
   * @return <type>
    * Ipay4me order details corresponding to the card first , last and card hash
   */

  public function getSummaryDetails($orderNumber,$cardFirst,$cardLast) {
    try
    {
      $query = Doctrine_Query::create()

      ->select('o.*,g.*,ord.*,fd.*')
      ->from("iPay4meOrder o")
      ->leftJoin('o.Gateway g')
      ->leftJoin('o.OrderRequestDetails ord')
      ->leftJoin('ord.FpsDetail fd')
      ->where('o.order_number=?',$orderNumber)
      ->andWhere('o.card_first=?',$cardFirst)
      ->andWhere('o.card_last=?',$cardLast)
      ->orderBy('o.updated_at DESC');
      $res = $query->execute(array(),Doctrine::HYDRATE_ARRAY);

      if(count($res) == 0) {
        return 0;
      }
      return $res;
    }
    catch (Exception $e) {
      throw New Exception ($e->getMessage());
    }
  }
/**
 *
 * @param <type> $orderNumber
 * @return <type>
 * To get retry id for the order number passsed.
 */

  public function getIpayOrderRetryId($orderNumber){
    if(isset ($orderNumber) && $orderNumber!=''){
      try{
        $q = $this->createQuery('o')
        ->select('o.id,d.id,d.retry_id as retryId')
        ->innerJoin('o.OrderRequestDetails d')
        ->where('o.order_number=?',$orderNumber);
        $res = $q->execute(array(),Doctrine::HYDRATE_ARRAY);
        return $res[0]['retryId'];
      }catch(Exception $e){
        return "Exception when retriving NIS Appliations";
      }
    }
  }
  /**
   *
   * @param <type> $cardNum
   * @return <type>
   * Added for bug id 28992
   */
  public function getIpayOrderDetailbyCardNum($cardNum) {

    if(isset ($cardNum) && $cardNum!='')
    {
      $query = Doctrine_Query::create()
      ->select("t.*")
      ->from("iPay4meOrder t")
      ->where("t.card_hash =?",$cardNum);
      return $query;
    }
  }

  /**
 *
 * @param <type> $orderNumber
 * @return <type>
 * To get retry id for the order number passsed.
 */

  public function updateIP4MOrderStatus($orderNumber){
    try{
      $q = Doctrine_Query::create()
      ->update('ipay4meOrder')
      ->set('payment_status','?',0)
      ->where('order_number =?',$orderNumber);
      $res = $q->execute();
      if($res){
        return true;
      }else{
        return false;
      }
    }catch(Exception $e){
      return "Exception when retriving NIS Appliations";
    }
  }
  public function getGatewayIdByOrderNo($orderNo)
  {
    $q = Doctrine_Query::create()
    ->select('i.gateway_id')
    ->from('ipay4meOrder i')
    ->where('i.order_number =?',$orderNo)
    ->execute(array(),Doctrine::HYDRATE_ARRAY);

    return $q;
  }

  public function getOrderNumbers($orderRequesDetailsId)
  {
    $q = Doctrine_Query::create()
    ->select('i.*')
    ->from('ipay4meOrder i')
    ->where('i.order_request_detail_id =?',$orderRequesDetailsId)
    ->andWhere('i.payment_status = 1')
    ->execute(array(),Doctrine::HYDRATE_ARRAY);
    if(!empty($q))
    {
      return $q;
    }
    else
    {
       return array();
    }
  }

  public function getIpay4meOrderDetails($request_order_number) {
    try {
      $q = $this->createQuery('o')
      ->select('o.order_number as order_number,o.order_request_detail_id,o.created_at as paid_at,o.id, d.id, i.transaction_number as item_number, o.card_first as card_first, o.card_last as card_last ')
      ->innerJoin('o.OrderRequestDetails d')
      ->innerJoin('d.OrderRequest i')
      ->where('o.order_number=?',$request_order_number );
      $res = $q->execute(array(),Doctrine::HYDRATE_ARRAY);

      if(count($res) == 0) {
        return 0;
      }
      return $res;
    }
    catch (Exception $e) {
      throw New Exception ($e->getMessage());
    }
  }

  public function findByUserid($userId)
  {
    try {
      $q = $this->createQuery('o')
      ->select('o.order_number as order_number,o.order_request_detail_id, d.amount as amount')
      ->innerJoin('o.OrderRequestDetails d')
      ->where('o.created_by=?',$userId )
      ->orderBy('o.id desc')
      ->limit(1);
      $res = $q->execute(array(),Doctrine::HYDRATE_ARRAY);

      if(count($res) == 0) {
        return 0;
      }

      return $res;
    }
    catch (Exception $e) {
      throw New Exception ($e->getMessage());
    }
  }

  public function updateIP4WTorderStatus($orderNumber){
    try{
      $q = Doctrine_Query::create()
      ->update('ipay4meOrder')
      ->set('payment_status','?',0)
      ->where('order_number =?',$orderNumber);
      $res = $q->execute();
      if($res){
        return true;
      }else{
        return false;
      }
    }catch(Exception $e){
      return "Exception when retriving NIS Appliations";
    }
  }

  public function getAllOrderIds($requestId)
  {

    $q = Doctrine_Query::create()
    ->select('i.order_number')
    ->from('ipay4meOrder i')
    ->where('i.gateway_id =?',4)
    ->andWhere('i.order_request_detail_id =?',$requestId)
    ->fetchArray();

    return $q;
  }

  public function getOrderNumbersLast($orderRequesDetailsId) {
        $query = Doctrine_Query::create();
        $query->select('i.*');
        $query->from('ipay4meOrder i');
        $query->where('i.order_request_detail_id =?', $orderRequesDetailsId);
        $query->andWhere('i.payment_status = ?','1');
        $query->orderBy('i.id DESC');
        $query->limit(1);

        $resultArray = $query->execute(array(), Doctrine::HYDRATE_ARRAY);

        if (!empty($resultArray)) {
            return $resultArray;
        } else {
            return false;
        }
  }

  public function getAllOrderNumbers($orderRequesDetailsId)
  {
    $q = Doctrine_Query::create()
    ->select('i.*')
    ->from('ipay4meOrder i')
    ->where('i.order_request_detail_id =?',$orderRequesDetailsId)
    ->execute(array(),Doctrine::HYDRATE_ARRAY);
    if(!empty($q))
    {
      return $q;
    }
    else
    {
      return false;
    }
  }


  public function getOrderNumbersForPaymentUpdateCron($startDate, $endDate)
  {
    $q = Doctrine_Query::create()
    ->select('i.*')
    ->from('ipay4meOrder i')
    ->where('i.created_at >= ?', $startDate)
    ->AndWhere('i.created_at <= ?', $endDate)
    ->AndWhere('i.payment_status = ?',1)
    ->AndWhere('i.gateway_id = ?',10) //10 for jna
    ->execute(array(),Doctrine::HYDRATE_ARRAY);
    if(!empty($q))
    {
      return $q;
    }
    else
    {
      return false;
    }
  }

  public function updateJobId($orderNumber, $jobId){
    try{
      $q = Doctrine_Query::create()
      ->update('ipay4meOrder')
      ->set('jobId','?',$jobId)
      ->where('order_number =?',$orderNumber);
      $res = $q->execute();
      if($res){
        return true;
      }else{
        return false;
      }
    }catch(Exception $e){
      return "Exception when updating JobId in Ipay4meOrder table.";
    }
  }

  public function getLastMonthPurchaseHistory($userId,$paidDate) {
    $query = $this->createQuery('ph')
       ->select('ph.id, ph.order_number AS request_order_number, ORD.amount AS amount, ORD.currency, ORD.description AS description,
                    ORD.paid_date AS response_transaction_date')
//    ->select('ph.*, ORD.*')
    ->leftJoin('ph.OrderRequestDetails ORD')
    ->where('ORD.user_id = ?', $userId)
    ->andWhere('ph.payment_status = "0"')
    ->andWhere('date(ORD.paid_date) >=?',$paidDate)
    ->OrderBy('ORD.paid_date DESC');
    //$res = $query->execute(array(),Doctrine::HYDRATE_ARRAY);
    //return $res;

//    echo $query->getSqlQuery();
//    echo "<br/>".$userId."  ".$paidDate;
    return $query;


  }

  public function userUnpaidApplication($userId)
  {
    try {
      $q = $this->createQuery('o')
      ->select('o.*,d.retry_id, d.amount as amount')
      ->leftJoin('o.OrderRequestDetails d')
      ->where('o.created_by=?',$userId)
      ->andWhere('o.payment_status=?','1')
      ->orderBy('o.id desc');
      
      $res = $q->execute(array(),Doctrine::HYDRATE_ARRAY);

      if(count($res) == 0) {
        return 0;
      }

      return $res;
    }
    catch (Exception $e) {
      throw New Exception ($e->getMessage());
    }
  }
  public function getReportByCardNumber($cardFirst,$cardLast)
  {   
      $cardReportArray = array();

      for($i=0;$i<4;$i++){
          $transCount = $this->createQuery('ip')
          ->select('ip.id')
          ->where('ip.card_first =?',$cardFirst)
          ->andWhere('ip.card_last=?',$cardLast)
          ->andWhere('ip.payment_status=?',$i)
          ->execute()->count();
          $cardReportArray[$i] = $transCount;
      }
      ## subtract 30 from days inplace of month...
      ## Ashwani Kumar
      $lastMonthDate = date("Y-m-d",mktime(0,0,0,date('m'),date('d')-30,date('Y')));

      for($i=0;$i<2;$i++){
          $transCount = $this->createQuery('ip')
          ->select('ip.id')
          ->where('ip.card_first =?',$cardFirst)
          ->andWhere('ip.card_last=?',$cardLast)
          ->andWhere('ip.payment_status=?',$i)
          ->andWhere('ip.created_at >?',$lastMonthDate)
          ->execute()->count();
          $cardReportArray[] = $transCount;
      }
      return $cardReportArray;
  }
  public function userPaidApplication($userId, $status=0, $orderBy = 'asc')
  {
    try {
      $q = $this->createQuery('o')
      ->select('o.*,d.retry_id, d.amount as amount')
      ->leftJoin('o.OrderRequestDetails d')
      ->where('o.created_by=?',$userId)
      ->andWhere('o.payment_status = ?',$status)
      ->orderBy("o.id $orderBy");
      $res = $q->execute();
      if(count($res) == 0) {
        return array();
      }
      return $res;
    }
    catch (Exception $e) {
      throw New Exception ($e->getMessage());
    }
  }

  /**
   * [WP:82] => CR:120
   * @param <type> $startDate
   * @param <type> $endDate
   * @param <type> $status
   * To get Transaction status report by given date range.
   */

  public function getTransactionStatusGraphReport($startDate, $endDate, $status=''){
      
     try{
         $q = $this->createQuery('i')
         ->select('*')
         ->where('i.updated_at >=?',$startDate.' 00:00:00')
         ->andWhere('i.updated_at <=?',$endDate.' 23:59:59');
         if($status != ''){ 
             $q->andWhere('i.payment_status =?', $status);
         }
         //die($q->getSqlQuery());
         $res = $q->execute();
         return $res;
     }
     catch(Exception $e){
         throw New Exception($e->getMessage());
     }
  }//End of public function getTransactionStatusGraphReport(...

   /**
    * [WP: 087] => CR: 126
    * @param <type> $orderNumber
    * @return <type>
    * This function return last order request id...
    */
   public function getLatestOrderRequestDetailId($orderNumber){
    try {
      $q = $this->createQuery('s')
      ->select('s.order_request_detail_id')
      ->where('s.order_number = ?',$orderNumber)
      ->orderBy('s.id DESC');
      $res = $q->execute(array(), Doctrine::HYDRATE_ARRAY);
      return  $res[0]['order_request_detail_id'] ;
    }catch(Exception $e) {
      echo("Problem found". $e->getMessage());die;
    }
  }

  public function getCardHashByCardDetails($cardFirst,$cardLast){
      $query = $this->createQuery('i')
               ->select('i.card_hash')
               ->where('i.card_first = ?',$cardFirst)
               ->andWhere('i.card_last =?',$cardLast)
               ->orderBy('i.id DESC')
               ->limit(1)
               ->execute(array(), Doctrine::HYDRATE_ARRAY);
               if(count($query) >0 ){
                    return $query[0]['card_hash'];
               }else{
                   return ($query =array());
               }
     
  }
  
  public function setOrderStatus($orderNumber, $status){
    if($orderNumber != '' && $status != ''){
        try{
            $q = Doctrine_Query::create()
            ->update('ipay4meOrder')
            ->set('payment_status','?',$status)
            ->where('order_number =?',$orderNumber);
            $res = $q->execute();
            if($res){
              return true;
            }else{
              return false;
            }
        }catch (Exception $e) {
            throw New Exception ($e->getMessage());
        }        
    }else{
        return false;
    }

  }

  /**
   * [WP: 102] => CR: 146
   * @param <type> $card_first
   * @param <type> $card_last
   * @param <type> $status
   * @param <type> $userId
   * @return <type>
   * This function return success order data...
   */
  public function getPaidOrderByCardFirstandLast($card_first, $card_last, $status = '', $userId = '', $start_date = '', $end_date = ''){

      if($card_first !='' && $card_last !=''){
        $q = $this->createQuery('i')
          ->select('i.*')          
          ->where('card_first = ?', $card_first)
          ->andWhere('card_last = ?', $card_last);
          if($status != ''){
            $q->andWhere('payment_status = ?', $status);
          }
          if($userId != ''){
            $q->andWhere('created_by = ?', $userId);
          }
          if($start_date != ''){
            $q->andWhere('updated_at >= ?', $start_date.' 00:00:00');
          }
          if($end_date != ''){
            $q->andWhere('updated_at <= ?', $end_date.' 23:59:59');
          }
          $q->orderBy('id DESC');

          $res = $q->execute();
          if($res){
            return $res;
          }else{
            return false;
          }
      }else{
          return false;
      }
    
  }

  /**
   * [WP: 102] => CR: 146
   * @param <type> $card_hash
   * @param <type> $status
   * @param <type> $userId
   * @param <type> $start_date
   * @param <type> $end_date
   * @return <type>
   * This function return success order data...
   * 
   */

  public function getPaidOrderByCardHash($card_hash, $status = '', $userId = '', $start_date = '', $end_date = ''){

      if($card_hash !=''){
        $q = $this->createQuery('i')
          ->select('i.*')
          ->where('card_hash = ?', $card_hash);          
          if($status >= 0){
            $q->andWhere('payment_status = ?', $status);
          }
          if($userId != ''){
            $q->andWhere('created_by = ?', $userId);
          }
          if($start_date != ''){
            $q->andWhere('updated_at >= ?', $start_date.' 00:00:00');
          }
          if($end_date != ''){
            $q->andWhere('updated_at <= ?', $end_date.' 23:59:59');
          }
          $q->orderBy('id DESC');

          $res = $q->execute();
          if($res){
            return $res;
          }else{
            return false;
          }
      }else{
          return false;
      }

  }

  /**
   * [WP: 102] => CR> 146
   * @param <type> $cardFirst
   * @param <type> $cardLast
   * @param <type> $card_hash
   * @param <type> $user_id
   * @param <type> $start_date
   * @param <type> $end_date
   * @return <type> 
   */
  public function getPaymentDetailsForReport($cardFirst,$cardLast, $card_hash = '', $user_id = '', $start_date = '', $end_date = '')
  {
      $cardReportArray = array();

      for($i=0;$i<4;$i++){
          $transCount = $this->createQuery('ip')
          ->select('ip.id')
          ->where('ip.card_first =?',$cardFirst)
          ->andWhere('ip.card_last=?',$cardLast)
          ->andWhere('ip.payment_status=?',$i);
          if($card_hash != ''){
              $transCount->andWhere('ip.card_hash=?',$card_hash);
          }
          if($user_id != ''){
              $transCount->andWhere('ip.created_by=?',$user_id);
          }
          if($start_date != ''){
            $transCount->andWhere('ip.updated_at >= ?', $start_date.' 00:00:00');
          }
          if($end_date != ''){
            $transCount>andWhere('ip.updated_at <= ?', $end_date.' 23:59:59');
          }
          $transCount = $transCount->execute()->count();
          $cardReportArray['year'][$i] = $transCount;
      }
      ## subtract 30 from days inplace of month...
      ## Ashwani Kumar
      $lastMonthDate = date("Y-m-d",mktime(0,0,0,date('m'),date('d')-30,date('Y')));

      for($i=0;$i<4;$i++){
          $transCount = $this->createQuery('ip')
          ->select('ip.id')
          ->where('ip.card_first =?',$cardFirst)
          ->andWhere('ip.card_last=?',$cardLast)
          ->andWhere('ip.payment_status=?',$i)
          ->andWhere('ip.created_at > ?',$lastMonthDate);
          if($card_hash != ''){
              $transCount->andWhere('ip.card_hash=?',$card_hash);
          }
          if($user_id != ''){
              $transCount->andWhere('ip.created_by=?',$user_id);
          }
          $transCount = $transCount->execute()->count();
          $cardReportArray['month'][$i] = $transCount;
      }
      
      return $cardReportArray;
  }

}
