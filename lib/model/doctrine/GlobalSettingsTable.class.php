<?php


class GlobalSettingsTable extends Doctrine_Table
{

  public static function getInstance()
  {
    return Doctrine_Core::getTable('GlobalSettings');
  }

  public function getByKey($key)
  {
    try{
      $q = $this->createQuery('st')
      ->select('st.var_value')
      ->where('st.var_key= ?',$key);

      $result = $q->execute()->toArray();
      if(count($result) > 0)
      return $result[0]['var_value'];
      else
      return false;
    }catch(Exception $e) {
      echo("Problem found". $e->getMessage());die;
    }

  }
}
