<?php


class NisDollarRevenueTable extends PluginNisDollarRevenueTable
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('NisDollarRevenue');
    }

////    public function getDollarDetails($startDate,$endDate){

      public function getDollarDetails($month,$year,$accountId){
      $q = $this->createQuery('j')
        ->select('id,date,amount,remark')
        ->where('YEAR(j.date) = ?', $year)
        ->andWhere('MONTH(j.date) = ?', $month)
        ->andWhere('j.account_id = ?', $accountId)
        ->orderBy('j.date');
        return $q->execute()->toArray();



      // Connects to your Database
//     

    }

    public function getRevDetail($date,$accountId){
        $q = $this->createQuery('j')
        ->select('id,date,amount,remark')
        ->where('j.date = ?', $date)
        ->andWhere('j.account_id = ?', $accountId);
        return $q->execute()->toArray();

    }

    public function updateDetail($date,$amount,$remark='',$hdnId,$hdnAccountId){
        try {
            $q = Doctrine_Query::create()
            ->update('NisDollarRevenue')
            ->set('amount', '?', $amount)
            ->set('date', '?', $date)
            ->set('remark', '?', $remark)
            ->set('account_id', '?', $hdnAccountId)
            ->where("id =?", $hdnId)
            ->execute();
            return true;
        }catch(Exception $e){
            echo("Problem found". $e->getMessage());die;
        }
    }

public function getPreviousRecord($userId){
    try {
        $q = $this->createQuery('j')
        ->select('j.*')
        ->where('j.created_by =?', $userId)
        ->andWhere('j.updated_by = ?', $userId)
        ->orderBy('j.updated_at DESC');
        return $q->execute()->toArray();
        }catch(Exception $e){
            echo("Problem found". $e->getMessage());die;
        }
}
}