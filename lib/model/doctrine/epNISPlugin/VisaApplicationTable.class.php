<?php


class VisaApplicationTable extends PluginVisaApplicationTable
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('VisaApplication');
    }

    public function findDuplicateVisaApplication($firstName = null, $dob = null,$appType = null,$id = null)
    {
      $payObj = new paymentHelper();
      $ipay4meGateWayId = $payObj->getiPayformeGatewayTypeId();
      $query = Doctrine_Query::create()
      ->select("pa.id,t.id,crt.id,pa.id as app_id,pa.ref_no as ref_no,concat(pa.title,' ' , pa.other_name,' ' ,pa.middle_name,' ' ,pa.surname) as name,pa.date_of_birth as dob,pa.status as status,pa.paid_at as paid_on,group_concat( cast( ifnull( ips.order_number, 0 ) AS char( 50 ) ) ORDER BY ifnull( ips.order_number, 0 ) DESC ) AS order_no")
      ->from("VisaApplication pa");
      if($appType == "visa")
      $query->leftJoin("pa.IPaymentRequest t on t.visa_id = pa.id");
      if($appType == "freezone")
      $query->leftJoin("pa.IPaymentRequest t on t.freezone_id = pa.id");
      $query->leftJoin("t.CartItemsTransactions crt")
      ->leftJoin("crt.IPaymentResponse ips");
      if(isset ($firstName) && $firstName!='' && isset ($dob) && $dob!='')
      {
        $query->where("pa.other_name='".$firstName."'")
        ->andWhere("pa.date_of_birth='".$dob."'");
      }else if(isset ($id) && $id!=''){
        $query->where("pa.id=$id");
        $query->andWhere("pa.status IN ('New') OR ips.transaction_number IS NOT NULL OR pa.payment_gateway_id !=$ipay4meGateWayId")->groupBy("pa.id");
        return $data = $query->execute(array(),Doctrine::HYDRATE_ARRAY);
      }
      $query->andWhere("pa.status IN ('New') OR ips.transaction_number IS NOT NULL OR pa.payment_gateway_id !=$ipay4meGateWayId")
      ->groupBy("pa.id");
      return $query;
//      $query = $query->execute(array(),Doctrine::HYDRATE_ARRAY);
//            echo "<pre>";print_r($query);die;
    }


    public function getPaidAmount($appId, $currencyId = 1) {
        $fee = $this->createQuery('a')
        ->where('a.id = ?', $appId)
        ->execute(array(),Doctrine::HYDRATE_ARRAY);

        ## Currency id 1 means dollar...
        if($currencyId == 1){
            return $fee['0']['paid_dollar_amount'];
        }else{
            return $fee['0']['amount'];
        }
    }

  public function getVisaFreshRecord($id)
  {
     $visa_application=Doctrine_Query::create()
      ->select("VA.*,VAI.*,C.country_name,VC.var_value,EM.embassy_name,VT.var_value")
      ->from('VisaApplication VA')
      ->leftJoin('VA.VisaApplicantInfo VAI')
      ->leftJoin('VA.CurrentCountry C','C.id = VA.present_nationality_id')
      ->leftJoin('VA.VisaCategory VC','VC.id = VA.visacategory_id')
      ->leftJoin('VAI.EmbassyMaster EM','EM.id = VAI.embassy_of_pref_id')
      ->leftJoin('VAI.VisaTypeId VT','VT.id = VAI.visatype_id')
      ->where("VA.id =". $id)
      ->execute()->toArray(true);
      return $visa_application;
  }
  public function getVisaInfoById($visaId)
  {
      $query = Doctrine_Query::create()
      ->select("pa.id,pa.ref_no,pa.title, pa.other_name,pa.middle_name,pa.surname,pa.date_of_birth,pa.email,pa.status,pa.paid_dollar_amount,pa.paid_at, pa.visacategory_id ")
      ->from("VisaApplication pa")
      ->where('pa.id= ?',$visaId)
      ->execute(array(),Doctrine::HYDRATE_ARRAY);
      return $query;

  }

  /** Added by ashwani for visa approval id...
   * Searches the application id and reference id from the datbase.
   * Provides basic validation check for existence of user provided
   * appid and refid.
   *
   * @param <type> $appId
   * @param <type> $refId
   * @return <type> false if the passed in appid and refid isn't found, true otherwise
   */
  public function getVisaAppIdRefId($appId, $refId)
  {
    $q = $this->createQuery('j')
    ->select('id')
    ->where('j.id = ?', $appId)
    ->addwhere('j.ref_no = ?', $refId)->execute()->toArray();

    if(count($q) > 0) {
      return $q[0]['id'];
    }
    return false;
  }

   public function isFreshEntry($appId) {
    $q = $this->createQuery('at')
    ->select('id')
    ->where('at.id = ?', $appId)->addwhere('at.visacategory_id = ?', Doctrine::getTable('VisaCategory')->getFreshEntryId())->execute()->toArray();
    if(count($q) > 0) {
      return $q[0]['id'];
    }
    return false;
  }

  public function isFreezoneFreshEntry($appId) {
    $q = $this->createQuery('at')
    ->select('id')
    ->where('at.id = ?', $appId)->addwhere('at.visacategory_id = ?', Doctrine::getTable('VisaCategory')->getFreshEntryFreezoneId())->execute()->toArray();
    if(count($q) > 0) {
      return $q[0]['id'];
    }
    return false;
  }



   public function checkDuplicateApplication($first_name, $last_name, $gender_id, $date_of_birth, $place_of_birth, $email, $FzoneId)
   {
      if($first_name != '' && $last_name != '' && $gender_id != '' && $date_of_birth != ''  && $place_of_birth != '' && $email != '' && $FzoneId != ''){
          $query = Doctrine_Query::create()
                ->select("pa.id AS appId, pa.ref_no AS ref_no, pa.visacategory_id AS visacategory_id, pa.status AS status")
                ->from("VisaApplication pa");
                
          $query->where("pa.other_name = ?", strtolower($first_name))
                ->andWhere("pa.surname = ?", strtolower($last_name))
                ->andWhere("pa.gender = ?", $gender_id)
                ->andWhere("pa.email = ?", $email)
                ->andWhere("pa.date_of_birth = ?", $date_of_birth)
                ->andWhere("pa.place_of_birth = ?", strtolower($place_of_birth))
                ->andWhere("pa.zone_type_id = ?", $FzoneId);
          $query->orderBy('pa.id DESC');
          $query->limit(1);
          
          $result = $query->execute(array(), Doctrine::HYDRATE_ARRAY);          

          if(count($result) > 0){
            return $result;
          }else{
            return array();
          }
      }else{
          return 0;
      }
    }



  public function getVisaFreshRecordSummery($id)
  {
     $visa_application=Doctrine_Query::create()
      ->select("VA.*,VAI.*,C.country_name,VC.var_value,EM.embassy_name,VT.var_value,voa.*,vpa.*,vad.*,vna.*,vrea.*,vrc.*,
  voc.*,vpc.*,pc.country_name as previous_country,vcp.*,s.*,l.*,vtr.*,dc.country_name as deported_country,et.*,vaic.country_name as applying_country")
      ->from('VisaApplication VA')
      ->leftJoin('VA.VisaApplicantInfo VAI')
      ->leftJoin('VA.CurrentCountry C','C.id = VA.present_nationality_id')
      ->leftJoin('VA.PreviousCountry pc','pc.id = VA.previous_nationality_id')
      ->leftJoin('VA.VisaCategory VC','VC.id = VA.visacategory_id')
      ->leftJoin('VAI.EmbassyMaster EM','EM.id = VAI.embassy_of_pref_id')
      ->leftJoin('VAI.VisaTypeId VT','VT.id = VAI.visatype_id')
      ->leftJoin('VA.VisaOfficeAddress voa')
      ->leftJoin('VA.VisaPermanentAddress vpa')
      ->leftJoin('VA.VisaApplicationDetails vad')
      ->leftJoin('vad.VisaIntendedAddressNigeriaAddress vna')
      ->leftJoin('vad.VisaRelativeEmployerAddress vrea')
      ->leftJoin('vrea.Country vrc')
      ->leftJoin('voa.Country voc')
      ->leftJoin('vpa.Country vpc')
      ->leftJoin('VAI.VisaProcessingCentre vcp')
      ->leftJoin('vna.State s')
      ->leftJoin('vna.LGA l')
      ->leftJoin('VAI.VisaTypeReason vtr')
      ->leftJoin('VAI.DeportedCountry dc')
      ->leftJoin('VAI.EntryType et')
      ->leftJoin('VAI.ApplyingCountry vaic')
      ->where("VA.id =". $id)
      ->execute()->toArray(true);
      return $visa_application;
 }

  /* end here */

  public function getVisaApplicationDetails($appId,$refNo){

      $visa_application = Doctrine_Query::create()
                        ->select('VA.*,VAI.*,EA.embassy_name')
                        ->from('VisaApplication VA')
                        ->leftJoin('VA.VisaApplicantInfo VAI')
                        ->leftJoin('VAI.EmbassyMaster EA')
//                        ->leftJoin('VAI.Country C','C.id = VAI.applying_country_id')
                        ->where('VA.id =  ?',$appId)
                        ->andWhere('VA.ref_no = ?',$refNo)
                        ->execute()->toArray();
//                       echo "<pre>";print_r($visa_application);die;
  if(isset($visa_application) && $visa_application != ''){
      return $visa_application;

      }else{
          return $a = array();
      }
  }


  // Update Entry from VisaApplication Table for Unpay Visa & Passport Applications
  public function updateVisaApp($app_id){
    try {
      $q = Doctrine_Query::create()
      ->update('VisaApplication v')
      ->set('v.payment_gateway_id', 'NULL')
      ->set('v.status', '?', 'New')
      ->set('paid_at', 'NULL')
      ->set('payment_trans_id', 'NULL')
      ->set('paid_dollar_amount', 'NULL')
      ->set('paid_naira_amount ', 'NULL')
      ->set('ispaid', 'NULL')
      ->where("v.id = ? ", $app_id)
      ->limit(1);
      $res = $q->execute();

      if($res){
        return true;
      }else{
        return false;
      }

    }catch(Exception $e){
      throw New Exception ($e->getMessage());     
    }
  }

  
}