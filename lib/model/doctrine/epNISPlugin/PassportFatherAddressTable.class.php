<?php


class PassportFatherAddressTable extends PluginPassportFatherAddressTable
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('PassportFatherAddress');
    }
}