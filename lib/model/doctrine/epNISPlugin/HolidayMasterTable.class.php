<?php


class HolidayMasterTable extends PluginHolidayMasterTable
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('HolidayMaster');
    }
}