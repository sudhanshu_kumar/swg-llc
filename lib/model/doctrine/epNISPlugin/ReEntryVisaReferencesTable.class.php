<?php


class ReEntryVisaReferencesTable extends PluginReEntryVisaReferencesTable
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('ReEntryVisaReferences');
    }

    public function getReEntryVisaReference($id){
      $visa_application=Doctrine_Query::create()
      ->select("RVR.*,RA.*")
      ->from('ReEntryVisaReferences RVR')
      ->leftJoin('RVR.ReEntryVisaReferencesAddress RA')
      ->where("RVR.application_id=".$id)
      ->execute()->toArray(true);
      return $visa_application;
    }
}