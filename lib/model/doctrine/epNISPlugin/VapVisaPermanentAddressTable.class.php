<?php


class VapVisaPermanentAddressTable extends PluginVapVisaPermanentAddressTable
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('VapVisaPermanentAddress');
    }
}