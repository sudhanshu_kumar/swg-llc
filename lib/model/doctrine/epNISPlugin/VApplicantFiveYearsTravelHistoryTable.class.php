<?php


class VApplicantFiveYearsTravelHistoryTable extends PluginVApplicantFiveYearsTravelHistoryTable
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('VApplicantFiveYearsTravelHistory');
    }
}