<?php


class PaymentGatewayTypeTable extends PluginPaymentGatewayTypeTable
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('PaymentGatewayType');
    }

    // Check Payemnt Gateway Name
    public function getGatewayName($paymentGatewayID) {
        // TODO: Make it cached one
        $feid = Doctrine_Query::create()
            ->from('PaymentGatewayType  p')
            ->where("p.id ='".$paymentGatewayID."'")
            //->useResultCache(true)
            ->execute(array(), Doctrine::HYDRATE_ARRAY);
        return $feid[0]['var_value'];
    }
}