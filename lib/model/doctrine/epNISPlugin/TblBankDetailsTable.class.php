<?php


class TblBankDetailsTable extends PluginTblBankDetailsTable
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('TblBankDetails');
    }

    public function updateBAnkRecord($accountNumber,$bankName,$city,$state,$country,$hdnId,$hdnAccountId,$accountName){
        try {
            $q = Doctrine_Query::create()
            ->update('TblBankDetails')
            ->set('account_name', '?', $accountName)
            ->set('account_number', '?', $accountNumber)
            ->set('bank_name', '?', $bankName)
            ->set('city', '?', $city)
            ->set('state', '?', $state)
            ->set('country', '?', $country)
            ->set('project_id', '?', $hdnId)
            ->where("id =?", $hdnAccountId)
            ->execute();
            return true;
        }catch(Exception $e){
            echo("Problem found". $e->getMessage());die;
        }
    }

    public function getAllAccount($projectId){
        try{
            $q = $this->createQuery('j')
            ->select('*')
            ->where('project_id = ?', $projectId)
            ->orderBy('bank_name');
            return $q->execute()->toArray();
        }catch(Exception $e){
            echo("Problem found". $e->getMessage());die;
        }
    }
}