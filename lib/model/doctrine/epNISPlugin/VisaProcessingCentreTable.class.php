<?php


class VisaProcessingCentreTable extends PluginVisaProcessingCentreTable
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('VisaProcessingCentre');
    }

    public function getProcessingCentre($id)
    {
        $q = Doctrine_Query::create()
        ->select('centre_name')
        ->from('VisaProcessingCentre')
        ->where('id = ?', $id)->execute()->toArray();
        if(count($q) > 0){
            return $q[0]['centre_name'];
        }
        return false;
    }
}