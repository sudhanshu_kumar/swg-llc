<?php


class GlobalMasterTable extends PluginGlobalMasterTable
{

    public static function getInstance()
    {
        return Doctrine_Core::getTable('GlobalMaster');
    }
     public function getDetail($id){
    if(!empty($id)){
      $q = Doctrine_Query::create()
      ->select('gm.*')
      ->from('GlobalMaster gm')
      ->wherein('gm.id',$id);
      return $q->execute(array(),Doctrine::HYDRATE_ARRAY);
    }else{
      return ;
    }
  }
}