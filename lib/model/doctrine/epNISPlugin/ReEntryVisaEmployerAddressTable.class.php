<?php


class ReEntryVisaEmployerAddressTable extends PluginReEntryVisaEmployerAddressTable
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('ReEntryVisaEmployerAddress');
    }
}