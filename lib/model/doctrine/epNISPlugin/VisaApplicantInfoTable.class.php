<?php


class VisaApplicantInfoTable extends PluginVisaApplicantInfoTable
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('VisaApplicantInfo');
    }

    public function getProcessingCountry($appId) {
        $Result = Doctrine_Query::create()
        ->select("*")
        ->from('VisaApplicantInfo a')
        //->leftJoin('a.VisaProcessingCentreTable b')
        ->where('a.application_id = ?', $appId)
        ->execute(array(),Doctrine::HYDRATE_ARRAY);

        if(count($Result) > 0){
            return $Result[0]['applying_country_id'];
        }
        return false;
        //return $Result['0']['applying_country_id'];
    }
    public function getAuthId($appId) {
        $Result = Doctrine_Query::create()
        ->select("authority_id")
        ->from('VisaApplicantInfo a')
        //->leftJoin('a.VisaProcessingCentreTable b')
        ->where('a.application_id = ?', $appId)
        ->execute(array(),Doctrine::HYDRATE_ARRAY);

        if(count($Result) > 0){
            return $Result[0]['authority_id'];
        }
        return "";
        //return $Result['0']['applying_country_id'];
    }
}