<?php


class EmbassyMasterTable extends PluginEmbassyMasterTable
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('EmbassyMaster');
    }
}