<?php


class PassportApplicationTable extends PluginPassportApplicationTable
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('PassportApplication');
    }

    public function findDuplicatePassportApplication($firstName = null, $dob = null,$appId = null)
    {
//      $appData = $this->getDuplicateApplication($firstName, $dob);
////      echo "<pre>";print_r($appData);die;
//      $appIdArr = array();
//      foreach ($appData as $detail){
//        $appIdArr[] = $detail['id'];
//      }
      return $applicationDetailArr = $this->getAllApplicationDetailsArr($firstName,$dob,$appId);
//            echo "<pre>";print_r($applicationDetailArr);die;

    }

    public function getDuplicateApplication($firstName = null, $dob = null)
    {
      return $appArr = Doctrine_Query::create()
      ->select('t.*')
      ->from("PassportApplication t")
      ->where("t.first_name = ?", $firstName)
      ->andWhere("t.date_of_birth='".$dob."'")
      ->execute(array(),Doctrine::HYDRATE_ARRAY);
    }

    public function getAllApplicationDetailsArr($firstName,$dob,$id){

      $payObj = new paymentHelper();
      $ipay4meGateWayId = $payObj->getiPayformeGatewayTypeId();
      $query = Doctrine_Query::create()
      ->select("pa.id,pa.id as app_id,pa.ref_no as ref_no,t.id,concat(pa.title_id,' ' , pa.first_name,' ' ,pa.mid_name,' ' ,pa.last_name) as name,pa.date_of_birth as dob,pa.status as status,pa.paid_at as paid_on,group_concat( cast( ifnull( ips.order_number, 0 ) AS char( 50 ) ) ORDER BY ifnull( ips.order_number, 0 ) DESC ) AS order_no")
      ->from("PassportApplication pa")
      ->leftJoin("pa.IPaymentRequest t")
      ->leftJoin("t.CartItemsTransactions crt")
      ->leftJoin("crt.IPaymentResponse ips");
      if(isset ($firstName) && $firstName!='' && isset ($dob) && $dob!='')
      {
        $query->where("pa.first_name = ?", $firstName)
        ->andWhere("pa.date_of_birth='$dob'");  //,t.id,crt.id
      }else if(isset ($id) && $id!=''){
        $query->where("pa.id=$id");
        $query->andWhere("pa.status IN ('New') OR ips.transaction_number IS NOT NULL OR pa.payment_gateway_id !=$ipay4meGateWayId")->groupBy("pa.id");
        return $data = $query->execute(array(),Doctrine::HYDRATE_ARRAY);
      }
      $query->andWhere("pa.status IN ('New') OR ips.transaction_number IS NOT NULL OR pa.payment_gateway_id !=$ipay4meGateWayId")
      ->groupBy("pa.id");
      return $query;

//      ->from("IPaymentRequest t")
//      ->innerJoin("t.CartItemsTransactions crt")
//            ->innerJoin("crt.IPaymentResponse ips")
//      ->innerJoin("t.PassportApplication pa")
//      ->where("lower(pa.first_name)='".strtolower($firstName)."'")
//      ->andWhere("pa.date_of_birth='".$dob."'");
//      ->execute(array(),Doctrine::HYDRATE_ARRAY);
//->getSQlQuery();

//echo $query;die;
//      echo "<pre>";print_r($query);die;
    }
    public function getPassportInfo($appId,$refNo='')
    {
      if($refNo=='')
      {
        $Result = $this->createQuery('pa')
        ->select("pa.id,pa.passporttype_id,pa.ref_no,pa.status,pa.title_id,pa.last_name,pa.mid_name,pa.first_name,pa.passporttype_id,pa.date_of_birth,pa.processing_embassy_id,pa.processing_passport_office_id,pa.created_at,pa.payment_trans_id,pa.payment_gateway_id,pa.paid_at")
        ->where("pa.id = ".$appId)
        ->execute(array(), Doctrine::HYDRATE_ARRAY);
      }
      else
      {
        $Result = $this->createQuery('pa')
        ->select("pa.id,pa.passporttype_id,pa.ref_no,pa.status,pa.title_id,pa.last_name,pa.mid_name,pa.first_name,pa.passporttype_id,pa.date_of_birth,pa.processing_embassy_id,pa.processing_passport_office_id,pa.created_at")
        ->where("pa.id = ".$appId)
        ->andWhere("pa.ref_no = ".$refNo)
        ->execute(array(), Doctrine::HYDRATE_ARRAY);
      }

      if(count($Result) > 0)
        return $Result;
      else
        return false;
    }

    public function getProcessingCountry($appId) {
        $fee = $this->createQuery('a')
        ->where('a.id = ?', $appId)
        ->execute(array(),Doctrine::HYDRATE_ARRAY);


        return $fee['0']['processing_country_id'];
    }
    
    public function getPaidAmount($appId, $currencyId=1) {
        $fee = $this->createQuery('a')
        ->where('a.id = ?', $appId)
        ->execute(array(),Doctrine::HYDRATE_ARRAY);

        ## currency id 1 means dollar...
        if($currencyId == 1){
            return $fee['0']['paid_dollar_amount'];
        }else{
            return $fee['0']['amount'];
        }
    }
    public function getPassportInfoById($passportId)
    {
        $Result = $this->createQuery('pa')
        ->select("pa.id,pa.ref_no,pa.status,pa.title_id,pa.last_name,pa.mid_name,pa.first_name,pa.date_of_birth,pa.email,pa.status,pa.paid_dollar_amount,pa.paid_at")
        ->where("pa.id = ".$passportId)
        ->execute(array(), Doctrine::HYDRATE_ARRAY);
        return $Result;
    }

      public function getPassportInfoByAppId($appId,$refNo)
    {

        $Result = $this->createQuery('pa')
        ->select("pa.id,pa.passporttype_id,pa.ref_no,pa.status,pa.title_id,pa.last_name,pa.mid_name,pa.first_name,pa.passporttype_id,pa.date_of_birth,pa.processing_embassy_id,pa.processing_passport_office_id,pa.created_at,pa.paid_at")
        ->where("pa.id = ".$appId)
        ->andWhere("pa.ref_no = ".$refNo)
        ->execute(array(), Doctrine::HYDRATE_ARRAY);


      if(count($Result) > 0)
        return $Result;
      else
        return false;
    }

    public function checkDuplicateApplication($first_name, $last_name, $gender_id, $date_of_birth, $place_of_birth, $email, $contact_phone){

        if($first_name != '' && $last_name != '' && $gender_id != '' && $date_of_birth != '' && $place_of_birth != '' && $email != '' &&  $contact_phone != ''){
            $query = Doctrine_Query::create()
                   ->select("pa.id AS appId, pa.ref_no AS ref_no, pa.status AS status")
                   ->from("PassportApplication pa")
                   ->leftJoin("pa.PassportApplicantContactinfo c");
            $query->where("pa.first_name = ?",strtolower($first_name))
                   ->andWhere("pa.last_name = ?",strtolower($last_name))
                   ->andWhere("pa.email = ?",strtolower($email))
                   ->andWhere("pa.gender_id = ?",$gender_id)
                   ->andWhere("pa.place_of_birth = ?",strtolower($place_of_birth))
                   ->andWhere("pa.date_of_birth = ?",$date_of_birth)
                   ->andWhere("c.contact_phone = ?",$contact_phone);
            $query->orderBy('pa.id DESC');
            $query->limit(1);

            $result = $query->execute(array(), Doctrine::HYDRATE_ARRAY);            
            
            if(count($result) > 0){
                return $result;
            }else{
                return array();
            }            
        }else{
            return 0;
        }
    }
    public function getPassportApplicationDetails($appId,$refNo){
        $appArray = Doctrine_Query::create()
                              ->select('PA.*,EA.embassy_name')
                              ->from('PassportApplication PA')
                              ->leftJoin('PA.EmbassyMaster EA')
                              ->where('PA.id = ?',$appId)
                              ->andWhere('PA.ref_no = ?',$refNo)
                              ->execute()->toArray();
//                              echo "<pre>";print_r($appArray);die;
            if(isset($appArray) && $appArray != ''){
                  return $appArray;

                  }else{
                      return $a = array();
                  }

    }

  // Update Entry from PassportApplication Table for Unpay Visa & Passport Applications
  public function updatePassportApp($app_id){
    try {
      $q = Doctrine_Query::create()
      ->update('PassportApplication p')
      ->set('p.payment_gateway_id', 'NULL')
      ->set('p.status', '?', 'New')
      ->set('paid_at', 'NULL')
      ->set('payment_trans_id', 'NULL')
      ->set('paid_dollar_amount', 'NULL')
      ->set('paid_local_currency_amount ', 'NULL')
      ->set('ispaid', 'NULL')
      ->set('local_currency_id', '?', 0)
      ->where("p.id = ? ", $app_id)
      ->limit(1);
      $res = $q->execute();

      if($res){
        return true;
      }else{
        return false;
      }

    }catch(Exception $e){
      throw New Exception ($e->getMessage());     
    }
  }


}
