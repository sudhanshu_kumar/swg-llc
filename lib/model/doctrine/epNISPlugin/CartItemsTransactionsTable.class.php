<?php


class CartItemsTransactionsTable extends PluginCartItemsTransactionsTable
{

  public static function getInstance()
  {
    return Doctrine_Core::getTable('CartItemsTransactions');
  }

  public function getCartItemInfo($payment_request_id){

    if(!empty($payment_request_id)){
      $q = Doctrine_Query::create()
      ->select('cii.cart_id')
      ->from('CartItemsTransactions cii')
      ->wherein('cii.item_id',$payment_request_id);
      return $q->execute(array(),Doctrine::HYDRATE_ARRAY);
    }else{
      return ;
    }
  }

  public function getOrderDetailsOnNis($ipayOrderNo = null){
    if(isset ($ipayOrderNo) && is_string($ipayOrderNo) && $ipayOrderNo!=''){
      $cartDetailArr = $this->getCartDetails($ipayOrderNo);

      $ipay4meOrder = Doctrine::getTable("Ipay4meOrder")->findByOrderNumber($ipayOrderNo);
      if(count($ipay4meOrder) > 0){
          $card_first = $ipay4meOrder[0]['card_first'];
          $card_last = $ipay4meOrder[0]['card_last'];
          $card_holder = $ipay4meOrder[0]['card_holder'];          
      }else{
          $card_first = '';
          $card_last = '';
          $card_holder = '';
      }

      $finalArr = array();
      if(isset ($cartDetailArr) && is_array($cartDetailArr) && count($cartDetailArr)>0){
        foreach ($cartDetailArr as $key=>$appDetails){
          if(isset ($appDetails['passport_id']) && $appDetails['passport_id']!=''){
            $dataArr = Doctrine::getTable("PassportApplication")->find($appDetails['passport_id'])->toArray();
            $itemDetailsArr['id'] = $dataArr['id'];
            $itemDetailsArr['ref_no'] = $dataArr['ref_no'];
            $itemDetailsArr['name'] = ucfirst(($dataArr['title_id']))." ".ucfirst(($dataArr['first_name']))." ".ucfirst(($dataArr['mid_name']))." ".ucfirst(($dataArr['last_name']));
            $itemDetailsArr['fname'] = ucfirst(($dataArr['first_name']));
            $itemDetailsArr['mname'] = ucfirst(($dataArr['mid_name']));
            $itemDetailsArr['lname'] = ucfirst(($dataArr['last_name']));
            $itemDetailsArr['dob'] = $dataArr['date_of_birth'];
            $itemDetailsArr['app_type'] = "Passport";
            $itemDetailsArr['status'] = $dataArr['status'];
            $itemDetailsArr['email'] = $dataArr['email'];
            $itemDetailsArr['processing_country'] = $dataArr['processing_country_id'];
            $itemDetailsArr['processing_embassy'] = $dataArr['processing_embassy_id'];
            $itemDetailsArr['card_first'] = $card_first;
            $itemDetailsArr['card_last'] = $card_last;
            $itemDetailsArr['card_holder'] = $card_holder;
            
          }else if(isset ($appDetails['visa_id']) && $appDetails['visa_id']!=''){
            $visaObj = Doctrine::getTable("VisaApplication")->find($appDetails['visa_id']);
            $dataArr = $visaObj->toArray();
            $itemDetailsArr['id'] = $dataArr['id'];
            $itemDetailsArr['ref_no'] = $dataArr['ref_no'];
            $itemDetailsArr['name'] = ucfirst(($dataArr['title']))." ".ucfirst(($dataArr['other_name']))." ".ucfirst(($dataArr['middle_name']))." ".ucfirst(($dataArr['surname']));
            $itemDetailsArr['fname'] = ucfirst(($dataArr['other_name']));
            $itemDetailsArr['mname'] = ucfirst(($dataArr['middle_name']));
            $itemDetailsArr['lname'] = ucfirst(($dataArr['surname']));
            $itemDetailsArr['dob'] = $dataArr['date_of_birth'];
            $itemDetailsArr['app_type'] = "Visa";
            $itemDetailsArr['status'] = $dataArr['status'];
            $itemDetailsArr['email'] = $dataArr['email'];
            $itemDetailsArr['card_first'] = $card_first;
            $itemDetailsArr['card_last'] = $card_last;
            $itemDetailsArr['card_holder'] = $card_holder;

            ## Fetching processing country and embassy...
            $visaApplicantInfo = Doctrine::getTable("VisaApplicantInfo")->findByApplicationId($dataArr['id']);            
            if(count($visaApplicantInfo) > 0){
                $itemDetailsArr['processing_country'] = $visaApplicantInfo[0]['applying_country_id'];
                $itemDetailsArr['processing_embassy'] = $visaApplicantInfo[0]['embassy_of_pref_id'];
            }else{
                $itemDetailsArr['processing_country'] = '';
                $itemDetailsArr['processing_embassy'] = '';
            }

          }else if(isset ($appDetails['freezone_id']) && $appDetails['freezone_id']!=''){
            $visaObj = Doctrine::getTable("VisaApplication")->find($appDetails['freezone_id']);
            $dataArr = $visaObj->toArray();
            $itemDetailsArr['id'] = $dataArr['id'];
            $itemDetailsArr['ref_no'] = $dataArr['ref_no'];
            $itemDetailsArr['name'] = ucfirst(($dataArr['title']))." ".ucfirst(($dataArr['other_name']))." ".ucfirst(($dataArr['middle_name']))." ".ucfirst(($dataArr['surname']));
            $itemDetailsArr['fname'] = ucfirst(($dataArr['other_name']));
            $itemDetailsArr['mname'] = ucfirst(($dataArr['middle_name']));
            $itemDetailsArr['lname'] = ucfirst(($dataArr['surname']));
            $itemDetailsArr['dob'] = $dataArr['date_of_birth'];
            $itemDetailsArr['app_type'] = "Freezone";
            $itemDetailsArr['status'] = $dataArr['status'];
            $itemDetailsArr['email'] = $dataArr['email'];
            $itemDetailsArr['card_first'] = $card_first;
            $itemDetailsArr['card_last'] = $card_last;
            $itemDetailsArr['card_holder'] = $card_holder;

            ## Fetching processing country and embassy...
           
            $itemDetailsArr['processing_country'] = 'NG';
            $itemDetailsArr['processing_embassy'] = NULL;
            
          }else if(isset ($appDetails['visa_arrival_program_id']) && $appDetails['visa_arrival_program_id']!=''){
            $visaObj = Doctrine::getTable("VapApplication")->find($appDetails['visa_arrival_program_id']);
            $dataArr = $visaObj->toArray();
            $itemDetailsArr['id'] = $dataArr['id'];
            $itemDetailsArr['ref_no'] = $dataArr['ref_no'];
            $itemDetailsArr['name'] = ucfirst(($dataArr['title']))." ".ucfirst(($dataArr['first_name']))." ".ucfirst(($dataArr['middle_name']))." ".ucfirst(($dataArr['surname']));
            $itemDetailsArr['fname'] = ucfirst(($dataArr['first_name']));
            $itemDetailsArr['mname'] = ucfirst(($dataArr['middle_name']));
            $itemDetailsArr['lname'] = ucfirst(($dataArr['surname']));
            $itemDetailsArr['dob'] = $dataArr['date_of_birth'];
            $itemDetailsArr['app_type'] = "Vap";
            $itemDetailsArr['status'] = $dataArr['status'];
            $itemDetailsArr['email'] = $dataArr['email'];
            $itemDetailsArr['card_first'] = $card_first;
            $itemDetailsArr['card_last'] = $card_last;
            $itemDetailsArr['card_holder'] = $card_holder;

            ## Fetching processing country and embassy...
            $itemDetailsArr['processing_country'] = $dataArr['applying_country_id'];
            $itemDetailsArr['processing_embassy'] = NULL;


          }
          $finalArr[] =  $itemDetailsArr;
        }
        return $finalArr;
      }else{
        return null;
      }
    }else{
      return null;
    }
  }

  public function getCartDetails111($orderNo = null){
    if(isset ($orderNo) && $orderNo!=''){
      $detailArr = Doctrine::getTable("IPaymentRequest")->createQuery()
      ->select("t.*,t.passport_id as pid")
      ->from("IPaymentRequest t")
      ->leftJoin("t.CartItemsTransactions ir")
      ->leftJoin("ir.IPaymentResponse is")
      ->where("is.order_number='$orderNo'")
      ->execute(array(),Doctrine::HYDRATE_ARRAY);
      return $detailArr;
    }
  }

  /*
   * Added by vineet to see the entry of passport/visa/freezone in this table
   *
   */
  public function getPaymentRequest($application_id, $application_type)
  {
    if(isset($application_id) && isset($application_type))
    {
      $q = Doctrine::getTable("IPaymentRequest")->createQuery()
      ->select("t.id")
      ->from("IPaymentRequest t");
      if($application_type == 'p'){
        $q->where("t.passport_id=?",$application_id);
      }

      if($application_type == 'v'){
        $q->where("t.visa_id=?",$application_id)
        ->orWhere("t.freezone_id=?",$application_id);
      }

      if($application_type == 'vap'){
        $q->where("t.visa_arrival_program_id=?",$application_id);
      }

      $detailArr = $q->execute(array(),Doctrine::HYDRATE_ARRAY);
      return $detailArr;
    }
  }

  public function getPaymentRequestMoneyOrder($application_id, $application_type)
  {
    if(isset($application_id))
    {
      $q = Doctrine::getTable("IPaymentRequest")->createQuery();
      $q->select("t.id");
      $q->from("IPaymentRequest t");
      switch(strtoupper($application_type))
      {
        case 'P': $q->where("t.passport_id = ?",$application_id); break;
          case 'V': $q->where("t.visa_id = ?",$application_id); break;
            case 'F': $q->where("t.freezone_id = ?",$application_id); break;
                case 'VAP': $q->where("t.visa_arrival_program_id = ?",$application_id); break;
            }

            //echo $application_id;
            //print_r($q->getSqlQuery()); die();

            return $q->execute(array(),Doctrine::HYDRATE_ARRAY);

          }else{
            return ;
          }
        }


        public function getApplicationDetails($appDetails){
          $itemDetailsArr = array();
          if(isset ($appDetails) && is_array($appDetails) && count($appDetails)>0){
            //        foreach ($cartDetailArr as $key=>$appDetails){
            if(isset ($appDetails['passport_id']) && $appDetails['passport_id']!=''){
              $dataArr = Doctrine::getTable("PassportApplication")->find($appDetails['passport_id'])->toArray();
              $itemDetailsArr['id'] = $dataArr['id'];
              $itemDetailsArr['ref_no'] = $dataArr['ref_no'];
              $itemDetailsArr['name'] = ucfirst(($dataArr['title_id']))." ".ucfirst(($dataArr['first_name']))." ".ucfirst(($dataArr['mid_name']))." ".ucfirst(($dataArr['last_name']));
              $itemDetailsArr['dob'] = $dataArr['date_of_birth'];
              $itemDetailsArr['app_type'] = "passport";
              $itemDetailsArr['status'] = $dataArr['status'];
              $itemDetailsArr['amount'] = $dataArr['paid_dollar_amount'];
              $itemDetailsArr['convert_amount'] = $dataArr['amount'];
              $itemDetailsArr['email'] = $dataArr['email'];
              $itemDetailsArr['country_id'] = $dataArr['processing_country_id'];
              $itemDetailsArr['embassy_id'] = $dataArr['processing_embassy_id'];
            }else if(isset ($appDetails['visa_id']) && $appDetails['visa_id']!=''){
              $visaObj = Doctrine::getTable("VisaApplication")->find($appDetails['visa_id']);
              $dataArr = $visaObj->toArray();
              $itemDetailsArr['id'] = $dataArr['id'];
              $itemDetailsArr['ref_no'] = $dataArr['ref_no'];
              $itemDetailsArr['name'] = ucfirst(($dataArr['title']))." ".ucfirst(($dataArr['other_name']))." ".ucfirst(($dataArr['middle_name']))." ".ucfirst(($dataArr['surname']));
              $itemDetailsArr['dob'] = $dataArr['date_of_birth'];
              $itemDetailsArr['app_type'] = "visa";
              $itemDetailsArr['status'] = $dataArr['status'];
              $itemDetailsArr['email'] = $dataArr['email'];
              $itemDetailsArr['amount'] = $dataArr['paid_dollar_amount'];
              $itemDetailsArr['convert_amount'] = $dataArr['amount'];
            }else if(isset ($appDetails['freezone_id']) && $appDetails['freezone_id']!=''){
              $visaObj = Doctrine::getTable("VisaApplication")->find($appDetails['freezone_id']);
              $dataArr = $visaObj->toArray();
              $itemDetailsArr['id'] = $dataArr['id'];
              $itemDetailsArr['ref_no'] = $dataArr['ref_no'];
              $itemDetailsArr['name'] = ucfirst(($dataArr['title']))." ".ucfirst(($dataArr['other_name']))." ".ucfirst(($dataArr['middle_name']))." ".ucfirst(($dataArr['surname']));
              $itemDetailsArr['dob'] = $dataArr['date_of_birth'];
              $itemDetailsArr['app_type'] = "freezone";
              $itemDetailsArr['status'] = $dataArr['status'];
              $itemDetailsArr['email'] = $dataArr['email'];
              $itemDetailsArr['amount'] = $dataArr['paid_dollar_amount'];
              $itemDetailsArr['visacategory_id'] = $dataArr['visacategory_id'];
            }else if(isset ($appDetails['visa_arrival_program_id']) && $appDetails['visa_arrival_program_id']!=''){
              $visaObj = Doctrine::getTable("VapApplication")->find($appDetails['visa_arrival_program_id']);
              $dataArr = $visaObj->toArray();
              $itemDetailsArr['id'] = $dataArr['id'];
              $itemDetailsArr['ref_no'] = $dataArr['ref_no'];
              $itemDetailsArr['name'] = ucfirst(($dataArr['title']))." ".ucfirst(($dataArr['first_name']))." ".ucfirst(($dataArr['middle_name']))." ".ucfirst(($dataArr['surname']));
              $itemDetailsArr['dob'] = $dataArr['date_of_birth'];
              $itemDetailsArr['app_type'] = "vap";
              $itemDetailsArr['status'] = $dataArr['status'];
              $itemDetailsArr['email'] = $dataArr['email'];
              $itemDetailsArr['amount'] = $dataArr['paid_dollar_amount'];
            }
            //        }
          }

          return $itemDetailsArr;
        }

        public function getCartDetailsQuery($orderNo = null,$type = null){
          if(isset ($orderNo) && $orderNo!=''){
            $detailArr = Doctrine::getTable("IPaymentRequest")->createQuery()
            ->select("t.*,t.passport_id as pid")
            ->from("IPaymentRequest t")
            ->leftJoin("t.CartItemsTransactions ir")
            ->leftJoin("ir.IPaymentResponse is")
            ->where("is.order_number= ?",$orderNo);
            return $detailArr;
          }
        }



        public function getCartDetails($orderNo = null,$type = null){
          if(isset ($orderNo) && $orderNo!=''){
            $detailArr = Doctrine::getTable("IPaymentRequest")->createQuery()
            ->select("t.*,t.passport_id as pid")
            ->from("IPaymentRequest t")
            ->leftJoin("t.CartItemsTransactions ir")
            ->leftJoin("ir.IPaymentResponse is")
            ->where('is.order_number=?',$orderNo);

            //echo $detailArr->getSqlQuery(); exit;
            return $detailArr->fetchArray();
          }
        }


    /**
     *
     * @param <type> $retryId
     * @return <type>
     * To get details corresponding to the retry id passes.
     */
        public function getCartDetailsByRetryIdQuery($retryId){
          if(isset ($retryId) && $retryId!=''){
            $detailArr = Doctrine::getTable("IPaymentRequest")->createQuery()
            ->select("t.*,t.passport_id as pid")
            ->from("IPaymentRequest t")
            ->leftJoin("t.CartItemsTransactions ir")
            ->where("ir.transaction_number='$retryId'");
            return $detailArr;
          }
        }

        public function getRecordByAppId($app_id)
        {
          $query = Doctrine_Core::create()
          >select('t.*')
          ->from('CartItemTransactions')
          ->where('t.app_id = ?',$app_id)
          ->execute()->toArray();
          return $query;

        }
        public function getRetryIdByPaymentRequestId($id)
        {
          $query = Doctrine_Query::create()
          ->select('*')
          ->from('CartItemsTransactions')
          ->where('item_id = ?',$id)
          ->execute(array(),Doctrine::HYDRATE_ARRAY);

          if(!empty($query))
          {
            return $query;
          }
          else{
            return false;
          }

        }
        public function getIPRequestId($retryId)
        {
          $query = Doctrine_Query::create()
          ->select('*')
          ->from('CartItemsTransactions')
          ->where('transaction_number = ?',$retryId)
          ->execute(array(),Doctrine::HYDRATE_ARRAY);
          if(!empty($query))
          {
            return $query;
          }
          else{
            return false;
          }
        }


    /**
     *
     * @param <type> $cart_id
     * @param <type> $fields can be any field from table. if user does not pass then it will return whole fields.
     * @return <type> written by ashwani kumar.
     */
        public function getCartItemInfoByCartId($cart_id, $fields='*')
        {
          if(!empty($cart_id)){
            $query = Doctrine_Query::create()
            ->select($fields)
            ->from('CartItemsTransactions')
            ->where('cart_id = ?',$cart_id);
            return $query->fetchArray();
          }else{
            return false ;
          }
        }




        public function getCartItemAllIdInfo($arrItemId){
          if(is_array($arrItemId)){
            $q = Doctrine_Query::create()
            ->select('cii.cart_id, cii.transaction_number')
            ->from('CartItemsTransactions cii')
            ->whereIn('cii.item_id ',$arrItemId);

            return $q->execute(array(),Doctrine::HYDRATE_ARRAY);

          }else{
            return ;
          }
        }

      }
