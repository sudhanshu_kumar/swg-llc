<?php


class IPaymentRequestTransactionTable extends PluginIPaymentRequestTransactionTable
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('IPaymentRequestTransaction');
    }
}