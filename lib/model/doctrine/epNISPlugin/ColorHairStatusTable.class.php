<?php


class ColorHairStatusTable extends PluginColorHairStatusTable
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('ColorHairStatus');
    }
}