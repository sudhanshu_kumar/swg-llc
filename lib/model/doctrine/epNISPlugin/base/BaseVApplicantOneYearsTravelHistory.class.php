<?php
// Connection Component Binding
Doctrine_Manager::getInstance()->bindComponent('VApplicantOneYearsTravelHistory', 'connection_nis');

/**
 * BaseVApplicantOneYearsTravelHistory
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property VisaApplication $VisaApplication
 * 
 * @method VisaApplication                 getVisaApplication() Returns the current record's "VisaApplication" value
 * @method VApplicantOneYearsTravelHistory setVisaApplication() Sets the current record's "VisaApplication" value
 * 
 * @package    ama
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseVApplicantOneYearsTravelHistory extends VisaApplicantTravelHistory
{
    public function setUp()
    {
        parent::setUp();
        $this->hasOne('VisaApplication', array(
             'local' => 'application_id',
             'foreign' => 'id'));
    }
}