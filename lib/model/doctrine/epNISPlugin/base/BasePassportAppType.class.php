<?php
// Connection Component Binding
Doctrine_Manager::getInstance()->bindComponent('PassportAppType', 'connection_nis');

/**
 * BasePassportAppType
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property Doctrine_Collection $PassportApplications
 * @property Doctrine_Collection $Fee
 * 
 * @method Doctrine_Collection getPassportApplications() Returns the current record's "PassportApplications" collection
 * @method Doctrine_Collection getFee()                  Returns the current record's "Fee" collection
 * @method PassportAppType     setPassportApplications() Sets the current record's "PassportApplications" collection
 * @method PassportAppType     setFee()                  Sets the current record's "Fee" collection
 * 
 * @package    ama
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BasePassportAppType extends GlobalMaster
{
    public function setUp()
    {
        parent::setUp();
        $this->hasMany('PassportApplication as PassportApplications', array(
             'local' => 'id',
             'foreign' => 'passporttype_id'));

        $this->hasMany('PassportFee as Fee', array(
             'local' => 'id',
             'foreign' => 'passporttype_id'));
    }
}