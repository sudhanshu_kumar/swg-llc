<?php
// Connection Component Binding
Doctrine_Manager::getInstance()->bindComponent('NisDollarRevenue', 'connection_nis');

/**
 * BaseNisDollarRevenue
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $id
 * @property date $date
 * @property double $amount
 * @property integer $account_id
 * @property string $remark
 * @property TblBankDetails $TblBankDetails
 * 
 * @method integer          getId()             Returns the current record's "id" value
 * @method date             getDate()           Returns the current record's "date" value
 * @method double           getAmount()         Returns the current record's "amount" value
 * @method integer          getAccountId()      Returns the current record's "account_id" value
 * @method string           getRemark()         Returns the current record's "remark" value
 * @method TblBankDetails   getTblBankDetails() Returns the current record's "TblBankDetails" value
 * @method NisDollarRevenue setId()             Sets the current record's "id" value
 * @method NisDollarRevenue setDate()           Sets the current record's "date" value
 * @method NisDollarRevenue setAmount()         Sets the current record's "amount" value
 * @method NisDollarRevenue setAccountId()      Sets the current record's "account_id" value
 * @method NisDollarRevenue setRemark()         Sets the current record's "remark" value
 * @method NisDollarRevenue setTblBankDetails() Sets the current record's "TblBankDetails" value
 * 
 * @package    ama
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseNisDollarRevenue extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('tbl_nis_dollar_revenue');
        $this->hasColumn('id', 'integer', null, array(
             'type' => 'integer',
             'primary' => true,
             'autoincrement' => true,
             ));
        $this->hasColumn('date', 'date', null, array(
             'type' => 'date',
             ));
        $this->hasColumn('amount', 'double', null, array(
             'type' => 'double',
             'notnull' => true,
             ));
        $this->hasColumn('account_id', 'integer', 11, array(
             'type' => 'integer',
             'notnull' => true,
             'length' => 11,
             ));
        $this->hasColumn('remark', 'string', 255, array(
             'type' => 'string',
             'length' => 255,
             ));


        $this->index('dateindex', array(
             'fields' => 
             array(
              0 => 'date',
              1 => 'account_id',
             ),
             'type' => 'unique',
             ));
        $this->option('type', 'InnoDB');
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasOne('TblBankDetails', array(
             'local' => 'account_id',
             'foreign' => 'id',
             'onDelete' => 'CASCADE'));

        $timestampable0 = new Doctrine_Template_Timestampable();
        $sftrackable0 = new sfTrackable();
        $this->actAs($timestampable0);
        $this->actAs($sftrackable0);
    }
}