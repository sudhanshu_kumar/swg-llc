<?php


class VisaRelativeEmployerAddressTable extends PluginVisaRelativeEmployerAddressTable
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('VisaRelativeEmployerAddress');
    }
}