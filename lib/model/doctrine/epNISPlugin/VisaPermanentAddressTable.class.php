<?php


class VisaPermanentAddressTable extends PluginVisaPermanentAddressTable
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('VisaPermanentAddress');
    }
}