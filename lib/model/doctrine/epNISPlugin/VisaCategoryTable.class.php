<?php


class VisaCategoryTable extends PluginVisaCategoryTable
{
    public static $FRESH_ENTRY = "Fresh";
    public static $RE_ENTRY = "Re-Entry";
    public static $FRESH_ENTRY_FREEZONE = "Fresh Freezone";
    public static $RE_ENTRY_FREEZONE = "Re-Entry Freezone";

    public function getFreshEntryId() {
        return $this->getEntryId(self::$FRESH_ENTRY);
    }

    public function getReEntryId() {
        return $this->getEntryId(self::$RE_ENTRY);
    }

    public function getFreshEntryFreezoneId() {
        return $this->getEntryId(self::$FRESH_ENTRY_FREEZONE);
    }

    public function getReEntryFreezoneId() {
        return $this->getEntryId(self::$RE_ENTRY_FREEZONE);
    }

    private function getEntryId($cType) {
        // TODO: Make it cached one
        $feid = $this->createQuery('ca')
        ->select('ca.id')
        ->addWhere('ca.var_value = ?', $cType)
        //->useResultCache(true)
        ->execute(array(), Doctrine::HYDRATE_ARRAY);
        
        return $feid[0]['id'];
    }

    public function getCachedQuery() {
        return $this->createQuery('ca');
            //->useResultCache(true);
    }

    public static function getInstance()
    {
        return Doctrine_Core::getTable('VisaCategory');
    }
}