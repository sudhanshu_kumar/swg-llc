<?php


class PassportReferenceAddressTable extends PluginPassportReferenceAddressTable
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('PassportReferenceAddress');
    }
}