<?php


class CountryTable extends PluginCountryTable
{
  /**
   * [WP:083] => CR:121
   * @var <type>
   * Remvoing Liberia(LR) country from the list...
   * Showing Liberia (LR) country in processing list.
   */
  ## [WP: 097] => CR:138 putting below varibales into app.yml...
  //static $nigeriaOnlyCountries_Passport = array('SL','CM','GH','SN','BF','TD','TZ','ZM','GN','NG','KE'); //'LR',//'CI',
  //static $nigeriaOnlyCountries_Visa = array('SL','CM','GH','SN','BF','BJ','TD','TZ','ZM','GN','NG','KE'); //'LR' //'CI',
  //static $nigeriaOnlyCountries_Vap = array('SL','CM','GH','SN','BF','BJ','TD','TZ','ZM','GN','NG'); //'LR' //'CI',
  //static $nigeriaOnlyCountries = array('SL','CM','GH','SN','BF','BJ','TD','TZ','ZM','GN','NG'); //'LR', //'CI', //KE

  public static function getInstance()
  {
    return Doctrine::getTable('Country');
  }

  public static function getCountryListOption($appType) {
      // ALTER TABLE `tbl_country` ADD `is_vwp` SMALLINT( 5 ) NULL DEFAULT NULL AFTER `is_african`
    $query = Doctrine_Query::create()
    ->from('Country');
//    if($appType == 'vap'){
//        $query->where('is_vwp = ?',1);
//    }
    $query->orderBy('country_name ASC');
    //echo $query->getSqlQuery();
    $choices = $query->execute(array(),Doctrine::HYDRATE_ARRAY);
    /**
     * [WP: 097] => CR: 139
     */
    $passportExcludedCountries = sfconfig::get('app_nigeriaOnlyCountries_Passport');
    $visaExcludedCountries = sfconfig::get('app_nigeriaOnlyCountries_Visa');
    $vapExcludedCountries = sfconfig::get('app_nigeriaOnlyCountries_Vap');
    
    foreach($choices as $choice)
    {
      if (strtolower($appType) == "p")
      {
        if (!in_array($choice['id'], $passportExcludedCountries))
        {
          $countryChoices[$choice['id']]=$choice['country_name'];
        }
      }
      else if(strtolower ($appType) == "v")
      {
        if (!in_array($choice['id'], $visaExcludedCountries))
        {
          $countryChoices[$choice['id']]=$choice['country_name'];
        }
      }
      else if(strtolower ($appType) == "vap")
      {
        if (!in_array($choice['id'], $vapExcludedCountries))
        {
          $countryChoices[$choice['id']]=$choice['country_name'];
        }
      }
      else
      {
        $countryChoices[$choice['id']]=htmlspecialchars(($choice['country_name']));

      }
    }
    return $countryChoices;
  }

  public function getOnlyEmbassyCountry(){
      $query = $this->createQuery('c')
             ->select('c.*')
             ->innerJoin('c.EmbassyMaster em')
             ->execute()->toArray();
            if($query){
              return $query;
            }else{
                return $query = '';
            }
  }

  public function getCountryList($presentCountryArray){

      $query = Doctrine_Query::create()
                   ->select('c.*')
                   ->from('Country c')
                   ->whereNotIn('c.id',$presentCountryArray);
           $result =   $query->execute()->toArray();
           $countryArray = array();
           foreach($result as $value){
               $countryArray[$value['id']] =$value['country_name'];
           }
           return $countryArray;

  }
/**function getCountryForValidation()
   *@purpose :get all active country from db
   *@param : 
   *@return :  list of all active country
   *@author : KSingh
   *@date : 13-10-2011
   */
  public function getCountryForValidation(){
    $nigeriaExcludedCountries = sfconfig::get('app_nigeriaOnlyCountries');
    $query = Doctrine_Query::create()
    ->from('Country')
    ->orderBy('country_name ASC');
    $choices = $query->execute(array(),Doctrine::HYDRATE_ARRAY);
    foreach($choices as $choice)
    {
      if(!in_array($choice['id'], $nigeriaExcludedCountries)){
        $countryChoices[$choice['id']]=htmlspecialchars(($choice['country_name']));

      }
    }
    return $countryChoices;
  }


  public function getSelectedCountryList($presentCountryArray){
      echo "<pre>";print_r($presentCountryArray);echo "</pre>";
      $presentCountryArray = array('AF','DZ','AO');
      $query = Doctrine_Query::create()
                   ->select('c.*')
                   ->from('Country c')
                   ->whereIn('c.id',$presentCountryArray)
                   ->orderBy('country_name ASC');


                   //die("A: ".$query->getSqlQuery());
           $result =   $query->execute()->toArray();
           $countryArray = array();
           foreach($result as $value){
               $countryArray[$value['id']] =$value['country_name'];
           }
           return $countryArray;

  }




}