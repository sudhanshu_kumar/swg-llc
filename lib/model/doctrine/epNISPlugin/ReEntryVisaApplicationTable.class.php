<?php


class ReEntryVisaApplicationTable extends PluginReEntryVisaApplicationTable
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('ReEntryVisaApplication');
    }

    public function getProcessingCountry($appId) {
        $Result = Doctrine_Query::create()
        ->select("*")
        ->from('ReEntryVisaApplication a')
        //->leftJoin('a.VisaProcessingCentreTable b')
        ->where('a.application_id = ?', $appId)
        ->execute(array(),Doctrine::HYDRATE_ARRAY);

        if(count($Result) > 0){
            return $Result['0']['processing_centre_id'];
        }
        
        
    }

  /*
   * Added by ashwani for visa payemnt receipt..
   */
  public function getVisaTypeId($appId)
  {
    $q = $this->createQuery('j')
    ->select('j.visa_type_id')
    ->where('j.application_id = ?', $appId)
    ->execute()
    ->toArray();
    if(count($q) > 0)
    return $q[0]['visa_type_id'];
    else
    return false;
  }
   
}