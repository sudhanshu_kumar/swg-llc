<?php


class GatewayTypeTable extends PluginGatewayTypeTable
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('GatewayType');
    }
}