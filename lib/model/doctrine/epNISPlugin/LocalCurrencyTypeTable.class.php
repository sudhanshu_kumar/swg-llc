<?php


class LocalCurrencyTypeTable extends PluginLocalCurrencyTypeTable
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('LocalCurrencyType');
    }
}