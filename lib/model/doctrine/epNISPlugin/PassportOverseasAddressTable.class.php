<?php


class PassportOverseasAddressTable extends PluginPassportOverseasAddressTable
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('PassportOverseasAddress');
    }
}