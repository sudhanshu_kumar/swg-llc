<?php


class EntryTypeTable extends PluginEntryTypeTable
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('EntryType');
    }
}