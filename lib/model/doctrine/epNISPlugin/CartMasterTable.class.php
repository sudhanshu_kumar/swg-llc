<?php


class CartMasterTable extends PluginCartMasterTable
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('CartMaster');
    }
}