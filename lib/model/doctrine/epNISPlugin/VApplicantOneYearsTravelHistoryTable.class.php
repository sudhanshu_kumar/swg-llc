<?php


class VApplicantOneYearsTravelHistoryTable extends PluginVApplicantOneYearsTravelHistoryTable
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('VApplicantOneYearsTravelHistory');
    }
}