<?php


class PassportAppTypeTable extends PluginPassportAppTypeTable
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('PassportAppType');
    }
   public function getPassportTypeName($id)
     {
        $q = $this->createQuery('j')
        ->select('var_value')
        ->addwhere('j.id = ?', $id)->execute()->toArray();
        if(count($q) > 0)
          return $q[0]['var_value'];
        else
          return false;
     }

   public function getPassportTypeNameArray($name1,$name2)
     {
        $q = $this->createQuery('j')
        ->select('id,var_value')
        ->where('j.var_value = ?', $name1)
        ->orWhere('j.var_value = ?', $name2)
        ->execute()
        ->toArray();
        return $q;
     }

   public function getPassportTypeIdArray($name)
     {
        $q = $this->createQuery('j')
        ->select('id,var_value')
        ->where('j.var_value = ?', $name)
        ->execute()
        ->toArray();
        return $q;
     }


   public function getPassportTypeNameArrayMrp($name1,$name2,$name3,$name4)
     {
        $q = $this->createQuery('j')
        ->select('id,var_value')
        ->where('j.var_value = ?', $name1)
        ->orWhere('j.var_value = ?', $name2)
        ->orWhere('j.var_value = ?', $name3)
        ->orWhere('j.var_value = ?', $name4)
        ->execute()
        ->toArray();
        return $q;
     }

}