<?php


class CurrencyConverterTable extends PluginCurrencyConverterTable
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('CurrencyConverter');
    }


    public function getConvertedAmount($from_currency, $to_currency){

       $query = Doctrine_Query::create()
                   ->select('c.*')
                   ->from('CurrencyConverter c')
                   ->where('c.from_currency = ?', $from_currency)
                   ->andWhere('c.to_currency = ?', $to_currency);
       $result = $query->execute();
       return $result;
  }
    
}