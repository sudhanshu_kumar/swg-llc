<?php


class PassportApplicationDetailsTable extends PluginPassportApplicationDetailsTable
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('PassportApplicationDetails');
    }
}