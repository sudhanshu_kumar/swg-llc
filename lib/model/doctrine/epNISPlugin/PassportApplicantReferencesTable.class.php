<?php


class PassportApplicantReferencesTable extends PluginPassportApplicantReferencesTable
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('PassportApplicantReferences');
    }
}