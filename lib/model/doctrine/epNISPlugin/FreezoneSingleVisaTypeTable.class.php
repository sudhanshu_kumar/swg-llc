<?php


class FreezoneSingleVisaTypeTable extends PluginFreezoneSingleVisaTypeTable
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('FreezoneSingleVisaType');
    }

     public static $SINGLE_REENTRY = "Single Re-entry";
      
     public function getFreeZoneVisaTypeId() {
        return $this->getVisaTypeId(self::$SINGLE_REENTRY);
      }
      private function getVisaTypeId($cType) {
        // TODO: Make it cached one
        $feid = $this->createQuery('ca')
        ->select('ca.id')
        ->addWhere('ca.var_value=?',$cType)
        //->useResultCache(true)
        ->execute(array(), Doctrine::HYDRATE_ARRAY);
        if(isset($feid[0]['id'])){
         return $feid[0]['id'];
        }else{
         return false;
        }
      }
}