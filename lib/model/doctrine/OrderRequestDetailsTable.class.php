<?php


class OrderRequestDetailsTable extends Doctrine_Table
{

    public static function getInstance()
    {
        return Doctrine_Core::getTable('OrderRequestDetails');
    }



    public function getPaymentStatus($order_request_id){
        $query = $this->createQuery('d')
        ->select('*')
        ->Where('d.order_request_id = ?', $order_request_id)
        ->andwhere("d.payment_status = '0'");

        return $query->execute()->count();
    }



    public function getNotificationTransactionDetails($request_order_number) {
        try {

            $res = array();

            $orview = $this->createQuery('0R')
            ->select('OR.*,RS.*,RQ.*')
            ->leftJoin('OR.OrderRequest O')
            ->leftJoin('0R.Request RQ')
            ->leftJoin('RQ.Response RS')
            ->where('RQ.request_order_number=?',$request_order_number)
            ->andWhere('RS.response_code=?',1);

            $res = $orview->execute(array(),Doctrine::HYDRATE_ARRAY);
            if(count($res)>0) {
                return $res[0];
            }
            return $res;
        }catch(Exception $e) {
            echo("Problem found". $e->getMessage());die;
        }
    }


    public function updatePaymentStatus($request_order_number) {
        try {

            $q = $this->createQuery('d')
            ->update()
            ->LeftJoin('d.Request r')
            ->set('d.payment_status',"'0'")
            ->where('r.request_order_number =?',$request_order_number);

            //     print $q->getSqlQuery();

            $result = $q->execute();

        }
        catch (Exception $e) {
            throw New Exception ($e->getMessage());
        }
    }

    public function getPaymentReport($fromDate='',$toDate='') {
        try {
            $res = array();
            $splitQry = $this->createQuery()
            ->select('sum(ors.item_fee ) , ors.merchant_code ,count(ord.id) as totaltransaction')
            ->from('OrderRequestSplit ors')
            ->leftJoin('ors.OrderRequestDetails ord')
            ->andWhere('ord.payment_status=?',0);
            if(''!=$fromDate){
                $splitQry->andWhere('ord.paid_date  >= ?',$fromDate);
            }
            if(''!=$toDate){
                $splitQry->andWhere('ord.paid_date <= ?',$toDate);

            }
            $splitQry->groupBy('ors.merchant_code');

            $res = $splitQry->execute(array(),Doctrine::HYDRATE_ARRAY);
            //            echo $fromDate."----".$toDate."----".$merchant_id."----".$splitQry->getSqlQuery();
            //        echo "<pre>";   print_r($res);
            //           die;
            return $res;
        }catch(Exception $e) {
            echo("Problem found". $e->getMessage());die;
        }
    }

    public function getPaymentDetailReport($fromDate='',$toDate=''){
        try {
            $res = array();
            $splitQry = $this->createQuery('0R')
            ->select('*, ORS.*,ORQ.id, ORQ.transaction_number')
            ->leftJoin('0R.OrderRequestSplit ORS')
            ->innerJoin('0R.OrderRequest ORQ')
            ->andWhere('0R.payment_status=?',0);
            if(''!=$fromDate){
                $splitQry->andWhere('paid_date  >= ?',$fromDate);
            }
            if(''!=$toDate){
                $splitQry->andWhere('paid_date <= ?',$toDate);

            }
            $splitQry->orderBy('paid_date desc');

            $res = $splitQry->execute(array(),Doctrine::HYDRATE_ARRAY);


            $res = $splitQry->execute(array(),Doctrine::HYDRATE_ARRAY);
            return $splitQry;
        }catch(Exception $e) {
            echo("Problem found". $e->getMessage());die;
        }
    }
    public function getSplitDetails($fromDate='',$toDate='',$merchant_id) {
        try {

            $res = array();
            $splitQry = $this->createQuery('0R')
            ->select('*, ORS.*,ORQ.id, ORQ.transaction_number')
            ->leftJoin('0R.OrderRequestSplit ORS')
            ->innerJoin('0R.OrderRequest ORQ')
            ->where('0R.merchant_id=?',$merchant_id)
            ->andWhere('0R.payment_status=?',0);
            if(''!=$fromDate){
                $splitQry->andWhere('paid_date  >= ?',$fromDate);
            }
            if(''!=$toDate){
                $splitQry->andWhere('paid_date <= ?',$toDate);

            }
            $splitQry->orderBy('paid_date');

            $res = $splitQry->execute(array(),Doctrine::HYDRATE_ARRAY);
            $totRec=count($res);
            if($totRec>0) {

                $mechQuery = $this->createQuery()
                ->select('DISTINCT ORS.merchant_id, MR.name')
                ->from('OrderRequestSplit ORS')
                ->leftJoin('ORS.OrderRequestDetails 0R')
                ->leftJoin('ORS.Merchant MR')
                ->where('0R.merchant_id = ?',$merchant_id);

                $merchArr = $mechQuery->execute(array(),Doctrine::HYDRATE_SCALAR);

                $retArr[0]=$splitQry;
                $retArr[1]=$merchArr;

                return $retArr;
            }
            return $splitQry;
        }catch(Exception $e) {
            echo("Problem found". $e->getMessage());die;
        }
    }
    public function updatePayment($orderDetailId, $userId)
    {
        try {
            $q = $this->createQuery('d')
            ->update()
            ->set('d.payment_status',"'0'")
            ->set('d.user_id', $userId)
            ->set('d.paid_date',"'".date('Y-m-d H:i:s')."'")
            ->where('d.id =?',$orderDetailId);
            //      print $q->getSqlQuery();die;
            $result = $q->execute();
            return $result;
        }
        catch (Exception $e) {
            throw New Exception ($e->getMessage());
        }
    }

    public function getOrderDetails($request_order_number) {
        try {
            $q = $this->createQuery('r')
            ->select('r.*,res.*,d.*,t.*')
            ->innerJoin('r.Request res')
            ->innerJoin('res.OrderRequestDetails d')
            ->innerJoin('d.OrderRequest t')
            ->where('res.request_order_number=?',$request_order_number );
            //   print $q->getSqlQuery();

            $res = $q->execute();

            if($res->count() == 0) {
                return 0;
            }
            return $res;
        }
        catch (Exception $e) {
            throw New Exception ($e->getMessage());
        }
    }

    public function getOrderRequestDetails($request_order_id) {
        try {
            $q = $this->createQuery('r')
            ->select('r.*,t.*,m.*')
            ->innerJoin('r.OrderRequest t')
            ->leftJoin('r.Merchant  m')
            ->where('r.id=?',$request_order_id ) ;
            $result = $q->fetchArray() ;
            //      $res = $q->execute();

            if(count($result) == 0) {
                return 0;
            }
            return $result[0];
        }
        catch (Exception $e) {
            throw New Exception ($e->getMessage());
        }
    }

    public function getTotAmountBetDate($NgnFromTime , $NgnToTime) {
        try {
            $sql = "select sum(ord.amount) as TotAmount
                        from order_request_details ord
                        left outer join ipay4me_order ipd on (ipd.order_request_detail_id=ord.id)
                        where
                                ipd.updated_at > '".$NgnFromTime."'
                            and ipd.updated_at <= '".$NgnToTime."'
                            and ipd.payment_status = '0'
                            and ipd.order_number NOT IN (SELECT order_number FROM `vault`)

                    ";
            return $sql;
        }catch(Exception $e) {
            echo("Problem found". $e->getMessage());die;
        }
    }

    public function getLatestUnpaidOrderRequestDetails($request_order_id) {
        try {
            $q = $this->createQuery('r')
            ->select('r.id')
            //->innerJoin('r.OrderRequest t')
            ->where('r.order_request_id =?',$request_order_id )
            //->where('r.payment_status=?',1)
            ->orderBy('r.updated_at  DESC')
            ->limit(1);
            $result = $q->fetchArray() ;
            if(count($result) == 0) {
                return 0;
            }
            return $result[0];
        }
        catch (Exception $e) {
            throw New Exception ($e->getMessage());
        }
    }


    public function setBlockStatus($orderRequestDetailId){
        try {
            $q = Doctrine_Query::create()
            ->update('OrderRequestDetails')
            ->set('payment_status',2)
            ->where('id =?',$orderRequestDetailId);
            $res = $q->execute();
            if($res){
                return true;
            }else{
                return false;
            }
        }
        catch (Exception $e) {
            throw New Exception ($e->getMessage());
        }
    }


    public function setChargebackStatus($orderRequestDetailId){
        try{
            $q = Doctrine_Query::create()
            ->update('OrderRequestDetails')
            ->set('payment_status',3)
            ->where('id =?',$orderRequestDetailId);
            $res = $q->execute();
            if($res){
                return true;
            }else{
                return false;
            }


        }
        catch (Exception $e) {
            throw New Exception ($e->getMessage());
        }
    }

    public function getOrderDetailId($transactionNo)
    {
        $query = Doctrine_Query::create()
        ->select('*')
        ->from('OrderRequestDetails')
        ->where('retry_id = ?',$transactionNo)
        ->execute(array(),Doctrine::HYDRATE_ARRAY);
        if(!empty($query))
        {
            return $query;
        }
        else
        {
            return false;
        }


    }
    public function getOrderRequestId($orderRequestId)
    {
        if(isset ($orderRequestId) && $orderRequestId!=''){
            $detailArr = $this->createQuery('o')
            ->select('o.*')
            ->from('OrderRequestDetails o')
            ->where('o.id = ?',$orderRequestId)
            ->execute(array(),Doctrine::HYDRATE_ARRAY);
            return $detailArr;
        }
    }

    public function getOrderDetailByRetryId($retryIds=array())
    {
        $query = Doctrine_Query::create()
        ->select('*')
        ->from('OrderRequestDetails')
        ->where('payment_status = ?', 0)
        ->andWhereIn('retry_id ',$retryIds)
        ->execute(array(),Doctrine::HYDRATE_ARRAY);
        if(!empty($query))
        {
            return $query;
        }
        else
        {
            return false;
        }


    }

    public function setOrderRequestDetailStatus($orderRequestDetailId, $status){

        if($orderRequestDetailId != '' && $status != '' ){

            try {
                $q = Doctrine_Query::create()
                ->update('OrderRequestDetails')
                ->set('payment_status',$status)
                ->where('id =?',$orderRequestDetailId);
                $res = $q->execute();
                if($res){
                    return true;
                }else{
                    return false;
                }
            }
            catch (Exception $e) {
                throw New Exception ($e->getMessage());
            }
        }else{
            return false;
        }
    }

}