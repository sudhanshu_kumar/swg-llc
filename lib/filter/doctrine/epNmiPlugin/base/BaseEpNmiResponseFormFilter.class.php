<?php

/**
 * EpNmiResponse filter form base class.
 *
 * @package    ama
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseEpNmiResponseFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'nmi_req_id'         => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('EpNmiRequest'), 'add_empty' => true)),
      'token_id'           => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'order_id'           => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'result'             => new sfWidgetFormFilterInput(),
      'result_text'        => new sfWidgetFormFilterInput(),
      'transaction_id'     => new sfWidgetFormFilterInput(),
      'result_code'        => new sfWidgetFormFilterInput(),
      'authorization_code' => new sfWidgetFormFilterInput(),
      'avs_result'         => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'action_type'        => new sfWidgetFormFilterInput(),
      'amount'             => new sfWidgetFormFilterInput(),
      'ip_address'         => new sfWidgetFormFilterInput(),
      'industry'           => new sfWidgetFormFilterInput(),
      'city'               => new sfWidgetFormFilterInput(),
      'processor_id'       => new sfWidgetFormFilterInput(),
      'currency'           => new sfWidgetFormFilterInput(),
      'tax_amount'         => new sfWidgetFormFilterInput(),
      'shipping_amount'    => new sfWidgetFormFilterInput(),
      'billing'            => new sfWidgetFormFilterInput(),
      'first_name'         => new sfWidgetFormFilterInput(),
      'last_name'          => new sfWidgetFormFilterInput(),
      'email'              => new sfWidgetFormFilterInput(),
      'phone'              => new sfWidgetFormFilterInput(),
      'address1'           => new sfWidgetFormFilterInput(),
      'address2'           => new sfWidgetFormFilterInput(),
      'town'               => new sfWidgetFormFilterInput(),
      'postal'             => new sfWidgetFormFilterInput(),
      'country'            => new sfWidgetFormFilterInput(),
      'state'              => new sfWidgetFormFilterInput(),
      'cc_number'          => new sfWidgetFormFilterInput(),
      'cc_exp'             => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'created_at'         => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'         => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'created_by'         => new sfWidgetFormFilterInput(),
      'updated_by'         => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'nmi_req_id'         => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('EpNmiRequest'), 'column' => 'id')),
      'token_id'           => new sfValidatorPass(array('required' => false)),
      'order_id'           => new sfValidatorPass(array('required' => false)),
      'result'             => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'result_text'        => new sfValidatorPass(array('required' => false)),
      'transaction_id'     => new sfValidatorPass(array('required' => false)),
      'result_code'        => new sfValidatorPass(array('required' => false)),
      'authorization_code' => new sfValidatorPass(array('required' => false)),
      'avs_result'         => new sfValidatorPass(array('required' => false)),
      'action_type'        => new sfValidatorPass(array('required' => false)),
      'amount'             => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'ip_address'         => new sfValidatorPass(array('required' => false)),
      'industry'           => new sfValidatorPass(array('required' => false)),
      'city'               => new sfValidatorPass(array('required' => false)),
      'processor_id'       => new sfValidatorPass(array('required' => false)),
      'currency'           => new sfValidatorPass(array('required' => false)),
      'tax_amount'         => new sfValidatorPass(array('required' => false)),
      'shipping_amount'    => new sfValidatorPass(array('required' => false)),
      'billing'            => new sfValidatorPass(array('required' => false)),
      'first_name'         => new sfValidatorPass(array('required' => false)),
      'last_name'          => new sfValidatorPass(array('required' => false)),
      'email'              => new sfValidatorPass(array('required' => false)),
      'phone'              => new sfValidatorPass(array('required' => false)),
      'address1'           => new sfValidatorPass(array('required' => false)),
      'address2'           => new sfValidatorPass(array('required' => false)),
      'town'               => new sfValidatorPass(array('required' => false)),
      'postal'             => new sfValidatorPass(array('required' => false)),
      'country'            => new sfValidatorPass(array('required' => false)),
      'state'              => new sfValidatorPass(array('required' => false)),
      'cc_number'          => new sfValidatorPass(array('required' => false)),
      'cc_exp'             => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'created_at'         => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'         => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'created_by'         => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'updated_by'         => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('ep_nmi_response_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'EpNmiResponse';
  }

  public function getFields()
  {
    return array(
      'id'                 => 'Number',
      'nmi_req_id'         => 'ForeignKey',
      'token_id'           => 'Text',
      'order_id'           => 'Text',
      'result'             => 'Number',
      'result_text'        => 'Text',
      'transaction_id'     => 'Text',
      'result_code'        => 'Text',
      'authorization_code' => 'Text',
      'avs_result'         => 'Text',
      'action_type'        => 'Text',
      'amount'             => 'Number',
      'ip_address'         => 'Text',
      'industry'           => 'Text',
      'city'               => 'Text',
      'processor_id'       => 'Text',
      'currency'           => 'Text',
      'tax_amount'         => 'Text',
      'shipping_amount'    => 'Text',
      'billing'            => 'Text',
      'first_name'         => 'Text',
      'last_name'          => 'Text',
      'email'              => 'Text',
      'phone'              => 'Text',
      'address1'           => 'Text',
      'address2'           => 'Text',
      'town'               => 'Text',
      'postal'             => 'Text',
      'country'            => 'Text',
      'state'              => 'Text',
      'cc_number'          => 'Text',
      'cc_exp'             => 'Date',
      'created_at'         => 'Date',
      'updated_at'         => 'Date',
      'created_by'         => 'Number',
      'updated_by'         => 'Number',
    );
  }
}
