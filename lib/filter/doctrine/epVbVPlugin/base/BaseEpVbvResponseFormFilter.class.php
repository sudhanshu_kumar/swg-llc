<?php

/**
 * EpVbvResponse filter form base class.
 *
 * @package    ama
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseEpVbvResponseFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'vbvrequest_id'        => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('EpVbvRequest'), 'add_empty' => true)),
      'msg_date'             => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'version'              => new sfWidgetFormFilterInput(),
      'order_id'             => new sfWidgetFormFilterInput(),
      'transaction_type'     => new sfWidgetFormFilterInput(),
      'pan'                  => new sfWidgetFormFilterInput(),
      'purchase_amount'      => new sfWidgetFormFilterInput(),
      'currency'             => new sfWidgetFormFilterInput(),
      'tran_date_time'       => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'response_code'        => new sfWidgetFormFilterInput(),
      'response_description' => new sfWidgetFormFilterInput(),
      'order_status'         => new sfWidgetFormFilterInput(),
      'approval_code'        => new sfWidgetFormFilterInput(),
      'order_description'    => new sfWidgetFormFilterInput(),
      'approval_code_scr'    => new sfWidgetFormFilterInput(),
      'purchase_amount_scr'  => new sfWidgetFormFilterInput(),
      'currency_scr'         => new sfWidgetFormFilterInput(),
      'order_status_scr'     => new sfWidgetFormFilterInput(),
      'shop_order_id'        => new sfWidgetFormFilterInput(),
      'three_ds_verificaion' => new sfWidgetFormFilterInput(),
      'response_xml'         => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'created_at'           => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'           => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
    ));

    $this->setValidators(array(
      'vbvrequest_id'        => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('EpVbvRequest'), 'column' => 'id')),
      'msg_date'             => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'version'              => new sfValidatorPass(array('required' => false)),
      'order_id'             => new sfValidatorPass(array('required' => false)),
      'transaction_type'     => new sfValidatorPass(array('required' => false)),
      'pan'                  => new sfValidatorPass(array('required' => false)),
      'purchase_amount'      => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'currency'             => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'tran_date_time'       => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'response_code'        => new sfValidatorPass(array('required' => false)),
      'response_description' => new sfValidatorPass(array('required' => false)),
      'order_status'         => new sfValidatorPass(array('required' => false)),
      'approval_code'        => new sfValidatorPass(array('required' => false)),
      'order_description'    => new sfValidatorPass(array('required' => false)),
      'approval_code_scr'    => new sfValidatorPass(array('required' => false)),
      'purchase_amount_scr'  => new sfValidatorPass(array('required' => false)),
      'currency_scr'         => new sfValidatorPass(array('required' => false)),
      'order_status_scr'     => new sfValidatorPass(array('required' => false)),
      'shop_order_id'        => new sfValidatorPass(array('required' => false)),
      'three_ds_verificaion' => new sfValidatorPass(array('required' => false)),
      'response_xml'         => new sfValidatorPass(array('required' => false)),
      'created_at'           => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'           => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
    ));

    $this->widgetSchema->setNameFormat('ep_vbv_response_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'EpVbvResponse';
  }

  public function getFields()
  {
    return array(
      'id'                   => 'Number',
      'vbvrequest_id'        => 'ForeignKey',
      'msg_date'             => 'Date',
      'version'              => 'Text',
      'order_id'             => 'Text',
      'transaction_type'     => 'Text',
      'pan'                  => 'Text',
      'purchase_amount'      => 'Number',
      'currency'             => 'Number',
      'tran_date_time'       => 'Date',
      'response_code'        => 'Text',
      'response_description' => 'Text',
      'order_status'         => 'Text',
      'approval_code'        => 'Text',
      'order_description'    => 'Text',
      'approval_code_scr'    => 'Text',
      'purchase_amount_scr'  => 'Text',
      'currency_scr'         => 'Text',
      'order_status_scr'     => 'Text',
      'shop_order_id'        => 'Text',
      'three_ds_verificaion' => 'Text',
      'response_xml'         => 'Text',
      'created_at'           => 'Date',
      'updated_at'           => 'Date',
    );
  }
}
