<?php

/**
 * EpGrayPayRequest filter form base class.
 *
 * @package    ama
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseEpGrayPayRequestFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'gateway_id'    => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'tran_date'     => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'request_ip'    => new sfWidgetFormFilterInput(),
      'amount'        => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'card_first'    => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'card_last'     => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'card_len'      => new sfWidgetFormFilterInput(),
      'card_type'     => new sfWidgetFormFilterInput(),
      'cvv'           => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'card_holder'   => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'expiry_month'  => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'expiry_year'   => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'address1'      => new sfWidgetFormFilterInput(),
      'address2'      => new sfWidgetFormFilterInput(),
      'city'          => new sfWidgetFormFilterInput(),
      'state'         => new sfWidgetFormFilterInput(),
      'zip'           => new sfWidgetFormFilterInput(),
      'country'       => new sfWidgetFormFilterInput(),
      'first_name'    => new sfWidgetFormFilterInput(),
      'last_name'     => new sfWidgetFormFilterInput(),
      'email'         => new sfWidgetFormFilterInput(),
      'phone'         => new sfWidgetFormFilterInput(),
      'order_number'  => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'ip_address'    => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'transactionid' => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'type'          => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'created_at'    => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'    => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
    ));

    $this->setValidators(array(
      'gateway_id'    => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'tran_date'     => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'request_ip'    => new sfValidatorPass(array('required' => false)),
      'amount'        => new sfValidatorPass(array('required' => false)),
      'card_first'    => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'card_last'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'card_len'      => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'card_type'     => new sfValidatorPass(array('required' => false)),
      'cvv'           => new sfValidatorPass(array('required' => false)),
      'card_holder'   => new sfValidatorPass(array('required' => false)),
      'expiry_month'  => new sfValidatorPass(array('required' => false)),
      'expiry_year'   => new sfValidatorPass(array('required' => false)),
      'address1'      => new sfValidatorPass(array('required' => false)),
      'address2'      => new sfValidatorPass(array('required' => false)),
      'city'          => new sfValidatorPass(array('required' => false)),
      'state'         => new sfValidatorPass(array('required' => false)),
      'zip'           => new sfValidatorPass(array('required' => false)),
      'country'       => new sfValidatorPass(array('required' => false)),
      'first_name'    => new sfValidatorPass(array('required' => false)),
      'last_name'     => new sfValidatorPass(array('required' => false)),
      'email'         => new sfValidatorPass(array('required' => false)),
      'phone'         => new sfValidatorPass(array('required' => false)),
      'order_number'  => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'ip_address'    => new sfValidatorPass(array('required' => false)),
      'transactionid' => new sfValidatorPass(array('required' => false)),
      'type'          => new sfValidatorPass(array('required' => false)),
      'created_at'    => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'    => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
    ));

    $this->widgetSchema->setNameFormat('ep_gray_pay_request_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'EpGrayPayRequest';
  }

  public function getFields()
  {
    return array(
      'id'            => 'Number',
      'gateway_id'    => 'Number',
      'tran_date'     => 'Date',
      'request_ip'    => 'Text',
      'amount'        => 'Text',
      'card_first'    => 'Number',
      'card_last'     => 'Number',
      'card_len'      => 'Number',
      'card_type'     => 'Text',
      'cvv'           => 'Text',
      'card_holder'   => 'Text',
      'expiry_month'  => 'Text',
      'expiry_year'   => 'Text',
      'address1'      => 'Text',
      'address2'      => 'Text',
      'city'          => 'Text',
      'state'         => 'Text',
      'zip'           => 'Text',
      'country'       => 'Text',
      'first_name'    => 'Text',
      'last_name'     => 'Text',
      'email'         => 'Text',
      'phone'         => 'Text',
      'order_number'  => 'Number',
      'ip_address'    => 'Text',
      'transactionid' => 'Text',
      'type'          => 'Text',
      'created_at'    => 'Date',
      'updated_at'    => 'Date',
    );
  }
}
