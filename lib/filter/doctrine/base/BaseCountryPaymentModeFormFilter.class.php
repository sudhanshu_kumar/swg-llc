<?php

/**
 * CountryPaymentMode filter form base class.
 *
 * @package    ama
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseCountryPaymentModeFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'country_code'          => new sfWidgetFormFilterInput(),
      'card_type'             => new sfWidgetFormFilterInput(),
      'service'               => new sfWidgetFormFilterInput(),
      'validation'            => new sfWidgetFormFilterInput(),
      'cart_capacity'         => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'cart_amount_capacity'  => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'number_of_transaction' => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'transaction_period'    => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'created_at'            => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'            => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'created_by'            => new sfWidgetFormFilterInput(),
      'updated_by'            => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'country_code'          => new sfValidatorPass(array('required' => false)),
      'card_type'             => new sfValidatorPass(array('required' => false)),
      'service'               => new sfValidatorPass(array('required' => false)),
      'validation'            => new sfValidatorPass(array('required' => false)),
      'cart_capacity'         => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'cart_amount_capacity'  => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'number_of_transaction' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'transaction_period'    => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'created_at'            => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'            => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'created_by'            => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'updated_by'            => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('country_payment_mode_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CountryPaymentMode';
  }

  public function getFields()
  {
    return array(
      'id'                    => 'Number',
      'country_code'          => 'Text',
      'card_type'             => 'Text',
      'service'               => 'Text',
      'validation'            => 'Text',
      'cart_capacity'         => 'Number',
      'cart_amount_capacity'  => 'Number',
      'number_of_transaction' => 'Number',
      'transaction_period'    => 'Number',
      'created_at'            => 'Date',
      'updated_at'            => 'Date',
      'created_by'            => 'Number',
      'updated_by'            => 'Number',
    );
  }
}
