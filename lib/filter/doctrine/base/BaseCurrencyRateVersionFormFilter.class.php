<?php

/**
 * CurrencyRateVersion filter form base class.
 *
 * @package    ama
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseCurrencyRateVersionFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'country_id'                 => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'currency'                   => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'passport'                   => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'visa'                       => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'visa_arrival'               => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'additional_charges_for_vap' => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'created_at'                 => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'                 => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'created_by'                 => new sfWidgetFormFilterInput(),
      'updated_by'                 => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'country_id'                 => new sfValidatorPass(array('required' => false)),
      'currency'                   => new sfValidatorPass(array('required' => false)),
      'passport'                   => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'visa'                       => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'visa_arrival'               => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'additional_charges_for_vap' => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'created_at'                 => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'                 => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'created_by'                 => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'updated_by'                 => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('currency_rate_version_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CurrencyRateVersion';
  }

  public function getFields()
  {
    return array(
      'id'                         => 'Number',
      'country_id'                 => 'Text',
      'currency'                   => 'Text',
      'passport'                   => 'Number',
      'visa'                       => 'Number',
      'visa_arrival'               => 'Number',
      'additional_charges_for_vap' => 'Number',
      'created_at'                 => 'Date',
      'updated_at'                 => 'Date',
      'created_by'                 => 'Number',
      'updated_by'                 => 'Number',
      'version'                    => 'Number',
    );
  }
}
