<?php

/**
 * MidMaster filter form base class.
 *
 * @package    ama
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseMidMasterFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'mid_service'         => new sfWidgetFormFilterInput(),
      'validation'          => new sfWidgetFormFilterInput(),
      'visa_amount_limit'   => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'master_amount_limit' => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'cart_amount_limit'   => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'created_at'          => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'          => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'created_by'          => new sfWidgetFormFilterInput(),
      'updated_by'          => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'mid_service'         => new sfValidatorPass(array('required' => false)),
      'validation'          => new sfValidatorPass(array('required' => false)),
      'visa_amount_limit'   => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'master_amount_limit' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'cart_amount_limit'   => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'created_at'          => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'          => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'created_by'          => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'updated_by'          => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('mid_master_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'MidMaster';
  }

  public function getFields()
  {
    return array(
      'id'                  => 'Number',
      'mid_service'         => 'Text',
      'validation'          => 'Text',
      'visa_amount_limit'   => 'Number',
      'master_amount_limit' => 'Number',
      'cart_amount_limit'   => 'Number',
      'created_at'          => 'Date',
      'updated_at'          => 'Date',
      'created_by'          => 'Number',
      'updated_by'          => 'Number',
    );
  }
}
