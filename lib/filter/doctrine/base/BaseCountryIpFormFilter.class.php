<?php

/**
 * CountryIp filter form base class.
 *
 * @package    ama
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseCountryIpFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'ipfrom'   => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'ipto'     => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'registry' => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'assigned' => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'iso2'     => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'country'  => new sfWidgetFormFilterInput(array('with_empty' => false)),
    ));

    $this->setValidators(array(
      'ipfrom'   => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'ipto'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'registry' => new sfValidatorPass(array('required' => false)),
      'assigned' => new sfValidatorPass(array('required' => false)),
      'iso2'     => new sfValidatorPass(array('required' => false)),
      'country'  => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('country_ip_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CountryIp';
  }

  public function getFields()
  {
    return array(
      'id'       => 'Number',
      'ipfrom'   => 'Number',
      'ipto'     => 'Number',
      'registry' => 'Text',
      'assigned' => 'Text',
      'iso2'     => 'Text',
      'country'  => 'Text',
    );
  }
}
