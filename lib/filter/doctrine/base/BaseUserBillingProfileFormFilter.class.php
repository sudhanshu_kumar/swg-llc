<?php

/**
 * UserBillingProfile filter form base class.
 *
 * @package    ama
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseUserBillingProfileFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'user_id'      => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'profile'      => new sfWidgetFormChoice(array('choices' => array('' => '', 'p1' => 'p1', 'p2' => 'p2', 'p3' => 'p3', 'p4' => 'p4', 'p5' => 'p5'))),
      'gateway_id'   => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'first_name'   => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'last_name'    => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'address'      => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'email'        => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'phone'        => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'card_first'   => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'card_last'    => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'card_len'     => new sfWidgetFormFilterInput(),
      'card_type'    => new sfWidgetFormFilterInput(),
      'expiry_month' => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'expiry_year'  => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'vault_id'     => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'created_at'   => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'   => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
    ));

    $this->setValidators(array(
      'user_id'      => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'profile'      => new sfValidatorChoice(array('required' => false, 'choices' => array('p1' => 'p1', 'p2' => 'p2', 'p3' => 'p3', 'p4' => 'p4', 'p5' => 'p5'))),
      'gateway_id'   => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'first_name'   => new sfValidatorPass(array('required' => false)),
      'last_name'    => new sfValidatorPass(array('required' => false)),
      'address'      => new sfValidatorPass(array('required' => false)),
      'email'        => new sfValidatorPass(array('required' => false)),
      'phone'        => new sfValidatorPass(array('required' => false)),
      'card_first'   => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'card_last'    => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'card_len'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'card_type'    => new sfValidatorPass(array('required' => false)),
      'expiry_month' => new sfValidatorPass(array('required' => false)),
      'expiry_year'  => new sfValidatorPass(array('required' => false)),
      'vault_id'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'created_at'   => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'   => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
    ));

    $this->widgetSchema->setNameFormat('user_billing_profile_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'UserBillingProfile';
  }

  public function getFields()
  {
    return array(
      'id'           => 'Number',
      'user_id'      => 'Number',
      'profile'      => 'Enum',
      'gateway_id'   => 'Number',
      'first_name'   => 'Text',
      'last_name'    => 'Text',
      'address'      => 'Text',
      'email'        => 'Text',
      'phone'        => 'Text',
      'card_first'   => 'Number',
      'card_last'    => 'Number',
      'card_len'     => 'Number',
      'card_type'    => 'Text',
      'expiry_month' => 'Text',
      'expiry_year'  => 'Text',
      'vault_id'     => 'Number',
      'created_at'   => 'Date',
      'updated_at'   => 'Date',
    );
  }
}
