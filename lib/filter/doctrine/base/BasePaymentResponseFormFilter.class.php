<?php

/**
 * PaymentResponse filter form base class.
 *
 * @package    ama
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BasePaymentResponseFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'request_order_number'        => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Request'), 'add_empty' => true)),
      'response_code'               => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'response_reason'             => new sfWidgetFormFilterInput(),
      'response_reason_txt'         => new sfWidgetFormFilterInput(),
      'response_reason_sub_code'    => new sfWidgetFormFilterInput(),
      'response_transaction_code'   => new sfWidgetFormFilterInput(),
      'response_authorization_code' => new sfWidgetFormFilterInput(),
      'response_transaction_date'   => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'created_at'                  => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'                  => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'deleted'                     => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'created_by'                  => new sfWidgetFormFilterInput(),
      'updated_by'                  => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'request_order_number'        => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Request'), 'column' => 'id')),
      'response_code'               => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'response_reason'             => new sfValidatorPass(array('required' => false)),
      'response_reason_txt'         => new sfValidatorPass(array('required' => false)),
      'response_reason_sub_code'    => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'response_transaction_code'   => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'response_authorization_code' => new sfValidatorPass(array('required' => false)),
      'response_transaction_date'   => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'created_at'                  => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'                  => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'deleted'                     => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'created_by'                  => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'updated_by'                  => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('payment_response_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'PaymentResponse';
  }

  public function getFields()
  {
    return array(
      'id'                          => 'Number',
      'request_order_number'        => 'ForeignKey',
      'response_code'               => 'Number',
      'response_reason'             => 'Text',
      'response_reason_txt'         => 'Text',
      'response_reason_sub_code'    => 'Number',
      'response_transaction_code'   => 'Number',
      'response_authorization_code' => 'Text',
      'response_transaction_date'   => 'Date',
      'created_at'                  => 'Date',
      'updated_at'                  => 'Date',
      'deleted'                     => 'Boolean',
      'created_by'                  => 'Number',
      'updated_by'                  => 'Number',
    );
  }
}
