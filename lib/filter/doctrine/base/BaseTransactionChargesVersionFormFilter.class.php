<?php

/**
 * TransactionChargesVersion filter form base class.
 *
 * @package    ama
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseTransactionChargesVersionFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'gateway_id'          => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'transaction_charges' => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'split_type'          => new sfWidgetFormChoice(array('choices' => array('' => '', 'flat' => 'flat', 'percentage' => 'percentage'))),
      'created_at'          => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'          => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'deleted'             => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'created_by'          => new sfWidgetFormFilterInput(),
      'updated_by'          => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'gateway_id'          => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'transaction_charges' => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'split_type'          => new sfValidatorChoice(array('required' => false, 'choices' => array('flat' => 'flat', 'percentage' => 'percentage'))),
      'created_at'          => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'          => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'deleted'             => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'created_by'          => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'updated_by'          => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('transaction_charges_version_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'TransactionChargesVersion';
  }

  public function getFields()
  {
    return array(
      'id'                  => 'Number',
      'gateway_id'          => 'Number',
      'transaction_charges' => 'Number',
      'split_type'          => 'Enum',
      'created_at'          => 'Date',
      'updated_at'          => 'Date',
      'deleted'             => 'Boolean',
      'created_by'          => 'Number',
      'updated_by'          => 'Number',
      'version'             => 'Number',
    );
  }
}
