<?php

/**
 * FpsDetail filter form base class.
 *
 * @package    ama
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseFpsDetailFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'order_number'            => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'order_request_detail_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('OrderRequestDetails'), 'add_empty' => true)),
      'gateway_id'              => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Gateway'), 'add_empty' => true)),
      'ip_address'              => new sfWidgetFormFilterInput(),
      'user_id'                 => new sfWidgetFormFilterInput(),
      'card_num'                => new sfWidgetFormFilterInput(),
      'email'                   => new sfWidgetFormFilterInput(),
      'trans_date'              => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'payment_status'          => new sfWidgetFormChoice(array('choices' => array('' => '', 0 => 0, 1 => 1))),
      'fps_rule_id'             => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'isverified'              => new sfWidgetFormChoice(array('choices' => array('' => '', 'no' => 'no', 'yes' => 'yes'))),
      'created_at'              => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'              => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'deleted'                 => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'created_by'              => new sfWidgetFormFilterInput(),
      'updated_by'              => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'order_number'            => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'order_request_detail_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('OrderRequestDetails'), 'column' => 'id')),
      'gateway_id'              => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Gateway'), 'column' => 'id')),
      'ip_address'              => new sfValidatorPass(array('required' => false)),
      'user_id'                 => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'card_num'                => new sfValidatorPass(array('required' => false)),
      'email'                   => new sfValidatorPass(array('required' => false)),
      'trans_date'              => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'payment_status'          => new sfValidatorChoice(array('required' => false, 'choices' => array(0 => 0, 1 => 1))),
      'fps_rule_id'             => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'isverified'              => new sfValidatorChoice(array('required' => false, 'choices' => array('no' => 'no', 'yes' => 'yes'))),
      'created_at'              => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'              => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'deleted'                 => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'created_by'              => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'updated_by'              => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('fps_detail_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'FpsDetail';
  }

  public function getFields()
  {
    return array(
      'id'                      => 'Number',
      'order_number'            => 'Number',
      'order_request_detail_id' => 'ForeignKey',
      'gateway_id'              => 'ForeignKey',
      'ip_address'              => 'Text',
      'user_id'                 => 'Number',
      'card_num'                => 'Text',
      'email'                   => 'Text',
      'trans_date'              => 'Date',
      'payment_status'          => 'Enum',
      'fps_rule_id'             => 'Number',
      'isverified'              => 'Enum',
      'created_at'              => 'Date',
      'updated_at'              => 'Date',
      'deleted'                 => 'Boolean',
      'created_by'              => 'Number',
      'updated_by'              => 'Number',
    );
  }
}
