<?php

/**
 * ApplicantVault filter form base class.
 *
 * @package    ama
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseApplicantVaultFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'card_first'       => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'card_last'        => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'card_hash'        => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'card_len'         => new sfWidgetFormFilterInput(),
      'card_type'        => new sfWidgetFormFilterInput(),
      'card_holder'      => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'expiry_month'     => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'expiry_year'      => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'cvv'              => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'address1'         => new sfWidgetFormFilterInput(),
      'address2'         => new sfWidgetFormFilterInput(),
      'town'             => new sfWidgetFormFilterInput(),
      'state'            => new sfWidgetFormFilterInput(),
      'zip'              => new sfWidgetFormFilterInput(),
      'country'          => new sfWidgetFormFilterInput(),
      'first_name'       => new sfWidgetFormFilterInput(),
      'last_name'        => new sfWidgetFormFilterInput(),
      'email'            => new sfWidgetFormFilterInput(),
      'phone'            => new sfWidgetFormFilterInput(),
      'address_proof'    => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'user_id_proof'    => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'failed_attempts'  => new sfWidgetFormFilterInput(),
      'status'           => new sfWidgetFormChoice(array('choices' => array('' => '', 'New' => 'New', 'Approved' => 'Approved', 'Reject' => 'Reject'))),
      'approver_id'      => new sfWidgetFormFilterInput(),
      'comments'         => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'transaction_type' => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'reason'           => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'agent_proof'      => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'user_id'          => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('sfGuardUser'), 'add_empty' => true)),
      'created_at'       => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'       => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'created_by'       => new sfWidgetFormFilterInput(),
      'updated_by'       => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'card_first'       => new sfValidatorPass(array('required' => false)),
      'card_last'        => new sfValidatorPass(array('required' => false)),
      'card_hash'        => new sfValidatorPass(array('required' => false)),
      'card_len'         => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'card_type'        => new sfValidatorPass(array('required' => false)),
      'card_holder'      => new sfValidatorPass(array('required' => false)),
      'expiry_month'     => new sfValidatorPass(array('required' => false)),
      'expiry_year'      => new sfValidatorPass(array('required' => false)),
      'cvv'              => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'address1'         => new sfValidatorPass(array('required' => false)),
      'address2'         => new sfValidatorPass(array('required' => false)),
      'town'             => new sfValidatorPass(array('required' => false)),
      'state'            => new sfValidatorPass(array('required' => false)),
      'zip'              => new sfValidatorPass(array('required' => false)),
      'country'          => new sfValidatorPass(array('required' => false)),
      'first_name'       => new sfValidatorPass(array('required' => false)),
      'last_name'        => new sfValidatorPass(array('required' => false)),
      'email'            => new sfValidatorPass(array('required' => false)),
      'phone'            => new sfValidatorPass(array('required' => false)),
      'address_proof'    => new sfValidatorPass(array('required' => false)),
      'user_id_proof'    => new sfValidatorPass(array('required' => false)),
      'failed_attempts'  => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'status'           => new sfValidatorChoice(array('required' => false, 'choices' => array('New' => 'New', 'Approved' => 'Approved', 'Reject' => 'Reject'))),
      'approver_id'      => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'comments'         => new sfValidatorPass(array('required' => false)),
      'transaction_type' => new sfValidatorPass(array('required' => false)),
      'reason'           => new sfValidatorPass(array('required' => false)),
      'agent_proof'      => new sfValidatorPass(array('required' => false)),
      'user_id'          => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('sfGuardUser'), 'column' => 'id')),
      'created_at'       => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'       => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'created_by'       => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'updated_by'       => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('applicant_vault_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'ApplicantVault';
  }

  public function getFields()
  {
    return array(
      'id'               => 'Number',
      'card_first'       => 'Text',
      'card_last'        => 'Text',
      'card_hash'        => 'Text',
      'card_len'         => 'Number',
      'card_type'        => 'Text',
      'card_holder'      => 'Text',
      'expiry_month'     => 'Text',
      'expiry_year'      => 'Text',
      'cvv'              => 'Number',
      'address1'         => 'Text',
      'address2'         => 'Text',
      'town'             => 'Text',
      'state'            => 'Text',
      'zip'              => 'Text',
      'country'          => 'Text',
      'first_name'       => 'Text',
      'last_name'        => 'Text',
      'email'            => 'Text',
      'phone'            => 'Text',
      'address_proof'    => 'Text',
      'user_id_proof'    => 'Text',
      'failed_attempts'  => 'Number',
      'status'           => 'Enum',
      'approver_id'      => 'Number',
      'comments'         => 'Text',
      'transaction_type' => 'Text',
      'reason'           => 'Text',
      'agent_proof'      => 'Text',
      'user_id'          => 'ForeignKey',
      'created_at'       => 'Date',
      'updated_at'       => 'Date',
      'created_by'       => 'Number',
      'updated_by'       => 'Number',
    );
  }
}
