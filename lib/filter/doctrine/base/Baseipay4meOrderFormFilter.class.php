<?php

/**
 * ipay4meOrder filter form base class.
 *
 * @package    ama
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class Baseipay4meOrderFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'order_number'            => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('OrderAuthorize'), 'add_empty' => true)),
      'validation_number'       => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'gateway_id'              => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Gateway'), 'add_empty' => true)),
      'order_request_detail_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('OrderRequestDetails'), 'add_empty' => true)),
      'payment_status'          => new sfWidgetFormChoice(array('choices' => array('' => '', 0 => 0, 1 => 1))),
      'processing_country'      => new sfWidgetFormFilterInput(),
      'response_code'           => new sfWidgetFormFilterInput(),
      'response_text'           => new sfWidgetFormFilterInput(),
      'payor_name'              => new sfWidgetFormFilterInput(),
      'card_holder'             => new sfWidgetFormFilterInput(),
      'card_first'              => new sfWidgetFormFilterInput(),
      'card_last'               => new sfWidgetFormFilterInput(),
      'card_hash'               => new sfWidgetFormFilterInput(),
      'card_len'                => new sfWidgetFormFilterInput(),
      'card_type'               => new sfWidgetFormFilterInput(),
      'address'                 => new sfWidgetFormFilterInput(),
      'phone'                   => new sfWidgetFormFilterInput(),
      'jobId'                   => new sfWidgetFormFilterInput(),
      'created_at'              => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'              => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'created_by'              => new sfWidgetFormFilterInput(),
      'updated_by'              => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'order_number'            => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('OrderAuthorize'), 'column' => 'id')),
      'validation_number'       => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'gateway_id'              => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Gateway'), 'column' => 'id')),
      'order_request_detail_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('OrderRequestDetails'), 'column' => 'id')),
      'payment_status'          => new sfValidatorChoice(array('required' => false, 'choices' => array(0 => 0, 1 => 1))),
      'processing_country'      => new sfValidatorPass(array('required' => false)),
      'response_code'           => new sfValidatorPass(array('required' => false)),
      'response_text'           => new sfValidatorPass(array('required' => false)),
      'payor_name'              => new sfValidatorPass(array('required' => false)),
      'card_holder'             => new sfValidatorPass(array('required' => false)),
      'card_first'              => new sfValidatorPass(array('required' => false)),
      'card_last'               => new sfValidatorPass(array('required' => false)),
      'card_hash'               => new sfValidatorPass(array('required' => false)),
      'card_len'                => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'card_type'               => new sfValidatorPass(array('required' => false)),
      'address'                 => new sfValidatorPass(array('required' => false)),
      'phone'                   => new sfValidatorPass(array('required' => false)),
      'jobId'                   => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'created_at'              => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'              => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'created_by'              => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'updated_by'              => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('ipay4me_order_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'ipay4meOrder';
  }

  public function getFields()
  {
    return array(
      'id'                      => 'Number',
      'order_number'            => 'ForeignKey',
      'validation_number'       => 'Number',
      'gateway_id'              => 'ForeignKey',
      'order_request_detail_id' => 'ForeignKey',
      'payment_status'          => 'Enum',
      'processing_country'      => 'Text',
      'response_code'           => 'Text',
      'response_text'           => 'Text',
      'payor_name'              => 'Text',
      'card_holder'             => 'Text',
      'card_first'              => 'Text',
      'card_last'               => 'Text',
      'card_hash'               => 'Text',
      'card_len'                => 'Number',
      'card_type'               => 'Text',
      'address'                 => 'Text',
      'phone'                   => 'Text',
      'jobId'                   => 'Number',
      'created_at'              => 'Date',
      'updated_at'              => 'Date',
      'created_by'              => 'Number',
      'updated_by'              => 'Number',
    );
  }
}
