<?php

/**
 * Moneyorder filter form base class.
 *
 * @package    ama
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseMoneyorderFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'moneyorder_number'  => new sfWidgetFormFilterInput(),
      'amount'             => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'convert_amount'     => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'currency'           => new sfWidgetFormFilterInput(),
      'moneyorder_date'    => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'paid_date'          => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'address'            => new sfWidgetFormFilterInput(),
      'phone'              => new sfWidgetFormFilterInput(),
      'moneyorder_office'  => new sfWidgetFormFilterInput(),
      'comments'           => new sfWidgetFormFilterInput(),
      'courier_flag'       => new sfWidgetFormChoice(array('choices' => array('' => '', 'Yes' => 'Yes', 'No' => 'No'))),
      'courier_service'    => new sfWidgetFormFilterInput(),
      'waybill_trackingno' => new sfWidgetFormFilterInput(),
      'status'             => new sfWidgetFormChoice(array('choices' => array('' => '', 'New' => 'New', 'Paid' => 'Paid'))),
      'created_at'         => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'         => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'created_by'         => new sfWidgetFormFilterInput(),
      'updated_by'         => new sfWidgetFormFilterInput(),
      'deleted'            => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
    ));

    $this->setValidators(array(
      'moneyorder_number'  => new sfValidatorPass(array('required' => false)),
      'amount'             => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'convert_amount'     => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'currency'           => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'moneyorder_date'    => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'paid_date'          => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'address'            => new sfValidatorPass(array('required' => false)),
      'phone'              => new sfValidatorPass(array('required' => false)),
      'moneyorder_office'  => new sfValidatorPass(array('required' => false)),
      'comments'           => new sfValidatorPass(array('required' => false)),
      'courier_flag'       => new sfValidatorChoice(array('required' => false, 'choices' => array('Yes' => 'Yes', 'No' => 'No'))),
      'courier_service'    => new sfValidatorPass(array('required' => false)),
      'waybill_trackingno' => new sfValidatorPass(array('required' => false)),
      'status'             => new sfValidatorChoice(array('required' => false, 'choices' => array('New' => 'New', 'Paid' => 'Paid'))),
      'created_at'         => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'         => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'created_by'         => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'updated_by'         => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'deleted'            => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
    ));

    $this->widgetSchema->setNameFormat('moneyorder_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Moneyorder';
  }

  public function getFields()
  {
    return array(
      'id'                 => 'Number',
      'moneyorder_number'  => 'Text',
      'amount'             => 'Number',
      'convert_amount'     => 'Number',
      'currency'           => 'Number',
      'moneyorder_date'    => 'Date',
      'paid_date'          => 'Date',
      'address'            => 'Text',
      'phone'              => 'Text',
      'moneyorder_office'  => 'Text',
      'comments'           => 'Text',
      'courier_flag'       => 'Enum',
      'courier_service'    => 'Text',
      'waybill_trackingno' => 'Text',
      'status'             => 'Enum',
      'created_at'         => 'Date',
      'updated_at'         => 'Date',
      'created_by'         => 'Number',
      'updated_by'         => 'Number',
      'deleted'            => 'Boolean',
    );
  }
}
