<?php

/**
 * RollbackPaymentDetails filter form base class.
 *
 * @package    ama
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseRollbackPaymentDetailsFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'rollback_payment_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('RollbackPayment'), 'add_empty' => true)),
      'app_id'              => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'app_type'            => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'amount'              => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'action'              => new sfWidgetFormChoice(array('choices' => array('' => '', 'refund' => 'refund', 'chargeback' => 'chargeback', 'refund and block' => 'refund and block'))),
      'status'              => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'created_at'          => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'          => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'created_by'          => new sfWidgetFormFilterInput(),
      'updated_by'          => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'rollback_payment_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('RollbackPayment'), 'column' => 'id')),
      'app_id'              => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'app_type'            => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'amount'              => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'action'              => new sfValidatorChoice(array('required' => false, 'choices' => array('refund' => 'refund', 'chargeback' => 'chargeback', 'refund and block' => 'refund and block'))),
      'status'              => new sfValidatorPass(array('required' => false)),
      'created_at'          => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'          => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'created_by'          => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'updated_by'          => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('rollback_payment_details_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'RollbackPaymentDetails';
  }

  public function getFields()
  {
    return array(
      'id'                  => 'Number',
      'rollback_payment_id' => 'ForeignKey',
      'app_id'              => 'Number',
      'app_type'            => 'Number',
      'amount'              => 'Number',
      'action'              => 'Enum',
      'status'              => 'Text',
      'created_at'          => 'Date',
      'updated_at'          => 'Date',
      'created_by'          => 'Number',
      'updated_by'          => 'Number',
    );
  }
}
