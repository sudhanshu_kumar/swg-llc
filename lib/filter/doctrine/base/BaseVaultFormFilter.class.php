<?php

/**
 * Vault filter form base class.
 *
 * @package    ama
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseVaultFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'gateway_id'     => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'vault_num'      => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'order_number'   => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'payment_status' => new sfWidgetFormChoice(array('choices' => array('' => '', 0 => 0, 1 => 1, 2 => 2, 3 => 3))),
    ));

    $this->setValidators(array(
      'gateway_id'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'vault_num'      => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'order_number'   => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'payment_status' => new sfValidatorChoice(array('required' => false, 'choices' => array(0 => 0, 1 => 1, 2 => 2, 3 => 3))),
    ));

    $this->widgetSchema->setNameFormat('vault_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Vault';
  }

  public function getFields()
  {
    return array(
      'id'             => 'Number',
      'gateway_id'     => 'Number',
      'vault_num'      => 'Number',
      'order_number'   => 'Number',
      'payment_status' => 'Enum',
    );
  }
}
