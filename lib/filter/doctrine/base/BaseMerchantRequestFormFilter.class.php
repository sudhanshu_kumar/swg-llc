<?php

/**
 * MerchantRequest filter form base class.
 *
 * @package    ama
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseMerchantRequestFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'txn_ref'                  => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'merchant_id'              => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Merchant'), 'add_empty' => true)),
      'merchant_service_id'      => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('MerchantService'), 'add_empty' => true)),
      'merchant_item_id'         => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('MerchantItem'), 'add_empty' => true)),
      'item_fee'                 => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'payment_status_code'      => new sfWidgetFormFilterInput(),
      'paid_amount'              => new sfWidgetFormFilterInput(),
      'paid_date'                => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'service_charge'           => new sfWidgetFormFilterInput(),
      'gateway_charge'           => new sfWidgetFormFilterInput(),
      'payforme_amount'          => new sfWidgetFormFilterInput(),
      'service_configuration_id' => new sfWidgetFormFilterInput(),
      'payment_mandate_id'       => new sfWidgetFormFilterInput(),
      'transaction_location'     => new sfWidgetFormFilterInput(),
      'validation_number'        => new sfWidgetFormFilterInput(),
      'created_by'               => new sfWidgetFormFilterInput(),
      'updated_by'               => new sfWidgetFormFilterInput(),
      'created_at'               => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'               => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'deleted'                  => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
    ));

    $this->setValidators(array(
      'txn_ref'                  => new sfValidatorPass(array('required' => false)),
      'merchant_id'              => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Merchant'), 'column' => 'id')),
      'merchant_service_id'      => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('MerchantService'), 'column' => 'id')),
      'merchant_item_id'         => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('MerchantItem'), 'column' => 'id')),
      'item_fee'                 => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'payment_status_code'      => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'paid_amount'              => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'paid_date'                => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'service_charge'           => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'gateway_charge'           => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'payforme_amount'          => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'service_configuration_id' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'payment_mandate_id'       => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'transaction_location'     => new sfValidatorPass(array('required' => false)),
      'validation_number'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'created_by'               => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'updated_by'               => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'created_at'               => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'               => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'deleted'                  => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
    ));

    $this->widgetSchema->setNameFormat('merchant_request_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'MerchantRequest';
  }

  public function getFields()
  {
    return array(
      'id'                       => 'Number',
      'txn_ref'                  => 'Text',
      'merchant_id'              => 'ForeignKey',
      'merchant_service_id'      => 'ForeignKey',
      'merchant_item_id'         => 'ForeignKey',
      'item_fee'                 => 'Number',
      'payment_status_code'      => 'Number',
      'paid_amount'              => 'Number',
      'paid_date'                => 'Date',
      'service_charge'           => 'Number',
      'gateway_charge'           => 'Number',
      'payforme_amount'          => 'Number',
      'service_configuration_id' => 'Number',
      'payment_mandate_id'       => 'Number',
      'transaction_location'     => 'Text',
      'validation_number'        => 'Number',
      'created_by'               => 'Number',
      'updated_by'               => 'Number',
      'created_at'               => 'Date',
      'updated_at'               => 'Date',
      'deleted'                  => 'Boolean',
    );
  }
}
