<?php

/**
 * Request filter form base class.
 *
 * @package    ama
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseRequestFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'order_detail_id'      => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('OrderRequestDetails'), 'add_empty' => true)),
      'request_order_number' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('PaymentResponse'), 'add_empty' => true)),
      'amount'               => new sfWidgetFormFilterInput(),
      'card_details_first'   => new sfWidgetFormFilterInput(),
      'card_details_last'    => new sfWidgetFormFilterInput(),
      'created_at'           => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'           => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'deleted'              => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'created_by'           => new sfWidgetFormFilterInput(),
      'updated_by'           => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'order_detail_id'      => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('OrderRequestDetails'), 'column' => 'id')),
      'request_order_number' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('PaymentResponse'), 'column' => 'id')),
      'amount'               => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'card_details_first'   => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'card_details_last'    => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'created_at'           => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'           => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'deleted'              => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'created_by'           => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'updated_by'           => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('request_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Request';
  }

  public function getFields()
  {
    return array(
      'id'                   => 'Number',
      'order_detail_id'      => 'ForeignKey',
      'request_order_number' => 'ForeignKey',
      'amount'               => 'Number',
      'card_details_first'   => 'Number',
      'card_details_last'    => 'Number',
      'created_at'           => 'Date',
      'updated_at'           => 'Date',
      'deleted'              => 'Boolean',
      'created_by'           => 'Number',
      'updated_by'           => 'Number',
    );
  }
}
