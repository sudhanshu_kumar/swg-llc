<?php

/**
 * CardCountryList filter form base class.
 *
 * @package    ama
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseCardCountryListFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'bin'           => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'processor_bin' => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'region'        => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'country'       => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'unknown'       => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'created_at'    => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'    => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
    ));

    $this->setValidators(array(
      'bin'           => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'processor_bin' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'region'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'country'       => new sfValidatorPass(array('required' => false)),
      'unknown'       => new sfValidatorPass(array('required' => false)),
      'created_at'    => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'    => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
    ));

    $this->widgetSchema->setNameFormat('card_country_list_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CardCountryList';
  }

  public function getFields()
  {
    return array(
      'id'            => 'Number',
      'bin'           => 'Number',
      'processor_bin' => 'Number',
      'region'        => 'Number',
      'country'       => 'Text',
      'unknown'       => 'Text',
      'created_at'    => 'Date',
      'updated_at'    => 'Date',
    );
  }
}
