<?php

/**
 * ReEntryVisaAddress filter form base class.
 *
 * @package    ama
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedInheritanceTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseReEntryVisaAddressFormFilter extends AddressMasterInterNationalFormFilter
{
  protected function setupInheritance()
  {
    parent::setupInheritance();

    $this->widgetSchema->setNameFormat('re_entry_visa_address_filters[%s]');
  }

  public function getModelName()
  {
    return 'ReEntryVisaAddress';
  }
}
