<?php

/**
 * IPaymentResponse filter form base class.
 *
 * @package    ama
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseIPaymentResponseFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'payment_request_id' => new sfWidgetFormFilterInput(),
      'transaction_number' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('CartItemsTransactions'), 'add_empty' => true)),
      'paid_amount'        => new sfWidgetFormFilterInput(),
      'currency'           => new sfWidgetFormFilterInput(),
      'payment_status'     => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'order_number'       => new sfWidgetFormFilterInput(),
      'created_at'         => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'         => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
    ));

    $this->setValidators(array(
      'payment_request_id' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'transaction_number' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('CartItemsTransactions'), 'column' => 'id')),
      'paid_amount'        => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'currency'           => new sfValidatorPass(array('required' => false)),
      'payment_status'     => new sfValidatorPass(array('required' => false)),
      'order_number'       => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'created_at'         => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'         => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
    ));

    $this->widgetSchema->setNameFormat('i_payment_response_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'IPaymentResponse';
  }

  public function getFields()
  {
    return array(
      'id'                 => 'Number',
      'payment_request_id' => 'Number',
      'transaction_number' => 'ForeignKey',
      'paid_amount'        => 'Number',
      'currency'           => 'Text',
      'payment_status'     => 'Text',
      'order_number'       => 'Number',
      'created_at'         => 'Date',
      'updated_at'         => 'Date',
    );
  }
}
