<?php

/**
 * VapProcessingCentre filter form base class.
 *
 * @package    ama
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedInheritanceTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseVapProcessingCentreFormFilter extends GlobalMasterFormFilter
{
  protected function setupInheritance()
  {
    parent::setupInheritance();

    $this->widgetSchema->setNameFormat('vap_processing_centre_filters[%s]');
  }

  public function getModelName()
  {
    return 'VapProcessingCentre';
  }
}
