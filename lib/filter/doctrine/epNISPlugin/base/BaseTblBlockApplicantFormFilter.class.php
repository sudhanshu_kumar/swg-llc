<?php

/**
 * TblBlockApplicant filter form base class.
 *
 * @package    ama
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseTblBlockApplicantFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'first_name'         => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'middle_name'        => new sfWidgetFormFilterInput(),
      'last_name'          => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'dob'                => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'card_first'         => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'card_last'          => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'app_id'             => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'ref_no'             => new sfWidgetFormFilterInput(),
      'app_type'           => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'card_holder_name'   => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'email'              => new sfWidgetFormFilterInput(),
      'processing_country' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Country'), 'add_empty' => true)),
      'embassy'            => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('EmbassyMaster'), 'add_empty' => true)),
      'blocked'            => new sfWidgetFormChoice(array('choices' => array('' => '', 'yes' => 'yes', 'no' => 'no'))),
      'created_at'         => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'         => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'created_by'         => new sfWidgetFormFilterInput(),
      'updated_by'         => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'first_name'         => new sfValidatorPass(array('required' => false)),
      'middle_name'        => new sfValidatorPass(array('required' => false)),
      'last_name'          => new sfValidatorPass(array('required' => false)),
      'dob'                => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'card_first'         => new sfValidatorPass(array('required' => false)),
      'card_last'          => new sfValidatorPass(array('required' => false)),
      'app_id'             => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'ref_no'             => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'app_type'           => new sfValidatorPass(array('required' => false)),
      'card_holder_name'   => new sfValidatorPass(array('required' => false)),
      'email'              => new sfValidatorPass(array('required' => false)),
      'processing_country' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Country'), 'column' => 'id')),
      'embassy'            => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('EmbassyMaster'), 'column' => 'id')),
      'blocked'            => new sfValidatorChoice(array('required' => false, 'choices' => array('yes' => 'yes', 'no' => 'no'))),
      'created_at'         => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'         => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'created_by'         => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'updated_by'         => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('tbl_block_applicant_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'TblBlockApplicant';
  }

  public function getFields()
  {
    return array(
      'id'                 => 'Number',
      'first_name'         => 'Text',
      'middle_name'        => 'Text',
      'last_name'          => 'Text',
      'dob'                => 'Date',
      'card_first'         => 'Text',
      'card_last'          => 'Text',
      'app_id'             => 'Number',
      'ref_no'             => 'Number',
      'app_type'           => 'Text',
      'card_holder_name'   => 'Text',
      'email'              => 'Text',
      'processing_country' => 'ForeignKey',
      'embassy'            => 'ForeignKey',
      'blocked'            => 'Enum',
      'created_at'         => 'Date',
      'updated_at'         => 'Date',
      'created_by'         => 'Number',
      'updated_by'         => 'Number',
    );
  }
}
