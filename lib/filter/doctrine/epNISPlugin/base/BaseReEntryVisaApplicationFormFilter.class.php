<?php

/**
 * ReEntryVisaApplication filter form base class.
 *
 * @package    ama
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseReEntryVisaApplicationFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'issusing_govt'             => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'passport_number'           => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'date_of_issue'             => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'date_of_exp'               => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'place_of_issue'            => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'address_id'                => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('ReEntryVisaAddress'), 'add_empty' => true)),
      'profession'                => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'reason_for_visa_requiring' => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'last_arrival_in_nigeria'   => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'proposeddate'              => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      're_entry_category'         => new sfWidgetFormChoice(array('choices' => array('' => '', 3 => '3', 6 => '6', 12 => '12'))),
      're_entry_type'             => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('EntryType'), 'add_empty' => true)),
      'no_of_re_entry_type'       => new sfWidgetFormFilterInput(),
      'employer_name'             => new sfWidgetFormFilterInput(),
      'employer_phone'            => new sfWidgetFormFilterInput(),
      'employer_address_id'       => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('ReEntryVisaEmployerAddress'), 'add_empty' => true)),
      'cerpa_quota'               => new sfWidgetFormFilterInput(),
      'cerpac_issuing_state'      => new sfWidgetFormFilterInput(),
      'cerpac_date_of_issue'      => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'cerpac_exp_date'           => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'issuing_cerpac_office'     => new sfWidgetFormFilterInput(),
      'visa_type_id'              => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('VisaType'), 'add_empty' => true)),
      'visa_state_id'             => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('VisaProceesingState'), 'add_empty' => true)),
      'visa_office_id'            => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('VisaOffice'), 'add_empty' => true)),
      'application_id'            => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('VisaApplication'), 'add_empty' => true)),
      'processing_centre_id'      => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('VisaProcessingCentre'), 'add_empty' => true)),
      'created_at'                => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'                => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
    ));

    $this->setValidators(array(
      'issusing_govt'             => new sfValidatorPass(array('required' => false)),
      'passport_number'           => new sfValidatorPass(array('required' => false)),
      'date_of_issue'             => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDateTime(array('required' => false)))),
      'date_of_exp'               => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDateTime(array('required' => false)))),
      'place_of_issue'            => new sfValidatorPass(array('required' => false)),
      'address_id'                => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('ReEntryVisaAddress'), 'column' => 'id')),
      'profession'                => new sfValidatorPass(array('required' => false)),
      'reason_for_visa_requiring' => new sfValidatorPass(array('required' => false)),
      'last_arrival_in_nigeria'   => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDateTime(array('required' => false)))),
      'proposeddate'              => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDateTime(array('required' => false)))),
      're_entry_category'         => new sfValidatorChoice(array('required' => false, 'choices' => array(3 => '3', 6 => '6', 12 => '12'))),
      're_entry_type'             => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('EntryType'), 'column' => 'id')),
      'no_of_re_entry_type'       => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'employer_name'             => new sfValidatorPass(array('required' => false)),
      'employer_phone'            => new sfValidatorPass(array('required' => false)),
      'employer_address_id'       => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('ReEntryVisaEmployerAddress'), 'column' => 'id')),
      'cerpa_quota'               => new sfValidatorPass(array('required' => false)),
      'cerpac_issuing_state'      => new sfValidatorPass(array('required' => false)),
      'cerpac_date_of_issue'      => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDateTime(array('required' => false)))),
      'cerpac_exp_date'           => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDateTime(array('required' => false)))),
      'issuing_cerpac_office'     => new sfValidatorPass(array('required' => false)),
      'visa_type_id'              => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('VisaType'), 'column' => 'id')),
      'visa_state_id'             => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('VisaProceesingState'), 'column' => 'id')),
      'visa_office_id'            => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('VisaOffice'), 'column' => 'id')),
      'application_id'            => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('VisaApplication'), 'column' => 'id')),
      'processing_centre_id'      => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('VisaProcessingCentre'), 'column' => 'id')),
      'created_at'                => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'                => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
    ));

    $this->widgetSchema->setNameFormat('re_entry_visa_application_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'ReEntryVisaApplication';
  }

  public function getFields()
  {
    return array(
      'id'                        => 'Number',
      'issusing_govt'             => 'Text',
      'passport_number'           => 'Text',
      'date_of_issue'             => 'Date',
      'date_of_exp'               => 'Date',
      'place_of_issue'            => 'Text',
      'address_id'                => 'ForeignKey',
      'profession'                => 'Text',
      'reason_for_visa_requiring' => 'Text',
      'last_arrival_in_nigeria'   => 'Date',
      'proposeddate'              => 'Date',
      're_entry_category'         => 'Enum',
      're_entry_type'             => 'ForeignKey',
      'no_of_re_entry_type'       => 'Number',
      'employer_name'             => 'Text',
      'employer_phone'            => 'Text',
      'employer_address_id'       => 'ForeignKey',
      'cerpa_quota'               => 'Text',
      'cerpac_issuing_state'      => 'Text',
      'cerpac_date_of_issue'      => 'Date',
      'cerpac_exp_date'           => 'Date',
      'issuing_cerpac_office'     => 'Text',
      'visa_type_id'              => 'ForeignKey',
      'visa_state_id'             => 'ForeignKey',
      'visa_office_id'            => 'ForeignKey',
      'application_id'            => 'ForeignKey',
      'processing_centre_id'      => 'ForeignKey',
      'created_at'                => 'Date',
      'updated_at'                => 'Date',
    );
  }
}
