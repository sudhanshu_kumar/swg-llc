<?php

/**
 * PassportApplicantContactinfo filter form base class.
 *
 * @package    ama
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BasePassportApplicantContactinfoFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'secondory_email' => new sfWidgetFormFilterInput(),
      'nationality_id'  => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Country'), 'add_empty' => true)),
      'state_of_origin' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('State'), 'add_empty' => true)),
      'contact_phone'   => new sfWidgetFormFilterInput(),
      'mobile_phone'    => new sfWidgetFormFilterInput(),
      'occupation'      => new sfWidgetFormFilterInput(),
      'home_town'       => new sfWidgetFormFilterInput(),
      'home_phone'      => new sfWidgetFormFilterInput(),
      'application_id'  => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('PassportApplication'), 'add_empty' => true)),
      'created_at'      => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'      => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
    ));

    $this->setValidators(array(
      'secondory_email' => new sfValidatorPass(array('required' => false)),
      'nationality_id'  => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Country'), 'column' => 'id')),
      'state_of_origin' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('State'), 'column' => 'id')),
      'contact_phone'   => new sfValidatorPass(array('required' => false)),
      'mobile_phone'    => new sfValidatorPass(array('required' => false)),
      'occupation'      => new sfValidatorPass(array('required' => false)),
      'home_town'       => new sfValidatorPass(array('required' => false)),
      'home_phone'      => new sfValidatorPass(array('required' => false)),
      'application_id'  => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('PassportApplication'), 'column' => 'id')),
      'created_at'      => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'      => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
    ));

    $this->widgetSchema->setNameFormat('passport_applicant_contactinfo_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'PassportApplicantContactinfo';
  }

  public function getFields()
  {
    return array(
      'id'              => 'Number',
      'secondory_email' => 'Text',
      'nationality_id'  => 'ForeignKey',
      'state_of_origin' => 'ForeignKey',
      'contact_phone'   => 'Text',
      'mobile_phone'    => 'Text',
      'occupation'      => 'Text',
      'home_town'       => 'Text',
      'home_phone'      => 'Text',
      'application_id'  => 'ForeignKey',
      'created_at'      => 'Date',
      'updated_at'      => 'Date',
    );
  }
}
