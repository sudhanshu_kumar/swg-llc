<?php

/**
 * VisaApplicantInfo filter form base class.
 *
 * @package    ama
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseVisaApplicantInfoFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'issusing_govt'                      => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'passport_number'                    => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'date_of_issue'                      => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'date_of_exp'                        => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'place_of_issue'                     => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'visatype_id'                        => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('VisaTypeId'), 'add_empty' => true)),
      'applying_country_id'                => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('ApplyingCountry'), 'add_empty' => true)),
      'embassy_of_pref_id'                 => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('EmbassyMaster'), 'add_empty' => true)),
      'purpose_of_journey'                 => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'entry_type_id'                      => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('EntryType'), 'add_empty' => true)),
      'no_of_re_entry_type'                => new sfWidgetFormFilterInput(),
      'stay_duration_days'                 => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'proposed_date_of_travel'            => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'mode_of_travel'                     => new sfWidgetFormFilterInput(),
      'money_in_hand'                      => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'application_id'                     => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('VisaApplication'), 'add_empty' => true)),
      'applied_nigeria_visa'               => new sfWidgetFormChoice(array('choices' => array('' => '', 'Yes' => 'Yes', 'No' => 'No'))),
      'nigeria_visa_applied_place'         => new sfWidgetFormFilterInput(),
      'applied_nigeria_visa_status'        => new sfWidgetFormChoice(array('choices' => array('' => '', 'Granted' => 'Granted', 'Rejected' => 'Rejected'))),
      'applied_nigeria_visa_reject_reason' => new sfWidgetFormFilterInput(),
      'have_visited_nigeria'               => new sfWidgetFormChoice(array('choices' => array('' => '', 'Yes' => 'Yes', 'No' => 'No'))),
      'visited_reason_type_id'             => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('VisaTypeReason'), 'add_empty' => true)),
      'applying_country_duration'          => new sfWidgetFormFilterInput(),
      'contagious_disease'                 => new sfWidgetFormChoice(array('choices' => array('' => '', 'Yes' => 'Yes', 'No' => 'No'))),
      'police_case'                        => new sfWidgetFormChoice(array('choices' => array('' => '', 'Yes' => 'Yes', 'No' => 'No'))),
      'narcotic_involvement'               => new sfWidgetFormChoice(array('choices' => array('' => '', 'Yes' => 'Yes', 'No' => 'No'))),
      'deported_status'                    => new sfWidgetFormChoice(array('choices' => array('' => '', 'Yes' => 'Yes', 'No' => 'No'))),
      'deported_county_id'                 => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('DeportedCountry'), 'add_empty' => true)),
      'visa_fraud_status'                  => new sfWidgetFormChoice(array('choices' => array('' => '', 'Yes' => 'Yes', 'No' => 'No'))),
      'authority_id'                       => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('VisaProcessingCentre'), 'add_empty' => true)),
      'created_at'                         => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'                         => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
    ));

    $this->setValidators(array(
      'issusing_govt'                      => new sfValidatorPass(array('required' => false)),
      'passport_number'                    => new sfValidatorPass(array('required' => false)),
      'date_of_issue'                      => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDateTime(array('required' => false)))),
      'date_of_exp'                        => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDateTime(array('required' => false)))),
      'place_of_issue'                     => new sfValidatorPass(array('required' => false)),
      'visatype_id'                        => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('VisaTypeId'), 'column' => 'id')),
      'applying_country_id'                => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('ApplyingCountry'), 'column' => 'id')),
      'embassy_of_pref_id'                 => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('EmbassyMaster'), 'column' => 'id')),
      'purpose_of_journey'                 => new sfValidatorPass(array('required' => false)),
      'entry_type_id'                      => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('EntryType'), 'column' => 'id')),
      'no_of_re_entry_type'                => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'stay_duration_days'                 => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'proposed_date_of_travel'            => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDateTime(array('required' => false)))),
      'mode_of_travel'                     => new sfValidatorPass(array('required' => false)),
      'money_in_hand'                      => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'application_id'                     => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('VisaApplication'), 'column' => 'id')),
      'applied_nigeria_visa'               => new sfValidatorChoice(array('required' => false, 'choices' => array('Yes' => 'Yes', 'No' => 'No'))),
      'nigeria_visa_applied_place'         => new sfValidatorPass(array('required' => false)),
      'applied_nigeria_visa_status'        => new sfValidatorChoice(array('required' => false, 'choices' => array('Granted' => 'Granted', 'Rejected' => 'Rejected'))),
      'applied_nigeria_visa_reject_reason' => new sfValidatorPass(array('required' => false)),
      'have_visited_nigeria'               => new sfValidatorChoice(array('required' => false, 'choices' => array('Yes' => 'Yes', 'No' => 'No'))),
      'visited_reason_type_id'             => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('VisaTypeReason'), 'column' => 'id')),
      'applying_country_duration'          => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'contagious_disease'                 => new sfValidatorChoice(array('required' => false, 'choices' => array('Yes' => 'Yes', 'No' => 'No'))),
      'police_case'                        => new sfValidatorChoice(array('required' => false, 'choices' => array('Yes' => 'Yes', 'No' => 'No'))),
      'narcotic_involvement'               => new sfValidatorChoice(array('required' => false, 'choices' => array('Yes' => 'Yes', 'No' => 'No'))),
      'deported_status'                    => new sfValidatorChoice(array('required' => false, 'choices' => array('Yes' => 'Yes', 'No' => 'No'))),
      'deported_county_id'                 => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('DeportedCountry'), 'column' => 'id')),
      'visa_fraud_status'                  => new sfValidatorChoice(array('required' => false, 'choices' => array('Yes' => 'Yes', 'No' => 'No'))),
      'authority_id'                       => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('VisaProcessingCentre'), 'column' => 'id')),
      'created_at'                         => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'                         => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
    ));

    $this->widgetSchema->setNameFormat('visa_applicant_info_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'VisaApplicantInfo';
  }

  public function getFields()
  {
    return array(
      'id'                                 => 'Number',
      'issusing_govt'                      => 'Text',
      'passport_number'                    => 'Text',
      'date_of_issue'                      => 'Date',
      'date_of_exp'                        => 'Date',
      'place_of_issue'                     => 'Text',
      'visatype_id'                        => 'ForeignKey',
      'applying_country_id'                => 'ForeignKey',
      'embassy_of_pref_id'                 => 'ForeignKey',
      'purpose_of_journey'                 => 'Text',
      'entry_type_id'                      => 'ForeignKey',
      'no_of_re_entry_type'                => 'Number',
      'stay_duration_days'                 => 'Number',
      'proposed_date_of_travel'            => 'Date',
      'mode_of_travel'                     => 'Text',
      'money_in_hand'                      => 'Number',
      'application_id'                     => 'ForeignKey',
      'applied_nigeria_visa'               => 'Enum',
      'nigeria_visa_applied_place'         => 'Text',
      'applied_nigeria_visa_status'        => 'Enum',
      'applied_nigeria_visa_reject_reason' => 'Text',
      'have_visited_nigeria'               => 'Enum',
      'visited_reason_type_id'             => 'ForeignKey',
      'applying_country_duration'          => 'Number',
      'contagious_disease'                 => 'Enum',
      'police_case'                        => 'Enum',
      'narcotic_involvement'               => 'Enum',
      'deported_status'                    => 'Enum',
      'deported_county_id'                 => 'ForeignKey',
      'visa_fraud_status'                  => 'Enum',
      'authority_id'                       => 'ForeignKey',
      'created_at'                         => 'Date',
      'updated_at'                         => 'Date',
    );
  }
}
