<?php

/**
 * VisaFee filter form base class.
 *
 * @package    ama
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseVisaFeeFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'country_id'        => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Country'), 'add_empty' => true)),
      'visa_cat_id'       => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('VisaCategory'), 'add_empty' => true)),
      'visa_type_id'      => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('VisaType'), 'add_empty' => true)),
      'entry_type_id'     => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('EntryType'), 'add_empty' => true)),
      'naira_amount'      => new sfWidgetFormFilterInput(),
      'dollar_amount'     => new sfWidgetFormFilterInput(),
      'is_fee_multiplied' => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'is_gratis'         => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'created_at'        => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'        => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'created_by'        => new sfWidgetFormFilterInput(),
      'updated_by'        => new sfWidgetFormFilterInput(),
      'version'           => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'country_id'        => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Country'), 'column' => 'id')),
      'visa_cat_id'       => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('VisaCategory'), 'column' => 'id')),
      'visa_type_id'      => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('VisaType'), 'column' => 'id')),
      'entry_type_id'     => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('EntryType'), 'column' => 'id')),
      'naira_amount'      => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'dollar_amount'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'is_fee_multiplied' => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'is_gratis'         => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'created_at'        => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'        => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'created_by'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'updated_by'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'version'           => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('visa_fee_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'VisaFee';
  }

  public function getFields()
  {
    return array(
      'id'                => 'Number',
      'country_id'        => 'ForeignKey',
      'visa_cat_id'       => 'ForeignKey',
      'visa_type_id'      => 'ForeignKey',
      'entry_type_id'     => 'ForeignKey',
      'naira_amount'      => 'Number',
      'dollar_amount'     => 'Number',
      'is_fee_multiplied' => 'Boolean',
      'is_gratis'         => 'Boolean',
      'created_at'        => 'Date',
      'updated_at'        => 'Date',
      'created_by'        => 'Number',
      'updated_by'        => 'Number',
      'version'           => 'Number',
    );
  }
}
