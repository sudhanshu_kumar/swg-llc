<?php

/**
 * PassportApplicationDetails filter form base class.
 *
 * @package    ama
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BasePassportApplicationDetailsFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'request_type_id'           => new sfWidgetFormChoice(array('choices' => array('' => '', 'Adult' => 'Adult', 'Minor' => 'Minor', 'None' => 'None'))),
      'correspondence_address'    => new sfWidgetFormFilterInput(),
      'permanent_address_id'      => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('PassportPermanentAddress'), 'add_empty' => true)),
      'contact_address_id'        => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('PassportContactAddress'), 'add_empty' => true)),
      'city'                      => new sfWidgetFormFilterInput(),
      'country_id'                => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Country'), 'add_empty' => true)),
      'stateoforigin'             => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('State'), 'add_empty' => true)),
      'specialfeatures'           => new sfWidgetFormFilterInput(),
      'application_id'            => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('PassportApplication'), 'add_empty' => true)),
      'employer'                  => new sfWidgetFormFilterInput(),
      'district'                  => new sfWidgetFormFilterInput(),
      'seamans_discharge_book'    => new sfWidgetFormFilterInput(),
      'seamans_previous_passport' => new sfWidgetFormFilterInput(),
      'overseas_address_id'       => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('PassportOverseasAddress'), 'add_empty' => true)),
      'created_at'                => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'                => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
    ));

    $this->setValidators(array(
      'request_type_id'           => new sfValidatorChoice(array('required' => false, 'choices' => array('Adult' => 'Adult', 'Minor' => 'Minor', 'None' => 'None'))),
      'correspondence_address'    => new sfValidatorPass(array('required' => false)),
      'permanent_address_id'      => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('PassportPermanentAddress'), 'column' => 'id')),
      'contact_address_id'        => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('PassportContactAddress'), 'column' => 'id')),
      'city'                      => new sfValidatorPass(array('required' => false)),
      'country_id'                => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Country'), 'column' => 'id')),
      'stateoforigin'             => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('State'), 'column' => 'id')),
      'specialfeatures'           => new sfValidatorPass(array('required' => false)),
      'application_id'            => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('PassportApplication'), 'column' => 'id')),
      'employer'                  => new sfValidatorPass(array('required' => false)),
      'district'                  => new sfValidatorPass(array('required' => false)),
      'seamans_discharge_book'    => new sfValidatorPass(array('required' => false)),
      'seamans_previous_passport' => new sfValidatorPass(array('required' => false)),
      'overseas_address_id'       => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('PassportOverseasAddress'), 'column' => 'id')),
      'created_at'                => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'                => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
    ));

    $this->widgetSchema->setNameFormat('passport_application_details_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'PassportApplicationDetails';
  }

  public function getFields()
  {
    return array(
      'id'                        => 'Number',
      'request_type_id'           => 'Enum',
      'correspondence_address'    => 'Text',
      'permanent_address_id'      => 'ForeignKey',
      'contact_address_id'        => 'ForeignKey',
      'city'                      => 'Text',
      'country_id'                => 'ForeignKey',
      'stateoforigin'             => 'ForeignKey',
      'specialfeatures'           => 'Text',
      'application_id'            => 'ForeignKey',
      'employer'                  => 'Text',
      'district'                  => 'Text',
      'seamans_discharge_book'    => 'Text',
      'seamans_previous_passport' => 'Text',
      'overseas_address_id'       => 'ForeignKey',
      'created_at'                => 'Date',
      'updated_at'                => 'Date',
    );
  }
}
