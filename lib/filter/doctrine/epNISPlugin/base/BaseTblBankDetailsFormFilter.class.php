<?php

/**
 * TblBankDetails filter form base class.
 *
 * @package    ama
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseTblBankDetailsFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'bank_name'      => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'account_number' => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'account_name'   => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'city'           => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'state'          => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'country'        => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'project_id'     => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('TblProjectName'), 'add_empty' => true)),
      'created_at'     => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'     => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'created_by'     => new sfWidgetFormFilterInput(),
      'updated_by'     => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'bank_name'      => new sfValidatorPass(array('required' => false)),
      'account_number' => new sfValidatorPass(array('required' => false)),
      'account_name'   => new sfValidatorPass(array('required' => false)),
      'city'           => new sfValidatorPass(array('required' => false)),
      'state'          => new sfValidatorPass(array('required' => false)),
      'country'        => new sfValidatorPass(array('required' => false)),
      'project_id'     => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('TblProjectName'), 'column' => 'id')),
      'created_at'     => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'     => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'created_by'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'updated_by'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('tbl_bank_details_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'TblBankDetails';
  }

  public function getFields()
  {
    return array(
      'id'             => 'Number',
      'bank_name'      => 'Text',
      'account_number' => 'Text',
      'account_name'   => 'Text',
      'city'           => 'Text',
      'state'          => 'Text',
      'country'        => 'Text',
      'project_id'     => 'ForeignKey',
      'created_at'     => 'Date',
      'updated_at'     => 'Date',
      'created_by'     => 'Number',
      'updated_by'     => 'Number',
    );
  }
}
