<?php

/**
 * VisaRelativeEmployerAddress filter form.
 *
 * @package    ama
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrinePluginFormFilterTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class VisaRelativeEmployerAddressFormFilter extends PluginVisaRelativeEmployerAddressFormFilter
{
  /**
   * @see AddressMasterInterNationalFormFilter
   */
  public function configure()
  {
    parent::configure();
  }
}
