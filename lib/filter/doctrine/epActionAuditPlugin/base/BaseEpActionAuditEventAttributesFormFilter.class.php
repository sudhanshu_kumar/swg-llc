<?php

/**
 * EpActionAuditEventAttributes filter form base class.
 *
 * @package    ama
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseEpActionAuditEventAttributesFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'audit_event_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('EpActionAuditEvent'), 'add_empty' => true)),
      'name'           => new sfWidgetFormFilterInput(),
      'svalue'         => new sfWidgetFormFilterInput(),
      'ivalue'         => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'audit_event_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('EpActionAuditEvent'), 'column' => 'id')),
      'name'           => new sfValidatorPass(array('required' => false)),
      'svalue'         => new sfValidatorPass(array('required' => false)),
      'ivalue'         => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('ep_action_audit_event_attributes_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'EpActionAuditEventAttributes';
  }

  public function getFields()
  {
    return array(
      'id'             => 'Number',
      'audit_event_id' => 'ForeignKey',
      'name'           => 'Text',
      'svalue'         => 'Text',
      'ivalue'         => 'Number',
    );
  }
}
