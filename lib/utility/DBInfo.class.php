<?php
/**
 * This helper class provides functionatlity
 * related to workflow
 */

final class DBInfo {

    public static function getDSNInfo() {
        // TODO: Change it so that this is picked from the
        // application configuration too
        $db = Doctrine_Manager::connection()->getManager()->getConnection('connection_ipay4me')->getOptions();
        $dbdsn = explode(':',$db['dsn']);
        $dbarray = explode(';',$dbdsn[1]);
       $dbname  = explode('=',$dbarray[0]);
        $dbhost = explode('=',$dbarray[1]);

        $dbInfo = array();
        $dbInfo['username'] = $db['username'];
        $dbInfo['password'] = $db['password'];
        $dbInfo['host'] = $dbname[1];
        $dbInfo['dbname'] = $dbhost[1];
        
        return $dbInfo;
    }
}

?>
