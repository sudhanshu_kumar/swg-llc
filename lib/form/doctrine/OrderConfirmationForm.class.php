<?php

class OrderConfirmationForm extends sfForm
{
  //protected static $subjects = array('Subject A', 'Subject B', 'Subject C');

 protected static function getMonth(){
    $monthArr = array();
   for($i=1;$i<=12;$i++){
      $mon = @date("m", mktime(0, 0, 0, $i+1, 0, 0, 0));
      $month = @date("F", mktime(0, 0, 0, $i+1, 0, 0, 0));
      $monthArr[$mon] = $month;
   }
   return $monthArr;
 }

 protected static function getYear(){
     $yearArr = array();
     for($i=@date("Y"); $i<=@date("Y")+30; $i++){
        $yearArr[$i] = $i;
     }
     return $yearArr;
 }

  public function configure()
  {
    $this->setWidgets(array(
      'card_num'    => new sfWidgetFormInputText(array(),array('maxlength'=>16, 'class' => "inputTxt", 'onblur'=>'checkCardType();')),
      'month' => new sfWidgetFormSelect(array('choices' => self::getMonth())),
      'year' => new sfWidgetFormSelect(array('choices' => self::getYear())),
      'cvv_number'  => new sfWidgetFormInputPassword(array(),array('maxlength'=>4, 'class' => "inputTxt")),
      
    ));
    $this->widgetSchema->setNameFormat('cardDetails[%s]');

    $this->setValidators(array(
      'card_num'    => new sfValidatorString(array('required' => true),array('required'=>'Please enter Card Number')),
      'month' => new sfValidatorChoice(array('choices' => array_keys(self::getMonth()))),
      'year' => new sfValidatorChoice(array('choices' => array_keys(self::getYear()))),
      'cvv_number'    => new sfValidatorString(array('required' => true),array('required'=>'Please enter CVV Number')),
    ));


   $this->widgetSchema->setLabels(array(
            'card_num'    => 'Card Number',
            'month'      => 'Card Expiry',
            'cvv_number'   => 'CVV Number'
        ));


  }
}