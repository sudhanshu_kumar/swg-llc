<?php

/**
 * EpVbvResponse form base class.
 *
 * @method EpVbvResponse getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseEpVbvResponseForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                   => new sfWidgetFormInputHidden(),
      'vbvrequest_id'        => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('EpVbvRequest'), 'add_empty' => false)),
      'msg_date'             => new sfWidgetFormDateTime(),
      'version'              => new sfWidgetFormInputText(),
      'order_id'             => new sfWidgetFormInputText(),
      'transaction_type'     => new sfWidgetFormInputText(),
      'pan'                  => new sfWidgetFormInputText(),
      'purchase_amount'      => new sfWidgetFormInputText(),
      'currency'             => new sfWidgetFormInputText(),
      'tran_date_time'       => new sfWidgetFormDateTime(),
      'response_code'        => new sfWidgetFormInputText(),
      'response_description' => new sfWidgetFormInputText(),
      'order_status'         => new sfWidgetFormInputText(),
      'approval_code'        => new sfWidgetFormInputText(),
      'order_description'    => new sfWidgetFormInputText(),
      'approval_code_scr'    => new sfWidgetFormInputText(),
      'purchase_amount_scr'  => new sfWidgetFormInputText(),
      'currency_scr'         => new sfWidgetFormInputText(),
      'order_status_scr'     => new sfWidgetFormInputText(),
      'shop_order_id'        => new sfWidgetFormInputText(),
      'three_ds_verificaion' => new sfWidgetFormInputText(),
      'response_xml'         => new sfWidgetFormTextarea(),
      'created_at'           => new sfWidgetFormDateTime(),
      'updated_at'           => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                   => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'vbvrequest_id'        => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('EpVbvRequest'))),
      'msg_date'             => new sfValidatorDateTime(array('required' => false)),
      'version'              => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'order_id'             => new sfValidatorString(array('max_length' => 200, 'required' => false)),
      'transaction_type'     => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'pan'                  => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'purchase_amount'      => new sfValidatorNumber(array('required' => false)),
      'currency'             => new sfValidatorNumber(array('required' => false)),
      'tran_date_time'       => new sfValidatorDateTime(array('required' => false)),
      'response_code'        => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'response_description' => new sfValidatorString(array('max_length' => 200, 'required' => false)),
      'order_status'         => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'approval_code'        => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'order_description'    => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'approval_code_scr'    => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'purchase_amount_scr'  => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'currency_scr'         => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'order_status_scr'     => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'shop_order_id'        => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'three_ds_verificaion' => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'response_xml'         => new sfValidatorString(),
      'created_at'           => new sfValidatorDateTime(),
      'updated_at'           => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('ep_vbv_response[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'EpVbvResponse';
  }

}
