<?php

/**
 * EpVbvRequest form base class.
 *
 * @method EpVbvRequest getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseEpVbvRequestForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'              => new sfWidgetFormInputHidden(),
      'order_id'        => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('VbvOrder'), 'add_empty' => false)),
      'mer_id'          => new sfWidgetFormInputText(),
      'acqbin'          => new sfWidgetFormInputText(),
      'purchase_amount' => new sfWidgetFormInputText(),
      'visual_amount'   => new sfWidgetFormInputText(),
      'currency'        => new sfWidgetFormInputText(),
      'ret_url_approve' => new sfWidgetFormTextarea(),
      'ret_url_decline' => new sfWidgetFormTextarea(),
      'post_url'        => new sfWidgetFormTextarea(),
      'user_id'         => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('sfGuardUser'), 'add_empty' => true)),
      'created_at'      => new sfWidgetFormDateTime(),
      'updated_at'      => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'              => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'order_id'        => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('VbvOrder'))),
      'mer_id'          => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'acqbin'          => new sfValidatorInteger(array('required' => false)),
      'purchase_amount' => new sfValidatorInteger(array('required' => false)),
      'visual_amount'   => new sfValidatorNumber(array('required' => false)),
      'currency'        => new sfValidatorInteger(array('required' => false)),
      'ret_url_approve' => new sfValidatorString(array('max_length' => 500, 'required' => false)),
      'ret_url_decline' => new sfValidatorString(array('max_length' => 500, 'required' => false)),
      'post_url'        => new sfValidatorString(array('max_length' => 500, 'required' => false)),
      'user_id'         => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('sfGuardUser'), 'required' => false)),
      'created_at'      => new sfValidatorDateTime(),
      'updated_at'      => new sfValidatorDateTime(),
    ));

    $this->validatorSchema->setPostValidator(
      new sfValidatorDoctrineUnique(array('model' => 'EpVbvRequest', 'column' => array('order_id')))
    );

    $this->widgetSchema->setNameFormat('ep_vbv_request[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'EpVbvRequest';
  }

}
