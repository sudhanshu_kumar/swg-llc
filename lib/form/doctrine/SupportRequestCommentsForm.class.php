<?php

/**
 * SupportRequestComments form.
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class SupportRequestCommentsForm extends BaseSupportRequestCommentsForm
{
  public function configure()
  {
    unset ($this['support_user_comments'],$this['upload_document1'],$this['created_at'],$this['updated_at'],$this['created_by'],$this['updated_by']);

    $this->setWidgets(array(
            'user_id' => new sfWidgetFormInputHidden(),
            'request_id' => new sfWidgetFormInputHidden(),
            'customer_comments' => new sfWidgetFormTextarea(array(),array('class'=>'txt-input','maxlength'=>32,'min_length'=>3)),
            'upload_document2' => new sfWidgetFormInputFile(array(),array('class'=>'txt-input','maxlength'=>20,'min_length'=>3)),
            'upload_document3' => new sfWidgetFormInputFile(array(),array('class'=>'txt-input','maxlength'=>20,'min_length'=>3)),
            'upload_document4' => new sfWidgetFormInputFile(array(),array('class'=>'txt-input','maxlength'=>20,'min_length'=>3)),

      ));

    $this->setValidators(array(
            'request_id' => new sfValidatorNumber(array('required'=>false)),
            'user_id' => new sfValidatorNumber(array('required'=>false)),
            'customer_comments' => new sfValidatorString(array('required'=>true),array('required'=>"Please enter comments")),
            'upload_document2'       => new sfValidatorFile(array(
                            'required'   => false,
                            'path'       => sfConfig::get('sf_upload_dir').'/applicant_profile',
                            'mime_types' => array(
                                                'image/jpeg',
                                                'image/png',
                                                'image/gif',
                                               ),
                            'max_size' => '410623',
          ),array('required'=>"Please upload credit card statement.","mime_types"=>"Please upload JPG/GIF/PNG images only.","max_size"=>"Credit card statement can not be more then 400 KB.")),
            'upload_document3'       => new sfValidatorFile(array(
                            'required'   => false,
                            'path'       => sfConfig::get('sf_upload_dir').'/applicant_profile',
                            'mime_types' => array(
                                                'image/jpeg',
                                                'image/png',
                                                'image/gif',
                                               ),
                            'max_size' => '410623',
          ),array('required'=>"Please upload credit card statement.","mime_types"=>"Please upload JPG/GIF/PNG images only.","max_size"=>"Credit card statement can not be more then 400 KB.")),

            'upload_document4'       => new sfValidatorFile(array(
                            'required'   => false,
                            'path'       => sfConfig::get('sf_upload_dir').'/applicant_profile',
                            'mime_types' => array(
                                                'image/jpeg',
                                                'image/png',
                                                'image/gif',
                                               ),
                            'max_size' => '410623',
          ),array('required'=>"Please upload credit card statement.","mime_types"=>"Please upload JPG/GIF/PNG images only.","max_size"=>"Credit card statement can not be more then 400 KB.")),

        ));


      $this->setDefault('request_id', $this->getOption('req_id'));
      $this->setDefault('user_id', $this->getOption('user_id'));

       $this->widgetSchema->setLabels(array(
     'customer_comments'=> 'Response Text',
     'upload_document2'=>'Upload Document 1:',
     'upload_document3'=>'Upload Document 2:',
     'upload_document4'=>'Upload Document 3:'));



 $this->widgetSchema->setNameFormat('support_request_comments[%s]');
  }
}
