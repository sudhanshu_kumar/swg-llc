<?php

/**
 * VisaApplicantPreviousHistory form.
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrinePluginFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class VisaApplicantPreviousHistoryForm extends PluginVisaApplicantPreviousHistoryForm
{
  public function configure()
  {
    //Unset Field
    unset($this['created_at'],$this['updated_at'],$this['application_id'],$this['travel_type_id']);

    //Set Field Label
    $this->widgetSchema['startdate'] = new sfWidgetFormDateCal(array('years'=>WidgetHelpers::getDateRanges()));
    $this->widgetSchema['endate'] = new sfWidgetFormDateCal(array('years'=>WidgetHelpers::getDateRanges()));
    $this->widgetSchema->setLabels(array( 'travel_type_id'    => 'Travel Type',
                                                'startdate'    => 'From(dd-mm-yyyy)',
                                                'endate'    => 'To(dd-mm-yyyy)',
        //'resid_address'    => 'Address'

      )
    );

    //Set Field Type
    // $this->widgetSchema['resid_address'] = new sfWidgetFormTextarea(array('label' => 'Address'));

    //Set Field Validation
    //$this->validatorSchema['resid_address'] = new sfValidatorString(array('max_length' => 255,'required' => false));
    $this->validatorSchema['startdate'] = new sfValidatorDate(array('max'=> time(),'required' =>false), array('max'=>'Start date can not be greater than To date.', 'invalid' => 'Start date is invalid.'));
    $this->validatorSchema['endate'] = new sfValidatorDate(array('max'=> time(),'required' =>false), array('max'=>'End date is invalid.', 'invalid' => 'End date is invalid.'));


    // Date compare Validation
    // $this->validatorSchema['resid_address']  = new sfValidatorString(array('max_length' => 255, 'required' => false),array('max_length'=>'Residence address can not be more than 255 characters.'));
    $this->validatorSchema->setPostValidator(
      new sfValidatorSchemaCompare('startdate', sfValidatorSchemaCompare::LESS_THAN_EQUAL, 'endate',
        array(),
        array('invalid' => '(From date) must be before or same to (To date).')
      ));
  }
}
