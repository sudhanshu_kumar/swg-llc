<?php

/**
 * EntryType form.
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrinePluginFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class EntryTypeForm extends PluginEntryTypeForm
{
  /**
   * @see GlobalMasterForm
   */
  public function configure()
  {
    parent::configure();
  }
}
