<?php

/**
 * PassportApplication form base class.
 *
 * @method PassportApplication getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BasePassportApplicationForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                            => new sfWidgetFormInputHidden(),
      'passporttype_id'               => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('PassportAppType'), 'add_empty' => true)),
      'ref_no'                        => new sfWidgetFormInputText(),
      'title_id'                      => new sfWidgetFormChoice(array('choices' => array('MR' => 'MR', 'MRS' => 'MRS', 'MISS' => 'MISS', 'DR' => 'DR'))),
      'first_name'                    => new sfWidgetFormInputText(),
      'last_name'                     => new sfWidgetFormInputText(),
      'mid_name'                      => new sfWidgetFormInputText(),
      'email'                         => new sfWidgetFormInputText(),
      'occupation'                    => new sfWidgetFormInputText(),
      'gender_id'                     => new sfWidgetFormChoice(array('choices' => array('Male' => 'Male', 'Female' => 'Female', 'None' => 'None'))),
      'place_of_birth'                => new sfWidgetFormInputText(),
      'date_of_birth'                 => new sfWidgetFormDate(),
      'color_eyes_id'                 => new sfWidgetFormChoice(array('choices' => array('Brown' => 'Brown', 'Blue' => 'Blue', 'Green' => 'Green', 'Gray' => 'Gray', 'None' => 'None'))),
      'color_hair_id'                 => new sfWidgetFormChoice(array('choices' => array('Black' => 'Black', 'Brown' => 'Brown', 'White' => 'White', 'Gray' => 'Gray', 'None' => 'None'))),
      'marital_status_id'             => new sfWidgetFormChoice(array('choices' => array('Single' => 'Single', 'Married' => 'Married', 'Widowed' => 'Widowed', 'Divorced' => 'Divorced', 'None' => 'None'))),
      'ispaid'                        => new sfWidgetFormInputText(),
      'payment_trans_id'              => new sfWidgetFormInputText(),
      'maid_name'                     => new sfWidgetFormInputText(),
      'height'                        => new sfWidgetFormInputText(),
      'processing_country_id'         => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Country'), 'add_empty' => true)),
      'processing_state_id'           => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('State'), 'add_empty' => true)),
      'processing_embassy_id'         => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('EmbassyMaster'), 'add_empty' => true)),
      'processing_passport_office_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('PassportOffice'), 'add_empty' => true)),
      'next_kin'                      => new sfWidgetFormInputText(),
      'next_kin_phone'                => new sfWidgetFormInputText(),
      'relation_with_kin'             => new sfWidgetFormInputText(),
      'next_kin_address_id'           => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('PassportKinAddress'), 'add_empty' => true)),
      'passport_no'                   => new sfWidgetFormInputText(),
      'interview_date'                => new sfWidgetFormDate(),
      'status'                        => new sfWidgetFormChoice(array('choices' => array('New' => 'New', 'Paid' => 'Paid', 'Vetted' => 'Vetted', 'Approved' => 'Approved', 'Rejected by Vetter' => 'Rejected by Vetter', 'Rejected by Approver' => 'Rejected by Approver'))),
      'payment_gateway_id'            => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('PaymentGatewayType'), 'add_empty' => true)),
      'paid_dollar_amount'            => new sfWidgetFormInputText(),
      'local_currency_id'             => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('LocalCurrencyType'), 'add_empty' => true)),
      'paid_local_currency_amount'    => new sfWidgetFormInputText(),
      'amount'                        => new sfWidgetFormInputText(),
      'currency_id'                   => new sfWidgetFormInputText(),
      'paid_at'                       => new sfWidgetFormDate(),
      'is_email_valid'                => new sfWidgetFormInputCheckbox(),
      'created_at'                    => new sfWidgetFormDateTime(),
      'updated_at'                    => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                            => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'passporttype_id'               => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('PassportAppType'), 'required' => false)),
      'ref_no'                        => new sfValidatorInteger(array('required' => false)),
      'title_id'                      => new sfValidatorChoice(array('choices' => array(0 => 'MR', 1 => 'MRS', 2 => 'MISS', 3 => 'DR'), 'required' => false)),
      'first_name'                    => new sfValidatorString(array('max_length' => 50)),
      'last_name'                     => new sfValidatorString(array('max_length' => 50)),
      'mid_name'                      => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'email'                         => new sfValidatorString(array('max_length' => 70)),
      'occupation'                    => new sfValidatorString(array('max_length' => 150, 'required' => false)),
      'gender_id'                     => new sfValidatorChoice(array('choices' => array(0 => 'Male', 1 => 'Female', 2 => 'None'), 'required' => false)),
      'place_of_birth'                => new sfValidatorString(array('max_length' => 70)),
      'date_of_birth'                 => new sfValidatorDate(array('required' => false)),
      'color_eyes_id'                 => new sfValidatorChoice(array('choices' => array(0 => 'Brown', 1 => 'Blue', 2 => 'Green', 3 => 'Gray', 4 => 'None'), 'required' => false)),
      'color_hair_id'                 => new sfValidatorChoice(array('choices' => array(0 => 'Black', 1 => 'Brown', 2 => 'White', 3 => 'Gray', 4 => 'None'), 'required' => false)),
      'marital_status_id'             => new sfValidatorChoice(array('choices' => array(0 => 'Single', 1 => 'Married', 2 => 'Widowed', 3 => 'Divorced', 4 => 'None'), 'required' => false)),
      'ispaid'                        => new sfValidatorString(array('max_length' => 1, 'required' => false)),
      'payment_trans_id'              => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'maid_name'                     => new sfValidatorString(array('max_length' => 70, 'required' => false)),
      'height'                        => new sfValidatorString(array('max_length' => 7, 'required' => false)),
      'processing_country_id'         => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Country'), 'required' => false)),
      'processing_state_id'           => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('State'), 'required' => false)),
      'processing_embassy_id'         => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('EmbassyMaster'), 'required' => false)),
      'processing_passport_office_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('PassportOffice'), 'required' => false)),
      'next_kin'                      => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'next_kin_phone'                => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'relation_with_kin'             => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'next_kin_address_id'           => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('PassportKinAddress'), 'required' => false)),
      'passport_no'                   => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'interview_date'                => new sfValidatorDate(array('required' => false)),
      'status'                        => new sfValidatorChoice(array('choices' => array(0 => 'New', 1 => 'Paid', 2 => 'Vetted', 3 => 'Approved', 4 => 'Rejected by Vetter', 5 => 'Rejected by Approver'), 'required' => false)),
      'payment_gateway_id'            => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('PaymentGatewayType'), 'required' => false)),
      'paid_dollar_amount'            => new sfValidatorNumber(array('required' => false)),
      'local_currency_id'             => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('LocalCurrencyType'), 'required' => false)),
      'paid_local_currency_amount'    => new sfValidatorNumber(array('required' => false)),
      'amount'                        => new sfValidatorNumber(array('required' => false)),
      'currency_id'                   => new sfValidatorInteger(array('required' => false)),
      'paid_at'                       => new sfValidatorDate(array('required' => false)),
      'is_email_valid'                => new sfValidatorBoolean(array('required' => false)),
      'created_at'                    => new sfValidatorDateTime(),
      'updated_at'                    => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('passport_application[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'PassportApplication';
  }

}
