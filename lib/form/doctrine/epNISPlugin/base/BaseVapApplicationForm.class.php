<?php

/**
 * VapApplication form base class.
 *
 * @method VapApplication getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseVapApplicationForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                       => new sfWidgetFormInputHidden(),
      'ref_no'                   => new sfWidgetFormInputText(),
      'title'                    => new sfWidgetFormChoice(array('choices' => array('MR' => 'MR', 'MRS' => 'MRS', 'MISS' => 'MISS', 'DR' => 'DR'))),
      'first_name'               => new sfWidgetFormInputText(),
      'surname'                  => new sfWidgetFormInputText(),
      'middle_name'              => new sfWidgetFormInputText(),
      'gender'                   => new sfWidgetFormChoice(array('choices' => array('Male' => 'Male', 'Female' => 'Female'))),
      'marital_status'           => new sfWidgetFormChoice(array('choices' => array('Single' => 'Single', 'Married' => 'Married', 'Widowed' => 'Widowed', 'Divorced' => 'Divorced', 'None' => 'None'))),
      'email'                    => new sfWidgetFormInputText(),
      'date_of_birth'            => new sfWidgetFormDate(),
      'place_of_birth'           => new sfWidgetFormInputText(),
      'present_nationality_id'   => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('CurrentCountry'), 'add_empty' => true)),
      'hair_color'               => new sfWidgetFormChoice(array('choices' => array('Black' => 'Black', 'Brown' => 'Brown', 'White' => 'White', 'Red' => 'Red', 'Gray' => 'Gray', 'None' => 'None'))),
      'eyes_color'               => new sfWidgetFormChoice(array('choices' => array('Brown' => 'Brown', 'Blue' => 'Blue', 'Green' => 'Green', 'Gray' => 'Gray', 'None' => 'None'))),
      'id_marks'                 => new sfWidgetFormInputText(),
      'height'                   => new sfWidgetFormInputText(),
      'office_address_id'        => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('VapVisaOfficeAddress'), 'add_empty' => true)),
      'office_phone_no'          => new sfWidgetFormInputText(),
      'applicant_type'           => new sfWidgetFormChoice(array('choices' => array('GB' => 'GB', 'PB' => 'PB'))),
      'vap_company_id'           => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('VapCompany'), 'add_empty' => true)),
      'customer_service_number'  => new sfWidgetFormInputText(),
      'document_1'               => new sfWidgetFormInputText(),
      'document_2'               => new sfWidgetFormInputText(),
      'document_3'               => new sfWidgetFormInputText(),
      'document_4'               => new sfWidgetFormInputText(),
      'document_5'               => new sfWidgetFormInputText(),
      'boarding_place'           => new sfWidgetFormInputText(),
      'flight_carrier'           => new sfWidgetFormInputText(),
      'flight_number'            => new sfWidgetFormInputText(),
      'issusing_govt'            => new sfWidgetFormInputText(),
      'passport_number'          => new sfWidgetFormInputText(),
      'date_of_issue'            => new sfWidgetFormDate(),
      'date_of_exp'              => new sfWidgetFormDate(),
      'place_of_issue'           => new sfWidgetFormInputText(),
      'applying_country_id'      => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('VapApplyingCountry'), 'add_empty' => false)),
      'type_of_visa'             => new sfWidgetFormInputText(),
      'trip_money'               => new sfWidgetFormInputText(),
      'local_company_name'       => new sfWidgetFormInputText(),
      'local_company_address_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('VapLocalComanyAddress'), 'add_empty' => true)),
      'contagious_disease'       => new sfWidgetFormChoice(array('choices' => array('Yes' => 'Yes', 'No' => 'No'))),
      'police_case'              => new sfWidgetFormChoice(array('choices' => array('Yes' => 'Yes', 'No' => 'No'))),
      'narcotic_involvement'     => new sfWidgetFormChoice(array('choices' => array('Yes' => 'Yes', 'No' => 'No'))),
      'deported_status'          => new sfWidgetFormChoice(array('choices' => array('Yes' => 'Yes', 'No' => 'No'))),
      'deported_county_id'       => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('VapDeportedCountry'), 'add_empty' => true)),
      'visa_fraud_status'        => new sfWidgetFormChoice(array('choices' => array('Yes' => 'Yes', 'No' => 'No'))),
      'processing_country_id'    => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('ProcessingCountry'), 'add_empty' => true)),
      'processing_center_id'     => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('VapProcessingCentre'), 'add_empty' => true)),
      'ispaid'                   => new sfWidgetFormInputCheckbox(),
      'is_email_valid'           => new sfWidgetFormInputCheckbox(),
      'payment_gateway_id'       => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('PaymentGatewayType'), 'add_empty' => true)),
      'paid_dollar_amount'       => new sfWidgetFormInputText(),
      'paid_naira_amount'        => new sfWidgetFormInputText(),
      'paid_date'                => new sfWidgetFormDate(),
      'arrival_date'             => new sfWidgetFormDate(),
      'status'                   => new sfWidgetFormChoice(array('choices' => array('New' => 'New', 'Paid' => 'Paid', 'Vetted' => 'Vetted', 'Approved' => 'Approved', 'Rejected' => 'Rejected', 'Expired' => 'Expired'))),
      'payment_trans_id'         => new sfWidgetFormInputText(),
      'business_address'         => new sfWidgetFormInputText(),
      'sponsore_type'            => new sfWidgetFormChoice(array('choices' => array('SS' => 'SS', 'CS' => 'CS'))),
      'remarks'                  => new sfWidgetFormInputText(),
      'created_at'               => new sfWidgetFormDateTime(),
      'updated_at'               => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                       => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'ref_no'                   => new sfValidatorInteger(array('required' => false)),
      'title'                    => new sfValidatorChoice(array('choices' => array(0 => 'MR', 1 => 'MRS', 2 => 'MISS', 3 => 'DR'), 'required' => false)),
      'first_name'               => new sfValidatorString(array('max_length' => 50)),
      'surname'                  => new sfValidatorString(array('max_length' => 50)),
      'middle_name'              => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'gender'                   => new sfValidatorChoice(array('choices' => array(0 => 'Male', 1 => 'Female'), 'required' => false)),
      'marital_status'           => new sfValidatorChoice(array('choices' => array(0 => 'Single', 1 => 'Married', 2 => 'Widowed', 3 => 'Divorced', 4 => 'None'), 'required' => false)),
      'email'                    => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'date_of_birth'            => new sfValidatorDate(array('required' => false)),
      'place_of_birth'           => new sfValidatorString(array('max_length' => 100)),
      'present_nationality_id'   => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('CurrentCountry'), 'required' => false)),
      'hair_color'               => new sfValidatorChoice(array('choices' => array(0 => 'Black', 1 => 'Brown', 2 => 'White', 3 => 'Red', 4 => 'Gray', 5 => 'None'), 'required' => false)),
      'eyes_color'               => new sfValidatorChoice(array('choices' => array(0 => 'Brown', 1 => 'Blue', 2 => 'Green', 3 => 'Gray', 4 => 'None'), 'required' => false)),
      'id_marks'                 => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'height'                   => new sfValidatorInteger(array('required' => false)),
      'office_address_id'        => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('VapVisaOfficeAddress'), 'required' => false)),
      'office_phone_no'          => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'applicant_type'           => new sfValidatorChoice(array('choices' => array(0 => 'GB', 1 => 'PB'), 'required' => false)),
      'vap_company_id'           => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('VapCompany'), 'required' => false)),
      'customer_service_number'  => new sfValidatorString(array('max_length' => 150, 'required' => false)),
      'document_1'               => new sfValidatorString(array('max_length' => 150, 'required' => false)),
      'document_2'               => new sfValidatorString(array('max_length' => 150, 'required' => false)),
      'document_3'               => new sfValidatorString(array('max_length' => 150, 'required' => false)),
      'document_4'               => new sfValidatorString(array('max_length' => 150, 'required' => false)),
      'document_5'               => new sfValidatorString(array('max_length' => 150, 'required' => false)),
      'boarding_place'           => new sfValidatorString(array('max_length' => 150, 'required' => false)),
      'flight_carrier'           => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'flight_number'            => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'issusing_govt'            => new sfValidatorString(array('max_length' => 192)),
      'passport_number'          => new sfValidatorString(array('max_length' => 192)),
      'date_of_issue'            => new sfValidatorDate(),
      'date_of_exp'              => new sfValidatorDate(),
      'place_of_issue'           => new sfValidatorString(array('max_length' => 192)),
      'applying_country_id'      => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('VapApplyingCountry'))),
      'type_of_visa'             => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'trip_money'               => new sfValidatorNumber(array('required' => false)),
      'local_company_name'       => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'local_company_address_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('VapLocalComanyAddress'), 'required' => false)),
      'contagious_disease'       => new sfValidatorChoice(array('choices' => array(0 => 'Yes', 1 => 'No'), 'required' => false)),
      'police_case'              => new sfValidatorChoice(array('choices' => array(0 => 'Yes', 1 => 'No'), 'required' => false)),
      'narcotic_involvement'     => new sfValidatorChoice(array('choices' => array(0 => 'Yes', 1 => 'No'), 'required' => false)),
      'deported_status'          => new sfValidatorChoice(array('choices' => array(0 => 'Yes', 1 => 'No'), 'required' => false)),
      'deported_county_id'       => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('VapDeportedCountry'), 'required' => false)),
      'visa_fraud_status'        => new sfValidatorChoice(array('choices' => array(0 => 'Yes', 1 => 'No'), 'required' => false)),
      'processing_country_id'    => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('ProcessingCountry'), 'required' => false)),
      'processing_center_id'     => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('VapProcessingCentre'), 'required' => false)),
      'ispaid'                   => new sfValidatorBoolean(array('required' => false)),
      'is_email_valid'           => new sfValidatorBoolean(array('required' => false)),
      'payment_gateway_id'       => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('PaymentGatewayType'), 'required' => false)),
      'paid_dollar_amount'       => new sfValidatorNumber(array('required' => false)),
      'paid_naira_amount'        => new sfValidatorNumber(array('required' => false)),
      'paid_date'                => new sfValidatorDate(array('required' => false)),
      'arrival_date'             => new sfValidatorDate(),
      'status'                   => new sfValidatorChoice(array('choices' => array(0 => 'New', 1 => 'Paid', 2 => 'Vetted', 3 => 'Approved', 4 => 'Rejected', 5 => 'Expired'), 'required' => false)),
      'payment_trans_id'         => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'business_address'         => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'sponsore_type'            => new sfValidatorChoice(array('choices' => array(0 => 'SS', 1 => 'CS'), 'required' => false)),
      'remarks'                  => new sfValidatorPass(),
      'created_at'               => new sfValidatorDateTime(),
      'updated_at'               => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('vap_application[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'VapApplication';
  }

}
