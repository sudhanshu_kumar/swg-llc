<?php

/**
 * PassportApplicantReferences form base class.
 *
 * @method PassportApplicantReferences getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BasePassportApplicantReferencesForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'             => new sfWidgetFormInputHidden(),
      'application_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('PassportApplication'), 'add_empty' => true)),
      'name'           => new sfWidgetFormInputText(),
      'address_id'     => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('PassportReferenceAddress'), 'add_empty' => true)),
      'phone'          => new sfWidgetFormInputText(),
      'created_at'     => new sfWidgetFormDateTime(),
      'updated_at'     => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'             => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'application_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('PassportApplication'), 'required' => false)),
      'name'           => new sfValidatorString(array('max_length' => 70)),
      'address_id'     => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('PassportReferenceAddress'), 'required' => false)),
      'phone'          => new sfValidatorString(array('max_length' => 20)),
      'created_at'     => new sfValidatorDateTime(),
      'updated_at'     => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('passport_applicant_references[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'PassportApplicantReferences';
  }

}
