<?php

/**
 * VisaFee form base class.
 *
 * @method VisaFee getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseVisaFeeForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                => new sfWidgetFormInputHidden(),
      'country_id'        => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Country'), 'add_empty' => true)),
      'visa_cat_id'       => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('VisaCategory'), 'add_empty' => true)),
      'visa_type_id'      => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('VisaType'), 'add_empty' => true)),
      'entry_type_id'     => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('EntryType'), 'add_empty' => true)),
      'naira_amount'      => new sfWidgetFormInputText(),
      'dollar_amount'     => new sfWidgetFormInputText(),
      'is_fee_multiplied' => new sfWidgetFormInputCheckbox(),
      'is_gratis'         => new sfWidgetFormInputCheckbox(),
      'created_at'        => new sfWidgetFormDateTime(),
      'updated_at'        => new sfWidgetFormDateTime(),
      'created_by'        => new sfWidgetFormInputText(),
      'updated_by'        => new sfWidgetFormInputText(),
      'version'           => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'                => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'country_id'        => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Country'), 'required' => false)),
      'visa_cat_id'       => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('VisaCategory'), 'required' => false)),
      'visa_type_id'      => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('VisaType'), 'required' => false)),
      'entry_type_id'     => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('EntryType'), 'required' => false)),
      'naira_amount'      => new sfValidatorInteger(array('required' => false)),
      'dollar_amount'     => new sfValidatorInteger(array('required' => false)),
      'is_fee_multiplied' => new sfValidatorBoolean(array('required' => false)),
      'is_gratis'         => new sfValidatorBoolean(array('required' => false)),
      'created_at'        => new sfValidatorDateTime(),
      'updated_at'        => new sfValidatorDateTime(),
      'created_by'        => new sfValidatorInteger(array('required' => false)),
      'updated_by'        => new sfValidatorInteger(array('required' => false)),
      'version'           => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('visa_fee[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'VisaFee';
  }

}
