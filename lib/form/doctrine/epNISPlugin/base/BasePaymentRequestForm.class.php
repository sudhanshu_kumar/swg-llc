<?php

/**
 * PaymentRequest form base class.
 *
 * @method PaymentRequest getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BasePaymentRequestForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'             => new sfWidgetFormInputHidden(),
      'passport_id'    => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('PassportApplication'), 'add_empty' => true)),
      'visa_id'        => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('VisaApplication'), 'add_empty' => true)),
      'ecowas_id'      => new sfWidgetFormInputText(),
      'ecowas_card_id' => new sfWidgetFormInputText(),
      'service_id'     => new sfWidgetFormInputText(),
      'created_at'     => new sfWidgetFormDateTime(),
      'updated_at'     => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'             => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'passport_id'    => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('PassportApplication'), 'required' => false)),
      'visa_id'        => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('VisaApplication'), 'required' => false)),
      'ecowas_id'      => new sfValidatorInteger(array('required' => false)),
      'ecowas_card_id' => new sfValidatorInteger(array('required' => false)),
      'service_id'     => new sfValidatorInteger(array('required' => false)),
      'created_at'     => new sfValidatorDateTime(),
      'updated_at'     => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('payment_request[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'PaymentRequest';
  }

}
