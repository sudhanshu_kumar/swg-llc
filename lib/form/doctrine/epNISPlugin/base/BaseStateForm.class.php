<?php

/**
 * State form base class.
 *
 * @method State getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseStateForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                   => new sfWidgetFormInputHidden(),
      'state_name'           => new sfWidgetFormInputText(),
      'country_id'           => new sfWidgetFormInputText(),
      'is_mrp_state'         => new sfWidgetFormInputCheckbox(),
      'is_official_state'    => new sfWidgetFormInputCheckbox(),
      'is_mrp_seamans_state' => new sfWidgetFormInputCheckbox(),
      'created_at'           => new sfWidgetFormDateTime(),
      'updated_at'           => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                   => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'state_name'           => new sfValidatorString(array('max_length' => 150)),
      'country_id'           => new sfValidatorString(array('max_length' => 2)),
      'is_mrp_state'         => new sfValidatorBoolean(array('required' => false)),
      'is_official_state'    => new sfValidatorBoolean(array('required' => false)),
      'is_mrp_seamans_state' => new sfValidatorBoolean(array('required' => false)),
      'created_at'           => new sfValidatorDateTime(),
      'updated_at'           => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('state[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'State';
  }

}
