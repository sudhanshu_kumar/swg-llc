<?php

/**
 * VisaApplicantPreviousHistory form base class.
 *
 * @method VisaApplicantPreviousHistory getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseVisaApplicantPreviousHistoryForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'               => new sfWidgetFormInputHidden(),
      'application_id'   => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('VisaApplication'), 'add_empty' => true)),
      'startdate'        => new sfWidgetFormDate(),
      'endate'           => new sfWidgetFormDate(),
      'resid_address_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('VisaApplicantPreviousHistoryAddress'), 'add_empty' => true)),
      'created_at'       => new sfWidgetFormDateTime(),
      'updated_at'       => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'               => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'application_id'   => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('VisaApplication'), 'required' => false)),
      'startdate'        => new sfValidatorDate(array('required' => false)),
      'endate'           => new sfValidatorDate(array('required' => false)),
      'resid_address_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('VisaApplicantPreviousHistoryAddress'), 'required' => false)),
      'created_at'       => new sfValidatorDateTime(),
      'updated_at'       => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('visa_applicant_previous_history[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'VisaApplicantPreviousHistory';
  }

}
