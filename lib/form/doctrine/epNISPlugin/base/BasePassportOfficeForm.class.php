<?php

/**
 * PassportOffice form base class.
 *
 * @method PassportOffice getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BasePassportOfficeForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                  => new sfWidgetFormInputHidden(),
      'office_name'         => new sfWidgetFormInputText(),
      'office_address'      => new sfWidgetFormInputText(),
      'office_state_id'     => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('State'), 'add_empty' => false)),
      'office_country_id'   => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Country'), 'add_empty' => false)),
      'office_capacity'     => new sfWidgetFormInputText(),
      'is_epassport_active' => new sfWidgetFormChoice(array('choices' => array(1 => '1', 0 => '0'))),
      'created_at'          => new sfWidgetFormDateTime(),
      'updated_at'          => new sfWidgetFormDateTime(),
      'created_by'          => new sfWidgetFormInputText(),
      'updated_by'          => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'                  => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'office_name'         => new sfValidatorString(array('max_length' => 150, 'required' => false)),
      'office_address'      => new sfValidatorString(array('max_length' => 255)),
      'office_state_id'     => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('State'))),
      'office_country_id'   => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Country'))),
      'office_capacity'     => new sfValidatorInteger(array('required' => false)),
      'is_epassport_active' => new sfValidatorChoice(array('choices' => array(0 => '1', 1 => '0'), 'required' => false)),
      'created_at'          => new sfValidatorDateTime(),
      'updated_at'          => new sfValidatorDateTime(),
      'created_by'          => new sfValidatorInteger(array('required' => false)),
      'updated_by'          => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('passport_office[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'PassportOffice';
  }

}
