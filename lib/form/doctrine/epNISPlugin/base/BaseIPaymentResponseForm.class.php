<?php

/**
 * IPaymentResponse form base class.
 *
 * @method IPaymentResponse getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseIPaymentResponseForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                 => new sfWidgetFormInputHidden(),
      'payment_request_id' => new sfWidgetFormInputText(),
      'transaction_number' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('CartItemsTransactions'), 'add_empty' => true)),
      'paid_amount'        => new sfWidgetFormInputText(),
      'currency'           => new sfWidgetFormInputText(),
      'payment_status'     => new sfWidgetFormInputText(),
      'order_number'       => new sfWidgetFormInputText(),
      'created_at'         => new sfWidgetFormDateTime(),
      'updated_at'         => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                 => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'payment_request_id' => new sfValidatorInteger(array('required' => false)),
      'transaction_number' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('CartItemsTransactions'), 'required' => false)),
      'paid_amount'        => new sfValidatorNumber(array('required' => false)),
      'currency'           => new sfValidatorString(array('max_length' => 10, 'required' => false)),
      'payment_status'     => new sfValidatorString(array('max_length' => 5)),
      'order_number'       => new sfValidatorInteger(array('required' => false)),
      'created_at'         => new sfValidatorDateTime(),
      'updated_at'         => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('i_payment_response[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'IPaymentResponse';
  }

}
