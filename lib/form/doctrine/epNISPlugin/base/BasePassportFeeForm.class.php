<?php

/**
 * PassportFee form base class.
 *
 * @method PassportFee getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BasePassportFeeForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'              => new sfWidgetFormInputHidden(),
      'naira_amount'    => new sfWidgetFormInputText(),
      'dollar_amount'   => new sfWidgetFormInputText(),
      'passporttype_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('PassportAppType'), 'add_empty' => true)),
      'created_at'      => new sfWidgetFormDateTime(),
      'updated_at'      => new sfWidgetFormDateTime(),
      'version'         => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'              => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'naira_amount'    => new sfValidatorInteger(array('required' => false)),
      'dollar_amount'   => new sfValidatorInteger(array('required' => false)),
      'passporttype_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('PassportAppType'), 'required' => false)),
      'created_at'      => new sfValidatorDateTime(),
      'updated_at'      => new sfValidatorDateTime(),
      'version'         => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('passport_fee[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'PassportFee';
  }

}
