<?php

/**
 * TblRevenueEditRequest form base class.
 *
 * @method TblRevenueEditRequest getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseTblRevenueEditRequestForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'             => new sfWidgetFormInputHidden(),
      'current_amount' => new sfWidgetFormInputText(),
      'date'           => new sfWidgetFormDate(),
      'project_id'     => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('TblProjectName'), 'add_empty' => false)),
      'account_id'     => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('TblBankDetails'), 'add_empty' => false)),
      'comments'       => new sfWidgetFormInputText(),
      'admin_comments' => new sfWidgetFormInputText(),
      'status'         => new sfWidgetFormChoice(array('choices' => array('Requested' => 'Requested', 'Approved' => 'Approved', 'Declined' => 'Declined'))),
      'created_at'     => new sfWidgetFormDateTime(),
      'updated_at'     => new sfWidgetFormDateTime(),
      'created_by'     => new sfWidgetFormInputText(),
      'updated_by'     => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'             => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'current_amount' => new sfValidatorInteger(),
      'date'           => new sfValidatorDate(),
      'project_id'     => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('TblProjectName'))),
      'account_id'     => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('TblBankDetails'))),
      'comments'       => new sfValidatorPass(),
      'admin_comments' => new sfValidatorPass(),
      'status'         => new sfValidatorChoice(array('choices' => array(0 => 'Requested', 1 => 'Approved', 2 => 'Declined'), 'required' => false)),
      'created_at'     => new sfValidatorDateTime(),
      'updated_at'     => new sfValidatorDateTime(),
      'created_by'     => new sfValidatorInteger(array('required' => false)),
      'updated_by'     => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('tbl_revenue_edit_request[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'TblRevenueEditRequest';
  }

}
