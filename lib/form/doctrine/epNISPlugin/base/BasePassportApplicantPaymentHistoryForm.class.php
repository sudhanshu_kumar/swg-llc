<?php

/**
 * PassportApplicantPaymentHistory form base class.
 *
 * @method PassportApplicantPaymentHistory getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BasePassportApplicantPaymentHistoryForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                  => new sfWidgetFormInputHidden(),
      'application_id'      => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('PassportApplication'), 'add_empty' => true)),
      'payment_date'        => new sfWidgetFormDateTime(),
      'payment_mode_id'     => new sfWidgetFormInputText(),
      'payment_category_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('GlobalMaster'), 'add_empty' => false)),
      'payment_flg'         => new sfWidgetFormInputText(),
      'created_at'          => new sfWidgetFormDateTime(),
      'updated_at'          => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                  => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'application_id'      => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('PassportApplication'), 'required' => false)),
      'payment_date'        => new sfValidatorDateTime(array('required' => false)),
      'payment_mode_id'     => new sfValidatorInteger(),
      'payment_category_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('GlobalMaster'))),
      'payment_flg'         => new sfValidatorString(array('max_length' => 1)),
      'created_at'          => new sfValidatorDateTime(),
      'updated_at'          => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('passport_applicant_payment_history[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'PassportApplicantPaymentHistory';
  }

}
