<?php

/**
 * VisaType form base class.
 *
 * @method VisaType getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedInheritanceTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseVisaTypeForm extends GlobalMasterForm
{
  protected function setupInheritance()
  {
    parent::setupInheritance();

    $this->widgetSchema->setNameFormat('visa_type[%s]');
  }

  public function getModelName()
  {
    return 'VisaType';
  }

}
