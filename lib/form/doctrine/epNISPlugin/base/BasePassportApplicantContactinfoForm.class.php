<?php

/**
 * PassportApplicantContactinfo form base class.
 *
 * @method PassportApplicantContactinfo getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BasePassportApplicantContactinfoForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'              => new sfWidgetFormInputHidden(),
      'secondory_email' => new sfWidgetFormInputText(),
      'nationality_id'  => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Country'), 'add_empty' => true)),
      'state_of_origin' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('State'), 'add_empty' => true)),
      'contact_phone'   => new sfWidgetFormInputText(),
      'mobile_phone'    => new sfWidgetFormInputText(),
      'occupation'      => new sfWidgetFormInputText(),
      'home_town'       => new sfWidgetFormInputText(),
      'home_phone'      => new sfWidgetFormInputText(),
      'application_id'  => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('PassportApplication'), 'add_empty' => true)),
      'created_at'      => new sfWidgetFormDateTime(),
      'updated_at'      => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'              => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'secondory_email' => new sfValidatorString(array('max_length' => 70, 'required' => false)),
      'nationality_id'  => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Country'), 'required' => false)),
      'state_of_origin' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('State'), 'required' => false)),
      'contact_phone'   => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'mobile_phone'    => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'occupation'      => new sfValidatorString(array('max_length' => 150, 'required' => false)),
      'home_town'       => new sfValidatorString(array('max_length' => 70, 'required' => false)),
      'home_phone'      => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'application_id'  => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('PassportApplication'), 'required' => false)),
      'created_at'      => new sfValidatorDateTime(),
      'updated_at'      => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('passport_applicant_contactinfo[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'PassportApplicantContactinfo';
  }

}
