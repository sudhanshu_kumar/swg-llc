<?php

/**
 * VapLocalComanyAddress form base class.
 *
 * @method VapLocalComanyAddress getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedInheritanceTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseVapLocalComanyAddressForm extends AddressMasterForm
{
  protected function setupInheritance()
  {
    parent::setupInheritance();

    $this->widgetSchema->setNameFormat('vap_local_comany_address[%s]');
  }

  public function getModelName()
  {
    return 'VapLocalComanyAddress';
  }

}
