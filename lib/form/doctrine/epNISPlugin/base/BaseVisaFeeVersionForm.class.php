<?php

/**
 * VisaFeeVersion form base class.
 *
 * @method VisaFeeVersion getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseVisaFeeVersionForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                => new sfWidgetFormInputHidden(),
      'country_id'        => new sfWidgetFormInputText(),
      'visa_cat_id'       => new sfWidgetFormInputText(),
      'visa_type_id'      => new sfWidgetFormInputText(),
      'entry_type_id'     => new sfWidgetFormInputText(),
      'naira_amount'      => new sfWidgetFormInputText(),
      'dollar_amount'     => new sfWidgetFormInputText(),
      'is_fee_multiplied' => new sfWidgetFormInputCheckbox(),
      'is_gratis'         => new sfWidgetFormInputCheckbox(),
      'created_at'        => new sfWidgetFormDateTime(),
      'updated_at'        => new sfWidgetFormDateTime(),
      'created_by'        => new sfWidgetFormInputText(),
      'updated_by'        => new sfWidgetFormInputText(),
      'version'           => new sfWidgetFormInputHidden(),
    ));

    $this->setValidators(array(
      'id'                => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'country_id'        => new sfValidatorString(array('max_length' => 2, 'required' => false)),
      'visa_cat_id'       => new sfValidatorInteger(array('required' => false)),
      'visa_type_id'      => new sfValidatorInteger(array('required' => false)),
      'entry_type_id'     => new sfValidatorInteger(array('required' => false)),
      'naira_amount'      => new sfValidatorInteger(array('required' => false)),
      'dollar_amount'     => new sfValidatorInteger(array('required' => false)),
      'is_fee_multiplied' => new sfValidatorBoolean(array('required' => false)),
      'is_gratis'         => new sfValidatorBoolean(array('required' => false)),
      'created_at'        => new sfValidatorDateTime(),
      'updated_at'        => new sfValidatorDateTime(),
      'created_by'        => new sfValidatorInteger(array('required' => false)),
      'updated_by'        => new sfValidatorInteger(array('required' => false)),
      'version'           => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'version', 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('visa_fee_version[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'VisaFeeVersion';
  }

}
