<?php

/**
 * GenderStatus form base class.
 *
 * @method GenderStatus getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedInheritanceTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseGenderStatusForm extends GlobalMasterForm
{
  protected function setupInheritance()
  {
    parent::setupInheritance();

    $this->widgetSchema->setNameFormat('gender_status[%s]');
  }

  public function getModelName()
  {
    return 'GenderStatus';
  }

}
