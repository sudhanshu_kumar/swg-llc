<?php

/**
 * PassportApplicationDetails form base class.
 *
 * @method PassportApplicationDetails getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BasePassportApplicationDetailsForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                        => new sfWidgetFormInputHidden(),
      'request_type_id'           => new sfWidgetFormChoice(array('choices' => array('Adult' => 'Adult', 'Minor' => 'Minor', 'None' => 'None'))),
      'correspondence_address'    => new sfWidgetFormInputText(),
      'permanent_address_id'      => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('PassportPermanentAddress'), 'add_empty' => true)),
      'contact_address_id'        => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('PassportContactAddress'), 'add_empty' => true)),
      'city'                      => new sfWidgetFormInputText(),
      'country_id'                => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Country'), 'add_empty' => true)),
      'stateoforigin'             => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('State'), 'add_empty' => true)),
      'specialfeatures'           => new sfWidgetFormInputText(),
      'application_id'            => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('PassportApplication'), 'add_empty' => true)),
      'employer'                  => new sfWidgetFormInputText(),
      'district'                  => new sfWidgetFormInputText(),
      'seamans_discharge_book'    => new sfWidgetFormInputText(),
      'seamans_previous_passport' => new sfWidgetFormInputText(),
      'overseas_address_id'       => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('PassportOverseasAddress'), 'add_empty' => true)),
      'created_at'                => new sfWidgetFormDateTime(),
      'updated_at'                => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                        => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'request_type_id'           => new sfValidatorChoice(array('choices' => array(0 => 'Adult', 1 => 'Minor', 2 => 'None'), 'required' => false)),
      'correspondence_address'    => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'permanent_address_id'      => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('PassportPermanentAddress'), 'required' => false)),
      'contact_address_id'        => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('PassportContactAddress'), 'required' => false)),
      'city'                      => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'country_id'                => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Country'), 'required' => false)),
      'stateoforigin'             => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('State'), 'required' => false)),
      'specialfeatures'           => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'application_id'            => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('PassportApplication'), 'required' => false)),
      'employer'                  => new sfValidatorString(array('max_length' => 150, 'required' => false)),
      'district'                  => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'seamans_discharge_book'    => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'seamans_previous_passport' => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'overseas_address_id'       => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('PassportOverseasAddress'), 'required' => false)),
      'created_at'                => new sfValidatorDateTime(),
      'updated_at'                => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('passport_application_details[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'PassportApplicationDetails';
  }

}
