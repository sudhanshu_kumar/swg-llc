<?php

/**
 * IPaymentRequestTransaction form base class.
 *
 * @method IPaymentRequestTransaction getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseIPaymentRequestTransactionForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                 => new sfWidgetFormInputHidden(),
      'payment_request_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('IPaymentRequest'), 'add_empty' => true)),
      'transaction_number' => new sfWidgetFormInputText(),
      'created_at'         => new sfWidgetFormDateTime(),
      'updated_at'         => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                 => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'payment_request_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('IPaymentRequest'), 'required' => false)),
      'transaction_number' => new sfValidatorInteger(array('required' => false)),
      'created_at'         => new sfValidatorDateTime(),
      'updated_at'         => new sfValidatorDateTime(),
    ));

    $this->validatorSchema->setPostValidator(
      new sfValidatorDoctrineUnique(array('model' => 'IPaymentRequestTransaction', 'column' => array('transaction_number')))
    );

    $this->widgetSchema->setNameFormat('i_payment_request_transaction[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'IPaymentRequestTransaction';
  }

}
