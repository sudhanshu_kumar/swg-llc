<?php

/**
 * Execution form base class.
 *
 * @method Execution getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseExecutionForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'execution_id'             => new sfWidgetFormInputHidden(),
      'workflow_id'              => new sfWidgetFormInputText(),
      'execution_parent'         => new sfWidgetFormInputText(),
      'execution_started'        => new sfWidgetFormInputText(),
      'execution_variables'      => new sfWidgetFormTextarea(),
      'execution_waiting_for'    => new sfWidgetFormTextarea(),
      'execution_threads'        => new sfWidgetFormTextarea(),
      'execution_next_thread_id' => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'execution_id'             => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'execution_id', 'required' => false)),
      'workflow_id'              => new sfValidatorInteger(),
      'execution_parent'         => new sfValidatorInteger(),
      'execution_started'        => new sfValidatorInteger(),
      'execution_variables'      => new sfValidatorString(),
      'execution_waiting_for'    => new sfValidatorString(),
      'execution_threads'        => new sfValidatorString(),
      'execution_next_thread_id' => new sfValidatorInteger(),
    ));

    $this->widgetSchema->setNameFormat('execution[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Execution';
  }

}
