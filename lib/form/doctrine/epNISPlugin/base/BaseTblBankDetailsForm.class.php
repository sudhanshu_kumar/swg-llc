<?php

/**
 * TblBankDetails form base class.
 *
 * @method TblBankDetails getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseTblBankDetailsForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'             => new sfWidgetFormInputHidden(),
      'bank_name'      => new sfWidgetFormInputText(),
      'account_number' => new sfWidgetFormInputText(),
      'account_name'   => new sfWidgetFormInputText(),
      'city'           => new sfWidgetFormInputText(),
      'state'          => new sfWidgetFormInputText(),
      'country'        => new sfWidgetFormInputText(),
      'project_id'     => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('TblProjectName'), 'add_empty' => false)),
      'created_at'     => new sfWidgetFormDateTime(),
      'updated_at'     => new sfWidgetFormDateTime(),
      'created_by'     => new sfWidgetFormInputText(),
      'updated_by'     => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'             => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'bank_name'      => new sfValidatorPass(),
      'account_number' => new sfValidatorPass(),
      'account_name'   => new sfValidatorPass(),
      'city'           => new sfValidatorPass(),
      'state'          => new sfValidatorPass(),
      'country'        => new sfValidatorPass(),
      'project_id'     => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('TblProjectName'))),
      'created_at'     => new sfValidatorDateTime(),
      'updated_at'     => new sfValidatorDateTime(),
      'created_by'     => new sfValidatorInteger(array('required' => false)),
      'updated_by'     => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('tbl_bank_details[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'TblBankDetails';
  }

}
