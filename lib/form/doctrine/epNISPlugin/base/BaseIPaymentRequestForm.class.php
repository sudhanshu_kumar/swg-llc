<?php

/**
 * IPaymentRequest form base class.
 *
 * @method IPaymentRequest getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseIPaymentRequestForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                      => new sfWidgetFormInputHidden(),
      'passport_id'             => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('PassportApplication'), 'add_empty' => true)),
      'visa_id'                 => new sfWidgetFormInputText(),
      'freezone_id'             => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('VisaApplication'), 'add_empty' => true)),
      'visa_arrival_program_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('VapApplication'), 'add_empty' => true)),
      'created_at'              => new sfWidgetFormDateTime(),
      'updated_at'              => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                      => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'passport_id'             => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('PassportApplication'), 'required' => false)),
      'visa_id'                 => new sfValidatorInteger(array('required' => false)),
      'freezone_id'             => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('VisaApplication'), 'required' => false)),
      'visa_arrival_program_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('VapApplication'), 'required' => false)),
      'created_at'              => new sfValidatorDateTime(),
      'updated_at'              => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('i_payment_request[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'IPaymentRequest';
  }

}
