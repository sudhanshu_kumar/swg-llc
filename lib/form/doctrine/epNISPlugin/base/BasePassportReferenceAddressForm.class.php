<?php

/**
 * PassportReferenceAddress form base class.
 *
 * @method PassportReferenceAddress getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedInheritanceTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BasePassportReferenceAddressForm extends AddressMasterForm
{
  protected function setupInheritance()
  {
    parent::setupInheritance();

    $this->widgetSchema->setNameFormat('passport_reference_address[%s]');
  }

  public function getModelName()
  {
    return 'PassportReferenceAddress';
  }

}
