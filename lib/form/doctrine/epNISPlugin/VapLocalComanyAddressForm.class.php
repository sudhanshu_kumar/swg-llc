<?php

/**
 * VapLocalComanyAddress form.
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrinePluginFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class VapLocalComanyAddressForm extends PluginVapLocalComanyAddressForm
{
  /**
   * @see AddressMasterForm
   */
  public function configure()
  {
    $requiredFlag = $this->getOption('requiredFlag');

    $this->widgetSchema['address_1'] = new sfWidgetFormInput(array('label' => 'Address 1'));
    $this->widgetSchema['address_2'] = new sfWidgetFormInput(array('label' => 'Address 2'));
    $this->widgetSchema['city'] = new sfWidgetFormInput(array('label' => 'City'));
    $this->widgetSchema['country_id'] = new sfWidgetFormChoice(array('choices' => array('NG' =>'Nigeria')),array('label'=>'Country'));
    
    //$this->widgetSchema['country_id']  = new sfWidgetFormChoice(array('choices' => array('NG' =>'Nigeria')));
    $this->widgetSchema['state'] = new sfWidgetFormDoctrineChoice(array('model' => 'State', 'add_empty' => '-- Please Select --','label' => 'State'));
    $this->widgetSchema['lga_id'] = new sfWidgetFormChoice(array('choices'=>array(''=>'Please select LGA')),array('label' => 'LGA'));
    //$this->widgetSchema['state']  = new sfWidgetFormChoice(array('choices' => array('' =>'-- Please Select --')));

    //$this->widgetSchema['lga_id'] = new sfWidgetFormChoice(array('choices' => array('' =>'-- Please Select --')));
    $this->widgetSchema['district'] = new sfWidgetFormInput(array('label' => 'District'));
    $this->widgetSchema['postcode'] = new sfWidgetFormInput(array('label' => 'Postcode'));


    $this->validatorSchema['postcode'] = new sfValidatorString(array('max_length' => 10,'required' =>false),array('max_length'=>'Postcode can not  be more than 10 characters.'));
    $this->validatorSchema['address_2'] = new sfValidatorString(array('max_length' => 100,'required' =>false),array('max_length'=>'Address 2 can not  be more than 100 characters.'));
    $this->validatorSchema['address_1'] = new sfValidatorString(array('max_length' => 100, 'required' => $requiredFlag),array('required' => 'Address 1 is required.','max_length'=>'Address 1 can not  be more than 100 characters.'));
    $this->validatorSchema['city'] = new sfValidatorString(array('max_length' => 100, 'required' => $requiredFlag),array('required' => 'City is required.','max_length'=>'City can not  be more than 100 characters.'));
    $this->validatorSchema['state']  = new sfValidatorDoctrineChoice(array('model' => 'State', 'required' => $requiredFlag),array('required'=>'State is required.'));
    $this->validatorSchema['district'] = new sfValidatorString(array('max_length' => 100,'required' =>false),array('max_length'=>'District can not  be more than 100 characters.'));

    
  }
}
