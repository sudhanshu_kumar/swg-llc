<?php

/**
 * VApplicantOneYearsTravelHistory form.
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrinePluginFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class VApplicantOneYearsTravelHistoryForm extends PluginVApplicantOneYearsTravelHistoryForm
{
  public function configure()
  {
    //Unset Field type
    unset($this['created_at'],$this['updated_at'],
      $this['application_id'],$this['duration_year']);

    $this->widgetSchema['country_id']->setOption('add_empty','-- Please Select --');
    $this->widgetSchema['country_id']->setOption('query',CountryTable::getCachedQuery());
    $this->widgetSchema['date_of_departure'] = new sfWidgetFormDateCal(array('years'=>WidgetHelpers::getDateRanges()));
    //Set Field Label
    //Set Field Label
    $this->widgetSchema->setLabels(
      array('country_id'    => 'Country','date_of_departure' =>'Date of departure(dd-mm-yyyy)'));

    $this->validatorSchema['date_of_departure']  = new sfValidatorDate(array('max'=> date('Y-m-d'), 'required' => false), array('invalid' => 'Date of departure is invalid.','max'=>'Date of departure should not be greater than '.date('d/m/Y').'.'));

    
    //Set Field Validation
    $this->validatorSchema['city'] = new sfValidatorRegex(array('pattern' => '/^[a-zA-Z ]*$/','max_length' => 30,'required'=> false),
      array('max_length'=>'City can not be more than 30 characters.', 'invalid'=>'City is not valid.' ));


  }
}
