<?php

/**
 * PassportApplicantContactinfo form.
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrinePluginFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class PassportApplicantContactinfoForm extends PluginPassportApplicantContactinfoForm
{
  public function configure()
  {
      /**
       * Unset the fields
       */
    unset(
           $this['created_at'], $this['updated_at'],$this['application_id'],$this['state_of_origin']
         );

     $nigeriaId = Doctrine::getTable('Country')->getNigeriaId();
     $this->widgetSchema['nationality_id']  = new sfWidgetFormSelect(array('choices' => array( $nigeriaId => 'Nigeria')));

	   /**
     * Custom Label
     */
      $this->widgetSchema->setLabels(array( 'nationality_id'    => 'Nationality '));
      $this->widgetSchema->setLabels(array( 'home_town'    => 'Home town '));
	   /**
     * Custom validation message
     */
     $this->validatorSchema['nationality_id'] =     new sfValidatorDoctrineChoice(array('model' => 'Country'),array('required' => 'Nationality is required.'));
     $this->validatorSchema['occupation'] =         new sfValidatorString(array('max_length' => 30),array('required' => 'Occupation is required.','max_length' => 'Occupation can not be more than 30 characters.'));

     $this->validatorSchema['home_town'] = new sfValidatorAnd(array(
        new sfValidatorString(array('max_length' => 20),array('required' => 'Home Town is required.','max_length'=>'Home Town can not be more than 20 characters.')),
        new sfValidatorRegex(array('pattern' => '/^[a-zA-Z0-9\-\ \.]*$/'),array('invalid' => 'Home Town is invalid.','required' => 'Home Town is required.'))
      ),
      array('halt_on_error' => true),
      array('required' => 'Home Town is required')
    );

     $this->validatorSchema['contact_phone'] =      new sfValidatorString(array('max_length' => 20,'min_length' => 6, 'required' => true),array('max_length' => 'Contact phone can not be more than 20 characters.','min_length'=>'Phone number should be minimum 6 digits including + and spaces.',));
     $this->validatorSchema['mobile_phone'] =      new sfValidatorString(array('max_length' => 14,'min_length' => 10,'required' => false),array('max_length' => 'Mobile phone can not be more than 14 characters.','min_length'=>'Mobile number should be minimum 10 digits including + and spaces.'));

 
     $this->validatorSchema['contact_phone'] = new sfValidatorAnd(array(
      new sfValidatorString(array('max_length' => 20,'min_length' => 6,'required' => true),array('min_length'=>'Phone number should be minimum 6 digits including +.','max_length'=>'Phone number should be maximum 20 digits including +.')),
      new sfValidatorRegex(array('pattern' => '/^[0-9+-]*$/'),array('invalid' => 'Contact phone is not valid.'))
      ),array('halt_on_error' => true),array('required' => 'Contact phone is required')
    );
/*
$this->validatorSchema['mobile_phone'] = new sfValidatorAnd(array(
      new sfValidatorString(array('max_length' => 13,'min_length' => 10,'required' => false),array('min_length'=>'Mobile number should be minimum 10 digits including + and spaces.','max_length'=>'Mobile number should be maximum 13 digits including + and spaces.')),
      new sfValidatorRegex(array('pattern' => '/[0-9]/'),array('invalid' => 'Mobile number is not valid.'))
      ),array('halt_on_error' => true)
    );

    $this->validatorSchema['home_phone'] = new sfValidatorAnd(array(
      new sfValidatorString(array('max_length' => 20,'min_length' => 6),array('required' => 'Home phone is required.','min_length'=>'Home number should be minimum 10 digits including + and spaces.')),
      new sfValidatorRegex(array('pattern' => '/[0-9+-]/'),array('invalid' => 'Home number is not valid.'))
      ),array('halt_on_error' => true),array('required' => 'Home phone is required')
    );



 */

     }
}
