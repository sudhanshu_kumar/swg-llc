<?php

/**
 * VisaFee form.
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrinePluginFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class VisaFeeForm extends PluginVisaFeeForm
{
  public function configure()
  {
    unset($this['created_at'],$this['updated_at'],$this['visa_cat_id'],$this['visa_type_id'],$this['entry_type_id']
      ,$this['naira_amount'],$this['dollar_amount'],$this['is_fee_multiplied'],$this['is_gratis']);
  }
}
