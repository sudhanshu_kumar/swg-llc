<?php

class EditVisaApplicationForm extends VisaMasterForm
{
  public function configure()
  {
      unset ($this['interview_date'], $this['id'],$this['visacategory_id'],$this['zone_type_id'],$this['ref_no'],$this['email'],$this['place_of_birth'],$this['id_marks'],$this['height'],$this['present_nationality_id'],$this['previous_nationality_id'],$this['permanent_address_id'],$this['perm_phone_no'],$this['profession'],$this['office_address_id'],$this['office_phone_no'],$this['milltary_in'],$this['military_dt_from'],$this['military_dt_to'],$this['ispaid'],$this['payment_trans_id'],$this['term_chk_flg'],$this['status'],$this['payment_gateway_id'],$this['paid_dollar_amount'],$this['paid_naira_amount'],$this['paid_at'],$this['is_email_valid'],$this['created_at'],$this['updated_at']);

      $this->widgetSchema['date_of_birth'] = new sfWidgetFormDateCal(array('years'=>WidgetHelpers::getDateRanges()));
      $this->validatorSchema['date_of_birth']  = new sfValidatorDate(array('max'=> time()), array('required' => 'Date of birth is required.','max'=>'Date of birth should be less than today.', 'invalid' => 'Invalid date of birth.'));
      $this->widgetSchema['hair_color'] = new sfWidgetFormChoice(array('choices' => array('Black' => 'Black', 'Brown' => 'Brown', 'White' => 'White', 'Gray' => 'Gray','Red'=>'Red')));
      $this->widgetSchema['eyes_color'] = new sfWidgetFormChoice(array('choices' => array('Brown' => 'Brown', 'Blue' => 'Blue', 'Green' => 'Green', 'Gray' => 'Gray')));
      $this->widgetSchema['marital_status'] = new sfWidgetFormChoice(array('choices' => array('Single' => 'Single', 'Married' => 'Married', 'Widowed' => 'Widowed', 'Divorced' => 'Divorced')));
      $this->widgetSchema['gender'] = new sfWidgetFormChoice(array('choices' => array('Male' => 'Male', 'Female' => 'Female')));
      $this->widgetSchema->setLabels(array('date_of_birth' => 'Date of Birth(DD/MM/YY)',
              'other_name' => 'First Name',
              'mid_name'   => 'Middle Name',
              'surname'    => 'Last Name',
          ));
//    $this->validatorSchema->setPostValidator(new sfValidatorAnd(array(new sfValidatorCallback(array('callback' => array($this, 'checInterviewDate'))))));

  }
  
// public function  checInterviewDate($validator, $values) {
//
//  $date =  strtotime($values['interview_date']);
//     if($date != '' && !empty($date)){
//         if($date < strtotime(date('Y-m-d'))){
//             $error = new sfValidatorError($validator,"Interview Date should not be past date");
//             throw new sfValidatorErrorSchema($validator,array('interview_date' => $error));
//
//         }
//     }
//     return $values;
// }
  
}