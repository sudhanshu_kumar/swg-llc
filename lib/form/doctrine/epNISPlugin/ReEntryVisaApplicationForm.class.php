<?php

/**
 * ReEntryVisaApplication form.
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrinePluginFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ReEntryVisaApplicationForm extends PluginReEntryVisaApplicationForm
{
  public function configure()
  {
    //Expiry date
    $expiry_date =  strtotime('+ 6 months');
    $purposed_date = strtotime('now');
    //Unset Field Name

$obj = sfContext::getInstance();
  $appReEntryTypeId= Doctrine::getTable('VisaZoneType')->getFreeZoneId();

$appReEntryTypeName = Doctrine::getTable('VisaZoneType')->getZoneTypeName($appReEntryTypeId);

if($appReEntryTypeName=='Conventional Zone'){
    unset($this['created_at'],$this['updated_at'],$this['ispaid'],$this['payment_trans_id'],$this['ref_no']
      ,$this['martial_status'],$this['application_id'],$this['processing_centre_id']);
}elseif($appReEntryTypeName=='Free Zone'){
    unset($this['created_at'],$this['updated_at'],$this['ispaid'],$this['payment_trans_id'],$this['ref_no']
      ,$this['martial_status'],$this['application_id'],$this['proposeddate'],$this['no_of_re_entry_type'],$this['visa_office_id'],$this['visa_state_id']);
}
    //Set Field Type
   // $this->widgetSchema['address'] =    new sfWidgetFormTextarea(array('label' => 'Address'));
    //$this->widgetSchema['employer_address'] =    new sfWidgetFormTextarea(array('label' => 'Employer Full Address<br>(Not P.O Box)'));
//    if(isset($_POST['zone_type'])){
//     $this->widgetSchema['zone_type']=new sfWidgetFormInputHidden($_POST['zone_type']);
//    }else{
//     $this->widgetSchema['zone_type']=new sfWidgetFormInputHidden();
//    }
    $this->widgetSchema['reason_for_visa_requiring'] =
    new sfWidgetFormTextarea(array('label' => 'Reason for requiring visa'));
    $this->widgetSchema['date_of_issue'] = new sfWidgetFormDateCal(array('years'=>WidgetHelpers::getDateRanges()));
    $this->widgetSchema['date_of_exp'] = new sfWidgetFormDateCal(array('years'=>WidgetHelpers::getDateRanges()));
    $this->widgetSchema['cerpac_date_of_issue'] = new sfWidgetFormDateCal(array('years'=>WidgetHelpers::getDateRanges()));
    $this->widgetSchema['cerpac_exp_date'] = new sfWidgetFormDateCal(array('years'=>WidgetHelpers::getDateRanges()));

    $this->widgetSchema['last_arrival_in_nigeria'] = new sfWidgetFormDateCal(array('years'=>WidgetHelpers::getDateRanges()));
if($appReEntryTypeName=='Conventional Zone'){
    $this->widgetSchema['proposeddate'] = new sfWidgetFormDateCal(array('years'=>WidgetHelpers::getDateRanges()));
    $this->widgetSchema['visa_type_id']->setOption('add_empty','-- Please Select --');
    $this->widgetSchema['visa_type_id']->setOption('query',VisaTypeTable::getCachedQuery());
    $this->widgetSchema['visa_state_id']->setOption('add_empty','-- Please Select --');
    $this->widgetSchema['visa_state_id']->setOption('query',StateTable::getCachedQuery());
    $this->widgetSchema['visa_office_id']->setOption('add_empty','-- Please Select --');
    $this->widgetSchema['visa_office_id']->setOption('query',VisaOfficeTable::getCachedQuery());
}elseif($appReEntryTypeName=='Free Zone'){
    $this->widgetSchema['processing_centre_id']->setOption('add_empty','-- Please Select --');
    $this->widgetSchema['processing_centre_id']->setOption('query',VisaProcessingCentreTable::getCachedQuery());
    $this->widgetSchema['visa_type_id']= new sfWidgetFormInputHidden();
    $this->widgetSchema['re_entry_category'] = new sfWidgetFormChoice(array('choices' => array('0'=>'Please Select','3' => 'Re-entry within 3 months', '6' => 'Re-entry within 6 months', '12' => 'Re-entry within 12 months')));

}

    $q = Doctrine_Query::create()
          ->from('Country c')
          ->where('c.id != ?', 'NG')
          ->orderBy('c.country_name');
    $countryListArr = array(''=>'-- Please Select --');
    $validcountryListArr = array();
    $countryData = $q->execute()->toArray();

    foreach ($countryData as $k => $v){
      $countryListArr[$v['country_name']] = $v['country_name'];
      $validcountryListArr[$v['country_name']] = $v['country_name'];
    }

    $this->widgetSchema['issusing_govt']  = new sfWidgetFormChoice(array('choices' => $countryListArr));

    //Set Radio Button
    $this->widgetSchema['re_entry_type'] = new sfWidgetFormDoctrineChoice(array
        ('model' => 'EntryType','expanded' => 'true', 'query' => EntryTypeTable::getCachedQuery())
    );
if($appReEntryTypeName=='Conventional Zone'){
    //Set Field Label
    $this->widgetSchema->setLabels(
      array(
                  'visacategory_id'    => 'Visa Category',
                  'visatype_id'    => 'Visa Type',
                  'nationality_id'    => 'Nationality',
                  'issusing_govt'   => 'Issuing Country',
                  'passport_number'   => 'Passport number',
                  'date_of_issue'   => 'Date of issue (dd-mm-yyyy)',
                  'date_of_exp'   => 'Expiry Date (dd-mm-yyyy)',
                  'place_of_issue'   => 'Place of Issue',
                  //'address'   => 'Address',
                  'profession'   => 'Profession',
                  'reason_for_visa_requiring'   => 'Reason for requiring visa',
                  'last_arrival_in_nigeria'   => 'Date of last arrival in Nigeria(dd-mm-yyyy)',
                  'proposeddate'   => 'Proposed Re-Entry date(dd-mm-yyyy)',
                  'employer_name'   => 'Name of Employer',
                  'employer_phone'      => 'Employer\'s Phone Number',
                  'cerpa_quota'      => 'CERPAC Number',
                  'cerpac_issuing_state'      => 'Issuing Code',
                  'cerpac_date_of_issue'      => 'Date of issue (dd-mm-yyyy)',
                  'cerpac_exp_date'      => 'Expiry date (dd-mm-yyyy)',
                  'issuing_cerpac_office'      => 'Issuing CERPAC Office',
                  'visa_type_id'      => 'Type of visa Required',
                  'visa_state_id'      => 'Visa Processing State',
                  'visa_office_id'      => 'Visa Processing Office',
                  'no_of_re_entry_type' => 'No. of Re-Entries'.(isset($_REQUEST['visa_application']['ReEntryApplicantInfo']['re_entry_type'])!=34?(isset($_REQUEST['visa_application']['ReEntryApplicantInfo'])?'':'<sup>*</sup>'):'<sup>*</sup>'),
                  're_entry_type' => 'Re-Entry Type'

      ));
}elseif($appReEntryTypeName=='Free Zone'){
     //Set Field Label
    $this->widgetSchema->setLabels(
      array(
                  'visacategory_id'    => 'Visa Category',
                  'visatype_id'    => 'Visa Type',
                  'nationality_id'    => 'Nationality',
                  'issusing_govt'   => 'Issuing country',
                  'passport_number'   => 'Passport number',
                  'date_of_issue'   => 'Date of issue (dd-mm-yyyy)',
                  'date_of_exp'   => 'Expiry Date (dd-mm-yyyy)',
                  'place_of_issue'   => 'Place of Issue',
                  //'address'   => 'Address',
                  'profession'   => 'Profession',
                  'reason_for_visa_requiring'   => 'Reason for requiring visa',
                  'last_arrival_in_nigeria'   => 'Date of last arrival in Nigeria(dd-mm-yyyy)',
                  're_entry_category' => 'Re-entry Category',
                  'employer_name'   => 'Name of Employer',
                  'employer_phone'      => 'Employer\'s Phone Number',
                  'cerpa_quota'      => 'CERPAC Number',
                  'cerpac_issuing_state'      => 'Issuing Code',
                  'cerpac_date_of_issue'      => 'Date of issue (dd-mm-yyyy)',
                  'cerpac_exp_date'      => 'Expiry date (dd-mm-yyyy)',
                  'issuing_cerpac_office'      => 'Issuing CERPAC Office',
                  'processing_centre_id'      => 'Free Zone Processing Centre',
                  're_entry_type' => 'Re-Entry Type'

      ));
}
    //'employer_name'   => 'Name of employer or Firm you are returning to,

    //Set Field Validation
//      $this->validatorSchema['issusing_govt']             = new sfValidatorRegex(array('pattern' => '/^[a-zA-Z]*$/','max_length' => 30),
//        array('required'=>'Issuing goverment is required.','max_length'=>'Issuing government can not be more than 30 characters',
//        'invalid'=>'Issuing goverment is not valid'));

      $this->validatorSchema['issusing_govt']  = new sfValidatorChoice(array('choices' => $validcountryListArr,'required'=>true),array('required' => 'Issuing country is required.'));

      $this->validatorSchema['passport_number']           = new sfValidatorRegex(array('pattern' => '/^[a-zA-Z0-9]*$/','max_length' => 20),
        array('required'=>'Passport number is required.','max_length'=>'Passport number can not be more than 20 characters',
        'invalid' => 'Passport Number is invalid.'));
      $this->validatorSchema['date_of_issue']             = new sfValidatorDate(array('max'=> time()),array('required' => 'Date of issue is required.','max' => 'Passport issue date is not valid.', 'invalid' => 'Invalid date of issue.'));
      $this->validatorSchema['date_of_exp']               = new sfValidatorDate(array('min' => $expiry_date),array('required' => 'Date of expiry is required.','min' => 'Passport must be valid for at least 6 months.', 'invalid' => 'Invalid expiry date.'));
      $this->validatorSchema['place_of_issue']            = new sfValidatorString(array('max_length' => 30),
        array('required'=>'Place of issue is required.','max_length'=>'Place of issue can not be more than 30 characters'));
     // $this->validatorSchema['address']                   = new sfValidatorString(array('max_length' => 255),array('required'=>'Address is required.','max_length'=>'Address can not be more than 255 characters'));
      $this->validatorSchema['profession']                = new sfValidatorString(array('max_length' => 50),
        array('required'=>'Profession is required.','max_length'=>'Profession can not be more than 50 characters.'));
      $this->validatorSchema['reason_for_visa_requiring'] = new sfValidatorString(array('max_length' => 255),array('required'=>'Reason of requiring visa is required.','max_length'=>'Reason of requiring visa can not be more than 255 characters'));
      $this->validatorSchema['last_arrival_in_nigeria']   = new sfValidatorDate(array('max'=> time()),array('required' => 'last arrival date is required.','max' => 'Last arrival date is not valid.', 'invalid' => 'Invalid date of last arrival.'));
      $this->validatorSchema['re_entry_type']             = new sfValidatorDoctrineChoice(
        array('model' => 'EntryType'),array('required' => 'Entry type is required.'));

      $this->validatorSchema['employer_name'] = new sfValidatorRegex(array('pattern' => sfConfig::get('app_name_exception_pattern'),'max_length' => 20,
     'required' => false),array('max_length' =>'Employer name can not be more than 20 characters.',
     'invalid' =>'Employer name is invalid.' ));

      $this->validatorSchema['employer_phone']            = new sfValidatorString(array('max_length' => 20, 'required' => false),array('max_length' => 'Employer phone can not be more than 20 digits.'));
      //$this->validatorSchema['employer_address']          = new sfValidatorString(array('max_length' => 100, 'required' => false),array('max_length' => 'Employer address can not be more than 100 characters.'));
      //$this->validatorSchema['cerpa_quota']               = new sfValidatorString(array('max_length' => 64,'required' => false),array('required'=>'CERPAC quota is required.','max_length'=>'CERPAC quota can not be more than 64 characters'));
      $this->validatorSchema['cerpa_quota'] = new sfValidatorRegex(array('pattern' => '/^[a-zA-Z0-9]*$/','max_length' => 64,'required' => false),array('max_length' =>'CERPAC quota can not be more than 64 characters.',
     'invalid' =>'CERPAC number is invalid.' ));
      $this->validatorSchema['cerpac_issuing_state']      = new sfValidatorString(array('max_length' => 64,'required' => false),array('required'=>'CERPAC issuing state is required.','max_length'=>'CERPAC issuing state can not be more than 64 characters'));
      $this->validatorSchema['cerpac_date_of_issue']      = new sfValidatorDate(array('required' => false),array('required' => 'Date of issue is required.', 'invalid' => 'Invalid date of issue.'));
      $this->validatorSchema['cerpac_exp_date']           = new sfValidatorDate(array('required' => false),array('required' => 'Exp date is required.', 'invalid' => 'Invalid expiry date.'));
      $this->validatorSchema['issuing_cerpac_office']     = new sfValidatorString(array('max_length' => 30,'required' => false),array('required'=>'CERPAC office is required.','max_length'=>'CERPAC office can not be more than 30 characters'));
      if($appReEntryTypeName=='Conventional Zone'){
        if($_REQUEST){
          $entryTypeId = Doctrine::getTable('EntryType')->getMultipleEntryType();
          if(isset($_REQUEST['visa_application']['ReEntryApplicantInfo']['re_entry_type']) && $_REQUEST['visa_application']['ReEntryApplicantInfo']['re_entry_type']==$entryTypeId){
             $this->validatorSchema['no_of_re_entry_type']       = new sfValidatorInteger(array('min'=>'1','max' => 100),
            array('max'=> 'No. of re entries can not be more than 100 times.','min'=>'No of re entry can not be less than 1.','invalid' => 'No. of re entries can not be more than 100 times.','required' => 'No. of re entries is required.'));
          }else{
            $this->validatorSchema['no_of_re_entry_type']       = new sfValidatorInteger(array('min'=>'1','max' => 100,'required' => false),
            array('max'=> 'No. of re entries can not be more than 100 times.','min'=>'No of re entry can not be less than 1.','invalid' => 'No. of re entries can not be more than 100 times.'));
          }
        }
//      $this->validatorSchema['no_of_re_entry_type']       = new sfValidatorInteger(array('min'=>'1','max' => 100,'required' => false),
//      array('max'=> 'No. of re entries can not be more than 100 times.','min'=>'No of re entry can not be less than 1.','invalid' => 'No. of re entries can not be more than 100 times.'));
      $this->validatorSchema['proposeddate']              = new sfValidatorDate(array('min' => $purposed_date),array('required' => 'Proposed date is required.','min'=>'Proposed date must be future date.'));
      $this->validatorSchema['visa_type_id']              = new sfValidatorDoctrineChoice(array('model' => 'VisaType'),array('required' => 'Visa type is required.'));
      $this->validatorSchema['visa_office_id']            = new sfValidatorDoctrineChoice(array('model' => 'VisaOffice'),array('required' => 'Visa office is required.'));
      $this->validatorSchema['visa_state_id']             = new sfValidatorDoctrineChoice(array('model' => 'State'),array('required' => 'Visa state is required.'));
}elseif($appReEntryTypeName=='Free Zone'){
      $this->validatorSchema['re_entry_category']         = new sfValidatorChoice(array('choices' => array('3' => '3', '6' => '6', '12' => '12')),array('required' => 'Re-entry Category is required.', 'invalid' => 'Please select Re-entry category.'));
      $this->validatorSchema['processing_centre_id']      = new sfValidatorDoctrineChoice(array('model' => 'VisaProcessingCentre'),array('required' => 'Free Zone Processing Centre is required.'));
if(isset($_POST['visa_application']['ReEntryApplicantInfo']['visa_type_id'])){
    $single= Doctrine::getTable('FreezoneSingleVisaType')->getFreeZoneVisaTypeId();
    $multiple= Doctrine::getTable('FreezoneMultipleVisaType')->getFreeZoneVisaTypeId();
     if($_POST['visa_application']['ReEntryApplicantInfo']['visa_type_id']==$multiple){
      $this->validatorSchema['visa_type_id']              = new sfValidatorDoctrineChoice(array('model' => 'FreezoneMultipleVisaType'),array('required' => false));
     }elseif($_POST['visa_application']['ReEntryApplicantInfo']['visa_type_id']==$single){
      $this->validatorSchema['visa_type_id']              = new sfValidatorDoctrineChoice(array('model' => 'FreezoneSingleVisaType'),array('required' => false));
     }
}

}
     //Compare issues date and expiry date
      $this->validatorSchema->setPostValidator(new sfValidatorAnd(array(
        new sfValidatorCallback(array('callback' => array($this, 'checkIssueDate'))),
        new sfValidatorCallback(array('callback' => array($this, 'checkLastArrivalDate'))),
        new sfValidatorCallback(array('callback' => array($this, 'checkCerpacDate'))))
      ));
  }

  public function checkIssueDate($validator, $values) {
    if (($values['date_of_issue'] != '') && ($values['date_of_exp'] != '')) {
        if($values['date_of_issue'] >= $values['date_of_exp']) {
          $error = new sfValidatorError($validator, 'Date of issue can not be greater than date of expiry');
          throw new sfValidatorErrorSchema($validator, array('date_of_issue' => $error));
        }
    }

    $date_of_birth = $_REQUEST['visa_application']['date_of_birth'];
    $date_of_birth_year = (($date_of_birth['year']=="")? '0':$date_of_birth['year']);
    $date_of_birth_month = (($date_of_birth['month']<10)? '0'.$date_of_birth['month']:$date_of_birth['month']);
    $date_of_birth_day = (($date_of_birth['day']<10)? '0'.$date_of_birth['day']:$date_of_birth['day']);
    $date_of_birth = mktime(0,0,0,$date_of_birth_month,$date_of_birth_day,$date_of_birth_year);
    $date_of_birth = date("Y-m-d", $date_of_birth);

    if (($date_of_birth != '0-0-0') && ($values['date_of_issue'] != '')) {
     // strtotime give previous date as small numeric value like if dates are 2008-07-01 and 2009-07-02   1246386600,1246473000,
     //then the date comarision will be made like 1246386600 > 1246473000

     if(strtotime($date_of_birth) > strtotime($values['date_of_issue'])){
        $error = new sfValidatorError($validator, 'Date of issue should be greater than date of birth.');
        throw new sfValidatorErrorSchema($validator, array('date_of_issue' => $error));
      }
    }
    return $values;
  }

  public function checkCerpacDate($validator, $values) {
    if (($values['cerpac_date_of_issue'] != '') && ($values['cerpac_exp_date'] != '')) {
        if($values['cerpac_date_of_issue'] >= $values['cerpac_exp_date']) {
          $error = new sfValidatorError($validator, 'Date of issue can not be greater than date of expiry');
          throw new sfValidatorErrorSchema($validator, array('cerpac_date_of_issue' => $error));
        }
    }

    $date_of_birth = $_REQUEST['visa_application']['date_of_birth'];
    $date_of_birth_year = (($date_of_birth['year']=="")? '0':$date_of_birth['year']);
    $date_of_birth_month = (($date_of_birth['month']<10)? '0'.$date_of_birth['month']:$date_of_birth['month']);
    $date_of_birth_day = (($date_of_birth['day']<10)? '0'.$date_of_birth['day']:$date_of_birth['day']);
    $date_of_birth = mktime(0,0,0,$date_of_birth_month,$date_of_birth_day,$date_of_birth_year);
    $date_of_birth = date("Y-m-d", $date_of_birth);

    if (($date_of_birth != '0-0-0') && ($values['cerpac_date_of_issue'] != '')) {
     // strtotime give previous date as small numeric value like if dates are 2008-07-01 and 2009-07-02   1246386600,1246473000,
     //then the date comarision will be made like 1246386600 > 1246473000

     if(strtotime($date_of_birth) > strtotime($values['cerpac_date_of_issue'])){
        $error = new sfValidatorError($validator, 'Date of issue of CERPAC should be greater than date of birth.');
        throw new sfValidatorErrorSchema($validator, array('cerpac_date_of_issue' => $error));
      }
    }
    return $values;
  }

  public function checkLastArrivalDate($validator, $values) {
    $date_of_birth = $_REQUEST['visa_application']['date_of_birth'];
    $date_of_birth_year = (($date_of_birth['year']=="")? '0':$date_of_birth['year']);
    $date_of_birth_month = (($date_of_birth['month']<10)? '0'.$date_of_birth['month']:$date_of_birth['month']);
    $date_of_birth_day = (($date_of_birth['day']<10)? '0'.$date_of_birth['day']:$date_of_birth['day']);
    $date_of_birth = mktime(0,0,0,$date_of_birth_month,$date_of_birth_day,$date_of_birth_year);
    $date_of_birth = date("Y-m-d", $date_of_birth);

    if (($date_of_birth != '0-0-0') && ($values['last_arrival_in_nigeria'] != '')) {
     // strtotime give previous date as small numeric value like if dates are 2008-07-01 and 2009-07-02   1246386600,1246473000,
     //then the date comarision will be made like 1246386600 > 1246473000

     if(strtotime($date_of_birth) > strtotime($values['last_arrival_in_nigeria'])){
        $error = new sfValidatorError($validator, 'Date of last arrival in Nigera should be greater than date of birth.');
        throw new sfValidatorErrorSchema($validator, array('last_arrival_in_nigeria' => $error));
      }
    }

    if (($values['date_of_issue'] !='') && ($values['last_arrival_in_nigeria'] != '')) {
     // strtotime give previous date as small numeric value like if dates are 2008-07-01 and 2009-07-02   1246386600,1246473000,
     //then the date comarision will be made like 1246386600 > 1246473000

     if(strtotime($values['date_of_issue']) > strtotime($values['last_arrival_in_nigeria'])){
        $error = new sfValidatorError($validator, 'Date of last arrival in Nigera should be greater than date of issue of passport.');
        throw new sfValidatorErrorSchema($validator, array('last_arrival_in_nigeria' => $error));
      }
    }
    return $values;
  }
}