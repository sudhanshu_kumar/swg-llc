<?php

/**
 * ReEntryVisaAddress form.
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrinePluginFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ReEntryVisaAddressForm extends PluginReEntryVisaAddressForm
{
  /**
   * @see AddressMasterInterNationalForm
   */
     public function configure()
    {
        $this->widgetSchema['address_1'] = new sfWidgetFormInput(array('label' => 'Address 1'));
        $this->widgetSchema['address_2'] = new sfWidgetFormInput(array('label' => 'Address 2'));
        $this->widgetSchema['city'] = new sfWidgetFormInput(array('label' => 'City'));
        $this->widgetSchema['country_id'] = new sfWidgetFormDoctrineChoice(array('model' => 'Country', 'add_empty' => '-- Please Select --','label' => 'Country'));
        $this->widgetSchema['country_id']->setOption('order_by',array('country_name','asc'));
        //$this->widgetSchema['country_id']  = new sfWidgetFormChoice(array('choices' => array('NG' =>'Nigeria')));
        $this->widgetSchema['state'] = new sfWidgetFormInput(array('label' => 'State'));
        //$this->widgetSchema['lga_id'] = new sfWidgetFormDoctrineSelect(array('model' => 'LGA', 'add_empty' => '-- Please Select --','label' => 'LGA'));
        //$this->widgetSchema['state']  = new sfWidgetFormChoice(array('choices' => array('' =>'-- Please Select --')));

        //$this->widgetSchema['lga_id'] = new sfWidgetFormChoice(array('choices' => array('' =>'-- Please Select --')));
        //$this->widgetSchema['district'] = new sfWidgetFormInput(array('label' => 'District'));
        $this->widgetSchema['postcode'] = new sfWidgetFormInput(array('label' => 'Postcode'));

        //$this->validatorSchema->setOption('allow_extra_fields', true);

        $this->validatorSchema['postcode'] = new sfValidatorString(array('max_length' => 10,'required' =>false),array('max_length'=>'Postcode can not  be more than 10 characters.'));
        $this->validatorSchema['address_2'] = new sfValidatorString(array('max_length' => 100,'required' =>false),array('max_length'=>'Address 2 can not  be more than 100 characters.'));
        $this->validatorSchema['address_1'] = new sfValidatorString(array('max_length' => 100),array('required' => 'Address 1 is required.','max_length'=>'Address 1 can not  be more than 100 characters.'));
        $this->validatorSchema['city'] = new sfValidatorString(array('max_length' => 100),array('required' => 'City is required.','max_length'=>'City can not  be more than 100 characters.'));
        $this->validatorSchema['state']  =  new sfValidatorString(array('max_length' => 100),array('required' => 'State is required.','max_length'=>'State can not  be more than 100 characters.'));
        $this->validatorSchema['country_id']  = new sfValidatorDoctrineChoice(array('model' => 'Country'),array('required'=>'Country is required'));
    }
}
