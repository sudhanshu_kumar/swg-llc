<?php

/**
 * PassportPermanentAddress form.
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrinePluginFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class PassportPermanentAddressForm extends PluginPassportPermanentAddressForm
{
  /**
   * @see AddressMasterForm
   */
  public function configure()
  {

    $this->widgetSchema['address_1'] = new sfWidgetFormInput(array('label' => 'Address 1'));
    $this->widgetSchema['address_2'] = new sfWidgetFormInput(array('label' => 'Address 2'));
    $this->widgetSchema['city'] = new sfWidgetFormInput(array('label' => 'City'));

    $this->widgetSchema['country_id']  = new sfWidgetFormChoice(array('choices' => array(""=>' -- Please Select -- ','NG' =>'Nigeria')));

   // $this->widgetSchema['state']->setOption('add_empty','-- Please Select --');
     $this->widgetSchema['state'] = new sfWidgetFormChoice(array('choices' => array('' => '-- Please Select --')));
    //$this->widgetSchema['state']->setOption('query',StateTable::getCachedQuery());
    $this->widgetSchema['lga_id'] = new sfWidgetFormDoctrineChoice(array('model' => 'LGA', 'add_empty' => '-- Please Select --','label' => 'LGA'));
    $this->widgetSchema['district'] = new sfWidgetFormInput(array('label' => 'District'));
    $this->widgetSchema['postcode'] = new sfWidgetFormInput(array('label' => 'Postcode'));

      $this->widgetSchema->setLabels(array('lga_id'   => 'LGA','country_id'=> 'Country'
      )
    );

    $this->validatorSchema['address_2'] = new sfValidatorString(array('max_length' => 100,'required' =>false),array('max_length'=>'Address 2 can not  be more than 100 characters.'));
    $this->validatorSchema['district'] = new sfValidatorString(array('max_length' => 100,'required' =>false),array('max_length'=>'District can not  be more than 100 characters.'));
    $this->validatorSchema['postcode'] = new sfValidatorString(array('max_length' => 10,'required' =>false),array('max_length'=>'Postcode can not  be more than 10 characters.'));

    $this->validatorSchema['address_1'] = new sfValidatorString(array('max_length' => 100, 'required' =>false),array('required' => 'Address 1 is required.','max_length'=>'Address 1 can not  be more than 100 characters.'));
    $this->validatorSchema['city'] = new sfValidatorString(array('max_length' => 100, 'required' =>false ),array('required' => 'City is required.','max_length'=>'City can not  be more than 100 characters.'));
    //$this->validatorSchema['country_id'] = new sfValidatorChoice(array('required' => false));
    //$this->validatorSchema['country_id'] = new sfValidatorChoice(array('required' => false, 'choices' => array('NG' =>'Nigeria')));


    //$this->validatorSchema['state']  = new sfValidatorDoctrineChoice(array('model' => 'State', 'required' =>false),array('required'=>'State is required.'));
    $this->validatorSchema['lga_id']  = new sfValidatorDoctrineChoice(array('model' => 'LGA', 'required' =>false),array('required'=>'LGA is required.'));

   $this->updateDependentValues();

  }

  public function render($attributes = null) {
      if ($this->isBound())
        $this->updateDependentValues();
      return parent::render($attributes);
    }


  public function updateDependentValues() {

    if ($this->getObject() && (!$this->getObject()->isNew())) {
      $obj = $this->getObject();
      $curr_country_id = (isset($obj["country_id"]))?$obj["country_id"]:'';      
      if(isset($_POST['passport_application']['PermanentAddress']['state'])){
        $curr_state_id = $_POST['passport_application']['PermanentAddress']['state'];
      }else{
        $curr_state_id = (isset($obj["state"]))?$obj["state"]:'';
      }
      $lga_id = (isset($obj["lga_id"]))?$obj["lga_id"]:'';
    } else {
      $curr_country_id = null;
      $curr_state_id = null;
      $lga_id = NULL;
    }


    if($curr_country_id) {
      $this->setdefaults(array('country_id' => $curr_country_id));

      if($curr_country_id=='NG'){
        $this->widgetSchema['state']  = new sfWidgetFormDoctrineChoice(
          array('default' => $curr_state_id,'model' =>'', 'add_empty' => '-- Please Select --'));
        $this->widgetSchema['state']->setOption('query',StateTable::getCachedQuery());
      }
      else
      {
        $this->widgetSchema['state']  = new sfWidgetFormSelect(array('choices' => array('' =>'-- Please Select --')));
      }

    }

    if($curr_state_id!='') {
      $q = Doctrine_Query::create()
      ->from('LGA L')
      ->where('L.branch_state = ?', $curr_state_id);

      $this->widgetSchema['lga_id']  = new sfWidgetFormDoctrineChoice(
        array('model' => 'LGA',
              'query' => $q,
              'add_empty' => '-- Please Select --', 'default' => $lga_id,'label' => 'LGA'));
    } else {
      $this->widgetSchema['lga_id']  = new sfWidgetFormSelect(array('choices' => array('' =>'-- Please Select --'),'label' => 'LGA'));
    }
  }
}
