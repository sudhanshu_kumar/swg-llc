<?php

/**
 * PassportApplication form.
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrinePluginFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class PassportApplicationForm extends PluginPassportApplicationForm
{
  public function __construct(sfDoctrineRecord $object = null, $options = array(), $CSRFSecret = null)
  {
    parent::__construct($object, $options, false);
  }

  public function configure()
  {
    $this->widgetSchema['terms_id'] = new sfWidgetFormInputCheckbox();
    $this->widgetSchema->setLabels(array('terms_id' => '&nbsp;'));
    $this->widgetSchema->setHelps(array('terms_id' => '* I Accept full responsibility for the information provided in this form.'));
    $this->validatorSchema['terms_id'] = new sfValidatorBoolean(array(),array('required' => "You Need to check the 'I Agree' box in order to accept responsibility on information provided"));

    $this->updateDependentValues();
  }

  public function render($attributes = null) {
    $logger = sfContext::getInstance()->getLogger();
    $logger->info('Form is About to be rendered NOW!!');
    if ($this->isBound()) {
      $this->updateDependentValues();
      // for each embedded form where address is being used - call updateDependentValues
      //     while calling, pass in the name of the form as well in updateDependentValues
    }
    return parent::render($attributes);
  }

  public function updateDependentValues() { //echo "<pre>";print_r($_POST);
   // die;
    $logger = sfContext::getInstance()->getLogger();

  if($this->isBound()) {
      $logger->info("{Passport} Form is bound");
      // this form is bounded - we should expect values from the $values array
      $curr_country_id = (isset($this->taintedValues["processing_country_id"]))?$this->taintedValues["processing_country_id"]:'';
      $curr_state_id = (isset($this->taintedValues["processing_state_id"]))?$this->taintedValues["processing_state_id"]:'';
      $curr_embassy_id = (isset($this->taintedValues["processing_embassy_id"]))?$this->taintedValues["processing_embassy_id"]:'';
      $curr_passport_office_id = (isset($this->taintedValues["processing_passport_office_id"]))?$this->taintedValues["processing_passport_office_id"]:'';
      $lga_id = (isset($this->taintedValues["lga_id"]))?$this->taintedValues["lga_id"]:'';


    } elseif ($this->getObject() && (!$this->getObject()->isNew())) {
      $logger->info("{Passport} Form has an object");
      $obj = $this->getObject();
      if(isset($_POST['countryId'])){
        $curr_country_id = $_POST['countryId'];
      }else{
        $curr_country_id =  (isset($obj["processing_country_id"]))?$obj["processing_country_id"]:'';
      }
      $curr_state_id = (isset($obj["processing_state_id"]))?$obj["processing_state_id"]:'';
      $curr_embassy_id = (isset($obj["processing_embassy_id"]))?$obj["processing_embassy_id"]:'';
      $curr_passport_office_id = (isset($obj["processing_passport_office_id"]))?$obj["processing_passport_office_id"]:'';
      $lga_id = (isset($obj["lga_id"]))?$obj["lga_id"]:'';



    } else {
      $logger->info("{Passport} Form neither bound nor has an object");
      $curr_country_id = null;
      $curr_state_id = null;
      $curr_embassy_id = null;
      $curr_passport_office_id = null;
      $lga_id = NULL;


    }

    

    if(!$this->isOfficial())
    {
      if($curr_country_id) {
        $this->setdefaults(array('processing_country_id' => $curr_country_id));
      }

      if($curr_state_id) {
        //$this->widgetSchema['processing_state_id']  = new sfWidgetFormDoctrineSelect(
        //  array('model' => 'State', 'add_empty' => '-- Please Select --', 'default' => $curr_state_id));
        $this->widgetSchema['processing_state_id']  = new sfWidgetFormDoctrineChoice(
        array('default' => $curr_state_id,'model' =>'', 'add_empty' => '-- Please Select --'));
        $this->widgetSchema['processing_state_id']->setOption('query',StateTable::getCachedQuery());

        $this->configureStateForMrp();
        $this->configureStateForOfficial();
        $this->configureStateForMrpSeamans();
      } else {
        $this->widgetSchema['processing_state_id']  = new sfWidgetFormSelect(array('choices' => array('' =>'-- Please Select --')));
      }

      if($curr_country_id != Doctrine::getTable('Country')->getNigeriaId()) {
        $q = Doctrine_Query::create()
        ->from('EmbassyMaster em')
        ->where('em.embassy_country_id = ?', $curr_country_id);
        $this->widgetSchema['processing_embassy_id']  = new sfWidgetFormDoctrineChoice(
          array('model' => 'EmbassyMaster',
                'query' => $q,
                'add_empty' => '-- Please Select --', 'default' => $curr_embassy_id));
      } else {
        $this->widgetSchema['processing_embassy_id']  = new sfWidgetFormDoctrineChoice(array('choices' => array('' =>'-- Please Select --')));

      }

      if($curr_state_id) {
        $passportType='';
        if($passportType=='Standard ePassport Application Form' || $passportType=='Official ePassport Application Form'|| $passportType=='MRP Official Passport Application Form' || $appType=='Standard ePassport Application Form' || $appType=='Official ePassport Application Form'|| $appType=='MRP Official Passport Application Form'){
          $q = Doctrine_Query::create()
        ->from('PassportOffice po')
        ->where('po.office_state_id = ?', $curr_state_id)
        ->andWhere('po.is_epassport_active =?','1');
        }else{
          $q = Doctrine_Query::create()
        ->from('PassportOffice po')
        ->where('po.office_state_id = ?', $curr_state_id);
        }

        $this->widgetSchema['processing_passport_office_id']  = new sfWidgetFormDoctrineChoice(
          array('model' => 'PassportOffice',
                'query' => $q,
                'add_empty' => '-- Please Select --', 'default' => $curr_passport_office_id));
      } else {
        $this->widgetSchema['processing_passport_office_id']  = new sfWidgetFormSelect(array('choices' => array('' =>'-- Please Select --')));
      }


      $this->widgetSchema->setLabels(array( 'processing_country_id' => 'Processing Country',
                                            'processing_state_id'   => 'Processing State <sup>*</sup>',
                                            'processing_passport_office_id'   => 'Passport Office <sup>*</sup>',
                                            'processing_embassy_id' => 'Processing Embassy <sup>*</sup>'
        )
      );


    }

  }

  /**
   * Adds states for MRP applicable states only
   */
  public function configureStateForMrp () {
    if ($this->isMrp()) {
      $mrp_state_query = Doctrine_Query::create()->from('State s')
      ->where('is_mrp_state = 1')
       ->orderBy('state_name ASC');
      //$this->widgetSchema['processing_state_id']->setOption('query', $mrp_state_query);
      $this->widgetSchema['processing_state_id']  = new sfWidgetFormDoctrineSelect(
        array('model' =>'', 'add_empty' => '-- Please Select --'));
      $this->widgetSchema['processing_state_id']->setOption('query',$mrp_state_query);
    }
  }

  public function isMrp() {
    return false;
  }


  public function configureForMrp () {
    $this->widgetSchema['MRP_TYPE'] = new sfWidgetFormInputHidden();
    $this->validatorSchema['MRP_TYPE'] = new sfValidatorString(array('required' => false));
  }
  /**
   * Adds states for Official applicable states only
   */
  public function configureStateForOfficial () {
    if ($this->isOfficial()) {
      $mrp_state_query = Doctrine_Query::create()->from('State s')
      ->where('is_official_state = 1')
       ->orderBy('state_name ASC');
      //$this->widgetSchema['processing_state_id']->setOption('query', $mrp_state_query);
      $this->widgetSchema['processing_state_id']  = new sfWidgetFormDoctrineSelect(
        array('model' =>'', 'add_empty' => '-- Please Select --'));
      $this->widgetSchema['processing_state_id']->setOption('query',$mrp_state_query);
    }
  }

  public function isOfficial() {
    return false;
  }
  public function configureForOfficial () {
    $this->widgetSchema['OFFICIAL_TYPE'] = new sfWidgetFormInputHidden();
    $this->validatorSchema['OFFICIAL_TYPE'] = new sfValidatorString(array('required' => false));
  }

  /**
   * Adds states for MRP applicable states only
   */
  public function configureStateForMrpSeamans () {
    if ($this->isMrpSeamans()) {
      $mrp_state_query = Doctrine_Query::create()->from('State s')
      ->where('is_mrp_seamans_state = 1')
      ->orderBy('state_name ASC');
      //$this->widgetSchema['processing_state_id']->setOption('query', $mrp_state_query);
      $this->widgetSchema['processing_state_id']  = new sfWidgetFormDoctrineSelect(
        array('model' =>'', 'add_empty' => '-- Please Select --'));
      $this->widgetSchema['processing_state_id']->setOption('query',$mrp_state_query);
    }
  }

  public function isMrpSeamans() {
    return false;
  }


  public function configureForMrpSeamans () {
    $this->widgetSchema['MRP_SEAMANS_TYPE'] = new sfWidgetFormInputHidden();
    $this->validatorSchema['MRP_SEAMANS_TYPE'] = new sfValidatorString(array('required' => false));
  }
}
