<?php

/**
 * UserDetail form.
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class UserDetailForm extends BaseUserDetailForm
{
  public function configure()
  {
        unset(
            $this['master_account_id'],$this['is_sms_charge_paid'],$this['cart_items'],
            $this['kyc_status'],$this['updated_at'],$this['created_at'],
            $this['payment_rights'],$this['updated_by'],$this['created_by'],
            $this['failed_attempt'],$this['mobile_phone'],$this['address'],
            $this['dob'],$this['user_id'],$this['deleted'],
            $this['approved_by'],$this['disapprove_reason'],$this['sms_charge']
        );

  }
}
