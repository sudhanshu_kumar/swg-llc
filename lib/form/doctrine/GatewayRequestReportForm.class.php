<?php
class GatewayRequestReportForm  extends sfForm {
    public function configure() {
        sfProjectConfiguration::getActive()->loadHelpers('Asset');

        $i18n = sfContext::getInstance()->getI18N();
        $user = sfContext::getInstance()->getUser();

        $culture = $user->getCulture();
        if (strlen($culture) > 2) {
            $culture = substr($culture, 0, 2);
        }

        $sfWidgetFormI18nJQueryDateOptions = array(
      'culture' => $culture,
      'image'   => image_path('calendar.png'),
      'config'  => "{ duration: '' }"
        );

        $this->setWidgets(array(
        'from_date' => new sfWidgetFormInput(array(),array('maxlength'=>10, 'readonly'=>'true','onfocus'=>'showCalendarControl(eWalletStatement_from_date)','id'=>'eWalletStatement_from_date','class'=>'txt-input')),
        'to_date' => new sfWidgetFormInput(array(),array('maxlength'=>10, 'readonly'=>'true','onfocus'=>'showCalendarControl(eWalletStatement_to_date)','id'=>'eWalletStatement_to_date','class'=>'txt-input')),
        ));

       $this->setValidators(array(
        'from_date'   => new sfValidatorDate(array('required' => true),array('required' => 'Please enter the From Date')),
        'to_date'   => new sfValidatorDate(array('required' => true),array('required' => 'Please enter the To Date')),
       ));
     if(isset($_REQUEST['split']['to_date']) && $_REQUEST['split']['to_date']!=""){
        $this->validatorSchema->setPostValidator(

             new sfValidatorAnd(array(
                        new sfValidatorSchemaCompare('from_date', sfValidatorSchemaCompare::LESS_THAN_EQUAL, 'to_date',
                            array('throw_global_error' => true),
                            array('invalid' => 'The From Date ("%left_field%") cannot be greater than To Date (%right_field%)')
                        )
                    ))

            );

      }
        $this->widgetSchema->setLabels(array(
         // 'merchant_id' => 'Merchant',
          'from_date'   =>  'From Date <font color=red>*</font>',
          'to_date'   =>  'To Date <font color=red>*</font>',
            ));

        $this->widgetSchema->setNameFormat('gatewayRequestReport[%s]');
        $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    }
}