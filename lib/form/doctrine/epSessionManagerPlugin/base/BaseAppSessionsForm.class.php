<?php

/**
 * AppSessions form base class.
 *
 * @method AppSessions getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseAppSessionsForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'sess_id'    => new sfWidgetFormInputHidden(),
      'user_id'    => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('sfGuardUser'), 'add_empty' => true)),
      'username'   => new sfWidgetFormInputText(),
      'ip_address' => new sfWidgetFormInputText(),
      'sess_data'  => new sfWidgetFormTextarea(),
      'sess_time'  => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'sess_id'    => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'sess_id', 'required' => false)),
      'user_id'    => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('sfGuardUser'), 'required' => false)),
      'username'   => new sfValidatorString(array('max_length' => 128, 'required' => false)),
      'ip_address' => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'sess_data'  => new sfValidatorString(array('max_length' => 4000)),
      'sess_time'  => new sfValidatorInteger(),
    ));

    $this->widgetSchema->setNameFormat('app_sessions[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'AppSessions';
  }

}
