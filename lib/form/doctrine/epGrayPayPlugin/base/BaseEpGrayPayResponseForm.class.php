<?php

/**
 * EpGrayPayResponse form base class.
 *
 * @method EpGrayPayResponse getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseEpGrayPayResponseForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'             => new sfWidgetFormInputHidden(),
      'graypay_req_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('EpGrayPayRequest'), 'add_empty' => false)),
      'response'       => new sfWidgetFormInputText(),
      'response_text'  => new sfWidgetFormInputText(),
      'authcode'       => new sfWidgetFormInputText(),
      'transactionid'  => new sfWidgetFormInputText(),
      'type'           => new sfWidgetFormInputText(),
      'orderid'        => new sfWidgetFormInputText(),
      'amount'         => new sfWidgetFormInputText(),
      'avsresponse'    => new sfWidgetFormInputText(),
      'cvvresponse'    => new sfWidgetFormInputText(),
      'response_code'  => new sfWidgetFormInputText(),
      'created_at'     => new sfWidgetFormDateTime(),
      'updated_at'     => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'             => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'graypay_req_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('EpGrayPayRequest'))),
      'response'       => new sfValidatorString(array('max_length' => 30, 'required' => false)),
      'response_text'  => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'authcode'       => new sfValidatorString(array('max_length' => 30, 'required' => false)),
      'transactionid'  => new sfValidatorString(array('max_length' => 30)),
      'type'           => new sfValidatorString(array('max_length' => 30)),
      'orderid'        => new sfValidatorString(array('max_length' => 20)),
      'amount'         => new sfValidatorPass(),
      'avsresponse'    => new sfValidatorString(array('max_length' => 10, 'required' => false)),
      'cvvresponse'    => new sfValidatorString(array('max_length' => 10, 'required' => false)),
      'response_code'  => new sfValidatorString(array('max_length' => 30, 'required' => false)),
      'created_at'     => new sfValidatorDateTime(),
      'updated_at'     => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('ep_gray_pay_response[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'EpGrayPayResponse';
  }

}
