<?php

/**
 * EpActionAuditEvent form base class.
 *
 * @method EpActionAuditEvent getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseEpActionAuditEventForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'          => new sfWidgetFormInputHidden(),
      'user_id'     => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('sfGuardUser'), 'add_empty' => true)),
      'username'    => new sfWidgetFormInputText(),
      'ip_address'  => new sfWidgetFormInputText(),
      'category'    => new sfWidgetFormInputText(),
      'subcategory' => new sfWidgetFormInputText(),
      'description' => new sfWidgetFormInputText(),
      'created_at'  => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'          => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'user_id'     => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('sfGuardUser'), 'required' => false)),
      'username'    => new sfValidatorString(array('max_length' => 128, 'required' => false)),
      'ip_address'  => new sfValidatorString(array('max_length' => 15)),
      'category'    => new sfValidatorString(array('max_length' => 40)),
      'subcategory' => new sfValidatorString(array('max_length' => 40)),
      'description' => new sfValidatorString(array('max_length' => 255)),
      'created_at'  => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('ep_action_audit_event[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'EpActionAuditEvent';
  }

}
