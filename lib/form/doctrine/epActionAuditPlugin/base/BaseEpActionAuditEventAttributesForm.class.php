<?php

/**
 * EpActionAuditEventAttributes form base class.
 *
 * @method EpActionAuditEventAttributes getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseEpActionAuditEventAttributesForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'             => new sfWidgetFormInputHidden(),
      'audit_event_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('EpActionAuditEvent'), 'add_empty' => true)),
      'name'           => new sfWidgetFormInputText(),
      'svalue'         => new sfWidgetFormInputText(),
      'ivalue'         => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'             => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'audit_event_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('EpActionAuditEvent'), 'required' => false)),
      'name'           => new sfValidatorString(array('max_length' => 40, 'required' => false)),
      'svalue'         => new sfValidatorString(array('max_length' => 40, 'required' => false)),
      'ivalue'         => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('ep_action_audit_event_attributes[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'EpActionAuditEventAttributes';
  }

}
