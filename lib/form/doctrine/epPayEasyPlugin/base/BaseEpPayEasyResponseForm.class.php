<?php

/**
 * EpPayEasyResponse form base class.
 *
 * @method EpPayEasyResponse getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseEpPayEasyResponseForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'             => new sfWidgetFormInputHidden(),
      'payeasy_req_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('EpPayEasyRequest'), 'add_empty' => false)),
      'time_taken'     => new sfWidgetFormInputText(),
      'issuer'         => new sfWidgetFormInputText(),
      'response_ip'    => new sfWidgetFormInputText(),
      'tran_type'      => new sfWidgetFormInputText(),
      'currency'       => new sfWidgetFormInputText(),
      'amount'         => new sfWidgetFormInputText(),
      'tran_id'        => new sfWidgetFormInputText(),
      'response_code'  => new sfWidgetFormInputText(),
      'response_text'  => new sfWidgetFormInputText(),
      'auth_code'      => new sfWidgetFormInputText(),
      'status'         => new sfWidgetFormInputText(),
      'fraud_score'    => new sfWidgetFormInputText(),
      'fraud_message'  => new sfWidgetFormInputText(),
      'response_xml'   => new sfWidgetFormTextarea(),
      'created_at'     => new sfWidgetFormDateTime(),
      'updated_at'     => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'             => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'payeasy_req_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('EpPayEasyRequest'))),
      'time_taken'     => new sfValidatorInteger(),
      'issuer'         => new sfValidatorInteger(),
      'response_ip'    => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'tran_type'      => new sfValidatorInteger(),
      'currency'       => new sfValidatorString(array('max_length' => 20)),
      'amount'         => new sfValidatorPass(),
      'tran_id'        => new sfValidatorString(array('max_length' => 30)),
      'response_code'  => new sfValidatorString(array('max_length' => 30, 'required' => false)),
      'response_text'  => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'auth_code'      => new sfValidatorString(array('max_length' => 30, 'required' => false)),
      'status'         => new sfValidatorInteger(array('required' => false)),
      'fraud_score'    => new sfValidatorPass(array('required' => false)),
      'fraud_message'  => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'response_xml'   => new sfValidatorString(),
      'created_at'     => new sfValidatorDateTime(),
      'updated_at'     => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('ep_pay_easy_response[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'EpPayEasyResponse';
  }

}
