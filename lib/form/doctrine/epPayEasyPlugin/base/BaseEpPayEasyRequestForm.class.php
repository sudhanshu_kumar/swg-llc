<?php

/**
 * EpPayEasyRequest form base class.
 *
 * @method EpPayEasyRequest getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseEpPayEasyRequestForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'              => new sfWidgetFormInputHidden(),
      'tran_date'       => new sfWidgetFormDateTime(),
      'issuer'          => new sfWidgetFormInputText(),
      'request_ip'      => new sfWidgetFormInputText(),
      'tran_type'       => new sfWidgetFormInputText(),
      'currency'        => new sfWidgetFormInputText(),
      'amount'          => new sfWidgetFormInputText(),
      'card_first'      => new sfWidgetFormInputText(),
      'card_last'       => new sfWidgetFormInputText(),
      'card_len'        => new sfWidgetFormInputText(),
      'card_type'       => new sfWidgetFormInputText(),
      'card_holder'     => new sfWidgetFormInputText(),
      'expiry_month'    => new sfWidgetFormInputText(),
      'expiry_year'     => new sfWidgetFormInputText(),
      'cvv'             => new sfWidgetFormInputText(),
      'customersupport' => new sfWidgetFormInputText(),
      'address1'        => new sfWidgetFormInputText(),
      'address2'        => new sfWidgetFormInputText(),
      'town'            => new sfWidgetFormInputText(),
      'state'           => new sfWidgetFormInputText(),
      'zip'             => new sfWidgetFormInputText(),
      'country'         => new sfWidgetFormInputText(),
      'first_name'      => new sfWidgetFormInputText(),
      'last_name'       => new sfWidgetFormInputText(),
      'email'           => new sfWidgetFormInputText(),
      'phone'           => new sfWidgetFormInputText(),
      'cus_id'          => new sfWidgetFormInputText(),
      'ip_address'      => new sfWidgetFormInputText(),
      'request_xml'     => new sfWidgetFormTextarea(),
      'created_at'      => new sfWidgetFormDateTime(),
      'updated_at'      => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'              => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'tran_date'       => new sfValidatorDateTime(array('required' => false)),
      'issuer'          => new sfValidatorInteger(),
      'request_ip'      => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'tran_type'       => new sfValidatorInteger(),
      'currency'        => new sfValidatorString(array('max_length' => 20)),
      'amount'          => new sfValidatorPass(),
      'card_first'      => new sfValidatorInteger(),
      'card_last'       => new sfValidatorInteger(),
      'card_len'        => new sfValidatorInteger(array('required' => false)),
      'card_type'       => new sfValidatorPass(array('required' => false)),
      'card_holder'     => new sfValidatorString(array('max_length' => 255)),
      'expiry_month'    => new sfValidatorString(array('max_length' => 5)),
      'expiry_year'     => new sfValidatorString(array('max_length' => 10)),
      'cvv'             => new sfValidatorInteger(),
      'customersupport' => new sfValidatorString(array('max_length' => 255)),
      'address1'        => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'address2'        => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'town'            => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'state'           => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'zip'             => new sfValidatorString(array('max_length' => 15, 'required' => false)),
      'country'         => new sfValidatorString(array('max_length' => 150, 'required' => false)),
      'first_name'      => new sfValidatorString(array('max_length' => 30, 'required' => false)),
      'last_name'       => new sfValidatorString(array('max_length' => 30, 'required' => false)),
      'email'           => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'phone'           => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'cus_id'          => new sfValidatorInteger(),
      'ip_address'      => new sfValidatorString(array('max_length' => 20)),
      'request_xml'     => new sfValidatorString(),
      'created_at'      => new sfValidatorDateTime(),
      'updated_at'      => new sfValidatorDateTime(),
    ));

    $this->validatorSchema->setPostValidator(
      new sfValidatorDoctrineUnique(array('model' => 'EpPayEasyRequest', 'column' => array('cus_id')))
    );

    $this->widgetSchema->setNameFormat('ep_pay_easy_request[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'EpPayEasyRequest';
  }

}
