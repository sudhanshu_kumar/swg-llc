<?php

/**
 * EpPayEasyRequest form.
 *
 * @package
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrinePluginFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class EpPayEasyRequestForm extends PluginEpPayEasyRequestForm
{
  public function configure()
  {
    //        echo "<pre>";print_r($_REQUEST);
    unset(
      $this['first_name'],$this['last_name'],$this['address1'],$this['address2'],$this['town'],$this['state'],$this['zip'],$this['country'],$this['email'],$this['phone'],$this['card_type'],$this['card_Num'],$this['card_holder'],$this['expiry_date'],$this['cvv'],$this['tran_date'],$this['issuer'],$this['request_ip'],$this['tran_type'],$this['card_len'],$this['customersupport'],$this['cus_id'],$this['ip_address'],$this['request_detail_id'],$this['request_xml']
    );

    


    $expload = explode(',', $_SESSION['card_type']);
    $cardArr = array();
    for($i=1; $i < count($expload); $i++)
    {
      if($expload[$i] == 'A')
      $cardArr[$expload[$i]] = 'American Express';
      if($expload[$i] == 'V')
      $cardArr[$expload[$i]] = 'VISA Card';
      if($expload[$i] == 'M')
      $cardArr[$expload[$i]] = 'Master Card';
    }
//    $vbv_active  = settings::isVbvActive();
//
//    echo "<pre>";print_r($cardArr);die;
//    if($vbv_active)
//    $cardArr = array("A"=>"American Express");
//    else
//    $cardArr = array("A"=>"American Express","M"=>"Master Card","V"=>"VISA Card");
//    $task = $this->getOption('task');
//
//    if(isset($_REQUEST) && isset($_REQUEST['paymentMode']) && $_REQUEST['paymentMode'] ){
//      if('americanexpress' == $_REQUEST['paymentMode']){ $cardArr = array("A"=>"American Express");}
//      if('master' == $_REQUEST['paymentMode']){ $cardArr = array("M"=>"Master Card");}
//      if('visa' == $_REQUEST['paymentMode']){ $cardArr = array("V"=>"VISA Card");}
//    }
      /* if('payment' == $task )
            $cardArr = array("A"=>"American Express","M"=>"Master Card");
       if('recharge' == $task )
            $cardArr = array("A"=>"American Express");*/

    $this->setWidgets(array(
      'id'              => new sfWidgetFormInputHidden(),
      'request_ip'      => new sfWidgetFormInputHidden(),
      'first_name'      => new sfWidgetFormInputText(array(),array('class'=>'txt-input','maxlength'=>32,'min_length'=>2)),
      'last_name'       => new sfWidgetFormInputText(array(),array('class'=>'txt-input','maxlength'=>32,'min_length'=>2)),
      'address1'        => new sfWidgetFormInputText(array(),array('class'=>'txt-input','maxlength'=>48,'min_length'=>2)),
      'address2'        => new sfWidgetFormInputText(array(),array('class'=>'txt-input','maxlength'=>48,'min_length'=>2)),
      'town'            => new sfWidgetFormInputText(array(),array('class'=>'txt-input','maxlength'=>48,'min_length'=>2)),
      'state'           => new sfWidgetFormInputText(array(),array('class'=>'txt-input','maxlength'=>32,'min_length'=>2)),
      'zip'             => new sfWidgetFormInputText(array(),array('class'=>'txt-input','maxlength'=>16,'min_length'=>1)),
      'country'         => new sfWidgetFormI18nChoiceCountry(array('add_empty' => '---Please select---')),
      'email'           => new sfWidgetFormInputText(array(),array('class'=>'txt-input','maxlength'=>66,'min_length'=>8)),
      'phone'           => new sfWidgetFormInputText(array(),array('class'=>'txt-input','maxlength'=>16,'min_length'=>8)),
      'card_type'       => new sfWidgetFormChoice(array('choices'=>$cardArr)),
      'card_Num'        => new sfWidgetFormInputText(array(),array('class'=>'txt-input','autocomplete'=>'off')),
      'card_holder'     => new sfWidgetFormInputText(array(),array('class'=>'txt-input','maxlength'=>32)),
      'expiry_date'     => new sfWidgetFormMonthYear(array('can_be_empty'=>false,'format'=> '%month%-%year%')),
      'cvv'             => new sfWidgetFormInputPassword(array(),array('class'=>'txt-input','maxlength'=>4,'min_length'=>3)),
      'agreed'          => new sfWidgetFormInputCheckbox(array(),array()),
      ));

    $this->setValidators(array(
      'id'                => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'request_ip'        => new sfValidatorString(array('required' => false)),
      'first_name'        => new sfValidatorString(array('max_length' => 32,'min_length'=>2, 'required' => true),array('required'=>'Please enter First  Name','max_length'=>'First Name cannot be more than 32 characters','min_length'=>'First Name cannot be less than 2 characters')),
      'first_name'        => new sfValidatorRegex(array('required'=>true,'pattern' => sfConfig::get('app_name_exception_pattern')),array('invalid'=>'First Name is invalid.', 'required'=>'Please enter First Name.')),
      'last_name'         => new sfValidatorString(array('max_length' => 32,'min_length'=>2, 'required' => true),array('required'=>'Please enter Last Name','max_length'=>'Last Name cannot be more than 32 characters','min_length'=>'Last Name cannot be less than 2 characters')),
      'last_name'         => new sfValidatorRegex(array('required'=>true,'pattern' => sfConfig::get('app_name_exception_pattern')),array('invalid'=>'Last Name is invalid.','required'=>'Please enter Last Name')),
      'address1'          => new sfValidatorString(array('max_length' => 48,'min_length'=>2, 'required' => true),array('required'=>'Please enter Address','max_length'=>'Address1 cannot be more than 48 characters','min_length'=>'Address1 cannot be less than 2 characters')),
      'address1'          => new sfValidatorRegex(array('required'=>true,'pattern' => '/[^\s+]/', 'required' => true),array('invalid'=>'Blank spaces are not allowed.', 'required' => 'Please enter Address1.')),
      'address2'          => new sfValidatorString(array('max_length' => 48,'min_length'=>2, 'required' => false),array('max_length'=>'Address2 cannot be more than 48 characters','min_length'=>'Address2 cannot be less than 2 characters')),
      'town'              => new sfValidatorString(array('max_length' => 48,'min_length'=>2, 'required' => true),array('required'=>'Please enter Town','max_length'=>'Town cannot be more than 48 characters','min_length'=>'Town cannot be less than 2 characters')),
      'town'              => new sfValidatorRegex(array('required'=>true,'pattern' => '/^[^\s+][a-zA-Z [\]]{1,32}?$/'),array('invalid'=>'Town is invalid.', 'required' => 'Please enter Town.')),
      'state'             => new sfValidatorString(array('max_length' => 32,'min_length'=>2, 'required' => false),array('max_length'=>'State cannot be more than 32 characters','min_length'=>'State cannot be less than 2 characters')),
      'state'              => new sfValidatorRegex(array('required'=>false,'pattern' => '/^[^\s+][a-zA-Z [\]]{1,32}?$/'),array('invalid'=>'State is invalid.')),
      'zip'               => new sfValidatorString(array('max_length' => 16,'min_length'=>1, 'required' => false),array('max_length'=>'ZIP code cannot be more than 16 characters')),
      'country'           => new sfValidatorString(array('max_length' => 150, 'required' => true),array('required'=>'Please select Country','max_length'=>'Country cannot be more than 150 characters')),
      'email'             => new sfValidatorEmail(array('max_length' => 66,'min_length'=>8, 'required' => true),array ('invalid'=>'Invalid E-Mail address','required'=>'Please enter E-Mail address','max_length'=>'e-mail cannot be more than 66 characters','min_length'=>'e-mail cannot be less than 8 characters')),
        //'phone'             => new sfValidatorRegex(array('required'=>true,'pattern'=>'/^(\+|0)(\d){10,14}?$/', 'required' => true),array('invalid'=>'Please enter valid Phone Number<br/>Phone Number should be between 10-14 digits','required'=>'Please enter Phone Number')),
      'phone'             => new sfValidatorRegex(array('max_length' => 16,'min_length'=>8,'required'=>true,'pattern'=>'/^[0-9 [\]+()-]{8,16}?$/', 'required' => true),array('invalid'=>'Please enter valid Phone Number<br/>Phone Number should be between 8-16 digits','required'=>'Please enter Phone Number')),
      'card_type'         => new sfValidatorPass(array('required' => true),array('required'=>'Please Select CardType')),
      'card_Num'          => new sfValidatorNumber(array('min'=>1000000000000,'max' =>9999999999999999,'required'=>true),array('invalid'=>'Invalid Card Number','required'=>'Please enter Card Number','min'=>'Card Number should be minimum of 13 digits','max'=>'Card Number should be maximum of 16 digits')),
      'card_holder'       => new sfValidatorString(array('max_length' => 32,'required' => true),array('required'=>'Please enter Card Holder Name','max_length'=>'Card Holder Name cannot be more than 32 characters')),
      'card_holder'       => new sfValidatorRegex(array('required'=>true,'pattern' => sfConfig::get('app_name_exception_pattern')),array('invalid'=>'Card Holder Name is invalid.', 'required'=>'Please enter Card Holder Name.')),
      'expiry_date'       => new sfValidatorString(array('max_length' => 50,'required' => true),array('required'=>'Please enter Expiry Date','max_length'=>'Entered Expiry Date too long')),
      'cvv'               => new sfValidatorString(array('max_length' => 4,'min_length'=>3,'required'=>true),array('required'=>'Please enter CVV','min_length'=>'CVV should be minimum of 3 digits<br>','max_length'=>'CVV should be maximum of 4 digits<br>')),
        //'cvv'               => new sfValidatorNumber(array('min'=>100,'max' => 9999,'required'=>true),array('required'=>'Please enter CVV','min'=>'CVV should be minimum of 3 digits','max'=>'CVV should be maximum of 4 digits')),
      'agreed'            => new sfValidatorBoolean(array('required'=>true),array('required'=>'Please agree to Terms & Conditions')),
      ));




    $this->widgetSchema->setLabels(array(
   'first_name'=> 'First Name',
   'last_name'=>'Last Name',
   'address1'=>'Address 1',
   'address2'=>'Address 2',
   'town'=>'Town',
   'state'=>'State',
   'zip'=>'ZIP(Postal Code)',
   'country'=>'Country',
   'email'=>'Email',
   'phone'=>'Phone Number ',
   'card_type'=>'Card Type',
   'card_Num'=>'Card Number',
   'card_holder'=>'Card Holder',
   'expiry_date'=>'Expiry Date',
   'cvv'=>'CVV',
   'agreed'  =>''
      ));
    $this->widgetSchema->setNameFormat('ep_pay_easy_request[%s]');

    $this->validatorSchema->setPostValidator(
      new sfValidatorAnd(array(
          new sfValidatorDoctrineUnique(array(
                                        'model' => 'UserDetail',
                                        'column' => array('email'),
                                        'primary_key' => 'id',
                                        'required' => 'Email cannot be left blank'),
            array('invalid'=>'The email address already exists'))

        ))

    );
    $this->validatorSchema->setPostValidator(new sfValidatorAnd(array(
          new sfValidatorCallback(array('callback' => array($this, 'checkIssueDate'))))));

    // start - change by vineet for making ZIP mandatory
    $countryCode = Doctrine::getTable('countryBasedZip')->getAllCountryCode();
    
    $arrCountry = array();
    for($i=0;$i<count($countryCode);$i++)
    {
      $arrCountry[] =  $countryCode[$i]['country_code'];
    }

    
    $argument = $this->getOption('argument');
    if(isset($argument['country']) && !empty($arrCountry))
    {
        if(in_array($argument['country'],$arrCountry))
        {
            $this->validatorSchema['zip'] = new sfValidatorAnd(
                    array(
                        new sfValidatorString(array('required' => true),
                            array('required'=>'ZIP(Postal Code) is compulsory.'))
                    ),
                    array('halt_on_error'=>true),
                    array('required'=>'ZIP(Postal Code) is compulsory.')
                );
        }
    }
    // end - change by vineet for making ZIP mandatory


  }
  public function checkIssueDate($validator, $values) {

    $visaReg = '/^4[0-9]{15}$/';
    $masterReg = '/^5[1-5]{1}[0-9]{14}$/';
    $americanReg = '/^3[47]{1}[0-9]{13}$/';
    $expload = explode(',', $_SESSION['card_type']);
    $expiryDateArr = $_REQUEST['ep_pay_easy_request']['expiry_date'];
    $cardType = $_REQUEST['ep_pay_easy_request']['card_type'];
    $cardNumber = $_REQUEST['ep_pay_easy_request']['card_Num'];
    $cvv = $_REQUEST['ep_pay_easy_request']['cvv'];

    if(!in_array($cardType,$expload)){
        $error = new sfValidatorError($validator, 'Invalid card type');
        throw new sfValidatorErrorSchema($validator, array('card_type' => $error));

    }
    else if($cardNumber !='' && $cardType == 'A' && !preg_match($americanReg,$cardNumber)){
        $error = new sfValidatorError($validator, 'Invalid card number');
        throw new sfValidatorErrorSchema($validator, array('card_Num' => $error));

    }
    else if($cardNumber !='' && $cardType == 'V' && !preg_match($visaReg,$cardNumber)){
        $error = new sfValidatorError($validator, 'Invalid card number');
        throw new sfValidatorErrorSchema($validator, array('card_Num' => $error));

    }
    else if($cardNumber !='' && $cardType == 'M' && !preg_match($masterReg,$cardNumber)){
        $error = new sfValidatorError($validator, 'Invalid card number');
        throw new sfValidatorErrorSchema($validator, array('card_Num' => $error));

    }

    

    if($cardType =='A' && $cvv !='' && strlen($cvv) <= 3){
        $error = new sfValidatorError($validator, 'CVV should be 4 digit long for American Express');
        throw new sfValidatorErrorSchema($validator, array('cvv' => $error));

    }
    if(isset ($expiryDateArr) && is_array($expiryDateArr) && count($expiryDateArr)==2){
      $expiryDateArr['day'] = 1;
      $expiryDateArr_year = (($expiryDateArr['year']=="")? '0':$expiryDateArr['year']);
      $expiryDateArr_month = (($expiryDateArr['month']=="")? '0'.$expiryDateArr['month']:$expiryDateArr['month']);
      $expiryDateArr_day = (($expiryDateArr['day']=="")? '0'.$expiryDateArr['day']:$expiryDateArr['day']);



      $date  = date('m-Y');
      $dateArr = explode("-", $date);
      $dateArr['day'] = 1;
      $dateArr_year = (($dateArr['1']=="")? '0':$dateArr['1']);
      $dateArr_month = (($dateArr['0']<10)? '0'.$dateArr['0']:$dateArr['0']);
      $dateArr_day = (($dateArr['day']<10)? '0'.$dateArr['day']:$dateArr['day']);


      $expiryDate = mktime(0,0,0,$expiryDateArr_month,$expiryDateArr_day,$expiryDateArr_year);
      $expiryDate = date("Y-m-d", $expiryDate);

      $checkExpiry = mktime(0,0,0,$dateArr_month,$dateArr_day,$dateArr_year);
      $checkExpiry = date("Y-m-d", $checkExpiry);

      //  echo $checkExpiry,"  ",$expiryDate;die;
      // strtotime give previous date as small numeric value like if dates are 2008-07-01 and 2009-07-02   1246386600,1246473000,
      //then the date comarision will be made like 1246386600 > 1246473000
      if(strtotime($expiryDate) < strtotime($checkExpiry)){
        $error = new sfValidatorError($validator, 'Invalid Expiry Date');
        throw new sfValidatorErrorSchema($validator, array('expiry_date' => $error));

      }
      if(($cardType !='A' && strlen($cvv) > 3) || !is_numeric($cvv)){
        $error = new sfValidatorError($validator, 'Invalid CVV ');
        throw new sfValidatorErrorSchema($validator, array('cvv' => $error));

      }

      


    }
  }

}
