<?php

/**
 * Moneyorder form.
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class MoneyorderForm extends BaseMoneyorderForm
{
  public function configure()
  {
    

    unset(
      $this['created_at'],$this['created_by'],$this['updated_at'],$this['updated_by'],$this['paid_date'],
     $this['moneyorder_office'],$this['paid_date'],$this['convert_amount'],$this['currency']
    );
    $this->widgetSchema['moneyorder_number'] =  new sfWidgetFormInputText(array(),array('class'=>'txt-input','maxlength'=>30,'min_length'=>3,'autocomplete'=>'off'));
    $this->widgetSchema['re_moneyorder_number'] =  new sfWidgetFormInputText(array(),array('class'=>'txt-input','maxlength'=>30,'min_length'=>3,'autocomplete'=>'off'));
    $this->widgetSchema['amount'] =  new sfWidgetFormInputText(array(),array('class'=>'txt-input','maxlength'=>32,'min_length'=>3));
    $this->widgetSchema['address'] =  new sfWidgetFormTextarea(array(),array('class'=>'txt-input','maxlength'=>100,'min_length'=>3));
    $this->widgetSchema['phone'] =  new sfWidgetFormInputText(array(),array('class'=>'txt-input','maxlength'=>32,'min_length'=>3));
    //   $this->widgetSchema['moneyorder_office'] =  new sfWidgetFormInputText(array(),array('class'=>'txt-input','maxlength'=>32,'min_length'=>3));
    $this->widgetSchema['moneyorder_date'] =  new sfWidgetFormDateCal(array(),array('class'=>'txt-input','maxlength'=>32,'min_length'=>3));
    $this->widgetSchema['courier_service'] =  new sfWidgetFormChoice(array('choices'=>sfConfig::get('app_courierCompanyList')),array('onchange'=> 'otherCourierTexBox(this)','style'=>'width:150px'));
    
    //$this->widgetSchema['courier_flag'] =  new sfWidgetFormInputCheckbox(array(),array('onClick'=> 'showCourierDetails(this)'));

    $courier_flag = array( 'Yes'=>'Courier', 'No'=>'Regular Mail');

    $this->widgetSchema['courier_flag']  = new sfWidgetFormSelectRadio(array('choices' => $courier_flag),array());
    
    $this->widgetSchema['waybill_trackingno'] =  new sfWidgetFormInputText(array(),array('class'=>'txt-input','maxlength'=>30,'min_length'=>3,'autocomplete'=>'off'));


    //   $this->widgetSchema['paid_date'] =  new sfWidgetFormDateCal(array(),array('class'=>'txt-input','maxlength'=>32,'min_length'=>3));

//    $this->validatorSchema['moneyorder_number'] =  new sfValidatorRegex(array('required'=>true,'pattern' => '/[^\s+`()\\~!#%$@^&*+\"|:=,<>-]/','required'=>true),array('required'=>'Money order number is required','invalid' => 'White spaces are not allowed.'));
                                                 
    $this->validatorSchema['address'] = new sfValidatorString(array('required'=>true,'max_length'=>100),array('required'=>'Address is required','max_length'=>'Address can not be more than 100 characters'));
    //$this->validatorSchema['moneyorder_office'] = new sfValidatorString(array('required'=>true,'max_length'=>50),array('required'=>'Money order issuing office is required','max_length'=>'Money order office can not be more than 100 characters'));
    $this->validatorSchema['phone'] = new sfValidatorRegex(array('max_length' => 20,'min_length'=>8,'required'=>true,'pattern'=>'/^[0-9 [\]+()-]{8,20}?$/', 'required' => true),array('invalid'=>'Please enter valid Phone Number<br/>Phone Number should be between 8-20 digits','required'=>'Please enter Phone Number'));
    //   $this->validatorSchema['amount'] = new sfValidatorInteger(array('required'=>true,'min'=>2,'max'=>100000),array('required'=>'Money order number is required','min'=>'Amount can not be less than $2','max'=>'Amount can not be more than $10000'));
    $this->validatorSchema['amount'] = new sfValidatorInteger(array('required'=>true,'min'=>1,'max'=>1000000),array('required'=>'Moner order number is required','min'=>'Amount cannot be less than $1.','max'=>'Amount cannot be more than $1000000'));
    $this->validatorSchema['moneyorder_date'] = new sfValidatorDate(array('required'=>true,'max'=>time()),array('required'=>'Money order issuing date is required','max'=>'Money order issuing date can not be future date'));
    //   $this->validatorSchema['paid_date'] = new sfValidatorDate(array('required'=>true),array('required'=>'Paid date is required'));

    $this->validatorSchema['moneyorder_number'] =
     new sfValidatorAnd(array(
         new sfValidatorRegex(array('required'=>true,'pattern' => '/^[\-0-9a-zA-Z]+$/','required'=>true),array('required'=>'Money order number is required','invalid' => 'White spaces and special characters are not allowed.')),
         new sfValidatorCallback(array('required'=>true,
        'callback'  => 'MoneyorderForm::findDuplicateMO') 
      )))
      ;
    $this->validatorSchema['re_moneyorder_number'] =
     new sfValidatorAnd(array(
         new sfValidatorRegex(array('required'=>true,'pattern' => '/^[\-0-9a-zA-Z]+$/','required'=>true),array('required'=>'Money order number is required','invalid' => 'White spaces and special characters are not allowed.')),
         new sfValidatorCallback(array('required'=>true,
        'callback'  => 'MoneyorderForm::findDuplicateMO')
      )));

//      $this->validatorSchema['courier_service'] = new sfValidatorString(array(),array());
//      $this->validatorSchema['waybill_trackingno'] = new sfValidatorString(array(),array());
//      

    //    $this->validatorSchema->setPostValidator(new sfValidatorAnd(array(
    //          new sfValidatorCallback(array('callback' => array($this, 'findDuplicateMO'))))));
    /**
     *
     *
     * Set label
     */
    $this->widgetSchema->setLabels(array(
         'moneyorder_number'    => 'Money Order Number',
         're_moneyorder_number'    => 'Re Enter Money Order Number',
         'amount'    => 'Money Order Amount',
         'paid_date'    => 'Money Order paid date',
         'address'    => 'Address',
         'phone'    => 'Phone Number',
         'moneyorder_office'    => 'Money Order Office',
         'courier_service' =>'Courier Company',
         'courier_flag' =>'Are you sending Money Order via Courier Service',
         'address' =>'Contact Information',
         'phone' =>'Phone',
         'waybill_trackingno'  => 'Waybill Tracking Number'
      )
    );

     $this->validatorSchema->setOption('allow_extra_fields', true);
  }

  public static function findDuplicateMO($validator, $value){

    $moneyOrderNumber =  $_REQUEST['moneyorder']['moneyorder_number'];
    $remoneyOrderNumber =  $_REQUEST['moneyorder']['re_moneyorder_number'];

    if($moneyOrderNumber != $remoneyOrderNumber){
        throw new sfValidatorError($validator, 'Money order serial number and Confirm money order serial number do not match.');
    }else{
        $isMoneyrderNumber = MoneyOrderTable::getInstance()->findByMoneyorderNumber($moneyOrderNumber);
        if(count($isMoneyrderNumber) > 0){
          throw new sfValidatorError($validator, 'The Money Order Serial Number entered is already used.');
        }
        else
        {
          return $value;
        }
    }
    
  }
}
