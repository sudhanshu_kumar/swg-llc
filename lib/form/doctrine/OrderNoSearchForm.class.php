<?php

/**
 * OrderSearch form.
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class OrderNoSearchForm extends sfForm
{
    public function configure()
    {
        $this->widgetSchema['order_number'] = new sfWidgetFormInput(array(),array('maxlength'=>15,'title'=>'Order Number','class'=>'txt-input'));

        $this->setValidators(array(
        'order_number'   => new sfValidatorString(array('required' => true),array('required' => 'Please enter Order Number.')),
            ));


        $this->widgetSchema->setLabels(array('order_number'    => 'Order Number <font color=red>*</font>',
            ));


        $this->widgetSchema->setNameFormat('order[%s]');
        $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    }
 

}
