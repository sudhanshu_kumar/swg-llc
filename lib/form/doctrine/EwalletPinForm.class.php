<?php

/**
 * EwalletPin form.
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class EwalletPinForm extends BaseEwalletPinForm
{
    public static function userVerifyPasswordCallBack($validator, $value, $arguments) {
        
        $pinObj=new pin();
        $ewalletDetail=$pinObj->getEwalletDetail();
        if(!$ewalletDetail)
        {
            throw new sfValidatorError($validator, 'Please generate the pin');
        }
        $pin= $ewalletDetail->getPinNumber();
        $pinRetries= $ewalletDetail->getPinRetries();
        if($pinRetries >=Settings::getMaxNoOfPinRetries())
        {
            throw new sfValidatorError($validator, 'Current pin is blocked. Please regenerate the pin');
        }
        if($pinObj->getEncryptedPin($value)!=$pin){
            $current_pin_retry=$pinObj->setPinRetries();
            if($current_pin_retry==Settings::getMaxNoOfPinRetries())
            {
                $pinObj->blockPin();
                throw new sfValidatorError($validator, 'Current pin is blocked. Please regenerate the pin');
            }
            throw new sfValidatorError($validator, 'invalid');
        }
        return $value;
    }
    public function configure()
    {
        unset(
            $this['user_id'],
            $this['activated_till'],
            $this['no_of_retries'],
            $this['pin_retries'],
            $this['status'],
            $this['created_at'],
            $this['updated_at'],
            $this['created_by'],
            $this['updated_by'],
            $this['deleted_at'],
            $this['deleted']
        );
        //        $userId = sfContext::getInstance()->getUser()->getAttribute('user_id', null, 'sfGuardSecurityUser');
        //        $activated_till=Settings::getEwalletPinActivatedTill();
        //        $dateTime=date('Y-m-d H:m:s');
        //        $dateTimeArray=explode(" ",$dateTime);
        //        $timeArray=explode(":",$dateTimeArray['1']);
        //        $dateArray=explode("-",$dateTimeArray['0']);
        //        $startDate=date('Y-m-d H:m:s',mktime($timeArray['0'],$timeArray['1']+$activated_till,$timeArray['2'],$dateArray['1'],$dateArray['2'],$dateArray['0']));
        //        echo "<pre>";
        //        print_r($dateTime."<br/>");
        //        print_r($startDate);
        //        echo "</pre>";
        //        die;
        $this->widgetSchema['pin_number'] = new sfWidgetFormInputPassword(array(),array('maxlength'=>4,'title'=>'Current Pin','value'=>$this->getOption('value'),'class'=>'txt-input'));
        //        $this->widgetSchema['user_id']= new sfWidgetFormInputHidden(array(),array('value'=>$userId));
        //        $this->widgetSchema['activated_till']= new sfWidgetFormInputHidden(array(),array('value'=>$dateTime));

        $this->validatorSchema['pin_number'] = new sfValidatorCallback(array(
                'callback'  => 'EwalletPinForm::userVerifyPasswordCallBack'));
        $this->validatorSchema['pin_number']->setOption('required',true);
        $this->validatorSchema['pin_number']->setMessage('required','Current pin is required');
        $this->validatorSchema['pin_number']->setMessage('invalid','Current pin is incorrect');

        $this->widgetSchema->setLabels(array(
            'pin_number'    => 'Pin Number ',
            ));
    }

}
