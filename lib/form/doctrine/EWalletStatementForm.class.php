<?php
class EWalletStatementForm  extends sfForm {
protected static $type = array('both'=>'Both', 'credit'=>'Credit', 'debit'=>'Debit');
    public function configure() {
        sfProjectConfiguration::getActive()->loadHelpers('Asset');

        $i18n = sfContext::getInstance()->getI18N();
        $user = sfContext::getInstance()->getUser();

        $culture = $user->getCulture();
        if (strlen($culture) > 2) {
            $culture = substr($culture, 0, 2);
        }

        $sfWidgetFormI18nJQueryDateOptions = array(
      'culture' => $culture,
      'image'   => image_path('calendar.png'),
      'config'  => "{ duration: '' }"
        );

        $this->setWidgets(array(
        //'merchant_id' => new sfWidgetFormDoctrineChoice(array('model' => 'Merchant', 'add_empty' => 'Please select Merchant')),
        'from_date' => new sfWidgetFormInput(array(),array('maxlength'=>10, 'readonly'=>'true','onfocus'=>'showCalendarControl(eWalletStatement_from_date)','id'=>'eWalletStatement_from_date','class'=>'txt-input')),
        'to_date' => new sfWidgetFormInput(array(),array('maxlength'=>10, 'readonly'=>'true','onfocus'=>'showCalendarControl(eWalletStatement_to_date)','id'=>'eWalletStatement_to_date','class'=>'txt-input')),
        'type' => new sfWidgetFormSelect(array('choices' => self::$type), array('lang'=>'blank', 'title'=>'Transaction Type')),
        //'from_date' => new sfWidgetFormDateCal(array(),array('maxlength'=>10, 'readonly'=>'true','class'=>'txt-input')),
        //'to_date' => new sfWidgetFormDateCal(array(),array('maxlength'=>10, 'readonly'=>'true','class'=>'txt-input')),
        //'type' => new sfWidgetFormSelectRadio(array('choices' => self::$type,'default'=>self::$type['both'])),

        //'cource_category' => new sfWidgetFormSelectCheckbox(array('choices' => self::$courceCategory)),

            ));
        
       //$this->setDefault('type','both');
        $this->setValidators(array(
        //'merchant_id' => new sfValidatorDoctrineChoice(array('model' => 'Merchant', 'required' => true),array('required' => 'Please select Merchant')),
        'from_date'   => new sfValidatorDate(array('required' => true),array('required' => 'Please enter the From Date')),
        'to_date'   => new sfValidatorDate(array('required' => true),array('required' => 'Please enter the To Date')),
        'type' => new sfValidatorString(array('required' => true),array('required' => 'Please select transaction type.')),
            ));
     if(isset($_REQUEST['eWalletStatement']['to_date']) && $_REQUEST['eWalletStatement']['to_date']!=""){
        $this->validatorSchema->setPostValidator(

             new sfValidatorAnd(array(
                        new sfValidatorSchemaCompare('from_date', sfValidatorSchemaCompare::LESS_THAN_EQUAL, 'to_date',
                            array('throw_global_error' => true),
                            array('invalid' => 'The From Date ("%left_field%") cannot be greater than To Date (%right_field%)')
                        )
                    ))

            );

      }
        $this->widgetSchema->setLabels(array(
         // 'merchant_id' => 'Merchant',
          'from_date'   =>  'From Date <font color=red>*</font>',
          'to_date'   =>  'To Date <font color=red>*</font>',
          'type'   =>  'Transaction Type',
            ));

        $this->widgetSchema->setNameFormat('eWalletStatement[%s]');
        $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    }
    /*
    public function configure()
    {
       $this->setWidgets(array(
        //'from_date' => new sfWidgetFormInputText(array(),array('accept'=>'blank', 'id'=>'from_date', 'class'=>'date-pick','style'=>'width:70px','readonly'=>'true','title'=>'From','maxlength'=>50)),
        'from_date' => new sfWidgetFormInput(array(),array('maxlength'=>10, 'readonly'=>'true','onfocus'=>'showCalendarControl(eWalletStatement_from_date)','class'=>'txt-input')),
        'to_date' => new sfWidgetFormInput(array(),array('maxlength'=>10, 'readonly'=>'true','onfocus'=>'showCalendarControl(eWalletStatement_to_date)','class'=>'txt-input')),
        ));

        $this->setValidators(array(
            'from_date' => new sfValidatorString(array('max_length' => 254, 'trim' => true),array('required' => 'The element name is required.')),
            'to_date' => new sfValidatorString(array('max_length' => 254, 'trim' => true),array('required' => 'The element name is required.')),
        ));

        $this->widgetSchema->setLabels(array(
                'from_date' => 'From',
                'to_date' => 'To',
            )
        );
        $this->widgetSchema->setNameFormat('eWalletStatement[%s]');
   }
     
     */


}   