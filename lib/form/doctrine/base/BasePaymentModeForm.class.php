<?php

/**
 * PaymentMode form base class.
 *
 * @method PaymentMode getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BasePaymentModeForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'           => new sfWidgetFormInputHidden(),
      'card_type'    => new sfWidgetFormInputText(),
      'gateway_id'   => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Gateway'), 'add_empty' => true)),
      'group_id'     => new sfWidgetFormInputText(),
      'display_name' => new sfWidgetFormInputText(),
      'created_at'   => new sfWidgetFormDateTime(),
      'updated_at'   => new sfWidgetFormDateTime(),
      'created_by'   => new sfWidgetFormInputText(),
      'updated_by'   => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'           => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'card_type'    => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'gateway_id'   => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Gateway'), 'required' => false)),
      'group_id'     => new sfValidatorNumber(array('required' => false)),
      'display_name' => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'created_at'   => new sfValidatorDateTime(),
      'updated_at'   => new sfValidatorDateTime(),
      'created_by'   => new sfValidatorInteger(array('required' => false)),
      'updated_by'   => new sfValidatorInteger(array('required' => false)),
    ));

    $this->validatorSchema->setPostValidator(
      new sfValidatorDoctrineUnique(array('model' => 'PaymentMode', 'column' => array('card_type')))
    );

    $this->widgetSchema->setNameFormat('payment_mode[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'PaymentMode';
  }

}
