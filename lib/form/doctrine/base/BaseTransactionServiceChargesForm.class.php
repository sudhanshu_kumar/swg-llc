<?php

/**
 * TransactionServiceCharges form base class.
 *
 * @method TransactionServiceCharges getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseTransactionServiceChargesForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                         => new sfWidgetFormInputHidden(),
      'app_id'                     => new sfWidgetFormInputText(),
      'ref_no'                     => new sfWidgetFormInputText(),
      'app_type'                   => new sfWidgetFormInputText(),
      'service_charge'             => new sfWidgetFormInputText(),
      'app_amount'                 => new sfWidgetFormInputText(),
      'app_convert_amount'         => new sfWidgetFormInputText(),
      'app_convert_service_charge' => new sfWidgetFormInputText(),
      'app_currency'               => new sfWidgetFormInputText(),
      'order_request_detail_id'    => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('OrderRequestDetails'), 'add_empty' => false)),
      'status'                     => new sfWidgetFormInputText(),
      'created_at'                 => new sfWidgetFormDateTime(),
      'updated_at'                 => new sfWidgetFormDateTime(),
      'created_by'                 => new sfWidgetFormInputText(),
      'updated_by'                 => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'                         => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'app_id'                     => new sfValidatorInteger(),
      'ref_no'                     => new sfValidatorInteger(),
      'app_type'                   => new sfValidatorPass(),
      'service_charge'             => new sfValidatorNumber(),
      'app_amount'                 => new sfValidatorNumber(),
      'app_convert_amount'         => new sfValidatorNumber(),
      'app_convert_service_charge' => new sfValidatorNumber(),
      'app_currency'               => new sfValidatorInteger(array('required' => false)),
      'order_request_detail_id'    => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('OrderRequestDetails'))),
      'status'                     => new sfValidatorInteger(array('required' => false)),
      'created_at'                 => new sfValidatorDateTime(),
      'updated_at'                 => new sfValidatorDateTime(),
      'created_by'                 => new sfValidatorInteger(array('required' => false)),
      'updated_by'                 => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('transaction_service_charges[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'TransactionServiceCharges';
  }

}
