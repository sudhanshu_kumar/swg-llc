<?php

/**
 * Vault form base class.
 *
 * @method Vault getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseVaultForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'             => new sfWidgetFormInputHidden(),
      'gateway_id'     => new sfWidgetFormInputText(),
      'vault_num'      => new sfWidgetFormInputText(),
      'order_number'   => new sfWidgetFormInputText(),
      'payment_status' => new sfWidgetFormChoice(array('choices' => array(0 => 0, 1 => 1, 2 => 2, 3 => 3))),
    ));

    $this->setValidators(array(
      'id'             => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'gateway_id'     => new sfValidatorInteger(array('required' => false)),
      'vault_num'      => new sfValidatorInteger(),
      'order_number'   => new sfValidatorInteger(),
      'payment_status' => new sfValidatorChoice(array('choices' => array(0 => 0, 1 => 1, 2 => 2, 3 => 3), 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('vault[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Vault';
  }

}
