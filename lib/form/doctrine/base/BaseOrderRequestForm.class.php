<?php

/**
 * OrderRequest form base class.
 *
 * @method OrderRequest getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseOrderRequestForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                 => new sfWidgetFormInputHidden(),
      'version'            => new sfWidgetFormInputText(),
      'merchant_id'        => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Merchant'), 'add_empty' => false)),
      'transaction_number' => new sfWidgetFormInputText(),
      'amount'             => new sfWidgetFormInputText(),
      'convert_amount'     => new sfWidgetFormInputText(),
      'currency'           => new sfWidgetFormInputText(),
      'created_at'         => new sfWidgetFormDateTime(),
      'updated_at'         => new sfWidgetFormDateTime(),
      'deleted'            => new sfWidgetFormInputCheckbox(),
      'created_by'         => new sfWidgetFormInputText(),
      'updated_by'         => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'                 => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'version'            => new sfValidatorString(array('max_length' => 10)),
      'merchant_id'        => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Merchant'))),
      'transaction_number' => new sfValidatorInteger(),
      'amount'             => new sfValidatorNumber(),
      'convert_amount'     => new sfValidatorNumber(),
      'currency'           => new sfValidatorInteger(array('required' => false)),
      'created_at'         => new sfValidatorDateTime(),
      'updated_at'         => new sfValidatorDateTime(),
      'deleted'            => new sfValidatorBoolean(array('required' => false)),
      'created_by'         => new sfValidatorInteger(array('required' => false)),
      'updated_by'         => new sfValidatorInteger(array('required' => false)),
    ));

    $this->validatorSchema->setPostValidator(
      new sfValidatorDoctrineUnique(array('model' => 'OrderRequest', 'column' => array('merchant_id', 'transaction_number')))
    );

    $this->widgetSchema->setNameFormat('order_request[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'OrderRequest';
  }

}
