<?php

/**
 * TransactionCharges form base class.
 *
 * @method TransactionCharges getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseTransactionChargesForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                  => new sfWidgetFormInputHidden(),
      'gateway_id'          => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Gateway'), 'add_empty' => false)),
      'transaction_charges' => new sfWidgetFormInputText(),
      'split_type'          => new sfWidgetFormChoice(array('choices' => array('flat' => 'flat', 'percentage' => 'percentage'))),
      'created_at'          => new sfWidgetFormDateTime(),
      'updated_at'          => new sfWidgetFormDateTime(),
      'deleted'             => new sfWidgetFormInputCheckbox(),
      'created_by'          => new sfWidgetFormInputText(),
      'updated_by'          => new sfWidgetFormInputText(),
      'version'             => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'                  => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'gateway_id'          => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Gateway'))),
      'transaction_charges' => new sfValidatorNumber(array('required' => false)),
      'split_type'          => new sfValidatorChoice(array('choices' => array(0 => 'flat', 1 => 'percentage'), 'required' => false)),
      'created_at'          => new sfValidatorDateTime(),
      'updated_at'          => new sfValidatorDateTime(),
      'deleted'             => new sfValidatorBoolean(array('required' => false)),
      'created_by'          => new sfValidatorInteger(array('required' => false)),
      'updated_by'          => new sfValidatorInteger(array('required' => false)),
      'version'             => new sfValidatorInteger(array('required' => false)),
    ));

    $this->validatorSchema->setPostValidator(
      new sfValidatorDoctrineUnique(array('model' => 'TransactionCharges', 'column' => array('merchant_id', 'gateway_id')))
    );

    $this->widgetSchema->setNameFormat('transaction_charges[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'TransactionCharges';
  }

}
