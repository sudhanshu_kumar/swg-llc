<?php

/**
 * CurrencyValue form base class.
 *
 * @method CurrencyValue getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseCurrencyValueForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'              => new sfWidgetFormInputHidden(),
      'value'           => new sfWidgetFormInputText(),
      'currency_date'   => new sfWidgetFormDateTime(),
      'conversion_type' => new sfWidgetFormChoice(array('choices' => array('naira_to_dollor' => 'naira_to_dollor', 'dollor_to_naira' => 'dollor_to_naira'))),
      'description'     => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'              => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'value'           => new sfValidatorNumber(),
      'currency_date'   => new sfValidatorDateTime(),
      'conversion_type' => new sfValidatorChoice(array('choices' => array(0 => 'naira_to_dollor', 1 => 'dollor_to_naira'), 'required' => false)),
      'description'     => new sfValidatorString(array('max_length' => 128, 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('currency_value[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CurrencyValue';
  }

}
