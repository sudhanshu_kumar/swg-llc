<?php

/**
 * Bill form base class.
 *
 * @method Bill getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseBillForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'               => new sfWidgetFormInputHidden(),
      'bill_number'      => new sfWidgetFormInputText(),
      'user_id'          => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('sfGuardUser'), 'add_empty' => false)),
      'order_request_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('OrderRequest'), 'add_empty' => false)),
      'expiry_date'      => new sfWidgetFormDateTime(),
      'status'           => new sfWidgetFormChoice(array('choices' => array('paid' => 'paid', 'unpaid' => 'unpaid'))),
      'created_at'       => new sfWidgetFormDateTime(),
      'updated_at'       => new sfWidgetFormDateTime(),
      'deleted'          => new sfWidgetFormInputCheckbox(),
      'created_by'       => new sfWidgetFormInputText(),
      'updated_by'       => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'               => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'bill_number'      => new sfValidatorInteger(),
      'user_id'          => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('sfGuardUser'))),
      'order_request_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('OrderRequest'))),
      'expiry_date'      => new sfValidatorDateTime(array('required' => false)),
      'status'           => new sfValidatorChoice(array('choices' => array(0 => 'paid', 1 => 'unpaid'), 'required' => false)),
      'created_at'       => new sfValidatorDateTime(),
      'updated_at'       => new sfValidatorDateTime(),
      'deleted'          => new sfValidatorBoolean(array('required' => false)),
      'created_by'       => new sfValidatorInteger(array('required' => false)),
      'updated_by'       => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('bill[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Bill';
  }

}
