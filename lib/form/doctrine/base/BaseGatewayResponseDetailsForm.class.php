<?php

/**
 * GatewayResponseDetails form base class.
 *
 * @method GatewayResponseDetails getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseGatewayResponseDetailsForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                   => new sfWidgetFormInputHidden(),
      'gateway_id'           => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Gateway'), 'add_empty' => false)),
      'response_code'        => new sfWidgetFormInputText(),
      'response_text'        => new sfWidgetFormInputText(),
      'response_description' => new sfWidgetFormTextarea(),
    ));

    $this->setValidators(array(
      'id'                   => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'gateway_id'           => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Gateway'))),
      'response_code'        => new sfValidatorString(array('max_length' => 30, 'required' => false)),
      'response_text'        => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'response_description' => new sfValidatorString(array('max_length' => 1000, 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('gateway_response_details[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'GatewayResponseDetails';
  }

}
