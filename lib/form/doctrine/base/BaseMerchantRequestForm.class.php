<?php

/**
 * MerchantRequest form base class.
 *
 * @method MerchantRequest getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseMerchantRequestForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                       => new sfWidgetFormInputHidden(),
      'txn_ref'                  => new sfWidgetFormInputText(),
      'merchant_id'              => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Merchant'), 'add_empty' => false)),
      'merchant_service_id'      => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('MerchantService'), 'add_empty' => false)),
      'merchant_item_id'         => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('MerchantItem'), 'add_empty' => false)),
      'item_fee'                 => new sfWidgetFormInputText(),
      'payment_status_code'      => new sfWidgetFormInputText(),
      'paid_amount'              => new sfWidgetFormInputText(),
      'paid_date'                => new sfWidgetFormDateTime(),
      'service_charge'           => new sfWidgetFormInputText(),
      'gateway_charge'           => new sfWidgetFormInputText(),
      'payforme_amount'          => new sfWidgetFormInputText(),
      'service_configuration_id' => new sfWidgetFormInputText(),
      'payment_mandate_id'       => new sfWidgetFormInputText(),
      'transaction_location'     => new sfWidgetFormInputText(),
      'validation_number'        => new sfWidgetFormInputText(),
      'created_by'               => new sfWidgetFormInputText(),
      'updated_by'               => new sfWidgetFormInputText(),
      'created_at'               => new sfWidgetFormDateTime(),
      'updated_at'               => new sfWidgetFormDateTime(),
      'deleted'                  => new sfWidgetFormInputCheckbox(),
    ));

    $this->setValidators(array(
      'id'                       => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'txn_ref'                  => new sfValidatorString(array('max_length' => 20)),
      'merchant_id'              => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Merchant'))),
      'merchant_service_id'      => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('MerchantService'))),
      'merchant_item_id'         => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('MerchantItem'))),
      'item_fee'                 => new sfValidatorNumber(),
      'payment_status_code'      => new sfValidatorInteger(array('required' => false)),
      'paid_amount'              => new sfValidatorNumber(array('required' => false)),
      'paid_date'                => new sfValidatorDateTime(array('required' => false)),
      'service_charge'           => new sfValidatorNumber(array('required' => false)),
      'gateway_charge'           => new sfValidatorNumber(array('required' => false)),
      'payforme_amount'          => new sfValidatorNumber(array('required' => false)),
      'service_configuration_id' => new sfValidatorInteger(array('required' => false)),
      'payment_mandate_id'       => new sfValidatorInteger(array('required' => false)),
      'transaction_location'     => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'validation_number'        => new sfValidatorInteger(array('required' => false)),
      'created_by'               => new sfValidatorInteger(array('required' => false)),
      'updated_by'               => new sfValidatorInteger(array('required' => false)),
      'created_at'               => new sfValidatorDateTime(),
      'updated_at'               => new sfValidatorDateTime(),
      'deleted'                  => new sfValidatorBoolean(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('merchant_request[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'MerchantRequest';
  }

}
