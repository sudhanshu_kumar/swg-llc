<?php

/**
 * Wiretransfer form base class.
 *
 * @method Wiretransfer getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseWiretransferForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                   => new sfWidgetFormInputHidden(),
      'wire_transfer_number' => new sfWidgetFormInputText(),
      'amount'               => new sfWidgetFormInputText(),
      'wire_transfer_date'   => new sfWidgetFormDateTime(),
      'paid_date'            => new sfWidgetFormDateTime(),
      'address'              => new sfWidgetFormInputText(),
      'wire_transfer_proof'  => new sfWidgetFormInputText(),
      'phone'                => new sfWidgetFormInputText(),
      'wire_transfer_office' => new sfWidgetFormInputText(),
      'comments'             => new sfWidgetFormInputText(),
      'status'               => new sfWidgetFormChoice(array('choices' => array('New' => 'New', 'Paid' => 'Paid'))),
      'created_at'           => new sfWidgetFormDateTime(),
      'updated_at'           => new sfWidgetFormDateTime(),
      'created_by'           => new sfWidgetFormInputText(),
      'updated_by'           => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'                   => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'wire_transfer_number' => new sfValidatorString(array('max_length' => 30, 'required' => false)),
      'amount'               => new sfValidatorNumber(),
      'wire_transfer_date'   => new sfValidatorDateTime(array('required' => false)),
      'paid_date'            => new sfValidatorDateTime(array('required' => false)),
      'address'              => new sfValidatorPass(array('required' => false)),
      'wire_transfer_proof'  => new sfValidatorPass(array('required' => false)),
      'phone'                => new sfValidatorPass(array('required' => false)),
      'wire_transfer_office' => new sfValidatorPass(array('required' => false)),
      'comments'             => new sfValidatorPass(array('required' => false)),
      'status'               => new sfValidatorChoice(array('choices' => array(0 => 'New', 1 => 'Paid'), 'required' => false)),
      'created_at'           => new sfValidatorDateTime(),
      'updated_at'           => new sfValidatorDateTime(),
      'created_by'           => new sfValidatorInteger(array('required' => false)),
      'updated_by'           => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('wiretransfer[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Wiretransfer';
  }

}
