<?php

/**
 * PaymentResponse form base class.
 *
 * @method PaymentResponse getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BasePaymentResponseForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                          => new sfWidgetFormInputHidden(),
      'request_order_number'        => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Request'), 'add_empty' => false)),
      'response_code'               => new sfWidgetFormInputText(),
      'response_reason'             => new sfWidgetFormInputText(),
      'response_reason_txt'         => new sfWidgetFormInputText(),
      'response_reason_sub_code'    => new sfWidgetFormInputText(),
      'response_transaction_code'   => new sfWidgetFormInputText(),
      'response_authorization_code' => new sfWidgetFormInputText(),
      'response_transaction_date'   => new sfWidgetFormDateTime(),
      'created_at'                  => new sfWidgetFormDateTime(),
      'updated_at'                  => new sfWidgetFormDateTime(),
      'deleted'                     => new sfWidgetFormInputCheckbox(),
      'created_by'                  => new sfWidgetFormInputText(),
      'updated_by'                  => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'                          => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'request_order_number'        => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Request'))),
      'response_code'               => new sfValidatorInteger(),
      'response_reason'             => new sfValidatorString(array('max_length' => 10, 'required' => false)),
      'response_reason_txt'         => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'response_reason_sub_code'    => new sfValidatorInteger(array('required' => false)),
      'response_transaction_code'   => new sfValidatorInteger(array('required' => false)),
      'response_authorization_code' => new sfValidatorString(array('max_length' => 10, 'required' => false)),
      'response_transaction_date'   => new sfValidatorDateTime(array('required' => false)),
      'created_at'                  => new sfValidatorDateTime(),
      'updated_at'                  => new sfValidatorDateTime(),
      'deleted'                     => new sfValidatorBoolean(array('required' => false)),
      'created_by'                  => new sfValidatorInteger(array('required' => false)),
      'updated_by'                  => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('payment_response[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'PaymentResponse';
  }

}
