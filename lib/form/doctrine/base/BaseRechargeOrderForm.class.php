<?php

/**
 * RechargeOrder form base class.
 *
 * @method RechargeOrder getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseRechargeOrderForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'             => new sfWidgetFormInputHidden(),
      'user_id'        => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('sfGuardUser'), 'add_empty' => false)),
      'gateway_id'     => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Gateway'), 'add_empty' => false)),
      'account_no'     => new sfWidgetFormInputText(),
      'amount'         => new sfWidgetFormInputText(),
      'item_fee'       => new sfWidgetFormInputText(),
      'service_charge' => new sfWidgetFormInputText(),
      'payment_status' => new sfWidgetFormChoice(array('choices' => array(0 => 0, 1 => 1))),
      'paid_date'      => new sfWidgetFormDateTime(),
      'created_at'     => new sfWidgetFormDateTime(),
      'updated_at'     => new sfWidgetFormDateTime(),
      'deleted'        => new sfWidgetFormInputCheckbox(),
      'created_by'     => new sfWidgetFormInputText(),
      'updated_by'     => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'             => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'user_id'        => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('sfGuardUser'))),
      'gateway_id'     => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Gateway'))),
      'account_no'     => new sfValidatorInteger(),
      'amount'         => new sfValidatorNumber(),
      'item_fee'       => new sfValidatorNumber(),
      'service_charge' => new sfValidatorNumber(),
      'payment_status' => new sfValidatorChoice(array('choices' => array(0 => 0, 1 => 1), 'required' => false)),
      'paid_date'      => new sfValidatorDateTime(array('required' => false)),
      'created_at'     => new sfValidatorDateTime(),
      'updated_at'     => new sfValidatorDateTime(),
      'deleted'        => new sfValidatorBoolean(array('required' => false)),
      'created_by'     => new sfValidatorInteger(array('required' => false)),
      'updated_by'     => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('recharge_order[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'RechargeOrder';
  }

}
