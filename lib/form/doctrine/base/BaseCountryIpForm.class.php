<?php

/**
 * CountryIp form base class.
 *
 * @method CountryIp getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseCountryIpForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'       => new sfWidgetFormInputHidden(),
      'ipfrom'   => new sfWidgetFormInputText(),
      'ipto'     => new sfWidgetFormInputText(),
      'registry' => new sfWidgetFormInputText(),
      'assigned' => new sfWidgetFormInputText(),
      'iso2'     => new sfWidgetFormInputText(),
      'country'  => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'       => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'ipfrom'   => new sfValidatorInteger(),
      'ipto'     => new sfValidatorInteger(),
      'registry' => new sfValidatorString(array('max_length' => 10)),
      'assigned' => new sfValidatorString(array('max_length' => 10)),
      'iso2'     => new sfValidatorString(array('max_length' => 3)),
      'country'  => new sfValidatorString(array('max_length' => 40)),
    ));

    $this->widgetSchema->setNameFormat('country_ip[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CountryIp';
  }

}
