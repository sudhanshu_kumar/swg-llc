<?php

/**
 * EwalletPin form base class.
 *
 * @method EwalletPin getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseEwalletPinForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'             => new sfWidgetFormInputHidden(),
      'user_id'        => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('sfGuardUser'), 'add_empty' => false)),
      'pin_number'     => new sfWidgetFormInputText(),
      'status'         => new sfWidgetFormChoice(array('choices' => array('blocked' => 'blocked', 'active' => 'active', 'inactive' => 'inactive'))),
      'no_of_retries'  => new sfWidgetFormInputText(),
      'pin_retries'    => new sfWidgetFormInputText(),
      'activated_till' => new sfWidgetFormInputText(),
      'created_at'     => new sfWidgetFormDateTime(),
      'updated_at'     => new sfWidgetFormDateTime(),
      'deleted'        => new sfWidgetFormInputCheckbox(),
      'created_by'     => new sfWidgetFormInputText(),
      'updated_by'     => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'             => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'user_id'        => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('sfGuardUser'))),
      'pin_number'     => new sfValidatorString(array('max_length' => 255)),
      'status'         => new sfValidatorChoice(array('choices' => array(0 => 'blocked', 1 => 'active', 2 => 'inactive'), 'required' => false)),
      'no_of_retries'  => new sfValidatorInteger(array('required' => false)),
      'pin_retries'    => new sfValidatorInteger(array('required' => false)),
      'activated_till' => new sfValidatorString(array('max_length' => 255)),
      'created_at'     => new sfValidatorDateTime(),
      'updated_at'     => new sfValidatorDateTime(),
      'deleted'        => new sfValidatorBoolean(array('required' => false)),
      'created_by'     => new sfValidatorInteger(array('required' => false)),
      'updated_by'     => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('ewallet_pin[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'EwalletPin';
  }

}
