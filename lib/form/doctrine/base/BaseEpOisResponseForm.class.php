<?php

/**
 * EpOisResponse form base class.
 *
 * @method EpOisResponse getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseEpOisResponseForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                 => new sfWidgetFormInputHidden(),
      'ois_req_id'         => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('EpOisRequest'), 'add_empty' => false)),
      'token_id'           => new sfWidgetFormInputText(),
      'order_id'           => new sfWidgetFormInputText(),
      'result'             => new sfWidgetFormInputText(),
      'result_text'        => new sfWidgetFormInputText(),
      'transaction_id'     => new sfWidgetFormInputText(),
      'result_code'        => new sfWidgetFormInputText(),
      'authorization_code' => new sfWidgetFormInputText(),
      'avs_result'         => new sfWidgetFormInputText(),
      'action_type'        => new sfWidgetFormInputText(),
      'amount'             => new sfWidgetFormInputText(),
      'ip_address'         => new sfWidgetFormInputText(),
      'industry'           => new sfWidgetFormInputText(),
      'city'               => new sfWidgetFormInputText(),
      'processor_id'       => new sfWidgetFormInputText(),
      'currency'           => new sfWidgetFormInputText(),
      'tax_amount'         => new sfWidgetFormInputText(),
      'shipping_amount'    => new sfWidgetFormInputText(),
      'billing'            => new sfWidgetFormInputText(),
      'first_name'         => new sfWidgetFormInputText(),
      'last_name'          => new sfWidgetFormInputText(),
      'email'              => new sfWidgetFormInputText(),
      'phone'              => new sfWidgetFormInputText(),
      'address1'           => new sfWidgetFormInputText(),
      'address2'           => new sfWidgetFormInputText(),
      'town'               => new sfWidgetFormInputText(),
      'postal'             => new sfWidgetFormInputText(),
      'country'            => new sfWidgetFormInputText(),
      'state'              => new sfWidgetFormInputText(),
      'cc_number'          => new sfWidgetFormInputText(),
      'cc_exp'             => new sfWidgetFormDateTime(),
      'created_at'         => new sfWidgetFormDateTime(),
      'updated_at'         => new sfWidgetFormDateTime(),
      'created_by'         => new sfWidgetFormInputText(),
      'updated_by'         => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'                 => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'ois_req_id'         => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('EpOisRequest'))),
      'token_id'           => new sfValidatorString(array('max_length' => 50)),
      'order_id'           => new sfValidatorString(array('max_length' => 100)),
      'result'             => new sfValidatorInteger(array('required' => false)),
      'result_text'        => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'transaction_id'     => new sfValidatorPass(array('required' => false)),
      'result_code'        => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'authorization_code' => new sfValidatorPass(array('required' => false)),
      'avs_result'         => new sfValidatorString(array('max_length' => 30)),
      'action_type'        => new sfValidatorString(array('max_length' => 30, 'required' => false)),
      'amount'             => new sfValidatorNumber(array('required' => false)),
      'ip_address'         => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'industry'           => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'city'               => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'processor_id'       => new sfValidatorPass(array('required' => false)),
      'currency'           => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'tax_amount'         => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'shipping_amount'    => new sfValidatorPass(array('required' => false)),
      'billing'            => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'first_name'         => new sfValidatorString(array('max_length' => 30, 'required' => false)),
      'last_name'          => new sfValidatorString(array('max_length' => 30, 'required' => false)),
      'email'              => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'phone'              => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'address1'           => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'address2'           => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'town'               => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'postal'             => new sfValidatorString(array('max_length' => 55, 'required' => false)),
      'country'            => new sfValidatorString(array('max_length' => 55, 'required' => false)),
      'state'              => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'cc_number'          => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'cc_exp'             => new sfValidatorDateTime(array('required' => false)),
      'created_at'         => new sfValidatorDateTime(),
      'updated_at'         => new sfValidatorDateTime(),
      'created_by'         => new sfValidatorInteger(array('required' => false)),
      'updated_by'         => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('ep_ois_response[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'EpOisResponse';
  }

}
