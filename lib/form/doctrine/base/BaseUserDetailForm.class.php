<?php

/**
 * UserDetail form base class.
 *
 * @method UserDetail getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseUserDetailForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                 => new sfWidgetFormInputHidden(),
      'user_id'            => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('sfGuardUser'), 'add_empty' => false)),
      'first_name'         => new sfWidgetFormInputText(),
      'last_name'          => new sfWidgetFormInputText(),
      'dob'                => new sfWidgetFormDate(),
      'address'            => new sfWidgetFormInputText(),
      'email'              => new sfWidgetFormInputText(),
      'mobile_phone'       => new sfWidgetFormInputText(),
      'work_phone'         => new sfWidgetFormInputText(),
      'failed_attempt'     => new sfWidgetFormInputText(),
      'kyc_status'         => new sfWidgetFormInputText(),
      'approved_by'        => new sfWidgetFormInputText(),
      'disapprove_reason'  => new sfWidgetFormInputText(),
      'master_account_id'  => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('EpMasterAccount'), 'add_empty' => true)),
      'sms_charge'         => new sfWidgetFormInputText(),
      'is_sms_charge_paid' => new sfWidgetFormChoice(array('choices' => array('yes' => 'yes', 'no' => 'no'))),
      'cart_items'         => new sfWidgetFormInputText(),
      'payment_rights'     => new sfWidgetFormInputText(),
      'created_at'         => new sfWidgetFormDateTime(),
      'updated_at'         => new sfWidgetFormDateTime(),
      'deleted'            => new sfWidgetFormInputCheckbox(),
      'created_by'         => new sfWidgetFormInputText(),
      'updated_by'         => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'                 => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'user_id'            => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('sfGuardUser'))),
      'first_name'         => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'last_name'          => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'dob'                => new sfValidatorDate(array('required' => false)),
      'address'            => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'email'              => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'mobile_phone'       => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'work_phone'         => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'failed_attempt'     => new sfValidatorInteger(array('required' => false)),
      'kyc_status'         => new sfValidatorInteger(array('required' => false)),
      'approved_by'        => new sfValidatorInteger(array('required' => false)),
      'disapprove_reason'  => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'master_account_id'  => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('EpMasterAccount'), 'required' => false)),
      'sms_charge'         => new sfValidatorNumber(),
      'is_sms_charge_paid' => new sfValidatorChoice(array('choices' => array(0 => 'yes', 1 => 'no'), 'required' => false)),
      'cart_items'         => new sfValidatorPass(array('required' => false)),
      'payment_rights'     => new sfValidatorInteger(array('required' => false)),
      'created_at'         => new sfValidatorDateTime(),
      'updated_at'         => new sfValidatorDateTime(),
      'deleted'            => new sfValidatorBoolean(array('required' => false)),
      'created_by'         => new sfValidatorInteger(array('required' => false)),
      'updated_by'         => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('user_detail[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'UserDetail';
  }

}
