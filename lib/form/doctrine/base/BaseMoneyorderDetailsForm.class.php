<?php

/**
 * MoneyorderDetails form base class.
 *
 * @method MoneyorderDetails getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseMoneyorderDetailsForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                      => new sfWidgetFormInputHidden(),
      'order_number'            => new sfWidgetFormInputText(),
      'tracking_number'         => new sfWidgetFormInputText(),
      'order_request_detail_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('OrderRequestDetails'), 'add_empty' => false)),
      'moneyorder_number'       => new sfWidgetFormInputText(),
      'amount'                  => new sfWidgetFormInputText(),
      'moneyorder_date'         => new sfWidgetFormDateTime(),
      'paid_date'               => new sfWidgetFormDateTime(),
      'address'                 => new sfWidgetFormInputText(),
      'phone'                   => new sfWidgetFormInputText(),
      'moneyorder_office'       => new sfWidgetFormInputText(),
      'created_at'              => new sfWidgetFormDateTime(),
      'updated_at'              => new sfWidgetFormDateTime(),
      'created_by'              => new sfWidgetFormInputText(),
      'updated_by'              => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'                      => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'order_number'            => new sfValidatorInteger(array('required' => false)),
      'tracking_number'         => new sfValidatorString(array('max_length' => 30)),
      'order_request_detail_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('OrderRequestDetails'))),
      'moneyorder_number'       => new sfValidatorString(array('max_length' => 30, 'required' => false)),
      'amount'                  => new sfValidatorNumber(),
      'moneyorder_date'         => new sfValidatorDateTime(array('required' => false)),
      'paid_date'               => new sfValidatorDateTime(array('required' => false)),
      'address'                 => new sfValidatorPass(array('required' => false)),
      'phone'                   => new sfValidatorPass(array('required' => false)),
      'moneyorder_office'       => new sfValidatorPass(array('required' => false)),
      'created_at'              => new sfValidatorDateTime(),
      'updated_at'              => new sfValidatorDateTime(),
      'created_by'              => new sfValidatorInteger(array('required' => false)),
      'updated_by'              => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('moneyorder_details[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'MoneyorderDetails';
  }

}
