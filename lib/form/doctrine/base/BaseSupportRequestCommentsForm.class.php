<?php

/**
 * SupportRequestComments form base class.
 *
 * @method SupportRequestComments getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseSupportRequestCommentsForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                    => new sfWidgetFormInputHidden(),
      'request_id'            => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('SupportRequestList'), 'add_empty' => false)),
      'support_user_comments' => new sfWidgetFormInputText(),
      'customer_comments'     => new sfWidgetFormInputText(),
      'upload_document1'      => new sfWidgetFormInputText(),
      'upload_document2'      => new sfWidgetFormInputText(),
      'upload_document3'      => new sfWidgetFormInputText(),
      'upload_document4'      => new sfWidgetFormInputText(),
      'user_id'               => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('sfGuardUser'), 'add_empty' => false)),
      'created_at'            => new sfWidgetFormDateTime(),
      'updated_at'            => new sfWidgetFormDateTime(),
      'created_by'            => new sfWidgetFormInputText(),
      'updated_by'            => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'                    => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'request_id'            => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('SupportRequestList'))),
      'support_user_comments' => new sfValidatorPass(array('required' => false)),
      'customer_comments'     => new sfValidatorPass(array('required' => false)),
      'upload_document1'      => new sfValidatorPass(array('required' => false)),
      'upload_document2'      => new sfValidatorPass(array('required' => false)),
      'upload_document3'      => new sfValidatorPass(array('required' => false)),
      'upload_document4'      => new sfValidatorPass(array('required' => false)),
      'user_id'               => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('sfGuardUser'))),
      'created_at'            => new sfValidatorDateTime(),
      'updated_at'            => new sfValidatorDateTime(),
      'created_by'            => new sfValidatorInteger(array('required' => false)),
      'updated_by'            => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('support_request_comments[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'SupportRequestComments';
  }

}
