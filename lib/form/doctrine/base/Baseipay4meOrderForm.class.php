<?php

/**
 * ipay4meOrder form base class.
 *
 * @method ipay4meOrder getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class Baseipay4meOrderForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                      => new sfWidgetFormInputHidden(),
      'order_number'            => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('OrderAuthorize'), 'add_empty' => false)),
      'validation_number'       => new sfWidgetFormInputText(),
      'gateway_id'              => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Gateway'), 'add_empty' => false)),
      'order_request_detail_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('OrderRequestDetails'), 'add_empty' => false)),
      'payment_status'          => new sfWidgetFormChoice(array('choices' => array(0 => 0, 1 => 1))),
      'processing_country'      => new sfWidgetFormInputText(),
      'response_code'           => new sfWidgetFormInputText(),
      'response_text'           => new sfWidgetFormTextarea(),
      'payor_name'              => new sfWidgetFormInputText(),
      'card_holder'             => new sfWidgetFormInputText(),
      'card_first'              => new sfWidgetFormInputText(),
      'card_last'               => new sfWidgetFormInputText(),
      'card_hash'               => new sfWidgetFormInputText(),
      'card_len'                => new sfWidgetFormInputText(),
      'card_type'               => new sfWidgetFormInputText(),
      'address'                 => new sfWidgetFormInputText(),
      'phone'                   => new sfWidgetFormInputText(),
      'jobId'                   => new sfWidgetFormInputText(),
      'created_at'              => new sfWidgetFormDateTime(),
      'updated_at'              => new sfWidgetFormDateTime(),
      'created_by'              => new sfWidgetFormInputText(),
      'updated_by'              => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'                      => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'order_number'            => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('OrderAuthorize'))),
      'validation_number'       => new sfValidatorInteger(),
      'gateway_id'              => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Gateway'))),
      'order_request_detail_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('OrderRequestDetails'))),
      'payment_status'          => new sfValidatorChoice(array('choices' => array(0 => 0, 1 => 1), 'required' => false)),
      'processing_country'      => new sfValidatorPass(array('required' => false)),
      'response_code'           => new sfValidatorString(array('max_length' => 30, 'required' => false)),
      'response_text'           => new sfValidatorString(array('max_length' => 1000, 'required' => false)),
      'payor_name'              => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'card_holder'             => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'card_first'              => new sfValidatorPass(array('required' => false)),
      'card_last'               => new sfValidatorPass(array('required' => false)),
      'card_hash'               => new sfValidatorPass(array('required' => false)),
      'card_len'                => new sfValidatorInteger(array('required' => false)),
      'card_type'               => new sfValidatorPass(array('required' => false)),
      'address'                 => new sfValidatorPass(array('required' => false)),
      'phone'                   => new sfValidatorPass(array('required' => false)),
      'jobId'                   => new sfValidatorInteger(array('required' => false)),
      'created_at'              => new sfValidatorDateTime(),
      'updated_at'              => new sfValidatorDateTime(),
      'created_by'              => new sfValidatorInteger(array('required' => false)),
      'updated_by'              => new sfValidatorInteger(array('required' => false)),
    ));

    $this->validatorSchema->setPostValidator(
      new sfValidatorDoctrineUnique(array('model' => 'ipay4meOrder', 'column' => array('order_number')))
    );

    $this->widgetSchema->setNameFormat('ipay4me_order[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'ipay4meOrder';
  }

}
