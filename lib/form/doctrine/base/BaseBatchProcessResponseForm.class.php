<?php

/**
 * BatchProcessResponse form base class.
 *
 * @method BatchProcessResponse getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseBatchProcessResponseForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'            => new sfWidgetFormInputHidden(),
      'request_id'    => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('BatchProcessRequest'), 'add_empty' => false)),
      'response'      => new sfWidgetFormInputText(),
      'response_text' => new sfWidgetFormInputText(),
      'authcode'      => new sfWidgetFormInputText(),
      'transactionid' => new sfWidgetFormInputText(),
      'type'          => new sfWidgetFormInputText(),
      'orderid'       => new sfWidgetFormInputText(),
      'amount'        => new sfWidgetFormInputText(),
      'avsresponse'   => new sfWidgetFormInputText(),
      'cvvresponse'   => new sfWidgetFormInputText(),
      'response_code' => new sfWidgetFormInputText(),
      'created_at'    => new sfWidgetFormDateTime(),
      'updated_at'    => new sfWidgetFormDateTime(),
      'created_by'    => new sfWidgetFormInputText(),
      'updated_by'    => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'            => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'request_id'    => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('BatchProcessRequest'))),
      'response'      => new sfValidatorString(array('max_length' => 20)),
      'response_text' => new sfValidatorString(array('max_length' => 20)),
      'authcode'      => new sfValidatorString(array('max_length' => 20)),
      'transactionid' => new sfValidatorString(array('max_length' => 20)),
      'type'          => new sfValidatorString(array('max_length' => 20)),
      'orderid'       => new sfValidatorString(array('max_length' => 20)),
      'amount'        => new sfValidatorNumber(),
      'avsresponse'   => new sfValidatorString(array('max_length' => 20)),
      'cvvresponse'   => new sfValidatorString(array('max_length' => 20)),
      'response_code' => new sfValidatorString(array('max_length' => 20)),
      'created_at'    => new sfValidatorDateTime(),
      'updated_at'    => new sfValidatorDateTime(),
      'created_by'    => new sfValidatorInteger(array('required' => false)),
      'updated_by'    => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('batch_process_response[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'BatchProcessResponse';
  }

}
