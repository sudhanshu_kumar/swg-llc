<?php

/**
 * VbvOrder form base class.
 *
 * @method VbvOrder getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseVbvOrderForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'              => new sfWidgetFormInputHidden(),
      'order_id'        => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('EpVbvRequest'), 'add_empty' => false)),
      'order_detail_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('OrderRequestDetails'), 'add_empty' => false)),
      'type'            => new sfWidgetFormChoice(array('choices' => array('pay' => 'pay', 'recharge' => 'recharge'))),
    ));

    $this->setValidators(array(
      'id'              => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'order_id'        => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('EpVbvRequest'))),
      'order_detail_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('OrderRequestDetails'))),
      'type'            => new sfValidatorChoice(array('choices' => array(0 => 'pay', 1 => 'recharge'), 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('vbv_order[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'VbvOrder';
  }

}
