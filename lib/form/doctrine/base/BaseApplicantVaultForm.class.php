<?php

/**
 * ApplicantVault form base class.
 *
 * @method ApplicantVault getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseApplicantVaultForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'               => new sfWidgetFormInputHidden(),
      'card_first'       => new sfWidgetFormInputText(),
      'card_last'        => new sfWidgetFormInputText(),
      'card_hash'        => new sfWidgetFormInputText(),
      'card_len'         => new sfWidgetFormInputText(),
      'card_type'        => new sfWidgetFormInputText(),
      'card_holder'      => new sfWidgetFormInputText(),
      'expiry_month'     => new sfWidgetFormInputText(),
      'expiry_year'      => new sfWidgetFormInputText(),
      'cvv'              => new sfWidgetFormInputText(),
      'address1'         => new sfWidgetFormInputText(),
      'address2'         => new sfWidgetFormInputText(),
      'town'             => new sfWidgetFormInputText(),
      'state'            => new sfWidgetFormInputText(),
      'zip'              => new sfWidgetFormInputText(),
      'country'          => new sfWidgetFormInputText(),
      'first_name'       => new sfWidgetFormInputText(),
      'last_name'        => new sfWidgetFormInputText(),
      'email'            => new sfWidgetFormInputText(),
      'phone'            => new sfWidgetFormInputText(),
      'address_proof'    => new sfWidgetFormInputText(),
      'user_id_proof'    => new sfWidgetFormInputText(),
      'failed_attempts'  => new sfWidgetFormInputText(),
      'status'           => new sfWidgetFormChoice(array('choices' => array('New' => 'New', 'Approved' => 'Approved', 'Reject' => 'Reject'))),
      'approver_id'      => new sfWidgetFormInputText(),
      'comments'         => new sfWidgetFormInputText(),
      'transaction_type' => new sfWidgetFormInputText(),
      'reason'           => new sfWidgetFormInputText(),
      'agent_proof'      => new sfWidgetFormInputText(),
      'user_id'          => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('sfGuardUser'), 'add_empty' => true)),
      'created_at'       => new sfWidgetFormDateTime(),
      'updated_at'       => new sfWidgetFormDateTime(),
      'created_by'       => new sfWidgetFormInputText(),
      'updated_by'       => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'               => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'card_first'       => new sfValidatorString(array('max_length' => 5)),
      'card_last'        => new sfValidatorString(array('max_length' => 5)),
      'card_hash'        => new sfValidatorString(array('max_length' => 128)),
      'card_len'         => new sfValidatorInteger(array('required' => false)),
      'card_type'        => new sfValidatorPass(array('required' => false)),
      'card_holder'      => new sfValidatorString(array('max_length' => 255)),
      'expiry_month'     => new sfValidatorString(array('max_length' => 5)),
      'expiry_year'      => new sfValidatorString(array('max_length' => 10)),
      'cvv'              => new sfValidatorInteger(),
      'address1'         => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'address2'         => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'town'             => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'state'            => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'zip'              => new sfValidatorString(array('max_length' => 15, 'required' => false)),
      'country'          => new sfValidatorString(array('max_length' => 150, 'required' => false)),
      'first_name'       => new sfValidatorString(array('max_length' => 30, 'required' => false)),
      'last_name'        => new sfValidatorString(array('max_length' => 30, 'required' => false)),
      'email'            => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'phone'            => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'address_proof'    => new sfValidatorString(array('max_length' => 255)),
      'user_id_proof'    => new sfValidatorString(array('max_length' => 255)),
      'failed_attempts'  => new sfValidatorInteger(array('required' => false)),
      'status'           => new sfValidatorChoice(array('choices' => array(0 => 'New', 1 => 'Approved', 2 => 'Reject'), 'required' => false)),
      'approver_id'      => new sfValidatorInteger(array('required' => false)),
      'comments'         => new sfValidatorString(array('max_length' => 255)),
      'transaction_type' => new sfValidatorString(array('max_length' => 255)),
      'reason'           => new sfValidatorPass(),
      'agent_proof'      => new sfValidatorString(array('max_length' => 255)),
      'user_id'          => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('sfGuardUser'), 'required' => false)),
      'created_at'       => new sfValidatorDateTime(),
      'updated_at'       => new sfValidatorDateTime(),
      'created_by'       => new sfValidatorInteger(array('required' => false)),
      'updated_by'       => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('applicant_vault[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'ApplicantVault';
  }

}
