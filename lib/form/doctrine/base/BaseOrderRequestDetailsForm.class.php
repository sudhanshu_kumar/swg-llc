<?php

/**
 * OrderRequestDetails form base class.
 *
 * @method OrderRequestDetails getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseOrderRequestDetailsForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'               => new sfWidgetFormInputHidden(),
      'order_request_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('OrderRequest'), 'add_empty' => false)),
      'user_id'          => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('sfGuardUser'), 'add_empty' => true)),
      'merchant_id'      => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Merchant'), 'add_empty' => false)),
      'retry_id'         => new sfWidgetFormInputText(),
      'name'             => new sfWidgetFormInputText(),
      'description'      => new sfWidgetFormTextarea(),
      'amount'           => new sfWidgetFormInputText(),
      'convert_amount'   => new sfWidgetFormInputText(),
      'currency'         => new sfWidgetFormInputText(),
      'payment_status'   => new sfWidgetFormChoice(array('choices' => array(0 => 0, 1 => 1))),
      'paid_date'        => new sfWidgetFormDateTime(),
      'created_at'       => new sfWidgetFormDateTime(),
      'updated_at'       => new sfWidgetFormDateTime(),
      'deleted'          => new sfWidgetFormInputCheckbox(),
      'created_by'       => new sfWidgetFormInputText(),
      'updated_by'       => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'               => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'order_request_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('OrderRequest'))),
      'user_id'          => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('sfGuardUser'), 'required' => false)),
      'merchant_id'      => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Merchant'))),
      'retry_id'         => new sfValidatorInteger(),
      'name'             => new sfValidatorString(array('max_length' => 100)),
      'description'      => new sfValidatorString(array('max_length' => 1000)),
      'amount'           => new sfValidatorNumber(),
      'convert_amount'   => new sfValidatorNumber(),
      'currency'         => new sfValidatorInteger(array('required' => false)),
      'payment_status'   => new sfValidatorChoice(array('choices' => array(0 => 0, 1 => 1), 'required' => false)),
      'paid_date'        => new sfValidatorDateTime(array('required' => false)),
      'created_at'       => new sfValidatorDateTime(),
      'updated_at'       => new sfValidatorDateTime(),
      'deleted'          => new sfValidatorBoolean(array('required' => false)),
      'created_by'       => new sfValidatorInteger(array('required' => false)),
      'updated_by'       => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('order_request_details[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'OrderRequestDetails';
  }

}
