<?php

/**
 * TrackingMoneyorder form base class.
 *
 * @method TrackingMoneyorder getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseTrackingMoneyorderForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'            => new sfWidgetFormInputHidden(),
      'cart_track_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('CartTrackingNumber'), 'add_empty' => false)),
      'moneyorder_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Moneyorder'), 'add_empty' => false)),
      'created_at'    => new sfWidgetFormDateTime(),
      'updated_at'    => new sfWidgetFormDateTime(),
      'created_by'    => new sfWidgetFormInputText(),
      'updated_by'    => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'            => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'cart_track_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('CartTrackingNumber'))),
      'moneyorder_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Moneyorder'))),
      'created_at'    => new sfValidatorDateTime(),
      'updated_at'    => new sfValidatorDateTime(),
      'created_by'    => new sfValidatorInteger(array('required' => false)),
      'updated_by'    => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('tracking_moneyorder[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'TrackingMoneyorder';
  }

}
