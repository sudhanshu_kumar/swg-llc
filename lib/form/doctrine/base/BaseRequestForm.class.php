<?php

/**
 * Request form base class.
 *
 * @method Request getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseRequestForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                   => new sfWidgetFormInputHidden(),
      'order_detail_id'      => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('OrderRequestDetails'), 'add_empty' => false)),
      'request_order_number' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('PaymentResponse'), 'add_empty' => false)),
      'amount'               => new sfWidgetFormInputText(),
      'card_details_first'   => new sfWidgetFormInputText(),
      'card_details_last'    => new sfWidgetFormInputText(),
      'created_at'           => new sfWidgetFormDateTime(),
      'updated_at'           => new sfWidgetFormDateTime(),
      'deleted'              => new sfWidgetFormInputCheckbox(),
      'created_by'           => new sfWidgetFormInputText(),
      'updated_by'           => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'                   => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'order_detail_id'      => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('OrderRequestDetails'))),
      'request_order_number' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('PaymentResponse'))),
      'amount'               => new sfValidatorNumber(array('required' => false)),
      'card_details_first'   => new sfValidatorInteger(array('required' => false)),
      'card_details_last'    => new sfValidatorInteger(array('required' => false)),
      'created_at'           => new sfValidatorDateTime(),
      'updated_at'           => new sfValidatorDateTime(),
      'deleted'              => new sfValidatorBoolean(array('required' => false)),
      'created_by'           => new sfValidatorInteger(array('required' => false)),
      'updated_by'           => new sfValidatorInteger(array('required' => false)),
    ));

    $this->validatorSchema->setPostValidator(
      new sfValidatorDoctrineUnique(array('model' => 'Request', 'column' => array('request_order_number')))
    );

    $this->widgetSchema->setNameFormat('request[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Request';
  }

}
