<?php

/**
 * EpMasterLedger form base class.
 *
 * @method EpMasterLedger getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseEpMasterLedgerForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                         => new sfWidgetFormInputHidden(),
      'master_account_id'          => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('EpMasterAccount'), 'add_empty' => true)),
      'entry_type'                 => new sfWidgetFormChoice(array('choices' => array('credit' => 'credit', 'debit' => 'debit'))),
      'is_cleared'                 => new sfWidgetFormChoice(array('choices' => array(1 => 1, 0 => 0))),
      'amount'                     => new sfWidgetFormInputText(),
      'description'                => new sfWidgetFormInputText(),
      'transaction_date'           => new sfWidgetFormDateTime(),
      'payment_transaction_number' => new sfWidgetFormInputText(),
      'created_at'                 => new sfWidgetFormDateTime(),
      'updated_at'                 => new sfWidgetFormDateTime(),
      'created_by'                 => new sfWidgetFormInputText(),
      'updated_by'                 => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'                         => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'master_account_id'          => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('EpMasterAccount'), 'required' => false)),
      'entry_type'                 => new sfValidatorChoice(array('choices' => array(0 => 'credit', 1 => 'debit'))),
      'is_cleared'                 => new sfValidatorChoice(array('choices' => array(0 => 1, 1 => 0), 'required' => false)),
      'amount'                     => new sfValidatorInteger(),
      'description'                => new sfValidatorString(array('max_length' => 255)),
      'transaction_date'           => new sfValidatorDateTime(),
      'payment_transaction_number' => new sfValidatorInteger(),
      'created_at'                 => new sfValidatorDateTime(),
      'updated_at'                 => new sfValidatorDateTime(),
      'created_by'                 => new sfValidatorInteger(array('required' => false)),
      'updated_by'                 => new sfValidatorInteger(array('required' => false)),
    ));

    $this->validatorSchema->setPostValidator(
      new sfValidatorDoctrineUnique(array('model' => 'EpMasterLedger', 'column' => array('payment_transaction_number')))
    );

    $this->widgetSchema->setNameFormat('ep_master_ledger[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'EpMasterLedger';
  }

}
