<?php

/**
 * EpMasterAccount form base class.
 *
 * @method EpMasterAccount getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseEpMasterAccountForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'              => new sfWidgetFormInputHidden(),
      'clear_balance'   => new sfWidgetFormInputText(),
      'unclear_balance' => new sfWidgetFormInputText(),
      'account_number'  => new sfWidgetFormInputText(),
      'account_name'    => new sfWidgetFormInputText(),
      'sortcode'        => new sfWidgetFormInputText(),
      'bankname'        => new sfWidgetFormInputText(),
      'type'            => new sfWidgetFormChoice(array('choices' => array('collection' => 'collection', 'receiving' => 'receiving', 'ewallet' => 'ewallet'))),
      'created_at'      => new sfWidgetFormDateTime(),
      'updated_at'      => new sfWidgetFormDateTime(),
      'created_by'      => new sfWidgetFormInputText(),
      'updated_by'      => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'              => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'clear_balance'   => new sfValidatorInteger(array('required' => false)),
      'unclear_balance' => new sfValidatorInteger(array('required' => false)),
      'account_number'  => new sfValidatorInteger(),
      'account_name'    => new sfValidatorString(array('max_length' => 100)),
      'sortcode'        => new sfValidatorString(array('max_length' => 40, 'required' => false)),
      'bankname'        => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'type'            => new sfValidatorChoice(array('choices' => array(0 => 'collection', 1 => 'receiving', 2 => 'ewallet'), 'required' => false)),
      'created_at'      => new sfValidatorDateTime(),
      'updated_at'      => new sfValidatorDateTime(),
      'created_by'      => new sfValidatorInteger(array('required' => false)),
      'updated_by'      => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('ep_master_account[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'EpMasterAccount';
  }

}
