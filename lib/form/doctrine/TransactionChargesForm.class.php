<?php

/**
 * TransactionCharges form.
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class TransactionChargesForm extends BaseTransactionChargesForm
{
    public function configure()
    {
        unset(
            $this['split_type'],$this['created_at'],$this['updated_at'],$this['deleted'],$this['created_by'],$this['updated_by']
        );

        $this->setWidgets(array(
      'id'              => new sfWidgetFormInputHidden(),
       'transaction_charges'       => new sfWidgetFormInputText(array(),array('class'=>'txt-input','maxlength'=>5)),
            ));


        $this->setValidators(array(
              'transaction_charges'     => new sfValidatorNumber(array('min'=>0,'max' =>99,'required'=>true),array('invalid'=>'Invalid Transaction Charges','required'=>'Please enter Transaction Charges','min'=>'Transaction Charges should be greater than 0','max'=>'Transaction Charges should be less than or equal to 99')),
            ));
        $this->validatorSchema->setPostValidator(
            new sfValidatorAnd(array(
                    new sfValidatorCallback(array('callback' => array($this, 'verifyTransactionCharge')))

                ))

        );

        $this->widgetSchema->setLabels(array(
      'transaction_charges'=>'Transaction Charges',

            ));

        $this->widgetSchema->setNameFormat('trans_charge[%s]');

    }
    public function verifyTransactionCharge($validator, $values) {
        $err = '';
        $transactionCharge =  $values['transaction_charges'];
        if(strstr($transactionCharge,'.')){
            $tran = explode('.',$transactionCharge);
            $strlen =  strlen($tran[1]);
            if($strlen>=3){
                
                $error = new sfValidatorError($validator,'There should be two digits after decimal point');
                throw new sfValidatorErrorSchema($validator,array('transaction_charges' => $error));
            }

        }
        

        return $values;
    }
}
