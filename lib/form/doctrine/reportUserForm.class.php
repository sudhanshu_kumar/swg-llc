<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of reportUserFormclass
 *
 * @author spandey
 */
class reportUserForm extends BaseUserDetailForm{
    public function configure()
    {

        

        sfProjectConfiguration::getActive()->loadHelpers('Asset');

        $i18n = sfContext::getInstance()->getI18N();
        $user = sfContext::getInstance()->getUser();

        $culture = $user->getCulture();
        if (strlen($culture) > 2) {
            $culture = substr($culture, 0, 2);
        }

        $sfWidgetFormI18nJQueryDateOptions = array(
          'culture' => $culture,
          'image'   => image_path('calendar.png'),
          'config'  => "{ duration: '' }"
        );

       

        $this->setWidgets(array(
//            'username'=>new sfWidgetFormInput(array(),array('class'=>'txt-input')),
            'user_id'=>new sfWidgetFormInput(array(),array('class'=>'txt-input','readonly'=>true)),
            'first_name'=>new sfWidgetFormInput(array(),array('class'=>'txt-input','maxlength'=>32,'min_length'=>2,)),
            'last_name'=>new sfWidgetFormInput(array(),array('class'=>'txt-input','maxlength'=>32,'min_length'=>2,)),
            'dob' => new sfWidgetFormInput(array(),array('maxlength'=>10, 'readonly'=>'true','onfocus'=>'showCalendarControl(report_user_dob)','class'=>'txt-input')),
            'address'=>new sfWidgetFormTextarea(array(),array('class'=>'txt-input','rows'=>3,'cols'=>30)),
            'email'=>new sfWidgetFormInput(array(),array('class'=>'txt-input')),
            'phone'           => new sfWidgetFormInputText(array(),array('class'=>'txt-input','maxlength'=>20,'min_length'=>10))
            ));

        $supportUserFlag = $this->getOption('supportUserFlag');

        if($supportUserFlag == 0){
            $this->setValidators(array(
                'user_id'       => new sfValidatorString(array('required' => false),array ('required'=>'Please enter User ID')),
                'first_name'    => new sfValidatorString(array('max_length' => 32,'min_length'=>2, 'required' => true),array('required'=>'Please enter First Name','max_length'=>'First Name cannot be more than 32 characters','min_length'=>'First Name cannot be less than 2 characters')),
                'last_name'     => new sfValidatorString(array('max_length' => 32,'min_length'=>2, 'required' => true),array('required'=>'Please enter Last Name','max_length'=>'Last Name cannot be more than 32 characters','min_length'=>'Last Name cannot be less than 2 characters')),
                'dob'           => new sfValidatorString(array('required' => false)),
                'email'         => new sfValidatorEmail(array('required' => true),array ('invalid'=>'Invalid E-Mail address','required'=>'Please enter E-Mail address')),
                'address'       => new sfValidatorString(array('required' => true),array('required'=>'Please enter Address')),
                'phone'         => new sfValidatorRegex(array('required'=>true,'pattern'=>'/^[0-9 [\]+()-]{8,20}?$/', 'required' => true),array('required' => 'Please enter Phone Number.','max_length' =>'Phone Number can not  be more than 20 digits.',
                                   'min_length' =>'Phone Number is too short(minimum 6 digits).',
                                   'invalid' =>'Phone Number is invalid'))
            ));
        }else{
            $this->setValidators(array(
                'user_id'        => new sfValidatorString(array('required' => false),array ('required'=>'Please enter User ID')),
                'first_name'        => new sfValidatorString(array('max_length' => 32,'min_length'=>2, 'required' => true),array('required'=>'Please enter First Name','max_length'=>'First Name cannot be more than 32 characters','min_length'=>'First Name cannot be less than 2 characters')),
                'last_name'        => new sfValidatorString(array('max_length' => 32,'min_length'=>2, 'required' => true),array('required'=>'Please enter Last Name','max_length'=>'Last Name cannot be more than 32 characters','min_length'=>'Last Name cannot be less than 2 characters')),
                'dob'        => new sfValidatorString(array('required' => false)),
                'email'             => new sfValidatorEmail(array('required' => true),array ('invalid'=>'Invalid E-Mail address','required'=>'Please enter E-Mail address')),
                'address'        => new sfValidatorString(array('required' => false),array('required'=>'Please enter Address')),
                'phone'         => new sfValidatorRegex(array('required'=>false,'pattern'=>'/^[0-9 [\]+()-]{8,20}?$/', 'required' => false),array('required' => 'Please enter Phone Number.','max_length' =>'Phone Number can not  be more than 20 digits.',
                                   'min_length' =>'Phone Number is too short(minimum 6 digits).',
                                   'invalid' =>'Phone Number is invalid'))
            ));
        }

         $this->validatorSchema->setPostValidator(
            new sfValidatorAnd(array(
//                    new sfValidatorDoctrineUnique(array(
//                                        'model' => 'UserDetail',
//                                        'column' => array('email'),
//                                        'primary_key' => 'id',
//                                        'required' => 'Email cannot be left blank'),
//                        array('invalid'=>'The email address  already exists')),
//                    new sfValidatorDoctrineUnique(array(
//                                        'model' => 'sfGuardUser',
//                                        'column' => array('username'),
//                                        'primary_key' => 'id',
//                                        'required' => 'User name cannot be left blank'),
////                        array('invalid'=>'The username  already exists')),
                   new sfValidatorCallback(array('callback' => array($this, 'checkDob'))),
                   new sfValidatorCallback(array('callback' => array($this, 'checkUniqueEmail'))),
                   new sfValidatorCallback(array('callback' => array($this, 'checkUniquePhone')))

                ))
        );
        $this->widgetSchema->setLabels(array(
           'user_id'=> 'User ID',
           'first_name'=> 'First Name',
           'last_name'=>'Last Name',
           'dob'=>'Date of Birth',
           'address'=>'Address',
           'email'=>'Email',
           'phone'=>'Mobile Number',

            ));
        $this->widgetSchema->setNameFormat('report_user[%s]');
    }
    public function  checkDob($validator, $values) {
        $dob =  strtotime($values['dob']);
      if(!empty($dob) && $dob != ''){
        $todate = strtotime(date('Y-m-d'));           
            if($dob>$todate){
                $error = new sfValidatorError($validator,"Please enter Date of Birth less than today's date");
                throw new sfValidatorErrorSchema($validator,array('dob' => $error));
            }
        }
        return $values;
    }
    public function checkUniqueEmail($validator, $values){ 
        $userId = $values['user_id'];
        $email = $values['email'];
        $data = Doctrine::getTable('userDetail')->checkUniqueEmailWhileUpdating($userId,$email);
        if($data){
            $error = new sfValidatorError($validator,"This email address already exist in the system");
                throw new sfValidatorErrorSchema($validator,array('email' => $error));
        }
        return $values; 
    }
    public function checkUniquePhone($validator,$values){
        $userId = $values['user_id'];
        $phone = $values['phone'];
        if(!empty($phone) && $phone != ''){
        $phone = $values['phone'];
        if($phone[0] != '+'){
                $phone = '+'.$phone;
            }
        $data = Doctrine::getTable('userDetail')->checkUniquePhoneWhileUpdating($userId,$phone);
        if($data){
            $error = new sfValidatorError($validator,"This Phone Number already exist in the system");
                throw new sfValidatorErrorSchema($validator,array('phone' => $error));
         }
        }
        return $values;
    }

}
?>
