<?php

/**
 * EpMenu form base class.
 *
 * @method EpMenu getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseEpMenuForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'          => new sfWidgetFormInputHidden(),
      'name'        => new sfWidgetFormInputText(),
      'label'       => new sfWidgetFormInputText(),
      'parent_id'   => new sfWidgetFormInputText(),
      'sfGuardPerm' => new sfWidgetFormInputText(),
      'url'         => new sfWidgetFormInputText(),
      'eval'        => new sfWidgetFormInputText(),
      'env'         => new sfWidgetFormInputText(),
      'sequence'    => new sfWidgetFormInputText(),
      'created_at'  => new sfWidgetFormDateTime(),
      'updated_at'  => new sfWidgetFormDateTime(),
      'deleted'     => new sfWidgetFormInputCheckbox(),
      'created_by'  => new sfWidgetFormInputText(),
      'updated_by'  => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'          => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'name'        => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'label'       => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'parent_id'   => new sfValidatorInteger(),
      'sfGuardPerm' => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'url'         => new sfValidatorString(array('max_length' => 200, 'required' => false)),
      'eval'        => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'env'         => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'sequence'    => new sfValidatorInteger(),
      'created_at'  => new sfValidatorDateTime(),
      'updated_at'  => new sfValidatorDateTime(),
      'deleted'     => new sfValidatorBoolean(array('required' => false)),
      'created_by'  => new sfValidatorInteger(array('required' => false)),
      'updated_by'  => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('ep_menu[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'EpMenu';
  }

}
