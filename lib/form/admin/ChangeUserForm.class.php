<?php

/**
 * CurrencyMaster form.
 *
 * @package    form
 * @subpackage CurrencyMaster
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class ChangeUserForm extends BasesfGuardUserForm
{
  public static function userVerifyPasswordCallBack($validator, $value, $arguments) {
    // this is my logged in user
    $user = sfContext::getInstance()->getUser();
    // TODO verify if following usage of == is correct
    if ($user == null) {
      throw new sfValidatorError($validator, 'invalid');
    }
    // TODO handle not logged in user properly
    if (!$user->isAuthenticated()) {
      throw new sfValidatorError($validator, 'invalid');
    }
    if (!$user->checkPassword($value)) {
      // password don't match
      throw new sfValidatorError($validator, 'invalid');
    }
    if($value==$_REQUEST['sf_guard_user']['password']){
      throw new sfValidatorError($validator, 'Current Password and New Password can not be same');
      //         throw new sfValidatorError($validator, 'invalid');
    }

    return $value;
  }

  public static function matchNewAndConfirmPasswordCallBack($validator, $value, $arguments){
    if(!preg_match('/[^\s+]/' ,$_REQUEST['sf_guard_user']['password']) ){
      throw new sfValidatorError($validator, 'White spaces are not allowed');
    }
    if(strlen($_REQUEST['sf_guard_user']['password']) < 8){
      throw new sfValidatorError($validator, 'Minimum 8 characters required');
    }
    if(strlen($_REQUEST['sf_guard_user']['password']) > 20){
      throw new sfValidatorError($validator, 'Password can not be more than 20 characters');
    }
    if($_REQUEST['sf_guard_user']['password'] != $_REQUEST['sf_guard_user']['confirm_password']){
      throw new sfValidatorError($validator, 'New Password and Confirm Password do not match');
    }
    return $value;

  }

  public function configure()
  {
    unset(
      $this['algorithm'],$this['salt'],$this['is_super_admin'],
      $this['last_login'],$this['updated_at'],$this['created_at'],
      $this['groups_list'],$this['permissions_list'],
      $this['is_active'],$this['username']
    );

    $this->widgetSchema['old_password'] = new sfWidgetFormInputPassword(array(),array('class'=>'txt-input','title'=>'Current Password','maxlength'=>20, 'autocomplete' => 'off', 'accept' => 'blank'));
    $this->validatorSchema['old_password'] = new sfValidatorCallback(array(
        'callback'  => 'ChangeUserForm::userVerifyPasswordCallBack'));
    $this->validatorSchema['old_password']->setOption('required',true);
    $this->validatorSchema['old_password']->setMessage('required','Current Password is required');
    $this->validatorSchema['old_password']->setMessage('invalid','Current password is incorrect');

       //working with changing the passwords
    $this->widgetSchema['password']= new sfWidgetFormInputPassword(array(),array('class'=>'txt-input','title'=>'New Password','maxlength'=>20, 'autocomplete' => 'off','accept' => 'blank'));
    $this->widgetSchema['confirm_password']= new sfWidgetFormInputPassword(array(),array('class'=>'txt-input','title'=>'Confirm Password','maxlength'=>20, 'autocomplete' => 'off','accept' => 'blank'));
    $this->validatorSchema['password'] = new sfValidatorString(array('max_length' => 20 ,'min_length' => 8),
      array('max_length' => 'Password can not be more than 20 characters',
      'required' => 'Please Enter your new password','min_length' => 'Minimum 8 character required'));

    $this->validatorSchema['password'] = new sfValidatorCallback(array(
        'callback'  => 'ChangeUserForm::matchNewAndConfirmPasswordCallBack'));
    $this->validatorSchema['password']->setOption('required',true);
    $this->validatorSchema['password']->setMessage('required','Password is required');

//
//    $this->validatorSchema['confirm_password'] = new sfValidatorString(array('max_length' => 20 , 'min_length' => 8),
//      array('max_length' => 'Password can not be more than 20 characters',
//      'required' => 'Please Re-enter your new password','min_length' => 'Minimum 8 character required'));


    $this->validatorSchema['confirm_password'] = new sfValidatorString(array('required' => true),
      array('required' => 'Please Re-enter your new password'));


//    $this->validatorSchema->setPostValidator(
//      new sfValidatorSchemaCompare('password', sfValidatorSchemaCompare::EQUAL, 'confirm_password',
//        array(),
//        array('invalid' => 'Password do not match, please try again')
//      ));

      // Set Labels
      $this->widgetSchema->setLabels(  array('old_password' => 'Current Password', 'password' => 'New Password', 'confirm_password'=> 'Confirm Password '));
      $this->getWidgetSchema()->moveField('password', sfWidgetFormSchema::AFTER, 'old_password');
      $this->getWidgetSchema()->moveField('confirm_password', sfWidgetFormSchema::AFTER, 'password');

    $this->widgetSchema->setHelps(array(
            'password'  => ' <br />Password Strength <span id="strength"></span>'
      ));

  }

  public function save($conn = null) {
    sfContext::getInstance()->getUser()->setPassword($this->values['password']);
  }
}
