<?php

/**
 * CurrencyMaster form.
 *
 * @package    form
 * @subpackage CurrencyMaster
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class NewUserForm extends BasesfGuardUserForm
{
  //protected $VendorRegistrationform = null;

  protected function doSave($con = null)
  {
    parent::doSave($con);

//    $this->savecategoryList($con);

  }

  public function savecategoryList($con = null)
  {
    if (!$this->isValid())
    {
      throw $this->getErrorSchema();
    }

    if (!isset($this->embeddedForms['details']->widgetSchema['category_list']))
    {
      // somebody has unset this widget
      return;
    }

    if (is_null($con))
    {
      $con = $this->getConnection();
    }
    $obj = $this->object->UserDetails;
    $existing = $obj->category->getPrimaryKeys();
    $vl = $this->getValue('details');
    $values = $vl['category_list'];  
    if (!is_array($values))
    {
      $values = array();
    }

    $unlink = array_diff($existing, $values);
    if (count($unlink))
    {
      $obj->unlink('category', array_values($unlink));

    }
    $link = array_diff($values, $existing);
    if (count($link))
    {
      $obj->link('category', array_values($link));
    }    
  }

  public function configure()
  {
    // do unsetting
    unset(
      $this['algorithm'],$this['salt'],$this['is_super_admin'],
      $this['last_login'],$this['updated_at'],$this['created_at'],
      $this['groups_list'],$this['permissions_list']
    );

    /*$this->widgetSchema->setLabels(
      array('username'=>'Username *', 'is_active' => 'Desktop Active'));*/

    $this->widgetSchema['password']= new sfWidgetFormInputPassword(array(),array('title'=>'Password','maxlength'=>10, 'autocomplete' => 'off','accept' => 'blank'));
    $this->widgetSchema['confirm_password']= new sfWidgetFormInputPassword(array(),array('title'=>'Confirm Password','maxlength'=>10, 'autocomplete' => 'off','accept' => 'blank'));


   $this->validatorSchema->setPostValidator(
      new sfValidatorSchemaCompare('password', sfValidatorSchemaCompare::EQUAL, 'confirm_password',
        array(),
        array('invalid' => 'Password do not match, please try again')
      ));



    // setup details form now
    $detailsForm = new UserDetailsForm($this->getObject()->getUserDetails());

//     $this->setValidators(array(
//      'first_name'    => new sfValidatorString(array('max_length' => 40)),
//                         new sfValidatorRegex(array('pattern' => '/^[a-zA-Z]*$/')),
//      'last_name'     => new sfValidatorString(array('max_length' => 40)),
//                         new sfValidatorRegex(array('pattern' => '/^[a-zA-Z]*$/')),
//      'email'         => new sfValidatorString(array()),
//                         new sfValidatorEmail(array(), array('invalid' => 'The email address is invalid.'))
//    ));
//
//    $userArr = sfContext::getInstance()->getUser()->getGroupNames();
    $this->widgetSchema->setLabels(
      array('password'=>'Password', 'username'=>'Username', 'confirm_password'=>'Confirm Password', 'is_active' => 'Active'));
    $detailsForm->getWidget('first_name')->setLabel('First Name');
    $detailsForm->getWidget('last_name')->setLabel('Last Name');
    $detailsForm->getWidget('email')->setLabel('Email');
//    if(sfContext::getInstance()->getUser()->getAttribute('is_super_admin')){
        $detailsForm->getWidget('ministry_id')->setLabel('Ministry');
        $detailsForm->getWidget('group_id')->setLabel('Select The Role');
        $detailsForm->getWidget('category_list')->setLabel('Categories');
//    }

    $this->validatorSchema['username'] = new sfValidatorAnd(array(
        new sfValidatorString(array('max_length' => 20),array('required' => 'Username is required.','max_length'=>'Username can not be more than 20 characters.')),
        new sfValidatorRegex(array('pattern' => '/^[a-zA-Z0-9._]*$/'),array('invalid' => 'Invalid Username.','required' => 'Username is required.')),
        new sfValidatorDoctrineUnique(array('model' => 'sfGuardUser', 'column' => array('username')),array('invalid' => 'Username already exist.'))),
      array('halt_on_error' => true),
      array('required' => 'Username is required')
    );

    $this->validatorSchema['password'] = new sfValidatorString(array('max_length' => 20, 'required' => true),array('max_length'=>'Confirm password can not  be more than 20 characters.','required' => 'Password is required'));
    $this->validatorSchema['confirm_password'] = new sfValidatorString(array('max_length' => 20, 'required' => true),array('max_length'=>'Confirm password can not  be more than 20 characters.','required' => 'Confirm password is required.'));
    $this->validatorSchema->setOption('allow_extra_fields', true);

    $this->embedForm('details', $detailsForm);

    //$this->renderGlobalErrors();
  }


  public function configureGroups() {
    $this->uiGroup = new Dlform();
    //$this->uiGroup->setLabel('Portal User');

    $newUserDetails = new FormBlock('New User Details');
    $this->uiGroup->addElement($newUserDetails);

     if(sfContext::getInstance()->getUser()->getAttribute('is_super_admin')){
            $newUserDetails->addElement(array('details:title','details:first_name','details:last_name','details:ministry_id',
//            super admin assign categories created user
            'details:email','username','password','confirm_password','details:group_id','details:category_list','is_active'));
//            'details:email','username','password','confirm_password','details:group_id','is_active'));
    }else {
            $newUserDetails->addElement(array('details:title','details:first_name','details:last_name','details:ministry_id',
            'details:email','username','password','confirm_password','details:group_id','details:category_list','is_active'));
    }

  }
}
