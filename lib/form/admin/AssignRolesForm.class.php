<?php

/**
 * This form is for assigning roles to users.
 *
 * @package    form
 */
class AssignRolesForm extends BasesfGuardUserForm
{

  public function configure()
  {
    unset(
      $this['algorithm'],$this['salt'],$this['is_super_admin'],
      $this['last_login'],$this['updated_at'],$this['created_at'],
      $this['username'],$this['permissions_list'],$this['password'],
      $this['is_active']
    );    
    $this->widgetSchema['username'] = new sfWidgetFormInput(array(), array('readonly' => 'true'));
    $this->validatorSchema['username'] = new sfValidatorString(array('max_length' => 20, 'required' => false));
    if(!sfContext::getInstance()->getUser()->getAttribute('is_super_admin')){
      $qObj = Doctrine_Query::create()
      ->select('*')
      ->from('sfGuardGroup')
      ->where("name != ?",'Super Admin')
      ->andWhere("name != ?",'Admin');
      $this->widgetSchema['groups_list'] = new sfWidgetFormDoctrineChoice(array('model' => 'sfGuardGroup','query'=>$qObj, 'expanded' => false));
    }
    else{
        $this->widgetSchema['groups_list'] = new sfWidgetFormDoctrineChoice(array('model' => 'sfGuardGroup', 'expanded' => false));
    }
    $this->widgetSchema['groups_list']->setLabel('Select The Roles');
  }

  public function configureGroups() {
    $this->uiGroup = new Dlform();

    $newUserDetailsGrp = new FormBlock();
    $this->uiGroup->addElement($newUserDetailsGrp);

    $newUserDetails = new FormBlock();
    $newUserDetailsGrp->addElement($newUserDetails);

    $newUserDetails->addElement(array( 'username' ,'groups_list'));

  }
}