<?php
/**
 * sfWidgetFormDateCal represents a date widget Calander.
 *
 * This widget needs JQuery and JQuery UI to work.
 *
 * @package    symfony
 * @subpackage widget
 * @author     Kamal Somani

 */
class widgetFormDateTimeCal extends widgetFormDate
{
  public function __construct($options = array(), $attributes = array())
  {
    if(!key_exists('format', $options)) {
      $options['format'] = '%day%/%month%/%year%';
    }
    parent::__construct($options, $attributes);
  }

  /**
   * Configures the current widget.
   *
   * Available options:
   *
   *  * image:   The image path to represent the widget (false by default)
   *  * config:  A JavaScript array that configures the JQuery date widget
   *  * culture: The user culture
   *
   * @param array $options     An array of options
   * @param array $attributes  An array of default HTML attributes
   *
   * @see sfWidgetForm
   */
  protected function configure($options = array(), $attributes = array())
  {
    $this->addOption('image', false);
    $this->addOption('config', '{}');
    $this->addOption('culture', '');

    $response = sfContext::getInstance()->getResponse();
    $response->addJavascript('/sf/calendar/calendar.js');
    $response->addJavascript('/sf/calendar/lang/calendar-en.js');
    $response->addJavascript('/sf/calendar/calendar-date-time-setup.js');
    $response->addStylesheet('/sf/calendar/skins/aqua/theme.css');

    parent::configure($options, $attributes);

    if ('en' == $this->getOption('culture'))
    {
      $this->setOption('culture', 'en');
    }
  }

  /**
   * @param  string $name        The element name
   * @param  string $value       The date displayed in this widget
   * @param  array  $attributes  An array of HTML attributes to be merged with the default HTML attributes
   * @param  array  $errors      An array of errors for the field
   *
   * @return string An HTML tag string
   *
   * @see sfWidgetForm
   */
  public function render($name, $value = null, $attributes = array(), $errors = array())
  {
    $prefix = $this->generateId($name);

    $image = '';
    if (false !== $this->getOption('image'))
    {
      $image = sprintf(', buttonImage: "%s", buttonImageOnly: true', $this->getOption('image'));
    }

    return parent::render($name, $value, $attributes, $errors).
           $this->renderTag('input', array('type' => 'hidden', 'size' => 10, 'id' => $prefix.'_date')).
           $this->renderTag('input',array('type'=>'button','id'=> $prefix.'_button','value'=>'...','class'=>'calBtn')).

sprintf(<<<EOF
<script>
function wfd_%s_update_linked(cal)
  {
var p = cal.params;
    m = cal.date.getMonth()+1
    d = cal.date.getDate()
    y = cal.date.getFullYear();
   //console.log(cal);
     jQuery("#%s_year").val(y);
    jQuery("#%s_month").val(m);
    jQuery("#%s_day").val(d);
    if(cal.dateClicked){p.inputField.value = cal.date.print(p.ifFormat);cal.callCloseHandler();}
  }

function wfd_%s_update_from_linked_days(cal)
  {
toDay = new Date();
  //console.log(cal.dateFormat);
   y = $('#%s_year').val();
   m = $('#%s_month').val();
   d = $('#%s_day').val();
   if(y==''){}
   if(m==''){m= toDay.getMonth()+1;}
   if(d==''){d=toDay.getDate();}
   $('#%s_date').val(y+'/'+m+'/'+d);
  //console.log(cal);
  }

 Calendar.setup({
 inputField : "%s_date",
 button : "%s_button",
//onUpdate : wfd_%s_update_from_linked_days,
onSelect : wfd_%s_update_linked

 });
jQuery("#%s_month, #%s_day, #%s_year").change(wfd_%s_update_from_linked_days);
</script>
EOF
      ,$prefix,$prefix,$prefix,$prefix,$prefix,$prefix,$prefix,$prefix,$prefix,$prefix,$prefix,$prefix,$prefix,$prefix,$prefix,$prefix,$prefix

);
}# end of function

public function render2($name, $value = null, $attributes = array(), $errors = array())
  {
    $prefix = $this->generateId($name);

    $image = '';
    if (false !== $this->getOption('image'))
    {
      $image = sprintf(', buttonImage: "%s", buttonImageOnly: true', $this->getOption('image'));
    }

    return parent::render($name, $value, $attributes, $errors).
           $this->renderTag('input', array('type' => 'hidden', 'size' => 10, 'id' => $id = $this->generateId($name).'_jquery_control')).
           sprintf(<<<EOF
<script type="text/javascript">
  function wfd_%s_read_linked()
  {
    jQuery("#%s").val(jQuery("#%s").val() + "-" + jQuery("#%s").val() + "-" + jQuery("#%s").val());

    return {};
  }

  function wfd_%s_update_linked(date)
  {
    jQuery("#%s").val(date.substring(0, 4));
    jQuery("#%s").val(date.substring(5, 7));
    jQuery("#%s").val(date.substring(8));
  }

  function wfd_%s_check_linked_days()
  {
    var daysInMonth = 32 - new Date(jQuery("#%s").val(), jQuery("#%s").val() - 1, 32).getDate();

    if (jQuery("#%s").val() > daysInMonth)
    {
      jQuery("#%s").val(daysInMonth);
    }
  }

  jQuery(document).ready(function() {
    jQuery("#%s").datepicker(jQuery.extend({}, {
      yearRange:  '1900:2050',
      minDate:    new Date(%s, 1 - 1, 1),
      maxDate:    new Date(%s, 12 - 1, 31),
      beforeShow: wfd_%s_read_linked,
      onSelect:   wfd_%s_update_linked,
      showOn:     "button"
      %s
    }, jQuery.datepicker.regional["%s"], %s, {dateFormat: "yy-mm-dd"}));
  });

  jQuery("#%s, #%s, #%s").change(wfd_%s_check_linked_days);
</script>
EOF
      ,
      $prefix, $id,
      $this->generateId($name.'[year]'), $this->generateId($name.'[month]'), $this->generateId($name.'[day]'),
      $prefix,
      $this->generateId($name.'[year]'), $this->generateId($name.'[month]'), $this->generateId($name.'[day]'),
      $prefix,
      $this->generateId($name.'[year]'), $this->generateId($name.'[month]'),
      $this->generateId($name.'[day]'), $this->generateId($name.'[day]'),
      ($this->getOption('can_be_empty') ? 'daysInMonth' : 'daysInMonth - 1'),
      $this->generateId($name.'[day]'), $this->generateId($name.'[day]'),
      $id,
      min($this->getOption('years')), max($this->getOption('years')),
      $prefix, $prefix, $image, $this->getOption('culture'), $this->getOption('config'),
      $this->generateId($name.'[day]'), $this->generateId($name.'[month]'), $this->generateId($name.'[year]'),
      $prefix
    );
  }
  /**
   * Gets the stylesheet paths associated with the widget.
   *
   * The array keys are files and values are the media names (separated by a ,):
   *
   *   array('/path/to/file.css' => 'all', '/another/file.css' => 'screen,print')
   *
   * @return array An array of stylesheet paths
   */
  public function getStylesheets()
  {
    //$css = '/js/lib/ui.datepicker.css';
    //use_stylesheet('/sf/calendar/skins/aqua/theme.css');//$css = '/js/lib/ui.datepicker.css';
    //$response = sfContext::getInstance()->getResponse();
    //return (!array_key_exists($css,$response->getStylesheets()))? array($css): array();
    return array();
  }

  /**
   * Gets the JavaScript paths associated with the widget.
   *
   * @return array An array of JavaScript paths
   */
  public function getJavaScripts()
  {
    $js= 'lib/ui.datepicker.js';

    $js= '';
//    $response = sfContext::getInstance()->getResponse();
//    $response->addJavascript('/sf/calendar/calendar.js');
//    $response->addJavascript('/sf/calendar/lang/calendar-en.js');
//    $response->addJavascript('/sf/calendar/calendar-setup.js');
    //use_javascript($js);
//    use_javascript('/sf/calendar/calendar.js');
//    use_javascript('/sf/calendar/lang/calendar-en.js');
//    use_javascript('/sf/calendar/calendar-setup.js');

    //echo"<pre>";print_r($response->getJavascripts());
    //return (!array_key_exists($js,$response->getJavascripts()))? array($js) :array();
    return array();
  }
}
