<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class SupportRequestManager {

    public function setSupportRequest($concatinatedMsg1, $concatinatedMsg2, $concatinatedMsg3) {

        $supportRequestListId = $this->supportRequestList($concatinatedMsg1);
        $id = $this->supportRequestComments($concatinatedMsg2, $supportRequestListId);
        $this->supportRequestApplicationDetails($concatinatedMsg3, $supportRequestListId);
        return $id;
    }

    function supportRequestList($concatinatedMsg1) {
        $obj = new SupportRequestList();
        $obj->setOrderNumber($concatinatedMsg1['Order Number']);
        $obj->setUserId($concatinatedMsg1['User Id']);
        $obj->setSupportCategory($concatinatedMsg1['Support Category']);
        if (isset($concatinatedMsg1['Sub Support Category']) && $concatinatedMsg1['Sub Support Category'] != '') {
            $obj->setSubSupportCategory($concatinatedMsg1['Sub Support Category']);
        } else {
            $obj->setSubSupportCategory(NULL);
        }
        $obj->setStatus('Pending');
        $obj->save();
        $supportRequestListId = $obj->getIncremented();
        return $supportRequestListId;
    }

    function supportRequestComments($concatinatedMsg2, $supportRequestListId) {
        $objComment = new SupportRequestComments();
        $objComment->setRequestId($supportRequestListId);
        $objComment->setCustomerComments($concatinatedMsg2['Message']);
        $objComment->setUploadDocument1($concatinatedMsg2['filename']);
        $objComment->setUserId($concatinatedMsg2['User Id']);
        $objComment->save();

        return $objComment->getId();;
    }

    function supportRequestApplicationDetails($concatinatedMsg3, $supportRequestListId) {
        $duplicate = '';

        if (isset($concatinatedMsg3['chk_fee']) && $concatinatedMsg3['chk_fee'] != '')
            $appDetail = $concatinatedMsg3['chk_fee'];
        foreach ($appDetail as $key => $val) {
            $objDetail = new SupportRequestApplicationDetails();
            $objDetail->setRequestId($supportRequestListId);
            $exp = explode('_', $val);
            $objDetail->setAppId($exp[0]);
            $objDetail->setAppType($exp[1]);
            if ((isset($concatinatedMsg3['AppIdType']) && $concatinatedMsg3['AppIdType'] != '') || (isset($concatinatedMsg3['Duplicate Order Number']) && $concatinatedMsg3['Duplicate Order Number'] != '')) {
                $objDetail->setRequestFor(trim($concatinatedMsg3['AppIdType']). "\n" .trim($concatinatedMsg3['Duplicate Order Number']));
            }
            if (isset($concatinatedMsg3['name']) && $concatinatedMsg3['name'] != '') {
              $objDetail->setRequestFor(trim($concatinatedMsg3['name']));
            }
            if (isset($concatinatedMsg3['dob']) && $concatinatedMsg3['dob'] != '') {
                $objDetail->setRequestFor(trim($concatinatedMsg3['dob']));
            }
            if (isset($concatinatedMsg3['office']) && $concatinatedMsg3['office'] != '') {
                $objDetail->setRequestFor(trim($concatinatedMsg3['office']));
            }
            $objDetail->save();
        }
        return;
    }

    /*  START  // Functions by Kuldeep */

    public function processReuqestData($arrReuqestDetails) {
//      echo '<pre>';print_r($arrReuqestDetails);die;
        $reuqestDetailsArr = array();
        $arrRequestComments = array();
        //request Details
        $reuqestDetailsArr['first_name'] = $arrReuqestDetails[0]['sfGuardUser']['UserDetail']['first_name'];
        $reuqestDetailsArr['last_name'] = $arrReuqestDetails[0]['sfGuardUser']['UserDetail']['last_name'];
        $reuqestDetailsArr['support_category'] = $arrReuqestDetails[0]['support_category'];
        $reuqestDetailsArr['sub_support_category'] = $arrReuqestDetails[0]['sub_support_category'];
        $reuqestDetailsArr['order_number'] = $arrReuqestDetails[0]['order_number'];
        $reuqestDetailsArr['app_detail'] = $arrReuqestDetails[0]['SupportRequestApplicationDetails'];
        //comments details for request
        $i = 1;
        foreach ($arrReuqestDetails[0]['SupportRequestComments'] as $key => $val) {
            $arrRequestComments[$key]['upload_document'] ='';
            $arrRequestComments[$key]['customer_comments'] = $val['customer_comments'];
            $arrRequestComments[$key]['support_user_comments'] = $val['support_user_comments'];
            if(!empty ($val['created_at'] )){
            $arrRequestComments[$key]['created_at'] = $val['created_at'];
            }
            if(!empty ($val['updated_at'] )){
            $arrRequestComments[$key]['updated_at'] = $val['updated_at'];
             }
            //upload doc path
            if(!empty ($val['upload_document1'] )){
              $arrRequestComments[$key]['upload_document'] .= $val['upload_document1']."####";
            }
            if(!empty ($val['upload_document2'])){
              $arrRequestComments[$key]['upload_document'] .= $val['upload_document2']."####";
            }
            if(!empty($val['upload_document3'])){
              $arrRequestComments[$key]['upload_document'] .= $val['upload_document3']."####";
            }
            if(!empty($val['upload_document4'])){
              $arrRequestComments[$key]['upload_document'] .= $val['upload_document4']."####";
            }
         }
//         echo '<pre>';print_r($arrRequestComments);die;
       return array('reuqestDetailsArr' => $reuqestDetailsArr, 'arrRequestComments' => $arrRequestComments);
    }

    public function saveRequestAssignment($userId, $requestId) {
        $obj = new SupportRequestAssignment();
        $obj->setRequestId($requestId);
        $obj->setAssignedTo($userId);
        $obj->setAssignActive('Yes');
        $obj->save();
    }

    public function getAllSupportUser($loggedInUserName) {
        $arrUserDetails = Doctrine::getTable('sfGuardUser')->getAllSupportUser('6');
        foreach ($arrUserDetails as $val) {
          if($val['username'] != $loggedInUserName){
            $arrUsers[$val['id']] = $val['username'];
          }

        }
        return $arrUsers;
    }

    public function setComments($param,$userId) {

        //get latest comments by request id
        $latestComments = Doctrine::getTable('supportRequestComments')->getUpdatedComments($param['hdn_requestId']);

        if ($latestComments['0']['support_user_comments']) {
            $obj = new SupportRequestComments();
            $obj->setRequestId($param['hdn_requestId']);
            if($param['txt_support_close_cmt'])
            {
            $obj->setSupportUserComments($param['txt_support_close_cmt']);
            }
            if($param['txt_support_assign_cmt']){
            $obj->setSupportUserComments($param['txt_support_assign_cmt']);
            }
            if($param['txt_support_respond_cmt']){
            $obj->setSupportUserComments($param['txt_support_respond_cmt']);
            }
            $obj->setUserId($userId);
            $obj->save();

        } else {
            $setSupportUserComment = Doctrine::getTable('supportRequestComments')->setSupportUserComments($param, $latestComments['0']['id']);

        }
        if ($param['mode'] == 'assign') {
            $this->setSupportRequestAssignment($param);
        }
        return;
    }

    public function setSupportRequestAssignment($param){
        //check if request already assigned
        $isAlreadyAssigned = SupportRequestAssignmentTable::getInstance()->isRequestAlreadyAssigned($param['hdn_requestId']);

        if(count($isAlreadyAssigned) > 0){
          //update previous assignment
          $updateAssigmemnt = SupportRequestAssignmentTable::getInstance()->updatePreviousAssignment($isAlreadyAssigned[0]['id']);

          $objAssign = new SupportRequestAssignment();
          $objAssign->setRequestId($param['hdn_requestId']);
          $objAssign->setAssignedTo($param['slt_user']);
          $objAssign->setAssignedFrom($isAlreadyAssigned[0]['id']);
          $objAssign->setAssignActive('Yes');
          $objAssign->save();
        }
    }

    /*  END  // Functions by Kuldeep */
    /* Start Function by Amit     */
    public function setCustomerComment($custComment,$requestId,$userId){

          try{

          $supReqCommentObj = new SupportRequestComments();
          $supReqCommentObj->setCustomerComments($custComment);
          $supReqCommentObj->setRequestId($requestId);
          $supReqCommentObj->setUserId($userId);
          $supReqCommentObj->save();
          return true;
          } catch(Exception $e){
              echo "Problem Found :".$e->getMessage();
          }

      }
    /* End Amit*/

   public function setMultipleComment($customerComment,$requestId,$userId,$finalImageNameArr){
       try {
         
            $supReqCommentObj = new SupportRequestComments();
            $supReqCommentObj->setCustomerComments($customerComment);
            $supReqCommentObj->setRequestId($requestId);
            $supReqCommentObj->setUserId($userId);
            if(is_array($finalImageNameArr) && !empty($finalImageNameArr[0])){
            $supReqCommentObj->setUploadDocument2($finalImageNameArr[0]);
            }
            if(is_array($finalImageNameArr) && !empty($finalImageNameArr[1])){
            $supReqCommentObj->setUploadDocument3($finalImageNameArr[1]);
            }
            if(is_array($finalImageNameArr) && !empty($finalImageNameArr[2])){
            $supReqCommentObj->setUploadDocument4($finalImageNameArr[2]);
            }
            $supReqCommentObj->save();

       }catch(Exception $e){
              echo "Problem Found :".$e->getMessage();
          }

   }
}

?>
