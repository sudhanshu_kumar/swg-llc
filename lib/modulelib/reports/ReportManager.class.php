<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class ReportManager
{
     /**
     * Method to generate payment transactions graph image
     * @param Array $result
     * @return String $file
     */
    public static function setPaymentTransactionsGraph($result,$post)
    {
        if($result instanceof Doctrine_Collection){
            //Do no
        }else{
            return false;
        }

        $data = array();
        
        $i = 0;
        if($post['paymentTransaction']['startDate'] !=  $post['paymentTransaction']['endDate']){
            foreach($result->getIterator() as $row)
            {
                $payment_date = date('Y-m-d',strtotime($row->getUpdatedAt()));
                $graph_name = "No. Of Transactions";
                $data[$graph_name][$payment_date] = $row->getTotalTransactions(); //echo 'c<br>';
            }
            $xAxisLabel = "Payment Date";
        }else{
            
            $currentTime = '00'.':'.'00';
            $endTime = '23'.':'.'59';
            $bits = explode( ':', $currentTime );
            $currentTime = $bits[0] * 60 + $bits[1];

            $bits = explode( ':', $endTime );
            $endTime = $bits[0] * 60 + $bits[1];

            $interval = $post['paymentTransaction']['duration']; // minutes
            if($interval == 6){$interval = $interval * 60;}
            for( $i = $currentTime; $i <= $endTime; $i += $interval )
            {
                $j = $i+$interval;
                $time = floor( $i / 60 ).':'.ReportManager::zeroPadding(($i % 60));
                $graph_name = "No. Of Transactions";
                $data[$graph_name][$time]=0;
            }
        
            foreach($result->getIterator() as $row){
                $payment_time = date('H-i',strtotime($row->getUpdatedAt()));               
                $userTime = str_replace("-",":",$payment_time);;
                $date_from_user = date('Y-m-d',strtotime($row->getUpdatedAt())).' '.$userTime;                
                
                for( $i = $currentTime; $i <= $endTime; $i += $interval )
                {
                    $j = $i+$interval;
                    $time=floor( $i / 60 ).':'.ReportManager::zeroPadding(($i % 60));
                    $start_date = date('Y-m-d',strtotime($row->getUpdatedAt())).' '.$time;
                    $end_date = date('Y-m-d',strtotime($row->getUpdatedAt())).' '.floor( $j / 60 ).':'.ReportManager::zeroPadding(($j % 60));
                    $start_ts = strtotime($start_date);
                    $end_ts = strtotime($end_date);
                    $user_ts = strtotime($date_from_user);
                    
                    if(($user_ts >= $start_ts) && ($user_ts <= $end_ts)){
                        $payment_interval = floor( $j / 60 ).':'.ReportManager::zeroPadding(($j % 60));
                        $data[$graph_name][$payment_interval] = $data[$graph_name][$payment_interval]+1;
                     }
                }
            }
            $xAxisLabel = "Payment Time";
        }
   //     exit;
       // print_r($data[$graph_name][$payment_interval] ); exit;

        $graph = new ezcGraphLineChart();
        $graph->driver = new ezcGraphGdDriver();
        $graph->palette = new ezcGraphPaletteBlack();
        //$graph->xAxis  = new ezcGraphChartElementDateAxis();
        $graph->xAxis->label = $xAxisLabel;
        $graph->yAxis->label = "No. Of Transactions";
        //$graph->options->fillLines = 210;
        $graph->title = 'Transactions Over Time';
        $graph->legend = true;
        $graph->legend->portraitSize  = .09;
        $graph->xAxis->axisLabelRenderer->showZeroValue=true;
        // Add data
        foreach($data as $key => $arr)
        {
            $graph->data[$key] = new ezcGraphArrayDataSet($arr);
            $graph->data[$key]->symbol = ezcGraph::BULLET;
        }

        $graph->renderer->options->barPadding = 0.5;

        $graph->options->font = 'fonts/tutorial_font.ttf';
        $graph->driver->options->jpegQuality = 100;
        $graph->driver->options->imageFormat = IMG_JPEG;

        $file = time().rand(1000, 9999).'.jpg';

        $chartsPath = sfConfig::get('sf_upload_dir')."/charts/";
        $old = umask(0);
        if(!is_dir($chartsPath)){
            if(!mkdir($chartsPath, 0777, true)){
                echo 'Unable to create folder';
                return 0;
            }
        }
        umask($old);
        $path = $chartsPath.$file;

        @chmod($path, 0777);

        $graph->render(870, 400, $path);

        $path = "/".str_replace($_SERVER['DOCUMENT_ROOT'], '', $chartsPath.$file);

        return $file;
    }

    /**
     * This function forms a query for payment transactions graph
     * @param Array $post
     * @return Doctrine_Manager $query
     */
    public static function getPaymentTransactionsGraphQuery($post)
    {
        if(!is_array($post) || !count($post))
        {
            return false;
        }

        $query = Doctrine::getTable('ipay4meOrder')
        ->createQuery('i')
        ->where('i.payment_status=?','0');


        if($post['paymentTransaction']['card_type'])
        {
            $query->andWhere('i.card_type = ?', $post['paymentTransaction']['card_type']);
        }

        if($post['paymentTransaction']['startDate'])
        {
            $from = $post['paymentTransaction']['startDate'].' 00:00:00';
            $query->andWhere("i.updated_at >= ?", $from);
        }

        if($post['paymentTransaction']['endDate'])
        {
            $to = $post['paymentTransaction']['endDate'].' 23:59:59';
            $query->andWhere("i.updated_at <= ?", $to);
        }
        if($post['paymentTransaction']['startDate'] !=  $post['paymentTransaction']['endDate']){
            $query->groupBy("date(i.updated_at)");
            $query->select("sum(i.payment_status) as total_transactions,i.id, i.updated_at");
        }else{
            $query->select("i.id, i.updated_at");
        }
        //echo $query->getSqlQuery();die;
        return $query;
    }

    public static function zeroPadding($n)
    {
        if ($n<10){ return "0".$n;}else{return "".$n;};
    }

}
?>
