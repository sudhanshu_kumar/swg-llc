<?php
class orderV1Manager implements orderService {
   public function getReceipt($receiptId){
//       $detail = Doctrine::getTable('Ipay4meOrder')->getReceipt($receiptId);
       $detail = Doctrine::getTable('Ipay4meOrder')->getReceipt($receiptId);
       return $detail;
 }

   public function getPurchaseHistory($userId){
//       $detail = Doctrine::getTable('Ipay4meOrder')->getPurchaseHistory($userId);
       $detail = Doctrine::getTable('Ipay4meOrder')->getPurchaseHistory($userId);
       return $detail;
   }

   public function getCardDetail($order_number){
//        $detail = Doctrine::getTable('EpPayEasyRequest')->getCardDetail($cusId);
        $detail = Doctrine::getTable('EpGrayPayRequest')->getCardDetail($order_number);
        return $detail;
       
   }

   public function getOrderNumber($type=''){ //function to be moved in a common library
     do{
      if('recharge' == $type)
        { $orderNumber = '5'.time().mt_rand(1000,9999);
          $modelName = 'ipay4meRechargeOrder';
        }
      else
        { $orderNumber = time().mt_rand(10000,99999);
          $modelName = 'ipay4meOrder';
        }
        
      $isExist = $this->checkDuplicacy($orderNumber,$modelName);
    }while($isExist > 0);
    return $orderNumber;
  }


  protected function checkDuplicacy($cusId,$modelName) {
    return $isExist = Doctrine::getTable($modelName)->checkDuplicacy($cusId);
  }

}
