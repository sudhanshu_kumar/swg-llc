<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of paymentModeManagerclass
 *
 * @author akumar1
 */
class PaymentModeManager
{

  public function getCountryCardType($arrCountry, $amount){

      $isPrivilegeUser = sfContext::getInstance()->getUser()->isPrivilegeUser();
      
      $optionsArr = array();
      $executeAll = true;
      for($i=0;$i<count($arrCountry);$i++){
          $paymentOptions = Doctrine::getTable('CountryPaymentMode')->getPaymentOptions($arrCountry[$i]);
          if($paymentOptions)
          {
              $optionsArr[] = $paymentOptions;
          }
          elseif($executeAll){
              $allOption = Doctrine::getTable('CountryPaymentMode')->getPaymentOptions('ALL');
              if($allOption)
              {
                  $optionsArr[] = $allOption;
                  $executeAll = false;
              }
          }
      }
      
    $arrPaymentOptions = array();
    $splitArr = array();
    switch(count($optionsArr))
    {
      case 0:

        break;
      case 1:
        $arrPaymentOptions = explode(',', $optionsArr[0]);
        break;
      default:
        $common = explode(',', $optionsArr[0]);
        for($i = 1; $i < count($optionsArr); $i++)
        {
          $splitArr = explode(',', $optionsArr[$i]);
          $common = array_intersect($common, $splitArr);
        }
        $arrPaymentOptions = $common;
        break;
    }    

    if(!empty($arrPaymentOptions)){
      $arrGroup = Doctrine::getTable('PaymentMode')->getCardGroup($arrPaymentOptions);
    }    


    $k=0;$j=0;
    $arrFinalArray= array();
    for($i=0;$i<count($arrGroup);$i++){
      $arrFinalArray[$arrGroup[$i]['group_id']][$k]['gateway_id'] = $arrGroup[$i]['gateway_id'];
      $arrFinalArray[$arrGroup[$i]['group_id']][$k]['card_type'] = $arrGroup[$i]['card_type'];
      $arrFinalArray[$arrGroup[$i]['group_id']][$k]['display_name'] = $arrGroup[$i]['display_name'];
      $k++;
    }
    $j=0;
    $returnCardType= array();
    foreach($arrFinalArray as $key=>$values){
      $returnCardType[$j]['group_id'] = $key;
      $i=0;
      foreach($values as $activity){
        $returnCardType[$j]['card_detail'][$i]['gateway_id'] = $activity['gateway_id'];
        $returnCardType[$j]['card_detail'][$i]['card_type'] = $activity['card_type'];
        $returnCardType[$j]['card_detail'][$i]['display_name'] = $activity['display_name'];
        $i++;
      }
      $j++;
    }

    if(!empty($returnCardType))
    {
        if($isPrivilegeUser){
            $returnCardType = $returnCardType;
        }else{            
             $returnCardType = $this->createArray($returnCardType,$amount);
        }
       
    }    
    return $returnCardType;

  }

  private function createArray($returnCardType,$amount)
  {
      $arrTemp = array();
      if(isset($returnCardType))
      {
          for($i=0;$i<count($returnCardType);$i++)
          {
              if(isset($returnCardType[$i]['card_detail']))
              {
//                  if(count($returnCardType[$i]['card_detail'])>2)
//                  {
                      $k = 0;
                      for($j=0;$j<count($returnCardType[$i]['card_detail']);$j++)
                      { 
                          if($this->isOverCartThreshold($amount,$returnCardType[$i]['card_detail'][$j]['gateway_id']))
                          {
                              if($returnCardType[$i]['card_detail'][$j]['card_type'] == 'V' || $returnCardType[$i]['card_detail'][$j]['card_type'] == 'nmi_vbv' || $returnCardType[$i]['card_detail'][$j]['card_type'] == 'M' || $returnCardType[$i]['card_detail'][$j]['card_type'] == 'nmi_mcs')
                              { 
                                  //cart no added
                              }
                              else
                              {
                                  if($this->reachedGpVisaThreshold($amount,$returnCardType[$i]['card_detail'][$j]['gateway_id']))
                                  { 
                                      if($returnCardType[$i]['card_detail'][$j]['card_type'] == 'V' || $returnCardType[$i]['card_detail'][$j]['card_type'] == 'nmi_vbv' || $returnCardType[$i]['card_detail'][$j]['card_type'] == 'jna_nmi_vbv')
                                      {
                                          //visa no add
                                      }
                                      else
                                      {
                                          if($this->reachedGpMasterThreshold($amount,$returnCardType[$i]['card_detail'][$j]['gateway_id']))
                                          {
                                              /**
                                               * [WP: 111] => CR: 157
                                               * Added condition of MasterCard SecureCode for threshold...
                                               */
                                              if($returnCardType[$i]['card_detail'][$j]['card_type'] == 'M' || $returnCardType[$i]['card_detail'][$j]['card_type'] == 'nmi_mcs' || $returnCardType[$i]['card_detail'][$j]['card_type'] == 'jna_nmi_mcs')
                                              {

                                              }
                                              else
                                              {
                                                  $arrTemp[$i]['group_id'] = $returnCardType[$i]['group_id'];
                                                  $arrTemp[$i]['card_detail'][$k]['gateway_id'] = $returnCardType[$i]['card_detail'][$j]['gateway_id'];
                                                  $arrTemp[$i]['card_detail'][$k]['card_type'] = $returnCardType[$i]['card_detail'][$j]['card_type'];
                                                  $arrTemp[$i]['card_detail'][$k]['display_name'] = $returnCardType[$i]['card_detail'][$j]['display_name'];
                                                  $k++;
                                              }
                                          }
                                          else
                                          {
                                              $arrTemp[$i]['group_id'] = $returnCardType[$i]['group_id'];
                                              $arrTemp[$i]['card_detail'][$k]['gateway_id'] = $returnCardType[$i]['card_detail'][$j]['gateway_id'];
                                              $arrTemp[$i]['card_detail'][$k]['card_type'] = $returnCardType[$i]['card_detail'][$j]['card_type'];
                                              $arrTemp[$i]['card_detail'][$k]['display_name'] = $returnCardType[$i]['card_detail'][$j]['display_name'];$k++;
                                          }

                                      }
                                  }
                                  else
                                  {
                                      if($this->reachedGpMasterThreshold($amount,$returnCardType[$i]['card_detail'][$j]['gateway_id']))
                                      {
                                          /**
                                           * [WP: 111] => CR: 157
                                           * Added condition of MasterCard SecureCode for threshold...
                                           */
                                          if($returnCardType[$i]['card_detail'][$j]['card_type'] == 'M' || $returnCardType[$i]['card_detail'][$j]['card_type'] == 'nmi_mcs' || $returnCardType[$i]['card_detail'][$j]['card_type'] == 'jna_nmi_mcs')
                                          {
                                            //master not add
                                          }
                                          else
                                          {
                                              $arrTemp[$i]['group_id'] = $returnCardType[$i]['group_id'];
                                              $arrTemp[$i]['card_detail'][$k]['gateway_id'] = $returnCardType[$i]['card_detail'][$j]['gateway_id'];
                                              $arrTemp[$i]['card_detail'][$k]['card_type'] = $returnCardType[$i]['card_detail'][$j]['card_type'];
                                              $arrTemp[$i]['card_detail'][$k]['display_name'] = $returnCardType[$i]['card_detail'][$j]['display_name'];$k++;
                                          }
                                      }
                                      else
                                      {
                                          $arrTemp[$i]['group_id'] = $returnCardType[$i]['group_id'];
                                          $arrTemp[$i]['card_detail'][$k]['gateway_id'] = $returnCardType[$i]['card_detail'][$j]['gateway_id'];
                                          $arrTemp[$i]['card_detail'][$k]['card_type'] = $returnCardType[$i]['card_detail'][$j]['card_type'];
                                          $arrTemp[$i]['card_detail'][$k]['display_name'] = $returnCardType[$i]['card_detail'][$j]['display_name'];$k++;
                                      }
                                  }
                              }
                          }
                          else
                          {
                              if($this->reachedGpVisaThreshold($amount,$returnCardType[$i]['card_detail'][$j]['gateway_id']))
                              {
                                  if($returnCardType[$i]['card_detail'][$j]['card_type'] == 'V' || $returnCardType[$i]['card_detail'][$j]['card_type'] == 'nmi_vbv' || $returnCardType[$i]['card_detail'][$j]['card_type'] == 'jna_nmi_vbv')
                                  {
                                      //visa no add
                                  }
                                  else
                                  {
                                      /**
                                       * [WP: 111] => CR: 157
                                       * Added condition of MasterCard SecureCode for threshold...
                                       */
                                      if($this->reachedGpMasterThreshold($amount,$returnCardType[$i]['card_detail'][$j]['gateway_id']))
                                      {
                                          if($returnCardType[$i]['card_detail'][$j]['card_type'] == 'M' || $returnCardType[$i]['card_detail'][$j]['card_type'] == 'nmi_mcs' || $returnCardType[$i]['card_detail'][$j]['card_type'] == 'jna_nmi_mcs')
                                          {

                                          }
                                          else
                                          {
                                              $arrTemp[$i]['group_id'] = $returnCardType[$i]['group_id'];
                                              $arrTemp[$i]['card_detail'][$k]['gateway_id'] = $returnCardType[$i]['card_detail'][$j]['gateway_id'];
                                              $arrTemp[$i]['card_detail'][$k]['card_type'] = $returnCardType[$i]['card_detail'][$j]['card_type'];
                                              $arrTemp[$i]['card_detail'][$k]['display_name'] = $returnCardType[$i]['card_detail'][$j]['display_name'];$k++;
                                          }
                                      }
                                      else
                                      {
                                          $arrTemp[$i]['group_id'] = $returnCardType[$i]['group_id'];
                                          $arrTemp[$i]['card_detail'][$k]['gateway_id'] = $returnCardType[$i]['card_detail'][$j]['gateway_id'];
                                          $arrTemp[$i]['card_detail'][$k]['card_type'] = $returnCardType[$i]['card_detail'][$j]['card_type'];
                                          $arrTemp[$i]['card_detail'][$k]['display_name'] = $returnCardType[$i]['card_detail'][$j]['display_name'];$k++;
                                      }

                                  }
                              }
                              else
                              {
                                  if($this->reachedGpMasterThreshold($amount,$returnCardType[$i]['card_detail'][$j]['gateway_id']))
                                  {
                                      /**
                                       * [WP: 111] => CR: 157
                                       * Added condition of MasterCard SecureCode for threshold...
                                       */
                                      if($returnCardType[$i]['card_detail'][$j]['card_type'] == 'M' || $returnCardType[$i]['card_detail'][$j]['card_type'] == 'nmi_mcs' || $returnCardType[$i]['card_detail'][$j]['card_type'] == 'jna_nmi_mcs')
                                      {
                                        //master not add
                                      }
                                      else
                                      {
                                          $arrTemp[$i]['group_id'] = $returnCardType[$i]['group_id'];
                                          $arrTemp[$i]['card_detail'][$k]['gateway_id'] = $returnCardType[$i]['card_detail'][$j]['gateway_id'];
                                          $arrTemp[$i]['card_detail'][$k]['card_type'] = $returnCardType[$i]['card_detail'][$j]['card_type'];
                                          $arrTemp[$i]['card_detail'][$k]['display_name'] = $returnCardType[$i]['card_detail'][$j]['display_name'];$k++;
                                      }
                                  }
                                  else
                                  {
                                      $arrTemp[$i]['group_id'] = $returnCardType[$i]['group_id'];
                                      $arrTemp[$i]['card_detail'][$k]['gateway_id'] = $returnCardType[$i]['card_detail'][$j]['gateway_id'];
                                      $arrTemp[$i]['card_detail'][$k]['card_type'] = $returnCardType[$i]['card_detail'][$j]['card_type'];
                                      $arrTemp[$i]['card_detail'][$k]['display_name'] = $returnCardType[$i]['card_detail'][$j]['display_name'];$k++;
                                  }
                              }
                          }


                      }
//                  }
              }
          }
      }

      return $arrTemp;
  }

  function reachedGpVisaThreshold($amount,$gatewayId){
    
    $midDetails = $this->getActiveGatewayId($gatewayId);
    $activeMIdService = strtoupper($midDetails['midService']);
    $activeGatewayId = strtoupper($midDetails['gateway_id']);
    //get Mid based cart limit
    $visaCardLimit = Doctrine::getTable('MidMaster')->findByMidService($activeMIdService);    
    $visaLimit = $visaCardLimit->getFirst()->getVisaAmountLimit();
    
    $vaultHelper = new VaultHelper() ;
    $thresholdDateTime = $vaultHelper->getDateTime() ;
    $cardType = 'V' ;
    $processedAmount = Doctrine::getTable('ipay4meOrder')->getDailyProcessedAmount($cardType, $thresholdDateTime,$activeGatewayId);

    if($visaLimit < ($processedAmount + $amount)){
      return true ;
    }
    return false ;
  }

  function reachedGpMasterThreshold($amount,$gatewayId){
    
    $midDetails = $this->getActiveGatewayId($gatewayId);    
    $activeMIdService = strtoupper($midDetails['midService']);
    $activeGatewayId = strtoupper($midDetails['gateway_id']);
    //get Mid based cart limit
    $masterCardLimit = Doctrine::getTable('MidMaster')->findByMidService($activeMIdService);
    $masterLimit = $masterCardLimit->getFirst()->getMasterAmountLimit();
    
    $vaultHelper = new VaultHelper() ;
    $thresholdDateTime = $vaultHelper->getDateTime() ;
    $cardType = 'M' ;
    $processedAmount = Doctrine::getTable('ipay4meOrder')->getDailyProcessedAmount($cardType, $thresholdDateTime,$activeGatewayId);

    //$GpMasterThreshold = sfConfig::get('app_threshold_master_limit');
    if($masterLimit < ($processedAmount + $amount)){
      return true ;
    }
    return false ;
  }

  function isOverCartThreshold($amount,$gatewayId){
    //get mid service for country
    $midDetails = $this->getActiveGatewayId($gatewayId);
    $activeMIdService = strtoupper($midDetails['midService']);
    //get Mid based cart limit
    $cartLimit = Doctrine::getTable('MidMaster')->findByMidService($activeMIdService);
    $CartThreshold = $cartLimit->getFirst()->getCartAmountLimit();
    //$CartThreshold = $cartLimit;//sfConfig::get('app_threshold_cart_amount');
    if($CartThreshold < $amount){
      return true ;
    }
    return false ;


  }

  public function array_remove_keys($array, $keys = array()) {

    // If array is empty or not an array at all, don't bother
    // doing anything else.
    if(empty($array) || (! is_array($array))) {
      return $array;
    }

    // If $keys is a comma-separated list, convert to an array.
    if(is_string($keys)) {
      $keys = explode(',', $keys);
    }

    // At this point if $keys is not an array, we can't do anything with it.
    if(! is_array($keys)) {
      return $array;
    }

    // array_diff_key() expected an associative array.
    $assocKeys = array();
    foreach($keys as $key) {
      $assocKeys[$key] = true;
    }

    return array_diff_key($array, $assocKeys);
  }


  public function getCountryCardTypeBackup($arrCountry, $amount){

    $arrCountry = array('0'=>'US','1'=>'IN','2'=>'GB');
    for($i=0;$i<count($arrCountry);$i++){
      $arrCardType = Doctrine::getTable('CountryPaymentMode')->getCardType($arrCountry[$i]);
      if(!empty($arrCardType)){
        break;
      }else{
        $arrCardType = Doctrine::getTable('CountryPaymentMode')->getCardType('ALL');
      }
    }
    echo '<pre>';print_r($arrCardType);die;
    $splitArr = explode(',',$arrCardType[0]['card_type']);
    $arrGroup = Doctrine::getTable('PaymentMode')->getCardGroup($splitArr);

    $k=0;$j=0;
    $arrFinalArray= array();
    for($i=0;$i<count($arrGroup);$i++){
      $arrFinalArray[$arrGroup[$i]['group_id']][$k]['gateway_id'] = $arrGroup[$i]['gateway_id'];
      $arrFinalArray[$arrGroup[$i]['group_id']][$k]['card_type'] = $arrGroup[$i]['card_type'];
      $arrFinalArray[$arrGroup[$i]['group_id']][$k]['display_name'] = $arrGroup[$i]['display_name'];
      $k++;
    }
    $j=0;
    $returnCardType= array();
    foreach($arrFinalArray as $key=>$values){
      $returnCardType[$j]['group_id'] = $key;
      $i=0;
      foreach($values as $activity){
        $returnCardType[$j]['card_detail'][$i]['gateway_id'] = $activity['gateway_id'];
        $returnCardType[$j]['card_detail'][$i]['card_type'] = $activity['card_type'];
        $returnCardType[$j]['card_detail'][$i]['display_name'] = $activity['display_name'];
        $i++;
      }
      $j++;
    }

    // Remove Card Type depending on filter as mentioned in app.yml

    $removeKeys = array() ;
    $addVbV = false ;
    if($this->reachedGpVisaThreshold($amount)){
      //remove GrayPay and NMI Visa
      $i = 0 ;
      foreach($returnCardType as $cardType){

        if($cardType['card_detail'][0]['card_type'] == 'V' || $cardType['card_detail'][0]['card_type'] == 'nmi_vbv'){

          array_push($removeKeys, $i) ;
        }
        $i++ ;
      }
      $addVbV = true ;
    }


    if($this->reachedGpMasterThreshold($amount)){
      //remove GrayPay and NMI Visa
      $i = 0 ;
      foreach($returnCardType as $cardType){

        if($cardType['card_detail'][0]['card_type'] == 'M' || $cardType['card_detail'][0]['card_type'] == 'nmi_mcs'){
          array_push($removeKeys, $i) ;
        }
        $i++ ;
      }
    }


    if($this->isOverCartThreshold($amount)){
      //remove GrayPay Visa, GrayPay Master, NMI Visa and NMI Master
      $i = 0 ;
      foreach($returnCardType as $cardType){

        if($cardType['card_detail'][0]['card_type'] == 'V' || $cardType['card_detail'][0]['card_type'] == 'nmi_vbv' || $cardType['card_detail'][0]['card_type'] == 'M' || $cardType['card_detail'][0]['card_type'] == 'nmi_mcs'){

          array_push($removeKeys, $i) ;
        }
        $i++ ;
      }
      $addVbV = true ;
    }

    foreach($removeKeys as $removeKey){
      $returnCardType = $this->array_remove_keys($returnCardType, "$removeKey") ;
    }

    if(!in_array('Vc',$splitArr)){
      if($addVbV){
        $vbvCard = Doctrine::getTable('PaymentMode')->getCardGroup(array('Vc'));
        $vbvCardType = array() ;
        $vbvCardType['card_detail'][0]['gateway_id'] = $vbvCard[0]['gateway_id'];
        $vbvCardType['card_detail'][0]['card_type'] = $vbvCard[0]['card_type'];
        $vbvCardType['card_detail'][0]['display_name'] = $vbvCard[0]['display_name'];

        array_push($returnCardType, $vbvCardType) ;
      }
    }
    //    echo "<pre>" ; print_r($returnCardType) ; die ;


    return $returnCardType;

  }

  public function getPaymentOptionArray($arrCountry)
  {
    //    $arrCountry = array('0'=>'US','1'=>'IN','2'=>'GB');
    $optionsArr = array();
    $executeAll = true;
    for($i=0;$i<count($arrCountry);$i++){
      $paymentOptions = Doctrine::getTable('CountryPaymentMode')->getPaymentOptions($arrCountry[$i]);
      if($paymentOptions)
      {
        $optionsArr[] = $paymentOptions;
      }
      elseif($executeAll){
        $allOption = Doctrine::getTable('CountryPaymentMode')->getPaymentOptions('ALL');
        if($allOption)
        {
          $optionsArr[] = $allOption;
          $executeAll = false;
        }
      }
    }

    $retuArr = array();
    switch(count($optionsArr))
    {
      case 0:

        break;
      case 1:
        $retuArr = explode(',', $optionsArr[0]);
        break;
      default:
        $common = explode(',', $optionsArr[0]);
        for($i = 1; $i < count($optionsArr); $i++)
        {
          $sp = explode(',', $optionsArr[$i]);
          $common = array_intersect($common, $sp);
        }
        $retuArr = $common;
        break;
    }
    return $retuArr;
  }

  private  function getActiveGatewayId($gatewayId)
  {
    $pcountry = $this->getFirstProcessingCountry();
    if($gatewayId == 10){
        $paymentCardType = 'jna_nmi_vbv';
    }else{
        $paymentCardType = '';
    }
    $midService = $this->getActiveMidService($pcountry, $paymentCardType);
    $midService = strtolower($midService);
    if($gatewayId == 1){

    /**
     * Fetching gateway id from mid details table...
     */
    $midDetailsObj = Doctrine::getTable('MidDetails')->findByServiceAndServiceType('graypay', $midService);
    if(count($midDetailsObj)){
        $gateway_id = $midDetailsObj->getFirst()->getGatewayId();
    }else{
          $gateway_id = 1;
      }

//      switch ($midService)
//      {
//        case 'ps1':
//          $gateway_id = 1;
//          break;
//        case 'llc2':
//          $gateway_id = 8;
//          break;
//        default:
//          $gateway_id = 1;
//          break;
//      }
    }
    else if($gatewayId ==5)
    {

        $midDetailsObj = Doctrine::getTable('MidDetails')->findByServiceAndServiceType('nmi', $midService);
        if(count($midDetailsObj)){
            $gateway_id = $midDetailsObj->getFirst()->getGatewayId();
        }else{
          $gateway_id = 5;
        }

//      switch ($midService)
//      {
//        case 'ps1':
//          $gateway_id = 5;
//          break;
//      case 'llc2':
//          $gateway_id = 9;
//        break;
//      default:
//          $gateway_id = 5;
//      break;
//
//      }
    }else{
      $gateway_id = $gatewayId;
    }
    return array('midService'=>$midService,'gateway_id'=>$gateway_id);
  }

  function getFirstProcessingCountry()
  {
    $arrCartItems = sfContext::getInstance()->getUser()->getCartItems();
    $firstProcessingCountry = array();

    foreach ($arrCartItems as $items) {
      if($items->getType() == 'Passport') {
        $firstProcessingCountry[] = Doctrine::getTable('PassportApplication')->getProcessingCountry($items->getAppId());
      }elseif ($items->getType() == 'Freezone') {
        $firstProcessingCountry[] = 'NG';
      }elseif ($items->getType() == 'Visa') {
        $firstProcessingCountry[] = Doctrine::getTable('VisaApplicantInfo')->getProcessingCountry($items->getAppId());
      }elseif ($items->getType() == 'Vap') {
        $firstProcessingCountry[] = Doctrine::getTable('VapApplication')->getProcessingCountry($items->getAppId());
      }

    }

    return $firstProcessingCountry[count($firstProcessingCountry)-1];
  }

  public function getActiveMidService($country,$paymentCardType='')
  {
      if($paymentCardType == ''){
          $paymentCardType = sfContext::getInstance()->getUser()->getAttribute('paymentCardType');
      }else{
          sfContext::getInstance()->getUser()->getAttributeHolder()->remove('paymentCardType');
      }

      $countryPaymentMode = Doctrine::getTable('CountryPaymentMode')->findByCountryCode($country);

      if(count($countryPaymentMode) > 0){
          $service = $countryPaymentMode[0]['service'];
      }else{
          $countryPaymentMode = Doctrine::getTable('CountryPaymentMode')->findByCountryCode('ALL');
          $service = $countryPaymentMode[0]['service'];
      }

      $serviceArray = explode(',',$service);
      if(count($serviceArray)>1){
        /**
         * [WP: 111] => CR: 157
         * Added condition of MasterCard SecureCode for JNA service...
         */
        if(strtolower(trim($paymentCardType)) == 'jna_nmi_vbv' || strtolower(trim($paymentCardType)) == 'jna_nmi_mcs'){
            $service = 'JNA1';
        }else{
            ##remove JNA1 form array
            for($i=0;$i<count($serviceArray);$i++ ){
                if(strtolower(trim($serviceArray[$i])) != 'jna1'){
                    $service = trim($serviceArray[$i]);
                    break;
                }
            }
        }
    }//End of if(count($serviceArray)>1){...

    return $service;
    
  }//End of public function getActiveMidService($country,$paymentCardType='')...

  public function getPrivilegeUserCardOption()
  {
    $userId = sfContext::getInstance()->getUser()->getGuardUser()->getId();
    //get Privilege User card type
    $arrCardOption = Doctrine::getTable('PaymentPrivilageUser')->findByUserId($userId);

    $arrExplodePaymentOption = array();
    $arrExplodePaymentOption = explode(',', $arrCardOption->getFirst()->getCardType());

    if(!empty($arrExplodePaymentOption)){
      $arrGroup = Doctrine::getTable('PaymentMode')->getCardGroup($arrExplodePaymentOption);
    }


    $k=0;$j=0;
    $arrFinalArray= array();
    for($i=0;$i<count($arrGroup);$i++){
      $arrFinalArray[$arrGroup[$i]['group_id']][$k]['gateway_id'] = $arrGroup[$i]['gateway_id'];
      $arrFinalArray[$arrGroup[$i]['group_id']][$k]['card_type'] = $arrGroup[$i]['card_type'];
      $arrFinalArray[$arrGroup[$i]['group_id']][$k]['display_name'] = $arrGroup[$i]['display_name'];
      $k++;
    }
    $j=0;
    $returnCardType= array();
    foreach($arrFinalArray as $key=>$values){
      $returnCardType[$j]['group_id'] = $key;
      $i=0;
      foreach($values as $activity){
        $returnCardType[$j]['card_detail'][$i]['gateway_id'] = $activity['gateway_id'];
        $returnCardType[$j]['card_detail'][$i]['card_type'] = $activity['card_type'];
        $returnCardType[$j]['card_detail'][$i]['display_name'] = $activity['display_name'];
        $i++;
      }
      $j++;
    }

    if(count($returnCardType) > 0){
      return $returnCardType;
    }else{
      return $this->getNmiOptions();
    }
    
  }

  private function getNmiOptions(){

    $paymentModsArr = array();
    $paymentModsArr['0']['group_id'] = '5.1';
    $paymentModsArr['0']['card_detail']['0']['gateway_id'] = '5';
    $paymentModsArr['0']['card_detail']['0']['card_type'] = 'nmi_vbv';
    $paymentModsArr['0']['card_detail']['0']['display_name'] = 'VISA (Inclusive of Verified by VISA)';

    $paymentModsArr['1']['group_id'] = '5.2';
    $paymentModsArr['1']['card_detail']['0']['gateway_id'] = '5';
    $paymentModsArr['1']['card_detail']['0']['card_type'] = 'nmi_mcs';
    $paymentModsArr['1']['card_detail']['0']['display_name'] = 'MasterCard (Inclusive of MasterCard 3D Secure)';

    $paymentModsArr['2']['group_id'] = '5.3';
    $paymentModsArr['2']['card_detail']['0']['gateway_id'] = '5';
    $paymentModsArr['2']['card_detail']['0']['card_type'] = 'nmi_amx';
    $paymentModsArr['2']['card_detail']['0']['display_name'] = 'Amex';
    return $paymentModsArr;
  }

  public function getPrivilegeUserGatewayId(){
    $arrCardOption = Doctrine::getTable('PaymentPrivilageUser')->findByUserId(sfContext::getInstance()->getUser()->getGuardUser()->getId());
    if($arrCardOption->getFirst()->getMidService() !=''){
      $service = $arrCardOption->getFirst()->getMidService();
    }else{
      $service = "PS1";
    }
    return $service;
  }

 /**
  *
  * @param <type> $cartId
  * @return <type>
  * This function return application details from the given cartId...
  */
  public function getApplicationDetailsByCartId($cartId = ''){
      $appDetailsArray = array();
      if ($cartId != '') {
        $fields = 'item_id AS item_id';
        $cartTrakingInfo = Doctrine::getTable('CartItemsTransactions')->getCartItemInfoByCartId($cartId, $fields);
        $cartTrakingInfoCount = count($cartTrakingInfo);
        $trackingArray = array();
        for ($j = 0; $j < $cartTrakingInfoCount; $j++) {
            $item_id = $cartTrakingInfo[$j]['item_id'];
            if ($item_id != '') {
                $appIdDetails = Doctrine::getTable('IPaymentRequest')->getAppId($item_id);
                if (empty($appIdDetails))
                    continue;

                $appDetails = array(); /* Bug id: 29837 */
                if ($appIdDetails[0]['passport_id'] != '') {
                    $appDetails['passport_id'] = $appIdDetails[0]['passport_id'];
                } else if ($appIdDetails[0]['visa_id'] != '') {
                    $appDetails['visa_id'] = $appIdDetails[0]['visa_id'];
                } else if ($appIdDetails[0]['freezone_id'] != '') {
                    $appDetails['freezone_id'] = $appIdDetails[0]['freezone_id'];
                } else if ($appIdDetails[0]['visa_arrival_program_id'] != '') {
                    $appDetails['visa_arrival_program_id'] = $appIdDetails[0]['visa_arrival_program_id'];
                }

                ## Getting application details...
                $applicationDetails = Doctrine::getTable('CartItemsTransactions')->getApplicationDetails($appDetails);

                $appDetailsArray[$j] = $applicationDetails;
            }//End of if($item_id != ''){...
        }//End of for($j=0;$j<$cartTrakingInfoCount;$j++){...
    }//End of if ($cartId != '') {...
    
    return $appDetailsArray;
    
  }//End of public function getApplicationDetailsByCartId($cartId = ''){...

  /**
   *
   * @param <type> $amount
   * @param <type> $country_id
   * @return <type> 
   */
  public static function convertRate($appType, $amount, $country_id = ''){
      if($country_id != ''){
          $currencyRates = Doctrine::getTable('CurrencyRate')->findByCountryId($country_id);
          if(count($currencyRates) > 0){
              $appType = strtolower($appType);
              switch($appType){
                  case 'passport':
                      $amount = $currencyRates[0]['passport'];
                      break;
                  case 'visa':
                      $amount = $currencyRates[0]['visa'];
                      break;                  
                  case 'vap':
                      $amount = $currencyRates[0]['visa_arrival']+$currencyRates[0]['additional_charges_for_vap'];
                      break;                  
                  default:
                      break;
              }
              //$ratePerDollar = $currencyRates[0]['rate_per_dollar'];
              //return $amount = $ratePerDollar * $amount;
          }
      }
      return $amount;
  }

/**
 *
 * @param <type> $currency
 * @return <type> 
 */
  public static function currencySymbol($currency = '1'){
      switch($currency){
          case '1':
              $symbol = '$';
              break;
          case 'pound':
              $symbol = '&amp;pound;';
              break;
          case '4':
              $symbol = '¥';
              break;
          case '5':
              $symbol = '&amp;pound;';
              break;
          default:
              $symbol = '$';
              break;
      }//End of switch($currency){...

      return $symbol;
  }

  
  
}

?>
