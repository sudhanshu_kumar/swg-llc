<?php
/* 
 * This class is used to full fill payment flow
 * return final payment mode after so many checks
 */

class paymentOptions{

    //public $commonPaymentMods = array();
    //public $paymentCard  = array();
    //public $cardName = null;
    //public $excludeCard = array();
    //public $excludedPaymentModArr = array();
    //public $checkExcludeCard = false;
    //public $requestId = null;
    //public $afterExcludedcommonPaymentMods = array();
    private $data = array();
    
    /**
     * This is a constructor called automatically...
     */
    function __construct(){
        $this->setCommonPaymentMods();        
        ## Getting exclude payment mode array...
        //$this->data['excludeCard'] = sfConfig::get('app_excludeCard');
        $this->data['excludeSurNameText'] = sfConfig::get('app_excludeSurNameText');
        $this->data['checkExcludeCard'] = false;

        /**
         * [WP:084] => CR:122
         * Getting processing country...
         * Based on that creating excludeCard...
         */
        $arrProcessingCountry = $this->arrProcessingCountry[count($this->arrProcessingCountry)-1];        
        $this->data['excludeCard'] = array();
        $countryPaymentModeObj = Doctrine::getTable('CountryPaymentMode')->getCardType($arrProcessingCountry);
        if(count($countryPaymentModeObj) > 0){
            ## do not write anything here...
        }else{
            $countryPaymentModeObj = Doctrine::getTable('CountryPaymentMode')->getCardType('ALL');
        }
        if(count($countryPaymentModeObj) > 0){
            $processingCountryPaymentOptions = $countryPaymentModeObj[0]['card_type'];

            $validation = $countryPaymentModeObj[0]['validation'];
            $validation = explode(',', $validation);
            $surnameMatch = false;
            if(in_array('SN', $validation)){
                $surnameMatch = true;
            }

            /**
             * If surname match and validation contains single value then
             * following code will run...
             */
            if(!$surnameMatch){
                $service = $countryPaymentModeObj[0]['service'];
                $serviceArray = explode(',', $service);
                if(count($serviceArray) == 1){
                    $midMasterObj = Doctrine::getTable('MidMaster')->findByMidService($service);
                    if(count($midMasterObj) > 0){
                        $validation = $midMasterObj[0]['validation'];
                        $validation = explode(',', $validation);
                        if(in_array('SN', $validation)){
                            $surnameMatch = true;
                        }
                    }
                }
            }//End of if(!$surnameMatch){...
            if($surnameMatch){
                $processingCountryPaymentOptions = explode(',', $processingCountryPaymentOptions);                
                for($i=0;$i<count($processingCountryPaymentOptions);$i++){
                    if($processingCountryPaymentOptions[$i]=='Mo' || $processingCountryPaymentOptions[$i]=='MO'){
                        unset($processingCountryPaymentOptions[$i]);
                    }
                }                
                $this->data['excludeCard'] = $processingCountryPaymentOptions;
            }//End of if(in_array('SN', $validation)){...
        }//End of if(count($countryPaymentModeObj) > 0){...
    }//End of function __construct(){...

    /**
     *
     * @param <type> $var contains variable name...
     * @param <type> $val contains variable value...
     */
    public function __set($var, $val) {
        $this->data[$var] = $val;
    }

    /**
     *
     * @param <type> $var contains variable name...
     * @return <type> 
     */
    public function __get($var) {
        return $this->data[$var];
    }

    /**
     *
     * @param <type> $cardName
     * @return <type>
     * This function works on the basis of flow chart...      
     */
    public function getPaymentOption($cardName){
        ## Removing surname match variable...        
        sfContext::getInstance()->getUser()->getAttributeHolder()->remove('surnamematch');
        sfContext::getInstance()->getUser()->getAttributeHolder()->remove('othercheck');
        sfContext::getInstance()->getUser()->getAttributeHolder()->remove('MoFlag');

        ## This will check wether processing country is able to payment through registered card only...
        $isRegisteredCardCountry = $this->getRegisterdCardCountry();
        
        $this->data['cardName'] = $cardName;
        $isCardExist = $this->isCardExist();
        $continue = false;
        $redirectArray = array();
        if($isCardExist){
            $selectPaymentOption = false;
            $vcForm = false;
            if($this->data['checkExcludeCard']){
                $surNameMatch = $this->isSurNameMatch();
                if($surNameMatch){                    
                    sfContext::getInstance()->getUser()->setAttribute('surnamematch', true);
                    $redirectArray = array("isRedirect" => true, "redirectUrl" => 'paymentGateway/ccChoice');
                }else{                    
                  $continue = true;
                }
            }else{                
               $continue = true;
            }

            if($continue){                
                $excludedPaymentModArrCount = count($this->data['excludedPaymentModArr']);
                if($excludedPaymentModArrCount > 1){                    
                    $redirectArray = array("isRedirect" => true, "redirectUrl" => 'paymentGateway/ccChoice' );
                }else{
                    $redirectArray = array("isRedirect" => true, "redirectUrl" => 'paymentGateway/ccChoice' );
                }
            }
            
        }else{ // If Vc card does not exist...
            $userId = sfContext::getInstance()->getUser()->getGuardUser()->getId();

            //$userId = 11;
            $isRegisteredCard = $this->isRegisteredCardExist($userId);

            if($isRegisteredCard){
                ## Showing registered card...
                sfContext::getInstance()->getUser()->setAttribute('othercheck', true);
            }else{

                if($isRegisteredCardCountry){
                    $redirectArray = array("isRedirect" => true, "redirectUrl" => 'paymentGateway/payInfo' );
                }else{ 
                    if($this->data['checkExcludeCard']){
                        $surNameMatch = $this->isSurNameMatch();                        
                        if($surNameMatch){
                            sfContext::getInstance()->getUser()->setAttribute('surnamematch', true);
                            $redirectArray = array("isRedirect" => true, "redirectUrl" => 'paymentGateway/ccChoice');
                        }else{
                          $continue = true;
                        }
                    }else{
                       $continue  = true;
                       $excludedPaymentModArrCount = count($this->data['excludedPaymentModArr']);
                       if(1 == $excludedPaymentModArrCount){
                            if('Mo' == $this->data['excludedPaymentModArr'][0]){
                                ## Generating Money Order Tracking Number...
                                ## $redirectArray = array("isRedirect" => true, "redirectUrl" => 'mo_configuration/trackingProcess?requestId=' . $this->data['requestId'] );
                                $redirectArray = array("isRedirect" => true, "redirectUrl" => 'paymentGateway/moInfo' );
                                
                                $continue  = false;
                            }
                       }else if(0 == $excludedPaymentModArrCount){
                         sfContext::getInstance()->getUser()->setFlash('notice', 'Transaction can not be processed at the moment. Please try again later . ');
                         $redirectArray = array("isRedirect" => true, "redirectUrl" => 'cart/list');
                         $continue  = false;
                       }
                       
                    }

                    if($continue){
                        $excludedPaymentModArrCount = count($this->data['excludedPaymentModArr']);
                        if($excludedPaymentModArrCount > 1){
                            $redirectArray = array("isRedirect" => true, "redirectUrl" => 'paymentGateway/ccChoice' );
                        }else if(1 == $excludedPaymentModArrCount){
                            if('Mo' == $this->data['excludedPaymentModArr'][0]){
                                $redirectArray = array("isRedirect" => true, "redirectUrl" => 'paymentGateway/payInfo' );
                            }else{
                                //Open payment Form...
                                $redirectArray = array("isRedirect" => true, "redirectUrl" => 'paymentGateway/ccChoice' );
                            }
                        }else{
                            ## open new page for registration...
                            $redirectArray = array("isRedirect" => true, "redirectUrl" => 'paymentGateway/payInfo' );
                        }
                    }//End of if($continue){...                    
                    
                }
                
            }
            
        }
        
        return array('F' => $this->data['commonPaymentMods'], 'S' => $redirectArray);
        
    }//End of public function getPaymentOption($cardName){...


    /**
     * This function will work only user come from registered card page and select other option...
     */
    public function getOtherCheck(){
        
       $continue = false;
       $redirectArray = array();
       if($this->data['checkExcludeCard']){
            $surNameMatch = $this->isSurNameMatch();
            if($surNameMatch){
                sfContext::getInstance()->getUser()->setAttribute('surnamematch', true);                
            }else{
              $continue = true;
            }
        }else{
           $continue = true;
        }

        if($continue){            
            $excludedPaymentModArrCount = count($this->data['excludedPaymentModArr']);
            if($excludedPaymentModArrCount > 1){
                
            }else if(1 == $excludedPaymentModArrCount){
                if('Mo' == $this->data['excludedPaymentModArr'][0]){
                    $redirectArray = array("isRedirect" => true, "redirectUrl" => 'paymentGateway/payInfo' );
                }else{
                }
            }else{
                ## open new page for registration...
                $redirectArray = array("isRedirect" => true, "redirectUrl" => 'paymentGateway/payInfo' );
            }
        }//End of if($continue){...
        return $redirectArray;
    }//End of public function getOtherCheck(){...


    /**
     *
     * @param <type> $userId
     * @return <type>
     * This function check wether user contains registered card or not...
     */

    public function isRegisteredCardExist($userId){
        if(trim($userId) != ''){

            $paymentModeArray = $this->getPaymentModeArray();

            $applicantVaultArray = ApplicantVaultTable::getInstance()->getFilterCardsFromVault($userId, $paymentModeArray);            
            if(count($applicantVaultArray) > 0){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    /**
     *
     * @return <type>
     * this function return payment mode like A, M, V, etc...
     */
    public function getPaymentModeArray(){
        $paymentModeArray = array();
        if (count($this->data['commonPaymentMods']) > 0) {
          foreach ($this->data['commonPaymentMods'] as $key => $val) {
            for ($i = 0; $i < count($val['card_detail']); $i++) {
              if ($val['card_detail'][$i]['card_type'] == 'nmi_mcs') {
                $paymentModeArray[] = 'M';
              }
              elseif ($val['card_detail'][$i]['card_type'] == 'nmi_vbv') {
                $paymentModeArray[] = 'V';
              }
              elseif ($val['card_detail'][$i]['card_type'] == 'nmi_amx') {
                $paymentModeArray[] = 'A';
              }
              elseif ($val['card_detail'][$i]['card_type'] == 'Vc') {
                $paymentModeArray[] = 'V';
              }
              elseif ($val['card_detail'][$i]['card_type'] == 'jna_nmi_vbv') {
                $paymentModeArray[] = 'V';
              }
              /**
               * [WP: 111] => CR: 157
               * Added condition of MasterCard SecureCode for JNA service...
               */
              elseif ($val['card_detail'][$i]['card_type'] == 'jna_nmi_mcs') {
                $paymentModeArray[] = 'M';
              }
              else {
                $paymentModeArray[] = $val['card_detail'][$i]['card_type'];
              }
            }//End of for ($i = 0;...
          }//End of foreach ($this->data['commonPaymentMods']...
        }//End of if (count($this->data['commonPaymentMods']) > 0) {...

        return $paymentModeArray;

    }//End of public function getPaymentModeArray(){...


    /**
     *
     * @return <type>
     * This function match surname and if surname match return true else false...
     */
    public function isSurNameMatch(){
        ## If surname will not match of all applications then system generate MO by default...
        $surnameArray = array();

        ## Getting surname validation object...
        $surNameValidationObj = new SurNameValidation();
        $surnameArray = $surNameValidationObj->getSurnameArray();
        
        $surnameArrayCount = count($surnameArray);
        $isSurNameMatch = true;
        if($surnameArrayCount > 0){
            $checkSurName = trim($surnameArray[0]);
            $isSurNameMatch = $surNameValidationObj->matchSurname($surnameArray, $checkSurName);
        }        
        return $isSurNameMatch;
    }
  
    /**
     *
     * @return <type>
     * This function return true or false based on wether card is exist or not
     */
    public function isCardExist(){
        $this->data['paymentCard'] = $this->getCards();
        if(in_array($this->data['cardName'], $this->data['paymentCard'])){
            return true;
        }else{
            return false;
        }        
    }//End of public function isCardExist(){...

    /**
     *
     * @return <type>
     * This function set excludedPaymentModArr
     * This function set afterExcludedcommonPaymentMods
     */
    public function getCards(){
      $this->data['excludedPaymentModArr'] = array();
      if(!empty($this->data['commonPaymentMods'])){
        $this->data['commonPaymentMods'] = array_values($this->data['commonPaymentMods']);
      }

      $this->data['afterExcludedcommonPaymentMods'] = $arrTemp = $this->data['commonPaymentMods'];
      
      $commonPaymentModsCount = count($this->data['commonPaymentMods']);
      $finalPaymentMod = array();
      for($i=0;$i<$commonPaymentModsCount;$i++){
        $cardDetailCount = count($this->data['commonPaymentMods'][$i]['card_detail']);
        if($cardDetailCount > 1){ 
          for($j=0;$j<$cardDetailCount;$j++){
            $card_type = $this->data['commonPaymentMods'][$i]['card_detail'][$j]['card_type'];
            $finalPaymentMod [] = $card_type;

            if(in_array($card_type, $this->data['excludeCard'])){
              $this->data['checkExcludeCard'] = true;
              unset($this->data['afterExcludedcommonPaymentMods'][$i]['card_detail'][$j]);
              ## resetting array key...
              $arrTemp[$i]['card_detail'] = array_values($this->data['afterExcludedcommonPaymentMods'][$i]['card_detail']);
            }else{
              $this->data['excludedPaymentModArr'] [] = $card_type;
            }
          }

          ## unsetting afterExlucdedcommonPaymentMods...
          if(0 == count($this->data['afterExcludedcommonPaymentMods'][$i]['card_detail'])){
            unset($this->data['afterExcludedcommonPaymentMods'][$i]);
          }

        }else{
          $card_type = $this->data['commonPaymentMods'][$i]['card_detail']['0']['card_type'];
          $finalPaymentMod [] = $card_type;
          if(in_array($card_type, $this->data['excludeCard'])){
            $this->data['checkExcludeCard'] = true;
            unset($this->data['afterExcludedcommonPaymentMods'][$i]);
            ## resetting array key...
            $arrTemp = array_values($this->data['afterExcludedcommonPaymentMods']);
          }else{
            $this->data['excludedPaymentModArr'] [] = $card_type;
          }
        }
      }//End of for($i=0;$i<$commonPaymentModsCount;$i++){...

      ## Assgining resetting key array after exclude...
      $this->data['afterExcludedcommonPaymentMods'] = $arrTemp;

      return $finalPaymentMod;
      
    }//End of public function getCards(){...

    /**
     *
     * @return <type>
     * This function return cart items...
     */
    public function getCartItems(){

        $arrCartItems = array();
        $arrCartItems = sfContext::getInstance()->getUser()->getCartItems();
        $this->data['totalCartItems'] = count($arrCartItems);
        return $arrCartItems;
    }

    /**
     * This function set common payment mods...
     */
    public function setCommonPaymentMods(){        
        $arrProcessingCountry = array();
        $totalAmt = 0;
        $totalAppAmount = 0;

        ## Fetching cart items...
        $arrCartItems = $this->getCartItems();

        foreach ($arrCartItems as $items) {
          if($items->getType() == 'Passport') {
            ## get Application details
            $arrProcessingCountry[] = Doctrine::getTable('PassportApplication')->getProcessingCountry($items->getAppId());
          }elseif ($items->getType() == 'Freezone') {
            $arrProcessingCountry[] = 'NG';
          }elseif ($items->getType() == 'Visa') {
            $arrProcessingCountry[] = Doctrine::getTable('VisaApplicantInfo')->getProcessingCountry($items->getAppId());
          }elseif ($items->getType() == 'Vap') {
            $arrProcessingCountry[] = Doctrine::getTable('VapApplication')->getProcessingCountry($items->getAppId());
          }
          $totalAmt += $items->getPrice();
          /**
           * [WP: 087] => CR: 126
           * Getting dollar amount for all cases...
           */
          $totalAppAmount = $totalAppAmount + Functions::getApplicationFees($items->getAppId(), $items->getType());
        }        
        
        ## get country based payment
        $arrCardType = array();
        for ($i = 0; $i < count($arrProcessingCountry); $i++) {
          $arrCardType[] = Doctrine::getTable('CountryBasedPayment')->getCardType($arrProcessingCountry[$i]);
          if (!empty($arrCardType)) {
          }
        }
        
        $commonPaymentMods = array();
        if (!empty($arrProcessingCountry)) {
          $paymentMode = new PaymentModeManager();
          /**
           * [WP: 087] => CR: 126
           * Checking threshold...
           */
          ##$commonPaymentMods = $paymentMode->getCountryCardType($arrProcessingCountry, $totalAmt);// Commented by ashwani for wp:087
          $commonPaymentMods = $paymentMode->getCountryCardType($arrProcessingCountry, $totalAppAmount);
        }

        $this->data['arrCardType'] = $arrCardType;
        $this->data['commonPaymentMods'] = $commonPaymentMods;
        $this->data['arrProcessingCountry'] = $arrProcessingCountry;
        
    }

    /**
     * This function return true or false.
     * This will check wether processing country is able to payment through registered card only...
     */
    public function getRegisterdCardCountry(){
        ## Getting array processing country...
        $arrProcessingCountry = $this->data['arrProcessingCountry'];        
        $arrProcessingCountryCount = count($arrProcessingCountry);
        $isRegisteredCardCountry = false;
        
        if($arrProcessingCountryCount > 0){
            $registeredCountry = Doctrine::getTable('CountryBasedRegisterCard')->findByCountryCode('ALL');            
            $registeredCountryCount = count($registeredCountry);
            if($registeredCountryCount > 0){                
                for($i=0;$i<$arrProcessingCountryCount;$i++){
                    $country_code = Doctrine::getTable('CountryBasedRegisterCard')->findByCountryCode($arrProcessingCountry[$i]);
                    if(count($country_code) > 0){                        
                        if(strtolower($country_code[0]['is_registered']) == 'yes'){
                            $isRegisteredCardCountry = true;
                            break;
                        }                        
                    }else{
                        if(strtolower($registeredCountry[0]['is_registered']) == 'yes'){
                            $isRegisteredCardCountry = true;
                            break;
                        }
                    }
                }//End of for($i=0;$i<$arrProcessingCountryCount;...
            }
        }
        return $isRegisteredCardCountry;
    }

    public function getPaymentOptions() {

        $sfUser = sfContext::getInstance()->getUser();        
        $sfController = sfContext::getInstance()->getController();

        $isPrivilegeUser = false;
        $isPrivilegeUser = $sfUser->isPrivilegeUser();

        $arrCardType = array();
        $paymentModsArr = array();


        $totalCartItems = $this->totalCartItems;
        $arrCardType = $this->arrCardType;
        $paymentModsArr = $this->commonPaymentMods;

        ## This will check wether processing country is able to payment through registered card only...
        $isRegisteredCardCountry = $this->getRegisterdCardCountry();
        
        
        //---start  - vineet - set explicit user to make payment for MO only
        $userId = $sfUser->getGuardUser()->getId();
        if ($userId == '95696') {
            $paymentModsArr = array();
            $paymentModsArr['0']['group_id'] = '6';
            $paymentModsArr['0']['card_detail']['0']['gateway_id'] = "6";
            $paymentModsArr['0']['card_detail']['0']['card_type'] = "Mo";
            $paymentModsArr['0']['card_detail']['0']['display_name'] = "Money Order";
            return array('A1' => $arrCardType, 'A2' => $paymentModsArr,  'A3' => $isRegisteredCardCountry);
        }
        //end  -- vineet -- set explicit user to make payment for MO only
        

        $result = array();
        if (!$isPrivilegeUser) {
            if ($totalCartItems > 1) {
                $paymentOptionsArr = $this->getPaymentOption('Vc');
                if (!empty($paymentOptionsArr['S']) && count($paymentOptionsArr['S']) > 0) {
                    if ($paymentOptionsArr['S']['isRedirect']) {
                        $sfController->redirect($paymentOptionsArr['S']['redirectUrl']);
                    }
                } else {
                    $paymentModsArr = $paymentOptionsArr['F'];
                }
            }//End of if($totalCartItems > 1){...
        } else {
            $paymentMode = new PaymentModeManager();
            $paymentModsArr = $paymentMode->getPrivilegeUserCardOption();

        }

        return array('A1' => $arrCardType, 'A2' => $paymentModsArr,  'A3' => $isRegisteredCardCountry);
    }//End of private function getPaymentMode() {...

    public function getPaymentMode() {

        $sfUser = sfContext::getInstance()->getUser();
        $sfController = sfContext::getInstance()->getController();
        
        $isPrivilegeUser = false;
        $isPrivilegeUser = $sfUser->isPrivilegeUser();

        $arrCardType = array();
        $paymentModsArr = array();

        $countCartItem = $this->totalCartItems;
        $arrCardType = $this->arrCardType;
        $paymentModsArr = $this->commonPaymentMods;
        if ($paymentModsArr) {
            if (!$isPrivilegeUser) {
                if ($countCartItem > 1) {

                    $paymentOptionsArr = $this->getCards();
                    ## If user coming from registered card selecting with 'Other' option...
                    $isOtherCheck = $sfUser->getAttribute('othercheck');
                    if ($isOtherCheck) {
                        $arrOtherFlg = $this->getOtherCheck();
                        if (!empty($arrOtherFlg)) {
                            if ($arrOtherFlg['isRedirect']) {
                                $sfController->redirect($arrOtherFlg['redirectUrl']);
                                exit;
                            }//End of if($arrOtherFlg['isRedirect']){...
                        }//End of if(!empty($arrOtherFlg)){...
                    }//End of if($isOtherCheck){...
                    ## Getting surname match variable...
                    $isSurNameMatch = $sfUser->getAttribute('surnamematch');
                    if ($isSurNameMatch) {
                        ##Do not write anything here...
                    } else {
                        $paymentModsArr = $this->afterExcludedcommonPaymentMods;
                    }                    
                }//End of if($countCartItem > 1){...
            } else { // if privilege user then...
              $paymentMode = new PaymentModeManager();
              $paymentModsArr = $paymentMode->getPrivilegeUserCardOption();
            }
        }

        //---start  - vineet - set explicit user to make payment for MO only
        $userId = $sfUser->getGuardUser()->getId();
        if ($userId == '95696') {
            $paymentModsArr = array();
            $paymentModsArr['0']['group_id'] = '6';
            $paymentModsArr['0']['card_detail']['0']['gateway_id'] = "6";
            $paymentModsArr['0']['card_detail']['0']['card_type'] = "Mo";
            $paymentModsArr['0']['card_detail']['0']['display_name'] = "Money Order";
        }
        //end  -- vineet -- set explicit user to make payment for MO only
        return array('A1' => $arrCardType, 'A2' => $paymentModsArr);
    }

    /**
     * [WP: 087] => CR:126     * 
     * @return <type>
     * This will return processing country from applications exists in the cart...
     */
    public function getProcessingCountry(){
        
        return $arrProcessingCountry = $this->arrProcessingCountry[count($this->arrProcessingCountry)-1];
    }

}
?>
