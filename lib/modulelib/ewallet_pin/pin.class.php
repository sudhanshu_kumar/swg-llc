<?php
class pin {
    function getPin($userId) {
        $pin = substr(rand(),0,4);
        //$pin=1111;
        Doctrine::getTable('EwalletPin')->setPin($this->getEncryptedPin($pin),$userId);
        //send sms
        $user_detail = Doctrine::getTable('UserDetail')->findByUserId($userId)->getFirst();
        $mobileNumber=$user_detail->getMobilePhone();
        $response=$this->sendSms($mobileNumber,$pin);
        return $response;
    }

    function sendSms($mobileNumber,$pin) {
        $mobileNumber=substr($mobileNumber,1,strlen($mobileNumber)-1);
        $message='Welcome to ipay4me.Your ewallet pin is:'.$pin;
        $smsObj=epSmsFactory::getSmsGatewayObject();
        if(Settings::getActiveSmsGateway()=='infobib')
        {
            $smsResponse=$smsObj->sendSms($mobileNumber,$message,'post','ipay4me');

        }else
        {
            $smsResponse=$smsObj->sendSms($mobileNumber,$message);

        }
        $status=$smsResponse['status'];
        return $status;
    }
   function getTotSmsCharge($userId) {
            if(1 == Settings::isEwalletPinActive())
             {
                $perSmsCharge = Settings::per_smscharge_amtInCents();
              
               $chargrePaid = sfContext::getInstance()->getUser()->getGuardUser()->getUserDetail()->getIsSmsChargePaid();
                //$TotSucccessfulPayment= Doctrine::getTable('RechargeOrder')->findTotSuccesfulPayment($userId);

                $EwalletDetailObj= $this->getEwalletDetail();
                if(!$EwalletDetailObj || 'yes' == $chargrePaid )// 0 != $TotSucccessfulPayment)
                    $noOfSmsSent = 0;
                 else
                    $noOfSmsSent = $EwalletDetailObj->getNoOfRetries();
                $TotSmsAmount = ($perSmsCharge/100) * $noOfSmsSent;
                return $TotSmsAmount;
             }
            return 0;
        }
    function getActivatedTill() {
        $dateTime=date('Y-m-d H:i:s');
        $dateTimeArray=explode(" ",$dateTime);
        $dateArray=explode("-",$dateTimeArray['0']);
        $timeArray=explode(":",$dateTimeArray['1']);
        $activatedTill=date('Y-m-d H:i:s',mktime($timeArray['0'],$timeArray['1'],$timeArray['2'],$dateArray['1'],$dateArray['2']+Settings::getEwalletPinActivatedTill(),$dateArray['0']));
        return $activatedTill;
    }

    function setKycStatus($approveBy,$userId) {
        $userDetail = Doctrine::getTable('UserDetail')->findByUserId($userId)->getFirst();
        $accountingObj = new Accounting();
        $master_account_id = $accountingObj->createUserAccount($userId);
        $userDetail->set('master_account_id',$master_account_id);
        $userDetail->set('kyc_status','1');
        $userDetail->set('approved_by',$approveBy);
        $userDetail->save();
        return true;
    }



    function setPinRetries() {
        $ewalletDetail=$this->getEwalletDetail();
        $ewalletDetail->set('pin_retries',$ewalletDetail->getPinRetries()+1);
        $ewalletDetail->save();
        return ($ewalletDetail->getPinRetries());
    }

    function blockPin() {
        $ewalletDetail=$this->getEwalletDetail();
        $ewalletDetail->set('status','blocked');
        $ewalletDetail->save();
    }
    //    function isActive()
    //    {
    //        if(Settings::isEwalletPinActive())
    //        {
    //            $ewalletDetail=$this->getEwalletDetail();
    //            $status= $ewalletDetail->getStatus();
    //            $endDate=Settings::getGracePeriodEndDate();
    //            $currentDate=date("Y-m-d");
    //            $gracePeriod=$this->dateDiff($currentDate, $endDate,1);
    //            if(($status=='active')||($status=='inactive' && $gracePeriod==0) || ($status=='blocked'))
    //            {
    //                return true;
    //            }else{
    //                return false;
    //            }
    //        }else{
    //            return false;
    //        }
    //    }
    function getEwalletDetail() {
        $userId=sfContext::getInstance()->getUser()->getGuardUser()->getId();
        $ewalletDetail= Doctrine::getTable('EwalletPin')->findByUserId(array($userId))->getFirst();
        return $ewalletDetail;
    }

    function getEncryptedPin($pin) {
        return md5($pin);
    }


}

?>
