<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class CurrencyManager
{

    public static function getCurrency($countryId){

        $countryObj = Doctrine::getTable('Country')->find($countryId);
        if(!empty($countryObj)){
            $currency_id = $countryObj->getCurrencyId();
        }else{
            $currency_id = '';
        }

        return $currency_id;
        
    }




    public static function currencySymbol($countryId, $currency = '1'){

      $currencyId = self::getCurrency($countryId);
      $symbol = self::currencySymbolByCurrencyId($currencyId);

      return $symbol;
  }

  public static function currencySymbolByCurrencyId($currencyId){
      switch($currencyId){
          case '1':
              $symbol = '$';
              break;
          case 'pound':
              $symbol = '&amp;pound;';
              break;
          case '4':
              $symbol = '¥';
              break;
          case '5':
              $symbol = '&amp;pound;';
              break;
          default:
              $symbol = '$';
              break;
      }//End of switch($currency){...

      return $symbol;
  }

  

  public static function getConvertedAmount($amount, $from_currency, $to_currency, $totalApplications=1){

        if($amount > 0){
            $currencyObj = Doctrine::getTable('CurrencyConverter')->getConvertedAmount($from_currency, $to_currency);
            if(count($currencyObj) > 0){
                $amount = $amount * $currencyObj->getFirst()->getAmount();

                if($currencyObj->getFirst()->getAdditional() > 0){
                    $amount = $amount + ($totalApplications * $currencyObj->getFirst()->getAdditional());
                    $amount = ceil($amount);
                }
            }
        }
        return $amount;
  }


  public static function getDestinationCurrency($processingCountryId){
      
    $countryObj = Doctrine::getTable('Country')->find($processingCountryId);    
    $destiNationCurrency = 1; //dollar
    if(!empty($countryObj)){
        $destiNationCurrency = $countryObj->getCurrencyId();
    }

    return $destiNationCurrency;
  }  

}

?>
