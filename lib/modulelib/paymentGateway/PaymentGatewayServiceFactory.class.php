<?php
class PaymentGatewayServiceFactory{
/**
  *
  * @param <type> $type
  * @return empService the service implementation
  */
 public static function getService($gateway='') {

     $paymentGatewayManager = ucfirst(strtolower($gateway)) . "Manager" ;
     $paymentGatewayManagerObj = new $paymentGatewayManager;
     return $paymentGatewayManagerObj;
 }
}
?>