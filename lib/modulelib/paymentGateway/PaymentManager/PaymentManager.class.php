<?php
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
*/
class PaymentManager {

    public static $version = 'V1';
    public function paymentRequest($postArray,$gateway_id='', $isMatch=false, $midService='', $sitePath='') {
        // $orderNumber = $this->getOrderNumber();

        $orderObj = $this->createNewOrder($gateway_id, $postArray);
        $order_id = $orderObj->getId();
        $orderNumber = $orderObj->getOrderNumber();


        $orderRequest=  Doctrine::getTable('OrderRequestDetails')->find($postArray['requestId']);
        $orderRequestObj = OrderRequestDetailsTable::getInstance();
        if($orderRequestObj->getPaymentStatus($orderRequest->getOrderRequestId())) {
            sfcontext::getInstance()->getUser()->setFlash('notice', 'Payment has already been made for this Item.');
            $result['status'] = "Already Done";
            return $result;
        }

        $postArray['ep_pay_easy_request']['amount'] = $orderRequest->getAmount();
        $response_text = '';
        switch(sfConfig::get('app_active_paymentgateway')) {
            case 'payEasy':
                $postArray['ep_pay_easy_request']['cus_id'] = $orderNumber;
                $epPayEasyManager = new EpPayEasyManager();
                $paymentStatus = $epPayEasyManager->doPayment($postArray['ep_pay_easy_request']);
                if(9 == $paymentStatus['response_code'])
                $this->insertIntoOrerAuthoeize($orderNumber,$paymentStatus,$gateway_id);
                $order_number = $paymentStatus['cusid'];
                $payStatus = $paymentStatus['status'];
                break;
            case 'grayPay':
                $postArray['ep_pay_easy_request']['order_number'] = $orderNumber;
                $epGrayPayManager = new EpGrayPayManager();
                $paymentStatus = $epGrayPayManager->createRequest($postArray['ep_pay_easy_request'], $gateway_id, $isMatch, $midService);

                $order_number = $paymentStatus['orderid'];
                $payStatus = $paymentStatus['response'];
                $response_text = $paymentStatus['response_text'];
                break  ;
            default:
                throw new Exception("Unable to handle request !");
                break;
        }

        ## this variable provide time period which runs before page...
        ## if this is 0 that means no break will come before thanks...
        $sec = sfConfig::get('app_payment_success_time_out_limit');

        if($sec == 0){
            $updateOrder = $this->updateOrderRequest($order_id, $paymentStatus);
        }

        if($payStatus == 1) {

            if($sec == 0){
                $updateOrder = $this->updateSplitAmount ($postArray['requestId'], $gateway_id);
                return $result = $this->paymentSuccess($order_number);
            }else{
                $sec = sfConfig::get('app_payment_success_time_out_limit');
                $start_time = date('Y-m-d H:i:s', mktime(date("H"),date("i"),date("s")+$sec,date("m"),date("d"),date("Y")));
                $url = "notification/updatePaymentStatus" ;
                $paymentStatusJobId = EpjobsContext::getInstance()->addJob('ApplicationPaymentSuccess',$url, array('order_id' => $order_id, 'orderNumber' => $orderNumber, 'requestId' => $postArray['requestId'], 'gateway_id' => $gateway_id, 'sitePath' => $sitePath, 'paymentStatus' => serialize($paymentStatus)), '', '', $start_time);
                sfContext::getInstance()->getLogger()->debug("sceduled job with id: $paymentStatusJobId");

                ## Updationg jobid into ipay4me_order table...
                Doctrine::getTable('ipay4meOrder')->updateJobId($order_number, $paymentStatusJobId);

                $result = array();
                $result['status'] = 'success';
                $result['validation'] = $order_number;
                return $result;
            }
        }
        else {
            if($sec != 0){
                $updateOrder = $this->updateOrderRequest($order_id, $paymentStatus);
            }
            return $result = $this->paymentFailure($order_number,$response_text);
        }

    }


    public function paymentRequestViaEwallet($postArray,$gateway_id='',$orderNumber='') {
        if(empty($orderNumber)) {
            $orderObj = $this->createNewOrder($gateway_id, $postArray,'ewallet');
            $order_id = $orderObj->getId();
            $orderNumber = $orderObj->getOrderNumber();
        }


        $orderRequest=  Doctrine::getTable('OrderRequestDetails')->find($postArray['requestId']);
        $orderRequestObj = OrderRequestDetailsTable::getInstance();
        if($orderRequestObj->getPaymentStatus($orderRequest->getOrderRequestId())) {
            sfcontext::getInstance()->getUser()->setFlash('notice', 'Payment has already been made for this Item');
            $result['status'] = "Already Done";
            return $result;
        }

        //do accounting of eWallet Account
        $paid_amount = $orderRequest->getAmount();
        $user_id = sfContext::getInstance()->getUser()->getGuardUser()->getId();
        $con = Doctrine_Manager::connection();
        try {
            $con->beginTransaction();
            $accountObj = new Accounting();
            $validationNumber =  $accountObj->doPaymentAccounting($paid_amount, $user_id, $orderNumber) ;

            if(!empty($validationNumber) && ctype_digit($validationNumber)) {

                $paymentStatus = array('status'=>1,'response_code'=>'','response_text'=>'');

                $result =  $this->doPayment($orderNumber,$paymentStatus,$postArray['requestId'], $gateway_id,$validationNumber);
                // $updateOrder = $this->updateOrderRequest($order_id, $paymentStatus,$validationNumber);
                //  $updateSplitOrder = $this->updateSplitAmount ($postArray['requestId'], $gateway_id);
                //////////////////Change this //////////////////////////////////

            }
            else {
                $paymentStatus = array('status'=>0,'response_code'=>'','response_text'=>'');
                $orderObj = Doctrine::getTable('ipay4meOrder')->findByOrderNumber($orderNumber);
                $order_id = $orderObj->getFirst()->getId();

                //update bill status and status in ipay4me order
                $updateOrder = $this->updateOrderRequest($order_id, $paymentStatus);
                $result = $this->paymentFailure($orderNumber);
            }
            $con->commit();
            return $result;

        }
        catch (Exception $e) {
            $con->rollback();
        }

    }

    public function doPayment($orderNumber,$paymentStatus,$requestId, $gateway_id,$validationNumber='' ) {


        $orderObj = Doctrine::getTable('ipay4meOrder')->findByOrderNumber($orderNumber);
        $order_id = $orderObj->getFirst()->getId();
        $paymentStatus = array('status'=>1,'response_code'=>'','response_text'=>'');
        //update bill status and status in ipay4me order
        $updateOrder = $this->updateOrderRequest($order_id, $paymentStatus,$validationNumber);

        //do the split
        $updateSplitOrder = $this->updateSplitAmount ($requestId, $gateway_id);

        //create job for mail and payment notification , also update order_request_details with payment notification
        return $result = $this->paymentSuccess($orderNumber);

    }

    public function rechargeRequest($postArray,$gateway_id='') {
        // $orderNumber = $this->getOrderNumber();

        $orderRequest=  Doctrine::getTable('RechargeOrder')->find(base64_decode($postArray['requestId']));
        $gateway_id = $orderRequest->getgatewayId();
        $gatewayObj = Doctrine::getTable('Gateway')->find($gateway_id);
        $gateway = $gatewayObj->getName();

        $orderObj = $this->createRechargeOrder($gateway_id, $postArray);
        $order_id = $orderObj->getId();
        $orderNumber = $orderObj->getOrderNumber();



        if(0 == $orderRequest->getPaymentStatus()) {
            sfContext::getInstance()->getUser()->setFlash('notice', 'Recharge has been done');
            $result['status'] = "Already Done";
            return $result;
        }


        $postArray['ep_pay_easy_request']['amount'] = $orderRequest->getAmount();

        switch($gateway) {
            case 'payEasy':
                $postArray['ep_pay_easy_request']['cus_id'] = $orderNumber;
                $epPayEasyManager = new EpPayEasyManager();
                $paymentStatus = $epPayEasyManager->doPayment($postArray['ep_pay_easy_request']);
                $order_number = $paymentStatus['cusid'];
                $payStatus = $paymentStatus['status'];
                break;
            case 'grayPay':
                $postArray['ep_pay_easy_request']['order_number'] = $orderNumber;
                $epGrayPayManager = new EpGrayPayManager();
                $paymentStatus = $epGrayPayManager->createRequest($postArray['ep_pay_easy_request']);
                $order_number = $paymentStatus['orderid'];
                $payStatus = $paymentStatus['response'];
                //  $updateOrder = Doctrine::getTable('ipay4meOrder')->saveOrderRequest($order_id, $request_id);
                break  ;
            default:
                throw new Exception("Unable to handle request !");
                break;
        }



        $updateOrder = $this->updateRechargeOrderRequest($order_id, $paymentStatus);


        if($payStatus == 1) {

            return $result = $this->rechargeSuccess($postArray['requestId'],$orderNumber);
        }
        else {
            return $result = $this->rechargeFailure($order_number);
        }

    }
    public function rechargeSuccess($requestId,$order_number) {
        $result = array();
        // $userId = sfContext::getInstance()->getUser()->getGuardUser()->getId();

        //    $orderDetailId = Doctrine::getTable('ipay4meRechargeOrder')->getOrderRequestDetails($order_number);

        $orderRequest=  Doctrine::getTable('RechargeOrder')->find(base64_decode($requestId));
        $payment_status = 0;
        $userId = $orderRequest->getUserId();
        $userDetail=  Doctrine::getTable('UserDetail')->findByUserId($userId);

        $masterAccount = Doctrine::getTable('EpMasterAccount')->find($userDetail->getFirst()->getMasterAccountId());
        $accountNumber =  $masterAccount->getAccountNumber();
        $total_amount = $orderRequest->getAmount();
        $gatewayId = $orderRequest->getGatewayId();
        $amount = $orderRequest->getItemFee();
        $con = Doctrine_Manager::connection();
        try {
            $con->beginTransaction();

            $accountObj = new Accounting();
            $validationNumber = $accountObj->getRechargeAccounts($gatewayId, $userId, $total_amount, $amount);
            if(!empty($validationNumber) && ctype_digit($validationNumber)) {

                // save validation no
                $rechargeOrderObj = Doctrine::getTable('ipay4meRechargeOrder')->findByrechargeOrderId(base64_decode($requestId));
                $rechargeOrderObj->getFirst()->setValidationNumber($validationNumber);
                $rechargeOrderObj->save();


                $orderRequest->setPaymentStatus($payment_status);
                $smsChargePaid = 'yes';
                $userDetail->getFirst()->setIsSmsChargePaid($smsChargePaid);
                $userDetail->getFirst()->save();
                $orderRequest->save();

                ////////////////// for ewaalet consolidation update ////////////////


                $actualAmount = $orderRequest->getAmount();

                $ewalletConsolidationObj = new UserEwalletConsolidation();
                $ewalletConsolidationObj->updateEwalletAccount($gatewayId,$userId,$actualAmount,$amount);

                //////////////////////////////////

                sfContext::getInstance()->getConfiguration()->loadHelpers(array('EPortal'));
                $amount_formatted = format_amount($orderRequest->getAmount(),1);
                $eventHolder = new pay4meAuditEventHolder(
                    EpAuditEvent::$CATEGORY_TRANSACTION,
                    EpAuditEvent::$SUBCATEGORY_TRANSACTION_RECHARGE,
                    EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_TRANSACTION_RECHARGE, array('accountNumber'=>$accountNumber)),
                    null
                );
                sfContext::getInstance()->getEventDispatcher()->notify(new sfEvent($eventHolder, 'epAuditEvent'));
                //    $notificationUrl = "notification/PaymentNotificationV1" ;
                //    $senMailUrl = "notification/paymentSuccessMail" ;
                //    $taskId = EpjobsContext::getInstance()->addJob('PaymentNofication',$notificationUrl, array('order_detail_id' => $orderDetailId));
                //    sfContext::getInstance()->getLogger()->debug("Recharge succesful id: $taskId");
                //    $this->auditPaymentTransaction($orderDetailId, $order_number);
                //add mail in job
                //    $mailTaskId = EpjobsContext::getInstance()->addJob('PaymentSuccessMail',$senMailUrl, array('order_detail_id'=>$orderDetailId));
                //    sfContext::getInstance()->getLogger()->debug("sceduled payment successful mail job with id: $mailTaskId");

                $result['status'] = 'success';
                $result['validation'] = $order_number;
                $con->commit();
                return $result;
            }
        }
        catch (Exception $e) {
            $con->rollback();
        }
    }


    public function createNewOrder($gateway_id,$postArray,$rtype='') {

/**
* [WP: 103] => CR: 148
* Fetching processing country...
*/
        $paymentOptions = new paymentOptions();
        $processingCountry = $paymentOptions->getProcessingCountry();

        $orderManagerObj = orderServiceFactory::getService(self::$version) ;
        $orderNumber = $orderManagerObj->getOrderNumber();
        $orderObj = new ipay4meOrder();
        $orderObj->setOrderNumber($orderNumber);
        $orderObj->setGatewayId($gateway_id);

        ## Setting processing country...
        $orderObj->setProcessingCountry($processingCountry);

        $orderObj->setOrderRequestDetailId($postArray['requestId']);

        if('ewallet' != $rtype) {
            $dataArray = $postArray['ep_pay_easy_request'];

            $card_len = strlen($dataArray['card_Num']);
            $card_first = substr($dataArray['card_Num'], 0, 4);
            $card_last = substr($dataArray['card_Num'], -4, 4);
            //    $card_Num = md5($dataArray['card_Num']);

            $orderObj->setCardFirst($card_first);
            $orderObj->setCardLast($card_last);
            $orderObj->setCardLen($card_len);
            //    $orderObj->setCardNum($card_Num);
            $orderObj->setCardType($dataArray['card_type']);
            $orderObj->setCardHash(md5($dataArray['card_Num']));
            $orderObj->setCardHolder($dataArray['card_holder']);
            $orderObj->setPhone($dataArray['phone']);
            $orderObj->setPayorName($dataArray['first_name']." ".$dataArray['last_name']);
            $concat_str = "~";
            $address = $dataArray['address1'].$concat_str.$dataArray['address2'].$concat_str.$dataArray['town'].$concat_str.$dataArray['state'].$concat_str.$dataArray['zip'].$concat_str.$dataArray['country'];
            $orderObj->setAddress($address);
        }
        $orderObj->save();
        return $orderObj;
    }

    public function createRechargeOrder($gateway_id,$postArray) {
        $orderManagerObj = orderServiceFactory::getService(self::$version) ;
        $orderNumber = $orderManagerObj->getOrderNumber('recharge');

        $orderObj = new ipay4meRechargeOrder();
        $orderObj->setOrderNumber($orderNumber);
        $orderObj->setGatewayId($gateway_id);
        $orderObj->setRechargeOrderId(base64_decode($postArray['requestId']));
        $dataArray = $postArray['ep_pay_easy_request'];

        $card_len = strlen($dataArray['card_Num']);
        $card_first = substr($dataArray['card_Num'], 0, 4);
        $card_last = substr($dataArray['card_Num'], -4, 4);

        $orderObj->setCardFirst($card_first);
        $orderObj->setCardLast($card_last);
        $orderObj->setCardLen($card_len);
        $orderObj->setCardType($dataArray['card_type']);
        $orderObj->setCardHolder($dataArray['card_holder']);
        $orderObj->setPhone($dataArray['phone']);
        $orderObj->setPayorName($dataArray['first_name']." ".$dataArray['last_name']);
        $concat_str = "~";
        $address = $dataArray['address1'].$concat_str.$dataArray['address2'].$concat_str.$dataArray['town'].$concat_str.$dataArray['state'].$concat_str.$dataArray['zip'].$concat_str.$dataArray['country'];
        $orderObj->setAddress($address);
        $orderObj->save();
        return $orderObj;
    }

    public function updateRechargeOrderRequest($order_id,$paymentResponseArray) {
        $orderObj = Doctrine::getTable('ipay4meRechargeOrder')->find($order_id);
        if($paymentResponseArray['status'] == 1) {
            $payment_status = 0;
        }
        else {
            $payment_status = 1;
        }

        ///////////////for vbv ///////////////

        if(isset($paymentResponseArray['card_Num'])) {
            $card_len = strlen($paymentResponseArray['card_Num']);
            $card_first = substr($paymentResponseArray['card_Num'], 0, 4);
            $card_last = substr($paymentResponseArray['card_Num'], -4, 4);

            $orderObj->setCardFirst($card_first);
            $orderObj->setCardLast($card_last);
            $orderObj->setCardLen(16);
        }
        /////////////////////////////////////
        $orderObj->setPaymentStatus($payment_status);
        $orderObj->setResponseCode($paymentResponseArray['response_code']);
        $orderObj->setResponseText($paymentResponseArray['response_text']);
        $orderObj->save();
        return $orderObj;
    }
    public function updatePaymentRequest($order_id,$paymentResponseArray) {
        $orderObj = Doctrine::getTable('ipay4meOrder')->find($order_id);
        if($paymentResponseArray['status'] == 1) {
            $payment_status = 0;
        }
        else {
            $payment_status = 1;
        }

        ///////////////for vbv ///////////////

        if(isset($paymentResponseArray['card_Num'])) {
            $card_len = 16;
            $card_first = substr($paymentResponseArray['card_Num'], 0, 4);
            $card_last = substr($paymentResponseArray['card_Num'], -4, 4);

            $orderObj->setCardFirst($card_first);
            $orderObj->setCardLast($card_last);
            $orderObj->setCardLen($card_len);
        }
        /////////////////////////////////////
        $orderObj->setPaymentStatus($payment_status);
        $orderObj->setCardType('V');
        $orderObj->setResponseCode($paymentResponseArray['response_code']);
        $orderObj->setResponseText($paymentResponseArray['response_text']);
        $orderObj->save();
        return $orderObj;
    }
    public function updatePaymentRequestNmi($order_id,$paymentResponseArray) {
        $orderObj = Doctrine::getTable('ipay4meOrder')->find($order_id);
        if($paymentResponseArray['status'] == 1) {
            $payment_status = 0;
        }
        else {
            $payment_status = 1;
        }

        ///////////////for vbv ///////////////

        if(isset($paymentResponseArray['card_Num'])) {
            $card_len = 16;
            $card_first = substr($paymentResponseArray['card_Num'], 0, 4);
            $card_last = substr($paymentResponseArray['card_Num'], -4, 4);

            $orderObj->setCardFirst($card_first);
            $orderObj->setCardLast($card_last);
            $orderObj->setCardLen($card_len);
        }
        /////////////////////////////////////
        $orderObj->setPaymentStatus($payment_status);
        $orderObj->setCardType($paymentResponseArray['card_type']);
        $orderObj->setResponseCode($paymentResponseArray['response_code']);
        $orderObj->setResponseText($paymentResponseArray['response_text']);
        $orderObj->save();
        return $orderObj;
    }
    public function updateSplitAmount($orderRequestDetail_id,$gateway_id) {

        $OrderSplitAllObj= Doctrine::getTable('OrderRequestSplit')->findByOrderDetailId($orderRequestDetail_id);
        // print "<pre>";
        // print_r($OrderSplitAllObj->toArray());exit;
        // $merchnat_id = $OrderSplitAllObj->getfirst()->getMerchantId();


        if($gateway_id != sfConfig::get('app_ewallet_gateway_id')) { //hardcoded to be changed to eWallet
            $transctionChargeObj= Doctrine::getTable('TransactionCharges')->findByGatewayId($gateway_id);



            if(is_object($transctionChargeObj)) {
                if("percentage" == $transctionChargeObj->getfirst()->getSplitType()) {
                    foreach($OrderSplitAllObj as $orderObj) {
                        $transCharge = round(($orderObj->getAmount() * $transctionChargeObj->getfirst()->getTransactionCharges())/100) ;
                        $itemFee = $orderObj->getAmount() - $transCharge;
                        $orderObj->setItemFee($itemFee);
                        $orderObj->setTransactionCharges($transCharge);
                        $orderObj->save();
                    }


                }
            }
        }
        else {
            $accountObj = new UserEwalletConsolidation();
            $user_id = sfContext::getInstance()->getUser()->getGuardUser()->getId();
            $detailsObj = Doctrine::getTable('OrderRequestDetails')->find($orderRequestDetail_id);
            sfContext::getInstance()->getConfiguration()->loadHelpers(array('EPortal'));
            $payable_amount = convertToCents($detailsObj->getAmount());
            $splitArray =  $accountObj->getSplitConfiguration($payable_amount,$user_id) ;
            //                    print "<pre>";
            //                    print_R($splitArray);
            $transactionCharge = 0;
            $charge = 0;
            foreach($splitArray as $key=>$value) {
                $charge = $charge+$value['service_charge'] * $value['amount'];

            }
            $transactionCharge = $charge/$payable_amount;

            foreach($OrderSplitAllObj as $orderObj) {
                $transCharge = round(($orderObj->getAmount() * $transactionCharge)/100) ;
                $itemFee = $orderObj->getAmount() - $transCharge;
                $orderObj->setItemFee($itemFee);
                $orderObj->setTransactionCharges($transCharge);
                $orderObj->save();
            }
            $accountObj->updateEwalletConsolidationAcct($splitArray,$user_id);
            //      exit;


        }
        return $orderObj;
    }





    public function insertIntoOrerAuthoeize($orderNumber,$paymentStatus,$gatewayId) {

        $orderAuthorizeObj = new OrderAuthorize();
        $orderAuthorizeObj->setOrderNumber($orderNumber);
        $orderAuthorizeObj->setGatewayId($gatewayId);
        $orderAuthorizeObj->setTransDate($paymentStatus['trans_time']);
        $orderAuthorizeObj->setResponseCode($paymentStatus['response_code']);
        $orderAuthorizeObj->setResponseText($paymentStatus['response_text']);
        $orderAuthorizeObj->save();
        return $orderAuthorizeObj;
    }
    public function updateOrderRequest($order_id,$paymentResponseArray,$validationNumber='') {
        $orderObj = Doctrine::getTable('ipay4meOrder')->find($order_id);




        if($paymentResponseArray['status'] == 1) {
            $payment_status = 0;
            //updating bill table
            $billDetails = $orderObj->getOrderRequestDetails()->getOrderRequest()->getBill();
            $billDetailsToUpdate = $billDetails->getfirst();
            $billDetailsToUpdate->setStatus('paid');
            $billDetailsToUpdate->save();
        }
        else {
            $payment_status = 1;
        }
        $orderObj->setPaymentStatus($payment_status);
        if(!empty($validationNumber)) {
            $orderObj->setValidationNumber($validationNumber);
        }
        $orderObj->setResponseCode($paymentResponseArray['response_code']);

        $orderObj->setResponseText($paymentResponseArray['response_text']);
        $orderObj->save();
        return $orderObj;
    }

    public function paymentSuccess($order_number) {
        $userObj = sfContext::getInstance()->getUser();
        //clear the cart
        $cartObj = new cartManager();
        $cartObj->emptyCart();
        $userObj->saveCartInDb();


        $result = array();
        $userId = $userObj->getGuardUser()->getId();

        $orderDetailId = Doctrine::getTable('ipay4meOrder')->getOrderRequestDetails($order_number);

        $updatePayment = Doctrine::getTable('OrderRequestDetails')->updatePayment($orderDetailId, $userId);

        /**
         * [WP: 112] => CR: 158
         * Updating payment status set to 0...
         */
        Functions::updateTransactionServiceChargesPaymentStatus($orderDetailId);

        $notificationUrl = "notification/PaymentNotificationV1" ;
        $senMailUrl = "notification/paymentSuccessMail" ;
        $taskId = EpjobsContext::getInstance()->addJob('PaymentNofication',$notificationUrl, array('order_detail_id' => $orderDetailId));
        sfContext::getInstance()->getLogger()->debug("sceduled job with id: $taskId");
        $this->auditPaymentTransaction($orderDetailId, $order_number);
        //add mail in job
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
        $url = url_for("report/getReceipt", true).'?receiptId='.Settings::encryptInput($order_number);
        $url1 = url_for("report/paymentHistory",true);

        $mailTaskId = EpjobsContext::getInstance()->addJob('PaymentSuccessMail',$senMailUrl, array('order_detail_id'=>$orderDetailId,'url'=>$url,'url1'=>$url1));
        sfContext::getInstance()->getLogger()->debug("sceduled payment successful mail job with id: $mailTaskId");

        $result['status'] = 'success';
        $result['validation'] = $order_number;
        return $result;
    }

    public function paymentFailure($order_number,$response_text='') {

        $result = array();
        $orderDetailId = Doctrine::getTable('ipay4meOrder')->getOrderRequestDetails($order_number);

        if(!empty($response_text))
        {
            if($response_text =='zip not matched')
            {
                $result['status'] = 'zip_failure';
            }
            else
            {
                $result['status'] = 'failure';
            }
        }
        else
        {
            $result['status'] = 'failure';
        }

        $result['requestId'] = $orderDetailId;
        $result['validation'] = $order_number;
        return $result;
    }
    public function rechargeFailure($order_number) {

        $result = array();
        $orderDetailId = Doctrine::getTable('ipay4meRechargeOrder')->getOrderRequestDetails($order_number);

        $result['status'] = 'failure';
        $result['requestId'] = $orderDetailId;
        return $result;
    }
    public function  auditPaymentTransaction($orderDetailId, $cusId) {
        //Log audit Details
        $orderDetailsObj = Doctrine::getTable('OrderRequestDetails')->find($orderDetailId);
        $amount = $orderDetailsObj->getAmount();
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('EPortal'));
        $amount_formatted = format_amount($amount,1);
        $applicationArr = array(new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_P4MTXNID,$orderDetailId,$orderDetailId));
        $eventHolder = new pay4meAuditEventHolder(
            EpAuditEvent::$CATEGORY_TRANSACTION,
            EpAuditEvent::$SUBCATEGORY_TRANSACTION_PAYMENT,
            EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_TRANSACTION_PAYMENT, array('reqOrderNumber' => $cusId, 'amount'=>$amount_formatted)),
            $applicationArr);

        sfContext::getInstance()->getEventDispatcher()->notify(new sfEvent($eventHolder, 'epAuditEvent'));
    }

    public function isRefundAllowed($gatewayId){

        /**
         * [WP: 107] => CR: 154
         * Fetching all mids from table and check weather refund is allowed or not...
         */
        $midDetailsObj = Doctrine::getTable('MidDetails')->findAll();
        $gatewayIds = array();
        if(count($midDetailsObj)){
            foreach($midDetailsObj AS $value){
                $gatewayIds[] = $value->getGatewayId();
            }
        }

        if(in_array($gatewayId, $gatewayIds)){
            return true ;
        }else{
            return false ;
        }

    }//End of public function isRefundAllowed($gatewayId){...

    public function refundPayment($orderNumber, $comments, $action='',$arrAppId, $gatewayId) {
        $totalAmount = 0;
        for($i=0;$i<count($arrAppId);$i++){
            $explode = explode('_',$arrAppId[$i]);
            // commented by ashwani for WP:102 => CR: 144
            //      if($explode[1] == 'passport'){
            //        $amount = Doctrine::getTable('PassportApplication')->getPaidAmount($explode[0], $explode[3]);
            //        $totalAmount += $amount;
            //      }else if($explode[1] == 'vap'){
            //          $amount = Doctrine::getTable('VapApplication')->getPaidAmount($explode[0]);
            //          $totalAmount += $amount;
            //      }else{
            //        $amount = Doctrine::getTable('VisaApplication')->getPaidAmount($explode[0], $explode[3]);
            //        $totalAmount += $amount;
            //      }

            $totalAmount += $explode[2];

        }
        //    $gatewayId = ipay4meOrderTable::getInstance()->getGatewayId($orderNumber) ;
        //    $isRefundAllowed = $this->isRefundAllowed($gatewayId) ;
        //    if($isRefundAllowed){
        $DontRefundGraypay = false ;
        $DontRefundGraypay = VaultRefundListTable::getInstance()->isDontRefundGraypayVault($orderNumber,$totalAmount);
        if( $DontRefundGraypay){
            $DontRefundGraypay = true ;
        }

        /**
         * [WP: 107] => CR: 154
         * Fetching gateways id from database to make below code dynamic...
         */

        $midDetailsObj = Doctrine::getTable('MidDetails')->findAll();
        $gatewayIds = array();
        if(count($midDetailsObj)){
            foreach($midDetailsObj AS $value){
                $gatewayIds[] = $value->getGatewayId();
            }
        }

        if($gatewayId == 10){ //for JNA only
            $midService = $this->getMidService($gatewayId);
            $epNmiManager = new EpNmiManager();
            ## Setting gatewayid and midService to the class...
            $epNmiManager->Gateway_id = $gatewayId;
            $epNmiManager->midService = $midService;

            $isRefunded = $epNmiManager->refund($orderNumber,$totalAmount, $gatewayId) ;
        }else if(in_array($gatewayId, $gatewayIds)){ //All gateways except JNA...
            if(!$DontRefundGraypay){
                $transaction_number = '' ;
                //if is in vault and processed then get the transaction_number from batch process response
                $vaultProcessedTxnNo = BatchProcessRequestTable::getInstance()->getVaultProcessedTxnNo($orderNumber);
                $transaction_number = $vaultProcessedTxnNo;

                $midService = $this->getMidService($gatewayId);

                $epGrayPayManager = new EpGrayPayManager();
                $isRefunded = $epGrayPayManager->refund($orderNumber,$totalAmount, $gatewayId, $transaction_number, $midService) ;
            }else{
                $isRefunded = true ;
            }
        } else {
            $isRefunded = false ;
        }
       
        if($isRefunded){
            $isIpay4meOrderBlocked = Doctrine::getTable('ipay4meOrder')->setBlockStatus($orderNumber) ;
            if($isIpay4meOrderBlocked){
                $orderRequestDetailId = Doctrine::getTable('ipay4meOrder')->getOrderRequestDetailId($orderNumber) ;
                $isOrderDetailsBlocked = Doctrine::getTable('OrderRequestDetails')->setBlockStatus($orderRequestDetailId) ;
                if($isOrderDetailsBlocked){         
                    if($action == '')
                    $action = 'refund' ;
                    $status = 'request sent for refund';

                    $isOrderExist = Doctrine::getTable('RollbackPayment')->findByOrderNumber($orderNumber)->toArray() ;

                    if(count($isOrderExist) > 0){
                        for($i=0;$i<count($arrAppId);$i++){
                            $explode = explode('_',$arrAppId[$i]);
                            $isRollbackDetailsUpdated = Doctrine::getTable('RollbackPaymentDetails')->setRollbackDetail($explode[0],$explode[1],$explode[2],$isOrderExist[0]['id'],$action,$status) ;
                        }
                    }else{
                        $isRollbackUpdated = Doctrine::getTable('RollbackPayment')->setRollback($orderNumber, $comments) ;
                        for($i=0;$i<count($arrAppId);$i++){
                            $explode = explode('_',$arrAppId[$i]);
                            $isRollbackDetailsUpdated = Doctrine::getTable('RollbackPaymentDetails')->setRollbackDetail($explode[0],$explode[1],$explode[2],$isRollbackUpdated,$action,$status) ;
                        }
                    }

                    if($isRollbackDetailsUpdated){
                        $requestDetails = Doctrine::getTable('OrderRequestDetails')->find($orderRequestDetailId);
                        $userDetailObj = $requestDetails->getSfGuardUser()->getUserDetail();
                        $name = $userDetailObj->getFirstName()." ".$userDetailObj->getLastName() ;
                        //Auditing refund
                        $applicationArr = array(new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_P4MTXNID,$orderRequestDetailId,$orderRequestDetailId));
                        $eventHolder = new pay4meAuditEventHolder(
                            EpAuditEvent::$CATEGORY_TRANSACTION,
                            EpAuditEvent::$SUBCATEGORY_TRANSACTION_REFUND,
                            EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_TRANSACTION_REFUND, array('username' => $name)),
                            $applicationArr);
                        sfContext::getInstance()->getEventDispatcher()->notify(new sfEvent($eventHolder, 'epAuditEvent'));
                        if($DontRefundGraypay)
                        {
                            $notificationUrl = "notification/refundMailVault" ;
                            $senMailUrl = "notification/refundAdminVaultMail" ;
                        }else{
                            $notificationUrl = "notification/paymentRefundMailUser" ;
                            $senMailUrl = "notification/paymentRefundMailAdmin" ;
                        }//mail to user
                        $taskId = EpjobsContext::getInstance()->addJob('paymentRefundMailUser',$notificationUrl, array('orderRequestDetailId' => $orderRequestDetailId,'totalRefund'=>$totalAmount,'orderNumber' =>$orderNumber));
                        sfContext::getInstance()->getLogger()->debug("sceduled job with id: $taskId");
                        //mail to admin
                        $mailTaskId = EpjobsContext::getInstance()->addJob('paymentRefundMailAdmin',$senMailUrl, array('orderRequestDetailId'=>$orderRequestDetailId,'totalRefund'=>$totalAmount,'orderNumber' =>$orderNumber));
                        sfContext::getInstance()->getLogger()->debug("sceduled job with id: $mailTaskId");
                        return true ;
                    }
                }
            }
        }
        //    }
        return false ;
    }

    public function refundAndBlockPayment($orderNumber, $comments,$action='',$appId, $gatewayId) {

        $isRefunded = $this->refundPayment($orderNumber, $comments, 'refund and block',$appId, $gatewayId) ;
        if($isRefunded){
            $orderRequestDetailId = Doctrine::getTable('ipay4meOrder')->getOrderRequestDetailId($orderNumber) ;
            /**
             * [WP: 112] => CR: 158
             * Updating payment status set to 2...
             */
            Functions::updateTransactionServiceChargesPaymentStatus($orderRequestDetailId, $status = 2);
            
            $requestDetails = Doctrine::getTable('OrderRequestDetails')->find($orderRequestDetailId);
            $user_id = $requestDetails->getUserId();
            $isIpay4meUserBlocked = Doctrine::getTable('UserDetail')->setBlockPaymentRights($user_id) ;
            $isUserCardBlock = Doctrine::getTable('FpsDetail')->setBlockUserCard($orderNumber) ;

            $userDetailObj = $requestDetails->getSfGuardUser()->getUserDetail();
            $name = $userDetailObj->getFirstName()." ".$userDetailObj->getLastName() ;
            //Auditing refund and block
            $applicationArr = array(new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_P4MTXNID,$orderRequestDetailId,$orderRequestDetailId));
            $eventHolder = new pay4meAuditEventHolder(
                EpAuditEvent::$CATEGORY_TRANSACTION,
                EpAuditEvent::$SUBCATEGORY_TRANSACTION_BLOCK,
                EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_TRANSACTION_BLOCK, array('username' => $name)),
                $applicationArr);

            sfContext::getInstance()->getEventDispatcher()->notify(new sfEvent($eventHolder, 'epAuditEvent'));
            return true ;
        }
        return false ;
    }

    public function chargebackPayment($orderNumber, $comments,$arrAppId) {


        $isIpay4meOrderBlocked = Doctrine::getTable('ipay4meOrder')->setChargebackStatus($orderNumber) ;
        if($isIpay4meOrderBlocked){
            $orderRequestDetailId = Doctrine::getTable('ipay4meOrder')->getOrderRequestDetailId($orderNumber) ;
            $isOrderDetailsBlocked = Doctrine::getTable('OrderRequestDetails')->setChargebackStatus($orderRequestDetailId) ;
            /**
             * [WP: 112] => CR: 158
             * Updating payment status set to 3...
             */
            Functions::updateTransactionServiceChargesPaymentStatus($orderRequestDetailId, $status = 3);

            if($isOrderDetailsBlocked){
                $action = 'chargeback' ;
                $status = 'request sent for chargeback';
                $isRollbackUpdated = Doctrine::getTable('RollbackPayment')->setRollback($orderNumber, $comments) ;
                for($i=0;$i<count($arrAppId);$i++){
                    $explode = explode('_',$arrAppId[$i]);
                    $isRollbackDetailsUpdated = Doctrine::getTable('RollbackPaymentDetails')->setRollbackDetail($explode[0],$explode[1],$explode[2],$isRollbackUpdated,$action,$status) ;
                }
                if($isRollbackDetailsUpdated){
                    $orderRequestDetailId = Doctrine::getTable('ipay4meOrder')->getOrderRequestDetailId($orderNumber) ;
                    $requestDetails = Doctrine::getTable('OrderRequestDetails')->find($orderRequestDetailId);
                    $user_id = $requestDetails->getUserId();
                    $isIpay4meUserBlocked = Doctrine::getTable('UserDetail')->setBlockPaymentRights($user_id) ;
                    $isUserCardBlock = Doctrine::getTable('FpsDetail')->setBlockUserCard($orderNumber) ;
                    //Auditing refund
                    $applicationArr = array(new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_P4MTXNID,$orderRequestDetailId,$orderRequestDetailId));
                    $userDetailObj = $requestDetails->getSfGuardUser()->getUserDetail();
                    $name = $userDetailObj->getFirstName()." ".$userDetailObj->getLastName() ;
                    $eventHolder = new pay4meAuditEventHolder(
                        EpAuditEvent::$CATEGORY_TRANSACTION,
                        EpAuditEvent::$SUBCATEGORY_TRANSACTION_CHARGEBACK,
                        EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_TRANSACTION_CHARGEBACK, array('username' => $name)),
                        $applicationArr);
                    sfContext::getInstance()->getEventDispatcher()->notify(new sfEvent($eventHolder, 'epAuditEvent'));
                    return true ;
                }


            }
        }

        return false ;

    }

    public function getAllPaymentByCard($orderNumber){
        //    $orderNumber = 129060371444158;
        $cardNumber = Doctrine::getTable('FpsDetail')->findByOrderNumber($orderNumber);

        if($cardNumber)
        {
            //get all order number by card number
            $allOrderNumber = Doctrine::getTable('FpsDetail')->getAllOrderNumber($cardNumber[0]['card_num'],$cardNumber[0]['trans_date']);
            return $allOrderNumber;
        }else{
            return $cardNumber;
        }
    }

    public function moneyorder($trackingArr, $moneyorderId, $form_val)
    {
        $orderManagerObj = orderServiceFactory::getService(self::$version) ;
        foreach ($trackingArr as $key => $value)
        {
            $acociatedObj = new TrackingMoneyorder();
            $acociatedObj->cart_track_id = $value[0];
            $acociatedObj->moneyorder_id = $moneyorderId;
            $acociatedObj->save();
            $orderNumber = $orderManagerObj->getOrderNumber();
            $this->createOrderForMo($orderNumber, $value[1], $form_val);
            $status = CartTrackingNumberTable::getInstance()->changeTrackingSataus($value[0], "Associated", $orderNumber);
        }
    }

    protected function createOrderForMo($orderNumber, $requestId, $dataArray)
    {
        /**
        * [WP: 103] => CR: 148
        * Fetching processing country...
        */
        $paymentOptions = new paymentOptions();
        $processingCountry = $paymentOptions->getProcessingCountry();


        $gateway_id = 6;
        $orderObj = new ipay4meOrder();
        $orderObj->setOrderNumber($orderNumber);
        $orderObj->setGatewayId($gateway_id);
        $orderObj->setOrderRequestDetailId($requestId);
        $orderObj->setProcessingCountry($processingCountry);
        $orderObj->save();
    }

    /**
    * functions start for wire transfer
    */
    public function wiretransfer($trackingArr, $wiretransferId, $form_val)
    {
        $orderManagerObj = orderServiceFactory::getService(self::$version) ;
        foreach ($trackingArr as $key => $value)
        {
            //$acociatedObj = new WireTrackingNumber();
            //$acociatedObj->wire_tracking_id = $wiretransferId;
            //$acociatedObj->save();

            $orderNumber = $orderManagerObj->getOrderNumber();
            $this->createOrderForWt($orderNumber, $value[1], $form_val);
            $status = WireTrackingNumberTable::getInstance()->changeTrackingSataus($value[0], "Associated", $orderNumber, $wiretransferId);
        }
    }


    protected function createOrderForWt($orderNumber, $requestId, $dataArray)
    {
        $gateway_id = 7;
        $orderObj = new ipay4meOrder();
        $orderObj->setOrderNumber($orderNumber);
        $orderObj->setGatewayId($gateway_id);
        $orderObj->setOrderRequestDetailId($requestId);
        $orderObj->save();
    }

    function getMidService($gatewayId){
        $midService = '';
        /**
         * [WP: 107] => CR: 154
         * Fetching mid service name for database...
         */
        $midDetailsObj = Doctrine::getTable('MidDetails')->findByGatewayId($gatewayId);
        if(count($midDetailsObj)){
            $midService = strtolower($midDetailsObj->getFirst()->getServiceType());
        }
        return $midService;
    }

}
?>