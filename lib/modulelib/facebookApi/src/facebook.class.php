<?php
/***
 * [WP:069] => CR:102
 * This class set facebook appId and secret key...
 */
class FacebookApi extends Facebook{
    
    public function __construct($returnUrl) {

        $config = array(
                  'appId'  => sfConfig::get('app_facebook_appId'),
                  'secret' => sfConfig::get('app_facebook_secretKey'),
                  'sitePath' => $returnUrl,
                );
        
        parent::__construct($config);
    }

}
?>