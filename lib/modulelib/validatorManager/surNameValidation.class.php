<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class SurNameValidation
{
  public function checkForSurNameValidation($arrSurNameParam,$selectedCardType){
    
    
    /**
     * [WP:084] => CR:122
     * Commenting app exclude array...
     */
    ##$excludeArr = sfConfig::get('app_excludeCard');
    $isPrivilegeUser = sfContext::getInstance()->getUser()->isPrivilegeUser();

    ## This variable contains text which need to exclude from application lastname i.e., surname...
    $excludeSurNameText = sfConfig::get('app_excludeSurNameText');
      
    ## Getting all application surname...
    $surnameArray = array();
    $surnameArray = $this->getSurnameArray();
    $card_holder_surname = trim($arrSurNameParam['last_name']);
    $card_holder_surname = $this->getAppLastName($card_holder_surname, $excludeSurNameText);

    $isSurNameMatch = $this->matchSurname($surnameArray, $card_holder_surname);         

    ## Getting fullname check variable...
    $fullname_check = sfConfig::get('app_fullname_check');
    
      if($fullname_check){
      /*
      * Getting all application first name and surname...
      * true means function return first name as well with surname...
      */
        $fullnameArray = array();
        $fullnameArray = $this->getSurnameArray($fullname = true);        
        ## if user input first name and middle name in first name field then explode will be benificial...
        list($firstName) = explode(' ',trim($arrSurNameParam['first_name']));

        $card_holder_surname = $this->getAppLastName(trim($arrSurNameParam['last_name']), $excludeSurNameText);
        $card_holder_fullname = trim($firstName) . ' ' . $card_holder_surname;

        $isFullNameMatch = $this->matchFullname($fullnameArray, $card_holder_fullname);
      }else{
        $isFullNameMatch = true;

      }    

      return array('isSurNameMatch'=>$isSurNameMatch,'isFullNameMatch'=>$isFullNameMatch);
  }

  /**
   * This function return first name, lastname or both from application (passport/visa)
   * @param <type> $fullname contains true or false...
   * @return <type> if $fullname is false that means function only return surname
   * else system return full name...
   */
  public function getSurnameArray($fullname = false)
  {
    $arrCartItems = sfContext::getInstance()->getUser()->getCartItems();

    ## This variable contains text which need to exclude from application lastname i.e., surname...
    $excludeSurNameText = sfConfig::get('app_excludeSurNameText');

    $surnameArray = array();
    foreach($arrCartItems as $items){
      if($items->getType() == 'Passport'){
        $passportAppObj = PassportApplicationTable::getInstance();
        $appObj = $passportAppObj->find($items->getAppId());


        ## Exploding Last Name...
        ## Only last word will be treated as Last Name.. i.e., "Singh" will be the last name in "Kumar Singh";
        $appLastName = trim($appObj->getLastName());
        $appLastName = $this->getAppLastName($appLastName, $excludeSurNameText);


        ## If full name require...
        if($fullname){
          ## if user input first name and middle name in first name field then explode will be benificial...
          list($firstName) = explode(' ',trim($appObj->getFirstName()));
          $surnameArray[] = trim($firstName) . ' ' . $appLastName;
        }else{
          $surnameArray[] = $appLastName;
        }
      }else if($items->getType() == 'Vap'){
        $vapAppObj = VapApplicationTable::getInstance();
        $appObj = $vapAppObj->find($items->getAppId());
        ## Exploding Last Name...
        ## Only last word will be treated as Last Name.. i.e., "Singh" will be the last name in "Kumar Singh";
        $appLastName = trim($appObj->getSurname());
        $appLastName = $this->getAppLastName($appLastName, $excludeSurNameText);


        ## If full name require...
        if($fullname){
          ## if user input first name and middle name in first name field then explode will be benificial...
          list($firstName) = explode(' ',trim($appObj->getFirstName()));
          $surnameArray[] = trim($firstName) . ' ' . $appLastName;
      }else{
          $surnameArray[] = $appLastName;
        }
      } else {
        $visaAppObj = VisaApplicationTable::getInstance();
        $appObj = $visaAppObj->find($items->getAppId());

         ## Exploding Last Name...
        ## Only last word will be treated as Last Name.. i.e., "Singh" will be the last name in "Kumar Singh";
        $appLastName = trim($appObj->getSurname());
        $appLastName = $this->getAppLastName($appLastName, $excludeSurNameText);


        ## If full name require...
        if($fullname){
          ## if user input first name and middle name in first name field then explode will be benificial...
          list($firstName) = explode(' ',trim($appObj->getOtherName()));
          $surnameArray[] = trim($firstName) . ' ' . $appLastName;
        }else{
          $surnameArray[] = $appLastName;
        }
      }
    }
    //echo "<pre>";print_r($surnameArray);die;
    return $surnameArray;
  }

  /**
   * This function will match card surname with all application's surname...
   * @param <type> $surnameArray contains all surname of applications.
   * @param <type> $surname4Match contains card surname (last name)...
   * @return <type>
   */

  public function matchSurname($surnameArray, $surname4Match)
  {
    $surnameCount = count($surnameArray);
    for($i = 0; $i < $surnameCount; $i++)
    {
      if(strtolower($surnameArray[$i]) != strtolower($surname4Match)){
        return false;
      }
    }
    return true;
  }


   /**
   * This function match atleast one application's full name with card full name...
   * @param <type> $fullnameArray contains all fullname of applications...
   * @param <type> $cardHolderFullName contains card full name (first name and last name)...
   * @return <type>
   */

  private function matchFullname($fullnameArray, $cardHolderFullName)
  {
    $fullnameCount = count($fullnameArray);
    for($i = 0; $i < $fullnameCount; $i++)
    {
      if(strtolower($fullnameArray[$i]) == strtolower($cardHolderFullName)){
        return true;
      }
    }
    return false;
  }

  public function getAppLastName($appLastName, $excludeSurNameText = array()){
    
    ## Removing predefined text from last name i.e., JR. Sr. Mr.
    foreach($excludeSurNameText AS $value){
        $appLastName = str_ireplace($value, '', $appLastName);
    }    

    ## Finally fetching applastname...
    $appLastName = explode(' ', trim($appLastName));    
    $lastName = end($appLastName);
    
    return $lastName;    
  }
  
}
?>
