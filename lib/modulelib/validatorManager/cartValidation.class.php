<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class CartValidation
{

  public $sfUser = NULL;
  public $sfController = NULL;
  public $requestId = NULL;

  function __construct($requestId=''){
      $this->sfUser = sfContext::getInstance()->getUser();
      $this->sfController = sfContext::getInstance()->getController();
      $this->requestId = $requestId;
  }


    /**function getCommonOption()
   *@purpose :get cart capacity by user
   *@param : N/A
   *@return :  array
   *@author : KSingh
   *@date : 30-05-2011
   */
  public  function getCommonOption($nisApplication,$appType,$aapId){
        $appArr = $nisApplication->toArray();
        $processingCountry = array();
        switch ($appType){
            case "Passport":
                $processingCountry[] = $appArr['processing_country_id'];
                break;
            case "Visa":
                $processingCountry[] = Doctrine::getTable('VisaApplicantInfo')->getProcessingCountry($aapId);
                break;
            case "Freezone":
                $processingCountry[] = 'NG';
                break;
            case "Vap":
                $processingCountry[] = Doctrine::getTable('VapApplication')->getProcessingCountry($aapId);
                break;
        }

        $this->cartItems = sfContext::getInstance()->getUser()->getCartItems();
        if(count($this->cartItems) > 0){
            foreach ($this->cartItems as $cartObj){
                switch ($cartObj->getType()){
                    case 'Passport':
                        $applicationArr = $cartObj->getApplication()->toArray();
                        (in_array($applicationArr['processing_country_id'], $processingCountry)) ? '' : ($processingCountry[] = $applicationArr['processing_country_id']);
                        break;
                    case "Visa":
                        $country = Doctrine::getTable('VisaApplicantInfo')->getProcessingCountry($cartObj->getAppId());
                        (in_array($country, $processingCountry)) ? '' : ($processingCountry[] = $country);
                        break;
                    case "Freezone":
                        $processingCountry[] = 'NG';
                        break;
                    case "Vap":
                        $country = Doctrine::getTable('VapApplication')->getProcessingCountry($cartObj->getAppId());
                        (in_array($country, $processingCountry)) ? '' : ($processingCountry[] = $country);
                        break;
                }
            }
        }

      /*
       * WP[084]=>CR123
       * To allow application of same processing country to be added into cart...
       */

      $uniqueArr = array_unique($processingCountry);
      if(count($uniqueArr) == 1){
                return true;
      }else{
          return false;
      }

//        $paymentMode = new PaymentModeManager();
//        $paymentModsArr = $paymentMode->getPaymentOptionArray($processingCountry);
//        if(count($paymentModsArr) > 0)
//        return 1;
//        else
//        return 0;
        
    }

  

    /**function checkUserCartCapacity()
   *@purpose :get cart capacity by user
   *@param : N/A
   *@return :  array
   *@author : KSingh
   *@date : 30-05-2011
   */
    public function checkUserCartCapacity(){

      $userId =  $userDetailObj = sfContext::getInstance()->getUser()->getGuardUser()->getId();
        //get user cart capacity
        $cartCapacity =array();
        $arrUserCartCapacity = UserTransactionLimitTable::getInstance()->getUserCartLimit($userId);
        if(!empty($arrUserCartCapacity) && count($arrUserCartCapacity) > 0){

            $arrProcessingCountry = $this->checkCartCapacityByProcessingCountry();

            if($arrUserCartCapacity[0]['cart_capacity'] != null){
                $cartCapacity['cart_capacity'] = $arrUserCartCapacity[0]['cart_capacity'];
            }else if($arrProcessingCountry['cart_capacity'] != null ){
                $cartCapacity['cart_capacity'] = $arrProcessingCountry['cart_capacity'];
            }


            if($arrUserCartCapacity[0]['cart_amount_capacity'] != null ){
                $cartCapacity['cart_amount_capacity'] = $arrUserCartCapacity[0]['cart_amount_capacity'];
            }else if($arrProcessingCountry['cart_amount_capacity'] != null ){
                $cartCapacity['cart_amount_capacity'] = $arrProcessingCountry['cart_amount_capacity'];
            }


            if($arrUserCartCapacity[0]['number_of_transaction'] != null ){
                $cartCapacity['number_of_transaction'] = $arrUserCartCapacity[0]['number_of_transaction'];
            }else if($arrProcessingCountry['cart_capacity'] != null ){
                $cartCapacity['number_of_transaction'] = $arrProcessingCountry['number_of_transaction'];
            }


            if($arrUserCartCapacity[0]['transaction_period'] != null ){
                $cartCapacity['transaction_period'] = $arrUserCartCapacity[0]['transaction_period'];
            }else if($arrProcessingCountry['transaction_period'] != null ){
                $cartCapacity['transaction_period'] = $arrProcessingCountry['transaction_period'];
            }

            return $cartCapacity;

        }else{

            $arrProcessingCountry = $this->checkCartCapacityByProcessingCountry();
            return $arrProcessingCountry;
        }


    }

    /**function checkCartCapacityByProcessingCountry1()
   *@purpose :get cart capacity by processing country
   *@param : N/A
   *@return :  array
   *@author : KSingh
   *@date : 12-05-2011
   */
    public function checkCartCapacityByProcessingCountry(){

        $arrCartItems = sfContext::getInstance()->getUser()->getCartItems();

        if (count($arrCartItems) >= 1) {
            $arrProcessingCountry = array();
            foreach ($arrCartItems as $items) {
                if ($items->getType() == 'Passport') {
                    $arrProcessingCountry[] = Doctrine::getTable('PassportApplication')->getProcessingCountry($items->getAppId());
                } elseif ($items->getType() == 'Freezone') {
                    $arrProcessingCountry[] = 'NG';
                } elseif ($items->getType() == 'Visa') {
                    $arrProcessingCountry[] = Doctrine::getTable('VisaApplicantInfo')->getProcessingCountry($items->getAppId());
                }elseif ($items->getType() == 'Vap') {
                    $arrProcessingCountry[] = Doctrine::getTable('VapApplication')->getProcessingCountry($items->getAppId());
                }
            }

            krsort($arrProcessingCountry);

            if(count($arrProcessingCountry) > 0){
                $arrCardType = Doctrine::getTable('CountryPaymentMode')->getCardType($arrProcessingCountry[count($arrProcessingCountry)-1]);
                if (!empty($arrCardType)) {
                    $arrCardType = $arrCardType;
                } else {
                    $arrCardType = Doctrine::getTable('CountryPaymentMode')->getCardType('ALL');
                }
            }
        } else {
            $arrCardType = Doctrine::getTable('CountryPaymentMode')->getCardType('ALL');
        }

        $cartCapacity = array();
        $cartCapacity['cart_capacity'] = $arrCardType[0]['cart_capacity'];
        $cartCapacity['cart_amount_capacity'] = $arrCardType[0]['cart_amount_capacity'];
        $cartCapacity['number_of_transaction'] = $arrCardType[0]['number_of_transaction'];
        $cartCapacity['transaction_period'] = $arrCardType[0]['transaction_period'];
        return $cartCapacity;
    }


    public function cartLimitMessage(){

        $arrCartCapacity = array();
        $arrCartCapacityLimit = $this->checkUserCartCapacity();
        if (!empty($arrCartCapacityLimit) && count($arrCartCapacityLimit)) {
            $arrCartCapacity = $arrCartCapacityLimit;
        }        
        $cartObj = new cartManager();
        $CapacityFlg = false;
        $amtCapacityFlg = false;

        $arrCartItems = $this->sfUser->getCartItems();

        if (count($arrCartItems) > 0) {
            if (count($arrCartItems) > $arrCartCapacity['cart_capacity']) {
                $CapacityFlg = true;
            }

            if (count($arrCartItems) > 1) {
                if ($cartObj->getCartCurrValue() > $arrCartCapacity['cart_amount_capacity']) {
                    $amtCapacityFlg = true;
                }
            }

            if ($CapacityFlg && $amtCapacityFlg) {
                $this->sfUser->setFlash('notice', sprintf('Your CART limit has been changed, You can process ' . $arrCartCapacity['cart_capacity'] . ' application(s) of amount $' . $arrCartCapacity['cart_amount_capacity'] . ' at a time.', false));
                $this->sfController->redirect('cart/list');
            } else if ($CapacityFlg) {
                $this->sfUser->setFlash('notice', sprintf('Your CART limit has been changed, You can process ' . $arrCartCapacity['cart_capacity'] . ' application(s) at a time.', false));
                $this->sfController->redirect('cart/list');
            } else if ($amtCapacityFlg) {
                $this->sfUser->setFlash('notice', sprintf('Your CART limit has been changed, You can process amount $' . $arrCartCapacity['cart_amount_capacity'] . ' at a time.', false));
                $this->sfController->redirect('cart/list');
            }
        }
    }





    public function emptyCartMessage(){
        $arrCartItems = $this->sfUser->getCartItems();        
        if (count($arrCartItems) == 0) {
            $this->sfUser->setFlash('notice', 'Applications has been already processed. ');            
            $this->sfController->redirect('cart/list');            
        }
    }

    public function checkIfAlreadyPaid($appType, $appId) {

        $appIDArray = array();
        switch (strtolower($appType)) {
            case 'passport';
                $appKey = 'passport_id';
                break;
            case 'visa';
                $appKey = 'visa_id';
                break;
            case 'freezone';
                $appKey = 'freezone_id';
                break;
            case 'vap';
                $appKey = 'visa_arrival_program_id';
                break;
        }

        if (isset($appKey)) {
            $appIDArray[$appKey] = $appId;
            return $cartDetails = CartItemsTransactionsTable::getInstance()->getApplicationDetails($appIDArray);
        } else {
            return;
        }
    }

    public function alreadyPaidMessage(){

        $arrCartItems = $this->sfUser->getCartItems();
        
        ## Check if application is already paid at NIS
        foreach ($arrCartItems as $items) {
            $isAlreadyPaid[] = $this->checkIfAlreadyPaid($items->getType(), $items->getAppId());
        }
        $strMsg = '';
        foreach ($isAlreadyPaid as $key => $val) {
            if ($val['status'] == 'Paid' || $val['status'] == 'Approved') {
                $strMsg .= $val['id'] . ' ,';
            }
        }

        if (substr($strMsg, 0, -1) != '') {
            $this->sfUser->setFlash('notice', 'Application Id(s): ' . substr($strMsg, 0, -1) . 'has been already processed. Please click remove link to remove this application from cart.');
            $this->sfController->redirect('cart/list');
        }
    }


    public function applicationAlreadyProcessMessage(){

        ## Check if application already processed
        $orderRequest = Doctrine::getTable('OrderRequestDetails')->find($this->requestId);
        //get Application on NIS
        $appDetails = CartItemsTransactionsTable::getInstance()->getRetryIdDetailsOnNis($orderRequest->getRetryId());        
        $appType = $appDetails[0]['app_type'];
        $appID = $appDetails[0]['id'];
        switch ($appType) {
            case 'passport': $appType = 'P';
                break;
            case 'visa': $appType = 'V';
                break;
            case 'freezone': $appType = 'F';
                break;
            case 'vap': $appType = 'vap';
                break;
        }
        $paymentRequestRecord = Doctrine::getTable('CartItemsTransactions')->getPaymentRequestMoneyOrder($appID, $appType);
        foreach ($paymentRequestRecord as $key => $items) {
            $arrItemId[$key] = $items['id'];
        }
        if (isset($arrItemId)) {

            $cartItemInfo = Doctrine::getTable('CartItemsTransactions')->getCartItemAllIdInfo($arrItemId);

            foreach ($cartItemInfo as $key => $item) {
                $arrCartId[$key] = $item['cart_id'];
            }
            if (isset($arrCartId)) {
                $cartTrakingInfo = Doctrine::getTable('CartTrackingNumber')->getTrackingNumber($arrCartId);
                if (empty($cartTrakingInfo)) {
                    $cartTrakingInfo = Doctrine::getTable('WireTrackingNumber')->getTrackingNumber($arrCartId);
                }
            }
        }

        if (!empty($cartTrakingInfo) && count($cartTrakingInfo) > 0) {
            $cartItems = $this->sfUser->clearCart();
            $userDetailObj = $this->sfUser->getGuardUser()->getUserDetail();
            $userDetailObj->setCartItems(NULL);
            $userDetailObj->save();
            $this->sfUser->setFlash('notice', 'Application has already been associated with money order. ');
            $this->sfController->redirect('cart/list');
        }
    }


    public function gotoApplicantVaultValidations(){
        
        //end:Kuldeep
        $arrCartItems = $this->sfUser->getCartItems();
        
        $isPrivilegeUser = $this->sfUser->isPrivilegeUser();
        
        if(!$isPrivilegeUser){
            ## System generate error message if cart limit exceed...
            $this->cartLimitMessage();
        }//End of if(!$isPrivilegeUser){...

        ## System generate error message if cart empty...
        $this->emptyCartMessage();

        ## System generate error message if application already paid...
        $this->alreadyPaidMessage();

        
        // *  *  Commented by aswani as all tracking checks are moving to when user try to generate tracking number... *
        ## System generates error message if application already in process...
        $this->applicationAlreadyProcessMessage();


        /**
         * Bug# 35737
         * [WP: 087] => CR:126
         * Checking application modified or not...
         */
        $this->checkCartApplicationType();

        /**
         * Bug# 35737
         * [WP: 087] => CR:126         * 
         * Checking order amount must be greater then 0...
         * If not then go to cart page with appropriate message...
         */
        $this->checkOrderAmount();
        
    }

    /**
     *
     * @param <type> $application_id
     * @param <type> $application_type
     * @return <type>
     * This function check whether application is processed with money order...
     */
    public function isAppProcessed($application_id, $application_type)
     {
         $arrIPay4MeRequest = CartItemsTransactionsTable::getInstance()->getPaymentRequest($application_id, $application_type);

         $arrItemId = array();
         for($i=0;$i<count($arrIPay4MeRequest);$i++)
         {
             $arrItemId[$i] = $arrIPay4MeRequest[$i]['id'];
         }

         $arrCartDetail = array();
         if(!empty($arrItemId))
         {
            $arrCartDetail = IPaymentRequestTable::getInstance()->getCartDetails($arrItemId);
         }

         $arrCartIds = array();
         for($j=0;$j<count($arrCartDetail);$j++)
         {
             $arrCartIds[$j] = $arrCartDetail[$j]['cart_id'];
         }

         $arrTrackingDeatil = CartTrackingNumberTable::getInstance()->getTrackingNumber($arrCartIds);
         if(!empty($arrTrackingDeatil))
         {
             //if not empty means app is processed
             return true;
         }
         else
         {
             //if not empty means app is processed
             $arrTrackingDeatilWT = WireTrackingNumberTable::getInstance()->getTrackingNumber($arrCartIds);
             if(!empty($arrTrackingDeatilWT))
             {
                 //if not empty means app is processed
                 return true;
             }
             else
             {
                return false;
             }
         }
     }

     /**
     * Bug# 35737
     * [WP: 087] => CR:126
     * Checking order amount must be greater then 0...
     * If not then go to cart page with appropriate message...
     */
     public function checkOrderAmount(){
        $orderReqDetailObj = Doctrine::getTable('OrderRequestDetails')->findById($this->requestId);
        if(count($orderReqDetailObj) > 0){
            if((int)$orderReqDetailObj->getFirst()->getAmount() <= 0){
                $this->sfUser->setFlash('notice', 'Please remove application(s) from cart and add again same application(s) to make payment.');
                $this->sfController->redirect('cart/list');
                die;
            }
        }
     }//End of public function checkOrderAmount(){...


     /**
      * [WP: 087] => CR: 126
      * This function check cart application type with database...
      * If both are not equal then go to cart page with appropriate message...
      */
     public function checkCartApplicationType(){
        ## get all cart items
        $cartItems = $this->sfUser->getCartItems();        
        $strMsg = '';
        foreach($cartItems as $value){
            ## Creating variable which contains table name...
            $appType = strtolower($value->getType());
            if ($appType == 'visa' || $appType == 'freezone') {                
                ## Fetching application current amount...
                $app = Doctrine::getTable('VisaApplication')->find($value->getAppId());                
                if($app->getVisacategoryId() == 29){
                    $modifiedAppType = 'visa';
                }else{
                    $modifiedAppType = 'freezone';
                }                
                if($appType != $modifiedAppType){                    
                    $strMsg .= $value->getAppId() . ' ,';
                }
            }//End of if ($appType == 'visa' || $appType == 'freezone') {...
        }//End of foreach($cartItems as $value){...

        if($strMsg != ''){           
            if (substr($strMsg, 0, -1) != '') {
                $this->sfUser->setFlash('notice', 'Application Id(s): ' . substr($strMsg, 0, -1) . 'has been modified. Please click remove link to remove this application from cart and add it again.');
                $this->sfController->redirect('cart/list');
                die;
            }
        }        

     }//End of public function checkCartApplicationType(){...
    

}
?>
