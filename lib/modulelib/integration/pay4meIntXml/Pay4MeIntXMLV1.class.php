<?php

/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Pay4MeIntXMLV1 {

    private function setXMLheader($doc,$xmlType){
        $root = $doc->createElement($xmlType);
        $root->setAttribute ( "xmlns:xsi", 'http://www.w3.org/2001/XMLSchema-instance' );
        $root->setAttribute ( "xmlns", 'http://www.ipay4me.com/schema/ipay4meorder/v1' );
        $root->setAttribute ( "xsi:schemaLocation", 'http://www.ipay4me.com/schema/ipay4meorder/v2 ipay4meorderV1.xsd' );
        return $root;
    }
   public function generatePaymentNotificationXMLV1($orderDetailId) {

        $logger=sfContext::getInstance()->getLogger();
        $doc = new DomDocument('1.0');
        $doc->formatOutput = true;
        $xmlType='payment-notification';
        $root = $this->setXMLheader($doc,$xmlType);
        $root = $doc->appendChild($root);
        $mainElm=$this->getPayNotificationContent($orderDetailId,$doc);
        $root->appendChild($mainElm);

        $xmldata = $doc->saveXML();    //echo $xmldata;die;
        ///////////////Logging   ////////////////////////////////////////////////////
        //
        //////////////////////////////////////////////////////////////////////////////

        //vallidate the XML
        $xdoc = new DomDocument;
        // $logger->info($xmldata);

        $isLoaded = $xdoc->LoadXML($xmldata);

    if($isLoaded)
           { 
            //validate the XML
            $xmlValidatorObj = new ValidatePay4meIntXML() ;
            $isValid = $xmlValidatorObj->validateXML($xdoc, "V1") ;
            if($isValid)
               return $xmldata;
             else
                 return false;
           }
      else
      {
               return false;
      }


  }
  private function getPayNotificationContent($orderDetailId,$doc) {

        
        //get all the details of the transaction_no from database
        
        $payForMeObj = Pay4MeIntServiceFactory::getService("V1");
        $order_details = Doctrine::getTable('OrderRequestDetails')->find($orderDetailId);

       // $TDetails = $payForMeObj->getNotificationTransactionDetails($orderDetailId);

      //  $responseObj = $TDetails->getFirst();
      //  $order_details = $responseObj->getRequest()->getOrderRequestDetails();
        $transactionNumber = $order_details->getOrderRequest()->getTransactionNumber();

        /**
         * [WP: 113] => CR: 159
         * Fetching applciation details...
         */
        $transactionServiceChargeObj = Doctrine::getTable('TransactionServiceCharges')->getApplicationDetailsByORDId($orderDetailId, $status = '0');      

        

        $orderObj = Doctrine::getTable('ipay4meOrder')->findSuccessfulPaymentDetail($orderDetailId);
        $order_id = $orderObj['order_number'];
       
        if($order_details) {

            $merchant = $doc->createElement("merchant");
            $merchant->setAttribute ( "id", $order_details->getMerchantId() );
           
            $item_number = $doc->createElement( "order");
            $item_number->setAttribute ( "number", $order_id);

            //transacction number
            $transaction_number = $doc->createElement("item-number");
            $transaction_number->appendChild($doc->createTextNode($transactionNumber));

           
            $retry_id = $doc->createElement("retry-id");
            $retry_id->appendChild($doc->createTextNode($order_details->getRetryId()));

            //payment Information
            $payment_information = $doc->createElement("payment-information");
            $amount = $doc->createElement("amount");
            $amount->appendChild($doc->createTextNode($order_details->getAmount()));
            $currency = $doc->createElement("currency");
            $currency->appendChild($doc->createTextNode(sfConfig::get('app_usd_currency')));



            
            $updatedAt=$this->convertDbDateToXsdDate($order_details->getPaidDate());
            $payment_date = $doc->createElement("payment-date");
            $payment_date->appendChild($doc->createTextNode($updatedAt));

            //get payment mode
            $paymentMode = $this->getPaymentMode($orderObj['gateway_id']);
            
            $mode = $doc->createElement("mode");
            $mode->appendChild($doc->createTextNode($paymentMode));


            $status = $doc->createElement("status");
            
            $payment_status = $this->getPaymentStatus($transactionNumber) ;
            $paymentStatusCode=$payment_status['code'];
            $paymentStatusDescription=$payment_status['description'];

            $code = $doc->createElement("code");
            $code->appendChild($doc->createTextNode($paymentStatusCode));
          
            $description = $doc->createElement("description");
            $description->appendChild($doc->createTextNode($paymentStatusDescription));
            
            $status->appendChild($code);
            $status->appendChild($description);

            //start appending the child to parent
           
           
            $payment_information->appendChild($amount);
            $payment_information->appendChild($currency);
            $payment_information->appendChild($payment_date);
            $payment_information->appendChild($mode);
            $payment_information->appendChild($status);

            /**
             * [WP: 113] => CR: 159
             * Adding application-count and application information node...
             */            

            $application_information = $doc->createElement('application-information');

            $i=1;
            foreach($transactionServiceChargeObj AS $transObj){

                $application_type = strtolower($transObj->app_type);
                if($application_type == 'freezone'){
                    $visaObj = Doctrine::getTable('VisaApplication')->getVisaInfoById($transObj->app_id);
                    if(count($visaObj)){
                        if($visaObj[0]['visacategory_id'] == '101'){
                            $application_type = 'entry freezone';
                        }
                    }
                }
                

                $application = $doc->createElement('application');
                $application->setAttribute ( "id", $i++ );

                $app_id = $doc->createElement('app-id');
                $app_id->appendChild($doc->createTextNode($transObj->app_id));

                $app_type = $doc->createElement('app-type');
                $app_type->appendChild($doc->createTextNode($application_type));

                $ref_no = $doc->createElement('ref-no');
                $ref_no->appendChild($doc->createTextNode($transObj->ref_no));

                $app_amount = $doc->createElement('app-amount');
                $app_amount->appendChild($doc->createTextNode($transObj->app_amount));

                $transaction_charges = $doc->createElement('transaction-charges');
                $transaction_charges->appendChild($doc->createTextNode($transObj->service_charge));

                $app_convert_amount = $doc->createElement('app-convert-amount');
                $app_convert_amount->appendChild($doc->createTextNode($transObj->app_convert_amount));

                $app_convert_transaction_charges = $doc->createElement('app-convert-transaction-charges');
                $app_convert_transaction_charges->appendChild($doc->createTextNode($transObj->app_convert_service_charge));

                $application->appendChild($app_id);
                $application->appendChild($app_type);
                $application->appendChild($ref_no);
                $application->appendChild($app_amount);
                $application->appendChild($transaction_charges);
                $application->appendChild($app_convert_amount);
                $application->appendChild($app_convert_transaction_charges);

                $application_information->appendChild($application);                
                
            }//End of foreach($transactionServiceChargeObj AS $transObj){...

            

            $item_number->appendChild($transaction_number);
            $item_number->appendChild($retry_id);
            $item_number->appendChild($payment_information);

            /**
             * [WP: 113] => CR: 159
             * Adding application-count and application information node...
             */          
            if(count($transactionServiceChargeObj)){
                $item_number->appendChild($application_information);
            }
            
            $merchant->appendChild($item_number);
            
            
           return $merchant;
        }
        else{
            return false;
        }


        //   return  $doc->saveXML();

    }

    protected function convertDbDateToXsdDate($dbDate) {
            $dtokens=explode(' ', $dbDate);
            $xdate = implode('T',$dtokens);
            return $xdate;
        }
    private function getPaymentStatus($transaction_no)
    {
        return array('code'=>0, 'description'=>'Payment Successfull') ;
    }




     public function setResponseHeader($merchantId){
        if(trim($merchantId)){

            $merchantTableObj = MerchantTable::getInstance();
            $merchantDetail = $merchantTableObj->getMerchantDetailsById($merchantId) ;
            unset($merchantTableObj);
            $header = "";
            if(count($merchantDetail)){
                $merchantCode = $merchantDetail[0]['merchant_code'];
                $merchantKey = $merchantDetail[0]['merchant_key'];
                $merchantAuthString = $merchantCode .":" .$merchantKey;
                // $logger->info("auth string: $merchantAuthString");
                $merchantAuth = base64_encode($merchantAuthString) ;

                $header['Authorization'] = "Basic $merchantAuth";
                $header['Content-Type'] =  "application/xml;charset=UTF-8";
                $header['Accept'] = "application/xml;charset=UTF-8";
            }
            $this->setHeader($header);
        }
    }

    private function setHeader($header){
        foreach($header as $key=>$val){
            sfContext::getInstance()->getResponse()->setHttpHeader("{$key}", $val);
        }
    }


     public function generatePay4MeIntOrderRedirectV1($url) {


        $doc = new DomDocument('1.0');
        $doc->formatOutput = true;
        $root = $doc->createElement('order-redirect');
        $root->setAttribute ( "xmlns:xsi", 'http://www.w3.org/2001/XMLSchema-instance' );
        $root->setAttribute ( "xmlns", 'http://www.ipay4me.com/schema/ipay4meorder/v1' );
        $root->setAttribute ( "xsi:schemaLocation", 'http://www.ipay4me.com/schema/ipay4meorder ipay4meorderV1.xsd' );
        $root = $doc->appendChild($root);
        $redirect_url = $doc->createElement("redirect-url");
        $redirect_url->appendChild($doc->createTextNode($url));
        $root->appendChild($redirect_url);
        $xmldata = $doc->saveXML();

        //vallidate the XML
        $xdoc = new DomDocument;
        $isLoaded = $xdoc->LoadXML($xmldata);


        //validate the XML
        $xmlValidatorObj = new validatePay4meIntXML() ;
        $isValid = $xmlValidatorObj->validateXML($xdoc, "V1") ;
        if($isValid) {
            return $xmldata;
        }
        else {
            return false;       }

    }
    
    public function generateErrorNotification($error_code, $desc) {
        $doc = new DomDocument('1.0');
        $doc->formatOutput = true;
        $root = $doc->createElement('error');
        $root->setAttribute ( "xmlns:xsi", 'http://www.w3.org/2001/XMLSchema-instance' );
        $root->setAttribute ( "xmlns", 'http://www.ipay4me.com/schema/ipay4meorder/v1' );
        $root->setAttribute ( "xsi:schemaLocation", 'http://www.ipay4me.com/schema/ipay4meorder/v1 ipay4meorderV1.xsd' );
        $root = $doc->appendChild($root);
        $code = $doc->createElement("error-code");
        $code->appendChild($doc->createTextNode($error_code));
        $description = $doc->createElement("error-message");
        $description->appendChild($doc->createTextNode($desc));
        $root->appendChild($code);
        $root->appendChild($description);
        $xmldata = $doc->saveXML();

        //vallidate the XML
        $xdoc = new DomDocument;
        $isLoaded = $xdoc->LoadXML($xmldata);

        //validate the XML
        $xmlValidatorObj = new ValidatePay4meIntXML() ;
        $isValid = $xmlValidatorObj->validateXML($xdoc, "V1") ;
        if($isValid) {
            return $xmldata;
        }
        else {
            return false;
        }
    }

    private function getPaymentMode($gateway_id){
        switch($gateway_id){
            case '1':
               return sfConfig::get('app_payment_mode');
               break;
            case '2':
               return sfConfig::get('app_payment_mode');
               break;
            case '3':
               return sfConfig::get('app_payment_mode');
               break;
            case '4':
               return sfConfig::get('app_payment_mode');
               break;
            case '5':
               return sfConfig::get('app_payment_mode');
               break;
            case '6':
               return 'money-order';
               break;
            default:
                return sfConfig::get('app_payment_mode');
                break;

        }
        
        
    }

}
?>
