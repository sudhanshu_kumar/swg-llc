<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of backendPaymentManagerclass
 *
 * @author akumar1
 */
class backendPaymentManager {
  //put your code here

  public $request_details;
  public $request_details_id;
  public $order_request_id;
  public $userId;
  public $vault;
  public $amount;
  public $cardDetailsArr;
  public static $version = 'V1';
  public $browserInstance ;


  public function __construct($request_details, $userId, $vault, $cardDetailsArr)
  {
    $this->request_details = $request_details;
    $this->userId = $userId;
    $this->vault = $vault;
    $this->amount = $this->request_details->getAmount();
    $this->request_details_id = $this->request_details->getId();
    $this->order_request_id = $this->request_details->getOrderRequestId();
    $this->cardDetailsArr = $cardDetailsArr;
  }
  public function doBackendPayment()
  {
    $gateway_id = 1;

    //generating bill
    $billObj = new billManager();
    $billObj->addBill($this->order_request_id);
    //payment part
    //update payment user
    $this->request_details->setUserId($this->userId);
    $this->request_details->save();

    $orderObj = $this->createNewOrder($gateway_id);
    $order_id = $orderObj->getId();
    $orderNumber = $orderObj->getOrderNumber();
    $graypay_request_id = $this->storeGraypayRequest($orderNumber);

    $process = $this->processPayment($orderNumber);
    if($process)
    {
      $this->storeGraypayResponse($graypay_request_id, $process);

      $updateOrder = $this->updateOrderRequest($order_id, $process);

      $updateOrder = $this->updateSplitAmount($gateway_id);
      return $result = $this->paymentSuccess($orderNumber);
    }else{
      return $result['status'] = 'success';
    }

  }

  public function createNewOrder($gateway_id)
  {
    $orderManagerObj = orderServiceFactory::getService(self::$version) ;
    $orderNumber = $orderManagerObj->getOrderNumber();

    $orderObj = new ipay4meOrder();
    $orderObj->setOrderNumber($orderNumber);
    $orderObj->setGatewayId($gateway_id);
    $orderObj->setOrderRequestDetailId($this->request_details_id);
    $orderObj->setCardFirst($this->cardDetailsArr[0]);
    $orderObj->setCardLast($this->cardDetailsArr[1]);
    $orderObj->setCardType($this->cardDetailsArr[2]);
    $orderObj->setCardLen($this->cardDetailsArr[3]);
    $orderObj->setCardHolder($this->cardDetailsArr[4]);
    $orderObj->save();
    return $orderObj;
  }

  private function storeGraypayRequest($orderNumber)
  {
    $card_len = $this->cardDetailsArr[3];
    $card_type = $this->cardDetailsArr[2];
    $card_first = $this->cardDetailsArr[0];
    $card_last = $this->cardDetailsArr[1];
    $card_holder = $this->cardDetailsArr[4];
    $card_exp_month = $this->cardDetailsArr[5];
    $card_exp_year = $this->cardDetailsArr[6];
    $ip_address = $ipReq = (empty($_SERVER['HTTP_X_FORWARDED_FOR'])?(empty($_SERVER['HTTP_CLIENT_IP'])?$_SERVER['REMOTE_ADDR']:$_SERVER['HTTP_CLIENT_IP']):$_SERVER['HTTP_X_FORWARDED_FOR']);

    $payEasyReponse = new EpGrayPayRequest ();
    $payEasyReponse->setTranDate(date('Y-m-d H:i:s'));
    $payEasyReponse->setRequestIp(sfConfig::get('app_pay_easy_request_IP'));
    $payEasyReponse->setAmount($this->amount);
    $payEasyReponse->setCardFirst($card_first);
    $payEasyReponse->setCardLast($card_last);
    $payEasyReponse->setCardLen($card_len);
    $payEasyReponse->setCardType($card_type);
    $payEasyReponse->setCardHolder($card_holder);
    $payEasyReponse->setExpiryMonth($card_exp_month);
    $payEasyReponse->setExpiryYear($card_exp_year);
    $payEasyReponse->setOrderNumber($orderNumber);
    $payEasyReponse->setIpAddress($ip_address);

    $payEasyReponse->save();
    return $Request_id = $payEasyReponse->getId();

  }
  public function storeGraypayResponse($request_id, $responseArr)
  {
    $grayPayReponse = new EpGrayPayResponse();
    $grayPayReponse->setGraypayReqId($request_id);
    $grayPayReponse->setResponse($responseArr['response']);
    $grayPayReponse->setResponseText($responseArr['responsetext']);
    $grayPayReponse->setResponseCode($responseArr['response_code']);
    $grayPayReponse->setTransactionid($responseArr['transactionid']);
    $grayPayReponse->setAuthcode($responseArr['authcode']);
    $grayPayReponse->setType($responseArr['type']);
    $grayPayReponse->setOrderid($responseArr['orderid']);
    $grayPayReponse->setAvsresponse($responseArr['avsresponse']);
    $grayPayReponse->setCvvresponse($responseArr['cvvresponse']);

    $grayPayReponse->save();
  }

  private function processPayment($orderNumber)
  {
    $manadatoryString = "username=".sfConfig::get('app_graypay_username')."&password=".sfConfig::get('app_graypay_password') ;
    $vaultString = "&amount=".$this->amount."&customer_vault_id=".$this->vault."&orderid=".$orderNumber ;
    $requestString = $manadatoryString.$vaultString;

    $this->createLogData($requestString,'paymentRequest-'.$orderNumber, 'backendPaymentLog');

    $url = sfConfig::get('app_graypay_url');

    $browser = $this->getBrowser() ;
    $browser->post($url, $requestString);
    $code = $browser->getResponseCode();

    if ($code != 200 ) {
      $respMsg = $browser->getResponseMessage();
      return false;
    }
    $respText = $browser->getResponseText();
    $responseArr = $this->extractResponse($respText) ;

    $this->createLogData($respText, 'paymentResponse-'.$orderNumber, 'backendPaymentLog');

    if(strcmp('100', $responseArr['response_code']) == 0){
      return $responseArr ;
    }
    return false;
  }

  public function createLogData($xmlData,$nameFormate,$parentLogFolder='', $appendTime=true, $append = false)
  {
    $path = $this->getLogPath($parentLogFolder);
    if($appendTime) {
      $file_name = $path."/".($nameFormate.'-'.date('Y_m_d_H:i:s')).".txt";
    }else {
      $file_name = $path."/".$nameFormate.".txt";
    }
    if($append) {
      @file_put_contents($file_name, $xmlData, FILE_APPEND);
    }else {
      $i=1;
      while(file_exists($file_name)) {
        $file_name = $path."/".($nameFormate.'-'.date('Y_m_d_H:i:s-')).$i.".txt";
        $i++;
      }
      @file_put_contents($file_name, $xmlData);
    }
  }

  public function getLogPath($parentLogFolder) {

    $logPath =$parentLogFolder==''?'/grayPaylog':'/'.$parentLogFolder;
    $logPath =  sfConfig::get('sf_log_dir').$logPath.'/'.date('Y-m-d');

    if(is_dir($logPath)=='') {
      $dir_path=$logPath."/";
      mkdir($dir_path,0777,true);
      chmod($dir_path, 0777);
    }
    return $logPath;
  }

  public function updateOrderRequest($order_id,$paymentResponseArray,$validationNumber='') {
    $orderObj = Doctrine::getTable('ipay4meOrder')->find($order_id);

    if($paymentResponseArray['response'] == 1) {
      $payment_status = 0;
      //updating bill table
      $billDetails = $orderObj->getOrderRequestDetails()->getOrderRequest()->getBill();
      $billDetailsToUpdate = $billDetails->getfirst();
      $billDetailsToUpdate->setStatus('paid');
      $billDetailsToUpdate->save();
    }
    else {
      $payment_status = 1;
    }
    $orderObj->setPaymentStatus($payment_status);
    if(!empty($validationNumber)) {
      $orderObj->setValidationNumber($validationNumber);
    }
    $orderObj->setResponseCode($paymentResponseArray['response_code']);

    $orderObj->setResponseText($paymentResponseArray['responsetext']);
    $orderObj->save();
    return $orderObj;
  }

  public function updateSplitAmount($gateway_id)
  {
    $OrderSplitAllObj= Doctrine::getTable('OrderRequestSplit')->findByOrderDetailId($this->request_details_id);

    $transctionChargeObj= Doctrine::getTable('TransactionCharges')->findByGatewayId($gateway_id);

    if(is_object($transctionChargeObj)) {
      if("percentage" == $transctionChargeObj->getfirst()->getSplitType()) {
        foreach($OrderSplitAllObj as $orderObj) {
          $transCharge = round(($orderObj->getAmount() * $transctionChargeObj->getfirst()->getTransactionCharges())/100) ;
          $itemFee = $orderObj->getAmount() - $transCharge;
          $orderObj->setItemFee($itemFee);
          $orderObj->setTransactionCharges($transCharge);
          $orderObj->save();
        }
      }
    }
    return $orderObj;
  }

  public function paymentSuccess($order_number) {
    $userObj = sfContext::getInstance()->getUser();
    //clear the cart
    $cartObj = new cartManager();
    $cartObj->emptyCart();
    $userObj->saveCartInDb();


    $result = array();
    $userId = $userObj->getGuardUser()->getId();

    $orderDetailId = Doctrine::getTable('ipay4meOrder')->getOrderRequestDetails($order_number);

    $updatePayment = Doctrine::getTable('OrderRequestDetails')->updatePayment($orderDetailId, $userId);

    $notificationUrl = "notification/PaymentNotificationV1" ;
    $taskId = EpjobsContext::getInstance()->addJob('PaymentNofication',$notificationUrl, array('order_detail_id' => $orderDetailId));
    sfContext::getInstance()->getLogger()->debug("sceduled job with id: $taskId");


    $result['status'] = 'success';
    $result['order_number'] = $order_number;
    return $result;
  }

  protected function getBrowser() {
    if(!$this->browserInstance) {
      $this->browserInstance = new sfWebBrowser(array(), 'sfCurlAdapter',
        array('SSL_VERIFYPEER' => false, 'SSL_VERIFYHOST' => false));
    }
    return $this->browserInstance;
  }

  private function extractResponse($res){
    parse_str($res, $output);
    return $output ;
  }
}
?>
