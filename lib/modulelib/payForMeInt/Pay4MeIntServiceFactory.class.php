<?php
class Pay4MeIntServiceFactory{
/**
  *
  * @param <type> $type
  * @return empService the service implementation
  */
 public static function getService($version='') {

     $pay4MeIntManager = "Pay4MeInt" . strtoupper($version) . "Manager" ;
     $pay4MeIntManagerObj = new $pay4MeIntManager;    
     return $pay4MeIntManagerObj;
 }
}
?>