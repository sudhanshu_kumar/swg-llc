<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Accounting {

  public $user_account_number;
  public $ledger_description;
  public $transaction_description;
  public $gateway_description;
  public $sms_description;

  //public $ledger_description;

  public function getAccountingHolderObj($description, $amount, $account_id, $isCleared, $entry_type) {
    return $attr = new EpAccountingAttributeHolder($description, $amount, $account_id, $isCleared, $entry_type);
  }

  public function getRechargeAccounts($gateway_id, $user_id, $total_amount, $flat_recharge_amount) {
    $acct_number = $this->getUserAccountNumber($user_id);
    

    sfContext::getInstance()->getConfiguration()->loadHelpers(array('EPortal'));

    $ewalletManager = new EwalletManager() ;
    $ipay4me_recharge_amount = $ewalletManager->getServiceCharge($gateway_id, 'ewallet_recharge', $flat_recharge_amount) ;
    $sms_charge = 0;
    $sms_charge = $this->getSMSCharges($user_id);
    $ewallet_amount = $total_amount - $ipay4me_recharge_amount - $sms_charge;
    $amount_formatted = format_amount($ewallet_amount,1);

    $this->ledger_description = EpAccountingDescription::getFomattedMessage(EpAccountingDescription::$RECHARGE_EWALLET, array('account_number' => $acct_number,'amount'=>$amount_formatted));

  //  $this->ledger_description = EpAccountingDescription::getFomattedMessage(EpAccountingDescription::$RECHARGE_EWALLET, array('account_number' => $acct_number,'amount'=>$amount_formatted));

    $attrArray = array();

    //credit logged in user ewallet Account
    $ewallet_amount_cents = convertToCents($ewallet_amount);
    $attr_ewallet = $this->updateEwalletUserAccount($ewallet_amount_cents, $user_id,  EpAccountingConstants::$ENTRY_TYPE_CREDIT);
    $attrArray[] = $attr_ewallet;


    //getTransactionCharges
    $transactionChargeObj= Doctrine::getTable('TransactionCharges')->getTransactionCharges($gateway_id);

    $ipay4me_ewallet_amount = 0;
    $gateway_charge = 0;
    $ipay4me_charges = 0;
    $transaction_charge = 0;
    $sms_charge = 0;
    $sms_charge = $this->getSMSCharges($user_id);
    

    if($transactionChargeObj) {
      $transaction_charge = $transactionChargeObj->getFirst()->getTransactionCharges();
      $gateway_charge = ($transaction_charge*$total_amount)/100;
      if($sms_charge > 0){
        $sms_charge = $sms_charge - ($transaction_charge*$sms_charge)/100;
      }
      $ipay4me_charges = $ipay4me_recharge_amount-(($transaction_charge*$ipay4me_recharge_amount)/100);     
    }

    $ipay4me_ewallet_amount = $ewallet_amount-(($transaction_charge*$ewallet_amount)/100);

    if($ipay4me_ewallet_amount > 0 ) {
    //reacharge iPay4me Account
      $ipay4me_ewallet_amount_cents = convertToCents($ipay4me_ewallet_amount);
      $attr_ipay4me = $this->updateIpay4meAccount($ipay4me_ewallet_amount_cents, EpAccountingConstants::$ENTRY_TYPE_CREDIT);
      $attrArray[] = $attr_ipay4me;
    }

     if($gateway_charge > 0 ) {
    //recharge Gateway Account
      $this->gateway_description = EpAccountingDescription::getFomattedMessage(EpAccountingDescription::$GATEWAY_CHARGE, array('account_number' => $acct_number,'amount'=>$amount_formatted));
      $gateway_charge_cents = convertToCents($gateway_charge);
      $attr_gateway = $this->creditGatewayAccount($gateway_charge_cents, $gateway_id);
      $attrArray[] = $attr_gateway;
    }

    if($ipay4me_charges > 0 ) {
    //reacharge iPay4me Account
      $ipay4me_charges_cents = convertToCents($ipay4me_charges);
      $this->transaction_description = EpAccountingDescription::getFomattedMessage(EpAccountingDescription::$TRANSACTION_CHARGE, array('account_number' => $acct_number,'amount'=>$amount_formatted));
      $attr_ipay4me = $this->updateIpay4meAccount($ipay4me_charges_cents, EpAccountingConstants::$ENTRY_TYPE_CREDIT,"transaction_description");
      $attrArray[] = $attr_ipay4me;
    }

    //getSMSCharges
    
    if($sms_charge > 0) {
      $sms_charge = convertToCents($sms_charge);
      $this->sms_description = EpAccountingDescription::getFomattedMessage(EpAccountingDescription::$SMS_CHARGE, array('account_number' => $acct_number,'amount'=>$amount_formatted));
      $attr_ipay4me = $this->updateIpay4meAccount($sms_charge, EpAccountingConstants::$ENTRY_TYPE_CREDIT,"sms_description");
      $attrArray[] = $attr_ipay4me;

    }

//
//print "<pre>";
//print_r($attrArray);exit;
   

    $event = sfContext::getInstance()->getEventDispatcher()->notify(new sfEvent($attrArray, 'epUpdateAccounts'));
    return $payment_transaction_number = $event->getReturnValue();
  }

  public function getUserAccount($user_id) {
    $userDetailObj = Doctrine::getTable('UserDetail')->findByUserId($user_id);
    return  $master_account_id = $userDetailObj->getFirst()->getMasterAccountId();
  }

  public function getUserAccountNumber($user_id) {
    $master_account_id = $this->getUserAccount($user_id);
    $accountingObj = new EpAccountingManager();
    $userAccountObj = $accountingObj->getAccount($master_account_id);
    return $user_account_number = $userAccountObj->getAccountNumber();
  }

  public function updateEwalletUserAccount($ewallet_amount, $user_id, $transaction_type) {
    $ewallet_account_id = $this->getUserAccount($user_id);
    return $this->getAccountingHolderObj($this->ledger_description, $ewallet_amount, $ewallet_account_id, $isCleared=1, $transaction_type);
  }

  public function updateIpay4meAccount($ipay4me_ewallet_amount, $transaction_type, $desc="") {
    $merchant_name = 'iPay4me';
    $merchantObj = Doctrine::getTable('Merchant')->findByName($merchant_name);
    $merchant_user_id = $merchantObj->getFirst()->getUserId();
    if($merchant_user_id) {
      $merchant_account_id = $this->getUserAccount($merchant_user_id);
    }
    if($desc == ""){
      $description = $this->ledger_description;
    }
    else {
      
      $description = $this->$desc;
    }
    //  $description = "Recharge";
    return $this->getAccountingHolderObj($description, $ipay4me_ewallet_amount, $merchant_account_id, $isCleared=1,$transaction_type);

  }

  public function creditGatewayAccount($gateway_amount, $gateway_id) {
    $gatewayMerchantObj = Doctrine::getTable('Gateway')->getGatewayMerchantDetails($gateway_id);
    $gateway_user_id = $gatewayMerchantObj->getFirst()->getMerchant()->getUserId();
    if($gateway_user_id) {
      $gateway_account_id = $this->getUserAccount($gateway_user_id);
    }
    // $description = "Recharge";
    return $this->getAccountingHolderObj($this->gateway_description, $gateway_amount, $gateway_account_id, $isCleared=1, EpAccountingConstants::$ENTRY_TYPE_CREDIT);

  }


  public function createUserAccount($userId) {
    $type = "ewallet";
    $walletObj = new EpAccountingManager;
    $userDetailObj = Doctrine::getTable('UserDetail')->findByUserId($userId);
    $account_name = $userDetailObj->getFirst()->getFirstName()." ".$userDetailObj->getFirst()->getLastName();
    $account_number = $this->generateAccountNumber();
    return $master_account_id =   $walletObj->createMasterAccount($account_name, $account_number, $type);
  }

  public function generateAccountNumber() {
    do {
      $account_number =  $this->getRandomNumber();
      $walletObj = new EpAccountingManager;
      $duplicacy_account_number = $walletObj->chkAccountNumber($account_number);
    } while ($duplicacy_account_number);
    return $account_number;
  }

  public function getRandomNumber() {
    return rand(1,5).''.rand('100','1000').''.rand('100','10000');
  }

  public function doPaymentAccounting($paid_amount, $user_id, $order_number) {
    try {
      sfContext::getInstance()->getConfiguration()->loadHelpers(array('EPortal'));
      $account_balance = $this->getAccountBalance($user_id);
      $paid_amount_cents = convertToCents($paid_amount);
      if($account_balance >= $paid_amount_cents) {
        $acct_number = $this->getUserAccountNumber($user_id);

        $amount_formatted = format_amount($paid_amount,1);
        $this->ledger_description = EpAccountingDescription::getFomattedMessage(EpAccountingDescription::$PAY_FROM_WALLET, array('account_number' => $acct_number,'amount'=>$amount_formatted, 'order_number'=>$order_number));


        $attrArray = array();

        //debit logged in user ewallet Account
        
        $attr_ewallet = $this->updateEwalletUserAccount($paid_amount_cents, $user_id, EpAccountingConstants::$ENTRY_TYPE_DEBIT);
        $attrArray[] = $attr_ewallet;

        //debit ipay4me account
        $attr_ipay4me = $this->updateIpay4meAccount($paid_amount_cents, EpAccountingConstants::$ENTRY_TYPE_DEBIT);
        $attrArray[] = $attr_ipay4me;


        $event = sfContext::getInstance()->getEventDispatcher()->notify(new sfEvent($attrArray, 'epUpdateAccounts'));
        return $payment_transaction_number = $event->getReturnValue();
      }
      else {
        throw New Exception("Insufficient Account Balance");
      }
    }
    catch (Exception $e) {
      return $e->getMessage();
    }

  }



  public function getAccountBalance($user_id) {
    $master_account_id = $this->getUserAccount($user_id);
    $accountingObj = new EpAccountingManager();
    $userAccountObj = $accountingObj->getAccount($master_account_id);
    return $current_balance = $userAccountObj->getBalance();
  }

  public function getSMSCharges($user_id) {
      $pinObj = new pin();
      return $smsCharge = $pinObj->getTotSmsCharge($user_id);
  }

  

 

}

?>
