<?php
class EpAccountingDescription {
  //category
  public static $RECHARGE_EWALLET = "eWallet - '{account_number}' recharged with '{amount}'";
  public static $PAY_FROM_WALLET = "Payment of '{amount}' for Order Number - '{order_number}' via eWallet - '{account_number}'";
  public static $SMS_CHARGE = "SMS charge  on recharge of '{amount}' collected from eWallet - '{account_number}' while recharge";
  public static $TRANSACTION_CHARGE = "Transaction charge on recharge of '{amount}' collected from eWallet - '{account_number}'";
  public static $GATEWAY_CHARGE = "Gateway charge on recharge of '{amount}' collected from eWallet - '{account_number}'";
  

  public static function getFomattedMessage($message, array $replaceVars=NULL) {
    $formattedMessage = $message;
    if(count($replaceVars) > 0){
      foreach ($replaceVars as $key => $value) {
        $formattedMessage = str_replace('{'.$key.'}', $value, $formattedMessage);
      }
    }
    return html_entity_decode($formattedMessage);
  }
}
?>
