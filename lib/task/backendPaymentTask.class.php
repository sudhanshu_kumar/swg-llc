<?php

class backendPaymentTask extends sfBaseTask
{
  public $browserInstance ;
  protected function configure()
  {
    // // add your own arguments here
    // $this->addArguments(array(
    //   new sfCommandArgument('my_arg', sfCommandArgument::REQUIRED, 'My argument'),
    // ));

    $this->addOption('application', null,
      sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'frontend');
    $this->addOption('env', null,
      sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev');

    $this->addOption('country', null, sfCommandOption::PARAMETER_OPTIONAL, 'Set Default Country', 'NZ');
    $this->namespace        = 'swglobal-llc';
    $this->name             = 'backendPayment';
    $this->briefDescription = 'This task will do backend payment';
    $this->detailedDescription = <<<EOF
    The [backendPayment|INFO] task does things.Call it with:  [php symfony swglobal-llc:backendPayment|INFO]
EOF;
  }

  protected function execute($arguments = array(), $options = array())
  {
    // initialize the database connection
    $databaseManager = new sfDatabaseManager($this->configuration);
    //    $connection = $databaseManager->getDatabase($options['connection'])->getConnection();

    // add your code here

    $nis = sfConfig::get('app_nis_portal_url');
    
    $nisUrl = $nis.'visa/autoFiller?cid='.$options['country'];
    $nisBrowser = $this->getBrowser() ;
    $nisBrowser->post($nisUrl);
    $code = $nisBrowser->getResponseCode();

    if ($code != 200 ) {
      $respMsg = $nisBrowser->getResponseMessage();
      $respText = $nisBrowser->getResponseText();
      $this->log(date('Y-m-d H:i:s')." :: Response error Message on redirecting to NIS : ".$respMsg);
      $this->log(date('Y-m-d H:i:s')." :: Response error Text on redirecting to NIS : ".$respMsg);
    }
    $redirectUrl = trim($redirectUrl);
    $redirectUrl = $nisBrowser->getResponseText();
    if(substr($redirectUrl, 0, 4) == "http")
    {
      //$this->log($redirectUrl);

      //do payment on ip4m after getting redirect url
      $ip4mBrowser = $this->getBrowser() ;
      $ip4mBrowser->post($redirectUrl);
      $code = $ip4mBrowser->getResponseCode();

      if ($code != 200 ) {
        $respMsg = $ip4mBrowser->getResponseMessage();
        $respText = $ip4mBrowser->getResponseText();
        $this->log(date('Y-m-d H:i:s')." :: Response error Message on redirecting to iPay4Me : ".$respMsg);
        $this->log(date('Y-m-d H:i:s')." :: Response error Text on redirecting to iPay4Me : ".$respMsg);
      }
      $statusMsg = $ip4mBrowser->getResponseText();

      $this->log(date('Y-m-d H:i:s')." :: Payment Status => ".$statusMsg);
    }else{
      $this->log(date('Y-m-d H:i:s')." :: Error at NIS end : $redirectUrl");
    }

  }

  protected function getBrowser() {
    if(!$this->browserInstance) {
      $this->browserInstance = new sfWebBrowser(array(), 'sfCurlAdapter',
        array('SSL_VERIFYPEER' => false, 'SSL_VERIFYHOST' => false));
    }
    return $this->browserInstance;
  }
}
