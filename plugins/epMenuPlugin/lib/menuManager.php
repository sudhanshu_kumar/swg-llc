<?php
class menuManager {

  public function getUserMenu() {
    $menuResult=Doctrine::getTable('EpMenu')->getMenu();
    // print "<pre>jj";
    //  print $menuResult->count();
    $menuTree = array();
    foreach($menuResult as $res) {
      $root_name = $res->getName();
      $menuTree[$root_name] = array();

      $menuTree[$root_name]['label'] = $res->getLabel();
      $menuTree[$root_name]['name'] = $res->getName();
      $menuTree[$root_name]['parent_id'] = $res->getParentId();
      if($res->getEpMenuChild1()->count()) {
        $menuTree[$root_name]['child'] = array();
        foreach($res->getEpMenuChild1() as $child_lvl1) {
          $menuTree[$root_name]['child'][$child_lvl1->getName()]=array();
          $menuTree[$root_name]['child'][$child_lvl1->getName()]['label'] = $child_lvl1->getLabel();
          $menuTree[$root_name]['child'][$child_lvl1->getName()]['url'] = $child_lvl1->getUrl();
          $menuTree[$root_name]['child'][$child_lvl1->getName()]['perm'] = $child_lvl1->getSfguardperm();
          $menuTree[$root_name]['child'][$child_lvl1->getName()]['name'] = $child_lvl1->getName();
          $menuTree[$root_name]['child'][$child_lvl1->getName()]['parent_id'] = $child_lvl1->getParentId();
          if( $child_lvl1->getEval()!="" ) {
                $menuTree[$root_name]['child'][$child_lvl1->getName()]['eval'] = $child_lvl1->getEval();

              }
          if($child_lvl1->getEpMenuChild2()->count()) {
            $menuTree[$root_name]['child'][$child_lvl1->getName()]['child'] = array();

            foreach($child_lvl1->getEpMenuChild2() as $childv2) {
              $menuTree[$root_name]['child'][$child_lvl1->getName()]['child'][$childv2->getName()]=array();
              $menuTree[$root_name]['child'][$child_lvl1->getName()]['child'][$childv2->getName()]['label'] = $childv2->getLabel();
              $menuTree[$root_name]['child'][$child_lvl1->getName()]['child'][$childv2->getName()]['url'] = $childv2->getUrl();
              $menuTree[$root_name]['child'][$child_lvl1->getName()]['child'][$childv2->getName()]['perm'] = $childv2->getSfguardperm();
              $menuTree[$root_name]['child'][$child_lvl1->getName()]['child'][$childv2->getName()]['name'] = $childv2->getName();
              $menuTree[$root_name]['child'][$child_lvl1->getName()]['child'][$childv2->getName()]['parent_id'] = $childv2->getParentId();
              if( $childv2->getEval()!="") {
                $menuTree[$root_name]['child'][$child_lvl1->getName()]['child'][$childv2->getName()]['eval'] = $childv2->getEval();

              }

            }
          }


        }

      }



    }
    return serialize($menuTree);


  }

  public function getMenu() {

    $menuRes=Doctrine::getTable('EpMenu')->getMainMenu();
    if($menuRes) {
      foreach($menuRes as $menu) {
      //                       print_r($menu);

        $this->loc_arr[$menu['name']]=$this->buildMenuArr($menu);


    }//forech

    }//menu res
    print "<pre>";
    print_R($this->loc_arr);exit;
    return serialize($this->loc_arr);
  }

  public function buildMenuArr($menu) {

    if($this->getNumberChildren($menu['id'])>0) {
      $tmpArr['label']=$menu['label'];
      $tmpArr['child']=array();
      $subtree = Doctrine::getTable('EpMenu')->getSubMenu($menu['id']);
      //$perLevel=(int)$menu['level']+1;
      $count=0;
      foreach ($subtree as $node) {
        $count++;
        $tmpArr['child'][$node['name']]=$this->buildMenuArr($node);
      }
      return $tmpArr;
    }
    else {
      $tmpArr['label']=$menu['label'];
      $tmpArr['url']=$menu['url'];
      if(''!=$menu['sfGuardPerm'] || Null!=$menu['sfGuardPerm'])
        $tmpArr['perm']=$menu['sfGuardPerm'];
      if(isset($menu['eval']) && NULL!= $menu['eval'] )
        $tmpArr['eval']=$menu['eval'];
      return $tmpArr;
    }
  }

  public function getNumberChildren($menuId) {
    $subtree = Doctrine::getTable('EpMenu')->getSubMenu($menuId);
    return count($subtree);

  }



}
?>
