<?php
class EpAccountingManager {
  public static function processPayment(sfEvent $event) {
    //$user = $event->getSubject();
    //$culture = $event['culture'];
    //
    // print "abcd";exit;
    //print $batch_update;exit;

    $eventObj = $event->getSubject();
    //    print "<pre>";
    //    print_r($eventObj);exit;

    //new transaction;generate transaction number

  //  $payment_transaction_number = self::updateAccounts($eventObj);
  //  $event->setReturnValue($payment_transaction_number);

    $parameters  = $event->getParameters();

      $payment_ref_num = "";
    if(array_key_exists('payment_ref_num',$parameters)) {
      $payment_ref_num = $parameters['payment_ref_num'];
    }

    //new transaction;generate transaction number


    $payment_ref_num = self::updateAccounts($eventObj,$payment_ref_num);

    $event->setReturnValue($payment_ref_num);

  }



  public static function updateAccounts($eventObj, $payment_ref_num="") {
    if($payment_ref_num != "") { $payment_transaction_number = $payment_ref_num;}
    else {
      $payment_transaction_number = self::generatePaymentTransactionNumber();
    }


   // $payment_transaction_number = self::generatePaymentTransactionNumber();
    if($payment_transaction_number) {
      foreach ($eventObj as $acctAttributes) {
       // print "<pre>";
       // print_r($acctAttributes);exit;
        $logger = sfContext::getInstance()->getLogger();
        $entry_type = $acctAttributes->getEntryType();
        $amount =  $acctAttributes->getAmount();
        $account_id = $acctAttributes->getAccountId();
        $description = $acctAttributes->getDescription();
        $isCleared = $acctAttributes->getIsCleared();
        $transaction_type = $acctAttributes->getTransactionType();
       // $accountant_id = $acctAttributes->getAccountantId();
        $masterAcctObj = self::getAccount($account_id);
        $logger->info("Accounting -".$account_id." - ".$entry_type." - ".$amount);
        //  $logger->info(print_r($masterAcctObj->toArray(),true));
        if($entry_type == EpAccountingConstants::$ENTRY_TYPE_CREDIT) {
          $logger->info("Crediting account");
          $masterAcctObj->creditAmount($amount, $description, $isCleared, $payment_transaction_number, $transaction_type);
          $logger->info("account Credited");
        } elseif($entry_type == EpAccountingConstants::$ENTRY_TYPE_DEBIT) {
          $masterAcctObj->debitAmount($amount, $description, $isCleared, $payment_transaction_number, $transaction_type);
        } else {
          throw new Exception('Unsupported type: '.$entry_type);
        }
      }
      return $payment_transaction_number;
    }
  }

  /**
   * Fetches and returns the objects corresponding to the given id
   *
   * @param integer $account_id account id to search
   * @return EpMasterAccount account object
   */
  public static function getAccount($account_id) {
    $masterAcctObj = Doctrine::getTable('EpMasterAccount')->find(array($account_id));
    if(!$masterAcctObj) {
      return null;
    }
    return  $masterAcctObj;
  }

  public function getUnclearedEntries($master_account_id) { //returns all uncleared ledger entry from the associated ledger
    $unClearedEntriesObj = Doctrine::getTable('EpMasterLedger')->getUnclearedEntries($master_account_id);
  }

  public function getWalletDetails($wallet_acct_number) {
    $obj = Doctrine::getTable('EpMasterAccount')->getAccountDetails($wallet_acct_number);
    if(is_object($obj)) {
      return $obj;
    }
    else {
      return 0;
    }
  }

  public function getTransactionDetails($wallet_number,$from_date="",$to_date="",$type="") {
    $obj = Doctrine::getTable('EpMasterLedger')->getTransactionDetails($wallet_number,$from_date,$to_date,$type);
    if(is_object($obj)) {
      return $obj;
    }
    else {
      return 0;
    }
  }




  public function createMasterAccount($account_name,$account_number, $type) {
    $acctObj = new EpMasterAccount();
    $acctObj->setAccountName($account_name);
    $acctObj->setAccountNumber($account_number);
    $acctObj->setType($type);
    $acctObj->save();
    return $acctObj->getId();
  }


  public function chkAccountNumber($account_number) {

    $masterAcctObj = Doctrine::getTable('EpMasterAccount')->findOneByAccountNumber($account_number);
    if(!$masterAcctObj) {
      return null;
    }
    return $masterAcctObj;
  }

  public function getCashControlDetails() {

    $masterAcctObj = Doctrine::getTable('EpMasterAccount')->getCashControlDetails();
    if(!$masterAcctObj) {
      return null;
    }
    return $masterAcctObj;
  }

  public function getStatementDetails($from_date="",$to_date="",$wallet_number="",$type="", $QueryType="") {
    $obj = Doctrine::getTable('EpMasterAccount')->getStatementDetails($from_date,$to_date,$wallet_number,$type,$QueryType);
    if(is_object($obj)) {
      return $obj;
    }
    else {
      return 0;
    }
  }

  public function getVirtualAccDetails($from_date="",$to_date="",$master_account_id="",$type="", $merchant_id= "", $QueryType="") {
    $obj = Doctrine::getTable('EpMasterAccount')->getVirtualAccDetails($from_date,$to_date,$master_account_id,$type,$merchant_id,$QueryType);
    if(is_object($obj)) {
      return $obj;
    }
    else {
      return 0;
    }
  }


  public function getP4mVirtualAccDetails($from_date="",$to_date="",$account_no="",$type="", $merchant_id= "", $QueryType="") {
    $obj = Doctrine::getTable('EpMasterAccount')->getP4mVirtualAccDetails($from_date,$to_date,$account_no,$type,$merchant_id,$QueryType);
    if(is_object($obj)) {
      return $obj;
    }
    else {
      return 0;
    }
  }



  public function getMasterAcountDetails($paymentModeOptionId="", $dql_callback="") {
    $obj = Doctrine::getTable('EpMasterAccount')->getMasterAcountDetails($paymentModeOptionId, $dql_callback);
    return $obj;

  }


  public function getEwalletHostDetailsQuery($from_date="",$to_date="",$account_details="",$type="") {
    $obj = Doctrine::getTable('EpMasterAccount')->getEwalletHostDetailsQuery($from_date,$to_date,$account_details,$type);

    //if(is_object($obj)) {
    return $obj;
    //}
    //else {
    //  return 0;
    //}
  }


  public function getVirtualAccQuery($from_date="",$to_date="",$merchant_id="",$type="") {
    $obj = Doctrine::getTable('EpMasterAccount')->getVirtualAccQuery($from_date,$to_date,$merchant_id,$type);

    //if(is_object($obj)) {
    return $obj;
    //}
    //else {
    //  return 0;
    //}
  }

  public function getStatementInfo($from_date = "", $to_date = "", $account_no = "", $account_name = "", $type = "") {
    $obj = Doctrine::getTable('EpMasterAccount')->getStatement($from_date, $to_date, $account_no,$account_name, $type);
    if(is_object($obj)) {
      return $obj;
    }
    else {
      return 0;
    }
  }



  public function getStatement($from_date = "", $to_date = "", $account_no = "", $account_name = "", $type = "",$acct_type="") {
    $obj = Doctrine::getTable('EpMasterAccount')->getStatementfrmAccntNo($from_date, $to_date, $account_no,$account_name, $type, $acct_type);
    if(is_object($obj)) {
      return $obj;
    }
    else {
      return 0;
    }
  }

  public function getAcctSummary($from_date="",$to_date="",$merchant_id="",$merchant_service_id="",$account_type="") {
    $obj = Doctrine::getTable('EpMasterAccount')->getAcctSummary($from_date,$to_date,$merchant_id,$merchant_service_id,$account_type);
    if(is_object($obj)) {
      return $obj;
    }
    else {
      return 0;
    }
  }

  public static function generatePaymentTransactionNumber() {
    do {
      $payment_transaction_number =  self::getRandomNumber();
      $duplicacy = Doctrine::getTable('EpMasterLedger')->chkPaymentTransactionNumber($payment_transaction_number);
    } while ($duplicacy>0);
    return $payment_transaction_number;
  }

  public static function getRandomNumber() {
    return mt_rand(1000000,9999999999);
  }



  //   public function getWalletDetails($acct_number){
  //      $obj = Doctrine::getTable('EpMasterAccount')->findByAccountNumber($acct_number);
  //      if(is_object($obj) && ($obj->getId() != ''))
  //      {
  //         return $obj;
  //      }
  //      else{
  //        return 0;
  //      }
  //   }

  public function checkLastTransaction($ewalletAccountId,$userId,$ewalletRechargeInterval=''){
    $start_date = '';
    $end_date = '';
    if($ewalletRechargeInterval){
      $start_date = date('Y-m-d H:i:s',mktime(date("H"),date("i")-$ewalletRechargeInterval,date("s"),date("n"),date("j"),date("y")));
      $end_date = date('Y-m-d H:i:s',mktime(date("H"),date("i"),date("s"),date("n"),date("j"),date("y")));
    }
    return Doctrine::getTable('EpMasterLedger')->getLastTransactionsCount($ewalletAccountId,$userId,$start_date,$end_date,'credit');

  }

  public function getDetailsByValidationNumber($validation_number){
    return Doctrine::getTable('EpMasterLedger')->getDetailsByValidationNumber($validation_number,'credit');
  }
  public function getAccountDetailsByValidationNumber($validation_no, $entry_type=''){
    return Doctrine::getTable('EpMasterLedger')->getAccountDetailsByValidationNumber($validation_no, $entry_type);
  }

  public function generateAccountNumber() {
        do {
            $account_number =  $this->getRandomNumber();
            $duplicacy_account_number = $this->chkAccountNumber($account_number);
        } while ($duplicacy_account_number);
        return $account_number;
    }
}

class IllegalEpLedgerActionException extends Exception {
}

?>