<?php

class EpOpenIDProviderYahoo extends EpOpenIDProviderIntf {
  public function getAuthenticationUrl($screenName=null) {
    $openIdUrl = sfConfig::get('app_openid_url_yahoo_url');
    return $this->preAuthenticate($openIdUrl);
  }

  public function getDbDetails($data) {
    $map = new DbtoOpendidAttrMap();
    $map[DbtoOpendidAttrMap::$USER_KEY] = $data['openid_identity'];
    $map[DbtoOpendidAttrMap::$FULL_NAME_KEY] = $data['openid_ax_value_fullname'];
    $map[DbtoOpendidAttrMap::$EMAIL_KEY]  = $data['openid_ax_value_email'];
    return $map;
  }
}