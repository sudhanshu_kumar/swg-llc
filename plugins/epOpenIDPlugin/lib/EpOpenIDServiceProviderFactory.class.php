<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class EpOpenIDServiceProviderFactory {

   public static $googleConfig = 'GOOGLE';
   public static $yahooConfig = 'YAHOO';
   public static $aolConfig = 'AOL';
   public static $pay4meConfig = 'PAY4ME';
  /**
  *
  * @param <type> $type
  * @return empService the service implementation
  */
    public static function getService($type) {
        switch($type){

            case 'GOOGLE':
                return new EpOpenIDProviderGoogle();
                break;
            case 'YAHOO':
                return new EpOpenIDProviderYahoo();
                break;
            case 'AOL':
                return new EpOpenIDProviderAol();
                break;
            case 'PAY4ME':
                return new EpOpenIDProviderPay4me();
                break;
        }
    }
}
