<?php

class EpPayEasyManager
{
    protected $Request_XML;
    protected $Response_XML;
    protected $Request_id;
    public function doPayment($userDetail){

        $transDate = date('d-M-Y H:i:s') ;
        $userDetail['transDate'] = $transDate;
        $userDetail['ip_address'] = $userDetail['request_ip'];//sfConfig::get('app_pay_easy_ip_address');
        $userDetail['card_len'] = strlen($userDetail['card_Num']);
        $userDetail['card_first'] = substr($userDetail['card_Num'], 0, 4);
        $userDetail['card_last'] = substr($userDetail['card_Num'], -4, 4);

      try{

        $requestXml = $this->getRequestXML($userDetail);

        if($requestXml){

             $xml = simplexml_load_string($requestXml);
               $xml->STRUCTURE->CARD_INFO->Card_No = $userDetail['card_first'].$this->getNoOfX($userDetail['card_len'] - 8).$userDetail['card_last']  ;
               $xml->STRUCTURE->CARD_INFO->CVV = $this->getNoOfX(strlen($userDetail['cvv']));

               $storingXml = $xml->asXML();
//           $storingXml = $this->getRequestXML($userDetail);
            $this->createLogData($storingXml,'RequestXml'.$userDetail['cus_id']);
            $Request_id = $this->storeDetail($userDetail,'Request');

            $resp = $this->getResponse($requestXml,$userDetail, $Request_id);
            if($resp)
             {
                 $resp['trans_time'] = date('Y-m-d H:i:s',strtotime($transDate)) ;
                 return $resp;
             }
        }
      }
        catch (Exception $e)
        {
            throw new Exception ($e->getMessage());
        }

    }

    public function getRequestXML($userDetail){
        $doc = new DomDocument('1.0');
        $doc->formatOutput = true;

        $root = $doc->createElement('PURCHASE_REQUEST');
//        $root->setAttribute ( "xmlns:xsi", 'http://www.w3.org/2001/XMLSchema-instance' );
//        $root->setAttribute ( "xmlns", 'http://www.ipay4me.com/schema/pay4meorder/v1' );
//        $root->setAttribute ( "xsi:schemaLocation", 'http://www.ipay4me.com/schema/ipay4meorder/v1 payEasy.xsd' );


        $root = $doc->appendChild($root);

        $headerElm=$this->getHeaderElm($userDetail,$doc);
        $root->appendChild($headerElm);


        $structElm = $doc->createElement("STRUCTURE");

        $transInfoElm=$this->getTransInfo($userDetail,$doc);
        $structElm->appendChild($transInfoElm);

        $cardInfoElm=$this->getCardInfo($userDetail,$doc);
        $structElm->appendChild($cardInfoElm);

        $billInfoElm=$this->getBillInfo($userDetail,$doc);
        $structElm->appendChild($billInfoElm);

        $contactInfoElm=$this->getContactInfo($userDetail,$doc);
        $structElm->appendChild($contactInfoElm );

        $otherInfoElm=$this->getOtherInfo($userDetail,$doc);
        $structElm->appendChild($otherInfoElm);

        $rebillingInfoElm=$this->getRebillingInfo($userDetail,$doc);
        $structElm->appendChild($rebillingInfoElm);

        $root->appendChild($structElm);

       $xmldata = $doc->saveXML();
        $xmldata = str_replace('<?xml version="1.0"?>','',$xmldata);

        //        vallidate the XML
//        $xdoc = new DomDocument;
//
//        $isLoaded = $xdoc->LoadXML($xmldata);

//        if($isLoaded)
//        {
//            $validRes=$this->validateXMLcontent($xdoc);
//            if($validRes)
//            {
                $this->Request_XML = $xmldata;
                return $xmldata;
//            }
//        }
//        else
//        {   return false;     }
    }

    private function getHeaderElm($userDetail,$doc){



        $headerElm = $doc->createElement("HEADER");

        $dateElm=$doc->createElement("Date");
        $dateElm->appendChild($doc->createTextNode($userDetail['transDate']));
        $headerElm->appendChild($dateElm);

        $issueElm=$doc->createElement("Issuer");
        $issueElm->appendChild($doc->createTextNode(sfConfig::get('app_pay_easy_issuer')));
        $headerElm->appendChild($issueElm);

        $merchantElm=$doc->createElement("Merchant_Id");
        $merchantElm->appendChild($doc->createTextNode(base64_encode(sfConfig::get('app_pay_easy_merchant_id'))));

        $headerElm->appendChild($merchantElm);

        $pwdElm=$doc->createElement("Password");
        $pwdElm->appendChild($doc->createTextNode(base64_encode(sfConfig::get('app_pay_easy_password'))));

        $headerElm->appendChild($pwdElm);

        $IPElm=$doc->createElement("RequestIP");
        $IPElm->appendChild($doc->createTextNode(sfConfig::get('app_pay_easy_request_IP')));
        $headerElm->appendChild($IPElm);

        return $headerElm;
    }
    private function getTransInfo($userDetail,$doc){
        $transElm = $doc->createElement("TRANSACTION_INFO");

        $tranTypeElm=$doc->createElement("Tran_Type");
        $tranTypeElm->appendChild($doc->createTextNode(sfConfig::get('app_pay_easy_tran_type')));
        $transElm->appendChild($tranTypeElm);

        $currElm=$doc->createElement("Currency");
        $currElm->appendChild($doc->createTextNode('USD'));
        $transElm->appendChild($currElm);

        $AmtElm = $doc->createElement("Amount");
        $AmtElm->appendChild($doc->createTextNode($userDetail['amount']));
        $transElm->appendChild($AmtElm);

        return $transElm;

    }
    private function getCardInfo($userDetail,$doc){

        $cardElm = $doc->createElement("CARD_INFO");
        $CardNoElm=$doc->createElement("Card_No");
        $CardNoElm->appendChild($doc->createTextNode($userDetail['card_Num']));
        $cardElm->appendChild($CardNoElm);


        $CardType=$doc->createElement("Card_Type");
        $CardType->appendChild($doc->createTextNode($userDetail['card_type']));
        $cardElm->appendChild($CardType);

        $Cardholder=$doc->createElement("Cardholder");
        $Cardholder->appendChild($doc->createTextNode(urlencode(htmlspecialchars($userDetail['card_holder']))));
        $cardElm->appendChild($Cardholder);


        $expMonth=$doc->createElement("Expiry_Month");
        $expMonth->appendChild($doc->createTextNode($userDetail['expiry_date']['month']));
        $cardElm->appendChild($expMonth);

        $expYear=$doc->createElement("Expiry_Year");
        $expYear->appendChild($doc->createTextNode($userDetail['expiry_date']['year']));
        $cardElm->appendChild($expYear);

        $cvv=$doc->createElement("CVV");
        $cvv->appendChild($doc->createTextNode($userDetail['cvv']));
        $cardElm->appendChild($cvv);


        $custSupport=$doc->createElement("CustSupport");
        $custSupport->appendChild($doc->createTextNode(sfConfig::get('app_pay_easy_customersupport')));
        $cardElm->appendChild($custSupport);

        return $cardElm;

    }
    private function getBillInfo($userDetail,$doc){

        $billElm = $doc->createElement("BILLING_INFO");

        $address1Elm=$doc->createElement("Address1");
        $address1Elm->appendChild($doc->createTextNode(urlencode(htmlspecialchars($userDetail['address1']))));
        $billElm->appendChild($address1Elm);

        $address2Elm=$doc->createElement("Address2");
        $address2Elm->appendChild($doc->createTextNode(urlencode(htmlspecialchars($userDetail['address2']))));
        $billElm->appendChild($address2Elm);

        $townElm=$doc->createElement("Town");
        $townElm->appendChild($doc->createTextNode(urlencode(htmlspecialchars($userDetail['town']))));
        $billElm->appendChild($townElm);

        $stateElm=$doc->createElement("State");
        $stateElm->appendChild($doc->createTextNode(urlencode(htmlspecialchars($userDetail['state']))));
        $billElm->appendChild($stateElm);

        $zipElm=$doc->createElement("Zip");
        $zipElm->appendChild($doc->createTextNode(urlencode(htmlspecialchars($userDetail['zip']))));
        $billElm->appendChild($zipElm);

        $countryElm=$doc->createElement("Country");
        $countryElm->appendChild($doc->createTextNode($userDetail['country']));
        $billElm->appendChild($countryElm);

        return $billElm;

    }
    private function getContactInfo($userDetail,$doc){

        $contactElm = $doc->createElement("CONTACT_INFO");

        $fnameElm=$doc->createElement("First_Name");
        $fnameElm->appendChild($doc->createTextNode(urlencode(htmlspecialchars($userDetail['first_name']))));
        $contactElm->appendChild($fnameElm);


        $lnameElm=$doc->createElement("Last_Name");
        $lnameElm->appendChild($doc->createTextNode(urlencode(htmlspecialchars($userDetail['last_name']))));
        $contactElm->appendChild($lnameElm);

        $emailElm=$doc->createElement("Email");
        $emailElm->appendChild($doc->createTextNode($userDetail['email']));
        $contactElm->appendChild($emailElm);

        $phoneElm=$doc->createElement("Phone_No");
        $phoneElm->appendChild($doc->createTextNode(urlencode(htmlspecialchars($userDetail['phone']))));
        $contactElm->appendChild($phoneElm);

        return $contactElm;

    }
    private function getOtherInfo($userDetail,$doc){

        $otherElm = $doc->createElement("OTHER_INFO");

        $CusidElm=$doc->createElement("Cusid");
        $CusidElm->appendChild($doc->createTextNode($userDetail['cus_id']));
        $otherElm->appendChild($CusidElm);

        $IPAddressElm=$doc->createElement("IP_Address");
        $IPAddressElm->appendChild($doc->createTextNode($userDetail['ip_address']));
        $otherElm->appendChild($IPAddressElm);

        $webElm=$doc->createElement("Web_Site");
        $webElm->appendChild($doc->createTextNode(sfConfig::get('app_pay_easy_web_site')));
        $otherElm->appendChild($webElm);

        $terminalElm=$doc->createElement("Terminal_Id");
        $terminalElm->appendChild($doc->createTextNode(sfConfig::get('app_pay_easy_terminal_id')));
        $otherElm->appendChild($terminalElm);

        return $otherElm ;


    }
    private function getRebillingInfo($userDetail,$doc){

        $rebillingElm = $doc->createElement("REBILLING_INFO");

        $terminalElm=$doc->createElement("Rebill_Period");
        $terminalElm->appendChild($doc->createTextNode("-1"));
        $rebillingElm->appendChild($terminalElm);

        return $rebillingElm;

    }


    public function validateXMLcontent($xdoc){
        $plugin_dir = sfConfig::get('sf_plugins_dir') ;

        $xmlschema = $plugin_dir. '/epPayEasyPlugin/config/payEasy.xsd';

        $handle = @fopen($xmlschema, "r");

        try {
            if ($xdoc->schemaValidate($xmlschema))
            return true;
            else
            {return false ;}

        }catch(Exception $ex)  {
            echo $ex->getMessage();
        }

    }
    public function storeDetail($getArr,$type='',$Request_id=''){
        if('Request' == $type){

            $payEasyRequest = new EpPayEasyRequest ();
            $payEasyRequest->setTranDate(date('Y-m-d H:i:s',strtotime($getArr['transDate'])));
            $payEasyRequest->setIssuer(sfConfig::get('app_pay_easy_issuer'));
            $payEasyRequest->setRequestIp(sfConfig::get('app_pay_easy_request_IP'));
            $payEasyRequest->setTranType(sfConfig::get('app_pay_easy_tran_type'));
            $payEasyRequest->setCurrency('USD');
            $payEasyRequest->setAmount($getArr['amount']);
            $payEasyRequest->setCardFirst($getArr['card_first']);
            $payEasyRequest->setCardLast($getArr['card_last']);
            $payEasyRequest->setCardLen($getArr['card_len']);
            $payEasyRequest->setCardType($getArr['card_type']);
            $payEasyRequest->setCardHolder($getArr['card_holder']);
            $payEasyRequest->setExpiryMonth($getArr['expiry_date']['month']);
            $payEasyRequest->setExpiryYear($getArr['expiry_date']['year']);
//            $payEasyRequest->setCvv($getArr['cvv']);
            $payEasyRequest->setCustomersupport(sfConfig::get('app_pay_easy_customersupport'));
            $payEasyRequest->setAddress1($getArr['address1']);
            $payEasyRequest->setAddress2($getArr['address2']);
            $payEasyRequest->setTown($getArr['town']);
            $payEasyRequest->setState($getArr['state']);
            $payEasyRequest->setZip($getArr['zip']);
            $payEasyRequest->setCountry($getArr['country']);
            $payEasyRequest->setFirstName($getArr['first_name']);
            $payEasyRequest->setLastName($getArr['last_name']);
            $payEasyRequest->setEmail($getArr['email']);
            $payEasyRequest->setPhone($getArr['phone']);
            $payEasyRequest->setCusId($getArr['cus_id']);
            $payEasyRequest->setIpAddress($getArr['ip_address']);
            $payEasyRequest->setRequestXml($this->Request_XML);

            $payEasyRequest->save();
            return $this->Request_id = $payEasyRequest->getId();
        }
        if('Response' == $type){
            $payEasyResponse = new EpPayEasyResponse ();
            $payEasyResponse->setPayeasyReqId($Request_id);
            $payEasyResponse->setTimeTaken($getArr['HEADER'][0]['Time_Taken']);
            $payEasyResponse->setIssuer($getArr['HEADER'][0]['Issuer']);
            $payEasyResponse->setResponseIp($getArr['HEADER'][0]['ResponseIP']);
            $payEasyResponse->setTranType($getArr['STRUCTURE'][1]['TRANSACTION_INFO'][0]['Tran_Type']);
            $payEasyResponse->setCurrency($getArr['STRUCTURE'][1]['TRANSACTION_INFO'][0]['Currency']);
            $payEasyResponse->setAmount($getArr['STRUCTURE'][1]['TRANSACTION_INFO'][0]['Amount']);
            $payEasyResponse->setTranId($getArr['STRUCTURE'][1]['PROCESSED_INFO'][1]['Tranid']);
            $payEasyResponse->setResponseCode($getArr['STRUCTURE'][1]['PROCESSED_INFO'][1]['Response_Code']);
            $payEasyResponse->setResponseText($getArr['STRUCTURE'][1]['PROCESSED_INFO'][1]['Response_Text']);
            $payEasyResponse->setAuthCode($getArr['STRUCTURE'][1]['PROCESSED_INFO'][1]['Auth_Code']);
            $payEasyResponse->setStatus($getArr['STRUCTURE'][1]['PROCESSED_INFO'][1]['Status']);
             if(isset($getArr['STRUCTURE'][1]['FRAUD_INFO']))
                {
                    $payEasyResponse->setFraudScore($getArr['STRUCTURE']['FRAUD_INFO']['Score']);
                    $payEasyResponse->setFraudMessage($getArr['STRUCTURE']['FRAUD_INFO']['Message']);
                }
            $payEasyResponse->setResponseXml($this->Response_XML);

            $payEasyResponse->save();
        }



  }

    public function getResponse($transXml,$userDetail,$Request_id =''){
        try{
//        if(!empty($this->Request_XML)){
            ////////Send Curl Request  Start ///////////
            $header[] = "Content-Type: application/x-www-form-urlencoded";
            $url =  sfConfig::get('app_pay_easy_web_url') ;
            //. sfConfig::get('app_pay_easy_terminal_id').
            $xmlRequest = 't='.sfConfig::get('app_pay_easy_t').'&load=%3C%3Fxml+version%3D%221.0%22+encoding%3D%22UTF-8%22%3F%3E'.$transXml;

            $curl = curl_init();



            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
            //curl_setopt($curl, CURLOPT_HEADER, 1);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $xmlRequest);
            curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);

            $responseXml = curl_exec($curl);

            $this->Response_XML = $responseXml;
            ////////Send Curl Request  End ///////////
            ///////////////////// LOGGING //////////////////////////////
            $this->createLogData($responseXml,'ResponseXml'.$userDetail['cus_id']);
            ///////////////////////////////////////////////////////////
//            $xdoc = new DomDocument;
//            if(isset($responseXml) &&  $responseXml!='')
//              $isLoaded = $xdoc->LoadXML($responseXml);
//            if(isset($isLoaded) && $isLoaded)
//            {
////                $validRes=$this->validateXMLcontent($xdoc);
////                if($validRes)
////                {
//                    //return $responseXml;//$xmldata;
////                }
//            }
//            else
//            {  // return false;     }

            //////////////////////////////////////////////////

            $crash = 0;
            $respObj = simplexml_load_string($responseXml);
            if(!isset($responseXml) || ''==$responseXml || !$respObj || ''==$respObj)
            {
               $crash = 1;
            }
            else
            {
                $respArr = $this->getArrayfrmXML($respObj,array());
            }

            if(0 == $crash && isset($respArr['STRUCTURE'][1]['TRANSACTION_INFO'][0]['Cusid']) &&  $userDetail['cus_id']==$respArr['STRUCTURE'][1]['TRANSACTION_INFO'][0]['Cusid'])
            {
                $this->storeDetail($respArr,'Response',$Request_id);
                $abResp = $this->extractResp($respArr,$userDetail);
            }
            else
            {
                if( isset($respArr) && ($userDetail['cus_id'] != $respArr['STRUCTURE'][1]['TRANSACTION_INFO'][0]['Cusid']))
                    $responseXml ='CusId is not same as sent'.$responseXml;
                $abResp = array();
                $abResp['cusid'] = $userDetail['cus_id'];
                $abResp['status'] = 0;
                $abResp['response_code'] = '';
                $abResp['response_text'] = '';

                $sendMailUrl = "notification/mailOnPaymentCrash" ;
                $taskId = EpjobsContext::getInstance()->addJob('PaymentNofication',$sendMailUrl, array('order_number' => $userDetail['cus_id'],'responseXML' =>$responseXml));
                sfContext::getInstance()->getLogger()->debug("sceduled job with id: $taskId");
                return $abResp;
                
            }

            

            return $abResp;
       }catch(Exception $e){
                $sendMailUrl = "notification/mailOnPaymentCrash" ;
                $taskId = EpjobsContext::getInstance()->addJob('PaymentNofication',$sendMailUrl, array('order_number' => $userDetail['cus_id'],'responseXML' =>$responseXml));
                sfContext::getInstance()->getLogger()->debug("sceduled job with id: $taskId");
            
        }
//        }


//        ////////Send Curl Request  ///////////
//        $url = sfConfig::get('app_pay_easy_url') ;
//        $browser = $this->getBrowser() ;
//        $header['Content-Type'] = "application/x-www-form-urlencode(htmlspecialcharsd";
//        $obj = new pay4MeXMLV1();
//        $browser->post($url, $transXml, $header);
//        $code = $browser->getResponseCode();
//        if ($code != 200 ) {
//          $respMsg = $browser->getResponseMessage();
//          $respText = $browser->getResponseText();
//          throw new Exception("Error in posting easypay request:".PHP_EOL.$code.' '.$respMsg.PHP_EOL.$respText);
//        }
//        $this->logMessage('payment XML send') ;
//        $this->logMessage($respText) ;
//        print_r($respText) ;
//
//        ////////Get Curl Response ///////////
////        $this->storeDetail($respText,'response');
//        if(200 == $respCode ){
//            $absResp = $this->extractResp($resp);
//
//        }
//        return false;
    }
    public function extractResp($respArr, $userDetail){
        $abResp = array();
       if( !isset($respArr['STRUCTURE'][1]['PROCESSED_INFO'][1]['Response_Code']))
           {
               $abResp['cusid'] = $userDetail['cus_id'];
               $abResp['status']= 0;
           }
       else{

            $abResp['cusid']= $respArr['STRUCTURE'][1]['TRANSACTION_INFO'][0]['Cusid'];
            if(8 == $respArr['STRUCTURE'][1]['PROCESSED_INFO'][1]['Response_Code'] || 9 == $respArr['STRUCTURE'][1]['PROCESSED_INFO'][1]['Response_Code'])
                $abResp['status']= 1;
            else
                $abResp['status']= 0;
        }
        $abResp['response_code'] = $respArr['STRUCTURE'][1]['PROCESSED_INFO'][1]['Response_Code'];
        $abResp['response_text'] = $respArr['STRUCTURE'][1]['PROCESSED_INFO'][1]['Response_Text'];

        return $abResp;

    }
    public function getArrayfrmXML($xml,$arr){
        $iter = 0;
        foreach($xml->children() as $b){
            $a = $b->getName();
            if(!$b->children()){
                $arr[$a] = trim($b[0]);
            }
            else{
                $arr[$a][$iter] = array();
                $arr[$a][$iter] = $this->getArrayfrmXML($b,$arr[$a][$iter]);
            }
            $iter++;
        }
        return $arr;
    }


    public function createLogData($xmlData,$nameFormate,$parentLogFolder='', $appendTime=true, $append = false)
    {
        $path = $this->getLogPath($parentLogFolder);
        if($appendTime){
            $file_name = $path."/".($nameFormate.'-'.date('Y_m_d_H:i:s')).".txt";
        }else{
            $file_name = $path."/".$nameFormate.".txt";
        }
        if($append)
        {
            @file_put_contents($file_name, $xmlData, FILE_APPEND);
        }else{
            $i=1;
            while(file_exists($file_name)) {
                $file_name = $path."/".($nameFormate.'-'.date('Y_m_d_H:i:s-')).$i.".txt";
                $i++;
            }
            @file_put_contents($file_name, $xmlData);
        }
    }



    public function getLogPath($parentLogFolder)
    {

        $logPath =$parentLogFolder==''?'/payEasylog':'/'.$parentLogFolder;
        $logPath =  sfConfig::get('sf_log_dir').$logPath.'/'.date('Y-m-d');

        if(is_dir($logPath)=='')
        {
            $dir_path=$logPath."/";
            mkdir($dir_path,0777,true);
            chmod($dir_path, 0777);
        }
        return $logPath;
    }

public function getNoOfX($length)
    {
        $text = "";
        for($i=0;$i<$length;$i++){
            $text.="x";
        }
        return $text;
    }

//   public function getBrowser() {
//    if(!$this->browserInstance) {
//      $this->browserInstance = new sfWebBrowser(array(), 'sfCurlAdapter',
//          array('SSL_VERIFYPEER' => false, 'SSL_VERIFYHOST' => false));
//    }
//    return $this->browserInstance;
//  }

}
?>
