<?php

class EpJobConstants {
  public static $JOB_STATE_ACTIVE = 'active';
  public static $JOB_STATE_SUSPENDED = 'suspended';
  public static $JOB_STATE_FINISHED = 'finished';

  public static $JOB_SCHEDULE_TYPE_ONCE = 'once';
  public static $JOB_SCHEDULE_TYPE_REPEAT = 'repeated';

  public static $JOB_QUEUE_STATUS_SCHEDULED = 'scheduled';
  public static $JOB_QUEUE_STATUS_RUNNING = 'running';

  public static $JOB_LAST_EXECUTION_STATE_PASS = 'pass';
  public static $JOB_LAST_EXECUTION_STATE_FAIL = 'failed';
  public static $JOB_LAST_EXECUTION_STATE_NOTEXECUTED = 'notexecuted';

  public static $JOB_SCHEDULE_HOST_NAME = "ep_int_hostname";
  public static $JOB_SCHEDULE_SCHEMA_NAME = "ep_int_schema";
  public static $DEV_ENV_NAME = "dev";

  // few exit code definitions
  public static $EXIT_CODE_JOB_BROWSER_EXCEPTION = 199;

  public static $EXIT_CODE_JOB_NOT_FOUND_IN_QUEUE = 150;
  public static $EXIT_CODE_JOB_ALREADY_RUNNING=151;

  // these exit codes can be used by the consuming applications
  /**
   * During te task execution, the url reported that the task
   * was failed with this code. In this case the business logic
   * of the task decided this failure and it's a direct intention
   * for rescheduling of the task.
   */
  public static $EXECUTION_REPORTED_FAILURE = 160;

  /**
   * Default environment with which the tasks shall be executed.
   *
   * @var string the default environment for test execution
   */
  public static $CFG_DEFAULT_ENVIRONMENT = 'test';

  /*
   * These are the variables that can be configured through
   * the app.yml of the config file as well. Only the default
   * values of these variables is present here and all of them
   * are elligible for re-definition in the app.yml
   */
  /**
   * Default application that shall be used for tasks execution if
   * none is specified by the user.
   * Can be over-ridden by config variable app_epjob_default_sf_application
   *
   * @var string default application for exeuction
   */
  public static $CFG_DEFAULT_APPLICATION = 'frontend';

  /**
   * How many child processes shall be utilized for job execution.
   * 
   * Can be over-ridden by config variable app_epjob_max_task_processes
   *
   * @var integer Maximum number of exeuction processes that can be spawned.
   *              Represented by app_epjobs_max_task_processes config param
   */
  public static $CFG_MAX_TASK_PROCESSES = 10;

  /**
   * How much time shall be added for re-secheduling a task.
   * Can be over-ridden by config variable app_epjob_reschedule_duration_min
   * 
   * @var integer time in minutes for rescheduling a failed task
   */
  public static $CFG_RESCHEDULE_DURATION_MIN= 5;

  /**
   *
   * How many times a failed job shall be reattempted for succesful
   * execution.
   * This is for scheduled job which is executed for once only
   * Can be over-ridden by config variable app_epjob_default_retry_count_once
   * 
   * @var integer how many times a failed job shall be retried
   */
  public static $CFG_DEFAULT_RETRY_COUNT_ONCE = 10;

  /**
   * How many times a failed job shall be reattempted for succesful
   * execution.
   * This is for repeated scheduled job 
   * Can be over-ridden by config variable app_epjob_default_retry_count_reptd
   *
   * @var integer how many times a failed job shall be retried
   */
  public static $CFG_DEFAULT_RETRY_COUNT_REPTD = 2;

}