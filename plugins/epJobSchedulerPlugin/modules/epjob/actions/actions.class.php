<?php

require_once dirname(__FILE__).'/../lib/epjobGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/epjobGeneratorHelper.class.php';

/**
 * epjob actions.
 *
 * @package    basefw
 * @subpackage epjob
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12474 2008-10-31 10:41:27Z fabien $
 */
class epjobActions extends autoEpjobActions
{
}
