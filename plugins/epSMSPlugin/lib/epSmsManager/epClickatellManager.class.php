<?php

class epClickatellManager {
    public $browserInstance ;

    public function sendSms($mobileNumber,$message){

        $xmlData= $this->generateSmsXml($mobileNumber, $message);
        $this->createLogData($xmlData,'RequestXml','smsLogs');
        //        header("Content-type: application/xml");
        //        print($xmlData);
        //        die();
        $getStatus= $this->postXML($xmlData);
        return $getStatus;
    }


    private function generateSmsXml($mobileNumber,$message)
    {

        $xml_data = new gc_XmlBuilderSms();
        $xml_data->Push('clickAPI');
        $xml_data->Push('sendMsg');
        $xml_data->Element('api_id', sfConfig::get('app_clickatell_api_id'));
        $xml_data->Element('user', sfConfig::get('app_clickatell_username'));
        $xml_data->Element('password', sfConfig::get('app_clickatell_password'));
        $xml_data->Element('to',$mobileNumber);
        $xml_data->Element('text',$message );
        $xml_data->Pop('sendMsg');
        $xml_data->Pop('clickAPI');
        return $xml_data->GetXML();

    }
    private function postXML($xmlData)
    {
        $xmlData ="data=".$xmlData;
        $response= array();
        $browser = $this->getBrowser();
        $header['Content-Type'] =  "application/xml;charset=UTF-8";
        $header['Accept'] = "application/xml;charset=UTF-8";
        $uri = sfConfig::get('app_clickatell_posturl');
        $browser->post($uri,$xmlData);
        $xdoc = new DomDocument;
        $responseXML= $browser->getResponseText();
        $responseCode= $browser->getResponseCode();
        $this->createLogData($responseXML,'ResponseXml','smsLogs');
        //        echo $responseXML;
        //        die;
//                header("Content-type: application/xml");
//                print($responseXML);
//                die();

        if($responseXML!='' && $xdoc->LoadXML($responseXML) && ($responseCode==200 ||$responseCode==202))
        {
            if($xdoc->getElementsByTagName('fault')->item(0)=='')
            {
//                $response['apiMsgId']= $xdoc->getElementsByTagName('apiMsgId')->item(0)->nodeValue;
//                $response['sequence_no']= $xdoc->getElementsByTagName('sequence_no')->item(0)->nodeValue;
                $response['status'] = 'SEND_OK';

            }
            else
            {
//                 $response['fault']= $xdoc->getElementsByTagName('fault')->item(0)->nodeValue;
                 $response['status'] = 'NO_RECIPIENTS';

            }
        }
        return $response;

    }




   /**
  * Silent posting of request XML with node value.
  *
  */
    private function getBrowser() {
        if(!$this->browserInstance) {
            $this->browserInstance = new sfWebBrowser(array(), 'sfCurlAdapter',
                array('SSL_VERIFYPEER' => false, 'SSL_VERIFYHOST' => false));
        }
        return $this->browserInstance;
    }
    public function createLogData($xmlData,$nameFormate,$parentLogFolder='', $appendTime=true, $append = false)
    {
        $path = $this->getLogPath($parentLogFolder);
        if($appendTime){
            $file_name = $path."/".($nameFormate.'-'.date('Y_m_d_H:i:s')).".txt";
        }else{
            $file_name = $path."/".$nameFormate.".txt";
        }
        if($append)
        {
            @file_put_contents($file_name, $xmlData, FILE_APPEND);
        }else{
            $i=1;
            while(file_exists($file_name)) {
                $file_name = $path."/".($nameFormate.'-'.date('Y_m_d_H:i:s-')).$i.".txt";
                $i++;
            }
            @file_put_contents($file_name, $xmlData);
        }
    }



    public function getLogPath($parentLogFolder)
    {

        $logPath =$parentLogFolder==''?'/payEasylog':'/'.$parentLogFolder;
        $logPath =  sfConfig::get('sf_log_dir').$logPath.'/'.date('Y-m-d');

        if(is_dir($logPath)=='')
        {
            $dir_path=$logPath."/";
            mkdir($dir_path,0777,true);
            chmod($dir_path, 0777);
        }
        return $logPath;
    }
}
?>
