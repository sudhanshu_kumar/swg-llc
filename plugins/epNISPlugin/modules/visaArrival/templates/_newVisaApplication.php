<?php $kenyaVoa = ($countryId == 'KE')?true:false; ?>
<?php include_stylesheets_for_form($form) ?>
<?php include_javascripts_for_form($form) ?>
<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">
  <?php include_partial('global/innerHeading', array('heading' => 'Visa on Arrival Program (STEP 1)')); ?>
  <form name="vap_form" id="vap_form" action="<?php echo url_for('visa/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$encriptedAppId: '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?> class="dlForm" >


    <div id="printHTML">
        <div class="clear"><br></div>
            <div class="inst_bg_step_box" id="inst_bg_step1_id" style="display:block">
                <div class="info_box">
                    <ul>
                        <li>
                            <?php if($kenyaVoa){ ?>
                                <strong>Requirements for Transit/Tourist/Visitor Visa:</strong>
                                <ul>
                                    <li>Passport valid for at least 6 months.</li>
                                    <li>Duly completed visa application form.</li>
                                    <li>Two recent passport photographs.</li>                                    
                                    <li>Evidence of online payment for visa fee.</li>                                    
                                    <li>Only applicants with the above requirements would be allowed entrance into Nigeria following further interview.</li>
                                </ul>
                            <?php }else{ ?>
                                <strong>Requirements for Business/Investor Visa:</strong>
                                <ul>
                                    <li>Passport valid for at least 6 months.</li>
                                    <li>Duly completed visa application form.</li>
                                    <li>Two recent passport photographs.</li>
                                    <li>A Letter of invitation from a company/host in Nigeria accepting immigration responsibility.</li>
                                    <li>Evidence of online payment for visa fee.</li>
                                    <li>Self sponsored business men may not require letter of invitation but will be required to show evidence of sufficient funds.</li>
                                    <li>Only applicants with the above requirements would be allowed entrance into Nigeria following further interview.</li>
                                </ul>
                            <?php } ?>
                        </li>                        
                    </ul>
                </div>
            </div>
        <div id="notallowed" style="display: none" class="errorBox">
            Sorry, you are not qualified for this application
        </div>
            <div class="clear"></div>
            <div class="bodyContent2">

              <div class="formH3">
                <h2>Personal Information</h2>
                <?php if(!$kenyaVoa){ ?>
                <h2 class="pdLeft9">Applicant Information</h2>
                <?php } ?>
              </div>
              <div class="formContent_new <?php if(!$kenyaVoa){ echo 'formBrd'; } ?>">
                <div class="lblName_new"><?php echo $form['surname']->renderLabel(); ?><span class="redLbl">*</span></div>
                <div class="lblField">
                  <?php echo $form['surname']->render(); ?>
                  <div class="error_msg" id="vap_application_surname_error"> <?php echo $form['surname']->renderError(); ?></div>
                  <i><span class="small-text" >(Please avoid placing suffixes with your surname)</span></i>
                </div>
                <div class="lblName_new"><?php echo $form['first_name']->renderLabel(); ?><span class="redLbl">*</span></div>
                <div class="lblField">
                  <?php echo $form['first_name']->render(); ?>
                  <div class="error_msg" id="vap_application_first_name_error"> <?php echo $form['first_name']->renderError(); ?></div>
                </div>                
                <div class="lblName_new"><?php echo $form['middle_name']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField">
                  <?php echo $form['middle_name']->render(); ?>
                  <div class="error_msg" id="vap_application_other_name_error"> <?php echo $form['middle_name']->renderError(); ?></div>
                </div>
                <div class="lblName_new"><?php echo $form['gender']->renderLabel(); ?><span class="redLbl">*</span></div>
                <div class="lblField">
                  <?php echo $form['gender']->render(); ?>
                  <div class="error_msg" id="vap_application_gender_error"> <?php echo $form['gender']->renderError(); ?></div>
                </div>
                
                <div class="lblName_new"><?php echo $form['email']->renderLabel(); ?><span class="redLbl">*</span></div>
                <div class="lblField">
                  <?php echo $form['email']->render(); ?>
                  <div class="error_msg" id="vap_application_email_error"> <?php echo $form['email']->renderError(); ?></div>
                </div>
                <div class="lblName_new"><?php echo $form['date_of_birth']->renderLabel(); ?><span class="redLbl">*</span></div>
                <div class="lblField">
                  <?php echo $form['date_of_birth']->render(); ?>
                  <div class="error_msg" id="vap_application_date_of_birth_error"> <?php echo $form['date_of_birth']->renderError(); ?></div>
                </div>
                <div class="lblName_new"><?php echo $form['place_of_birth']->renderLabel(); ?><span class="redLbl">*</span></div>
                <div class="lblField">
                  <?php echo $form['place_of_birth']->render(); ?>
                  <div class="error_msg" id="vap_application_place_of_birth_error"> <?php echo $form['place_of_birth']->renderError(); ?></div>
                </div>
              </div>

              <?php if(!$kenyaVoa){ ?>
              <div class="formContent_new pdLeft9">
              <div class="lblName"><?php echo $form['applicant_type']->renderLabel(); ?><span class="redLbl">*</span></div>
                <div class="lblField">
                    <?php echo $form['applicant_type']->render(); ?>
                    <div class="error_msg" id="vap_application_applicant_type_error"> <?php echo $errorFields['Step 5'][] = $form['applicant_type']->renderError(); ?></div>
                </div>                
                <div id="vap_hdn_business_address">
                  <div class="lblName"  id="vap_hdn_business_address_label"><?php echo $form['business_address']->renderLabel(); ?><span class="redLbl">*</span></div>
                  <div class="lblField">
                      <?php echo $form['business_address']->render(); ?>
                      <div class="error_msg" id="business_address_error"> <?php echo $errorFields['Step 5'][] = $form['business_address']->renderError(); ?></div>
                  </div>
                </div>
                
                <div id="vap_hdn_business_investor">
                  <div class="lblName">Business Investor <span class="redLbl">*</span></div>
                  <div class="lblField">
                      <input type="radio" name="business_investor" id="business_investor_yes" value="yes" checked="checked"  onclick="javascript:businessInvestor('yes');">&nbsp;Yes
                      <input type="radio" name="business_investor" id="business_investor_no" value="no"  onclick="javascript:businessInvestor('no');">&nbsp;No
                  </div>
                </div>
                </div>

               <?php }//End of if(!$kenyaVoa){...  ?>
                
            </div>
            <div class="clear"></div>
          <input type="hidden" class="normalbutton" name="countryId" value="<?php echo $countryId;?>" />

    </div> <div class="clear">&nbsp;</div>
        <div class="pixbr_new X50_new">
            <p class="text_hide">
                
                    <input type="checkbox" name="i_agree" id="i_agree" value="1">&nbsp;
                    I agree to the indicated requirements, and admit total responsibility for any query that follows my non-compliance of the stated requirements.
            </p>
          </div>
                 <div class="clear">&nbsp;</div>
        <div class="border_top"></div>
          
          <div class="pixbr_new X50_new">
            <p class="text_hide"><font color="red">*</font> - Compulsory fields</p>
          </div>
      <center id="multiFormNav">
        <table align="center" class="nobrd">            
          <tr><td>
              <input type="button"  class="normalbutton"  id="multiFormSubmit" value="Next" onclick="checkForm();" />
              <input type="hidden" id="appId" name="appId"  class="normalbutton" value="" />
              <input type="hidden" id="appSatusFlag" name="appSatusFlag"  class="normalbutton" value="" />              
              <input type="hidden" id="type" name="type"  class="normalbutton" value="<?php echo base64_encode($countryId); ?>" />
              <input type="hidden" id="printUrl" name="printUrl"  class="normalbutton" value="" />
              <input type="hidden" id="formType" name="formType"  class="normalbutton" value="" />
        </td></tr></table>
      </center>
    <?php if ($form->getObject()->isNew()) {?>
      <?php }else if (!$form->getObject()->isNew()) { ?>
    <input type="hidden" name="sf_method" value="put" />
    <?php } ?>

  </form>
<div id="popupContent"></div>
<div id="backgroundPopup"></div>
<div id="loading" class="no_display"><?php echo image_tag('/images/ajax-loader_1.gif',array('alt'=>'Loading ...', 'align' => 'absmiddle'));?><br/>&nbsp;&nbsp;Please wait...</div>
<script>
    function businessInvestor(checked)
    {
        if(checked == 'yes')
        {
            document.getElementById("inst_bg_step1_id").style.display = 'block';
            document.getElementById("notallowed").style.display = 'none';
            $('#multiFormSubmit').attr("disabled","");
        }
        else
        {
            document.getElementById("inst_bg_step1_id").style.display = 'none';
            document.getElementById("notallowed").style.display = 'block';
            $('#multiFormSubmit').attr("disabled","disabled");
        }
    }
    
    function validateAppName(str)
    {
      var reg = /^[A-Za-z \-\.]*$/; //allow alphabet and spaces only...
      if(reg.test(str) == false) {
        return 1;
      }

      return 0;
    }

    function validateEmail(email) {

    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    if(reg.test(email) == false) {

      return 1;
    }

    return 0;
  }
    function formValidate(){
        var first_name = jQuery.trim($('#vap_application_first_name').val());
        var last_name = jQuery.trim($('#vap_application_surname').val());
        var gender_id = jQuery.trim($('#vap_application_gender').val());
        var day = jQuery.trim($('#vap_application_date_of_birth_day').val());
        var month = jQuery.trim($('#vap_application_date_of_birth_month').val());
        var year = jQuery.trim($('#vap_application_date_of_birth_year').val());
        var place_of_birth = jQuery.trim($('#vap_application_place_of_birth').val());
        var email = jQuery.trim($('#vap_application_email').val());
        <?php if(!$kenyaVoa){ ?>
        var business_address = jQuery.trim($('#vap_application_business_address').val());
        <?php } ?>


        var err  = 0;
        if(first_name == "")
        {
          $('#vap_application_first_name_error').html("Please enter First Name");
          err = err+1;
        }else if(validateAppName(first_name) != 0)
        {
          $('#vap_application_first_name_error').html("Please enter valid First Name");
          err = err+1;
        }
        else
        {
          $('#vap_application_first_name_error').html("");
        }
        <?php if(!$kenyaVoa){ ?>
        if(business_address == "")
        {
            if($("input[name='vap_application[applicant_type]']:checked").next('label').text() == 'Government Business'){
                $('#business_address_error').html("Please enter Corresponding Government Agency in Nigeria (include address) ");
            }else if($("input[name='vap_application[applicant_type]']:checked").next('label').text() == 'Private Business'){
                $('#business_address_error').html("Please enter Name of Company (include address) ");
            }
            err = err+1;
        }
        else
        {
            if(business_address.length > 255 ){
                $('#business_address_error').html("Address can not contains more than 255 Characters.");
                err = err+1;
            }else{
                $('#business_address_error').html("");
            }
        }
        <?php } ?>
        if(last_name == "")
        {
          $('#vap_application_surname_error').html("Please enter Last Name");
          err = err+1;
        }else if(validateAppName(last_name) != 0)
        {
          $('#vap_application_surname_error').html("Please enter valid Last Name");
          err = err+1;
        }
        else
        {
          $('#vap_application_surname_error').html("");
        }


        if(gender_id == "")
        {
          $('#vap_application_gender_error').html("Please select Gender");
          err = err+1;
        }
        else
        {
          $('#vap_application_gender_error').html("");
        }

        var date_of_birth_flag = false;
        if(day == "")
        {
          date_of_birth_flag = true;
        }
        if(month == "")
        {
          date_of_birth_flag = true;
        }
        if(year == "")
        {
          date_of_birth_flag = true;
        }

        var current_date  = new Date();

        if(date_of_birth_flag){
            $('#vap_application_date_of_birth_error').html("Please select Date of Birth");
            err = err+1;
        }else{

            var date_of_birth = new Date(year,month-1,day);
            if(date_of_birth.getTime() > current_date.getTime()) {
                $('#vap_application_date_of_birth_error').html("Date of birth cannot be Future Date");
                err = err+1;
            }else{
                $('#vap_application_date_of_birth_error').html("");
            }
        }

        if(place_of_birth == "")
        {
          $('#vap_application_place_of_birth_error').html("Please enter Place of Birth");
          err = err+1;
        }else if(validateAppName(place_of_birth) != 0)
        {
          $('#vap_application_place_of_birth_error').html("Please enter valid Place of Birth");
          err = err+1;
        }
        else
        {
          $('#vap_application_place_of_birth_error').html("");
        }

        if(email == "")
        {
            $('#vap_application_email_error').html("Please enter Email");
            err = err+1;
        }
        else if(validateEmail(email) != 0)
        {
            $('#vap_application_email_error').html("Please enter Valid Email");
            err = err+1;
        }
        else
        {
            $('#vap_application_email_error').html("");
        }

        if(err == 0){
            if(document.getElementById('i_agree').checked==false)
            {
                alert('Please select “I Agree” checkbox.');
                document.getElementById('i_agree').focus();
                return false;
            }
            return true;
        }else{
            return false;
        }

    }

    function checkForm() {

        

        if(!formValidate()){
            return false;
        }

        $('#multiFormSubmit').attr("disabled","disabled");
        $('#multiFormSubmit').val("Please wait...");

        var url = "<?php echo url_for('visaArrival/newVisaApplicationAjax'); ?>";

        centerPopup();
        var elementPos = findElementPosition('multiFormSubmit');
        elementPos = elementPos.split(',');
        $("#popupContent").css({
                "position": "absolute",
                "top": (parseInt(elementPos[1])-190)+'px',
                "left":(parseInt(elementPos[0])-90)+'px',
                "width":'230px'
        });
        $("#popupContent").html($('#loading').html());
        loadPopup();        
        $.ajax({
            type: "POST",
            url: url,
            data: $('#vap_form').serialize(),            
            success: function (data) {                
                data = jQuery.trim(data);
                if(data == 'notfound'){
                    var url = '<?php echo url_for('visaArrival/newvisa?type='.base64_encode($countryId));?>';
                    $('#appSatusFlag').val('new');
                    document.vap_form.action = url;
                    document.vap_form.submit();
                }else if(data == 'error'){
                    $('#popupContent').hide('slow');
                    alert('Oops! Some technical problem occur. Please try it again.');
                }else{
                    $('#popupContent').html(data);
                    $("#popupContent").css({
                        "width": '600px'
                    });

                    var elementPos = findElementPosition('multiFormSubmit');
                    elementPos = elementPos.split(',');
                    $("#popupContent").css({
                            "position": "absolute",
                            "top": (parseInt(elementPos[1])-290)+'px',
                            "left":(parseInt(elementPos[0])-240)+'px'
                    });
                    
                    $('#multiFormSubmit').attr("disabled","");
                    $('#multiFormSubmit').val("Next");
                }
            }//End of success: function (data) {...
        });

    }//End of function checkForm() {...

    $(document).ready(function(){
        checkDisplayCompanyCustomer();
    })

    function openExistingApp(appId){
       $('#appSatusFlag').val('old');       
       if(appId != ''){
           var url = '<?php echo url_for('visaArrival/newVisaApplication');?>';
           $('#appId').val(appId);
           document.vap_form.action = url;
           document.vap_form.submit();
       }else{
           alert("application does not exist.");
       }
    }

    function openNewApp(type){        
        $('#formType').val(type);
        var url = '<?php echo url_for('visaArrival/newvisa?type='.base64_encode($countryId));?>';
        document.vap_form.action = url;
        document.vap_form.submit();
    }

    function printExistingApp(printUrl){
        location.href = printUrl;
    }

    function checkDisplayCompanyCustomer(){

        if($("input[name='vap_application[applicant_type]']:checked").next('label').text() == 'Government Business'){
            document.getElementById("vap_hdn_business_address_label").innerHTML = 'Corresponding Government Agency in Nigeria (include address) <span class="redLbl">*</span>';
            //$('#vap_hdn_business_address_label').innerHTML('Corresponding Government Agency in Nigeria (include address)');

        }else if($("input[name='vap_application[applicant_type]']:checked").next('label').text() == 'Private Business'){
            document.getElementById("vap_hdn_business_address_label").innerHTML = 'Name of Company (include address) <span class="redLbl">*</span>';
            //$('#vap_hdn_business_address_label').innerHTML('Name of Company (include address)');
        }
    }
//    onload=loadImage;
//    function loadImage(){
//        alert("a");
//    }
</script>
</div>
</div>
<div class="content_wrapper_bottom"></div>