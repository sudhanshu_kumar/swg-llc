<?php
function getDateFormate($date)
{
 $dateFormate = date("d-m-Y", strtotime($date));

  return $dateFormate;
}

/**
 * [WP: 089] => CR: 128
 * Setting non kenya flag...
 */
$nonKenya = ($visa_application[0]['applying_country_id'] != 'KE')?true:false;

?>
<link rel="stylesheet" type="text/css" media="print" href="<?php  echo stylesheet_path('print');?>" />
<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">
  <?php if ($sf_user->hasFlash('error')): ?>
  <div id="flash_notice" class="alertBox" > <?php echo nl2br($sf_user->getFlash('error'));?></div>
  <?php  endif  ?>
  <div class="noscreen"> <img src="<?php echo image_path('nis.gif'); ?>" style="height: 63px; width: 45px;" alt="Test" class="pd4" /> </div>
    <?php include_partial('global/innerHeading',array('heading'=>"Visa on Arrival Program"));?>
    <div class="clear"></div>
    <div class="tmz-spacer"></div>
    <div id="printHTML">
      <div class="clear_print"></div>
      <div class="page_heading_new">Visa on Arrival Program</div>
      <div class="inst_bg">
          <div class="inst_container1">
                <ul>
                  <li>FOR ANY QUESTIONS AND/OR CONCERNS PLEASE CONTACT US AT <a href="mailto:support@swgloballlc.com" class="mail_link">support@swgloballlc.com</a></li>
                  <li>APPLICATION FEES PAID FOR VISA/FREEZONE ARE NON REFUNDABLE EXCEPT IN CASED OF DOUBLE PAYMENT MISTAKENLY MADE FOR THE SAME APPLICATION OR FRAUDULENT USE OF CREDIT/DEBIT CARD. TO SEEK A REFUND PLEASE CONTACT <a href="mailto:refund@swgloballlc.com" class="mail_link">refund@swgloballlc.com</a></li>
                  <li>PAYMENTS ARE VALID FOR SIX (6) MONTHS ONLY FROM THE PAYMENT DATE.</li>
                  <li>ONLY online payment is acceptable.</li>
                  <li>Anyone who pays otherwise and receives service is subject to prosecution and revocation of Visa.</li>
                  <li>If you have already completed an application, please check your application status rather than completing a duplicate application</li>
                  <li>Expatriate Quota is not a requirement for Free Zone Entry Visa</li>
                </ul>
              </div>
            </div>
            <div class="clear"></div>
            <?php if($visa_application[0]['status'] == 'Rejected'){ ?>
            <div class="block red">
                Your application has been rejected<?php echo (empty($visa_application[0]['remarks']))?' due to the following reason(s):':'.';?>
                <br />
                <ul>
                <?php if($visa_application[0]['contagious_disease'] == 'Yes'){ ?>
                <li> You have been infected by any contagious disease (e.g. Tuberculosis) or suffered serious mental illness.</li>
                <?php } ?>
                <?php if($visa_application[0]['police_case'] == 'Yes'){ ?>
                <li> You have been arrested or convicted for an offence (even though subject to pardon).</li>
                <?php } ?>
                <?php if($visa_application[0]['narcotic_involvement'] == 'Yes'){ ?>
                <li> You have been involved in narcotic activity.</li>
                <?php } ?>
            </div>
            <div class="clear"></div>
            <?php } ?>
           <?php if($visa_application[0]['status'] == 'Expired'){ ?>
            <div class="block red">
                Your application has been expired. Application validity on this portal is <?php echo (int)sfConfig::get('app_visa_arrival_flight_update_limit')/30; ?> months from the date of payment.
                <br /><br />
                <strong>Note:</strong> Payment date of your application is <?php $datetime = date_create($visa_application[0]['paid_date']); echo date_format($datetime, 'd/F/Y');?>.
            </div>
            <div class="clear"></div>
            <?php } ?>
            <div class='confirmation-msg'>
        <div class="confirmation-msg1">
                <p>
                  <center>
              <b class="red">BELOW IS YOUR APPLICATION ID AND REFERENCE NUMBER</b>
                  </center>
                </p>
                <p>
                  <center>
              <b class="red">KINDLY PRINT THE SCREEN AND KEEP IT SAFE</b>
                  </center>
                </p>
                <p>
                  <center>
              <b class="red">YOU WILL NEED THEM TO COMPLETE THE PROCESS AT THE PORT OF ENTRY, CONSULATE OR HIGH COMMISSION</b>
                  </center>
                </p>
              </div>
              <h5>
                <center>
            Application Id: <?php echo $visa_application[0]['id']; ?>
                </center>
              </h5>
              <h5>
                <center>
            Reference No: <?php echo $visa_application[0]['ref_no'] ?>
                </center>
              </h5>
            </div>
      <div class="clear">&nbsp;</div>
      <table cellpadding="0" cellspacing="0" border="0" class="no_border_new" width="695">
                  <tr>
          <td valign="top"  id="uiGroup_Step_1_of_4"><table cellpadding="0" cellspacing="0" border="0" class="no_border_new" width="695">
              <tr>
                <td width="347" height="25" valign="middle" id="form_new_heading">Personal Information</td>
                <td rowspan="32" align="center" valign="top" id="border_new_left"></td>
                <td width="347" height="25" valign="middle" id="form_new_heading">&nbsp;</td>
              </tr>
              <tr>
                <td valign="top" height="5"></td>
                <td valign="top" height="5"></td>
              </tr>
              <tr>
                <td valign="top"><table width="347" border="0" cellspacing="0" cellpadding="0" class="no_border_new">
                    <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_title">Title</label></td>
                    <td width="175" valign="top" class="lblfield_new"><span class="lblField">
                      <?php
                    if(!empty ($visa_application[0]['title'])){
                      echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['title']."'>";
                    }
                    else{
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                    }
                ?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_surname">Last Name (<i>Surname</i>)</label></td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php
                  if($visa_application[0]['surname']){
                    echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['surname']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }
                  ?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_first_name">First Name</label></td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php
                  if(!empty ($visa_application[0]['first_name'])){
                    echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['first_name']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_middle_name">Middle Name</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php
                  if(!empty ($visa_application[0]['middle_name'])){
                    echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['middle_name']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='' readonly = 'readonly' >";
                  }?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_gender">Gender</label></td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php
                  if(!empty ($visa_application[0]['gender'])){
                    echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['gender']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }?>
                      </span></td>
                  </tr>
                  <tr>
                    <td valign="top" class="label_name_new"><label for="visa_application_marital_status">Marital Status</label></td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php
                  if(!empty ($visa_application[0]['marital_status'])){
                    echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['marital_status']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }?>
                      </span></td>
                  </tr>
                  <tr>
                    <td valign="top" class="label_name_new"><label for="visa_application_email">Email</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php
                  if(!empty ($visa_application[0]['email'])){
                    echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['email']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }?>
                      </span></td>
                  </tr>
                  <tr>
                    <td valign="top" class="label_name_new"><label for="visa_application_date_of_birth">Date of Birth (dd-mm-yyyy)</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php
                  if(!empty ($visa_application[0]['date_of_birth'])){
                    $explode = explode('-',getDateFormate($visa_application[0]['date_of_birth']));
                    echo "<input type='text' class='printdate' readonly=readonly value='".$explode[0]."'>";
                    echo "<input type='text' class='printdate' readonly=readonly value='".$explode[1]."'>";
                    echo "<input type='text' class='printyear' readonly=readonly value='".$explode[2]."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }?>
                      </span></td>
                  </tr>
                  <tr>
                    <td valign="top" class="label_name_new"><label for="visa_application_place_of_birth">Place of Birth</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php
                  if(!empty ($visa_application[0]['place_of_birth'])){
                    echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['place_of_birth']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }?>
                      </span></td>
                  </tr>
                  </table></td>
                <td valign="top"><table width="347" border="0" cellspacing="0" cellpadding="0" class="no_border_new">
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_present_nationality_id">Present Nationality</label>
                    </td>
                    <td width="175" valign="top" class="lblfield_new"><span class="lblField">
                      <?php
                  if(!empty ($visa_application[0]['CurrentCountry']['country_name'])){
                  echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['CurrentCountry']['country_name']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }?>
                      </span></td>
                  </tr>                  
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_hair_color">Color of Hair</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php
                  if(!empty ($visa_application[0]['hair_color'])){
                    echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['hair_color']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_eyes_color">Color of Eyes</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php
                  if(!empty ($visa_application[0]['eyes_color'])){
                  echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['eyes_color']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_id_marks">Identification Marks</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php
                  if(!empty ($visa_application[0]['id_marks'])){
                    echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['id_marks']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }?>
                      </span></td>
                  </tr>
                  <tr>
                    <td valign="top" class="label_name_new"><label for="visa_application_height">Height (in cm)</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php
                    if(!empty ($visa_application[0]['height'])){
                    echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['height']."'>";
                    }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }?>
                      </span></td>
                  </tr>
                  </table></td>
              </tr>
                  <tr>
                <td valign="top" height="5"></td>
                <td valign="top" height="5"></td>
              </tr>
              <tr>
                <td height="25" valign="middle" id="form_new_heading"><?php echo ($nonKenya)?'Office':'Permanent'; ?> Address</td>
                <td valign="middle" id="form_new_heading">&nbsp;</td>
              </tr>
              <tr>
                <td valign="top" height="5"></td>
                <td valign="top" height="5"></td>
              </tr>
              <tr>
                <td valign="top"><table width="347" border="0" cellspacing="0" cellpadding="0" class="no_border_new">
                    <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_VisaOfficeAddressForm_address_1">Address 1</label>
                    </td>
                    <td width="175" valign="top" class="lblfield_new"><span class="lblField">
                      <?php
                  if(!empty ($visa_application[0]['VapVisaOfficeAddress']['address_1'])){
                    echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['VapVisaOfficeAddress']['address_1']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_VisaOfficeAddressForm_address_2">Address 2</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php
                  if(!empty ($visa_application[0]['VapVisaOfficeAddress']['address_2'])){
                    echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['VapVisaOfficeAddress']['address_2']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }?>
                      </span></td>
                  </tr>
                  
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_VisaOfficeAddressForm_city">City</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php if(!empty ($visa_application[0]['VapVisaOfficeAddress']['city'])){
                    echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['VapVisaOfficeAddress']['city']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_VisaOfficeAddressForm_country_id">Country</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php

                  if(!empty ($visa_application[0]['VapVisaOfficeAddress']['Country']['country_name'])){
                    echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['VapVisaOfficeAddress']['Country']['country_name']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }?>
                      </span></td>
                  </tr>
                  </table></td>
                <td valign="top"><table width="347" border="0" cellspacing="0" cellpadding="0" class="no_border_new">
                  
                  
                  
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_VisaOfficeAddressForm_state">State</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php
                  if(!empty ($visa_application[0]['VapVisaOfficeAddress']['state'])){
                    echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['VapVisaOfficeAddress']['state']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }?>
                      </span></td>
                  </tr>
                  <tr>
                    <td valign="top" class="label_name_new"><label for="visa_application_VisaOfficeAddressForm_postcode">Postcode</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php
                  if(!empty ($visa_application[0]['VapVisaOfficeAddress']['postcode'])){
                    echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['VapVisaOfficeAddress']['postcode']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='' readonly = 'readonly' >";
                  }?>
                      </span></td>
                  </tr>
                  <tr>
                    <td valign="top" class="label_name_new"><label for="visa_application_office_phone_no">Office Phone</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php
                  if(!empty ($visa_application[0]['office_phone_no'])){
                    echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['office_phone_no']."'>";
                  }
                  else
                  {
                     echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }?>
                      </span></td>
                  </tr>
                  </table></td>
              </tr>
                  <tr>
                <td valign="top" height="5"></td>
                <td valign="top" height="5"></td>
              </tr>
              <tr>
                <td valign="top" height="5"></td>
                <td valign="top" height="5"></td>
              </tr>
            </table></td>
        </tr>
        <tr>
          <td valign="top" id="uiGroup_Step_2_of_4"><table cellpadding="0" cellspacing="0" border="0" class="no_border_new" width="695">
              <tr>
                <td width="347" height="25" valign="middle" id="form_new_heading">Passport Information</td>
                <td  height="25" rowspan="39" align="center" valign="top" id="border_new_left"></td>
                <td width="347" height="25" valign="middle" id="form_new_heading">Visa Processing Information</td>
              </tr>
              <tr>
                <td valign="top" height="5"></td>
                <td valign="top" height="5"></td>
              </tr>
              <tr>
                <td valign="top"><table width="347" border="0" cellspacing="0" cellpadding="0" class="no_border_new">
                    <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_issusing_govt">Issuing Country</label>
                    </td>
                    <td width="175" valign="top" class="lblfield_new"><span class="lblField">
                        <?php if(!empty ($visa_application[0]['issusing_govt'])){echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['issusing_govt']."'>";
                  }else{
                echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                }?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_passport_number">Passport Number</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php if(!empty ($visa_application[0]['passport_number'])){
                    echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['passport_number']."'>";
                  }else{


                echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                } ?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_date_of_issue">Date of Issue (dd-mm-yyyy)</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php if(!empty ($visa_application[0]['date_of_issue']))
                  {

                    $explode = explode('-',getDateFormate($visa_application[0]['date_of_issue']));
                    echo "<input type='text' class='printdate' readonly=readonly value='".$explode[0]."'>";
                    echo "<input type='text' class='printdate' readonly=readonly value='".$explode[1]."'>";
                    echo "<input type='text' class='printyear' readonly=readonly value='".$explode[2]."'>";

                }else{
                    echo "<input type='text' class='printdate' readonly=readonly value='--'>";
                    echo "<input type='text' class='printdate' readonly=readonly value='--'>";
                    echo "<input type='text' class='printyear' readonly=readonly value='--'>";
                } ?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_date_of_exp">Expiry Date (dd-mm-yyyy)</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php if(!empty ($visa_application[0]['date_of_exp'])){

                    $explode = explode('-',getDateFormate($visa_application[0]['date_of_exp']));
                    echo "<input type='text' class='printdate' readonly=readonly value='".$explode[0]."'>";
                    echo "<input type='text' class='printdate' readonly=readonly value='".$explode[1]."'>";
                    echo "<input type='text' class='printyear' readonly=readonly value='".$explode[2]."'>";
                  }else{
                    echo "<input type='text' class='printdate' readonly=readonly value='--'>";
                    echo "<input type='text' class='printdate' readonly=readonly value='--'>";
                    echo "<input type='text' class='printyear' readonly=readonly value='--'>";
                  } ?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_place_of_issue">Place of Issue</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                        <?php if(!empty ($visa_application[0]['place_of_issue'])){echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['place_of_issue']."'>";}else{
                echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                } ?>
                      </span></td>
                  </tr>
                  </table></td>
                <td valign="top"><table width="347" border="0" cellspacing="0" cellpadding="0" class="no_border_new">
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_applying_country_id">Type of Visa</label>
                    </td>
                    <td valign="top" class="lblfield_new "><span class="lblField">
                    <?php
                    $visaType  = Doctrine::getTable('VisaType')->findById($visa_application[0]['type_of_visa']);
                    ?>
                    <input type='text' class='print_input_box' value='<?php echo ucwords($visaType[0]['var_value']); ?>' readonly=readonly>
                    </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_applying_country_id">Country Applying From</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                        <?php if(!empty ($visa_application[0]['applying_country_id'])){
                          $country  = Doctrine::getTable('Country')->findById($visa_application[0]['applying_country_id']);
                         echo "<input class='print_input_box' type='text' readonly=readonly value='".$country[0]['country_name']."'>";
                        }else{
                            echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                } ?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_trip_money">Money for the trip (USD)</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                        <?php if(!empty ($visa_application[0]['trip_money'])){
                         echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['trip_money']."'>";
                        }else{
                            echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                } ?>
                      </span></td>
                  </tr>
                  </table></td>
              </tr>

                  <tr>
                <td valign="top" height="5"></td>
                <td valign="top" height="5"></td>
              </tr>
            </table></td>
        </tr>
        <tr>
          <td valign="top" id="uiGroup_Step_3_of_4"><table cellpadding="0" cellspacing="0" border="0" class="no_border_new" width="695">
              <tr>
                <td width="347" height="25" valign="middle" id="form_new_heading">Corresponding Local Company</td>
                <td  rowspan="32" align="center" valign="top" id="border_new_left"></td>
                <td width="347" height="25" valign="middle" id="form_new_heading">&nbsp;</td>
              </tr>
              <tr>
                <td valign="top" height="5"></td>
                <td valign="top" height="5"></td>
              </tr>
              <tr>
                <td valign="top"><table width="347" border="0" cellspacing="0" cellpadding="0" class="no_border_new">
                    <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_local_company_name">Local company name</label>
                    </td>
                    <td width="175" valign="top" class="lblfield_new"><span class="lblField">
                      <?php  if(!empty ($visa_application[0]['local_company_name'])){
                      echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['local_company_name']."'>";
                    }else{echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";} ?>
                      </span></td>
                  </tr>
                  <?php $local_address = Doctrine::getTable('AddressMaster')->findById($visa_application[0]['local_company_address_id']);

                  ?>
                  <tr>
                    <td valign="top" height="2"></td>
                    <td valign="top" height="2"></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_local_address_1">Address 1</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php if(!empty ($local_address[0]['address_1'])){
                      echo "<textarea class='' readonly= readonly class='auto-height'>".$local_address[0]['address_1']."</textarea>";
                    }else{echo "<textarea readonly= readonly class='auto-height'></textarea>";} ?>
                      </span></td>
                  </tr>
                  <tr>
                     <td valign="top" height="2" class="noPrint"></td>
                    <td valign="top" height="2" class="noPrint"></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_local_address_2">Address 2</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php if(!empty ($local_address[0]['address_2'])){
                      echo "<textarea class='' type='text' readonly=readonly value='".$local_address[0]['address_2']."</textarea>";
                    }else{echo  "<textarea readonly= readonly class='auto-height'></textarea>";} ?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_local_city">City</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php if(!empty ($local_address[0]['city'])){
                      echo "<textarea class='' readonly= readonly class='auto-height'>".$local_address[0]['city']."</textarea>";
                    }else{echo "<textarea readonly= readonly class='auto-height'></textarea>";} ?>
                      </span></td>
                  </tr>
                  </table></td>
                <td valign="top"><table width="347" border="0" cellspacing="0" cellpadding="0" class="no_border_new">
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_local_country">Country</label>
                    </td>
                    <td width="175" valign="top" class="lblfield_new"><span class="lblField">
                      <?php if(!empty ($local_address[0]['country_id'])){
                        $country  = Doctrine::getTable('Country')->findById($local_address[0]['country_id']);
                      echo "<input class='print_input_box' type='text' readonly=readonly value='".$country[0]['country_name']."'>";
                    }else{echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";} ?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_local_state">State</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php if(!empty ($local_address[0]['state'])){
                        $state = Doctrine::getTable('State')->findById($local_address[0]['state']);
                         echo "<input class='print_input_box' type='text' readonly=readonly value='".$state[0]['state_name']."'>";
                    }else{echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";} ?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_local_lga">LGA</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php if(!empty ($local_address[0]['lga_id'])){
                        $lga = Doctrine::getTable('Lga')->findById($local_address[0]['lga_id']);
                         echo "<input class='print_input_box' type='text' readonly=readonly value='".$lga[0]['lga']."'>";
                    }else{echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";} ?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_local_district">District</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php if(!empty ($local_address[0]['district'])){
                         echo "<input class='print_input_box' type='text' readonly=readonly value='".$local_address[0]['district']."'>";
                    }else{echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";} ?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_local_postcode">Postcode</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php if(!empty ($local_address[0]['postcode'])){
                         echo "<input class='print_input_box' type='text' readonly=readonly value='".$local_address[0]['postcode']."'>";
                    }else{echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";} ?>
                      </span></td>
                  </tr>
                  </table></td>
              </tr>
              <tr>
                <td valign="top" height="5"></td>
                <td valign="top" height="5"></td>
              </tr>
            </table></td>
        </tr>        
        <tr>
          <td valign="top" id="uiGroup_Step_4_of_4"><table cellpadding="0" cellspacing="0" border="0" class="no_border_new" width="695">
              <tr>
                <td width="347" height="25" valign="middle" id="form_new_heading">Travel History</td>
                <td  rowspan="32" align="center" valign="top" id="border_new_left"></td>
                <td width="347" height="25" valign="middle" id="form_new_heading">&nbsp;</td>
              </tr>
              <tr>
                <td valign="top" height="5"></td>
                <td valign="top" height="5"></td>
              </tr>
              <tr>
                <td valign="top"><table width="347" border="0" cellspacing="0" cellpadding="0" class="no_border_new">                    
                    <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_contagious_disease">Have you ever been infected by any contagious disease (e.g. Tuberculosis) or suffered serious mental illness?</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php if(!empty ($visa_application[0]['contagious_disease'])){
                      echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['contagious_disease']."'>";
                    }else{echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";} ?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_police_case">Have you ever been arrested or convicted for an offence (even though subject to pardon)?</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php if(!empty ($visa_application[0]['police_case'])){
                      echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['police_case']."'>";
                    }else{echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";} ?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_narcotic_involvement">Have you ever been involved in narcotic activity?</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php if(!empty ($visa_application[0]['narcotic_involvement'])){
                      echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['narcotic_involvement']."'>";
                    }else{echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";} ?>
                      </span></td>
                  </tr>
                  </table></td>
                <td valign="top"><table width="347" border="0" cellspacing="0" cellpadding="0" class="no_border_new">
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_deported_status">Have you ever been deported?</label>
                    </td>
                    <td width="175" valign="top" class="lblfield_new"><span class="lblField">
                      <?php if(!empty ($visa_application[0]['deported_status'])){
                      echo"<input class='print_input_box' type='text' readonly=readonly value='". $visa_application[0]['deported_status']."'>";
                    }else{echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";} ?>
                      <?php if($visa_application[0]['deported_status'] == "Yes"){ ?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_deported_county_id">If you have ever been deported from which country?</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php if(!empty ($visa_application[0]['deported_county_id'])){
                    $country  = Doctrine::getTable('Country')->findById($visa_application[0]['deported_county_id']);
                    echo "<input class='print_input_box' type='text' readonly=readonly value='".$country[0]['country_name']."'>";
                    }else{echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";} ?>
                      <?php } ?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_visa_fraud_status">Have you sought to obtain<br/>
                      visa by mis-representation or fraud?</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php if(!empty ($visa_application[0]['visa_fraud_status'])){
                      echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['visa_fraud_status']."'>";
                    }else{echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";} ?>
                      </span></td>
                  </tr>
                  </table></td>
              </tr>

            </table></td>
        </tr>
        <tr>
          <td valign="top" id="uiGroup_Step_4_of_4">

         <?php if($nonKenya){ ?>

          <table cellpadding="0" cellspacing="0" border="0" class="no_border_new" width="695">
              <tr>
                <td width="347" height="25" valign="middle" id="form_new_heading">Applicant Information</td>
                <td  rowspan="32" align="center" valign="top" id="border_new_left"></td>
                <td width="347" height="25" valign="middle" id="form_new_heading">&nbsp;</td>
              </tr>
              <tr>
                <td valign="top" height="5"></td>
                <td valign="top" height="5"></td>
              </tr>
              <tr>
                <td valign="top"><table width="347" border="0" cellspacing="0" cellpadding="0" class="no_border_new">

                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_applicant_type">Applicant type</label>
                    </td>
                    <td valign="top" class="lblfield_new"><div class="lblField_radio">
                        <?php
                    $multiFlg='';
                    $singleFlg = '';
                    if($visa_application[0]['applicant_type'] == "GB"){
                      $singleFlg = "checked";
                    }else if($visa_application[0]['applicant_type'] == "PB"){
                      $multiFlg = "checked";
                    }
                    echo "<input type='radio' disabled $singleFlg class='align-right' > Government Business ";echo"<br/>";echo "<input type='radio' disabled $multiFlg class='align-right' > Private Business";?>
                      </div></td>
                  </tr>
                  <?php if($visa_application[0]['applicant_type'] == "GB"){ ?>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_vap_company_id">Corresponding Government Agency in Nigeria (include address)</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php if(!empty ($visa_application[0]['business_address'])){
                      echo "<textarea class='' readonly= readonly class='auto-height'>".$visa_application[0]['business_address']."</textarea>";
                    }else{echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";} ?>
                      </span></td>
                  </tr>
                  <?php }
                  if($visa_application[0]['applicant_type'] == "PB"){ ?>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_customer_service_number">Name of Company (include address)</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php if(!empty ($visa_application[0]['business_address'])){
                      echo "<textarea class='' readonly= readonly class='auto-height'>".$visa_application[0]['business_address']."</textarea>";
                    }else{echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";} ?>
                      </span></td>
                  </tr>
                  <?php } ?>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_customer_service_number">Business Investor</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField_radio"><input type="radio" class="align-right" checked="checked" disabled=""> Yes <input type="radio" class="align-right" disabled=""> No</span></td>
                  </tr>
                  
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_document_1">International Passport (First page, having photograph)</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                            <?php
                            $documentName = $visa_application[0]['document_1'];
                            if(!empty($documentName)){
                                $documentNameArray = explode('_',$documentName, 2);
                                $documentName = $documentNameArray[1];
                            }
                            ?>
                      <?php if(!empty ($documentName)){
                      echo "<textarea class='' readonly= readonly class='auto-height'>".$documentName."</textarea>";
                    }else{echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";} ?>
                      </span></td>
                  </tr>
                  <tr>
                      <td valign="top" height="2"></td>
                <td valign="top" height="2"></td>
                  </tr>

                  
              </table></td>
                  <td valign="top"><table width="347" border="0" cellspacing="0" cellpadding="0" class="no_border_new">
                   

                  <?php if($visa_application[0]['sponsore_type'] == 'CS') { ?>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_customer_service_number">Are you?</label>
                    </td>
                    <td valign="top" class="lblfield_new"><div class="lblField_radio">
                        <span class="lblField">
                            <input type="radio" class="align-right" checked="checked" disabled=""> Company Sponsored Businessman<br>
                            <input type="radio" class="align-right" disabled=""> Self Sponsored Businessman
                        </span>
                     </div></td>
                  </tr>
                  <?php } ?>
                  <?php if($visa_application[0]['sponsore_type'] == 'SS') { ?>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_customer_service_number">Are you?</label>
                    </td>
                    <td valign="top" class="lblfield_new"><div class="lblField_radio">
                        <span class="lblField">
                            <input type="radio" class="align-right" disabled=""> Company Sponsored Businessman<br>
                            <input type="radio" class="align-right" checked="checked" disabled=""> Self Sponsored Businessman
                        </span>
                        </div></td>
                  </tr>
                  <?php } ?>
                  <tr>
                    <td width="175" valign="top" class="label_name_new">
                        <label for="visa_application_document_1">
                            <?php if($visa_application[0]['sponsore_type'] == 'CS') { ?>
                            Letter of Invitation
                            <?php } ?>
                            <?php if($visa_application[0]['sponsore_type'] == 'SS') { ?>
                            Proof of Funds
                            <?php } ?>
                        </label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                            <?php
                            $documentName = $visa_application[0]['document_2'];
                            if(!empty($documentName)){
                                $documentNameArray = explode('_',$documentName, 2);
                                $documentName = $documentNameArray[1];
                            }
                            ?>
                      <?php if(!empty ($documentName)){
                      echo "<textarea class='' readonly= readonly class='auto-height'>".$documentName."</textarea>";
                    }else{echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";} ?>
                      </span></td>
                  </tr>

                  
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_deported_status">Boarding place</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php if(!empty ($visa_application[0]['boarding_place'])){
                      echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['boarding_place']."'>";
                    }else{echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";} ?>
                      </span></td>
                  </tr>

                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_visa_fraud_status">Flight carrier</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php if(!empty ($visa_application[0]['flight_carrier'])){
                      echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['flight_carrier']."'>";
                    }else{echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";} ?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_visa_fraud_status">Flight number</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php if(!empty ($visa_application[0]['flight_number'])){
                      echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['flight_number']."'>";
                    }else{echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";} ?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_customer_service_number">Date of Arrival (dd-mm-yyyy)</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                        <?php if(!empty ($visa_application[0]['arrival_date']))
                  {

                    $explode = explode('-',getDateFormate($visa_application[0]['arrival_date']));
                    echo "<input type='text' class='printdate' readonly=readonly value='".$explode[0]."'>";
                    echo "<input type='text' class='printdate' readonly=readonly value='".$explode[1]."'>";
                    echo "<input type='text' class='printyear' readonly=readonly value='".$explode[2]."'>";

                }else{
                    echo "<input type='text' class='printdate' readonly=readonly value='--'>";
                    echo "<input type='text' class='printdate' readonly=readonly value='--'>";
                    echo "<input type='text' class='printyear' readonly=readonly value='--'>";
                } ?>


                        </span></td>
                  </tr>
                  
                  </table></td>

              </tr>

            </table>

            <?php }else { ?>
            <table cellpadding="0" cellspacing="0" border="0" class="no_border_new" width="695">
              <tr>
                <td width="347" height="25" valign="middle" id="form_new_heading">Flight Information</td>
                <td  rowspan="32" align="center" valign="top" id="border_new_left"></td>
                <td width="347" height="25" valign="middle" id="form_new_heading">&nbsp;</td>
              </tr>
              <tr>
                <td valign="top" height="5"></td>
                <td valign="top" height="5"></td>
              </tr>
              <tr>
                <td valign="top"><table width="347" border="0" cellspacing="0" cellpadding="0" class="no_border_new">

                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_deported_status">Boarding place</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php if(!empty ($visa_application[0]['boarding_place'])){
                      echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['boarding_place']."'>";
                    }else{echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";} ?>
                      </span></td>
                  </tr>

                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_visa_fraud_status">Flight carrier</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php if(!empty ($visa_application[0]['flight_carrier'])){
                      echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['flight_carrier']."'>";
                    }else{echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";} ?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_visa_fraud_status">Flight number</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php if(!empty ($visa_application[0]['flight_number'])){
                      echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['flight_number']."'>";
                    }else{echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";} ?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_customer_service_number">Date of Arrival (dd-mm-yyyy)</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                        <?php if(!empty ($visa_application[0]['arrival_date']))
                  {

                    $explode = explode('-',getDateFormate($visa_application[0]['arrival_date']));
                    echo "<input type='text' class='printdate' readonly=readonly value='".$explode[0]."'>";
                    echo "<input type='text' class='printdate' readonly=readonly value='".$explode[1]."'>";
                    echo "<input type='text' class='printyear' readonly=readonly value='".$explode[2]."'>";

                }else{
                    echo "<input type='text' class='printdate' readonly=readonly value='--'>";
                    echo "<input type='text' class='printdate' readonly=readonly value='--'>";
                    echo "<input type='text' class='printyear' readonly=readonly value='--'>";
                } ?>


                        </span></td>
                  </tr>

              </table></td>
              </tr>

            </table>
            <?php } ?>


            </td>
        </tr>
        <tr>
          <td valign="top" id="uiGroup_Step_5_of_4"><table cellpadding="0" cellspacing="0" border="0" class="no_border_new" width="695">
              <tr>
                <td width="347" height="25" valign="middle" id="form_new_heading">Processing Country Information</td>
                <td  rowspan="32" align="center" valign="top" id="border_new_left"></td>
                <td width="347" height="25" valign="middle" id="form_new_heading">&nbsp;</td>
              </tr>
              <tr>
                <td valign="top" height="5"></td>
                <td valign="top" height="5"></td>
              </tr>
              <tr>
                <td valign="top"><table width="347" border="0" cellspacing="0" cellpadding="0" class="no_border_new">

                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_processing_country_id">Processing country</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php echo "<input type='text' class='print_input_box' value='Nigeria' readonly=readonly>"; ?>
                      </span></td>
                  </tr>
                  <tr>
                    <td valign="top" height="2"></td>
                    <td valign="top" height="2"></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_processing_center_id">Processing center</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php if(!empty ($visa_application[0]['processing_center_id'])){
                      $visa_application[0]['processing_center_id'] == '44';
                      $processingCenter = Doctrine::getTable('GlobalMaster')->getDetail($visa_application[0]['processing_center_id']);
                     echo "<textarea class='' readonly= readonly class='auto-height'>".$processingCenter[0]['var_value']."</textarea>";
                    }else{echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";} ?>
                      </span></td>
                  </tr>
                  </table></td>
              </tr>
              <tr>
                <td valign="top" height="5"></td>
                <td valign="top" height="5"></td>
              </tr>

            </table></td>
        </tr>
                </table>
            <div class="clear"></div>
      <div class="body_text border_top"> <b><br/>
        I understand that I will be required to comply with the immigration / Alien and other laws governing entry of the immigrants into the country to which I now apply for Visa / Entry Permit.</b> </div>
          </div>
        <div class="clear"></div>
    <div class="clear noPrint">&nbsp;</div>
    <div class="border_top noPrint"></div>
        <div class="pixbr_new">
          <div class="Y20_new" >
            <div class="l_new">Date: _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _</div>
            <div class="r_new">Signature / Thumb Impression: _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _</div>
            <div class="pixbr_new"></div>
          </div>      
      <p class="top-padding printtxt" >PLEASE NOTE: SIGN AND TAKE THE APPLICATION WITH TWO PASSPORT PICTURES ON YOUR FLIGHT AND PRESENT TO THE IMMIGRATION OFFICER AT YOUR PORT OF ENTRY.</p>
        </div>
        <div class="clear"></div>
         <div class="pixbr_new">
         <div class="border_top no_display">&nbsp;</div>
        <fieldset class="fieldset_border">
         <legend>FOR OFFICE USE ONLY</legend>
<table cellpadding="0" cellspacing="0" border="0" width="100%"><tr><td width="30%" class="grey_bg"><strong>CLEARED</strong></td><td><input type="checkbox" /></td></tr><tr><td class="grey_bg"><strong>NOT CLEARED</strong></td><td><input type="checkbox" /></td></tr>

<tr><td colspan="2" height="30"></td></tr>
<tr><td align="left">_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _<br/><br/>Passenger's Signature:</td><td align="right">_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _<br/><br/>Officer's Signature:</td></tr>
</table>





      
    
       
     
         </fieldset>
      
        </div>
    <div align="center">
          <?php if(!$mode){?>
      <div align="center" class="noPrint">
            <center id="multiFormNav">
              <table align="center" class="nobrd">
                <tr>
              <td><div  id="btn_pnc">
                      <?php if($visa_application[0]['status'] == 'New'){ ?>
                  <input type="button" class="normalbutton noPrint"  value="Print & Continue" onclick="showHidePrintButtons();">
                      <?php } else { ?>
                  <input type="button" class="normalbutton noPrint"  value="Print" onclick="window.print();">
                      <?php } ?>
                    </div>
                    <!-- changes for edit issue-->
                <input type="button"  disabled class="no_display normalbutton"  id="multiFormSubmit" value="Continue" onclick= "continueProcess();"/>
                  </td>
                </tr>
              </table>
            </center>
          </div>
          <?php } else{?>
      <div align="center" class="noPrint">
            <center id="multiFormNav">
              <table align="center" class="nobrd">
                <tr>
              <td><input type="button" class="normalbutton noPrint"  value="Print" onclick="window.print();">
                  </td>
              <td><input type="button" class="normalbutton noPrint"  value="Close" onclick="window.close();">
                  </td>
                </tr>
              </table>
            </center>
          </div>
          <?php } ?>
          <div class="print_note" align="center"><em>Please use 0.5 inch/ 12.7mm margin (on all sides of page) for best printing</em></div>
        </div>
        <div class="warningArea">
          <h2>Warning</h2>
          <ul>
            <li>PLEASE ENSURE THAT ALL INFORMATION PROVIDED ON YOUR APPLICATION IS CORRECT BEFORE YOU PROCEED TO PAYMENT. YOUR APPLICATION CANNOT BE
              EDITED ONCE PAYMENT IS INITIATED. </li>
          </ul>
        </div>
      </div>
<script>
  function showHidePrintButtons(){
    $('#multiFormSubmit').show();
    $('#btn_pnc').hide();
    window.print();
    $('#multiFormSubmit').removeAttr('disabled');
  }

  function continueProcess(){

    window.location ="<?php echo url_for('visaArrival/show?chk='.$chk.'&p='.$p.'&id='.$idParam)?>";
  }
  document.title = 'The Nigeria Immigration Service';
</script>
</div>
<div class="content_wrapper_bottom"></div>
