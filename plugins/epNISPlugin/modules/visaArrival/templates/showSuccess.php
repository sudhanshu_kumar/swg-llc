<?php $nonKenya = ($visa_application[0]['applying_country_id'] != 'KE')?true:false; ?>
<div class="noscreen"> <img src="<?php echo image_path('nis.gif'); ?>" alt="Test" class="pd4" /> </div>
<?php use_javascript('jquery.printElement.js'); ?>
<link rel="stylesheet" type="text/css" media="print" href="<?php  echo stylesheet_path('printPopUpMessage');?>" />
<script>
$(document).ready(function(){
    $('#popPrint').click(function(){
        printSelection();
    });
});
/* Start Ashwani This function is using jquery print  */
    function printSelection(){
     printElem({});     
   }
   function printElem(options){
     $('#mainDiv').printElement(options);
   }
</script>
<!-- Display popup message or not -->
<?php //if($sf_request->getParameter('p') !='i') {?>
    <?php
    if($msgFlag){
        //echo ePortal_popup("<div class='highlight red' id='flash_notice'><b>Important!!! You have to generate tracking number for this application to complete the payment process, by clicking on 'Proceed to Online Payment' from 'CART' page.</b></div>Please 1Keep This Safe","<p><b>You will need it later</b></p><h5>Application Id: ".$visa_application[0]['id']."</h5><h5> Reference No: ".$visa_application[0]['ref_no']."</h5>");
        echo ePortal_popup("<div class='highlight red' id='flash_notice'><b>Important!!! You have to generate tracking number for this application to complete the payment process, by clicking on 'Proceed to Online Payment' from 'CART' page.</b></div>"."<div class='confirmation-msg'><font style='color: #FF0000'><center><b>BELOW IS YOUR APPLICATION ID AND REFERENCE NUMBER</b></center>","<p><center><b>PRINT THIS SCREEN OR WRITE DOWN THESE NUMBERS AND KEEP THEM SAFE</b></center></p><p><center><b>YOU WILL NEED THEM TO COMPLETE THE PROCESS AND/OR AT THE EMBASSY/HIGH COMMISSION</b></center></p></font><h5><center>Application Id: ".$visa_application[0]['id']."</center></h5><h5><center> Reference No: ".$visa_application[0]['ref_no']."</center></h5></div>",array('width'=>'650'));
    }else{
       // echo ePortal_popup("Please 1Keep This Safe","<p><b>You will need it later</b></p><h5>Application Id: ".$visa_application[0]['id']."</h5><h5> Reference No: ".$visa_application[0]['ref_no']."</h5>");
        //echo ePortal_popup("<div class='confirmation-msg'><font style='color: #FF0000'><center><b>BELOW IS YOUR APPLICATION ID AND REFERENCE NUMBER</b></center>","<p><center><b>PRINT THIS SCREEN OR WRITE DOWN THESE NUMBERS AND KEEP THEM SAFE</b></center></p><p><center><b>YOU WILL NEED THEM TO COMPLETE THE PROCESS AND/OR AT THE EMBASSY/HIGH COMMISSION</b></center></p></font><h5><center>Application Id: ".$visa_application[0]['id']."</center></h5><h5><center> Reference No: ".$visa_application[0]['ref_no']."</center></h5></div>",array('width'=>'650'));
        //echo ePortal_popup("<font color=red> <center>BELOW IS YOUR APPLICATION ID AND REFERENCE NUMBER .<br />PRINT THIS SCREEN OR WRITE DOWN THESE NUMBERS AND KEEP THEM SAFE.<br /> YOU WILL NEED THEM TO COMPLETE THE PROCESS AND/OR AT THE EMBASSY/HIGH COMMISSION</center></font><h5><p><center>Application Id: ".$visa_application[0]['id']."</center></h5><h5> <center>Reference No: ".$passport_application[0]['ref_no']."</center></p></h5>");
    }

    ?>
<?php //if(isset($chk)){ echo "<script>pop();</script>"; } ?>
<?php //} ?>
<?php $isGratis = false;
      if($visa_application[0]['ispaid'] == 1){
        if(($visa_application[0]['paid_naira_amount'] == 0) && ($visa_application[0]['paid_dollar_amount'] == 0)){
           $isGratis = true;
          }else{
            $isGratis = false;
           }
          }?>
<?php
$sf = sfContext::getInstance()->getUser();
if($sf->hasFlash('notice')){ ?>
    <div id="flash_notice" class="alertBox" >
<?php
    echo nl2br($sf->getFlash('notice'));
?>
</div>
<?php }?>
<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">
    <?php include_partial('global/innerHeading',array('heading'=>"Applicant's Details"));?>
    <div class="clear"></div>
    <div class="tmz-spacer"></div>
<?php
$sf = sfContext::getInstance()->getUser();
if($sf->hasFlash('error')){ ?>
    <div id="flash_error" class="alertBox" >
<?php
    echo nl2br($sf->getFlash('error'));
?>
    </div>
    <br/>
<?php }?>
<form action="<?php echo url_for('cart/list') ?>" method="POST" class='dlForm multiForm'>
<div>
<center>
<?php
if($statusType == 1){
if((isset($countFailedAttempt) && $countFailedAttempt > 0) && ($visa_application[0]['ispaid'] != 1)){

  echo ePortal_highlight("<b>You made $countFailedAttempt payment attempts.None of the payment attempt(s) returned a message.<br /> Please attempt(s) Payment ONLY If You Are Sure The Previous attempt(s) (IF ANY) Was Not Successful.</b>",'',array('class'=>'black'));
}else{
  echo ($visa_application[0]['ispaid'] == 1)?"":ePortal_highlight("<b>No Previous Payment Attempt History Found</b>",'',array('class'=>'black'));}
}
?>
      </center>
  <div  id="Reciept" >
<?php if($show_details==1){ ?>
        <!--<fieldset class="bdr">-->
        <table cellpadding="0" cellspacing="0" border="0" width="100%" class="show_table">
          <tbody>
            <tr>
              <td class="blbar" colspan="2"><strong><?php echo ePortal_legend("Profile"); ?></strong></td>
            </tr>
            <tr>
              <td width="50%"><label>Full Name:</label></td>
              <td><?php echo ePortal_displayName($visa_application[0]['title'],$visa_application[0]['first_name'],$visa_application[0]['middle_name'],$visa_application[0]['surname']);?></td>
            </tr>
            <tr>
              <td><label>Gender:</label></td>
              <td><?php echo $visa_application[0]['gender'];?></td>
            </tr>
            <tr>
              <td><label>Date of Birth:</label></td>
              <td><?php $datetime = date_create($visa_application[0]['date_of_birth']); echo date_format($datetime, 'd/F/Y');?></td>
            </tr>
            <tr>
              <td><label>Email:</label></td>
              <td><?php if($visa_application[0]['email']!='') echo $visa_application[0]['email']; else echo "--"; ?></td>
            </tr>
            <tr>
              <td><label>Country of Origin:</label></td>
              <td><?php echo $visa_application[0]['CurrentCountry']['country_name']; ?></td>
            </tr>                        
<?php } ?>
<?php if($show_details==1){ ?>
            <tr>
              <td class="blbar" colspan="2"><strong><?php echo ePortal_legend("Application Information "); ?></strong></td>
            </tr>
            <tr>
              <td><label>Category:</label></td>
              <td>Visa on Arrival</td>
            </tr>

            <?php if($nonKenya) { ?>

            <tr>              
              <?php if($visa_application[0]['applicant_type'] =='GB') {
                     $applicantType = ' Government Business';
                     $addressLabel = 'Corresponding Government Agency in Nigeria (including address):';
              }else{
                     $applicantType = ' Private Business';
                     $addressLabel = 'Name of Company (including address):';

              }?>
             <td><label>Applicant Type:</label></td>
             <td><?php echo $applicantType; ?></td>
            </tr>
            <tr>
              <td width="50%"><label><?php echo $addressLabel ; ?></label></td>
              <td><?php echo nl2br($visa_application[0]['business_address']);?></td>
            </tr>
            <tr>
            <tr>
              <td><label>Business Investor :</label></td>
              <td>Yes</td>
            </tr>
            <tr>
            <?php if($visa_application[0]['sponsore_type'] == 'CS') {
                   $sponsorType = 'Company Sponsored Businessman';
            }else{
                   $sponsorType = 'Self Sponsored Businessman';
               } ?>

              <td><label>You are:</label></td>
              <td><?php echo $sponsorType ;?></td>
            </tr>
            <?php } ?>
            <?php $visaType  = Doctrine::getTable('VisaType')->findById($visa_application[0]['type_of_visa']); ?>
            <tr>
              <td><label>Visa Type:</label></td>
              <td><?php echo ucwords($visaType[0]['var_value']);?></td>
            </tr>
            
            <tr>
              <td><label>Request:</label></td>
              <td>Visa on Arrival Single</td>
            </tr>
            <tr>
              <td><label>Date:</label></td>
              <td><?php $datetime = date_create($visa_application[0]['created_at']); echo date_format($datetime, 'd/F/Y'); ?></td>
            </tr>
<?php } ?>
            <tr>
              <td><label>Date of Arrival:</label></td>
              <td><?php $datetime = date_create($visa_application[0]['arrival_date']); echo date_format($datetime, 'd/F/Y');?></td>
            </tr>
            <tr>
              <td><label>Boarding Place:</label></td>
              <td><?php echo $visa_application[0]['boarding_place']; ?></td>
            </tr>
            <tr>
              <td><label>Flight Number:</label></td>
              <td><?php echo $visa_application[0]['flight_number'];?></td>
            </tr>
            <tr>
              <td><label>Application Id:</label></td>
              <td><?php echo $visa_application[0]['id'];?></td>
            </tr>
            <tr>
              <td><label>Reference No:</label></td>
              <td><?php echo $visa_application[0]['ref_no']; ?></td>
            </tr>
            <tr>
              <td colspan="2" class="blbar"><strong><?php echo ePortal_legend("Processing Information"); ?></strong> </td>
            </tr>
<?php if($show_details==1){ ?>
            <tr>
              <td><label>Country:</label></td>
              <td><?php echo $applying_country ;?></td>
            </tr>
            <tr>
              <td><label>State:</label></td>
              <td>Not Applicable</td>
            </tr>            
            <tr>
              <td><label>Office:</label></td>
              <td>Not Applicable</td>
            </tr>
<?php } ?>

<?php
$isPaid = $visa_application[0]['ispaid'];
if($show_payment==1 || $isPaid==0){ ?>            
            <tr>
              <td class="blbar" colspan="2"><strong><?php echo ePortal_legend("Payment Information"); ?></strong></td>
            </tr>
      <?php
    if($visa_application[0]['ispaid'] == 1)
    {
      if($visa_application[0]['paid_dollar_amount'] != 0.00)


      {
        echo "<tr><td><label>Dollar Amount:</label></tr><td>USD&nbsp;".$visa_application[0]['paid_dollar_amount']."</td></tr>";
      }
//      else
//      {
//        echo "Not Applicable";
//      }
    }else
    {
      if(isset($payment_details['dollar_amount'])){ echo "<tr><td><label>Dollar Amount:</label></td><td>USD&nbsp;".$payment_details['dollar_amount']."</td></tr>";}else{ echo "<tr><td><label>Dollar Amount:</label></td><td>USD&nbsp;"."0</td></tr>";}
    }
    ?>
            <tr>
              <td><label>Payment Status:</label></td>
              <td><?php
    $isGratis = false;
    if($visa_application[0]['ispaid'] == 1)
    {
      if(($visa_application[0]['paid_naira_amount'] == 0) && ($visa_application[0]['paid_dollar_amount'] == 0))
      {
        $isGratis = true;
        echo "This Application is Gratis (Requires No Payment)";
      }else
      {
      echo "Payment Done";
      }
    }else{
      echo "Available after Payment";
    }

    ?></td>
            </tr>
            <tr>
              <td><label>Payment Currency:</label></td>
              <td><?php
    if($visa_application[0]['ispaid'] == 1)
    {
      if(($visa_application[0]['paid_naira_amount'] == 0) && ($visa_application[0]['paid_dollar_amount'] == 0))
      {
        echo "Not Applicable";
      }else
      {
      echo $paymentCurrency;
      }
    }else{
      echo "Available after Payment";
    }
    ?></td>
            </tr>
            <tr>
              <td><label>Payment Gateway:</label></td>
              <td><?php
    if($visa_application[0]['ispaid'] == 1)
    {
      if(($visa_application[0]['paid_naira_amount'] == 0.00) && ($visa_application[0]['paid_dollar_amount'] == 0.00))
      {
        echo "Not Applicable";
      }else
      {
     echo $PaymentGatewayType;
      }
    }else{
      echo "Available after Payment";
    }
    ?></td>
            </tr>
            <tr>
              <td><label>Amount Paid:</label></td>
              <td><?php
    if($visa_application[0]['ispaid'] == 1)
    {
      if(($visa_application[0]['paid_naira_amount'] == 0.00) && ($visa_application[0]['paid_dollar_amount'] == 0.00))
      {
        echo "Not Applicable";
      }else
      {
      echo $currencySign."&nbsp;".$paidAmount;
      }
    }else
    {
      echo "Available after Payment";
    }
    ?>
              </td>
<?php  } ?>
<?php if($visa_application[0]['status'] != 'New' && $visa_application[0]['status'] != 'Paid'){?>
            </tr>
            <tr>
              <td class="blbar" colspan="2"><?php echo ePortal_legend("Application Status"); ?></td>
            </tr>
            <tr>
              <td><label>Current Application Status:</label></td>
              <td><?php echo $visa_application[0]['status'] ;?></td>
<?php }?>
<?php

if($isPaid == 1)
{
?>
<?php if($show_payment==1){ 
    echo ePortal_highlight('YOUR PAYMENT PROCESS WAS SUCCESSFULL! PLEASE PRINT YOUR ACKNOWLEDGMENT & RECEIPT SLIP.','',array('class'=>'green'));
} ?>
            </tr>
          </tbody>
        </table>
</div>
<div class="clear"></div>
<?php
}else{
?>
        <div class="pixbr XY20">
  <center id="multiFormNav" >
            <table  align="center" class="nobrd noPrint" width="99%">
              <tr>
                <td align="right">&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td width="40%" align="right">
                  <input name="button" type="button" class="normalbutton" id="multiPrintw" onclick="javascript:window.print()" value='Print' />
                </td>
                <td width="50%" align="left">
                  <input type="submit" class="normalbutton" id="multiFormSubmit" value='Proceed To Checkout'>
                </td>
              </tr>
            </table>
  </center>
</div>
  </div>
<?php // if(!$IsProceed) echo ePortal_highlight("You have attempted a payment in last ".sfConfig::get("app_time_interval_payment_attempt")." minutes!! Please wait for ".sfConfig::get("app_time_interval_payment_attempt")." minutes.",'',array('class'=>'red')); ?>
<?php if($show_details==1){ ?>
  <?php // echo ePortal_highlight('PLEASE CONFIRM YOUR ORDER BEFORE PROCEEDING TO PAYMENTS. NIS WILL NOT REFUND APPLICANT FOR A WRONG ORDER / PAYMENT','WARNING',array('class'=>'yellow'));?>
  <?php echo ePortal_highlight('PLEASE ENSURE THAT ALL INFORMATION PROVIDED ON YOUR APPLICATION IS CORRECT BEFORE YOU PROCEED TO PAYMENT.  YOUR APPLICATION CANNOT BE EDITED ONCE PAYMENT IS INITIATED.<br/>','WARNING',array('class'=>'yellow'));?>
<?php }
}
?>
<input type ="hidden" value="<?php echo $getAppIdToPaymentProcess;?>" name="appDetails" id="appDetails"/>
</form>
</div>
</div>
<div class="content_wrapper_bottom"></div>
