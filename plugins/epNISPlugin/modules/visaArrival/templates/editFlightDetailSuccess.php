<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
    <div class="content_container">

        <?php
        $sf = sfContext::getInstance()->getUser();
        if($sf->hasFlash('error')){ ?>
        <div id="flash_error" class="alertBox" >
            <?php
            echo nl2br($sf->getFlash('error'));
            ?>
        </div>
        <?php }?>


    <?php include_partial('global/innerHeading', array('heading' => 'Update VOAP Arrival Details')); ?>
      <div class="bodyContent2">
                <div class="formH3">
                  <h2>Flight Information</h2>
                </div>

              </div> 
        <div class="clear"></div>
        <div class="tmz-spacer"></div>
        <form action="<?php echo url_for('visaArrival/updateFlightDetail') ?>"  method="post" onsubmit="return validateForm()">
            <div class="formContent_new">
                <div class="lblName_new" style="width:198px;"><?php echo $form['boarding_place']->renderLabel(); ?><span class="redLbl">*</span></div>
                <div class="lblField">
                    <?php echo $form['boarding_place']->render(); ?>
                    <div class="error_msg" id="boarding_place_error"> <?php echo $form['boarding_place']->renderError(); ?></div>
                </div>
                <div class="lblName_new" style="width:198px;"><?php echo $form['flight_carrier']->renderLabel(); ?></div>
                <div class="lblField">
                    <?php echo $form['flight_carrier']->render(); ?>
                    <div class="error_msg" id="flight_carrier_error"> <?php echo $form['flight_carrier']->renderError(); ?></div>
                </div>
                <div class="lblName_new" style="width:198px;"><?php echo $form['flight_number']->renderLabel(); ?><span class="redLbl">*</span></div>
                <div class="lblField">
                    <?php echo $form['flight_number']->render(); ?>
                    <div class="error_msg" id="flight_number_error"> <?php echo $form['flight_number']->renderError(); ?></div>
                </div>
                <div class="lblName_new" style="width:198px;"><?php echo $form['arrival_date']->renderLabel(); ?><span class="redLbl">*</span></div>
                <div class="lblField">
                    <?php echo $form['arrival_date']->render(); ?>
                    <div class="error_msg" id="vap_application_arrival_date_error"> <?php echo $form['arrival_date']->renderError(); ?></div>
                </div>

                
                <div class="lblName_new" style="width:198px;"><?php echo $dateLabel; ?></div>
                <div class="lblField">
                    <?php echo date_format(date_create($createdAt),'d/F/Y'); ; ?>
                </div>
            </div>
            <div class="clear"></div>
            <div style="height:25px;"></div>
            <div class="lblName_new">
                    <strong><span class="notice-text">Note:</span></strong> Date of Arrival should not be greater than <strong><?php echo date_format(date_create($expiry_date), 'd/F/Y'); ?>.
            </div>
            <div class="clear"></div>
                <div class="body_text border_top"></div>
                <div class="clear"></div>

                    <div class="pixbr_new">
                        <p class="text_hide"><font color="red">*</font> - Compulsory fields</p>
                      </div>
                <div>
                    <center id="multiFormNav">
                        <table align="center" class="nobrd">
                            <tr>
                                <td>
                                    <input type="submit" value="Update" class="normalbutton">
                                </td>
                            </tr>
                        </table>
                    </center>
                </div>
        </form>
    </div>
</div>
<div class="content_wrapper_bottom"></div>

<script>
    document.ready(function(){

        $('#vap_application_flight_carrier').addClass('txt-input');
    });

    function validateForm(){
        var boardingPlace = jQuery.trim($('#vap_application_boarding_place').val());
        var flightCarrier = jQuery.trim($('#vap_application_flight_carrier').val());
        var flightNumber = jQuery.trim($('#vap_application_flight_number').val());


        var err = 0;

        if(boardingPlace == ''){
            $('#boarding_place_error').html('Please enter Boarding Place.');
            err = err + 1;
        }else{
            $('#boarding_place_error').html('');
        }

        if(flightNumber == ''){
            $('#flight_number_error').html('Please enter Flight Number.');
            err = err + 1;
        }else{
            $('#flight_number_error').html('');
        }


        if($('#vap_application_arrival_date_day').val() == "" && $('#vap_application_arrival_date_month').val() == "" && $('#vap_application_arrival_date_year').val() == ""){
            $('#vap_application_arrival_date_error').html("Please enter valid Date of Arrival.");
            err = err + 1;
        }else{
            if($('#vap_application_arrival_date_day').val() == ""){
                $('#vap_application_arrival_date_error').html("Please enter valid Date of Arrival (day).");
                err = err + 1;
            }else if($('#vap_application_arrival_date_month').val() == ""){
                $('#vap_application_arrival_date_error').html("Please enter valid Date of Arrival (month).");
                err = err + 1;
            }else if($('#vap_application_arrival_date_year').val() == ""){
                $('#vap_application_arrival_date_error').html("Please enter valid Date of Arrival (year).");
                err = err + 1;
            }else{
                $('#vap_application_arrival_date_error').html("");
            }
        }       

        if(err > 0){
            return false;
        }else{
            if(confirm('Do you realy want to update flight details?')){
                return true;
            }else{
                return false;
            }
        }
    }
</script>