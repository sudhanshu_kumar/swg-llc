<?php if($status == 'new'){ ?>
<div class="NwPopUp">
    <div class="floatRight"><strong>press [Esc] to close</strong></div><!-- Bug Id:33770 -->
    <div class="NwHeading"><?php echo image_tag('/images/info.png',array('alt'=>'SWGlobal LLC', 'align' => 'absmiddle'));?> Application Information</div>
    <?php if($isAppProcess == 'no') { ?>

    <div class="NwMessage">
        An application with these details exists in our system with application id <strong><?php echo $appId; ?></strong>&nbsp;and reference number <strong><?php echo $ref_no; ?></strong>. This application is <strong>NOT PAID</strong> per current status.
        <br /><br />
        You can <strong>edit, print and/or make payment</strong> for the existing application by clicking <strong>"Open existing application"</strong> button below. If you choose to start a new application, this existing application will be cancelled and will not be available in system going forward.
        <br /><br /><br />
        <strong>Do you wish to proceed with New Application?</strong>
    </div>
    <br />
    <div align="center"><input type="button" class="normalbutton" value="No, open existing application" onclick="openExistingApp('<?php echo $appId; ?>');" />&nbsp;&nbsp;<input type="button" class="normalbutton" value="Yes, proceed to new application" onclick="openNewApp('notpaidNew');" /> </div>
    <?php } else { ?>    
    <div class="NwMessage">
        An application with these details exists in our system with application id <strong><?php echo $appId; ?></strong>&nbsp;and reference number <strong><?php echo $ref_no; ?></strong>. This application is <strong>NOT PAID</strong> per current status.
        <br /><br />
        <span class="red"><strong>Your application is under payment awaited state!! you can not edit this application.</strong></span>
        <br /><br />
        You can <strong>view, print and/or make payment</strong> for the existing application by clicking <strong>"Open existing application"</strong> button below. If you choose to start a new application, this existing application will be cancelled and will not be available in system going forward.
        <br /><br /><br />
    </div>
    <br />
    <div align="center"><input type="button" class="normalbutton" value="No, open existing application"  onclick="printExistingApp('<?php echo $printUrl; ?>');"/>&nbsp;&nbsp;<input type="button" class="normalbutton" value="Yes, proceed to new application" onclick="openNewApp('notpaidNew');" /> </div>
    <?php } ?>
</div>
<?php }else if($status == 'rejected'){ ?>
<div class="NwPopUp" style="height:300px;">
    <div class="floatRight"><strong>press [Esc] to close</strong></div><!-- Bug Id:33770 -->
    <div class="NwHeading"><?php echo image_tag('/images/info.png',array('alt'=>'SWGlobal LLC', 'align' => 'absmiddle'));?> Application Information</div>
    <div class="NwMessage">
        An application with these details exists in our system with application id  <strong><?php echo $appId; ?></strong>&nbsp;and reference number <strong><?php echo $ref_no; ?></strong>. This application is <strong>REJECTED</strong> per current status.
        <div class="block red">            
            <?php if(empty($remarks)){ ?>
            Your application has been rejected due to the following reason(s):
            <br />
            <ul>
            <?php if($appDetails['contagious_disease'] == 'Yes'){ ?>
            <li> You have been infected by any contagious disease (e.g. Tuberculosis) or suffered serious mental illness.</li>
            <?php } ?>
            <?php if($appDetails['police_case'] == 'Yes'){ ?>
            <li> You have been arrested or convicted for an offence (even though subject to pardon).</li>
            <?php } ?>
            <?php if($appDetails['narcotic_involvement'] == 'Yes'){ ?>
            <li> You have been involved in narcotic activity.</li>
            <?php } ?>
            </ul>
            <?php } else { ?>
            <strong>Your application has been rejected due to the following reason(s):</strong>
            <div style="overflow:auto;height:50px;padding-top:5px;"><?php echo nl2br($remarks); ?></div>
            <?php } ?>
        </div>        
        You can <strong>view and print</strong> the existing application by clicking <strong>"Open existing application"</strong> button below. If you want to start a new application, click on <strong>"Yes, Proceed to new application"</strong>.
        <br /><br />
        <strong>Do you wish to proceed with New Application?</strong>
    </div>
    <br />
    <div class="btnArea" align="center" ><input type="button" class="normalbutton" value="No, open existing application" onclick="printExistingApp('<?php echo $printUrl; ?>');" />&nbsp;&nbsp;<input type="button" class="normalbutton" value="Yes, proceed to new application" onclick="openNewApp('rejectedNew');" /> </div>
</div>
<?php }else if($status == 'expired'){ ?>
<div class="NwPopUp" style="height:300px;">
    <div class="floatRight"><strong>press [Esc] to close</strong></div><!-- Bug Id:33770 -->
    <div class="NwHeading"><?php echo image_tag('/images/info.png',array('alt'=>'SWGlobal LLC', 'align' => 'absmiddle'));?> Application Information</div>
    <div class="NwMessage">
        An application with these details exists in our system with application id  <strong><?php echo $appId; ?></strong>&nbsp;and reference number <strong><?php echo $ref_no; ?></strong>. This application is <strong>EXPIRED</strong> per current status.
        <div class="block red">
            Your application has been expired. Application validity on this portal is <?php echo (int)sfConfig::get('app_visa_arrival_flight_update_limit')/30; ?> months from the date of payment.
            <br /><br />
            <strong>Note:</strong> Your application payment date is <?php echo date_format(date_create($appDetails['paid_date']), 'd/F/Y');?>.
        </div>
        You can <strong>view and print</strong> the existing application by clicking <strong>"Open existing application"</strong> button below. If you want to start a new application, click on <strong>"Yes, Proceed to new application"</strong>.
        <br /><br />
        <strong>Do you wish to proceed with New Application?</strong>
    </div>
    <br />
    <div class="btnArea" align="center" ><input type="button" class="normalbutton" value="No, open existing application" onclick="printExistingApp('<?php echo $printUrl; ?>');" />&nbsp;&nbsp;<input type="button" class="normalbutton" value="Yes, proceed to new application" onclick="openNewApp('rejectedNew');" /> </div>
</div>
<?php } else { ?>
<div class="NwPopUp"> 
    <div class="floatRight"><strong>press [Esc] to close</strong></div><!-- Bug Id:33770 -->
    <div class="NwHeading"><?php echo image_tag('/images/info.png',array('alt'=>'SWGlobal LLC', 'align' => 'absmiddle'));?> Application Information</div>
    <div class="NwMessage">
        An application with these details exists in our system with application id  <strong><?php echo $appId; ?></strong>&nbsp;and reference number <strong><?php echo $ref_no; ?></strong>. This application is <strong>PAID</strong> per current status.
        <br /><br />
        You can view and print the existing application by clicking <strong>"Open existing application"</strong> button below. If you choose to start a new application, this existing application will be cancelled and will not be available in system going forward.<br />Please note that your existing application fee WILL NOT be refunded in case you choose to start a new application with these details.
        <br /><br /><br />
        <strong>Do you wish to proceed with New Application?</strong>
    </div>
    <br />
    <div class="btnArea" align="center" ><input type="button" class="normalbutton" value="No, open existing application" onclick="printExistingApp('<?php echo $printUrl; ?>');" />&nbsp;&nbsp;<input type="button"  class="normalbutton" value="Yes, proceed to new application" onclick="openNewApp('paidNew');" /> </div>
</div>    
<?php } ?>