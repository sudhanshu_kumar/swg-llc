<?php
/**
 * [WP: 089] => CR: 128
 * Making flag for kenya...
 */
$kenyaVoa = ($countryId == 'KE')?true:false;

$errorFields = array();
?>

<?php include_stylesheets_for_form($form) ?>
<?php include_javascripts_for_form($form) ?>
<script>

    function checkSponsoredBusinessman()
    {
        if($("input[name='vap_application[sponsore_type]']:checked").next('label').text() == 'Company Sponsored Businessman'){
            document.getElementById("document2_Label").innerHTML = 'Letter of Invitation <span class="redLbl">*</span>';
        }else if($("input[name='vap_application[sponsore_type]']:checked").next('label').text() == 'Self Sponsored Businessman'){
            document.getElementById("document2_Label").innerHTML = 'Proof of Funds <span class="redLbl">*</span>';
        }
        $('#vap_document2_error').html('');
        return false;
    }

    $(document).ready(function(){
        $(function()
        {
            getDeportedCountryStatus();
            //checkDisplayCompanyCustomer();
            //goToDirectTab('Step 4');
        });

        var stateId = $("#vap_application_VapLocalComanyAddressForm_state").val();
<?php $lga_id = (isset($lga_id))?$lga_id:''; ?>
        var lga_id = "<?php echo $lga_id; ?>";

        var url = "<?php echo url_for('visaArrival/getLga/'); ?>";
        $("#vap_application_VapLocalComanyAddressForm_lga_id").load(url, {state_id: stateId, lga_id:lga_id});

        $("#vap_application_VapLocalComanyAddressForm_country_id").change(function(){
            var countryId = $(this).val();
            var url = "<?php echo url_for('visaArrival/getState/'); ?>";
            $("#vap_application_VapLocalComanyAddressForm_state").load(url, {country_id: countryId,add_option: '--- Please Select ---'});
            $("#vap_application_VapLocalComanyAddressForm_lga_id").html( '<option value="">--Please Select--</option>');
        });

        $("#vap_application_VapLocalComanyAddressForm_state").change(function(){
            var stateId = $(this).val();
            var url = "<?php echo url_for('visaArrival/getLga/'); ?>";
            $("#vap_application_VapLocalComanyAddressForm_lga_id").load(url, {state_id: stateId});
        });
        // provides an popup for height fields
        // popHeight();


    });

    var categoryId = <?php echo $catId = Doctrine::getTable('VisaCategory')->getFreshEntryId();?>;




    function submitVapApplication()
    { 
        var err  = 0;

        <?php if(!$kenyaVoa){ ?>

        var business_address = jQuery.trim($('#vap_application_business_address').val());
        if(business_address == "")
        {
            if($("input[name='vap_application[applicant_type]']:checked").next('label').text() == 'Government Business'){
                $('#business_address_error').html("Please enter Corresponding Government Agency in Nigeria (include address) ");
            }else if($("input[name='vap_application[applicant_type]']:checked").next('label').text() == 'Private Business'){
                $('#business_address_error').html("Please enter Name of Company (include address) ");
            }
            err = err+1;
        }
        else
        {
            if(business_address.length > 255 ){
                $('#business_address_error').html("Address can not contains more than 255 Characters.");
                err = err+1;
            }else{
                $('#business_address_error').html("");
            }
        }

        <?php } ?>

        

<?php if ($form->getObject()->isNew()) { ?>

        <?php if(!$kenyaVoa){ ?>

        if($('#vap_application_document_1').val() == ''){
            $('#vap_document1_error').html('Please upload International Passport Scanned Copy');
            err = err +1;
        }else{
            $('#vap_document1_error').html('');
        }

        if($('#vap_application_document_2').val() == ''){
            var sponsorType = $("input[name='vap_application[sponsore_type]']:checked").val();
            if(sponsorType == 'CS'){
                $('#vap_document2_error').html('Please upload Letter of Invitation');
                err = err +1;
            }else{
                $('#vap_document2_error').html('Please upload Proof of Funds');
                err = err +1;
            }

        }else{
            $('#vap_document2_error').html('');
        }

        <?php } ?>

    <?php } ?>

            if($('#vap_application_boarding_place').val() == ''){
                $('#vap_boarding_error').html('Please enter Boarding Place');
                err = err +1;
            }else{
                $('#vap_boarding_error').html('');
            }

            if($('#vap_application_flight_number').val() == ''){
                $('#vap_flight_number_error').html('Please enter Flight Number');
                err = err +1;
            }else{
                $('#vap_flight_number_error').html('');
            }

            if($('#vap_application_processing_country_id').val() == ''){
                $('#vap_processing_center_id_error').html('Please select Processing Country');
                err = err +1;
            }else{
                $('#vap_processing_center_id_error').html('');
            }

            if($('#vap_application_processing_center_id').val() == ''){
                $('#vap_processing_center_id_error').html('Please select Processing Center');
                err = err +1;
            }else{
                $('#vap_processing_center_id_error').html('');
            }


            /* ### Start Validating arrival date fields ### */
            if($('#vap_application_arrival_date_day').val() == ""){
                $('#vap_application_arrival_date_error').html("Please enter valid Date of Arrival (day).");
                err = err+1;
            }else if($('#vap_application_arrival_date_month').val() == ""){
                $('#vap_application_arrival_date_error').html("Please enter valid Date of Arrival (month).");
                err = err+1;
            }else if($('#vap_application_arrival_date_year').val() == ""){
                $('#vap_application_arrival_date_error').html("Please enter valid Date of Arrival (year).");
                err = err+1;
            }else{
                $('#vap_application_arrival_date_error').html("");
            }
            /* ### End Validating arrival date fields ### */

            if(err){
                alert('Some fields are need to be filled with valid value on Step 4.');
                return false;
            }else{


                if(document.getElementById('vap_application_terms_id').checked==false)
                {
                    alert('Please select “I Accept” checkbox.');
                    document.getElementById('vap_application_terms_id').focus();
                    return false;
                }


                //resolve edit application issueTake printed
<?php if (!$form->getObject()->isNew()) { ?>
            var updatedName = document.getElementById('vap_application_title').value+' '+document.getElementById('vap_application_first_name').value+' '+document.getElementById('vap_application_middle_name').value+' '+document.getElementById('vap_application_surname').value;
            $('#updateNameVal').html(updatedName);
            //return pop2();
    <?php }?>
                document.vap_form.submit();
                return true;
            }
        }

        //display pop mssage for confirmation
        function newApplicationAction()
        {
            window.location = 'http://' + '<?php echo $_SERVER['HTTP_HOST']?>'+"<?php echo url_for('visa/newvisa'); ?>";
        }

        function updateExistingAction()
        {
            document.vap_form.submit();
        }




</script>



<script>

    function goToDirectTab(nextTab){

        // below code using for anchor...
        var new_position = $('#tabname').offset();
        window.scrollTo(new_position.left,new_position.top);


        var activeTab = $('#activeTab').val();
        if(nextTab == "Step 1"){
            var showtab = 1;
        }else if(nextTab == "Step 2"){
            var showtab = 2;
        }
        else if(nextTab == "Step 3"){
            var showtab = 3;
        }
        else if(nextTab == "Step 4"){
            var showtab = 4;
        }

        if(parseInt(activeTab) != parseInt(showtab))
        {
            //active selected tab
            $('#uiGroup_Step_'+showtab+'_of_4').show();
            $('#step'+showtab).removeClass('step_process');
            $('#step'+showtab).addClass('step_process_active');


            //hide current tab
            $('#uiGroup_Step_'+activeTab+'_of_4').hide();
            $('#step'+activeTab).removeClass('step_process_active');
            $('#step'+activeTab).addClass('step_process');

            //change active tab value
            $('#activeTab').val(showtab);


            if(showtab == 1){
                document.getElementById('multiFormNavBackid').style.display = 'none' ;
                $('#multiFormNavNext').show();
                $('#multiFormSubmitid').hide();
            }else if(showtab == 4){
                $('#multiFormSubmitid').show();
                $('#multiFormNavBackid').show();
                $('#multiFormNavNext').hide();
            }
            else{
                $('#multiFormNavBackid').show();
                $('#multiFormNavNext').show();
                $('#multiFormSubmitid').hide();
            }
        }
    }
    function gotoTab(act){

        // below code using for anchor...
        //var new_position = $('#tabname').offset();
        //window.scrollTo(new_position.left,new_position.top);

        var start_stop_flag = '';
        var activeTab = $('#activeTab').val();


        if(act == 'next'){
            activeTab = parseInt(activeTab) + 1;
            var activeTabLoop = activeTab;

        }else{ //if(act == 'back'){
            activeTab = parseInt(activeTab) - 1;
            var activeTabLoop = parseInt(activeTab) + 1;
        }


        for(i=1;i<=activeTabLoop;i++){
            if(i == activeTab){
                $('#uiGroup_Step_'+i+'_of_4').show();
                $('#step'+i).removeClass('step_process');
                $('#step'+i).addClass('step_process_active');
                $('#activeTab').val(activeTab);
            }else{
                if(i==1 && act == 'next'){
                    start_stop_flag = validateStep1();
                }
                else if(i==2 && act == 'next'){
                    start_stop_flag = validateStep2();
                }
                else if(i==3 && act == 'next'){
                    start_stop_flag = validateStep3();
                }

                else{
                    start_stop_flag = true;
                }
                if(start_stop_flag){
                    $('#uiGroup_Step_'+i+'_of_4').hide();
                    $('#step'+i).removeClass('step_process_active');
                    $('#step'+i).addClass('step_process');
                }else{
                    break;
                }
            }
        }//End of for(i=1;i<=4;i++){...


        if(start_stop_flag){
            if(act == 'next'){
                if(activeTab == 1){
                    document.getElementById('multiFormNavBackid').style.display = 'inline-block' ;
                    $('#multiFormNavNext').show();
                    $('#multiFormSubmitid').hide();
                }else if(activeTab == 4){
                    $('#multiFormSubmitid').show();
                    $('#multiFormNavBackid').show();
                    $('#multiFormNavNext').hide();
                }
                else{
                    $('#multiFormNavBackid').show();
                    $('#multiFormNavNext').show();
                    $('#multiFormSubmitid').hide();
                }
            }else{ //if(act == 'back'){

                if(activeTab == 1){
                    $('#multiFormNavBackid').hide();
                    $('#multiFormSubmitid').hide();
                }else{
                    if(activeTab == 4){
                        document.getElementById('multiFormNavBackid').style.display = 'inline-block' ;
                        document.getElementById('multiFormNavNext').style.display = 'inline-block' ;
                        $('#multiFormSubmitid').show();
                    }else{
                        $('#multiFormNavBackid').show();
                        $('#multiFormNavNext').show();
                        $('#multiFormSubmitid').hide();
                    }
                }
            }
        }

    }//End of function gotoTab(act){...

    var arr = new Array();
    function validateStep1(){ 

        var err  = 0;
        if($('#vap_application_title').val() == "")
        {
            $('#vap_application_title_error').html("Please select Title");
            err = err+1;
        }
        else
        {
            $('#vap_application_title_error').html("");
        }


        if($('#vap_application_surname').val() == "")
        {
            $('#vap_application_surname_error').html("Please enter Last Name");
            err = err+1;
        }else if(validateString($('#vap_application_surname').val()) !=0)
        {
            $('#vap_application_surname_error').html("Please enter valid Last Name");
            err = err+1;
        }
        else
        {
            $('#vap_application_surname_error').html("");
        }

        if($('#vap_application_first_name').val() == "")
        {
            $('#vap_application_first_name_error').html("Please enter First Name");
            err = err+1;
        }else if(validateString($('#vap_application_first_name').val()) !=0)
        {
            $('#vap_application_first_name_error').html("Please enter valid First Name");
            err = err+1;
        }
        else
        {
            $('#vap_application_first_name_error').html("");
        }

        if($('#vap_application_gender').val() == "")
        {
            $('#vap_application_gender_error').html("Please select Gender");
            err = err+1;
        }
        else
        {
            $('#vap_application_gender_error').html("");
        }

        if($('#vap_application_marital_status').val() == "")
        {
            $('#vap_application_marital_status_error').html("Please select Marital Status");
            err = err+1;
        }
        else
        {
            $('#vap_application_marital_status_error').html("");
        }


        if($('#vap_application_email').val() == "")
        {
            $('#vap_application_email_error').html("Please enter Email");
            err = err+1;
        }
        else if(validateEmail($('#vap_application_email').val()) !=0)
        {
            $('#vap_application_email_error').html("Please enter Valid Email");
            err = err+1;
        }
        else
        {
            $('#vap_application_email_error').html("");
        }

        if($('#vap_application_place_of_birth').val() == "")
        {
            $('#vap_application_place_of_birth_error').html("Please enter Place of Birth");
            err = err+1;
        }
        else
        {
            $('#vap_application_place_of_birth_error').html("");
        }

        /* ### Start Validating date of birth fields ### */
        if($('#vap_application_date_of_birth_day').val() == ""){
            $('#vap_application_date_of_birth_error').html("Please enter valid Date of Birth (day).");
            err = err+1;
        }else if($('#vap_application_date_of_birth_month').val() == ""){
            $('#vap_application_date_of_birth_error').html("Please enter valid Date of Birth (month).");
            err = err+1;
        }else if($('#vap_application_date_of_birth_year').val() == ""){
            $('#vap_application_date_of_birth_error').html("Please enter valid Date of Birth (year).");
            err = err+1;
        }else{
            $('#vap_application_date_of_birth_error').html("");
        }
        /* ### End Validating date of birth fields ### */

        if($('#vap_application_present_nationality_id').val() == "")
        {
            $('#vap_application_present_nationality_id_error').html("Please select Present Nationality");
            err = err+1;
        }
        else
        {
            $('#vap_application_present_nationality_id_error').html("");
        }

        if($('#vap_application_hair_color').val() == "")
        {
            $('#vap_application_hair_color_error').html("Please enter Hair Color");
            err = err+1;
        }
        else
        {
            $('#vap_application_hair_color_error').html("");
        }

        if($('#vap_application_eyes_color').val() == "")
        {
            $('#vap_application_eyes_color_error').html("Please enter Eyes Color");
            err = err+1;
        }
        else
        {
            $('#vap_application_eyes_color_error').html("");
        }

        if($('#vap_application_id_marks').val() == "")
        {
            $('#vap_application_id_marks_error').html("Please enter Identification Marks");
            err = err+1;
        }
        else
        {
            $('#vap_application_id_marks_error').html("");
        }

        if($('#vap_application_height').val() == "")
        {
            $('#vap_application_height_error').html("Please enter Height");
            err = err+1;
        }
        else
        {
            $('#vap_application_height_error').html("");
        }
        if($('#vap_application_VapVisaOfficeAddressForm_address_1').val() == "")
        {
            $('#vap_application_VapVisaOfficeAddressForm_address_1_error').html("Please enter Address 1");
            err = err+1;
        }
        else
        {
            $('#vap_application_VapVisaOfficeAddressForm_address_1_error').html("");
        }

        if($('#vap_application_VapVisaOfficeAddressForm_city').val() == "")
        {
            $('#vap_application_VapVisaOfficeAddressForm_city_error').html("Please enter City");
            err = err+1;
        }
        else
        {
            $('#vap_application_VapVisaOfficeAddressForm_city_error').html("");
        }

        if($('#vap_application_VapVisaOfficeAddressForm_country_id').val() == "")
        {
            $('#vap_application_VapVisaOfficeAddressForm_country_id_error').html("Please enter Country");
            err = err+1;
        }
        else
        {
            $('#vap_application_VapVisaOfficeAddressForm_country_id_error').html("");
        }

        if($('#vap_application_VapVisaOfficeAddressForm_state').val() == "")
        {
            $('#vap_application_VapVisaOfficeAddressForm_state_error').html("Please enter State");
            err = err+1;
        }
        else
        {
            $('#vap_application_VapVisaOfficeAddressForm_state_error').html("");
        }

        if(err == 0){
            return true;
        }else{

            alert('Some fields are need to be filled with valid value on Step 1.');
            return false;
        }

    }


    function validateStep2(){ 
        var err = 0;
        if($('#vap_application_issusing_govt').val() == ''){
            $('#vap_application_issusing_govt_error').html("Please select Issuing Country");
            err = err+1;
        }else{
            $('#vap_application_issusing_govt_error').html("");
        }
        if($('#vap_application_passport_number').val() == ''){
            $('#vap_application_passport_number_error').html("Please enter Passport Number");
            err = err+1;
        }else{
            $('#vap_application_passport_number_error').html("");
        }

        if($('#vap_application_date_of_issue_day').val() == "")
        {
            $('#vap_application_date_of_issue_error').html("Please select Date of Issue");
            err = err+1;
        }else if($('#vap_application_date_of_issue_month').val() == "")
        {
            $('#vap_application_date_of_issue_error').html("Please select Date of Issue");
            err = err+1;
        }else if($('#vap_application_date_of_issue_year').val() == "")
        {
            $('#vap_application_date_of_issue_error').html("Please enter Date of Issue");
            err = err+1;
        }
        else
        {
            $('#vap_application_date_of_issue_error').html("");
        }

        //date of expiry
        if($('#vap_application_date_of_exp_day').val() == "")
        {
            $('#vap_application_date_of_exp_error').html("Please enter Date of Expiry");
            err = err+1;
        }else if($('#vap_application_date_of_exp_month').val() == "")
        {
            $('#vap_application_date_of_exp_error').html("Please enter Date of Expiry");
            err = err+1;
        }else if($('#vap_application_date_of_exp_year').val() == "")
        {
            $('#vap_application_date_of_exp_error').html("Please enter Date of Expiry");
            err = err+1;
        }
        else
        {
            $('#vap_application_date_of_exp_error').html("");
        }

        if($('#vap_application_place_of_issue').val() == "")
        {
            $('#vap_application_place_of_issue_error').html("Please enter Place of Issue");
            err = err+1;
        }
        else
        {
            $('#vap_application_place_of_issue_error').html("");
        }

        if($("input[name='vap_application[type_of_visa]']").length == 1){
            $("input[name='vap_application[type_of_visa]']").attr('checked','checked');
        }

        if (undefined === $("input[name='vap_application[type_of_visa]']:checked").val())
        {
          $('#vap_application_type_of_visa_error').html("Please select Visa Type");
          err = err+1;
        }
        else
        {
          $('#vap_application_type_of_visa_error').html("");
        }

        if($('#vap_application_trip_money').val() == "")
        {
            $('#vap_application_trip_money_error').html("Please enter Money in Hand");
            err = err+1;
        }else{
            if(isNaN($('#vap_application_trip_money').val()))
            {
                $('#vap_application_trip_money_error').html("Please enter correct value.");
                err = err+1;
            }
            else
            {
                //$('#vap_application_trip_money_error').html("");
                trip = $('#vap_application_trip_money').val().split('.');
                if(trip[0].length>10)
                {
                    $('#vap_application_trip_money_error').html("Money in hand should not be more than 10 digits.");
                    err = err+1;
                }
                else
                {
                    $('#vap_application_trip_money_error').html("");
                }
            }
        }

        <?php if(!$kenyaVoa){ ?>

        //company validation

        if($('#vap_application_local_company_name').val() == ''){
            $('#vap_application_local_company_name_error').html("Please enter Company Name");
            err = err+1;
        }else{
            $('#vap_application_local_company_name_error').html("");
        }
        
        if($('#vap_application_VapLocalComanyAddressForm_address_1').val() == "")
        {
            $('#vap_application_VapLocalComanyAddressForm_address_1_error').html("Please enter Address 1");
            err = err+1;
        }
        else
        {
            $('#vap_application_VapLocalComanyAddressForm_address_1_error').html("");
        }


        if($('#vap_application_VapLocalComanyAddressForm_city').val() == "")
        {
            $('#vap_application_VapLocalComanyAddressForm_city_error').html("Please enter City");
            err = err+1;
        }
        else
        {
            $('#vap_application_VapLocalComanyAddressForm_city_error').html("");
        }

        if($('#vap_application_VapLocalComanyAddressForm_country_id').val() == "")
        {
            $('#vap_application_VapLocalComanyAddressForm_country_id_error').html("Please select Country.");
            err = err+1;
        }
        else
        {
            $('#vap_application_VapLocalComanyAddressForm_country_id_error').html("");
        }

        if($('#vap_application_VapLocalComanyAddressForm_state').val() == "")
        {
            $('#vap_application_VapLocalComanyAddressForm_state_error').html("Please select State");
            err = err+1;
        }
        else
        {
            $('#vap_application_VapLocalComanyAddressForm_state_error').html("");
        }

        <?php } ?>

        if(err == 0){
            return true;
        }else{
            alert('Some fields are need to be filled with valid value on Step 2.');
            return false;
        }
    }


    function validateStep3(){ 

        var err = 0;

        if($('#vap_application_deported_status').val() == 'Yes'){
            if($('#vap_application_deported_county_id').val() == ''){
                $('#vap_application_deported_county_id_error').html("Please select deported country.");
                err = err+1;
            }else{
                $('#vap_application_deported_county_id_error').html("");
            }
        }else{
            $('#vap_application_deported_county_id_error').html("");
        }

        if(err == 0){
            return true;
        }else{
            alert('Some fields are need to be filled with valid value on Step 3.');
            return false;
        }
    }

    function validateString(str)
    {
        var reg = /^[A-Za-z0-9 \' \` \- \. \,]*$/; //allow special characters ` ' - . and space
        if(reg.test(str) == false) {
            return 1;
        }

        return 0;
    }

    function validateEmail(email) {

        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        if(reg.test(email) == false) {

            return 1;
        }

        return 0;
    }


    function getDeportedCountryStatus() {

        if($("#vap_application_deported_status").val() == 'Yes'){
            $("#hdn_deported_county_id").show();
        }
        else{
            $("#hdn_deported_county_id").hide();
            $("#vap_application_deported_county_id").val();
        }

    }


    function expandDiv(id,divId){
        if(id == 'Step 1'){
            if($('#hdn_'+divId).val() == 1){
                $('#div_1').show();
                $("#"+divId).html('[-]');
                $('#hdn_'+divId).val('2');
            }else{
                $('#div_1').hide();
                $("#"+divId).html('[+]');
                $('#hdn_'+divId).val('1');
            }


        }else if(id == 'Step 2'){
            if($('#hdn_'+divId).val() == 1){
                $('#div_2').show();
                $("#"+divId).html('[-]');
                $('#hdn_'+divId).val('2');
            }else{
                $('#div_2').hide();
                $("#"+divId).html('[+]');
                $('#hdn_'+divId).val('1');
            }

        }else if(id == 'Step 3'){
            if($('#hdn_'+divId).val() == 1){
                $('#div_3').show();
                $("#"+divId).html('[-]');
                $('#hdn_'+divId).val('2');
            }else{
                $('#div_3').hide();
                $("#"+divId).html('[+]');
                $('#hdn_'+divId).val('1');
            }


        }else if(id == 'Step 4'){
            if($('#hdn_'+divId).val() == 1){
                $('#div_4').show();
                $("#"+divId).html('[-]');
                $('#hdn_'+divId).val('2');
            }else{
                $('#div_4').hide();
                $("#"+divId).html('[+]');
                $('#hdn_'+divId).val('1');
            }


        }else{
            $('#div_1').hide();
            $('#div_2').hide();
            $('#div_3').hide();
            $('#div_4').hide();
        }

    }

    //    function checkDisplayCompanyCustomer(){
    //
    //      if($("input[name='vap_application[applicant_type]']:checked").next('label').text() == 'Government Business'){
    //        $('#vap_hdn_company_id').hide();
    //        $('#vap_hdn_cusomer_service_id').show();
    //      }else if($("input[name='vap_application[applicant_type]']:checked").next('label').text() == 'Private Business'){
    //        $('#vap_hdn_cusomer_service_id').hide();
    //        $('#vap_hdn_company_id').show();
    //      }
    //    }
</script>



<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
<div class="content_container">
    <?php include_partial('global/innerHeading',array('heading'=>"Visa on Arrival Program (STEP 2)"));?>
    <div class="clear"></div>
    <div class="tmz-spacer"></div>

    <!--<div class="wrapForm2">-->
<?php
$sf = sfContext::getInstance()->getUser();
if($sf->hasFlash('error')){ ?>
    <div id="flash_error" class="alertBox" >
        <?php
        echo nl2br($sf->getFlash('error'));
        ?>
    </div>
    <?php }?>

<?php
//  $sf = sfContext::getInstance()->getUser();
?>
    <!-- changes for edit issue -->


<?php if (!$form->getObject()->isNew()) {
    //echo ePortal_popup("<p>You are trying to EDIT an EXITING APPLICATION. </p>","<P>Following is the summary of name changes</p><h5>Application Id: ".$form->getObject()->getId()."</h5><h5> Reference No: ".$form->getObject()->getRefNo()."</h5><h5>&nbsp;</h5><h5>Name to update: ".$form->getObject()->getTitleId().' '.$form->getObject()->getFirstName().' '.$form->getObject()->getMidName().' '.$form->getObject()->getLastName()."</h5><h5>Changed Name: <span id='updateNameVal'></span></h5><h5>&nbsp;</h5><h5><a id='updateAction' href='#' onclick='updateExistingAction();'>Update Application</a>&nbsp;&nbsp;<a id='updateAction' href='#' onclick='newApplicationAction();'>Create New Application</a></h5> ");
    $popData = array(
        'appId'=>$form->getObject()->getId(),
        'refId'=>$form->getObject()->getRefNo(),
        'oldName'=>$form->getObject()->getTitle().' '.$form->getObject()->getFirstName().' '.$form->getObject()->getMiddleName().' '.$form->getObject()->getSurname()
    );
    include_partial('global/editPop',$popData);

}?>

    <form name="vap_form" id="vap_form" action="<?php echo url_for('visaArrival/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$encriptedAppId: '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?> id="multiForm" class="dlForm" onsubmit="updateValue()">
        <div id="printHTML">
            <div class="inst_bg">
                <div class="inst_container1">
                    <ul>
                        <li>FOR ANY QUESTIONS AND/OR CONCERNS PLEASE CONTACT US AT <a href="mailto:support@swgloballlc.com" class="mail_link">support@swgloballlc.com</a></li>
                        <li>APPLICATION FEES PAID FOR VISA/FREEZONE ARE NON REFUNDABLE EXCEPT IN CASED OF DOUBLE PAYMENT MISTAKENLY MADE FOR THE SAME APPLICATION OR FRAUDULENT USE OF CREDIT/DEBIT CARD. TO SEEK A REFUND PLEASE CONTACT <a href="mailto:refund@swgloballlc.com" class="mail_link">refund@swgloballlc.com</a></li>
                        <li>PAYMENTS ARE VALID FOR SIX (6) MONTHS ONLY FROM THE PAYMENT DATE.</li>
                        <li>ONLY online payment is acceptable.</li>
                        <li>Anyone who pays otherwise and receives service is subject to prosecution and revocation of Visa.</li>
                        <li>If you have already completed an application, please check your application status rather than completing a duplicate application</li>
                        <li>Expatriate Quota is not a requirement for Free Zone Entry Visa</li>
                    </ul>
                </div>
            </div>
            <div class="clear"></div>


            <div class="tmz-error-container" id="error_msg_div" style="display:none;">
                <!-- error heading starts-->
                <div class="tmz-error-heading">
                    <img src="<?php echo image_path('tmz-error.png'); ?>" alt="" title="Error" align="absmiddle"/> Error Summary
                </div>
                <!-- error heading ends-->
                <!-- error box starts-->
                <div class="tmz-error-box">
                    <!-- div for all steps-->
                    <div class="tmz-error-steps" id="inner_error_div">

                    </div>
                </div>
            </div>  <!-- error container ends-->


            <a name="tabname" id="tabname"></a>
            <div class="steps_container">
                <div class="step_process_active" id="step1">Step 1</div>
                <div class="step_process" id="step2">Step 2</div>
                <div class="step_process" id="step3">Step 3</div>
                <div class="step_process" id="step4">Step 4</div>
            </div>
            <div class="bodyContent2"  style="display:block" id="uiGroup_Step_1_of_4">

                <div class="formH3">
                    <h2>Personal Information</h2>
                </div>
                <div class="formContent_new formBrd">
                    <div class="lblName"><?php echo $form['title']->renderLabel(); ?><span class="redLbl">*</span></div>
                    <div class="lblField">
                        <?php echo $form['title']->render(); ?>
                        <div class="error_msg" id="vap_application_title_error"> <?php echo $errorFields['Step 1'][] = $form['title']->renderError(); ?></div>
                    </div>
                    <div class="lblName"><?php echo $form['surname']->renderLabel(); ?><span class="redLbl">*</span></div>
                    <div class="lblField">
                        <?php echo $form['surname']->render(); ?>
                        <div class="error_msg" id="vap_application_surname_error"> <?php echo $errorFields['Step 1'][] = $form['surname']->renderError(); ?></div>
                        <i><span class="small-text" style="font-family: Arial,Helvetica,sans-serif;font-size: 10.5px; color: #555555;">(Please avoid placing suffixes with your surname)</span></i>
                    </div>
                    <div class="lblName"><?php echo $form['first_name']->renderLabel(); ?><span class="redLbl">*</span></div>
                    <div class="lblField">
                        <?php echo $form['first_name']->render(); ?>
                        <div class="error_msg" id="vap_application_first_name_error"> <?php echo $errorFields['Step 1'][] = $form['first_name']->renderError(); ?></div>
                    </div>
                    <div class="lblName"><?php echo $form['middle_name']->renderLabel(); ?><span class="redLbl"></span></div>
                    <div class="lblField">
                        <?php echo $form['middle_name']->render(); ?>
                        <div class="error_msg"> <?php echo $errorFields['Step 1'][] = $form['middle_name']->renderError(); ?></div>
                    </div>
                    <div class="lblName"><?php echo $form['gender']->renderLabel(); ?><span class="redLbl">*</span></div>
                    <div class="lblField">
                        <?php echo $form['gender']->render(); ?>
                        <div class="error_msg" id="vap_application_gender_error"> <?php echo $errorFields['Step 1'][] = $form['gender']->renderError(); ?></div>
                    </div>
                    <div class="lblName"><?php echo $form['marital_status']->renderLabel(); ?><span class="redLbl">*</span></div>
                    <div class="lblField">
                        <?php echo $form['marital_status']->render(); ?>
                        <div class="error_msg" id="vap_application_marital_status_error"> <?php echo $errorFields['Step 1'][] = $form['marital_status']->renderError(); ?></div>
                    </div>

                    <div class="lblName"><?php echo $form['email']->renderLabel(); ?><span class="redLbl">*</span></div>
                    <div class="lblField">
                        <?php echo $form['email']->render(); ?>
                        <div class="error_msg" id="vap_application_email_error"> <?php echo $errorFields['Step 1'][] = $form['email']->renderError(); ?></div>
                    </div>
                    <div class="lblName"><?php echo $form['date_of_birth']->renderLabel(); ?><span class="redLbl">*</span></div>
                    <div class="lblField">
                        <?php echo $form['date_of_birth']->render(); ?>
                        <div class="error_msg" id="vap_application_date_of_birth_error"> <?php echo $errorFields['Step 1'][] = $form['date_of_birth']->renderError(); ?></div>
                    </div>
                    <div class="lblName"><?php echo $form['place_of_birth']->renderLabel(); ?><span class="redLbl">*</span></div>
                    <div class="lblField">
                        <?php echo $form['place_of_birth']->render(); ?>
                        <div class="error_msg" id="vap_application_place_of_birth_error"> <?php echo $errorFields['Step 1'][] = $form['place_of_birth']->renderError(); ?></div>
                    </div>

                </div>
                <div class="formContent_new pdLeft9">
                    <div class="lblName"><?php echo $form['present_nationality_id']->renderLabel(); ?><span class="redLbl">*</span></div>
                    <div class="lblField" id="width_dropbox">
                        <?php echo $form['present_nationality_id']->render(); ?>
                        <div class="error_msg" id="vap_application_present_nationality_id_error"> <?php echo $errorFields['Step 1'][] = $form['present_nationality_id']->renderError(); ?></div>
                    </div>


                    <div class="lblName"><?php echo $form['hair_color']->renderLabel(); ?><span class="redLbl">*</span></div>
                    <div class="lblField">
                        <?php echo $form['hair_color']->render(); ?>
                        <div class="error_msg" id="vap_application_hair_color_error"> <?php echo $errorFields['Step 1'][] = $form['hair_color']->renderError(); ?></div>
                    </div>
                    <div class="lblName"><?php echo $form['eyes_color']->renderLabel(); ?><span class="redLbl">*</span></div>
                    <div class="lblField">
                        <?php echo $form['eyes_color']->render(); ?>
                        <div class="error_msg" id="vap_application_eyes_color_error"> <?php echo $errorFields['Step 1'][] = $form['eyes_color']->renderError(); ?></div>
                    </div>
                    <div class="lblName"><?php echo $form['id_marks']->renderLabel(); ?><span class="redLbl">*</span></div>
                    <div class="lblField">
                        <?php echo $form['id_marks']->render(); ?>
                        <div class="error_msg" id="vap_application_id_marks_error"> <?php echo $errorFields['Step 1'][] = $form['id_marks']->renderError(); ?></div>
                    </div>
                    <div id="passport_application_height_row">
                        <div class="lblName"><?php echo $form['height']->renderLabel(); ?><span class="redLbl">*</span></div>
                        <div class="lblField">
                            <?php echo $form['height']->render(); ?>
                            <div class="error_msg" id="vap_application_height_error"> <?php echo $errorFields['Step 1'][] = $form['height']->renderError(); ?></div>
                        </div>
                    </div>
                </div>

                <div class="formH3">
                    <h2><?php echo (!$kenyaVoa)?'Office':'Permanent';?> Address</h2>
                    <h2 class="pdLeft9">&nbsp;</h2>
                </div>
                <div class="formContent_new formBrd">
                    <div class="lblName"><?php echo $form['VapVisaOfficeAddressForm']['address_1']->renderLabel(); ?><span class="redLbl">*</span></div>
                    <div class="lblField">
                        <?php echo $form['VapVisaOfficeAddressForm']['address_1']->render(); ?>
                        <div class="error_msg" id="vap_application_VapVisaOfficeAddressForm_address_1_error"> <?php echo $errorFields['Step 1'][] = $form['VapVisaOfficeAddressForm']['address_1']->renderError(); ?></div>
                    </div>
                    <div class="lblName"><?php echo $form['VapVisaOfficeAddressForm']['address_2']->renderLabel(); ?><span class="redLbl"></span></div>
                    <div class="lblField">
                        <?php echo $form['VapVisaOfficeAddressForm']['address_2']->render(); ?>
                        <div class="error_msg"> <?php echo $errorFields['Step 1'][] = $form['VapVisaOfficeAddressForm']['address_2']->renderError(); ?></div>
                    </div>
                    <div class="lblName"><?php echo $form['VapVisaOfficeAddressForm']['city']->renderLabel(); ?><span class="redLbl">*</span></div>
                    <div class="lblField">
                        <?php echo $form['VapVisaOfficeAddressForm']['city']->render(); ?>
                        <div class="error_msg" id="vap_application_VapVisaOfficeAddressForm_city_error"> <?php echo $errorFields['Step 1'][] = $form['VapVisaOfficeAddressForm']['city']->renderError(); ?></div>
                    </div>
                    <div class="lblName"><?php echo $form['VapVisaOfficeAddressForm']['country_id']->renderLabel(); ?><span class="redLbl">*</span></div>
                    <div class="lblField" id="width_dropbox">
                        <?php echo $form['VapVisaOfficeAddressForm']['country_id']->render(); ?>
                        <div class="error_msg" id="vap_application_VapVisaOfficeAddressForm_country_id_error"> <?php echo $errorFields['Step 1'][] = $form['VapVisaOfficeAddressForm']['country_id']->renderError(); ?></div>
                    </div>
                    <div class="lblName"><?php echo $form['VapVisaOfficeAddressForm']['state']->renderLabel(); ?><span class="redLbl">*</span></div>
                    <div class="lblField">
                        <?php echo $form['VapVisaOfficeAddressForm']['state']->render(); ?>
                        <div class="error_msg" id="vap_application_VapVisaOfficeAddressForm_state_error"> <?php echo $errorFields['Step 1'][] = $form['VapVisaOfficeAddressForm']['state']->renderError(); ?></div>
                    </div>

                </div>
                <div class="formContent_new pdLeft9">

                    <div class="lblName"><?php echo $form['VapVisaOfficeAddressForm']['postcode']->renderLabel(); ?><span class="redLbl"></span></div>
                    <div class="lblField">
                        <?php echo $form['VapVisaOfficeAddressForm']['postcode']->render(); ?>
                        <div class="error_msg"> <?php echo $errorFields['Step 1'][] = $form['VapVisaOfficeAddressForm']['postcode']->renderError(); ?></div>
                    </div>
                    <div class="lblName"><?php echo $form['office_phone_no']->renderLabel(); ?><span class="redLbl"></span></div>
                    <div class="lblField">
                        <?php echo $form['office_phone_no']->render(); ?>
                        <div class="error_msg"> <?php echo $errorFields['Step 1'][] = $form['office_phone_no']->renderError(); ?></div>
                    </div>

                </div>



            </div>
            <div class="bodyContent2 no_display" id="uiGroup_Step_2_of_4">
                <div class="formH3">
                    <h2>Passport Information</h2>
                    <h2 class="pdLeft9">Visa Processing Information</h2>
                </div>
                <div class="formContent_new formBrd">
                    <div class="lblName"><?php echo $form['issusing_govt']->renderLabel(); ?><span class="redLbl">*</span></div>
                    <div class="lblField" id="width_dropbox">
                        <?php echo $form['issusing_govt']->render(); ?>
                        <div class="error_msg" id="vap_application_issusing_govt_error"> <?php echo $errorFields['Step 2'][] = $form['issusing_govt']->renderError(); ?></div>
                    </div>
                    <div class="lblName"><?php echo $form['passport_number']->renderLabel(); ?><span class="redLbl">*</span></div>
                    <div class="lblField">
                        <?php echo $form['passport_number']->render(); ?>
                        <div class="error_msg" id="vap_application_passport_number_error"> <?php echo $errorFields['Step 2'][] = $form['passport_number']->renderError(); ?></div>
                    </div>
                    <div class="lblName"><?php echo $form['date_of_issue']->renderLabel(); ?><span class="redLbl">*</span></div>
                    <div class="lblField">
                        <?php echo $form['date_of_issue']->render(); ?>
                        <div class="error_msg" id="vap_application_date_of_issue_error"> <?php echo $errorFields['Step 2'][] = $form['date_of_issue']->renderError(); ?></div>
                    </div>
                    <div class="lblName"><?php echo $form['date_of_exp']->renderLabel(); ?><span class="redLbl">*</span></div>
                    <div class="lblField">
                        <?php echo $form['date_of_exp']->render(); ?>
                        <div class="error_msg" id="vap_application_date_of_exp_error"> <?php echo $errorFields['Step 2'][] = $form['date_of_exp']->renderError(); ?></div>
                    </div>
                    <div class="lblName"><?php echo $form['place_of_issue']->renderLabel(); ?><span class="redLbl">*</span></div>
                    <div class="lblField">
                        <?php echo $form['place_of_issue']->render(); ?>
                        <div class="error_msg" id="vap_application_place_of_issue_error"> <?php echo $errorFields['Step 2'][] = $form['place_of_issue']->renderError(); ?></div>
                    </div>

                </div>
                <div class="formContent_new pdLeft9">
                    <div class="lblName"><?php echo $form['type_of_visa']->renderLabel(); ?><span class="redLbl">*</span></div>
                    <div class="lblField"><?php echo $form['type_of_visa']->render(); ?>
                        <div class="error_msg" id='vap_application_type_of_visa_error'> <?php echo $errorFields['Step 2'][] = $form['type_of_visa']->renderError(); ?></div>
                    </div>
                    <div class="clear"></div>
                    <div class="lblName"><?php echo $form['applying_country_id']->renderLabel(); ?><span class="redLbl">*</span></div>
                    <div class="lblField">
                        <?php echo $form['applying_country_id']->render(); ?>
                        <div class="error_msg"> <?php echo $errorFields['Step 2'][] = $form['applying_country_id']->renderError(); ?></div>
                    </div>
                    <div class="clear"></div>
                    <div class="lblName"><?php echo $form['trip_money']->renderLabel(); ?><span class="redLbl">*</span></div>
                    <div class="lblField">
                        <?php echo $form['trip_money']->render(); ?>
                        <div class="error_msg" id="vap_application_trip_money_error"> <?php echo $errorFields['Step 2'][] = $form['trip_money']->renderError(); ?></div>
                    </div>
                    <div class="clear"></div>

                </div>

                <div class="formH3">
                    <h2>Corresponding Local Company</h2>
                    <?php $mandatorySign = (!$kenyaVoa)?'<span class="redLbl">*</span>':''; ?>
                </div>
                <div class="formContent_new formBrd">
                    <div class="lblName"><?php echo $form['local_company_name']->renderLabel(); ?><?php echo $mandatorySign; ?></div>
                    <div class="lblField">
                        <?php echo $form['local_company_name']->render(); ?>
                        <div class="error_msg" id="vap_application_local_company_name_error"> <?php echo $errorFields['Step 2'][] = $form['local_company_name']->renderError(); ?></div>
                    </div>
                    <div class="lblName"><?php echo $form['VapLocalComanyAddressForm']['address_1']->renderLabel(); ?><?php echo $mandatorySign; ?></div>
                    <div class="lblField">
                        <?php echo $form['VapLocalComanyAddressForm']['address_1']->render(); ?>
                        <div class="error_msg" id="vap_application_VapLocalComanyAddressForm_address_1_error"> <?php echo $errorFields['Step 2'][] = $form['VapLocalComanyAddressForm']['address_1']->renderError(); ?></div>
                    </div>
                    <div class="clear"></div>

                    <div class="lblName"><?php echo $form['VapLocalComanyAddressForm']['address_2']->renderLabel(); ?></div>
                    <div class="lblField">
                        <?php echo $form['VapLocalComanyAddressForm']['address_2']->render(); ?>
                        <div class="error_msg"> <?php echo $errorFields['Step 2'][] = $form['VapLocalComanyAddressForm']['address_2']->renderError(); ?></div>
                    </div>

                    <div class="lblName"><?php echo $form['VapLocalComanyAddressForm']['city']->renderLabel(); ?><?php echo $mandatorySign; ?></div>
                    <div class="lblField">
                        <?php echo $form['VapLocalComanyAddressForm']['city']->render(); ?>
                        <div class="error_msg" id="vap_application_VapLocalComanyAddressForm_city_error"> <?php echo $errorFields['Step 2'][] = $form['VapLocalComanyAddressForm']['city']->renderError(); ?></div>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="formContent_new pdLeft9">
                    <div class="lblName"><?php echo $form['VapLocalComanyAddressForm']['country_id']->renderLabel(); ?><?php echo $mandatorySign; ?></div>
                    <div class="lblField">
                        <?php echo $form['VapLocalComanyAddressForm']['country_id']->render(); ?>
                        <div class="error_msg" id="vap_application_VapLocalComanyAddressForm_country_id_error"> <?php echo $errorFields['Step 2'][] = $form['VapLocalComanyAddressForm']['country_id']->renderError(); ?></div>
                    </div>

                    <div class="clear"></div>

                    <div class="lblName"><?php echo $form['VapLocalComanyAddressForm']['state']->renderLabel(); ?><?php echo $mandatorySign; ?></div>
                    <div class="lblField">
                        <?php echo $form['VapLocalComanyAddressForm']['state']->render(); ?>
                        <div class="error_msg" id="vap_application_VapLocalComanyAddressForm_state_error"> <?php echo $errorFields['Step 2'][] = $form['VapLocalComanyAddressForm']['state']->renderError(); ?></div>
                    </div>


                    <div class="lblName"><?php echo $form['VapLocalComanyAddressForm']['lga_id']->renderLabel(); ?></div>
                    <div class="lblField">
                        <?php echo $form['VapLocalComanyAddressForm']['lga_id']->render(); ?>
                        <div class="error_msg"> <?php echo $errorFields['Step 2'][] = $form['VapLocalComanyAddressForm']['lga_id']->renderError(); ?></div>
                    </div>

                    <div class="lblName"><?php echo $form['VapLocalComanyAddressForm']['district']->renderLabel(); ?></div>
                    <div class="lblField">
                        <?php echo $form['VapLocalComanyAddressForm']['district']->render(); ?>
                        <div class="error_msg"> <?php echo $errorFields['Step 2'][] = $form['VapLocalComanyAddressForm']['district']->renderError(); ?></div>
                    </div>


                    <div class="lblName"><?php echo $form['VapLocalComanyAddressForm']['postcode']->renderLabel(); ?></div>
                    <div class="lblField">
                        <?php echo $form['VapLocalComanyAddressForm']['postcode']->render(); ?>
                        <div class="error_msg"> <?php echo $errorFields['Step 2'][] = $form['VapLocalComanyAddressForm']['postcode']->renderError(); ?></div>
                    </div>

                </div>
            </div>



            <div class="bodyContent2 no_display" id="uiGroup_Step_3_of_4">
                <div class="div_height"></div>
                <div class="formH3">
                    <h2>Travel History</h2>
                </div>
                <div class="formContent_new formBrd">
                    <div class="lblName"><?php echo $form['contagious_disease']->renderLabel(); ?></div>
                    <div class="lblField">
                        <?php echo $form['contagious_disease']->render(); ?>
                        <div class="error_msg"> <?php echo $errorFields['Step 3'][] = $form['contagious_disease']->renderError(); ?></div>
                    </div>
                    <div class="clear"></div>
                    <div class="lblName"><?php echo $form['police_case']->renderLabel(); ?></div>
                    <div class="lblField">
                        <?php echo $form['police_case']->render(); ?>
                        <div class="error_msg"> <?php echo $errorFields['Step 3'][] = $form['police_case']->renderError(); ?></div>
                    </div>
                    <div class="clear"></div>
                    <div class="lblName"><?php echo $form['narcotic_involvement']->renderLabel(); ?></div>
                    <div class="lblField">
                        <?php echo $form['narcotic_involvement']->render(); ?>
                        <div class="error_msg"> <?php echo $errorFields['Step 3'][] = $form['narcotic_involvement']->renderError(); ?></div>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="formContent_new pdLeft9">
                    <div class="lblName"><?php echo $form['deported_status']->renderLabel(); ?></div>
                    <div class="lblField">
                        <?php echo $form['deported_status']->render(); ?>
                        <div class="error_msg"> <?php echo $errorFields['Step 3'][] = $form['deported_status']->renderError(); ?></div>
                    </div>
                    <div class="clear"></div>
                    <div style="display:none" id="hdn_deported_county_id">
                        <div class="lblName" ><?php echo $form['deported_county_id']->renderLabel(); ?><span class="redLbl">*</span></div>
                        <div class="lblField" id="width_dropbox">
                            <?php echo $form['deported_county_id']->render(); ?>
                            <div class="error_msg" id="vap_application_deported_county_id_error"> <?php echo $errorFields['Step 3'][] = $form['deported_county_id']->renderError(); ?></div>
                        </div>
                    </div>
                    <div class="clear"></div>
                    <div class="lblName"><?php echo $form['visa_fraud_status']->renderLabel(); ?></div>
                    <div class="lblField">
                        <?php echo $form['visa_fraud_status']->render(); ?>
                        <div class="error_msg"> <?php echo $errorFields['Step 3'][] = $form['visa_fraud_status']->renderError(); ?></div>
                    </div>
                    <div class="clear"></div>


                </div>


            </div>

            <div class="bodyContent2 no_display" id="uiGroup_Step_4_of_4">
                <div class="div_height"></div>
                <div class="formH3">
                     <?php if(!$kenyaVoa){ ?>
                    <h2>Applicant Information</h2>
                    <?php } else { ?>
                    <h2>Flight Information</h2>
                    <?php } ?>
                </div>
                <?php if(!$kenyaVoa){ ?>
                <div class="formContent_new formBrd">                     
                    <div class="lblName"><?php echo $form['applicant_type']->renderLabel(); ?><span class="redLbl">*</span></div>
                    <div class="lblField_app">
                        <?php echo $form['applicant_type']->render(); ?>
                        <div class="error_msg" id="vap_application_applicant_type_error"> <?php echo $errorFields['Step 4'][] = $form['applicant_type']->renderError(); ?></div>
                    </div>
                    <div id="vap_hdn_business_address">
                        <div class="lblName"  id="vap_hdn_business_address_label"><?php echo $form['business_address']->renderLabel(); ?><span class="redLbl">*</span></div>
                        <div class="lblField">
                            <?php echo $form['business_address']->render(); ?>
                            <div class="error_msg" id="business_address_error"> <?php echo $errorFields['Step 4'][] = $form['business_address']->renderError(); ?></div>
                        </div>
                    </div>

                    <div class="lblName"><?php echo $form['document_1']->renderLabel(); ?><span class="redLbl">*</span></div>
                    <div class="lblField_app">
                        <?php echo $form['document_1']->render(); ?>
                        <br>-Please upload JPG/GIF/PNG/PDF files only.
                        <br>-File size upto 400 KB.
                        <br>
                        <?php
                        if(file_exists(sfConfig::get('sf_upload_dir').'/vap_document/'.$form->getObject()->get('document_1')) && $form->getObject()->get('document_1')!=''){
                            //get image size
                            $fileName =  _compute_public_path($form->getObject()->get('document_1'), 'uploads/vap_document', '', true);
                            $size = getimagesize($fileName);
                            ?>
                        <a href="javascript:void(0);" onclick="javascript:window.open('<?= _compute_public_path($form->getObject()->get('document_1'), 'uploads/vap_document', '', true);;?>','MyPage','width=<?php echo $size[0]+20; ?>,height=<?php echo $size[1]+20; ?>,scrollbars=1,resizable=yes');">View uploaded International Passport scanned copy</a>
                        <?php } ?>
                        <div class="error_msg" id="vap_document1_error"> <?php echo $errorFields['Step 4'][] = $form['document_1']->renderError(); ?></div>
                    </div>

                    <div class="lblName"><?php echo $form['sponsore_type']->renderLabel(); ?><span class="redLbl">*</span></div>
                    <div class="lblField_app">
                        <?php echo $form['sponsore_type']->render(); ?>
                        <div class="error_msg" id="vap_sponsore_type_error"> <?php echo $errorFields['Step 4'][] = $form['sponsore_type']->renderError(); ?></div>
                    </div>

                    <!-- DOCUMENT 2 -->
                    <div class="lblName" id="document2_Label"><?php echo $form['document_2']->renderLabel(); ?><span class="redLbl">*</span></div>
                    <div class="lblField_app">
                        <?php echo $form['document_2']->render(); ?>
                        <br>-Please upload JPG/GIF/PNG/PDF files only.
                        <br>-File size upto 400 KB.
                        <br>
                        <?php
                        if(file_exists(sfConfig::get('sf_upload_dir').'/vap_document/'.$form->getObject()->get('document_2')) && $form->getObject()->get('document_2')!=''){
                            //get image size
                            $fileName =  _compute_public_path($form->getObject()->get('document_2'), 'uploads/vap_document', '', true);
                            $size = getimagesize($fileName);
                            ?>
                            <?php if($sponsore_type == 'CS') { ?>
                        <div id="document2_uploadedMsg"><a href="javascript:void(0);" onclick="javascript:window.open('<?= _compute_public_path($form->getObject()->get('document_2'), 'uploads/vap_document', '', true);;?>','MyPage','width=<?php echo $size[0]; ?>,height=<?php echo $size[1]; ?>,scrollbars=1,resizable=yes');" id="document2_anchor_uploadedMsg">View uploaded Letter of Invitation</a></div>
                        <?php } else { ?>
                        <div id="document2_uploadedMsg"><a href="javascript:void(0);" onclick="javascript:window.open('<?= _compute_public_path($form->getObject()->get('document_2'), 'uploads/vap_document', '', true);;?>','MyPage','width=<?php echo $size[0]; ?>,height=<?php echo $size[1]; ?>,scrollbars=1,resizable=yes');" id="document2_anchor_uploadedMsg">View uploaded Proof of Funds</a></div>
                        <?php } ?>

                    <?php } ?>
                        <div class="error_msg" id="vap_document2_error"> <?php echo $errorFields['Step 4'][] = $form['document_2']->renderError(); ?></div>
                    </div>
                </div>
                <?php } ?>
                <div class="formContent_new <?php echo (!$kenyaVoa)?'pdLeft9':''; ?>">
                    <div class="lblName"><?php echo $form['boarding_place']->renderLabel(); ?><span class="redLbl">*</span></div>
                    <div class="lblField">
                        <?php echo $form['boarding_place']->render(); ?>
                        <div class="error_msg" id="vap_boarding_error"> <?php echo $errorFields['Step 4'][] = $form['boarding_place']->renderError(); ?></div>
                    </div>
                    <div class="clear"></div>
                    <div>
                        <div class="lblName" ><?php echo $form['flight_carrier']->renderLabel(); ?><span class="redLbl"></span></div>
                        <div class="lblField" id="width_dropbox">
                            <?php echo $form['flight_carrier']->render(); ?>
                            <div class="error_msg"> <?php echo $errorFields['Step 4'][] = $form['flight_carrier']->renderError(); ?></div>
                        </div>
                    </div>
                    <div class="clear"></div>
                    <div class="lblName"><?php echo $form['flight_number']->renderLabel(); ?><span class="redLbl">*</span></div>
                    <div class="lblField">
                        <?php echo $form['flight_number']->render(); ?>
                        <div class="error_msg" id="vap_flight_number_error"> <?php echo $errorFields['Step 4'][] = $form['flight_number']->renderError(); ?></div>
                    </div>
                    <div class="clear"></div>
                    <div class="lblName"><?php echo $form['arrival_date']->renderLabel(); ?><span class="redLbl">*</span></div>
                    <div class="lblField">
                        <?php echo $form['arrival_date']->render(); ?>
                        <div class="error_msg" id="vap_application_arrival_date_error"> <?php echo $errorFields['Step 4'][] = $form['arrival_date']->renderError(); ?></div>
                    </div>
                    <div class="clear"></div>
                    <div class='lblName' style="padding-top:10px;width:100%;">
                        <span class='red'>Note:</span>
                        Above details can be updated, in case of any change in applicant schedule from Menu (<strong>Application >> Update VOAP Arrival Details</strong>). To avoid unnecessary
                        delays in the services, be sure your arrival details should be updated on the SWGlobal LLC portal.
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="div_height"></div>
                <div class="formH3">
                    <h2>Processing Country Information </h2>
                </div>
                <div class="formContent_new formBrd">
                    <div class="lblName"><?php echo $form['processing_country_id']->renderLabel(); ?><span class="redLbl">*</span></div>
                    <div class="lblField">
                        <?php echo $form['processing_country_id']->render(); ?>
                        <div class="error_msg" id="vap_application_applicant_type_error"> <?php echo $errorFields['Step 4'][] = $form['processing_country_id']->renderError(); ?></div>
                    </div>
                    <div class="lblName"><?php echo $form['processing_center_id']->renderLabel(); ?><span class="redLbl">*</span></div>
                    <div class="lblField">
                        <?php echo $form['processing_center_id']->render(); ?>
                        <div class="error_msg" id="vap_processing_center_id_error"> <?php echo $errorFields['Step 4'][] = $form['processing_center_id']->renderError(); ?></div>
                    </div>


                </div>
                <div class="clear"></div>
                <div class="body_text border_top">
                    <br/>
                    <input type="checkbox" name="vap_application[terms_id]" id="vap_application_terms_id" value="checkbox" />
                I understand that I will be required to comply with immigration/alien and other laws governing entry of immigrants into the country to which I now apply for Visa/Entry permit.<br/><br/></div>
                <div class="error_msg"> <?php echo $errorFields['Step 4'][] = $form['terms_id']->renderError(); ?></div>

                <div class="clear"></div>
            </div>





            <input type="hidden" name="countryId" value="<?php echo $countryId;?>" />
            <!--input type="hidden" name="vap_application[business_address]" value="<?php //echo $businessAddress;?>" /-->
            <!--input type="hidden" name="vap_application[applicant_type]" value="<?php //echo $applicantType;?>" /-->
            <!--input type="hidden" name="vap_application[arrival_date]" value="<?php //echo $arrivalDate;?>" /-->
            <?php //echo $form['applicant_type']->render(); ?>
            <?php //echo $form['business_address']->render(); ?>

            <?php echo $form['id']->render(); ?>

            <?php echo $form['VapVisaOfficeAddressForm']['id']->render(); ?>
            <?php //echo $form['VapVisaPermanentAddressForm']['id']->render(); ?>
            <?php echo $form['VapLocalComanyAddressForm']['id']->render(); ?>


        </div> <!-- END of <div class="container_main" id="printHTML"> -->
<!--    <div class="container_bottombg"></div>-->

        <div class="clear"></div>
        <div class="pixbr_new X50_new mg_left_zero">
            <p class="text_hide"><font color="red">*</font> - Compulsory fields</p>
            <div class="Y20_new">
            </div>
            <div class="border_top_new"></div>
            <div>
                <p><span class="notice-text" >PLEASE NOTE: SIGN AND TAKE THE APPLICATION WITH TWO PASSPORT PICTURES ON YOUR FLIGHT AND PRESENT TO THE IMMIGRATION OFFICER AT YOUR PORT OF ENTRY.</span></p>
            </div>
        </div>
        <div align="center">

            <div id="btn_visa" align="center">

                <div class="a_button" id="multiFormNavBack"><a href="#" class='normalbutton no_display' style="text-decoration:none; border:none;" id='multiFormNavBackid'  onclick="gotoTab('back');">Back</a></div>
                <div class="a_button" id="multiFormNavNext"><a href ="#" class='normalbutton pdleft15' id=''  onclick="gotoTab('next');">Next</a></div>
                <?php if ($form->getObject()->isNew()) {?>
                <div class="a_button" id="multiFormSubmit" >
                <a href="#" id="multiFormSubmitid"  value="Submit Application" class='normalbutton no_display' onclick="return submitVapApplication();" >Submit Application</a></div>

                    <?php  }
                else if (!$form->getObject()->isNew()) { ?>
                <div class="a_button" id="multiFormSubmit" > <a href="#" id="multiFormSubmitid"  value="Update Application" class='normalbutton no_display' onclick="return submitVapApplication();" >Proceed</a></div>
                <?php } ?>

                <!--div class="a_button" id="multiFormSubmit1"><a href="javascript:void(0);" id="multiFormSubmit1" value="Print" class="input_btn"  onClick= "printPassportNew();return false;">Print</a></div-->
            </div>
        </div>
        <?php if ($form->getObject()->isNew()) {?>
            <?php }else if (!$form->getObject()->isNew()) { ?>
        <input type="hidden" name="sf_method" value="put" />
        <?php } ?>
        <!-- </div> -->
    </form>


    <div class="pixbr XY20"></div>
    <div class="warningArea">
        <h2>Warning</h2>
        <ul>
            <li>PLEASE ENSURE THAT ALL INFORMATION PROVIDED ON YOUR APPLICATION IS CORRECT BEFORE YOU PROCEED TO PAYMENT. YOUR APPLICATION CANNOT BE
                EDITED ONCE PAYMENT IS INITIATED.
            </li>
        </ul>
    </div>
    <input type="hidden" name="activeTab" id="activeTab" value="1" />
</div>
<div class="content_wrapper_bottom"></div>
<script>
    var ht='';
<?php
//echo '<pre>';print_r($errorFields);die;
if(count($errorFields) > 0){
    for($i=1;$i<=count($errorFields);$i++){
        if(count(array_filter($errorFields['Step '.$i])) > 0){
            //$errorPageNumber = $i;
            $error = array_filter($errorFields['Step '.$i]);
            ?>
                ht += "<div class='tmz-error-step-heading'><span id='<?php echo $i; ?>' onclick='expandDiv(\"Step "+<?php echo $i; ?>+"\","+<?php echo $i; ?>+")' style='cursor:pointer;'>[-]</span> <span class='tmz-step-heading'>Step "+<?php echo $i; ?>+"</span>&nbsp;&nbsp; [Please <span style='cursor:pointer' onclick='goToDirectTab(\"Step "+<?php echo $i; ?>+"\")'><b><u>Click Here</u></b></span> to see the errors  <input type ='hidden' id='hdn_"+ <?php echo $i; ?> +"' value='0'>]</div>";
                ht += '<div class ="tmz-error-step-info" style="display:blank;" id="div_'+<?php echo $i; ?>+'">';

            <?php foreach($error as $key=>$val){ ?>
                ht += "<ul><li><?php echo trim($val); ?></li></ul>";
                <?php } ?>
                    ht += '</div>';
            <?php
        }
    }
}


$errorPageNumber = 0;
if(count($errorFields) > 0){
    for($i=1;$i<=count($errorFields);$i++){
        if(count(array_filter($errorFields['Step '.$i])) > 0){
            $errorPageNumber = $i-1;
            break;
        }
    }
}

?>
    if(ht !=''){
        $('#error_msg_div').show();
        $('#inner_error_div').html(ht);
    }


    // Showing error page...
    var errorPageNumber = '<?php echo $errorPageNumber;?>';
    if (errorPageNumber != -1){
        $('#activeTab').val(errorPageNumber);
        gotoTab('next');
    }

    /**
     * This function is being used to changes address caption on selection of radio button...
     */
    function checkDisplayCompanyCustomer(){
        if($("input[name='vap_application[applicant_type]']:checked").next('label').text() == 'Government Business'){
            document.getElementById("vap_hdn_business_address_label").innerHTML = 'Corresponding Government Agency in Nigeria (include address) <span class="redLbl">*</span>';            
        }else if($("input[name='vap_application[applicant_type]']:checked").next('label').text() == 'Private Business'){
            document.getElementById("vap_hdn_business_address_label").innerHTML = 'Name of Company (include address) <span class="redLbl">*</span>';            
        }
    }

    $(document).ready(function(){
        var applicantType = '<?php echo $applicantType; ?>';
        if(applicantType == 'GB'){
            $("#vap_hdn_business_address_label").html('Corresponding Government Agency in Nigeria (include address) <span class="redLbl">*</span>');
            $('#vap_application_applicant_type_GB').attr('checked','checked');
        }else if(applicantType == 'PB'){
            $("#vap_hdn_business_address_label").html('Name of Company (include address) <span class="redLbl">*</span>');
            $('#vap_application_applicant_type_PB').attr('checked','checked');
        }

        var sponsore_type = '<?php echo $sponsore_type; ?>';
        if(sponsore_type == 'CS'){
            $("#document2_Label").html('Letter of Invitation <span class="redLbl">*</span>');
            $('#vap_application_sponsore_type_CS').attr('checked', 'checked');
        }else if(sponsore_type == 'SS'){
            $("#document2_Label").html('Proof of Funds <span class="redLbl">*</span>');
            $('#vap_application_sponsore_type_SS').attr('checked', 'checked');
        }

        if($("input[name='vap_application[type_of_visa]']").length == 1){
            $("input[name='vap_application[type_of_visa]']").attr('checked','checked');
        }

    })
    
    
    
</script>
