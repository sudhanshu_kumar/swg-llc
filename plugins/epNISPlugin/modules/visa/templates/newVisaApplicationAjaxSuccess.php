<?php if($status == 'new'){ ?>
<div class="NwPopUp">
    <div class="floatRight"><strong>press [Esc] to close</strong></div><!-- Bug Id:33770 -->
    <div class="NwHeading"><?php echo image_tag('/images/info.png',array('alt'=>'SWGlobal LLC', 'align' => 'absmiddle'));?> Application Information</div>
    <?php if($isAppProcess == 'no') { ?>
    <div class="NwMessage">
        An application with these details exists in our system with application id <strong><?php echo $appId; ?></strong>&nbsp;and reference number <strong><?php echo $ref_no; ?></strong>. This application is <strong>NOT PAID</strong> per current status.
        <br /><br />
        You can <strong>edit, print and/or make payment</strong> for the existing application by clicking <strong>"Open existing application"</strong> button below. If you choose to start a new application, this existing application will be cancelled and will not be available in system going forward.
        <br /><br /><br />
        <strong>Do you wish to proceed with New Application?</strong>
    </div>
    <br />
    <div align="center"><input type="button" id="continue" class="normalbutton" value="No, open existing application" onclick="openExistingApp('<?php echo $appId; ?>');" />&nbsp;&nbsp;<input type="button" class="normalbutton" value="Yes, proceed to new application" onclick="openNewApp();" /> </div>
    <?php } else { ?>
    <div class="NwMessage">
        An application with these details exists in our system with application id <strong><?php echo $appId; ?></strong>&nbsp;and reference number <strong><?php echo $ref_no; ?></strong>. This application is <strong>NOT PAID</strong> per current status.
        <br /><br />
        <span class="red"><strong>Your application is under payment awaited state!! you can not edit this application.</strong></span>
        <br /><br />
        You can <strong>view, print and/or make payment</strong> for the existing application by clicking <strong>"Open existing application"</strong> button below. If you choose to start a new application, this existing application will be cancelled and will not be available in system going forward.
        <br /><br /><br />
        <strong>Do you wish to proceed with New Application?</strong>
    </div>
    <br />
    <div align="center"><input type="button" class="normalbutton" value="No, open existing application" onclick="printExistingApp('<?php echo $printUrl; ?>');" />&nbsp;&nbsp;<input type="button" class="normalbutton" value="Yes, proceed to new application" onclick="openNewApp();" /> </div>
    <?php } ?>
</div>
<?php } else { ?>
<div class="NwPopUp"> 
    <div class="floatRight"><strong>press [Esc] to close</strong></div><!-- Bug Id:33770 -->
    <div class="NwHeading"><?php echo image_tag('/images/info.png',array('alt'=>'SWGlobal LLC', 'align' => 'absmiddle'));?> Application Information</div>
    <div class="NwMessage">
        An application with these details exists in our system with application id  <strong><?php echo $appId; ?></strong>&nbsp;and reference number <strong><?php echo $ref_no; ?></strong>. This application is <strong>PAID</strong> per current status.
        <br /><br />
        You can <strong>view and print</strong> the existing application by clicking <strong>"Open existing application"</strong> button below. If you choose to start a new application, this existing application will be cancelled and will not be available in system going forward.<br />Please note that your existing application fee WILL NOT be refunded in case you choose to start a new application with these details.
        <br /><br /><br />
        <strong>Do you wish to proceed with New Application?</strong>
    </div>
    <br />
    <div class="btnArea" align="center" ><input type="button" id="existing" class="normalbutton" value="No, open existing application" onclick="printExistingApp('<?php echo $printUrl; ?>');" />&nbsp;&nbsp;<input type="button" class="normalbutton" value="Yes, proceed to new application" onclick="openNewApp();" /> </div>
</div>
<?php } ?>