<div class="noscreen"> <img src="<?php echo image_path('nis.gif'); ?>" alt="Test" class="pd4" /> </div>
<?php use_javascript('jquery.printElement.js'); ?>
<link rel="stylesheet" type="text/css" media="print" href="<?php  echo stylesheet_path('printPopUpMessage');?>" />
<script>
$(document).ready(function(){
    $('#popPrint').click(function(){
        printSelection();
    });
});
/* Start Ashwani This function is using jquery print  */
    function printSelection(){
     printElem({});     
   }
   function printElem(options){
     $('#mainDiv').printElement(options);
   }
</script>
<!-- Display popup message or not -->
<?php //if($sf_request->getParameter('p') !='i') {?>
    <?php
    if($msgFlag){
        //echo ePortal_popup("<div class='highlight red' id='flash_notice'><b>Important!!! You have to generate tracking number for this application to complete the payment process, by clicking on 'Proceed to Online Payment' from 'CART' page.</b></div>Please 1Keep This Safe","<p><b>You will need it later</b></p><h5>Application Id: ".$visa_application[0]['id']."</h5><h5> Reference No: ".$visa_application[0]['ref_no']."</h5>");
        echo ePortal_popup("<div class='highlight red' id='flash_notice'><b>Important!!! You have to generate tracking number for this application to complete the payment process, by clicking on 'Proceed to Online Payment' from 'CART' page.</b></div>"."<div class='confirmation-msg'><font style='color: #FF0000'><center><b>BELOW IS YOUR APPLICATION ID AND REFERENCE NUMBER</b></center>","<p><center><b>PRINT THIS SCREEN OR WRITE DOWN THESE NUMBERS AND KEEP THEM SAFE</b></center></p><p><center><b>YOU WILL NEED THEM TO COMPLETE THE PROCESS AND/OR AT THE EMBASSY/HIGH COMMISSION</b></center></p></font><h5><center>Application Id: ".$visa_application[0]['id']."</center></h5><h5><center> Reference No: ".$visa_application[0]['ref_no']."</center></h5></div>",array('width'=>'650'));
    }else{
       // echo ePortal_popup("Please 1Keep This Safe","<p><b>You will need it later</b></p><h5>Application Id: ".$visa_application[0]['id']."</h5><h5> Reference No: ".$visa_application[0]['ref_no']."</h5>");
        //echo ePortal_popup("<div class='confirmation-msg'><font style='color: #FF0000'><center><b>BELOW IS YOUR APPLICATION ID AND REFERENCE NUMBER</b></center>","<p><center><b>PRINT THIS SCREEN OR WRITE DOWN THESE NUMBERS AND KEEP THEM SAFE</b></center></p><p><center><b>YOU WILL NEED THEM TO COMPLETE THE PROCESS AND/OR AT THE EMBASSY/HIGH COMMISSION</b></center></p></font><h5><center>Application Id: ".$visa_application[0]['id']."</center></h5><h5><center> Reference No: ".$visa_application[0]['ref_no']."</center></h5></div>",array('width'=>'650'));
        //echo ePortal_popup("<font color=red> <center>BELOW IS YOUR APPLICATION ID AND REFERENCE NUMBER .<br />PRINT THIS SCREEN OR WRITE DOWN THESE NUMBERS AND KEEP THEM SAFE.<br /> YOU WILL NEED THEM TO COMPLETE THE PROCESS AND/OR AT THE EMBASSY/HIGH COMMISSION</center></font><h5><p><center>Application Id: ".$visa_application[0]['id']."</center></h5><h5> <center>Reference No: ".$passport_application[0]['ref_no']."</center></p></h5>");
    }

    ?>
<?php //if(isset($chk)){ echo "<script>pop();</script>"; } ?>
<?php //} ?>
<?php $isGratis = false;
      if($visa_application[0]['ispaid'] == 1){
        if(($visa_application[0]['paid_naira_amount'] == 0) && ($visa_application[0]['paid_dollar_amount'] == 0)){
           $isGratis = true;
          }else{
            $isGratis = false;
           }
          }?>
<?php
$sf = sfContext::getInstance()->getUser();
if($sf->hasFlash('notice')){ ?>
    <div id="flash_notice" class="alertBox" >
<?php
    echo nl2br($sf->getFlash('notice'));
?>
</div>
<?php }?>
<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">
    <?php include_partial('global/innerHeading',array('heading'=>"Applicant's Details"));?>
    <div class="clear"></div>
    <div class="tmz-spacer"></div>
<?php
$sf = sfContext::getInstance()->getUser();
if($sf->hasFlash('error')){ ?>
    <div id="flash_error" class="alertBox" >
<?php
    echo nl2br($sf->getFlash('error'));
?>
    </div>
    <br/>
<?php }?>
<form action="<?php echo url_for('cart/list') ?>" method="POST" class='dlForm multiForm'>
<div>
<center>
<?php
if($statusType == 1){
if((isset($countFailedAttempt) && $countFailedAttempt > 0) && ($visa_application[0]['ispaid'] != 1)){

  echo ePortal_highlight("<b>You made $countFailedAttempt payment attempts.None of the payment attempt(s) returned a message.<br /> Please attempt(s) Payment ONLY If You Are Sure The Previous attempt(s) (IF ANY) Was Not Successful.</b>",'',array('class'=>'black'));
}else{
  echo ($visa_application[0]['ispaid'] == 1)?"":ePortal_highlight("<b>No Previous Payment Attempt History Found</b>",'',array('class'=>'black'));}
}
?>
      </center>
  <div  id="Reciept" >
<?php if($show_details==1){ ?>
        <!--<fieldset class="bdr">-->
        <table cellpadding="0" cellspacing="0" border="0" width="100%" class="show_table">
          <tbody>
            <tr>
              <td class="blbar" colspan="2"><strong><?php echo ePortal_legend("Profile"); ?></strong></td>
            </tr>
            <tr>
              <td><label>Full Name:</label></td>
              <td><?php echo ePortal_displayName($visa_application[0]['title'],$visa_application[0]['other_name'],$visa_application[0]['middle_name'],$visa_application[0]['surname']);?></td>
            </tr>
            <tr>
              <td><label>Gender:</label></td>
              <td><?php echo $visa_application[0]['gender'];?></td>
            </tr>
            <tr>
              <td><label>Date of Birth:</label></td>
              <td><?php $datetime = date_create($visa_application[0]['date_of_birth']); echo date_format($datetime, 'd/F/Y');?></td>
            </tr>
            <tr>
              <td><label>Email:</label></td>
              <td><?php if($visa_application[0]['email']!='') echo $visa_application[0]['email']; else echo "--"; ?></td>
            </tr>
            <tr>
              <td><label>Country of Origin:</label></td>
              <td><?php echo $visa_application[0]['CurrentCountry']['country_name']; ?></td>
            </tr>
            <tr>
              <td><label>State of Origin:</label></td>
              <td>Not Applicable</td>
            </tr>
            <tr>
              <td><label>Profession:</label></td>
              <td><?php echo $visa_application[0]['profession']; ?></td>
            </tr>
<?php } ?>
<?php if($show_details==1){ ?>
            <tr>
              <td class="blbar" colspan="2"><strong><?php echo ePortal_legend("Application Information "); ?></strong></td>
            </tr>
            <tr>
              <td><label>Category:</label></td>
              <td><?php if($visa_application[0]['VisaCategory']['var_value'] == "Fresh"){ echo "Entry Visa" ."/Permit";}else{echo "Fresh Free Zone Visa" ."/Permit";}?></td>
            </tr>
            <tr>
              <td><label>Type:</label></td>
              <td><?php if($visa_application[0]['VisaApplicantInfo']['VisaTypeId']['var_value']!="Free Zone Visa"){echo $visa_application[0]['VisaApplicantInfo']['VisaTypeId']['var_value']."&nbsp;Visa";}else{echo $visa_application[0]['VisaApplicantInfo']['VisaTypeId']['var_value'];} ?></td>
            </tr>
            <tr>
              <td><label>Request:</label></td>
              <td><?php if($visa_application[0]['VisaCategory']['var_value'] == "Fresh"){echo "Entry Visa";}else{echo "Free Zone Entry Visa";}
      if(!empty($visa_application[0]['VisaApplicantInfo']['no_of_re_entry_type']))
      {echo"-&nbsp;[&nbsp;".$visa_application[0]['VisaApplicantInfo']['no_of_re_entry_type']."&nbsp;Multiple Entries ]";
      }
      ?></td>
            </tr>
            <tr>
              <td><label>Date:</label></td>
              <td><?php $datetime = date_create($visa_application[0]['created_at']); echo date_format($datetime, 'd/F/Y'); ?></td>
            </tr>
<?php } ?>
            <tr>
              <td><label>Application Id:</label></td>
              <td><?php echo $visa_application[0]['id'];?></td>
            </tr>
            <tr>
              <td><label>Reference No:</label></td>
              <td><?php echo $visa_application[0]['ref_no']; ?></td>
            </tr>
            <tr>
              <td colspan="2" class="blbar"><strong><?php echo ePortal_legend("Processing Information"); ?></strong> </td>
            </tr>
<?php if($show_details==1){ ?>
            <tr>
              <td><label>Country:</label></td>
              <td><?php echo $applying_country ;?></td>
            </tr>
            <tr>
              <td><label>State:</label></td>
              <td>Not Applicable</td>
            </tr>
            <tr>
              <td><label>Embassy: </label></td>
              <td><?php if($applying_country != "Nigeria"){echo (isset($visa_application[0]['VisaApplicantInfo']['EmbassyMaster']))?$visa_application[0]['VisaApplicantInfo']['EmbassyMaster']['embassy_name']:'Not Available';}else{echo "Not Applicable";}?></td>
            </tr>
            <tr>
              <td><label>Office:</label></td>
              <td>Not Applicable</td>
            </tr>
<?php } ?>
<?php if($show_payment==1){ ?>
            <tr>
              <td><label>Interview Date:</label></td>
              <td><?php if($visa_application[0]['ispaid'] == 1)
    {
      if(($visa_application[0]['paid_naira_amount'] == 0) && ($visa_application[0]['paid_dollar_amount'] == 0))
      {
      echo "Check the Passport Office / Embassy / High Commission";
      }else
      {
        $datetime = date_create($visa_application[0]['interview_date']); echo date_format($datetime, 'd/F/Y');
      }
    }else
    {
      echo "Available after Payment";
    } ?>
<?php } ?>
<?php
$isPaid = $visa_application[0]['ispaid'];
if($show_payment==1 || $isPaid==0){ ?>
              </td>
            </tr>
            <tr>
              <td class="blbar" colspan="2"><strong><?php echo ePortal_legend("Payment Information"); ?></strong></td>
            </tr>
      <?php

    $additional_charges_text = sfConfig::get('app_visa_additional_charges_text');

    /**
     * [WP: 102] => CR: 144
     * Showing amount according to currency and with service charge if applicable...
     */
    if($visa_application[0]['visacategory_id'] == 29 || $visa_application[0]['visacategory_id'] == 101){
          $visaType = 'visa';
    }else{
      $visaType = '';
    }
    $visa_additional_charges_flag = sfConfig::get('app_visa_additional_charges_flag');
    $service_charges = sfConfig::get('app_visa_additional_charges');

    $transactionServiceChargesObj = Doctrine::getTable('TransactionServiceCharges')->getSingleRecordByAppIdAndAppType($visa_application[0]['id'], $visaType);

    if($visa_application[0]['ispaid'] == 1){
      if($visa_application[0]['paid_dollar_amount'] != 0.00){
        if($visa_application[0]['currency_id'] == 4){            
            echo "<tr><td><label>Yuan Amount:</label><td>CNY&nbsp;".$visa_application[0]['amount']."</td></tr>";
        }else{            
            if(count($transactionServiceChargesObj) > 0){
                $appOrigAmount = $transactionServiceChargesObj->getFirst()->getAppAmount();
                $service_charge = $transactionServiceChargesObj->getFirst()->getServiceCharge();
                $service_charges_text = ($service_charge > 0)?' ('.$additional_charges_text.': USD '.$service_charge.')':'';
                echo "<tr><td><label>Dollar Amount:</label><td>USD&nbsp;".$appOrigAmount.$service_charges_text."</td></tr>";
                
            }else{
                echo "<tr><td><label>Dollar Amount:</label><td>USD&nbsp;".$visa_application[0]['paid_dollar_amount']."</td></tr>";
            }
        }
      }
    }else{
      if(isset($payment_details['dollar_amount'])){
          $service_charges_text = ($visa_additional_charges_flag)?' ('.$additional_charges_text.': USD'.$service_charges.')':'';
          echo "<tr><td><label>Dollar Amount:</label></td><td>USD&nbsp;".$payment_details['dollar_amount'].$service_charges_text."</td></tr>";
      }else if(isset($payment_details['yuan_amount'])){          
          $appAmountData = Functions::getConvertedVisaAndAdditionalFees($visa_application[0]['id'], $visaType, $service_charges);
          if($visa_additional_charges_flag){
              $appAmount = $appAmountData['appAmount'] + $appAmountData['additional_charges'];
          }else{
              $appAmount = $appAmountData['appAmount'];
          }          
          $service_charges_text = ($visa_additional_charges_flag)?' ('.$additional_charges_text.': CNY '.$appAmountData['additional_charges'].')':'';
          echo "<tr><td><label>Yuan Amount:</label></td><td>CNY&nbsp;".$appAmount.$service_charges_text."</td></tr>";
      }else{
          echo "<tr><td><label>Dollar Amount:</label></td><td>USD&nbsp;"."0</td></tr>";
      }
    }
    ?>
            <tr>
              <td><label>Payment Status:</label></td>
              <td><?php
    $isGratis = false;
    if($visa_application[0]['ispaid'] == 1)
    {
      if(($visa_application[0]['paid_naira_amount'] == 0) && ($visa_application[0]['paid_dollar_amount'] == 0))
      {
        $isGratis = true;
        echo "This Application is Gratis (Requires No Payment)";
      }else
      {
      echo "Payment Done";
      }
    }else{
      echo "Available after Payment";
    }

    ?></td>
            </tr>
            <tr>
              <td><label>Payment Currency:</label></td>
              <td><?php
    if($visa_application[0]['ispaid'] == 1)
    {
      if(($visa_application[0]['paid_naira_amount'] == 0) && ($visa_application[0]['paid_dollar_amount'] == 0))
      {
        echo "Not Applicable";
      }else
      {
        if($visa_application[0]['currency_id'] == 4){
            echo "Yuan";
        }else{
            echo $paymentCurrency;
        }
      }
    }else{
      echo "Available after Payment";
    }
    ?></td>
            </tr>
            <tr>
              <td><label>Payment Gateway:</label></td>
              <td><?php
    if($visa_application[0]['ispaid'] == 1)
    {
      if(($visa_application[0]['paid_naira_amount'] == 0.00) && ($visa_application[0]['paid_dollar_amount'] == 0.00))
      {
        echo "Not Applicable";
      }else
      {
     echo $PaymentGatewayType;
      }
    }else{
      echo "Available after Payment";
    }
    ?></td>
            </tr>
            <tr>
              <td><label>Amount Paid:</label></td>
              <td><?php
    if($visa_application[0]['ispaid'] == 1)
    {
      if(($visa_application[0]['paid_naira_amount'] == 0.00) && ($visa_application[0]['paid_dollar_amount'] == 0.00))
      {
        echo "Not Applicable";
      }else
      {
         if($visa_application[0]['currency_id'] == 4){      
           echo "CNY ".$visa_application[0]['amount'];
         }else{            
            if(count($transactionServiceChargesObj) > 0){
                $appOrigAmount = $transactionServiceChargesObj->getFirst()->getAppAmount();
                $service_charge = $transactionServiceChargesObj->getFirst()->getServiceCharge();
                $service_charges_text = ($service_charge > 0)?' (Inclusive service charge: USD '.$service_charge.')':'';
                echo $currencySign."&nbsp;".$appOrigAmount.$service_charges_text;

            }else{
               echo $currencySign."&nbsp;".$paidAmount;
            }
         }
      }
    }else
    {
      echo "Available after Payment";
    }
    ?>
              </td>
<?php  } ?>
<?php if($visa_application[0]['status'] != 'New' && $visa_application[0]['status'] != 'Paid'){?>
            </tr>
            <tr>
              <td class="blbar" colspan="2"><?php echo ePortal_legend("Application Status"); ?></td>
            </tr>
            <tr>
              <td><label>Current Application Status:</label></td>
              <td><?php echo $visa_application[0]['status'] ;?></td>
<?php }?>
<?php

if($isPaid == 1)
{ 
?>
<?php if($show_payment==1){ 
    echo ePortal_highlight('YOUR PAYMENT PROCESS WAS SUCCESSFULL! PLEASE PRINT YOUR ACKNOWLEDGMENT & RECEIPT SLIP.','',array('class'=>'green'));
} ?>
            </tr>
          </tbody>
        </table>
        <div class="clear"></div>
<div class="pixbr XY20"> 
  <center id="multiFormNav">
   <table  align="center" class="nobrd">
              <tr>
                <td><!--<div class="lblButton" >-->
     <!-- <input type="button" value="Print Acknowledgment Slip" onclick="javascript:window.open('<?php// echo url_for('visa/visaAcknowledgmentSlip?id='.$encriptedAppId) ?>','MyPage','width=750,height=700,scrollbars=1,resize=no');">&nbsp;&nbsp;-->
<?php if(($paidAmount != 0) || $isGratis){ ?>
<?php if($visa_application[0]['VisaCategory']['var_value'] == "Fresh"){ $AppTypes='1';}else{$AppTypes='3';}?>
<?php if($visa_application[0]['ispaid'] == 1 && $show_details==1 && sfConfig::get('app_enable_pay4me_validation')==1) {  ?>
                        <input type="hidden" name="AppTypes"  value="<?php echo SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT($AppTypes)); ?>"/>
                        <input type="hidden" name="AppId"  value="<?php echo SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT($visa_application[0]['id'])); ?>"/>
                        <input type="hidden" name="RefId"  value="<?php echo SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT($visa_application[0]['ref_no'])); ?>"/>
                      <?php if(!$isGratis) { ?>
                          <input type="hidden" name="app_gratis"  value="<?php echo SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT(1)); ?>"/>
                       <?php }else{ ?>
                          <input type="hidden" name="app_gratis"  value="<?php echo SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT(2)); ?>"/>
                      <?php } ?>
                <?php if($data['pay4me'] && !$data['validation_number']){?>
                <input type="hidden" name="ErrorPage"  value="<?php echo SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT(1)); ?>"/>
                <input type="hidden" name="GatewayType"  value="<?php echo SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT(1)); ?>"/>
                <input type="hidden" name="id"  value="<?php echo SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT($visa_application[0]['id'])); ?>"/>
                  <input type="submit" id="multiFormSubmit" class="normalbutton"  value='View your application payment status' onClick="this.form.action='<?php echo secure_url_for('visa/show'); ?>';" />
                <?php } else{?>
                  <input type="submit" id="multiFormSubmit" class="normalbutton"  value='View your application payment status' onClick="this.form.action='<?php echo secure_url_for('visa/OnlineQueryStatus'); ?>';" />
                <?php }?>
                 <?php } ?>
  <?php if($show_payment==1){ ?>
  <?php if(!$isGratis) { ?>
     <!--<input type="button"  class="button"  value="Print Receipt" onclick="javascript:window.open('<?php //echo url_for('visa/PrintRecipt?visa_app_id='.$encriptedAppId.'&visa_app_refId='.$encriptedRefId) ?>','MyPage','width=750,height=700,scrollbars=1,location=no');">&nbsp;&nbsp;-->
  <?php } ?>
    <!--<input type="button"  class="button"  value="Print Acknowledgment Slip" onclick="javascript:window.open('<?php //echo url_for('visa/visaAcknowledgmentSlip?id='.$encriptedAppId) ?>','MyPage','width=750,height=700,scrollbars=1');">&nbsp;&nbsp;-->
<?php } } ?>
                </td>
              </tr>
            </table>
  </center>
</div>
</div>
<?php
}else{
?>
        <div class="pixbr XY20">
  <center id="multiFormNav" >
            <table  align="center" class="nobrd noPrint" width="99%">
              <tr>
                <td align="right">&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td width="40%" align="right">
                  <input name="button" type="button" class="normalbutton" id="multiPrintw" onclick="javascript:window.print()" value='Print' />
                </td>
                <td width="50%" align="left">
                  <input type="submit" class="normalbutton" id="multiFormSubmit" value='Proceed To Checkout'>
                </td>
              </tr>
            </table>
  </center>
</div>
  </div>
<?php // if(!$IsProceed) echo ePortal_highlight("You have attempted a payment in last ".sfConfig::get("app_time_interval_payment_attempt")." minutes!! Please wait for ".sfConfig::get("app_time_interval_payment_attempt")." minutes.",'',array('class'=>'red')); ?>
<?php if($show_details==1){ ?>
  <?php // echo ePortal_highlight('PLEASE CONFIRM YOUR ORDER BEFORE PROCEEDING TO PAYMENTS. NIS WILL NOT REFUND APPLICANT FOR A WRONG ORDER / PAYMENT','WARNING',array('class'=>'yellow'));?>
  <?php echo ePortal_highlight('PLEASE ENSURE THAT ALL INFORMATION PROVIDED ON YOUR APPLICATION IS CORRECT BEFORE YOU PROCEED TO PAYMENT.  YOUR APPLICATION CANNOT BE EDITED ONCE PAYMENT IS INITIATED.<br/>','WARNING',array('class'=>'yellow'));?>
<?php }
}
?>
<input type ="hidden" value="<?php echo $getAppIdToPaymentProcess;?>" name="appDetails" id="appDetails"/>
</form>
</div>
</div>
<div class="content_wrapper_bottom"></div>
