<?php function getDateFormate($date)
{
 $dateFormate = date("d-m-Y", strtotime($date));

  return $dateFormate;
} ?>
<link rel="stylesheet" type="text/css" media="print" href="<?php  echo stylesheet_path('print');?>" />
  <div class="noscreen" style="margin-top: -5px; position:absolute;" > <img src="<?php echo image_path('nis.gif'); ?>" alt="Test" class="pd4" /> </div>
  <?php if ($sf_user->hasFlash('error')): ?>
  <div id="flash_notice" class="alertBox" > <?php echo nl2br($sf_user->getFlash('error'));?></div>
  <?php  endif  ?>
<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">
    <?php include_partial('global/innerHeading', array('heading' => 'New Re-Entry Visa Application (Free Zone)')); ?>
    <div class="clear"></div>
    <div class="tmz-spacer"></div>
      <div id="printHTML">
        <div class="clear_print"></div>
        <div class="page_heading_new">New Re-Entry Visa Application (Free Zone)</div>
              <div class="inst_bg">
                <div class="inst_container1">
                  <ul>
                    <li>FOR ANY QUESTIONS AND/OR CONCERNS PLEASE CONTACT US AT <a href="mailto:support@swgloballlc.com" class="mail_link">support@swgloballlc.com</a></li>
                    <li>APPLICATION FEES PAID FOR VISA/FREEZONE ARE NON REFUNDABLE EXCEPT IN CASED OF DOUBLE PAYMENT MISTAKENLY MADE FOR THE SAME APPLICATION OR FRAUDULENT USE OF CREDIT/DEBIT CARD. TO SEEK A REFUND PLEASE CONTACT <a href="mailto:refund@swgloballlc.com" class="mail_link">refund@swgloballlc.com</a></li>
                    <li>PAYMENTS ARE VALID FOR SIX (6) MONTHS ONLY FROM THE PAYMENT DATE.</li>
                    <li>ONLY online payment is acceptable.</li>
                    <li>Anyone who pays otherwise and receives service is subject to prosecution and revocation of Visa.</li>
                    <li>If you have already completed an application, please check your application status rather than completing a duplicate application</li>
                  </ul>
                </div>
              </div>
              <div class="clear"></div>
            <div class='confirmation-msg'>
        <div class="confirmation-msg1">
                <p>
                  <center>
              <b class="red">BELOW IS YOUR APPLICATION ID AND REFERENCE NUMBER</b>
                  </center>
                </p>
                <p>
                  <center>
              <b class="red">KINDLY PRINT THE SCREEN AND KEEP IT SAFE</b>
                  </center>
                </p>
                <p>
                  <center>
              <b class="red">YOU WILL NEED THEM TO COMPLETE THE PROCESS AT THE EMBASSY, CONSULATE OR HIGH COMMISSION</b>
                  </center>
                </p>
              </div>
              <h5 class="print_text">
                <center>
            Application Id: <?php echo $visa_application[0]['id']; ?>
                </center>
              </h5>
              <h5 class="print_text">
                <center>
            Reference No: <?php echo $visa_application[0]['ref_no'] ?>
                </center>
              </h5>
            </div>
      <div class="clear">&nbsp;</div>
      <table cellpadding="0" cellspacing="0" border="0" class="no_border_new" width="695" id="uiGroup_">
                  <tr>
          <td width="347" height="20" valign="middle" id="form_new_heading">Personal Information</td>
          <td valign="top" id="border_new_left" rowspan="32"></td>
          <td width="347" height="20" valign="middle" id="form_new_heading">Passport Information</td>
        </tr>
       
        <tr>
          <td valign="top"><table width="347" border="0" cellspacing="0" cellpadding="0" class="no_border_new">
              <tr class="noPrint">
                <td height="5"></td>
                <td height="5"></td>
              </tr>
              <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_title">Title</label></td>
                    <td width="175" valign="top" class="lblfield_new"><span class="lblField">
                      <?php
                  if(isset($visa_application[0]['title']) && $visa_application[0]['title']!='')
                  {
                      echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['title']."'>";
                  }
                  else
                  {
                  echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_surname">Last name (<i>Surname</i>)</label></td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php
                  if(isset($visa_application[0]['surname']) && $visa_application[0]['surname']!='')
                  {
                  echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['surname']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }
                  ?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_other_name">First name</label></td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php
                  if(isset($visa_application[0]['other_name']) && $visa_application[0]['other_name']!='')
                  {
                    echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['other_name']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }

                  ?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_middle_name">Middle name</label>
                      <span class="redLbl"></span></td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php
                  if(isset($visa_application[0]['middle_name']) && $visa_application[0]['middle_name']!='')
                  {
                    echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['middle_name']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }
                  ?>
                      </span></td>
                  </tr>
                  <tr>
                    <td valign="top" class="label_name_new"><label for="visa_application_gender">Gender</label></td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php
                  if(isset($visa_application[0]['gender']) && $visa_application[0]['gender']!='')
                  {
                      echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['gender']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }?>
                      </span> </td>
                  </tr>
                  <tr>
                    <td valign="top" class="label_name_new"><label for="visa_application_email">Email</label>
                    </td>
                    <td valign="top" class="lblfield_new"><div class="lblField">
                        <?php
                  if(isset($visa_application[0]['email']) && $visa_application[0]['email']!='')
                  {
                      echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['email']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }
                  ?>
                      </div></td>
                  </tr>
                  <tr>
                    <td valign="top" class="label_name_new"><label for="visa_application_date_of_birth">Date of birth(dd-mm-yyyy)</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php
                if(isset($visa_application[0]['date_of_birth']) && $visa_application[0]['date_of_birth']!='')
                {
                      $explode = explode('-',getDateFormate($visa_application[0]['date_of_birth']));
                      echo "<input type='text' class='printdate' readonly=readonly value='".$explode[0]."'>";
                      echo "<input type='text' class='printdate' readonly=readonly value='".$explode[1]."'>";
                      echo "<input type='text' class='printyear' readonly=readonly value='".$explode[2]."'>";

                }
                else
                {
                    echo "<input type='text' class='printdate' readonly=readonly value='--'>";
                    echo "<input type='text' class='printdate' readonly=readonly value='--'>";
                    echo "<input type='text' class='printyear' readonly=readonly value='--'>";
                }
                ?>
                      </span> </td>
                  </tr>
                  <tr>
                    <td valign="top" class="label_name_new"><label for="visa_application_place_of_birth">Place of birth</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php
                  if(isset($visa_application[0]['place_of_birth']) && $visa_application[0]['place_of_birth']!='')
                  {
                      echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['place_of_birth']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }
                  ?>
                      </span> </td>
                  </tr>
                  <tr>
                    <td valign="top" class="label_name_new"><label for="visa_application_present_nationality_id">Nationality</label>
                </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php
                  if(isset($visa_application[0]['CurrentCountry']['country_name']) && $visa_application[0]['CurrentCountry']['country_name']!='')
                  {
                      echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['CurrentCountry']['country_name']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }
                  ?>
                      </span> </td>
                  </tr>
            </table></td>
          <td valign="top"><table width="347" border="0" cellspacing="0" cellpadding="0" class="no_border_new">
                         <tr class="noPrint">
                <td height="5"></td>
                <td height="5"></td>
              </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_ReEntryApplicantInfo_passport_number">Passport number</label>
                    </td>
                    <td width="175" valign="top" class="lblfield_new"><span class="lblField">
                      <?php
                  if(isset($visa_application[0]['ReEntryVisaApplication']['passport_number']) && $visa_application[0]['ReEntryVisaApplication']['passport_number']!='')
                  {
                  echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['ReEntryVisaApplication']['passport_number']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }
                  ?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_ReEntryApplicantInfo_issusing_govt">Issuing country</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php
                  if(isset($visa_application[0]['ReEntryVisaApplication']['issusing_govt']) && $visa_application[0]['ReEntryVisaApplication']['issusing_govt']!=''){
                      echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['ReEntryVisaApplication']['issusing_govt']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }
                  ?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_ReEntryApplicantInfo_date_of_issue">Date of issue (dd-mm-yyyy)</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php
                  if(isset($visa_application[0]['ReEntryVisaApplication']['date_of_issue']) && $visa_application[0]['ReEntryVisaApplication']['date_of_issue']!='')
                   {
                      $explode = explode('-',getDateFormate($visa_application[0]['ReEntryVisaApplication']['date_of_issue']));
                      echo "<input type='text' class='printdate' readonly=readonly value='".$explode[0]."'>";
                      echo "<input type='text' class='printdate' readonly=readonly value='".$explode[1]."'>";
                      echo "<input type='text' class='printyear' readonly=readonly value='".$explode[2]."'>";
                      //echo getDateFormate();
                  }
                  else
                  {
                    echo "<input type='text' class='printdate' readonly=readonly value='--'>";
                    echo "<input type='text' class='printdate' readonly=readonly value='--'>";
                    echo "<input type='text' class='printyear' readonly=readonly value='--'>";
                  }?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_ReEntryApplicantInfo_date_of_exp">Expiry Date (dd-mm-yyyy)</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php
                if($visa_application[0]['ReEntryVisaApplication']['date_of_exp']!='')
                {

                  $explode = explode('-',getDateFormate($visa_application[0]['ReEntryVisaApplication']['date_of_exp']));
                    echo "<input type='text' class='printdate' readonly=readonly value='".$explode[0]."'>";
                    echo "<input type='text'class='printdate' readonly=readonly value='".$explode[1]."'>";
                    echo "<input type='text' class='printyear' readonly=readonly value='".$explode[2]."'>";
                }
                else
                {
                    echo "<input type='text' class='printdate' readonly=readonly value='--'>";
                    echo "<input type='text' class='printdate' readonly=readonly value='--'>";
                    echo "<input type='text' class='printyear' readonly=readonly value='--'>";
                }
               ?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_ReEntryApplicantInfo_place_of_issue">Place of Issue</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php
                  if( $visa_application[0]['ReEntryVisaApplication']['place_of_issue']!='')
                  {
                      echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['ReEntryVisaApplication']['place_of_issue']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }
                  ?>
                      </span></td>
                  </tr>
            </table></td>
        </tr>
                 
        <tr>
          <td height="20" valign="middle" id="form_new_heading">Address</td>
          <td height="20" valign="middle"></td>
        </tr>
        <tr>
          <td valign="top"><table width="345" border="0" cellspacing="0" cellpadding="0" class="no_border_new">
                <tr class="noPrint">
                <td valign="top" height="5"></td>
                <td valign="top" height="5"></td>
              </tr>
              <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_ReEntryVisaAddress_address_1">Address 1</label>
                    </td>
                    <td width="175" valign="top"><span class="lblField">
                      <?php
                  if($visa_application[0]['ReEntryVisaApplication']['ReEntryVisaAddress']['address_1']!='')
                  {
                      echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['ReEntryVisaApplication']['ReEntryVisaAddress']['address_1']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }
                  ?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_ReEntryVisaAddress_address_2">Address 2</label>
                      <span class="redLbl"></span> </td>
                    <td valign="top"><span class="lblField">
                      <?php
                  if($visa_application[0]['ReEntryVisaApplication']['ReEntryVisaAddress']['address_2']!='')
                  {
                  echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['ReEntryVisaApplication']['ReEntryVisaAddress']['address_2']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }
                  ?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_ReEntryVisaAddress_city">City</label>
                    </td>
                    <td valign="top"><span class="lblField">
                      <?php
                  if($visa_application[0]['ReEntryVisaApplication']['ReEntryVisaAddress']['city']!='')
                  {
                      echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['ReEntryVisaApplication']['ReEntryVisaAddress']['city']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }
                  ?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_ReEntryVisaAddress_country_id">Country</label>
                    </td>
                    <td valign="top"><span class="lblField">
                      <?php
                  if($visa_application[0]['visa_addess_country']!='')
                  {
                  echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['visa_addess_country']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }
                  ?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_ReEntryVisaAddress_state">State</label>
                    </td>
                    <td valign="top"><span class="lblField">
                      <?php
                  if($visa_application[0]['ReEntryVisaApplication']['ReEntryVisaAddress']['state']!='')
                  {
                      echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['ReEntryVisaApplication']['ReEntryVisaAddress']['state']."'>";
                  }
                  else{
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }
                  ?>
                      </span></td>
                  </tr>
                  <tr>
                    <td valign="top" class="label_name_new"><label for="visa_application_ReEntryVisaAddress_postcode">Postcode</label>
                      <span class="redLbl"></span> </td>
                    <td valign="top"><span class="lblField">
                      <?php
                  if($visa_application[0]['ReEntryVisaApplication']['ReEntryVisaAddress']['postcode']!='')
                  {
                  echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['ReEntryVisaApplication']['ReEntryVisaAddress']['postcode']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }
                  ?>
                      </span></td>
                  </tr>
            </table></td>
          <td valign="top"><table width="345" border="0" cellspacing="0" cellpadding="0" class="no_border_new">
                  
              <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_ReEntryApplicantInfo_profession">Profession</label>
                    </td>
                    <td width="175" valign="top"><span class="lblField">
                      <?php
                  if($visa_application[0]['ReEntryVisaApplication']['profession']!=''){
                      echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['ReEntryVisaApplication']['profession']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }
                  ?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_ReEntryApplicantInfo_reason_for_visa_requiring">Reason for requiring visa</label>
                    </td>
                    <td valign="top"><span class="lblField">
                      <?php
                  if($visa_application[0]['ReEntryVisaApplication']['reason_for_visa_requiring']!='')
                  {
                      echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['ReEntryVisaApplication']['reason_for_visa_requiring']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }
                  ?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_ReEntryApplicantInfo_last_arrival_in_nigeria">Date of last arrival in <br/>
                      Nigeria(dd-mm-yyyy)</label>
                    </td>
                <td valign="top"  class="lblfield_new"><span class="lblField">
                      <?php
                if($visa_application[0]['ReEntryVisaApplication']['last_arrival_in_nigeria']!=''){
                  $explode = explode('-',getDateFormate($visa_application[0]['ReEntryVisaApplication']['last_arrival_in_nigeria']));
                  echo "<input type='text' class='printdate' readonly=readonly value='".$explode[0]."'>";
                  echo "<input type='text' class='printdate' readonly=readonly value='".$explode[1]."'>";
                  echo "<input type='text' class='printyear' readonly=readonly value='".$explode[2]."'>";
                }
                else
                {
                    echo "<input type='text' class='printdate' readonly=readonly value='--'>";
                    echo "<input type='text' class='printdate' readonly=readonly value='--'>";
                    echo "<input type='text' class='printyear' readonly=readonly value='--'>";
                }
                ?>
                      </span> </td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_ReEntryApplicantInfo_re_entry_category">Re-entry Category</label>
                    </td>
                    <td valign="top"><span class="lblField">
                      <?php
                if($visa_application[0]['ReEntryVisaApplication']['re_entry_category'] == '3'){
                  echo "<input class='print_input_box' type='text' readonly=readonly value='Re-entry within 3 months'>";
                }else if($visa_application[0]['ReEntryVisaApplication']['re_entry_category'] == '6'){
                  echo "<input class='print_input_box' type='text' readonly=readonly value='Re-entry within 6 months'>";
                }else if($visa_application[0]['ReEntryVisaApplication']['re_entry_category'] == '12'){
                  echo "<input class='print_input_box' type='text' readonly=readonly value='Re-entry within 12 months'>";
                }
                ?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_ReEntryApplicantInfo_re_entry_type">Re-Entry Type</label>
                    </td>
                    <td valign="top"><div class="lblField_radio" style="padding:0px 0 0 0;">
                        <?php
                    $multiFlg='';
                    $singleFlg = '';
                    if($visa_application[0]['ReEntryVisaApplication']['EntryType']['var_value'] == "Single"){
                      $singleFlg = "checked";
                    }else if($visa_application[0]['ReEntryVisaApplication']['EntryType']['var_value'] == "Multiple"){
                      $multiFlg = "checked";
                    }

                    echo "<input type='radio' disabled $singleFlg class='' > Single ";echo "<input type='radio' disabled $multiFlg class='' > Multiple";?>
                      </div></td>
                  </tr>
            </table></td>
        </tr>
                     <tr class="noPrint">
          <td valign="top" height="5"></td>
          <td valign="top" height="5"></td>
        </tr>
        <tr>
          <td height="20" valign="middle" id="form_new_heading">Employer's Information</td>
          <td height="20" valign="middle" id="form_new_heading">Employer's Address</td>
        </tr>
           <tr class="noPrint">
          <td valign="top" height="5"></td>
          <td valign="top" height="5"></td>
        </tr>
        <tr>
          <td valign="top"><table width="347" border="0" cellspacing="0" cellpadding="0" class="no_border_new">
              <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_ReEntryApplicantInfo_employer_name">Name of Employer</label>
                      <span class="redLbl"></span> </td>
                    <td width="175" valign="top"><span class="lblField">
                      <?php
                  if($visa_application[0]['ReEntryVisaApplication']['employer_name']!='')
                  {
                  echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['ReEntryVisaApplication']['employer_name']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }
                  ?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_ReEntryApplicantInfo_employer_phone">Employer's Phone Number</label>
                      <span class="redLbl"></span> </td>
                    <td valign="top"><span class="lblField">
                      <?php
                  if($visa_application[0]['ReEntryVisaApplication']['employer_phone']!='')
                  {
                  echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['ReEntryVisaApplication']['employer_phone']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }

                  ?>
                      </span></td>
                  </tr>
            </table></td>
          <td valign="top"><table width="347" border="0" cellspacing="0" cellpadding="0" class="no_border_new">
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_ReEntryVisaEmployerAddress_address_1">Address 1</label>
                      <span class="redLbl"></span> </td>
                    <td width="175" valign="top"><span class="lblField">
                      <?php
                  if($visa_application[0]['ReEntryVisaApplication']['ReEntryVisaEmployerAddress']['address_1']!=''){
                      echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['ReEntryVisaApplication']['ReEntryVisaEmployerAddress']['address_1']."'>";
                  }
                  else{
                     echo "<input type='text' class='print_input_box' value='--' readonly=readonly>" ;
                  }?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_ReEntryVisaEmployerAddress_address_2">Address 2</label>
                      <span class="redLbl"></span> </td>
                    <td valign="top"><span class="lblField">
                      <?php
                  if($visa_application[0]['ReEntryVisaApplication']['ReEntryVisaEmployerAddress']['address_2']!='')
                  {
                  echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['ReEntryVisaApplication']['ReEntryVisaEmployerAddress']['address_2']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }
                  ?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_ReEntryVisaEmployerAddress_city">City</label>
                      <span class="redLbl"></span></td>
                    <td valign="top"><span class="lblField">
                      <?php
                  if($visa_application[0]['ReEntryVisaApplication']['ReEntryVisaEmployerAddress']['city']!='')
                  {
                  echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['ReEntryVisaApplication']['ReEntryVisaEmployerAddress']['city']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }
                  ?>
                      </span> </td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_ReEntryVisaEmployerAddress_country_id">Country</label>
                      <span class="redLbl"></span> </td>
                    <td valign="top"><span class="lblField">
                      <?php
                  if($visa_application[0]['employer_country']!='')
                  {
                  echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['employer_country']."'>";
                  }

                  else
                      {
                     echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }
                  ?>
                      </span> </td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_ReEntryVisaEmployerAddress_state">State</label>
                      <span class="redLbl"></span> </td>
                    <td valign="top"><span class="lblField">
                      <?php
                  if($visa_application[0]['ReEntryVisaApplication']['ReEntryVisaEmployerAddress']['state']!='')
                  {
                      echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['ReEntryVisaApplication']['ReEntryVisaEmployerAddress']['state']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }
                  ?>
                      </span> </td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_ReEntryVisaEmployerAddress_postcode">Postcode</label>
                      <span class="redLbl"></span> </td>
                    <td valign="top"><span class="lblField">
                      <?php
                  if($visa_application[0]['ReEntryVisaApplication']['ReEntryVisaEmployerAddress']['postcode']!='')
                  {
                  echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['ReEntryVisaApplication']['ReEntryVisaEmployerAddress']['postcode']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }
                  ?>
                      </span> </td>
                  </tr>
            </table></td>
        </tr>
                     <tr class="noPrint">
          <td valign="top" height="5"></td>
          <td valign="top" height="5"></td>
        </tr>
        <tr>
          <td height="20" valign="middle" id="form_new_heading">Reference - Refree</td>
          <td height="20" valign="middle" id="form_new_heading">CERPAC Information (Where Applicable)</td>
        </tr>
           <tr class="noPrint">
          <td valign="top" height="5"></td>
          <td valign="top" height="5"></td>
        </tr>
        <tr>
          <td valign="top"><table width="347" border="0" cellspacing="0" cellpadding="0" class="no_border_new">
              <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_Reference0_name_of_refree">Name of Referee</label>
                    </td>
                    <td width="175" valign="top"><span class="lblField">
                      <?php
                  if($referenceDetails[0]['name_of_refree']!='')
                  {
                      echo "<input class='print_input_box' type='text' readonly=readonly value='".$referenceDetails[0]['name_of_refree']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }
                  ?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_ReEntryVisaReferenceAddress0_address_1">Address</label>
                    </td>
                    <td valign="top"><span class="lblField">
                      <?php
                  if($referenceDetails[0]['ReEntryVisaReferencesAddress']['address_1']!='')
                  {
                  echo "<input class='print_input_box' type='text' readonly=readonly value='".$referenceDetails[0]['ReEntryVisaReferencesAddress']['address_1']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }
                  ?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_Reference0_phone_of_refree">Phone No.</label>
                    </td>
                    <td valign="top"><span class="lblField">
                      <?php
                  if($referenceDetails[0]['phone_of_refree']!='')
                  {
                  echo "<input class='print_input_box' type='text' readonly=readonly value='".$referenceDetails[0]['phone_of_refree']."'>";
                  }
                  else{
                  echo "<input type='text' class='print_input_box' value='--' readonly=readonly>" ;
                  }
                  ?>
                      </span> </td>
                  </tr>
            </table></td>
          <td valign="top"><table width="347" border="0" cellspacing="0" cellpadding="0" class="no_border_new">
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_ReEntryApplicantInfo_cerpa_quota">CERPAC Number</label>
                </td>
                    <td width="175" valign="top"><span class="lblField">
                      <?php
                  if($visa_application[0]['ReEntryVisaApplication']['cerpa_quota']!='')
                  {
                      echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['ReEntryVisaApplication']['cerpa_quota']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }
                  ?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_ReEntryApplicantInfo_cerpac_issuing_state">Issuing Code</label>
                      <span class="redLbl"></span> </td>
                    <td valign="top"><span class="lblField" >
                      <?php
                  if($visa_application[0]['ReEntryVisaApplication']['cerpac_issuing_state']!='')
                  {
                  echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['ReEntryVisaApplication']['cerpac_issuing_state']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }
                  ?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_ReEntryApplicantInfo_cerpac_date_of_issue">Date of issue (dd-mm-yyyy)</label>
                    </td>
                <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php
                if($visa_application[0]['ReEntryVisaApplication']['cerpac_date_of_issue']!='')
                {

                  $explode = explode('-',getDateFormate($visa_application[0]['ReEntryVisaApplication']['cerpac_date_of_issue']));
                  echo "<input type='text' class='printdate' readonly=readonly value='".$explode[0]."'>";
                  echo "<input type='text' class='printdate' readonly=readonly value='".$explode[1]."'>";
                  echo "<input type='text' class='printyear' readonly=readonly value='".$explode[2]."'>";
                }
                else
                {
                   echo "<input type='text' class='printdate' readonly=readonly value='--'>";
                   echo "<input type='text' class='printdate' readonly=readonly value='--'>";
                   echo "<input type='text' class='printyear' readonly=readonly value='--'>";
                }
                ?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_ReEntryApplicantInfo_cerpac_exp_date">Expiry date (dd-mm-yyyy)</label>
                      <span class="redLbl"></span></td>
                <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php
                  if($visa_application[0]['ReEntryVisaApplication']['cerpac_exp_date']!='')
                      {
                        $explode = explode('-',getDateFormate($visa_application[0]['ReEntryVisaApplication']['cerpac_exp_date']));
                        echo "<input type='text' class='printdate' readonly=readonly value='".$explode[0]."'>";
                        echo "<input type='text' class='printdate' readonly=readonly value='".$explode[1]."'>";
                        echo "<input type='text' class='printyear' readonly=readonly value='".$explode[2]."'>";

                      }
                      else
                      {
                    echo "<input type='text' class='printdate' readonly=readonly value='--'>";
                    echo "<input type='text' class='printdate' readonly=readonly value='--'>";
                    echo "<input type='text' class='printyear' readonly=readonly value='--'>";
                      }
                      ?>
                      </span> </td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_ReEntryApplicantInfo_issuing_cerpac_office">Issuing CERPAC Office</label>
                      <span class="redLbl"></span> </td>
                    <td valign="top"><span class="lblField">
                      <?php
                  if($visa_application[0]['ReEntryVisaApplication']['issuing_cerpac_office']!='')
                  {
                  echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['ReEntryVisaApplication']['issuing_cerpac_office']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }
                  ?>
                      </span> </td>
                  </tr>
            </table></td>
        </tr>
                    <tr class="noPrint">
          <td valign="top" height="5"></td>
          <td valign="top" height="5"></td>
        </tr>
        <tr>
          <td height="20" valign="middle" id="form_new_heading">Visa Processing Information</td>
          <td valign="top"></td>
        </tr>
          <tr class="noPrint">
          <td valign="top" height="5"></td>
          <td valign="top" height="5"></td>
        </tr>
        <tr>
          <td valign="top"><table width="345" border="0" cellspacing="0" cellpadding="0" class="no_border_new">
              <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_ReEntryApplicantInfo_processing_centre_id">Free Zone Processing Centre</label>
                    </td>
                    <td valign="top"><span class="lblField">
                      <?php
                  if($visa_application[0]['ReEntryVisaApplication']['VisaProcessingCentre']['centre_name']!='')
                  {
                  echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['ReEntryVisaApplication']['VisaProcessingCentre']['centre_name']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }

                  ?>
                      </span> </td>
                  </tr>
            </table></td>
          <td valign="top"></td>
        </tr>
                </table>
        <div class="clear noPrint"></div>
          <div class="clear noPrint">&nbsp;</div>
    <div class="border_top noPrint"></div>
        <div class="pixbr_new">
          <div class="Y20_new">
            <div class="l_new">Date: _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _</div>
            <div class="r_new">Signature / Thumb Impression: _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _</div>
            <div class="pixbr_new"></div>
          </div>
        <p class="printtxt">I will be required to comply with the Immigration and other laws governing entry of the immigrants into the country to which I now apply for Visa</p>
        <p class="top-padding printtxt">PLEASE NOTE: PRINT AND SIGN THIS APPLICATION AND TAKE IT WITH TWO (2) PASSPORT PHOTOGRAPHS TO THE SELECTED PASSPORT OFFICE FOR FURTHER PROCESSING.</p>
        </div>
      </div>
      <?php if(!$mode){?>
    <div align="center">
        <center id="multiFormNav">
          <table align="center" class="nobrd">
            <tr>
            <td><div align="center" style="display:block" id="btn_pnc">
                  <?php if($visa_application[0]['status'] == 'New'){ ?>
                <input name="button" type="button" class="normalbutton noPrint" onclick="showHideButtons('<?php echo $printUrl; ?>');"  value="Print & Continue" />
                  <?php } else { ?>
                <input type="button" class="normalbutton noPrint"  value="Print" onclick="printApplication('<?php echo $printUrl; ?>');">
                  <?php } ?>
                </div>
              <input type="button"  disabled style="display:none" class="normalbutton noPrint"  id="multiFormSubmit" value="Continue" onclick= "continueProcess();"/>
              </td>
            </tr>
          </table>
        </center>
      </div>
      <?php }else{?>
    <div align="center">
        <center id="multiFormNav">
          <table align="center" class="nobrd">
            <tr>
            <td><input type="button" class="normalbutton"  value="Print" onclick="printApplication('<?php echo $printUrl; ?>');">
              </td>
            <td><input type="button" class="normalbutton"  value="Close" onclick="window.close();">
              </td>
            </tr>
          </table>
        </center>
      </div>
      <?php  } ?>
	  <div class="print_note" align="center"><em>Please use 0.5 inch/ 12.7mm margin (on all sides of page) for best printing</em></div>
      <div class="warningArea">
        <h2>Warning</h2>
        <ul>
          <li>PLEASE ENSURE THAT ALL INFORMATION PROVIDED ON YOUR APPLICATION IS CORRECT BEFORE YOU PROCEED TO PAYMENT. YOUR APPLICATION CANNOT BE
            EDITED ONCE PAYMENT IS INITIATED. </li>
        </ul>
      </div>
    </div>
  </div>
<div class="content_wrapper_bottom"></div>
<script>
  function showHideButtons(url){
    $('#multiFormSubmit').show();
    $('#btn_pnc').hide();
    //window.print();
    printApplication(url)
    $('#multiFormSubmit').removeAttr('disabled');
  }


  function printApplication(url){
      openPopUp(url);
  }

  function openPopUp(url){
     window.open (url, "_blank","status=1,toolbar=0,width=1000px,height=800px,location=0,menubar=0,scrollbars=1");
  }

  function continueProcess(){

    window.location ="<?php echo url_for('visa/showReEntry?chk='.$chk.'&p='.$p.'&id='.$idParam)?>";
  }
  document.title = 'The Nigeria Immigration Service';
</script>
