<!-- Display popup message or not -->
<?php //if($sf_request->getParameter('p') !='i') {?>
<?php //echo ePortal_popup("Please Keep This Safe","<p><b>You will need it later</b></p><h5>Application Id: ".$visa_application[0]['id']."</h5><h5> Reference No: ".$visa_application[0]['ref_no']."</h5>");
?>
<?php //if(isset($chk)){ echo "<script>pop();</script>"; } ?>
<?php //} ?>
<?php $isGratis = false;
      if($visa_application[0]['ispaid'] == 1){
        if(($visa_application[0]['paid_naira_amount'] == 0) && ($visa_application[0]['paid_dollar_amount'] == 0)){
           $isGratis = true;
          }else{
            $isGratis = false;
           }
          }?>
<?php
$sf = sfContext::getInstance()->getUser();
if($sf->hasFlash('notice')){ ?>

<div id="flash_notice" class="alertBox" >
<?php
    echo nl2br($sf->getFlash('notice'));
?>
<br/>
</div>
<?php }?>
<?php
$sf = sfContext::getInstance()->getUser();
if($sf->hasFlash('error')){ ?>
<div id="flash_error" class="alertBox" >
<?php
    echo nl2br($sf->getFlash('error'));
?>
<br/>
</div>
<?php }?>
<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">
    <?php include_partial('global/innerHeading',array('heading'=>"Applicant's Details"));?>
    <form action="<?php echo url_for('cart/list') ?>" method="POST" class='dlForm multiForm'>
      <div>
        <center>
          <?php
      if($statusType == 1){
        if((isset($countFailedAttempt) && $countFailedAttempt > 0) && ($visa_application[0]['ispaid'] != 1)){
      
          echo ePortal_highlight("<b>You made $countFailedAttempt payment attempts.None of the payment attempt(s) returned a message.<br /> Please attempt(s) Payment ONLY If You Are Sure The Previous attempt(s) (IF ANY) Was Not Successful.</b>",'',array('class'=>'black'));
        }else{
          echo ($visa_application[0]['ispaid'] == 1)?"":ePortal_highlight("<b>No Previous Payment Attempt History Found</b>",'',array('class'=>'black'));}
      }
      ?>
        </center>
        <div  id="Reciept" >
          <div class="clear"></div>
          <div class="tmz-spacer"></div>
          <?php if($show_details==1){ ?>
          <table cellspacing="0" cellpadding="0" border="0" width="99%" class="show_table">
            <tbody>
              <tr>
                <td colspan="2" class="blbar"><strong><?php echo ePortal_legend("Profile"); ?></strong></td>
              </tr>
              <tr>
                <td align="left" valign="top"><label>Full Name:</label></td>
                <td><?php echo ePortal_displayName($visa_application[0]['title'],$visa_application[0]['other_name'],$visa_application[0]['middle_name'],$visa_application[0]['surname']);?></td>
              </tr>
              <tr>
                <td align="left" valign="top"><label>Gender:</label></td>
                <td><?php echo $visa_application[0]['gender'];?></td>
              </tr>
              <tr>
                <td align="left" valign="top"><label>Date of Birth:</label></td>
                <td><?php $datetime = date_create($visa_application[0]['date_of_birth']); echo date_format($datetime, 'd/F/Y'); ?></td>
              </tr>
              <tr>
                <td align="left" valign="top"><label>Email:</label></td>
                <td><?php if($visa_application[0]['email']!='') echo $visa_application[0]['email']; else echo "--"; ?></td>
              </tr>
              <tr>
                <td align="left" valign="top"><label>Nationality:</label></td>
                <td><?php echo $visa_application[0]['CurrentCountry']['country_name']; ?></td>
              </tr>
              <tr>
                <td align="left" valign="top"><label>Profession:</label></td>
                <td><?php echo $visa_application[0]['ReEntryVisaApplication']['profession']; ?></td>
              </tr>
              <?php } ?>
              <tr>
                <td colspan="2" class="blbar"><strong><?php echo ePortal_legend("Application Information"); ?></strong></td>
              </tr>
              <?php
    if($visa_application[0]['VisaCategory']['var_value']=='Re-Entry Freezone'){
    $app_cat='Re-Entry Free Zone';

    $VisaTypeId=Doctrine::getTable('ReEntryVisaApplication')->getVisaTypeId($visa_application[0]['id']);
        $FreezoneSingleVisaId=Doctrine::getTable('FreezoneSingleVisaType')->getFreeZoneVisaTypeId();
        $FreezoneMultipleVisaId=Doctrine::getTable('FreezoneMultipleVisaType')->getFreeZoneVisaTypeId();
        if($VisaTypeId==$FreezoneSingleVisaId){
         $VisaType='Single Re-entry';
        }else if($VisaTypeId==$FreezoneMultipleVisaId){
         $VisaType='Multiple Re-entry';
        }


    }else{
    $app_cat=$visa_application[0]['VisaCategory']['var_value'];
    }
    ?>
              <?php if($show_details==1){ ?>
              <tr>
                <td align="left" valign="top"><label>Category:</label></td>
                <td><?php echo $app_cat."&nbsp;Visa/Permit&nbsp;";?></td>
              </tr>
              <tr>
                <td align="left" valign="top"><label>Type:</label></td>
                <td><?php  if($visa_application[0]['VisaZoneType']['var_value']!="Free Zone"){echo $visa_application[0]['ReEntryVisaApplication']['VisaType']['var_value']."&nbsp;Visa";}else{echo $VisaType;} ?></td>
              </tr>
              <tr>
                <td align="left" valign="top"><label>Request:</label></td>
                <td><?php echo $app_cat."&nbsp;Visa"; if(isset($visa_application[0]['ReEntryVisaApplication']['no_of_re_entry_type'])&&$visa_application[0]['ReEntryVisaApplication']['no_of_re_entry_type']!=""){echo"-&nbsp;[&nbsp;".$visa_application[0]['ReEntryVisaApplication']['no_of_re_entry_type']."&nbsp;Multiple Re Entries ]"; }?></td>
              </tr>
              <tr>
                <td align="left" valign="top"><label>Date:</label></td>
                <td><?php $datetime = date_create($visa_application[0]['created_at']); echo date_format($datetime, 'd/F/Y'); ?>
                  <?php } ?></td>
              </tr>
              <tr>
                <td align="left" valign="top"><label>Application Id:</label></td>
                <td><?php echo $visa_application[0]['id'];?></td>
              </tr>
              <tr>
                <td align="left" valign="top"><label>Reference No:</label></td>
                <td><?php echo $visa_application[0]['ref_no']; ?></td>
              </tr>
              <tr>
                <td colspan="2" class="blbar"><strong><?php echo ePortal_legend("Processing Information"); ?></strong>
                  <?php if($show_details==1){ ?></td>
              </tr>
              <tr>
                <td align="left" valign="top"><label>Country:</label></td>
                <td>Nigeria</td>
              </tr>
              <?php if($visa_application[0]['VisaZoneType']['var_value']=="Free Zone"){?>
              <tr>
                <td align="left" valign="top"><label>Processing Centre:</label></td>
                <td><?php echo Doctrine::getTable('VisaProcessingCentre')->getProcessingCentre($visa_application[0]['ReEntryVisaApplication']['processing_centre_id']);?></td>
              </tr>
              <?php }else{ ?>
              <tr>
                <td align="left" valign="top"><label>Visa Processing State:</label></td>
                <td><?php if($visa_application[0]['VisaZoneType']['var_value']!="Free Zone"){echo $visa_application[0]['ReEntryVisaApplication']['VisaProceesingState']['state_name'];}else{echo "Not Applicable";}?></td>
              </tr>
              <tr>
                <td align="left" valign="top"><label>Embassy:</label></td>
                <td>Not Applicable</td>
              </tr>
              <tr>
                <td align="left" valign="top"><label>Office:</label></td>
                <td><?php if($visa_application[0]['VisaZoneType']['var_value']!="Free Zone"){echo $visa_application[0]['ReEntryVisaApplication']['VisaOffice']['office_name'];}else{echo "Not Applicable";} ?></td>
              </tr>
              <?php } ?>
              <?php } ?>
              <?php if($show_payment==1){ ?>
              <tr>
                <td align="left" valign="top"><label>Interview Date:</label></td>
                <td><?php if($visa_application[0]['ispaid'] == 1)
          {
            if(($visa_application[0]['paid_naira_amount'] == 0) && ($visa_application[0]['paid_dollar_amount'] == 0))
            {
              echo "Check the Passport Office / Embassy / High Commission";
            }else
            {
              $datetime = date_create($visa_application[0]['interview_date']); echo date_format($datetime, 'd/F/Y');
            }
          }else
          {
            echo "Available after Payment";
          } ?></td>
              </tr>
              <?php } ?>
              <?php
    $isPaid = $visa_application[0]['ispaid'];
    if($show_payment==1 || $isPaid==0){ ?>
              <tr>
                <td colspan="2" class="blbar"><strong><?php echo ePortal_legend("Payment Information"); ?></strong></td>
              </tr>
              <?php
          if($visa_application[0]['VisaZoneType']['var_value']!='Free Zone' ){
            if($visa_application[0]['ispaid'] == 1  )
            {
              if($visa_application[0]['paid_naira_amount'] != 0.00){
                echo "<tr><td><label>Naira Amount:</label></td><td>NGN&nbsp;".$visa_application[0]['paid_naira_amount']."</td></tr>";
              }
//              else{
//                echo "Not Applicable";
//              }
            }else
            {
              if(isset($payment_details['naira_amount'])){echo "<tr><td><label>Naira Amount:</label></dt><dd>NGN&nbsp;".$payment_details['naira_amount']."</td></tr>";}else{echo "NGN&nbsp;"."0</td></tr>";}
            }
          }
//          else echo "Not Applicable";
          ?>
              <!-- Display Dollar or neira amount -->
              <?php
          if($visa_application[0]['VisaZoneType']['var_value']=='Free Zone' ){
            if($visa_application[0]['ispaid'] == 1)
            {
              if($visa_application[0]['paid_dollar_amount'] != 0.00){
                echo "<tr><td><label>Dollar Amount:</label></td><td>USD&nbsp;".$visa_application[0]['paid_dollar_amount']."</td> </tr>";
              }
            }else
            {
              if(isset($payment_details['dollar_amount'])){ echo "<tr><td><label>Dollar Amount:</label></td><td>USD&nbsp;".$payment_details['dollar_amount']."</td> </tr>";}else{ echo "<tr><td><label>Dollar Amount:</label></td><td>USD&nbsp;"."0"."</td> </tr>";}
            }
          }
          ?>
              <tr>
                <td align="left" valign="top"><label>Payment Status:</label></td>
                <td><?php
          $isGratis = false;
          if($visa_application[0]['ispaid'] == 1)
          {
            if(($visa_application[0]['paid_naira_amount'] == 0) && ($visa_application[0]['paid_dollar_amount'] == 0))
            {
              echo "This Application is Gratis (Requires No Payment)";
              $isGratis = true;
            }else
            {
              echo "Payment Done";
            }
          }else{
            echo "Available after Payment";
          }
          ?></td>
              </tr>
              <tr>
                <td align="left" valign="top"><label>Payment Currency</label></td>
                <td><?php
          if($visa_application[0]['ispaid'] == 1)
          {
            if(($visa_application[0]['paid_naira_amount'] == 0) && ($visa_application[0]['paid_dollar_amount'] == 0))
            {
              echo "Not Applicable";
            }else
            {
              echo $paymentCurrency;
            }
          }else{
            echo "Available after Payment";
          }
          ?></td>
              </tr>
              <tr>
                <td align="left" valign="top"><label>Payment Gateway:</label></td>
                <td><?php
          if($visa_application[0]['ispaid'] == 1)
          {
            if(($visa_application[0]['paid_naira_amount'] == 0.00) && ($visa_application[0]['paid_dollar_amount'] == 0.00))
            {
              echo "Not Applicable";
            }else
            {
              echo $PaymentGatewayType;
            }
          }else{
            echo "Available after Payment";
          }
          ?></td>
              </tr>
              <tr>
                <td align="left" valign="top"><label>Amount Paid:</label></td>
                <td><?php
          if($visa_application[0]['ispaid'] == 1)
          {
            if(($visa_application[0]['paid_naira_amount'] == 0.00) && ($visa_application[0]['paid_dollar_amount'] == 0.00))
            {
              echo "Not Applicable";
            }else
            {
              echo $currencySign."&nbsp;".$paidAmount;
            }
          }else
          {
            echo "Available after Payment";
          }
          ?>
                  <?php } ?>
                  <?php if($visa_application[0]['status'] != 'New' && $visa_application[0]['status'] != 'Paid'){?></td>
              </tr>
              <tr>
                <td colspan="2" class="blbar"><?php echo ePortal_legend("Application Status"); ?></td>
              </tr>
              <tr>
                <td align="left" valign="top"><label>Current Application Status:</label></td>
                <td><?php echo $visa_application[0]['status'] ;?></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
            </tbody>
          </table>
          <?php }?>
          <?php
    if($isPaid == 1)
    {
      ?>
          <?php
      if($show_payment==1){
      echo ePortal_highlight('YOUR PAYMENT PROCESS WAS SUCCESSFULL! PLEASE PRINT YOUR ACKNOWLEDGMENT & RECEIPT SLIP.','',array('class'=>'green'));
      }?>
          <div class="clear"></div>
          <div class="pixbr XY20">
            <center id="multiFormNav">
              <table align="center" class="nobrd">
                <tr>
                  <td><!-- <input type="button" value="Print Acknowledgment Slip" onclick="javascript:window.open('<?php //echo url_for('visa/visaReEntryAcknowledgmentSlip?id='.$encriptedAppId) ?>','MyPage','width=750,height=700,scrollbars=1');"> -->
                    <?php if(($paidAmount != 0) || $isGratis ){
            if(($show_details==1) && sfConfig::get('app_enable_pay4me_validation')==1) {  ?>
                    <?php if($visa_application[0]['VisaZoneType']['var_value']=="Free Zone"){ $AppTypes='3';}else{$AppTypes='1';}?>
                    <input type="hidden" name="AppTypes"  value="<?php echo SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT($AppTypes)); ?>"/>
                    <input type="hidden" name="AppId"  value="<?php echo SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT($visa_application[0]['id'])); ?>"/>
                    <input type="hidden" name="RefId"  value="<?php echo SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT($visa_application[0]['ref_no'])); ?>"/>
                    <?php if(!$isGratis) { ?>
                    <input type="hidden" name="app_gratis"  value="<?php echo SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT(1)); ?>"/>
                    <?php }else{ ?>
                    <input type="hidden" name="app_gratis"  value="<?php echo SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT(2)); ?>"/>
                    <?php } ?>
                    <?php if($data['pay4me'] && !$data['validation_number']){?>
                    <input type="hidden" name="ErrorPage"  value="<?php echo SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT(1)); ?>"/>
                    <input type="hidden" name="GatewayType"  value="<?php echo SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT(1)); ?>"/>
                    <input type="hidden" name="id"  value="<?php echo SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT($visa_application[0]['id'])); ?>"/>
                    <input type="submit" class="normalbutton"  id="multiFormSubmit"  value='View your application payment status' onClick="this.form.action='<?php echo secure_url_for('visa/showReEntry'); ?>';" />
                    <?php } else{?>
                    <!-- <div class="lblButton">-->
                    <input type="submit" class="normalbutton"  id="multiFormSubmit"  value='View your application payment status' onClick="this.form.action='<?php echo secure_url_for('visa/OnlineQueryStatus'); ?>';" />
                    <?php }?>
                    <?php } ?>
                    <?php if($show_payment==1){ ?>
                    <?php if(!$isGratis) {?>
                    <!-- <input type="button" class="button"  value="Print Receipt" onclick="javascript:window.open('<?php echo url_for('visa/PrintRecipt?visa_app_id='.$encriptedAppId.'&visa_app_refId='.$encriptedRefId) ?>','MyPage','width=750,height=700,scrollbars=1,location=no');">-->
                    &nbsp;&nbsp;
                    <?php }?>
                    <!-- <input type="button" class="button"  value="Print Acknowledgment Slip" onclick="javascript:window.open('<?php echo url_for('visa/visaReEntryAcknowledgmentSlip?id='.$encriptedAppId) ?>','MyPage','width=750,height=700,scrollbars=1');">-->
                    &nbsp;&nbsp;
                    <?php }} ?> <?php }else { ?></td>
                </tr>
              </table>
              
            </center>
          </div>
         
          <div class="clear">&nbsp;</div>
          <div class="pixbr XY20">
            <center id="multiFormNav">
              <table align="center" class="nobrd noPrint">
                <!--<td><input type="button" class="normalbutton" id="multiPrintw" value='Print' onclick="javascript:window.print()" ></td>-->
                  <td><!-- <input type="button" value="Print Acknowledgment Slip" onclick="javascript:window.open('<?php// echo url_for('visa/visaReEntryAcknowledgmentSlip?id='.$encriptedAppId) ?>','MyPage','width=750,height=700,scrollbars=1');">&nbsp;&nbsp;-->
                    <?php if($IsProceed){?>
                    <input type="submit" class="normalbutton"  id="multiFormSubmit" value='Proceed To Checkout'>
                    <?php }?></td>
                </tr>
              </table>
            </center>
          </div>
        </div>
        <?php if(!$IsProceed) echo ePortal_highlight("You have attempted a payment in last ".sfConfig::get("app_time_interval_payment_attempt")." minutes!! Please wait for ".sfConfig::get("app_time_interval_payment_attempt")." minutes.",'',array('class'=>'red')); ?>
        <?php // echo ePortal_highlight('PLEASE CONFIRM YOUR ORDER BEFORE PROCEEDING TO PAYMENTS. NIS WILL NOT REFUND APPLICANT FOR A WRONG ORDER / PAYMENT','WARNING',array('class'=>'yellow'));?>
        <?php if($show_details==1){ echo ePortal_highlight('PLEASE ENSURE THAT ALL INFORMATION PROVIDED ON YOUR APPLICATION IS CORRECT BEFORE YOU PROCEED TO PAYMENT.  YOUR APPLICATION CANNOT BE EDITED ONCE PAYMENT IS INITIATED.<br/>','WARNING',array('class'=>'yellow')); } ?>
        <?php
  }
  ?>
        <input type ="hidden" value="<?php echo $getAppIdToPaymentProcess;?>" name="appDetails" id="appDetails"/>
      </div>
    </form>

</div>
<div class="content_wrapper_bottom"></div>
