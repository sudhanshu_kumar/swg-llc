<script>
  function validateForm()
   {
     if(document.getElementById('zone_type').value=='')
       {
         alert('Please select Zone type.');
         document.getElementById('zone_type').focus();
         return false;
       }
   }
  </script>

<h1>Apply Re-Entry Visa Application</h1>
<div class="XY20">
  <form name='passportEditForm' action='<?php echo url_for('visa/reentryvisa');?>' method='post' class='dlForm'>
  <fieldset class="bdr">
  <?php echo ePortal_legend("Select Zone Type"); ?>
    <dl>
        <dt><label >Re-Entry Zone Type:</label ></dt>
        <dd>
          <select name="zone_type" id='zone_type'>
            <option value="">-- Please Select --</option>
            <?php foreach($zoneTypeArray as $data){?>
              <option value="<?php echo $data['id'];?>"><?php echo $data['var_value'];?></option>
            <?php } ?>
          </select><br />&nbsp;
          </dd>
    </dl>
  </fieldset>
  <div class="XY20"><center><input type='submit' value='Start Application' onclick='return validateForm();'></center></div>
  </form>
</div>