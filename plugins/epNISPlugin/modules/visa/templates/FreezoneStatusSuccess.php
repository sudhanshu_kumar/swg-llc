<?php use_helper('Form') ?>
<script>
  function validateForm()
  {
    if(document.getElementById('visa_app_id').value == '')
    {
      alert('Please insert Free Zone Application Id.');
      document.getElementById('visa_app_id').focus();
      return false;
    }
  if(document.getElementById('visa_app_id').value != "")
    {
      if(isNaN(document.getElementById('visa_app_id').value))
        {
          alert('Please insert only numeric value.');
          document.getElementById('visa_app_id').value = "";
          document.getElementById('visa_app_id').focus();
          return false;
        }

    }

    if(document.getElementById('visa_app_refId').value == '')
    {
      alert('Please insert Free Zone Application Ref No.');
      document.getElementById('visa_app_refId').focus();
      return false;
    }
  if(document.getElementById('visa_app_refId').value != "")
    {
      if(isNaN(document.getElementById('visa_app_refId').value))
        {
          alert('Please insert only numeric value.');
          document.getElementById('visa_app_refId').value = "";
          document.getElementById('visa_app_refId').focus();
          return false;
        }

    }

  }
</script>

<h1>Check Free Zone Status</h1>
<div class="multiForm dlForm">
  <form name='visaEditForm' action='<?php echo url_for('visa/FreezoneStatusReport');?>' method='post' class="dlForm">
    <div align="center"><font color='red'><?php if(isset($errMsg)) echo $errMsg;?></font></div>
    <fieldset class="bdr">
      <?php echo ePortal_legend("Search for Application"); ?>
      <dl>
        <dt><label>Free Zone Application Id<sup>*</sup>:</label></dt>
      <dd><?php
      $app_id = (isset($_POST['visa_app_id']))?$_POST['visa_app_id']:"";
      echo input_tag('visa_app_id', $app_id, array('size' => 20, 'maxlength' => 20,'autocomplete'=>'off')); ?>
      </dd>
      </dl>
      <dl>
        <dt><label>Free Zone Reference No<sup>*</sup>:</label></dt>
      <dd><?php
      $reff_id = (isset($_POST['visa_app_refId']))?$_POST['visa_app_refId']:"";
      echo input_tag('visa_app_refId', $reff_id, array('size' => 20, 'maxlength' => 20,'autocomplete'=>'off')); ?>
      </dd>
      </dl>
    </fieldset>
    <div class="pixbr XY20">
      <center id="multiFormNav"><input type='submit' id="multiFormSubmit" value='Search Record' onclick='return validateForm();'>
      &nbsp;<!--<input type='reset' value='Cancel'>-->
      </center>
    </div>
  </form>
</div>
