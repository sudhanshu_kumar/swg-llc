<style>
   .calBtn, .text_hide, .top_subbarwrap1, .footer_wrapbg, .flRight {display:none;}
</style>
<link rel="stylesheet" type="text/css" media="print" href="<?php  echo stylesheet_path('print');?>" />
<div class="page_heading" style="color:#000000; padding:0 0 0 25px;">Entry Visa/Free Zone Application</div>
<div class="inst_container1">
  <div class="inst_top"></div>
  <div class="inst_bg">
    <div class="inst_container1">
      <ul>
        <div class="clear"></div>
        <div class="page_heading_visa">Entry Visa/Free Zone Application</div>
        <div class="clear"></div>
        <li>FOR ANY QUESTIONS AND/OR CONCERNS PLEASE CONTACT US AT <a href="mailto:support@swgloballlc.com" class="mail_link">support@swgloballlc.com</a></li>
        <li>APPLICATION FEES PAID FOR VISA/FREEZONE ARE NON REFUNDABLE EXCEPT IN CASED OF DOUBLE PAYMENT MISTAKENLY MADE FOR THE SAME APPLICATION OR FRAUDULENT USE OF CREDIT/DEBIT CARD. TO SEEK A REFUND PLEASE CONTACT <a href="mailto:refund@swgloballlc.com" class="mail_link">refund@swgloballlc.com</a></li>
        <li>PAYMENTS ARE VALID FOR SIX (6) MONTHS ONLY FROM THE PAYMENT DATE.</li>
        <li>ONLY online payment is acceptable.</li>
        <li>Anyone who pays otherwise and receives service is subject to prosecution and revocation of Visa.</li>
        <li>If you have already completed an application, please check your application status rather than completing a duplicate application</li>
        <li>Expatriate Quota is not a requirement for Free Zone Entry Visa</li>
      </ul>
    </div>
  </div>
  <div class="inst_bottom"></div>  
  <form name="visa_form" id="visa_form" action="#" method="post"  id="multiForm" class="dlForm" onsubmit="updateValue()">
    <div id="print_content" > </div>
  </form>
</div>

<script> 
  <?php if($sf_params->get("type") == "v")
  { ?>   
    $("#print_content").append(window.opener.document.getElementById("uiGroup_Step_1_of_4").innerHTML);
    $("#print_content").append(window.opener.document.getElementById("uiGroup_Step_2_of_4").innerHTML);
    $("#print_content").append(window.opener.document.getElementById("uiGroup_Step_3_of_4").innerHTML);
    $("#print_content").append(window.opener.document.getElementById("uiGroup_Step_4_of_4").innerHTML);
  <?php }else{ ?>
   
    $("#print_content").append(window.opener.document.getElementById("uiGroup_").innerHTML);
     
    

  <?php  } ?>
  
</script>
<?php if($sf_params->get("type") == "r"){ ?>
<div class="pixbr X50">
  <p> I will be required to comply with the Immigration and other laws governing entry of the immigrants into the country to which I now apply for Visa </p>
  <p> <b> Please take printed, and signed form together with evidence of payment to the Nigerian Embassy / High Commission of your residence for further processing.</b></p>
  <br>
  <p> <font color="red">*</font> - Compulsory fields</p>
</div>
<?php   } ?>
<div class="pixbr XY20" align="center">
  <center id="multiFormNav">
    <input type="button" id="closepage" class="normalbutton noPrint" value="close" onclick="javaScript:window.close()" />
  </center>
</div>
<br />
<br />
<script>
    var currentFormElement = document.getElementById("visa_form").elements;
    var parentObj = window.opener.document.getElementById("visa_form");
    if(parentObj == null){ //this condition will work if parent window form do not exist...
        window.close();        
    }else{ //this condition will work if parent window form exist...
        var formElement = parentObj.elements;
        var formElementLength = formElement.length;
        //formElementLength = 100;
        //alert(formElementLength);
        for(i=0;i<formElementLength;i++){
            //alert(formElement[i].type +' == '+ formElement[i].value)
            switch(formElement[i].type){
                case 'radio':
                    if(formElement[i].checked == true){
                        currentFormElement[i].checked = true;
                    }
                    break;
                case 'checkbox':
                    if(formElement[i].checked == true){
                        currentFormElement[i].checked = true;
                    }
                    break;
                case 'text':
                case 'textarea':
                case 'select-one':
                    currentFormElement[i].value = formElement[i].value;
                    break;
                default:
                    currentFormElement[i].value = formElement[i].value;
                    break;
            }//End of switch(formElement[i].type){...
        }//End of for(i=0;i<formElementLength;i++){...
        if($('#visa_application_date_of_birth_day').length > 0){
          $('#visa_application_date_of_birth_day').addClass('small_dropbox');
          $('#visa_application_date_of_birth_month').addClass('small_dropbox');
          $('#visa_application_date_of_birth_year').addClass('small_dropbox');

          $('#visa_application_military_dt_from_day').addClass('small_dropbox');
          $('#visa_application_military_dt_from_month').addClass('small_dropbox');
          $('#visa_application_military_dt_from_year').addClass('small_dropbox');
          
          $('#visa_application_military_dt_to_day').addClass('small_dropbox');
          $('#visa_application_military_dt_to_month').addClass('small_dropbox');
          $('#visa_application_military_dt_to_year').addClass('small_dropbox');

          $('#visa_application_ApplicantInfo_date_of_issue_day').addClass('small_dropbox');
          $('#visa_application_ApplicantInfo_date_of_issue_month').addClass('small_dropbox');
          $('#visa_application_ApplicantInfo_date_of_issue_year').addClass('small_dropbox');
          
          $('#visa_application_ApplicantInfo_date_of_exp_day').addClass('small_dropbox');
          $('#visa_application_ApplicantInfo_date_of_exp_month').addClass('small_dropbox');
          $('#visa_application_ApplicantInfo_date_of_exp_year').addClass('small_dropbox');

          $('#visa_application_ApplicantInfo_proposed_date_of_travel_day').addClass('small_dropbox');
          $('#visa_application_ApplicantInfo_proposed_date_of_travel_month').addClass('small_dropbox');
          $('#visa_application_ApplicantInfo_proposed_date_of_travel_year').addClass('small_dropbox');

          $('#visa_application_PreviousHistory0_startdate_day').addClass('small_dropbox');
          $('#visa_application_PreviousHistory0_startdate_month').addClass('small_dropbox');
          $('#visa_application_PreviousHistory0_startdate_year').addClass('small_dropbox');
          
          $('#visa_application_PreviousHistory0_endate_day').addClass('small_dropbox');
          $('#visa_application_PreviousHistory0_endate_month').addClass('small_dropbox');
          $('#visa_application_PreviousHistory0_endate_year').addClass('small_dropbox');

          $('#visa_application_PreviousHistory1_startdate_day').addClass('small_dropbox');
          $('#visa_application_PreviousHistory1_startdate_month').addClass('small_dropbox');
          $('#visa_application_PreviousHistory1_startdate_year').addClass('small_dropbox');

          $('#visa_application_PreviousHistory1_endate_day').addClass('small_dropbox');
          $('#visa_application_PreviousHistory1_endate_month').addClass('small_dropbox');
          $('#visa_application_PreviousHistory1_endate_year').addClass('small_dropbox');

          $('#visa_application_PreviousHistory2_startdate_day').addClass('small_dropbox');
          $('#visa_application_PreviousHistory2_startdate_month').addClass('small_dropbox');
          $('#visa_application_PreviousHistory2_startdate_year').addClass('small_dropbox');
          
          $('#visa_application_PreviousHistory2_endate_day').addClass('small_dropbox');
          $('#visa_application_PreviousHistory2_endate_month').addClass('small_dropbox');
          $('#visa_application_PreviousHistory2_endate_year').addClass('small_dropbox');
          
          $('#visa_application_FiveYears0_date_of_departure_day').addClass('small_dropbox');
          $('#visa_application_FiveYears0_date_of_departure_month').addClass('small_dropbox');
          $('#visa_application_FiveYears0_date_of_departure_year').addClass('small_dropbox');
          
          $('#visa_application_FiveYears1_date_of_departure_day').addClass('small_dropbox');
          $('#visa_application_FiveYears1_date_of_departure_month').addClass('small_dropbox');
          $('#visa_application_FiveYears1_date_of_departure_year').addClass('small_dropbox');
          
          $('#visa_application_FiveYears2_date_of_departure_day').addClass('small_dropbox');
          $('#visa_application_FiveYears2_date_of_departure_month').addClass('small_dropbox');
          $('#visa_application_FiveYears2_date_of_departure_year').addClass('small_dropbox');
          
          $('#visa_application_OneYears0_date_of_departure_day').addClass('small_dropbox');
          $('#visa_application_OneYears0_date_of_departure_month').addClass('small_dropbox');
          $('#visa_application_OneYears0_date_of_departure_year').addClass('small_dropbox');
          
          $('#visa_application_OneYears1_date_of_departure_day').addClass('small_dropbox');
          $('#visa_application_OneYears1_date_of_departure_month').addClass('small_dropbox');
          $('#visa_application_OneYears1_date_of_departure_year').addClass('small_dropbox');
          
          $('#visa_application_OneYears2_date_of_departure_day').addClass('small_dropbox');
          $('#visa_application_OneYears2_date_of_departure_month').addClass('small_dropbox');
          $('#visa_application_OneYears2_date_of_departure_year').addClass('small_dropbox');
       }   
        window.print();
    }

    
    
    
</script>
