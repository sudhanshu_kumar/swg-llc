<?php
$errorFields = array();
?>
<!--<style type="text/css">.radio_list li{padding:0px!important;}</style>-->
<?php include_stylesheets_for_form($form) ?>
<?php include_javascripts_for_form($form) ?>
<script>
  $(document).ready(function(){
    $(function()
    {
      multipleOnChange();showHideEntryType();getDeportedCountryStatus();checkDisplayLicence();
      var targetFree = "<?php echo (isset ($authorityId) && !empty ($authorityId))? $authorityId:"N"; ?>";


      if(targetFree != "N")
      {
        $('#licence_hdn_id').show();
        $('#authority_hdn_id').show();
        $('#visa_application_ApplicantInfo_licenced_freezone').val('Yes');
        $('#visa_application_ApplicantInfo_authority_id').val(targetFree);
      }
    });
    //
    // provides an popup for height fields
    // popHeight();
  });
  var zoneId = <?php echo Doctrine::getTable('VisaZoneType')->getConventionalZoneId();?>;
  var categoryId = <?php echo $catId = Doctrine::getTable('VisaCategory')->getFreshEntryId();?>;
  var freezoneId = <?php echo Doctrine::getTable('VisaZoneType')->getFreeZoneId();?>;
  var freezoneCategoryId = <?php echo $catId = Doctrine::getTable('VisaCategory')->getFreshEntryFreezoneId();?>;
  function checkDisplayLicence(){

    if($("input[name='visa_application[ApplicantInfo][visatype_id]']:checked").next('label').text() == 'STR (Subject To Regularization)'){
      $('#licence_hdn_id').show();
    }else{
      $('#licence_hdn_id').hide();
      $('#visa_application_ApplicantInfo_authority_id').val('');
      $('#visa_application_ApplicantInfo_licenced_freezone').val('No');
      $('#authority_hdn_id').hide();
    }
  }

  function showHideEntryType(){

    if($("input[name='visa_application[ApplicantInfo][entry_type_id]']:checked").next('label').text() == 'Multiple'){
      $('#hdn_no_of_re_entry_type').show();
    }else{
      $('#hdn_no_of_re_entry_type').hide();
      $('#visa_application_ApplicantInfo_no_of_re_entry_type').val('');


      //$('#authority_hdn_id').hide();
    }
  }

  function multipleOnChange(){

    if($('#visa_application_ApplicantInfo_licenced_freezone').val()=='Yes')
    { 
      $('#authority_hdn_id').show();
      $('#visa_application_visacategory_id').val(freezoneCategoryId);
      $('#visa_application_zone_type_id').val(freezoneId);
    }else{ 
      $('#authority_hdn_id').hide();
      $('#visa_application_ApplicantInfo_authority_id').val('');
      $('#visa_application_visacategory_id').val(categoryId);
      $('#visa_application_zone_type_id').val(zoneId);
    }
  }
  function selectPassportType()
  {
    var err = 0;
    var duration = jQuery.trim($('#visa_application_ApplicantInfo_applying_country_duration').val());
    if(duration == ''){
      $('#visa_application_ApplicantInfo_applying_country_duration_error').html('Please enter Living duration in applied country');
      err = err+1;
    }
    else if(isNaN(duration)) {
      $('#visa_application_ApplicantInfo_applying_country_duration_error').html("Please enter Duration in Year(s).");
      err = err+1;
    }else if(parseInt(duration) > 100) {
      $('#visa_application_ApplicantInfo_applying_country_duration_error').html("Duration can not be more than 100 years.");
      err = err+1;
    }else{
      $('#visa_application_ApplicantInfo_applying_country_duration_error').html('');
    }

    if($('#visa_application_ApplicantInfo_deported_status').val() == 'Yes'){
       if($('#visa_application_ApplicantInfo_deported_county_id').val() == ''){
            $('#visa_application_ApplicantInfo_deported_county_id_error').html("Please select deported country.");
            err = err+1;
       }
    }else{
       $('#visa_application_ApplicantInfo_deported_county_id_error').html("");
    }

    if(err != 0){
        // below code using for anchor...
        var new_position = $('#tabname').offset();
        window.scrollTo(new_position.left,new_position.top);
        return false;
    }

    if(document.getElementById('visa_application_terms_id').checked==false)
    {
      alert('Please select “I Accept” checkbox.');
      document.getElementById('visa_application_terms_id').focus();
      return false;
    }
    document.getElementById('visa_application_terms_id').checked = false;
    //resolve edit application issueTake printed
   <?php if (!$form->getObject()->isNew()) { ?>
    var updatedName = document.getElementById('visa_application_title').value+' '+document.getElementById('visa_application_other_name').value+' '+document.getElementById('visa_application_middle_name').value+' '+document.getElementById('visa_application_surname').value;
    $('#updateNameVal').html(updatedName);
    //return pop2();
   <?php }?>
      document.visa_form.submit();
      return true;
    }

    //display pop mssage for confirmation
    function newApplicationAction()
    {
      window.location = 'http://' + '<?php echo $_SERVER['HTTP_HOST']?>'+"<?php echo url_for('visa/newvisa'); ?>";
    }

    function updateExistingAction()
    {
      document.visa_form.submit();
    }


    $(document).ready(function()
    {



      // Get Embassy according to county Id

      var embassyId = $("#visa_application_ApplicantInfo_embassy_of_pref_id").val();
      var country = $("#visa_application_ApplicantInfo_applying_country_id").val();
      var url = "<?php echo url_for('visa/getEmbassy/'); ?>";
      $("#visa_application_ApplicantInfo_embassy_of_pref_id").load(url, {country_id: country,embassy_id:embassyId});

      $("#visa_application_ApplicantInfo_applying_country_id").change(function()
      {

        var url = "<?php echo url_for('visa/getEmbassy/'); ?>";
        country = $(this).val();
        $("#visa_application_ApplicantInfo_embassy_of_pref_id").load(url, {country_id: country});
      });
      //end of embassy code

      //Text
      // moveUnderstandNode();

    });

    $(document).ready(function()
    {
      /// Temperaray for deported country hiding



      // Relative employer Address  change of country
      $("#visa_application_VisaRelativeEmployerAddress_country_id").change(function(){
        var countryId = $(this).val();
        var url = "<?php echo url_for('visa/getState/'); ?>";
        $("#visa_application_VisaRelativeEmployerAddress_state").load(url, {country_id: countryId,add_option: '--- Please Select ---'});
        $("#visa_application_VisaRelativeEmployerAddress_lga_id").html( '<option value="">--Please Select--</option>');
        $("#visa_application_VisaRelativeEmployerAddress_district_id").html( '<option value="">--Please Select--</option>');
        document.getElementById('visa_application_VisaRelativeEmployerAddress_postcode').value="";
      });

      // Change of Relative Address state
      $("#visa_application_VisaRelativeEmployerAddress_state").change(function(){
        var stateId = $(this).val();
        var url = "<?php echo url_for('visa/getLga/'); ?>";
        $("#visa_application_VisaRelativeEmployerAddress_lga_id").load(url, {state_id: stateId});
        $("#visa_application_VisaRelativeEmployerAddress_district_id").html( '<option value="">--Please Select--</option>');
        document.getElementById('visa_application_VisaRelativeEmployerAddress_postcode').value="";
      });

      // if in near future , client provide postalcodes then it will be uncomment
      /*
$("#visa_application_VisaRelativeEmployerAddress_lga_id").change(function(){
var lgaId = $(this).val();
var url = "<?php //echo url_for('visa/getDistrict/'); ?>";
$("#visa_application_VisaRelativeEmployerAddress_district_id").load(url, {lga_id: lgaId});
document.getElementById('visa_application_VisaRelativeEmployerAddress_postcode').value="";
});

$("#visa_application_VisaRelativeEmployerAddress_district_id").change(function(){
var districtId = $(this).val();
var urlDistrict = "<?php //echo url_for('visa/getPostalCode/'); ?>";

$.get(urlDistrict, {district_id: districtId},function(data){   $("#visa_application_VisaRelativeEmployerAddress_postcode").val(data);},'text');

});
       */
      // intende employer Address  Change of country
      $("#visa_application_VisaIntendedAddressNigeriaAddress_country_id").change(function(){
        var countryId = $(this).val();
        var url = "<?php echo url_for('visa/getState/'); ?>";
        $("#visa_application_VisaIntendedAddressNigeriaAddress_state").load(url, {country_id: countryId,add_option: '--- Please Select ---'});
        $("#visa_application_VisaIntendedAddressNigeriaAddress_lga_id").html( '<option value="">--Please Select--</option>');
        $("#visa_application_VisaIntendedAddressNigeriaAddress_district_id").html( '<option value="">--Please Select--</option>');
        document.getElementById('visa_application_VisaIntendedAddressNigeriaAddress_postcode').value = "";
      });

      // Change of employer Address state
      $("#visa_application_VisaIntendedAddressNigeriaAddress_state").change(function(){
        var stateId = $(this).val();
        var url = "<?php echo url_for('visa/getLga/'); ?>";
        $("#visa_application_VisaIntendedAddressNigeriaAddress_lga_id").load(url, {state_id: stateId});
        $("#visa_application_VisaIntendedAddressNigeriaAddress_district_id").html( '<option value="">--Please Select--</option>');
        document.getElementById('visa_application_VisaIntendedAddressNigeriaAddress_postcode').value = "";
      });
      // if in near future , client provide postalcodes then it will be uncomment
      /*
$("#visa_application_VisaIntendedAddressNigeriaAddress_lga_id").change(function(){
var lgaId = $(this).val();
var url = "<?php //echo url_for('visa/getDistrict/'); ?>";
$("#visa_application_VisaIntendedAddressNigeriaAddress_district_id").load(url, {lga_id: lgaId});
document.getElementById('visa_application_VisaIntendedAddressNigeriaAddress_postcode').value = "";
});

$("#visa_application_VisaIntendedAddressNigeriaAddress_district_id").change(function(){
var districtId = $(this).val();
var urlDistrict = "<?php //echo url_for('visa/getPostalCode/'); ?>";

$.get(urlDistrict, {district_id: districtId},function(data){$("#visa_application_VisaIntendedAddressNigeriaAddress_postcode").val(data);},'text');

});
       */
      // Relative previous history Address  change of country
      $("#visa_application_VisaApplicantPreviousHistoryAddress0_country_id").change(function(){
        var countryId = $(this).val();
        var url = "<?php echo url_for('visa/getState/'); ?>";
        $("#visa_application_VisaApplicantPreviousHistoryAddress0_state").load(url, {country_id: countryId,add_option: '--- Please Select ---'});
        $("#visa_application_VisaApplicantPreviousHistoryAddress0_lga_id").html( '<option value="">--Please Select--</option>');
        $("#visa_application_VisaApplicantPreviousHistoryAddress0_district_id").html( '<option value="">--Please Select--</option>');
        document.getElementById('visa_application_VisaApplicantPreviousHistoryAddress0_postcode').value = "";
      });

      // Change of previous history Address state
      $("#visa_application_VisaApplicantPreviousHistoryAddress0_state").change(function(){
        var stateId = $(this).val();
        var url = "<?php echo url_for('visa/getLga/'); ?>";
        $("#visa_application_VisaApplicantPreviousHistoryAddress0_lga_id").load(url, {state_id: stateId});
        $("#visa_application_VisaApplicantPreviousHistoryAddress0_district_id").html( '<option value="">--Please Select--</option>');
        document.getElementById('visa_application_VisaApplicantPreviousHistoryAddress0_postcode').value = "";
      });

      // if in near future , client provide postalcodes then it will be uncomment
      /*
$("#visa_application_VisaApplicantPreviousHistoryAddress0_lga_id").change(function(){
var lgaId = $(this).val();
var url = "<?php //echo url_for('visa/getDistrict/'); ?>";
$("#visa_application_VisaApplicantPreviousHistoryAddress0_district_id").load(url, {lga_id: lgaId});
document.getElementById('visa_application_VisaApplicantPreviousHistoryAddress0_postcode').value = "";
});

$("#visa_application_VisaApplicantPreviousHistoryAddress0_district_id").change(function(){
var districtId = $(this).val();
var urlDistrict = "<?php //echo url_for('visa/getPostalCode/'); ?>";

$.get(urlDistrict, {district_id: districtId},function(data){   $("#visa_application_VisaApplicantPreviousHistoryAddress0_postcode").val(data);},'text');

});
       */
      // Relative previous history Address  change of country
      $("#visa_application_VisaApplicantPreviousHistoryAddress1_country_id").change(function(){
        var countryId = $(this).val();
        var url = "<?php echo url_for('visa/getState/'); ?>";
        $("#visa_application_VisaApplicantPreviousHistoryAddress1_state").load(url, {country_id: countryId,add_option: '--- Please Select ---'});
        $("#visa_application_VisaApplicantPreviousHistoryAddress1_lga_id").html( '<option value="">--Please Select--</option>');
        $("#visa_application_VisaApplicantPreviousHistoryAddress1_district_id").html( '<option value="">--Please Select--</option>');
        document.getElementById('visa_application_VisaApplicantPreviousHistoryAddress1_postcode').value = "";
      });

      // Change of previous history Address state
      $("#visa_application_VisaApplicantPreviousHistoryAddress1_state").change(function(){
        var stateId = $(this).val();
        var url = "<?php echo url_for('visa/getLga/'); ?>";
        $("#visa_application_VisaApplicantPreviousHistoryAddress1_lga_id").load(url, {state_id: stateId});
        $("#visa_application_VisaApplicantPreviousHistoryAddress1_district_id").html( '<option value="">--Please Select--</option>');
        document.getElementById('visa_application_VisaApplicantPreviousHistoryAddress1_postcode').value = "";
      });
      // if in near future , client provide postalcodes then it will be uncomment
      /*
$("#visa_application_VisaApplicantPreviousHistoryAddress1_lga_id").change(function(){
var lgaId = $(this).val();
var url = "<?php// echo url_for('visa/getDistrict/'); ?>";
$("#visa_application_VisaApplicantPreviousHistoryAddress1_district_id").load(url, {lga_id: lgaId});
document.getElementById('visa_application_VisaApplicantPreviousHistoryAddress1_postcode').value = "";
});

$("#visa_application_VisaApplicantPreviousHistoryAddress1_district_id").change(function(){
var districtId = $(this).val();
var urlDistrict = "<?php// echo url_for('visa/getPostalCode/'); ?>";

$.get(urlDistrict, {district_id: districtId},function(data){   $("#visa_application_VisaApplicantPreviousHistoryAddress1_postcode").val(data);},'text');

});
       */
      // Relative previous history Address  change of country
      $("#visa_application_VisaApplicantPreviousHistoryAddress2_country_id").change(function(){
        var countryId = $(this).val();
        var url = "<?php echo url_for('visa/getState/'); ?>";
        $("#visa_application_VisaApplicantPreviousHistoryAddress2_state").load(url, {country_id: countryId,add_option: '--- Please Select ---'});
        $("#visa_application_VisaApplicantPreviousHistoryAddress2_lga_id").html( '<option value="">--Please Select--</option>');
        $("#visa_application_VisaApplicantPreviousHistoryAddress2_district_id").html( '<option value="">--Please Select--</option>');
        document.getElementById('visa_application_VisaApplicantPreviousHistoryAddress2_postcode').value = "";
      });

      // Change of previous history Address state
      $("#visa_application_VisaApplicantPreviousHistoryAddress2_state").change(function(){
        var stateId = $(this).val();
        var url = "<?php echo url_for('visa/getLga/'); ?>";
        $("#visa_application_VisaApplicantPreviousHistoryAddress2_lga_id").load(url, {state_id: stateId});
        $("#visa_application_VisaApplicantPreviousHistoryAddress2_district_id").html( '<option value="">--Please Select--</option>');
        document.getElementById('visa_application_VisaApplicantPreviousHistoryAddress2_postcode').value = "";
      });
      // if in near future , client provide postalcodes then it will be uncomment
      /*
$("#visa_application_VisaApplicantPreviousHistoryAddress2_lga_id").change(function(){
var lgaId = $(this).val();
var url = "<?php //echo url_for('visa/getDistrict/'); ?>";
$("#visa_application_VisaApplicantPreviousHistoryAddress2_district_id").load(url, {lga_id: lgaId});
document.getElementById('visa_application_VisaApplicantPreviousHistoryAddress2_postcode').value = "";
});

$("#visa_application_VisaApplicantPreviousHistoryAddress2_district_id").change(function(){
var districtId = $(this).val();
var urlDistrict = "<?php //echo url_for('visa/getPostalCode/'); ?>";

$.get(urlDistrict, {district_id: districtId},function(data){   $("#visa_application_VisaApplicantPreviousHistoryAddress2_postcode").val(data);},'text');

});
       */
    });
</script>



<script>

  function goToDirectTab(nextTab){

    // below code using for anchor...
    var new_position = $('#tabname').offset();
    window.scrollTo(new_position.left,new_position.top);


    var activeTab = $('#activeTab').val();
    if(nextTab == "Step 1"){
     var showtab = 1;
    }else if(nextTab == "Step 2"){
      var showtab = 2;
    }
    else if(nextTab == "Step 3"){
      var showtab = 3;
    }
    else if(nextTab == "Step 4"){
      var showtab = 4;
    }
    if(parseInt(activeTab) != parseInt(showtab))
    {
    //active selected tab
    $('#uiGroup_Step_'+showtab+'_of_4').show();
    $('#step'+showtab).removeClass('step_process');
    $('#step'+showtab).addClass('step_process_active');


    //hide current tab
    $('#uiGroup_Step_'+activeTab+'_of_4').hide();
    $('#step'+activeTab).removeClass('step_process_active');
    $('#step'+activeTab).addClass('step_process');

    //change active tab value
    $('#activeTab').val(showtab);


    if(showtab == 1){
      document.getElementById('multiFormNavBackid').style.display = 'none' ;
      $('#multiFormNavNext').show();
      $('#multiFormSubmitid').hide();
    }else if(showtab == 4){
      $('#multiFormSubmitid').show();
      $('#multiFormNavBackid').show();
      $('#multiFormNavNext').hide();
    }
    else{
      $('#multiFormNavBackid').show();
      $('#multiFormNavNext').show();
      $('#multiFormSubmitid').hide();
    }
   }
  }
  function gotoTab(act){

    // below code using for anchor...
    //var new_position = $('#tabname').offset();
    //window.scrollTo(new_position.left,new_position.top);

    var start_stop_flag = '';
    var activeTab = $('#activeTab').val();


    if(act == 'next'){
            activeTab = parseInt(activeTab) + 1;
            var activeTabLoop = activeTab;

        }else{ //if(act == 'back'){
            activeTab = parseInt(activeTab) - 1;
            var activeTabLoop = parseInt(activeTab) + 1;
        }


    for(i=1;i<=activeTabLoop;i++){
        if(i == activeTab){
              $('#uiGroup_Step_'+i+'_of_4').show();
              $('#step'+i).removeClass('step_process');
              $('#step'+i).addClass('step_process_active');
              $('#activeTab').val(activeTab);
        }else{
              if(i==1 && act == 'next'){
                start_stop_flag = validateStep1();
              }else if(i==2 && act == 'next'){
                start_stop_flag = validateStep2();
              }else{
                start_stop_flag = true;
              }
              if(start_stop_flag){
                  $('#uiGroup_Step_'+i+'_of_4').hide();
                  $('#step'+i).removeClass('step_process_active');
                  $('#step'+i).addClass('step_process');
              }else{
                  break;
              }
        }
    }//End of for(i=1;i<=4;i++){...


    if(start_stop_flag){
        if(act == 'next'){
            if(activeTab == 1){
                document.getElementById('multiFormNavBackid').style.display = 'inline-block' ;
                $('#multiFormNavNext').show();
                $('#multiFormSubmitid').hide();
            }else if(activeTab == 4){
                $('#multiFormSubmitid').show();
                $('#multiFormNavBackid').show();
                $('#multiFormNavNext').hide();
            }
            else{
                $('#multiFormNavBackid').show();
                $('#multiFormNavNext').show();
                $('#multiFormSubmitid').hide();
            }
        }else{ //if(act == 'back'){

            if(activeTab == 1){
                $('#multiFormNavBackid').hide();
                $('#multiFormSubmitid').hide();
            }else{
                if(activeTab == 4){
                    document.getElementById('multiFormNavBackid').style.display = 'inline-block' ;
                    document.getElementById('multiFormNavNext').style.display = 'inline-block' ;
                    $('#multiFormSubmitid').show();
                }else{
                    $('#multiFormNavBackid').show();
                    $('#multiFormNavNext').show();
                    $('#multiFormSubmitid').hide();
                }
            }
        }
    }

  }//End of function gotoTab(act){...

var arr = new Array();
  function validateStep1(){

    var err  = 0;
    if($('#visa_application_title').val() == "")
    {
      $('#visa_application_title_error').html("Please select Title");
      err = err+1;
    }
    else
    {
      $('#visa_application_title_error').html("");
    }


    if($('#visa_application_surname').val() == "")
    {
      $('#visa_application_surname_error').html("Please enter Last Name");
      err = err+1;
    }else if(validateString($('#visa_application_surname').val()) !=0)
    {
      $('#visa_application_surname_error').html("Please enter valid Last Name");
      err = err+1;
    }
    else
    {
      $('#visa_application_surname_error').html("");
    }

    if($('#visa_application_other_name').val() == "")
    {
      $('#visa_application_other_name_error').html("Please enter First Name");
      err = err+1;
    }else if(validateString($('#visa_application_other_name').val()) !=0)
    {
      $('#visa_application_other_name_error').html("Please enter valid First Name");
      err = err+1;
    }
    else
    {
      $('#visa_application_other_name_error').html("");
    }

    if($('#visa_application_gender').val() == "")
    {
      $('#visa_application_gender_error').html("Please select Gender");
      err = err+1;
    }
    else
    {
      $('#visa_application_gender_error').html("");
    }

    if($('#visa_application_marital_status').val() == "")
    {
      $('#visa_application_marital_status_error').html("Please select Marital Status");
      err = err+1;
    }
    else
    {
      $('#visa_application_marital_status_error').html("");
    }


    if($('#visa_application_email').val() == "")
    {
      $('#visa_application_email_error').html("Please enter Email");
      err = err+1;
    }
    else if(validateEmail($('#visa_application_email').val()) !=0)
    {
      $('#visa_application_email_error').html("Please enter Valid Email");
      err = err+1;
    }
    else
    {
      $('#visa_application_email_error').html("");
    }

    if($('#visa_application_place_of_birth').val() == "")
    {
      $('#visa_application_place_of_birth_error').html("Please enter Place of Birth");
      err = err+1;
    }
    else
    {
      $('#visa_application_place_of_birth_error').html("");
    }

    /* ### Start Validating date of birth fields ### */
    if($('#visa_application_date_of_birth_day').val() == ""){
      $('#visa_application_date_of_birth_error').html("Please enter valid Date of Birth");
      err = err+1;
    }else if($('#visa_application_date_of_birth_month').val() == ""){
      $('#visa_application_date_of_birth_error').html("Please enter valid Date of Birth");
      err = err+1;
    }else if($('#visa_application_date_of_birth_year').val() == ""){
      $('#visa_application_date_of_birth_error').html("Please enter valid Date of Birth");
      err = err+1;
    }else{
      $('#visa_application_date_of_birth_error').html("");
    }
    /* ### End Validating date of birth fields ### */

    if($('#visa_application_present_nationality_id').val() == "")
    {
      $('#visa_application_present_nationality_id_error').html("Please select Present Nationality");
      err = err+1;
    }
    else
    {
      $('#visa_application_present_nationality_id_error').html("");
    }

    if($('#visa_application_hair_color').val() == "")
    {
      $('#visa_application_hair_color_error').html("Please enter Hair Color");
      err = err+1;
    }
    else
    {
      $('#visa_application_hair_color_error').html("");
    }

    if($('#visa_application_eyes_color').val() == "")
    {
      $('#visa_application_eyes_color_error').html("Please enter Eyes Color");
      err = err+1;
    }
    else
    {
      $('#visa_application_eyes_color_error').html("");
    }

    if($('#visa_application_id_marks').val() == "")
    {
      $('#visa_application_id_marks_error').html("Please enter Identification Marks");
      err = err+1;
    }
    else
    {
      $('#visa_application_id_marks_error').html("");
    }

    if($('#visa_application_height').val() == "")
    {
      $('#visa_application_height_error').html("Please enter Height");
      err = err+1;
    }
    else
    {
      $('#visa_application_height_error').html("");
    }

    if($('#visa_application_VisaPermanentAddressForm_address_1').val() == "")
    {
      $('#visa_application_VisaPermanentAddressForm_address_1_error').html("Please enter Address 1");
      err = err+1;
    }
    else
    {
      $('#visa_application_VisaPermanentAddressForm_address_1_error').html("");
    }

    if($('#visa_application_VisaPermanentAddressForm_city').val() == "")
    {
      $('#visa_application_VisaPermanentAddressForm_city_error').html("Please enter City");
      err = err+1;
    }
    else
    {
      $('#visa_application_VisaPermanentAddressForm_city_error').html("");
    }

    if($('#visa_application_VisaPermanentAddressForm_country_id').val() == "")
    {
      $('#visa_application_VisaPermanentAddressForm_country_id_error').html("Please enter Country");
      err = err+1;
    }
    else
    {
      $('#visa_application_VisaPermanentAddressForm_country_id_error').html("");
    }

    if($('#visa_application_VisaPermanentAddressForm_state').val() == "")
    {
      $('#visa_application_VisaPermanentAddressForm_state_error').html("Please enter State");
      err = err+1;
    }
    else
    {
      $('#visa_application_VisaPermanentAddressForm_state_error').html("");
    }

    if($('#visa_application_perm_phone_no').val() == "")
    {
      $('#visa_application_perm_phone_no_error').html("Please enter Permanent Phone");
      err = err+1;
    }
    else
    {
      $('#visa_application_perm_phone_no_error').html("");
    }

    if($('#visa_application_profession').val() == "")
    {
      $('#visa_application_profession_error').html("Please enter Profession");
      err = err+1;
    }
    else
    {
      $('#visa_application_profession_error').html("");
    }

    if($('#visa_application_VisaOfficeAddressForm_address_1').val() == "")
    {
      $('#visa_application_VisaOfficeAddressForm_address_1_error').html("Please enter Address 1");
      err = err+1;
    }
    else
    {
      $('#visa_application_VisaOfficeAddressForm_address_1_error').html("");
    }

    if($('#visa_application_VisaOfficeAddressForm_city').val() == "")
    {
      $('#visa_application_VisaOfficeAddressForm_city_error').html("Please enter City");
      err = err+1;
    }
    else
    {
      $('#visa_application_VisaOfficeAddressForm_city_error').html("");
    }

    if($('#visa_application_VisaOfficeAddressForm_country_id').val() == "")
    {
      $('#visa_application_VisaOfficeAddressForm_country_id_error').html("Please enter Country");
      err = err+1;
    }
    else
    {
      $('#visa_application_VisaOfficeAddressForm_country_id_error').html("");
    }

    if($('#visa_application_VisaOfficeAddressForm_state').val() == "")
    {
      $('#visa_application_VisaOfficeAddressForm_state_error').html("Please enter State");
      err = err+1;
    }
    else
    {
      $('#visa_application_VisaOfficeAddressForm_state_error').html("");
    }


    //        if($('#visa_application_scan_address_name').val() == "")
    //        {
    //            $('#visa_application_scan_address_name_error').html("Address proof/Passport copy required.");
    //            err = err+1;
    //        }else if($('#visa_application_scan_address_name').val() != ''){
    //            var filename = $('#visa_application_scan_address_name').val();
    //            var filelength = parseInt(filename.length) - 3;
    //            var fileext = filename.substring(filelength, filelength + 3);
    //            if (fileext.toLowerCase() != "gif" && fileext.toLowerCase() != "jpg" && fileext.toLowerCase() != "peg" && fileext.toLowerCase() != "png")
    //            {
    //                $('#visa_application_scan_address_name_error').html("You can only upload gif, jpg, jpeg or png images.");
    //                err = err+1;
    //            }else{
    //                $('#visa_application_scan_address_name_error').html("");
    //            }
    //        }



    if(err == 0){
      return true;
    }else{
     
     alert('Some fields are need to be filled on Step 1.');
      return false;
    }

  }


  function validateStep2(){

    /* calling function to set visa category id into hidden parameters...*/
    multipleOnChange();
    
    var err = 0;
    if($('#visa_application_ApplicantInfo_issusing_govt').val() == ''){
      $('#visa_application_ApplicantInfo_issusing_govt_error').html("Please select Issuing Country");
      err = err+1;
    }else{
      $('#visa_application_ApplicantInfo_issusing_govt_error').html("");
    }
    if($('#visa_application_ApplicantInfo_passport_number').val() == ''){
      $('#visa_application_ApplicantInfo_passport_number_error').html("Please enter Passport Number");
      err = err+1;
    }else{
      $('#visa_application_ApplicantInfo_passport_number_error').html("");
    }


    if (undefined === $("input[name='visa_application[ApplicantInfo][visatype_id]']:checked").val())
    {
      $('#visa_application_ApplicantInfo_visatype_id_error').html("Please select Visa Type");
      err = err+1;
    }
    else
    {
      $('#visa_application_ApplicantInfo_visatype_id_error').html("");
    }

    if($('#visa_application_ApplicantInfo_licenced_freezone').val()=='Yes')
    {
        if($('#visa_application_ApplicantInfo_authority_id').val() == "")
        {
          $('#visa_application_ApplicantInfo_authority_id_error').html("Please select zone authority");
          err = err+1;
        }
        else
        {
          $('#visa_application_ApplicantInfo_authority_id_error').html("");
        }

    }
    //date of issue

    if($('#visa_application_ApplicantInfo_date_of_issue_day').val() == "")
    {
      $('#visa_application_ApplicantInfo_date_of_issue_error').html("Please enter Date of Issue");
      err = err+1;
    }else if($('#visa_application_ApplicantInfo_date_of_issue_month').val() == "")
    {
      $('#visa_application_ApplicantInfo_date_of_issue_error').html("Please enter Date of Issue");
      err = err+1;
    }else if($('#visa_application_ApplicantInfo_date_of_issue_year').val() == "")
    {
      $('#visa_application_ApplicantInfo_date_of_issue_error').html("Please enter Date of Issue");
      err = err+1;
    }
    else
    {
      $('#visa_application_ApplicantInfo_date_of_issue_error').html("");
    }

    //date of expiry
    if($('#visa_application_ApplicantInfo_date_of_exp_day').val() == "")
      {
        $('#visa_application_ApplicantInfo_date_of_exp_error').html("Please enter Date of Expiry");
        err = err+1;
      }else if($('#visa_application_ApplicantInfo_date_of_exp_month').val() == "")
      {
        $('#visa_application_ApplicantInfo_date_of_exp_error').html("Please enter Date of Expiry");
        err = err+1;
      }else if($('#visa_application_ApplicantInfo_date_of_exp_year').val() == "")
      {
        $('#visa_application_ApplicantInfo_date_of_exp_error').html("Please enter Date of Expiry");
        err = err+1;
      }
      else
      {
        $('#visa_application_ApplicantInfo_date_of_exp_error').html("");
      }

    if($('#visa_application_ApplicantInfo_place_of_issue').val() == "")
    {
      $('#visa_application_ApplicantInfo_place_of_issue_error').html("Please enter Place of Issue");
      err = err+1;
    }
    else
    {
      $('#visa_application_ApplicantInfo_place_of_issue_error').html("");
    }

    if($('#visa_application_ApplicantInfo_embassy_of_pref_id').val() == "")
    {
      $('#visa_application_ApplicantInfo_embassy_of_pref_id_error').html("Please select Embassy of Preference");
      err = err+1;
    }
    else
    {
      $('#visa_application_ApplicantInfo_embassy_of_pref_id_error').html("");
    }

    if($('#visa_application_ApplicantInfo_purpose_of_journey').val() == "")
    {
      $('#visa_application_ApplicantInfo_purpose_of_journey_error').html("Please enter Purpose of Journey");
      err = err+1;
    }
    else
    {
      $('#visa_application_ApplicantInfo_purpose_of_journey_error').html("");
    }


    if (undefined === $("input[name='visa_application[ApplicantInfo][entry_type_id]']:checked").val())
    {
      $('#visa_application_ApplicantInfo_entry_type_id_error').html("Please select Number of entries");
      err = err+1;
    }
    else
    {
      $('#visa_application_ApplicantInfo_entry_type_id_error').html("");
    }

    if($('#visa_application_ApplicantInfo_money_in_hand').val() == "")
    {
      $('#visa_application_ApplicantInfo_money_in_hand_error').html("Please enter Money");
      err = err+1;
    }
    else
    {
      $('#visa_application_ApplicantInfo_money_in_hand_error').html("");
    }


    if($("input[name='visa_application[ApplicantInfo][entry_type_id]']:checked").next('label').text() == 'Multiple'){
      if($('#visa_application_ApplicantInfo_no_of_re_entry_type').val() == "")
      {
        $('#visa_application_ApplicantInfo_no_of_re_entry_type_error').html("Please enter Number of entries");
        err = err+1;
      }
      else
      {
        $('#visa_application_ApplicantInfo_no_of_re_entry_type_error').html("");
      }
    }






    if($('#visa_application_ApplicantInfo_stay_duration_days').val() == "")
    {
      $('#visa_application_ApplicantInfo_stay_duration_days_error').html("Please enter Duration");
      err = err+1;
    }
    else
    {
      $('#visa_application_ApplicantInfo_stay_duration_days_error').html("");
    }

    //proposed date valiation
    if($('#visa_application_ApplicantInfo_proposed_date_of_travel_day').val() == "")
    {
      $('#visa_application_ApplicantInfo_proposed_date_of_travel_error').html("Please enter Proposed date of travel");
      err = err+1;
    }else if($('#visa_application_ApplicantInfo_proposed_date_of_travel_month').val() == "")
    {
      $('#visa_application_ApplicantInfo_proposed_date_of_travel_error').html("Please enter Proposed date of travel");
      err = err+1;
    }else if($('#visa_application_ApplicantInfo_proposed_date_of_travel_year').val() == "")
    {
      $('#visa_application_ApplicantInfo_proposed_date_of_travel_error').html("Please enter Proposed date of travel");
      err = err+1;
    }
    else
    {
      $('#visa_application_ApplicantInfo_proposed_date_of_travel_error').html("");
    }

    if($('#visa_application_VisaIntendedAddressNigeriaAddress_address_1').val() == "")
    {
      $('#visa_application_VisaIntendedAddressNigeriaAddress_address_1_error').html("Please enter Address 1");
      err = err+1;
    }
    else
    {
      $('#visa_application_VisaIntendedAddressNigeriaAddress_address_1_error').html("");
    }

    if($('#visa_application_VisaIntendedAddressNigeriaAddress_city').val() == "")
    {
      $('#visa_application_VisaIntendedAddressNigeriaAddress_city_error').html("Please enter City");
      err = err+1;
    }
    else
    {
      $('#visa_application_VisaIntendedAddressNigeriaAddress_city_error').html("");
    }

    if($('#visa_application_VisaIntendedAddressNigeriaAddress_country_id').val() == "")
    {
      $('#visa_application_VisaIntendedAddressNigeriaAddress_country_id_error').html("Please enter Country");
      err = err+1;
    }
    else
    {
      $('#visa_application_VisaIntendedAddressNigeriaAddress_country_id_error').html("");
    }

    if($('#visa_application_VisaIntendedAddressNigeriaAddress_state').val() == "")
    {
      $('#visa_application_VisaIntendedAddressNigeriaAddress_state_error').html("Please enter State");
      err = err+1;
    }
    else
    {
      $('#visa_application_VisaIntendedAddressNigeriaAddress_state_error').html("");
    }


    if(err == 0){
      return true;
    }else{
       alert('Some fields are need to be filled on Step 2.');
      return false;
    }
  }



  function validateString(str)
  {
    var reg = /^[A-Za-z0-9 \' \` \- \. \,]*$/; //allow special characters ` ' - . and space
    if(reg.test(str) == false) {
      return 1;
    }

    return 0;
  }

  function validateEmail(email) {

    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    if(reg.test(email) == false) {

      return 1;
    }

    return 0;
  }

  function copyPermanentAddress(){
    if($('#per_address').attr('checked')){
      var address1 = $('#visa_application_VisaPermanentAddressForm_address_1').val();
      var address2 = $('#visa_application_VisaPermanentAddressForm_address_2').val();
      var city = $('#visa_application_VisaPermanentAddressForm_city').val();
      var country = $('#visa_application_VisaPermanentAddressForm_country_id').val();
      var state = $('#visa_application_VisaPermanentAddressForm_state').val();
      var postcode = $('#visa_application_VisaPermanentAddressForm_postcode').val();
      var phone = $('#visa_application_perm_phone_no').val();

      $('#visa_application_VisaOfficeAddressForm_address_1').val(address1);
      $('#visa_application_VisaOfficeAddressForm_address_2').val(address2);
      $('#visa_application_VisaOfficeAddressForm_city').val(city);
      $('#visa_application_VisaOfficeAddressForm_country_id').val(country);
      $('#visa_application_VisaOfficeAddressForm_state').val(state);
      $('#visa_application_VisaOfficeAddressForm_postcode').val(postcode);
      $('#visa_application_office_phone_no').val(phone);
    }else{
      $('#visa_application_VisaOfficeAddressForm_address_1').val('');
      $('#visa_application_VisaOfficeAddressForm_address_2').val('');
      $('#visa_application_VisaOfficeAddressForm_city').val('');
      $('#visa_application_VisaOfficeAddressForm_country_id').val('');
      $('#visa_application_VisaOfficeAddressForm_state').val('');
      $('#visa_application_VisaOfficeAddressForm_postcode').val('');
      $('#visa_application_office_phone_no').val('');
    }
  }




  function getDeportedCountryStatus() {
    if($("#visa_application_ApplicantInfo_deported_status").val() == 'Yes')
      $("#hdn_deported_county_id").show();
    else
      $("#hdn_deported_county_id").hide();
  }


function expandDiv(id,divId){
  if(id == 'Step 1'){
    if($('#hdn_'+divId).val() == 1){
      $('#div_1').show();
      $("#"+divId).html('[-]');
      $('#hdn_'+divId).val('2');
    }else{
      $('#div_1').hide();
      $("#"+divId).html('[+]');
      $('#hdn_'+divId).val('1');
    }


  }else if(id == 'Step 2'){
      if($('#hdn_'+divId).val() == 1){
      $('#div_2').show();
      $("#"+divId).html('[-]');
      $('#hdn_'+divId).val('2');
    }else{
      $('#div_2').hide();
      $("#"+divId).html('[+]');
      $('#hdn_'+divId).val('1');
    }

  }else if(id == 'Step 3'){
      if($('#hdn_'+divId).val() == 1){
      $('#div_3').show();
      $("#"+divId).html('[-]');
      $('#hdn_'+divId).val('2');
    }else{
      $('#div_3').hide();
      $("#"+divId).html('[+]');
      $('#hdn_'+divId).val('1');
    }


  }else if(id == 'Step 4'){
       if($('#hdn_'+divId).val() == 1){
      $('#div_4').show();
      $("#"+divId).html('[-]');
      $('#hdn_'+divId).val('2');
    }else{
      $('#div_4').hide();
      $("#"+divId).html('[+]');
      $('#hdn_'+divId).val('1');
    }


  }else{
    $('#div_1').hide();
    $('#div_2').hide();
    $('#div_3').hide();
    $('#div_4').hide();
  }

  }
</script>



<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">
      <?php include_partial('global/innerHeading',array('heading'=>"Entry Visa/Free Zone Application (STEP 2)"));?>
	  <div class="clear"></div>
	  <div class="tmz-spacer"></div>

<!--<div class="wrapForm2">-->
  <?php
  $sf = sfContext::getInstance()->getUser();
  if($sf->hasFlash('error')){ ?>
  <div id="flash_error" class="alertBox" >
    <?php
    echo nl2br($sf->getFlash('error'));
    ?>
  </div>
  <?php }?>

  <?php
//  $sf = sfContext::getInstance()->getUser();
  ?>
  <!-- changes for edit issue -->


<?php if (!$form->getObject()->isNew()) {

  //echo ePortal_popup("<p>You are trying to EDIT an EXITING APPLICATION. </p>","<P>Following is the summary of name changes</p><h5>Application Id: ".$form->getObject()->getId()."</h5><h5> Reference No: ".$form->getObject()->getRefNo()."</h5><h5>&nbsp;</h5><h5>Name to update: ".$form->getObject()->getTitleId().' '.$form->getObject()->getFirstName().' '.$form->getObject()->getMidName().' '.$form->getObject()->getLastName()."</h5><h5>Changed Name: <span id='updateNameVal'></span></h5><h5>&nbsp;</h5><h5><a id='updateAction' href='#' onclick='updateExistingAction();'>Update Application</a>&nbsp;&nbsp;<a id='updateAction' href='#' onclick='newApplicationAction();'>Create New Application</a></h5> ");
  $popData = array(
        'appId'=>$form->getObject()->getId(),
        'refId'=>$form->getObject()->getRefNo(),
        'oldName'=>$form->getObject()->getTitle().' '.$form->getObject()->getOtherName().' '.$form->getObject()->getMiddleName().' '.$form->getObject()->getSurname()
  );
  include_partial('global/editPop',$popData);

}?>

  <form name="visa_form" id="visa_form" action="<?php echo url_for('visa/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$encriptedAppId: '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?> id="multiForm" class="dlForm" onsubmit="updateValue()">
    <div id="printHTML">
            <div class="inst_bg">
              <div class="inst_container1">
                <ul>
                  <li>FOR ANY QUESTIONS AND/OR CONCERNS PLEASE CONTACT US AT <a href="mailto:support@swgloballlc.com" class="mail_link">support@swgloballlc.com</a></li>
                  <li>APPLICATION FEES PAID FOR VISA/FREEZONE ARE NON REFUNDABLE EXCEPT IN CASED OF DOUBLE PAYMENT MISTAKENLY MADE FOR THE SAME APPLICATION OR FRAUDULENT USE OF CREDIT/DEBIT CARD. TO SEEK A REFUND PLEASE CONTACT <a href="mailto:refund@swgloballlc.com" class="mail_link">refund@swgloballlc.com</a></li>
                  <li>PAYMENTS ARE VALID FOR SIX (6) MONTHS ONLY FROM THE PAYMENT DATE.</li>
                  <li>ONLY online payment is acceptable.</li>
                  <li>Anyone who pays otherwise and receives service is subject to prosecution and revocation of Visa.</li>
                  <li>If you have already completed an application, please check your application status rather than completing a duplicate application</li>
                  <li>Expatriate Quota is not a requirement for Free Zone Entry Visa</li>
                </ul>
              </div>
            </div>
            <div class="clear"></div>
    

 <div class="tmz-error-container" id="error_msg_div" style="display:none;">
	<!-- error heading starts-->
    <div class="tmz-error-heading">
    <img src="<?php echo image_path('tmz-error.png'); ?>" alt="" title="Error" align="absmiddle"/> Error Summary
    </div>
    <!-- error heading ends-->
     <!-- error box starts-->
    <div class="tmz-error-box">
        <!-- div for all steps-->
        <div class="tmz-error-steps" id="inner_error_div">
   
         </div>
      </div>
</div>  <!-- error container ends-->


            <a name="tabname" id="tabname"></a>
            <div class="steps_container">
              <div class="step_process_active" id="step1">Step 1</div>
              <div class="step_process" id="step2">Step 2</div>
              <div class="step_process" id="step3">Step 3</div>
              <div class="step_process" id="step4">Step 4</div>
            </div>
            <div class="bodyContent2"  style="display:block" id="uiGroup_Step_1_of_4">

              <div class="formH3">
                <h2>Personal Information</h2>
              </div>
              <div class="formContent_new formBrd">
                <div class="lblName"><?php echo $form['title']->renderLabel(); ?><span class="redLbl">*</span></div>
                <div class="lblField">
                  <?php echo $form['title']->render(); ?>
                  <div class="error_msg" id="visa_application_title_error"> <?php echo $errorFields['Step 1'][] = $form['title']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['surname']->renderLabel(); ?><span class="redLbl">*</span></div>
                <div class="lblField">
                  <?php echo $form['surname']->render(); ?>
                  <div class="error_msg" id="visa_application_surname_error"> <?php echo $errorFields['Step 1'][] = $form['surname']->renderError(); ?></div>
                  <i><span class="small-text" style="font-family: Arial,Helvetica,sans-serif;font-size: 10.5px; color: #555555;">(Please avoid placing suffixes with your surname)</span></i>
                </div>
                <div class="lblName"><?php echo $form['other_name']->renderLabel(); ?><span class="redLbl">*</span></div>
                <div class="lblField">
                  <?php echo $form['other_name']->render(); ?>
                  <div class="error_msg" id="visa_application_other_name_error"> <?php echo $errorFields['Step 1'][] = $form['other_name']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['middle_name']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField">
                  <?php echo $form['middle_name']->render(); ?>
                  <div class="error_msg"> <?php echo $errorFields['Step 1'][] = $form['middle_name']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['gender']->renderLabel(); ?><span class="redLbl">*</span></div>
                <div class="lblField">
                  <?php echo $form['gender']->render(); ?>
                  <div class="error_msg" id="visa_application_gender_error"> <?php echo $errorFields['Step 1'][] = $form['gender']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['marital_status']->renderLabel(); ?><span class="redLbl">*</span></div>
                <div class="lblField">
                  <?php echo $form['marital_status']->render(); ?>
                  <div class="error_msg" id="visa_application_marital_status_error"> <?php echo $errorFields['Step 1'][] = $form['marital_status']->renderError(); ?></div>
                </div>

                <div class="lblName"><?php echo $form['email']->renderLabel(); ?><span class="redLbl">*</span></div>
                <div class="lblField">
                  <?php echo $form['email']->render(); ?>
                  <div class="error_msg" id="visa_application_email_error"> <?php echo $errorFields['Step 1'][] = $form['email']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['date_of_birth']->renderLabel(); ?><span class="redLbl">*</span></div>
                <div class="lblField">
                  <?php echo $form['date_of_birth']->render(); ?>
                  <div class="error_msg" id="visa_application_date_of_birth_error"> <?php echo $errorFields['Step 1'][] = $form['date_of_birth']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['place_of_birth']->renderLabel(); ?><span class="redLbl">*</span></div>
                <div class="lblField">
                  <?php echo $form['place_of_birth']->render(); ?>
                  <div class="error_msg" id="visa_application_place_of_birth_error"> <?php echo $errorFields['Step 1'][] = $form['place_of_birth']->renderError(); ?></div>
                </div>

              </div>
              <div class="formContent_new pdLeft9">
                <div class="lblName"><?php echo $form['present_nationality_id']->renderLabel(); ?><span class="redLbl">*</span></div>
                <div class="lblField" id="width_dropbox">
                  <?php echo $form['present_nationality_id']->render(); ?>
                  <div class="error_msg" id="visa_application_present_nationality_id_error"> <?php echo $errorFields['Step 1'][] = $form['present_nationality_id']->renderError(); ?></div>
                </div>

                <div class="lblName"><?php echo $form['previous_nationality_id']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField" id="width_dropbox">
                  <?php echo $form['previous_nationality_id']->render(); ?>
                  <div class="error_msg"> <?php echo $errorFields['Step 1'][] =  $form['previous_nationality_id']->renderError(); ?></div>
                </div>

                <div class="lblName"><?php echo $form['hair_color']->renderLabel(); ?><span class="redLbl">*</span></div>
                <div class="lblField">
                  <?php echo $form['hair_color']->render(); ?>
                  <div class="error_msg" id="visa_application_hair_color_error"> <?php echo $errorFields['Step 1'][] = $form['hair_color']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['eyes_color']->renderLabel(); ?><span class="redLbl">*</span></div>
                <div class="lblField">
                  <?php echo $form['eyes_color']->render(); ?>
                  <div class="error_msg" id="visa_application_eyes_color_error"> <?php echo $errorFields['Step 1'][] = $form['eyes_color']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['id_marks']->renderLabel(); ?><span class="redLbl">*</span></div>
                <div class="lblField">
                  <?php echo $form['id_marks']->render(); ?>
                  <div class="error_msg" id="visa_application_id_marks_error"> <?php echo $errorFields['Step 1'][] = $form['id_marks']->renderError(); ?></div>
                </div>
                <div id="passport_application_height_row">
                  <div class="lblName"><?php echo $form['height']->renderLabel(); ?><span class="redLbl">*</span></div>
                  <div class="lblField">
                    <?php echo $form['height']->render(); ?>
                    <div class="error_msg" id="visa_application_height_error"> <?php echo $errorFields['Step 1'][] = $form['height']->renderError(); ?></div>
                  </div>
                </div>
              </div>

              <div class="formH3">
                <h2>Permanent Address</h2>
                <h2 class="pdLeft9">Office Address</h2>
              </div>
              <div class="formContent_new formBrd">
                <div class="lblName"><?php echo $form['VisaPermanentAddressForm']['address_1']->renderLabel(); ?><span class="redLbl">*</span></div>
                <div class="lblField">
                  <?php echo $form['VisaPermanentAddressForm']['address_1']->render(); ?>
                  <div class="error_msg" id="visa_application_VisaPermanentAddressForm_address_1_error"> <?php echo $errorFields['Step 1'][] = $form['VisaPermanentAddressForm']['address_1']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['VisaPermanentAddressForm']['address_2']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField">
                  <?php echo $form['VisaPermanentAddressForm']['address_2']->render(); ?>
                  <div class="error_msg"> <?php echo $errorFields['Step 1'][] = $form['VisaPermanentAddressForm']['address_2']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['VisaPermanentAddressForm']['city']->renderLabel(); ?><span class="redLbl">*</span></div>
                <div class="lblField">
                  <?php echo $form['VisaPermanentAddressForm']['city']->render(); ?>
                  <div class="error_msg" id="visa_application_VisaPermanentAddressForm_city_error"> <?php echo $errorFields['Step 1'][] = $form['VisaPermanentAddressForm']['city']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['VisaPermanentAddressForm']['country_id']->renderLabel(); ?><span class="redLbl">*</span></div>
                <div class="lblField" id="width_dropbox">
                  <?php echo $form['VisaPermanentAddressForm']['country_id']->render(); ?>
                  <div class="error_msg" id="visa_application_VisaPermanentAddressForm_country_id_error"> <?php echo $errorFields['Step 1'][] = $form['VisaPermanentAddressForm']['country_id']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['VisaPermanentAddressForm']['state']->renderLabel(); ?><span class="redLbl">*</span></div>
                <div class="lblField">
                  <?php echo $form['VisaPermanentAddressForm']['state']->render(); ?>
                  <div class="error_msg" id="visa_application_VisaPermanentAddressForm_state_error"> <?php echo $errorFields['Step 1'][] = $form['VisaPermanentAddressForm']['state']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['VisaPermanentAddressForm']['postcode']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField">
                  <?php echo $form['VisaPermanentAddressForm']['postcode']->render(); ?>
                  <div class="error_msg"> <?php echo $errorFields['Step 1'][] = $form['VisaPermanentAddressForm']['postcode']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['perm_phone_no']->renderLabel(); ?><span class="redLbl">*</span></div>
                <div class="lblField">
                  <?php echo $form['perm_phone_no']->render(); ?>
                  <div class="error_msg" id="visa_application_perm_phone_no_error"> <?php echo $errorFields['Step 1'][] = $form['perm_phone_no']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['profession']->renderLabel(); ?><span class="redLbl">*</span></div>
                <div class="lblField">
                  <?php echo $form['profession']->render(); ?>
                  <div class="error_msg" id="visa_application_profession_error"> <?php echo $errorFields['Step 1'][] = $form['profession']->renderError(); ?></div>
                </div>

              </div>
              <div class="formContent_new pdLeft9">

                <div class="text_hide" ><span style="color:#555">Click here if you want to treat permanent address as the official address.</span></div>
                <div  class="text_hide"><input type="checkbox" name="per_address" id="per_address" value="PA" onclick="copyPermanentAddress();"></div>

                <div class="lblName"><?php echo $form['VisaOfficeAddressForm']['address_1']->renderLabel(); ?><span class="redLbl">*</span></div>
                <div class="lblField">
                  <?php echo $form['VisaOfficeAddressForm']['address_1']->render(); ?>
                  <div class="error_msg" id="visa_application_VisaOfficeAddressForm_address_1_error"> <?php echo $errorFields['Step 1'][] = $form['VisaOfficeAddressForm']['address_1']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['VisaOfficeAddressForm']['address_2']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField">
                  <?php echo $form['VisaOfficeAddressForm']['address_2']->render(); ?>
                  <div class="error_msg"> <?php echo $errorFields['Step 1'][] = $form['VisaOfficeAddressForm']['address_2']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['VisaOfficeAddressForm']['city']->renderLabel(); ?><span class="redLbl">*</span></div>
                <div class="lblField">
                  <?php echo $form['VisaOfficeAddressForm']['city']->render(); ?>
                  <div class="error_msg" id="visa_application_VisaOfficeAddressForm_city_error"> <?php echo $errorFields['Step 1'][] = $form['VisaOfficeAddressForm']['city']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['VisaOfficeAddressForm']['country_id']->renderLabel(); ?><span class="redLbl">*</span></div>
                <div class="lblField" id="width_dropbox">
                  <?php echo $form['VisaOfficeAddressForm']['country_id']->render(); ?>
                  <div class="error_msg" id="visa_application_VisaOfficeAddressForm_country_id_error"> <?php echo $errorFields['Step 1'][] = $form['VisaOfficeAddressForm']['country_id']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['VisaOfficeAddressForm']['state']->renderLabel(); ?><span class="redLbl">*</span></div>
                <div class="lblField">
                  <?php echo $form['VisaOfficeAddressForm']['state']->render(); ?>
                  <div class="error_msg" id="visa_application_VisaOfficeAddressForm_state_error"> <?php echo $errorFields['Step 1'][] = $form['VisaOfficeAddressForm']['state']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['VisaOfficeAddressForm']['postcode']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField">
                  <?php echo $form['VisaOfficeAddressForm']['postcode']->render(); ?>
                  <div class="error_msg"> <?php echo $errorFields['Step 1'][] = $form['VisaOfficeAddressForm']['postcode']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['office_phone_no']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField">
                  <?php echo $form['office_phone_no']->render(); ?>
                  <div class="error_msg"> <?php echo $errorFields['Step 1'][] = $form['office_phone_no']->renderError(); ?></div>
                </div>

              </div>
              <div class="formH3">
                <h2>If you have served in the military,please state</h2>
              </div>
              <div class="formContent_new">
                <div class="lblName"><?php echo $form['milltary_in']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField">
                  <?php echo $form['milltary_in']->render(); ?>
                  <div class="error_msg"> <?php echo $errorFields['Step 1'][] = $form['milltary_in']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['military_dt_from']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField">
                  <?php echo $form['military_dt_from']->render(); ?>
                  <div class="error_msg"> <?php echo $errorFields['Step 1'][] = $form['military_dt_from']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['military_dt_to']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField">
                  <?php echo $form['military_dt_to']->render(); ?>
                  <div class="error_msg"> <?php echo $errorFields['Step 1'][] = $form['military_dt_to']->renderError(); ?></div>
                </div>


              </div>

              <!-- <div class="formH3">
<h2>ID Proof</h2>
</div>
<div class="formContent_new">
<div class="lblName"><?php //echo $form['scan_address_name']->renderLabel(); ?><span class="redLbl">*</span></div>
<div class="lblField">
<?php //echo $form['scan_address_name']->render(); ?>
<div class="error_msg"  id="visa_application_scan_address_name_error" > <?php //echo $form['scan_address_name']->renderError(); ?></div>
</div>
<div class="inst_container1">
<div class="inst_top"></div>
<div class="inst_bg">
<div class="inst_container1" style="padding-left:15px;">
Please upload scanned copy of your passport as per following instructions:<br>
<ul>
<li>Scanned copy of your international passport which should clearly show the first page (with photograph, full name, date &nbsp;&nbsp;&nbsp; of birth).</li>
</ul>
<br><b>Please upload JPG/GIF/PNG images only - (DO NO UPLOAD IMAGE LARGER THAN 500 KB).</b>
</div>
</div>
<div class="inst_bottom"></div>
<div class="clear"></div>
</div>
</div>
              -->
            </div>
            <div class="bodyContent2 no_display" id="uiGroup_Step_2_of_4">
              <div class="formH3">
                <h2>Passport Information</h2>
                <h2 class="pdLeft9">Visa Processing Information</h2>
              </div>
              <div class="formContent_new formBrd">
                <div class="lblName"><?php echo $form['ApplicantInfo']['issusing_govt']->renderLabel(); ?><span class="redLbl">*</span></div>
                <div class="lblField" id="width_dropbox">
                  <?php echo $form['ApplicantInfo']['issusing_govt']->render(); ?>
                  <div class="error_msg" id="visa_application_ApplicantInfo_issusing_govt_error"> <?php echo $errorFields['Step 2'][] = $form['ApplicantInfo']['issusing_govt']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['ApplicantInfo']['passport_number']->renderLabel(); ?><span class="redLbl">*</span></div>
                <div class="lblField">
                  <?php echo $form['ApplicantInfo']['passport_number']->render(); ?>
                  <div class="error_msg" id="visa_application_ApplicantInfo_passport_number_error"> <?php echo $errorFields['Step 2'][] = $form['ApplicantInfo']['passport_number']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['ApplicantInfo']['date_of_issue']->renderLabel(); ?><span class="redLbl">*</span></div>
                <div class="lblField">
                  <?php echo $form['ApplicantInfo']['date_of_issue']->render(); ?>
                  <div class="error_msg" id="visa_application_ApplicantInfo_date_of_issue_error"> <?php echo $errorFields['Step 2'][] = $form['ApplicantInfo']['date_of_issue']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['ApplicantInfo']['date_of_exp']->renderLabel(); ?><span class="redLbl">*</span></div>
                <div class="lblField">
                  <?php echo $form['ApplicantInfo']['date_of_exp']->render(); ?>
                  <div class="error_msg" id="visa_application_ApplicantInfo_date_of_exp_error"> <?php echo $errorFields['Step 2'][] = $form['ApplicantInfo']['date_of_exp']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['ApplicantInfo']['place_of_issue']->renderLabel(); ?><span class="redLbl">*</span></div>
                <div class="lblField">
                  <?php echo $form['ApplicantInfo']['place_of_issue']->render(); ?>
                  <div class="error_msg" id="visa_application_ApplicantInfo_place_of_issue_error"> <?php echo $errorFields['Step 2'][] = $form['ApplicantInfo']['place_of_issue']->renderError(); ?></div>
                </div>

              </div>
              <div class="formContent_new pdLeft9">
                <div class="lblName"><?php echo $form['ApplicantInfo']['visatype_id']->renderLabel(); ?><span class="redLbl">*</span></div>
                <div class="lblField">
                  <?php echo $form['ApplicantInfo']['visatype_id']->render(); ?>
                  <div class="error_msg" id="visa_application_ApplicantInfo_visatype_id_error"> <?php echo $errorFields['Step 2'][] = $form['ApplicantInfo']['visatype_id']->renderError(); ?></div>
                </div>

                <div class="lblName"><?php echo $form['ApplicantInfo']['applying_country_id']->renderLabel(); ?><span class="redLbl">*</span></div>
                <div class="lblField">
                  <?php echo $form['ApplicantInfo']['applying_country_id']->render(); ?>
                  <div class="error_msg"> <?php echo $errorFields['Step 2'][] = $form['ApplicantInfo']['applying_country_id']->renderError(); ?></div>
                </div>
                <div class="clear"></div>
                <div class="no_display" id="licence_hdn_id">
                  <div class="lblName" ><?php echo $form['ApplicantInfo']['licenced_freezone']->renderLabel(); ?><span class="redLbl">*</span></div>
                  <div class="lblField">
                    <?php echo $form['ApplicantInfo']['licenced_freezone']->render(); ?>
                    <div class="error_msg"> <?php echo $errorFields['Step 2'][] = $form['ApplicantInfo']['licenced_freezone']->renderError(); ?></div>
                  </div>
                </div>
                <div class="clear"></div>
                <div class="no_display" id="authority_hdn_id">
                  <div class="lblName"><?php echo $form['ApplicantInfo']['authority_id']->renderLabel(); ?><span class="redLbl"></span></div>
                  <div class="lblField" id="width_dropbox">
                    <?php echo $form['ApplicantInfo']['authority_id']->render(); ?>
                    <div class="error_msg" id="visa_application_ApplicantInfo_authority_id_error"> <?php echo $errorFields['Step 2'][] = $form['ApplicantInfo']['authority_id']->renderError(); ?></div>
                  </div>
                </div>

                <div class="clear"></div>
              </div>
              <div class="formH3">
                <h2>Visa Processing Information </h2>
                <h2 class="pdLeft9">Visa Processing information </h2>
              </div>
              <div class="formContent_new formBrd">
                <div class="lblName"><?php echo $form['ApplicantInfo']['embassy_of_pref_id']->renderLabel(); ?><span class="redLbl">*</span></div>
                <div class="lblField">
                  <?php echo $form['ApplicantInfo']['embassy_of_pref_id']->render(); ?>
                  <div class="error_msg" id="visa_application_ApplicantInfo_embassy_of_pref_id_error"> <?php echo $errorFields['Step 2'][] = $form['ApplicantInfo']['embassy_of_pref_id']->renderError(); ?></div>
                </div><div class="clear"></div>
                <div class="lblName"><?php echo $form['ApplicantInfo']['purpose_of_journey']->renderLabel(); ?><span class="redLbl">*</span></div>
                <div class="lblField">
                  <?php echo $form['ApplicantInfo']['purpose_of_journey']->render(); ?>
                  <div class="error_msg" id="visa_application_ApplicantInfo_purpose_of_journey_error"> <?php echo $errorFields['Step 2'][] = $form['ApplicantInfo']['purpose_of_journey']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['ApplicantInfo']['entry_type_id']->renderLabel(); ?><span class="redLbl">*</span></div>
                <div class="lblField">
                  <?php echo $form['ApplicantInfo']['entry_type_id']->render(); ?>
                  <div class="error_msg" id="visa_application_ApplicantInfo_entry_type_id_error"> <?php echo $errorFields['Step 2'][] = $form['ApplicantInfo']['entry_type_id']->renderError(); ?></div>
                </div>
                <div class="no_display" id="hdn_no_of_re_entry_type">
                  <div class="lblName"><?php echo $form['ApplicantInfo']['no_of_re_entry_type']->renderLabel(); ?><span class="redLbl"></span></div>
                  <div class="lblField">
                    <?php echo $form['ApplicantInfo']['no_of_re_entry_type']->render(); ?>
                    <div class="error_msg" id="visa_application_ApplicantInfo_no_of_re_entry_type_error"> <?php echo $errorFields['Step 2'][] = $form['ApplicantInfo']['no_of_re_entry_type']->renderError(); ?></div>
                  </div>
                </div>
                <div class="clear"></div>


              </div>
              <div class="formContent_new pdLeft9">
                <div class="lblName"><?php echo $form['ApplicantInfo']['money_in_hand']->renderLabel(); ?><span class="redLbl">*</span></div>
                <div class="lblField">
                  <?php echo $form['ApplicantInfo']['money_in_hand']->render(); ?>
                 <div class="error_msg" id="visa_application_ApplicantInfo_money_in_hand_error"><?php echo $errorFields['Step 2'][] = $form['ApplicantInfo']['money_in_hand']->renderError(); ?></div><i><span >(Eg: 100)</span></i>
                </div><div class="clear"></div>

                <div class="lblName"><?php echo $form['ApplicantInfo']['stay_duration_days']->renderLabel(); ?><span class="redLbl">*</span></div>
                <div class="lblField">
                  <?php echo $form['ApplicantInfo']['stay_duration_days']->render(); ?>
                  <div class="error_msg" id="visa_application_ApplicantInfo_stay_duration_days_error"> <?php echo $errorFields['Step 2'][] = $form['ApplicantInfo']['stay_duration_days']->renderError(); ?></div>
                </div><div class="clear"></div>
                <div class="lblName"><?php echo $form['ApplicantInfo']['proposed_date_of_travel']->renderLabel(); ?><span class="redLbl">*</span></div>
                <div class="lblField">
                  <?php echo $form['ApplicantInfo']['proposed_date_of_travel']->render(); ?>
                  <div class="error_msg" id="visa_application_ApplicantInfo_proposed_date_of_travel_error"> <?php echo $errorFields['Step 2'][] = $form['ApplicantInfo']['proposed_date_of_travel']->renderError(); ?></div>
                </div>
                <div class="clear"></div>
                <div class="lblName"><?php echo $form['ApplicantInfo']['mode_of_travel']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField">
                  <?php echo $form['ApplicantInfo']['mode_of_travel']->render(); ?>
                  <div class="error_msg"> <?php echo $errorFields['Step 2'][] = $form['ApplicantInfo']['mode_of_travel']->renderError(); ?></div>
                </div>




                <div class="clear"></div>

              </div>
              <div class="div_height"></div>
              <div class="formH3">
                <h2>If the purpose of your journey to Nigeria is for employment,state</h2>
              </div>
              <div class="formContent_new">
                <div class="lblName"><?php echo $form['VisaDetails']['employer_name']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField">
                  <?php echo $form['VisaDetails']['employer_name']->render(); ?>
                  <div class="error_msg"> <?php echo $errorFields['Step 2'][] = $form['VisaDetails']['employer_name']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['VisaDetails']['position_occupied']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField">
                  <?php echo $form['VisaDetails']['position_occupied']->render(); ?>
                  <div class="error_msg"> <?php echo $errorFields['Step 2'][] = $form['VisaDetails']['position_occupied']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['VisaDetails']['job_description']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField">
                  <?php echo $form['VisaDetails']['job_description']->render(); ?>
                  <div class="error_msg"> <?php echo $errorFields['Step 2'][] = $form['VisaDetails']['job_description']->renderError(); ?></div>
                </div>


              </div>
              <div class="div_height"></div>
              <div class="formH3">
                <h2>Give particulars of the employment of parents,<br/>spouse in Nigeria(if applicable)</h2>
                <h2 class="pdLeft9">Employer's Address</h2>
              </div>
              <div class="formContent_new">
                <div class="lblName"><?php echo $form['VisaDetails']['relative_employer_name']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField">
                  <?php echo $form['VisaDetails']['relative_employer_name']->render(); ?>
                  <div class="error_msg"> <?php echo $errorFields['Step 2'][] = $form['VisaDetails']['relative_employer_name']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['VisaDetails']['relative_employer_phone']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField">
                  <?php echo $form['VisaDetails']['relative_employer_phone']->render(); ?>
                  <div class="error_msg"> <?php echo $errorFields['Step 2'][] = $form['VisaDetails']['relative_employer_phone']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['VisaDetails']['relative_nigeria_leaving_mth']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField">
                  <?php echo $form['VisaDetails']['relative_nigeria_leaving_mth']->render(); ?>
                  <div class="error_msg"> <?php echo $errorFields['Step 2'][] = $form['VisaDetails']['relative_nigeria_leaving_mth']->renderError(); ?></div>
                </div>




              </div>
              <div class="formContent_new pdLeft9 formBrd_left">
                <div class="lblName"><?php echo $form['VisaRelativeEmployerAddress']['address_1']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField">
                  <?php echo $form['VisaRelativeEmployerAddress']['address_1']->render(); ?>
                  <div class="error_msg"> <?php echo $errorFields['Step 2'][] = $form['VisaRelativeEmployerAddress']['address_1']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['VisaRelativeEmployerAddress']['address_2']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField">
                  <?php echo $form['VisaRelativeEmployerAddress']['address_2']->render(); ?>
                  <div class="error_msg"> <?php echo $errorFields['Step 2'][] = $form['VisaRelativeEmployerAddress']['address_2']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['VisaRelativeEmployerAddress']['city']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField">
                  <?php echo $form['VisaRelativeEmployerAddress']['city']->render(); ?>
                  <div class="error_msg"> <?php echo $errorFields['Step 2'][] = $form['VisaRelativeEmployerAddress']['city']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['VisaRelativeEmployerAddress']['country_id']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField" id="width_dropbox">
                  <?php echo $form['VisaRelativeEmployerAddress']['country_id']->render(); ?>
                  <div class="error_msg"> <?php echo $errorFields['Step 2'][] = $form['VisaRelativeEmployerAddress']['country_id']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['VisaRelativeEmployerAddress']['state']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField">
                  <?php echo $form['VisaRelativeEmployerAddress']['state']->render(); ?>
                  <div class="error_msg"> <?php echo $errorFields['Step 2'][] = $form['VisaRelativeEmployerAddress']['state']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['VisaRelativeEmployerAddress']['postcode']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField">
                  <?php echo $form['VisaRelativeEmployerAddress']['postcode']->render(); ?>
                  <div class="error_msg"> <?php echo $errorFields['Step 2'][] = $form['VisaRelativeEmployerAddress']['postcode']->renderError(); ?></div>
                </div>

              </div>
              <div class="formH3">
                <h2>Intended address in Nigeria</h2>
              </div>
              <div class="formContent_new formBrd">
                <div class="lblName"><?php echo $form['VisaIntendedAddressNigeriaAddress']['address_1']->renderLabel(); ?><span class="redLbl">*</span></div>
                <div class="lblField">
                  <?php echo $form['VisaIntendedAddressNigeriaAddress']['address_1']->render(); ?>
                  <div class="error_msg" id="visa_application_VisaIntendedAddressNigeriaAddress_address_1_error"> <?php echo $errorFields['Step 2'][] = $form['VisaIntendedAddressNigeriaAddress']['address_1']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['VisaIntendedAddressNigeriaAddress']['address_2']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField">
                  <?php echo $form['VisaIntendedAddressNigeriaAddress']['address_2']->render(); ?>
                  <div class="error_msg"> <?php echo $errorFields['Step 2'][] = $form['VisaIntendedAddressNigeriaAddress']['address_2']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['VisaIntendedAddressNigeriaAddress']['city']->renderLabel(); ?><span class="redLbl">*</span></div>
                <div class="lblField">
                  <?php echo $form['VisaIntendedAddressNigeriaAddress']['city']->render(); ?>
                  <div class="error_msg" id="visa_application_VisaIntendedAddressNigeriaAddress_city_error"> <?php echo $errorFields['Step 2'][] = $form['VisaIntendedAddressNigeriaAddress']['city']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['VisaIntendedAddressNigeriaAddress']['country_id']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField">
                  <?php echo $form['VisaIntendedAddressNigeriaAddress']['country_id']->render(); ?>
                  <div class="error_msg" id="visa_application_VisaIntendedAddressNigeriaAddress_country_id_error"> <?php echo $errorFields['Step 2'][] = $form['VisaIntendedAddressNigeriaAddress']['country_id']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['VisaIntendedAddressNigeriaAddress']['state']->renderLabel(); ?><span class="redLbl">*</span></div>
                <div class="lblField">
                  <?php echo $form['VisaIntendedAddressNigeriaAddress']['state']->render(); ?>
                  <div class="error_msg" id="visa_application_VisaIntendedAddressNigeriaAddress_state_error"> <?php echo $errorFields['Step 2'][] = $form['VisaIntendedAddressNigeriaAddress']['state']->renderError(); ?></div>
                </div>





              </div>
              <div class="formContent_new pdLeft9">
                <div class="lblName"><?php echo $form['VisaIntendedAddressNigeriaAddress']['lga_id']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField">
                  <?php echo $form['VisaIntendedAddressNigeriaAddress']['lga_id']->render(); ?>
                  <div class="error_msg"> <?php echo $errorFields['Step 2'][] = $form['VisaIntendedAddressNigeriaAddress']['lga_id']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['VisaIntendedAddressNigeriaAddress']['district']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField">
                  <?php echo $form['VisaIntendedAddressNigeriaAddress']['district']->render(); ?>
                  <div class="error_msg"> <?php echo $errorFields['Step 2'][] = $form['VisaIntendedAddressNigeriaAddress']['district']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['VisaIntendedAddressNigeriaAddress']['postcode']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField">
                  <?php echo $form['VisaIntendedAddressNigeriaAddress']['postcode']->render(); ?>
                  <div class="error_msg"> <?php echo $errorFields['Step 2'][] = $form['VisaIntendedAddressNigeriaAddress']['postcode']->renderError(); ?></div>
                </div>




              </div>
            </div>
            <div class="bodyContent2 no_display" id="uiGroup_Step_3_of_4">
              <div class="formH3">
                <h2>Previous Application Information</h2>
              </div>
              <div class="formContent_new formBrd">
                <div class="lblName"><?php echo $form['ApplicantInfo']['applied_nigeria_visa']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField">
                  <?php echo $form['ApplicantInfo']['applied_nigeria_visa']->render(); ?>
                  <div class="error_msg"> <?php echo $errorFields['Step 3'][] = $form['ApplicantInfo']['applied_nigeria_visa']->renderError(); ?></div>
                </div>
                <div class="clear"></div>

                <div class="lblName"><?php echo $form['ApplicantInfo']['nigeria_visa_applied_place']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField">
                  <?php echo $form['ApplicantInfo']['nigeria_visa_applied_place']->render(); ?>
                  <div class="error_msg"> <?php echo $errorFields['Step 3'][] = $form['ApplicantInfo']['nigeria_visa_applied_place']->renderError(); ?></div>
                </div>

                <div class="lblName"><?php echo $form['ApplicantInfo']['applied_nigeria_visa_status']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField">
                  <?php echo $form['ApplicantInfo']['applied_nigeria_visa_status']->render(); ?>
                  <div class="error_msg"> <?php echo $errorFields['Step 3'][] = $form['ApplicantInfo']['applied_nigeria_visa_status']->renderError(); ?></div>
                </div>
                <div class="clear"></div>
                <div class="lblName"><?php echo $form['ApplicantInfo']['applied_nigeria_visa_reject_reason']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField">
                  <?php echo $form['ApplicantInfo']['applied_nigeria_visa_reject_reason']->render(); ?>
                  <div class="error_msg"> <?php echo $errorFields['Step 3'][] = $form['ApplicantInfo']['applied_nigeria_visa_reject_reason']->renderError(); ?></div>
                </div>

                <div class="clear"></div>

              </div>
              <div class="formContent_new pdLeft9">


                <div class="lblName"><?php echo $form['ApplicantInfo']['have_visited_nigeria']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField">
                  <?php echo $form['ApplicantInfo']['have_visited_nigeria']->render(); ?>
                  <div class="error_msg"> <?php echo $errorFields['Step 3'][] = $form['ApplicantInfo']['have_visited_nigeria']->renderError(); ?></div>
                </div>
                <div class="clear"></div>
                <div class="lblName"><?php echo $form['ApplicantInfo']['visited_reason_type_id']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField">
                  <?php echo $form['ApplicantInfo']['visited_reason_type_id']->render(); ?>
                  <div class="error_msg"> <?php echo $errorFields['Step 3'][] = $form['ApplicantInfo']['visited_reason_type_id']->renderError(); ?></div>
                </div>



              </div>
              <div class="formH3">
                <h2>State the period of previous visits to Nigeria and address at which you stayed </h2>
              </div>
              <div class="formH3">
                <h2>Period 1</h2>
                <h2 class="pdLeft9">Address</h2>
              </div>
              <div class="formContent_new">
                <div class="lblName"><?php echo $form['PreviousHistory0']['startdate']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField">
                  <?php echo $form['PreviousHistory0']['startdate']->render(); ?>
                  <div class="error_msg"> <?php echo $errorFields['Step 3'][] = $form['PreviousHistory0']['startdate']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['PreviousHistory0']['endate']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField">
                  <?php echo $form['PreviousHistory0']['endate']->render(); ?>
                  <div class="error_msg"> <?php echo $errorFields['Step 3'][] = $form['PreviousHistory0']['endate']->renderError(); ?></div>
                </div>



                <div class="clear"></div>
                <div class="clear"></div>
              </div>
              <div class="formContent_new pdLeft9 formBrd_left">
                <div class="lblName"><?php echo $form['VisaApplicantPreviousHistoryAddress0']['address_1']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField">
                  <?php echo $form['VisaApplicantPreviousHistoryAddress0']['address_1']->render(); ?>
                  <div class="error_msg"> <?php echo $errorFields['Step 3'][] = $form['VisaApplicantPreviousHistoryAddress0']['address_1']->renderError(); ?></div>
                </div>

                <div class="lblName"><?php echo $form['VisaApplicantPreviousHistoryAddress0']['address_2']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField">
                  <?php echo $form['VisaApplicantPreviousHistoryAddress0']['address_2']->render(); ?>
                  <div class="error_msg"> <?php echo $errorFields['Step 3'][] = $form['VisaApplicantPreviousHistoryAddress0']['address_2']->renderError(); ?></div>
                </div>


                <div class="lblName"><?php echo $form['VisaApplicantPreviousHistoryAddress0']['city']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField">
                  <?php echo $form['VisaApplicantPreviousHistoryAddress0']['city']->render(); ?>
                  <div class="error_msg"> <?php echo $errorFields['Step 3'][] = $form['VisaApplicantPreviousHistoryAddress0']['city']->renderError(); ?></div>
                </div>


                <div class="lblName"><?php echo $form['VisaApplicantPreviousHistoryAddress0']['country_id']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField">
                  <?php echo $form['VisaApplicantPreviousHistoryAddress0']['country_id']->render(); ?>
                  <div class="error_msg"> <?php echo $errorFields['Step 3'][] = $form['VisaApplicantPreviousHistoryAddress0']['country_id']->renderError(); ?></div>
                </div>


                <div class="lblName"><?php echo $form['VisaApplicantPreviousHistoryAddress0']['state']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField">
                  <?php echo $form['VisaApplicantPreviousHistoryAddress0']['state']->render(); ?>
                  <div class="error_msg"> <?php echo $errorFields['Step 3'][] = $form['VisaApplicantPreviousHistoryAddress0']['state']->renderError(); ?></div>
                </div>


                <div class="lblName"><?php echo $form['VisaApplicantPreviousHistoryAddress0']['lga_id']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField">
                  <?php echo $form['VisaApplicantPreviousHistoryAddress0']['lga_id']->render(); ?>
                  <div class="error_msg"> <?php echo $errorFields['Step 3'][] = $form['VisaApplicantPreviousHistoryAddress0']['lga_id']->renderError(); ?></div>
                </div>


                <div class="lblName"><?php echo $form['VisaApplicantPreviousHistoryAddress0']['district']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField">
                  <?php echo $form['VisaApplicantPreviousHistoryAddress0']['district']->render(); ?>
                  <div class="error_msg"> <?php echo $errorFields['Step 3'][] = $form['VisaApplicantPreviousHistoryAddress0']['district']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['VisaApplicantPreviousHistoryAddress0']['postcode']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField">
                  <?php echo $form['VisaApplicantPreviousHistoryAddress0']['postcode']->render(); ?>
                  <div class="error_msg"> <?php echo $errorFields['Step 3'][] = $form['VisaApplicantPreviousHistoryAddress0']['postcode']->renderError(); ?></div>
                </div>


              </div>
              <div class="formH3">
                <h2>Period 2 </h2>
                <h2 class="pdLeft9">Address</h2>
              </div>
              <div class="formContent_new">
                <div class="lblName"><?php echo $form['PreviousHistory1']['startdate']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField">
                  <?php echo $form['PreviousHistory1']['startdate']->render(); ?>
                  <div class="error_msg"> <?php echo $errorFields['Step 3'][] = $form['PreviousHistory1']['startdate']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['PreviousHistory1']['endate']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField">
                  <?php echo $form['PreviousHistory1']['endate']->render(); ?>
                  <div class="error_msg"> <?php echo $errorFields['Step 3'][] = $form['PreviousHistory1']['endate']->renderError(); ?></div>
                </div>
                <div class="clear"></div>
                <div class="clear"></div>
              </div>
              <div class="formContent_new pdLeft9 formBrd_left">
                <div class="lblName"><?php echo $form['VisaApplicantPreviousHistoryAddress1']['address_1']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField">
                  <?php echo $form['VisaApplicantPreviousHistoryAddress1']['address_1']->render(); ?>
                  <div class="error_msg"> <?php echo $errorFields['Step 3'][] = $form['VisaApplicantPreviousHistoryAddress1']['address_1']->renderError(); ?></div>
                </div>

                <div class="lblName"><?php echo $form['VisaApplicantPreviousHistoryAddress1']['address_2']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField">
                  <?php echo $form['VisaApplicantPreviousHistoryAddress1']['address_2']->render(); ?>
                  <div class="error_msg"> <?php echo $errorFields['Step 3'][] = $form['VisaApplicantPreviousHistoryAddress1']['address_2']->renderError(); ?></div>
                </div>


                <div class="lblName"><?php echo $form['VisaApplicantPreviousHistoryAddress1']['city']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField">
                  <?php echo $form['VisaApplicantPreviousHistoryAddress1']['city']->render(); ?>
                  <div class="error_msg"> <?php echo $errorFields['Step 3'][] = $form['VisaApplicantPreviousHistoryAddress1']['city']->renderError(); ?></div>
                </div>


                <div class="lblName"><?php echo $form['VisaApplicantPreviousHistoryAddress1']['country_id']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField">
                  <?php echo $form['VisaApplicantPreviousHistoryAddress1']['country_id']->render(); ?>
                  <div class="error_msg"> <?php echo $errorFields['Step 3'][] = $form['VisaApplicantPreviousHistoryAddress1']['country_id']->renderError(); ?></div>
                </div>


                <div class="lblName"><?php echo $form['VisaApplicantPreviousHistoryAddress1']['state']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField">
                  <?php echo $form['VisaApplicantPreviousHistoryAddress1']['state']->render(); ?>
                  <div class="error_msg"> <?php echo $errorFields['Step 3'][] = $form['VisaApplicantPreviousHistoryAddress1']['state']->renderError(); ?></div>
                </div>


                <div class="lblName"><?php echo $form['VisaApplicantPreviousHistoryAddress1']['lga_id']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField">
                  <?php echo $form['VisaApplicantPreviousHistoryAddress1']['lga_id']->render(); ?>
                  <div class="error_msg"> <?php echo $errorFields['Step 3'][] = $form['VisaApplicantPreviousHistoryAddress1']['lga_id']->renderError(); ?></div>
                </div>


                <div class="lblName"><?php echo $form['VisaApplicantPreviousHistoryAddress1']['district']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField">
                  <?php echo $form['VisaApplicantPreviousHistoryAddress1']['district']->render(); ?>
                  <div class="error_msg"> <?php echo $errorFields['Step 3'][] = $form['VisaApplicantPreviousHistoryAddress1']['district']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['VisaApplicantPreviousHistoryAddress1']['postcode']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField">
                  <?php echo $form['VisaApplicantPreviousHistoryAddress1']['postcode']->render(); ?>
                  <div class="error_msg"> <?php echo $errorFields['Step 3'][] = $form['VisaApplicantPreviousHistoryAddress1']['postcode']->renderError(); ?></div>
                </div>

              </div>
              <div class="div_new_height"></div>
              <div class="formH3">
                <h2>Period 3 </h2>
                <h2 class="pdLeft9">Address</h2>
              </div>
              <div class="formContent_new">
                <div class="lblName"><?php echo $form['PreviousHistory2']['startdate']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField">
                  <?php echo $form['PreviousHistory2']['startdate']->render(); ?>
                  <div class="error_msg"> <?php echo $errorFields['Step 3'][] = $form['PreviousHistory2']['startdate']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['PreviousHistory2']['endate']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField">
                  <?php echo $form['PreviousHistory2']['endate']->render(); ?>
                  <div class="error_msg"> <?php echo $errorFields['Step 3'][] = $form['PreviousHistory2']['endate']->renderError(); ?></div>
                </div>
                <div class="clear"></div>
                <div class="clear"></div>
              </div>
              <div class="formContent_new pdLeft9 formBrd_left">
                <div class="lblName"><?php echo $form['VisaApplicantPreviousHistoryAddress2']['address_1']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField">
                  <?php echo $form['VisaApplicantPreviousHistoryAddress2']['address_1']->render(); ?>
                  <div class="error_msg"> <?php echo $errorFields['Step 3'][] = $form['VisaApplicantPreviousHistoryAddress2']['address_1']->renderError(); ?></div>
                </div>

                <div class="lblName"><?php echo $form['VisaApplicantPreviousHistoryAddress2']['address_2']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField">
                  <?php echo $form['VisaApplicantPreviousHistoryAddress2']['address_2']->render(); ?>
                  <div class="error_msg"> <?php echo $errorFields['Step 3'][] = $form['VisaApplicantPreviousHistoryAddress2']['address_2']->renderError(); ?></div>
                </div>


                <div class="lblName"><?php echo $form['VisaApplicantPreviousHistoryAddress2']['city']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField">
                  <?php echo $form['VisaApplicantPreviousHistoryAddress2']['city']->render(); ?>
                  <div class="error_msg"> <?php echo $errorFields['Step 3'][] = $form['VisaApplicantPreviousHistoryAddress2']['city']->renderError(); ?></div>
                </div>


                <div class="lblName"><?php echo $form['VisaApplicantPreviousHistoryAddress2']['country_id']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField">
                  <?php echo $form['VisaApplicantPreviousHistoryAddress2']['country_id']->render(); ?>
                  <div class="error_msg"> <?php echo $errorFields['Step 3'][] = $form['VisaApplicantPreviousHistoryAddress2']['country_id']->renderError(); ?></div>
                </div>


                <div class="lblName"><?php echo $form['VisaApplicantPreviousHistoryAddress2']['state']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField">
                  <?php echo $form['VisaApplicantPreviousHistoryAddress2']['state']->render(); ?>
                  <div class="error_msg"> <?php echo $errorFields['Step 3'][] = $form['VisaApplicantPreviousHistoryAddress2']['state']->renderError(); ?></div>
                </div>


                <div class="lblName"><?php echo $form['VisaApplicantPreviousHistoryAddress2']['lga_id']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField">
                  <?php echo $form['VisaApplicantPreviousHistoryAddress2']['lga_id']->render(); ?>
                  <div class="error_msg"> <?php echo $errorFields['Step 3'][] = $form['VisaApplicantPreviousHistoryAddress2']['lga_id']->renderError(); ?></div>
                </div>


                <div class="lblName"><?php echo $form['VisaApplicantPreviousHistoryAddress2']['district']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField">
                  <?php echo $form['VisaApplicantPreviousHistoryAddress2']['district']->render(); ?>
                  <div class="error_msg"> <?php echo $errorFields['Step 3'][] = $form['VisaApplicantPreviousHistoryAddress2']['district']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['VisaApplicantPreviousHistoryAddress2']['postcode']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField">
                  <?php echo $form['VisaApplicantPreviousHistoryAddress2']['postcode']->render(); ?>
                  <div class="error_msg"> <?php echo $errorFields['Step 3'][] = $form['VisaApplicantPreviousHistoryAddress2']['postcode']->renderError(); ?></div>
                </div>

              </div>
            </div>
            <div class="bodyContent2 no_display" id="uiGroup_Step_4_of_4">
              <div class="div_height"></div>
              <div class="formH3">
                <h2>Travel History</h2>
              </div>
              <div class="formContent_new formBrd">
                <div class="lblName"><?php echo $form['ApplicantInfo']['applying_country_duration']->renderLabel(); ?><span class="redLbl">*</span></div>
                <div class="lblField">
                  <?php echo $form['ApplicantInfo']['applying_country_duration']->render(); ?>
                  <div class="error_msg" id="visa_application_ApplicantInfo_applying_country_duration_error"> <?php echo $errorFields['Step 4'][] = $form['ApplicantInfo']['applying_country_duration']->renderError(); ?></div>
                </div>
                <div class="clear"></div>
                <div class="lblName"><?php echo $form['ApplicantInfo']['contagious_disease']->renderLabel(); ?><span class="redLbl">*</span></div>
                <div class="lblField">
                  <?php echo $form['ApplicantInfo']['contagious_disease']->render(); ?>
                  <div class="error_msg"> <?php echo $errorFields['Step 4'][] = $form['ApplicantInfo']['contagious_disease']->renderError(); ?></div>
                </div>
                <div class="clear"></div>
                <div class="lblName"><?php echo $form['ApplicantInfo']['police_case']->renderLabel(); ?><span class="redLbl">*</span></div>
                <div class="lblField">
                  <?php echo $form['ApplicantInfo']['police_case']->render(); ?>
                  <div class="error_msg"> <?php echo $errorFields['Step 4'][] = $form['ApplicantInfo']['police_case']->renderError(); ?></div>
                </div>
                <div class="clear"></div>
                <div class="lblName"><?php echo $form['ApplicantInfo']['narcotic_involvement']->renderLabel(); ?><span class="redLbl">*</span></div>
                <div class="lblField">
                  <?php echo $form['ApplicantInfo']['narcotic_involvement']->render(); ?>
                  <div class="error_msg"> <?php echo $errorFields['Step 4'][] = $form['ApplicantInfo']['narcotic_involvement']->renderError(); ?></div>
                </div>
                <div class="clear"></div>
              </div>
              <div class="formContent_new pdLeft9">
                <div class="lblName"><?php echo $form['ApplicantInfo']['deported_status']->renderLabel(); ?><span class="redLbl">*</span></div>
                <div class="lblField">
                  <?php echo $form['ApplicantInfo']['deported_status']->render(); ?>
                  <div class="error_msg"> <?php echo $errorFields['Step 4'][] = $form['ApplicantInfo']['deported_status']->renderError(); ?></div>
                </div>
                <div class="clear"></div>
                <div class="no_display" id="hdn_deported_county_id">
                  <div class="lblName" ><?php echo $form['ApplicantInfo']['deported_county_id']->renderLabel(); ?><span class="redLbl"></span></div>
                  <div class="lblField" id="width_dropbox">
                    <?php echo $form['ApplicantInfo']['deported_county_id']->render(); ?>
                    <div class="error_msg" id="visa_application_ApplicantInfo_deported_county_id_error"> <?php echo $errorFields['Step 4'][] = $form['ApplicantInfo']['deported_county_id']->renderError(); ?></div>
                  </div>
                </div>
                <div class="clear"></div>
                <div class="lblName"><?php echo $form['ApplicantInfo']['visa_fraud_status']->renderLabel(); ?><span class="redLbl">*</span></div>
                <div class="lblField">
                  <?php echo $form['ApplicantInfo']['visa_fraud_status']->render(); ?>
                  <div class="error_msg"> <?php echo $errorFields['Step 4'][] = $form['ApplicantInfo']['visa_fraud_status']->renderError(); ?></div>
                </div>
                <div class="clear"></div>


              </div>

              <div class="formH3">
                <h2>Give a list of the countries you have lived for more than one year </h2>
              </div>
              <div class="formH3">
                <h2>Period 1</h2>
                <h2 class="pdLeft9">Period 2 </h2>
              </div>
              <div class="formContent_new">

                <div class="lblName"><?php echo $form['FiveYears0']['country_id']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField" id="width_dropbox">
                  <?php echo $form['FiveYears0']['country_id']->render(); ?>
                  <div class="error_msg"> <?php echo $errorFields['Step 4'][] = $form['FiveYears0']['country_id']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['FiveYears0']['city']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField">
                  <?php echo $form['FiveYears0']['city']->render(); ?>
                  <div class="error_msg"> <?php echo $errorFields['Step 4'][] = $form['FiveYears0']['city']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['FiveYears0']['date_of_departure']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField">
                  <?php echo $form['FiveYears0']['date_of_departure']->render(); ?>
                  <div class="error_msg"> <?php echo $errorFields['Step 4'][] = $form['FiveYears0']['date_of_departure']->renderError(); ?></div>
                </div>

                <div class="clear"></div>
                <div class="clear"></div>
              </div>
              <div class="formContent_new pdLeft9 formBrd_left">


                <div class="lblName"><?php echo $form['FiveYears1']['country_id']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField" id="width_dropbox">
                  <?php echo $form['FiveYears1']['country_id']->render(); ?>
                  <div class="error_msg"> <?php echo $errorFields['Step 4'][] = $form['FiveYears1']['country_id']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['FiveYears1']['city']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField">
                  <?php echo $form['FiveYears1']['city']->render(); ?>
                  <div class="error_msg"> <?php echo $errorFields['Step 4'][] = $form['FiveYears1']['city']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['FiveYears1']['date_of_departure']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField">
                  <?php echo $form['FiveYears1']['date_of_departure']->render(); ?>
                  <div class="error_msg"> <?php echo $errorFields['Step 4'][] = $form['FiveYears1']['date_of_departure']->renderError(); ?></div>
                </div>



              </div>
              <div class="formH3">
                <h2>Period 3 </h2>
              </div>
              <div class="formContent_new">


                <div class="lblName"><?php echo $form['FiveYears2']['country_id']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField" id="width_dropbox">
                  <?php echo $form['FiveYears2']['country_id']->render(); ?>
                  <div class="error_msg"> <?php echo $errorFields['Step 4'][] = $form['FiveYears2']['country_id']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['FiveYears2']['city']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField">
                  <?php echo $form['FiveYears2']['city']->render(); ?>
                  <div class="error_msg"> <?php echo $errorFields['Step 4'][] = $form['FiveYears2']['city']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['FiveYears2']['date_of_departure']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField">
                  <?php echo $form['FiveYears2']['date_of_departure']->render(); ?>
                  <div class="error_msg"> <?php echo $errorFields['Step 4'][] = $form['FiveYears2']['date_of_departure']->renderError(); ?></div>
                </div>


                <div class="clear"></div>
                <div class="clear"></div>
              </div>
              <!--			  <div class="new_height"></div>-->
              <div class="formH3">
                <h2>Give a list of the countries you have visited in the last twelve(12) months</h2>
              </div>
              <div class="formH3">
                <h2>Period 1</h2>
                <h2 class="pdLeft9">Period 2 </h2>
              </div>
              <div class="formContent_new">


                <div class="lblName"><?php echo $form['OneYears0']['country_id']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField" id="width_dropbox">
                  <?php echo $form['OneYears0']['country_id']->render(); ?>
                  <div class="error_msg"> <?php echo $errorFields['Step 4'][] = $form['OneYears0']['country_id']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['OneYears0']['city']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField">
                  <?php echo $form['OneYears0']['city']->render(); ?>
                  <div class="error_msg"> <?php echo $errorFields['Step 4'][] = $form['OneYears0']['city']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['OneYears0']['date_of_departure']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField">
                  <?php echo $form['OneYears0']['date_of_departure']->render(); ?>
                  <div class="error_msg"> <?php echo $errorFields['Step 4'][] = $form['OneYears0']['date_of_departure']->renderError(); ?></div>
                </div>



                <div class="clear"></div>
                <div class="clear"></div>
              </div>
              <div class="formContent_new pdLeft9 formBrd_left">

                <div class="lblName"><?php echo $form['OneYears1']['country_id']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField" id="width_dropbox">
                  <?php echo $form['OneYears1']['country_id']->render(); ?>
                  <div class="error_msg"> <?php echo $errorFields['Step 4'][] = $form['OneYears1']['country_id']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['OneYears1']['city']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField">
                  <?php echo $form['OneYears1']['city']->render(); ?>
                  <div class="error_msg"> <?php echo $errorFields['Step 4'][] = $form['OneYears1']['city']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['OneYears1']['date_of_departure']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField">
                  <?php echo $form['OneYears1']['date_of_departure']->render(); ?>
                  <div class="error_msg"> <?php echo $errorFields['Step 4'][] = $form['OneYears1']['date_of_departure']->renderError(); ?></div>
                </div>
              </div>
              <div class="formH3">
                <h2>Period 3 </h2>
              </div>
              <div class="formContent_new">
                <div class="lblName"><?php echo $form['OneYears2']['country_id']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField" id="width_dropbox">
                  <?php echo $form['OneYears2']['country_id']->render(); ?>
                  <div class="error_msg"> <?php echo $errorFields['Step 4'][] = $form['OneYears2']['country_id']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['OneYears2']['city']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField">
                  <?php echo $form['OneYears2']['city']->render(); ?>
                  <div class="error_msg"> <?php echo $errorFields['Step 4'][] = $form['OneYears2']['city']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['OneYears2']['date_of_departure']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField">
                  <?php echo $form['OneYears2']['date_of_departure']->render(); ?>
                  <div class="error_msg"> <?php echo $errorFields['Step 4'][] = $form['OneYears2']['date_of_departure']->renderError(); ?></div>
                </div>
                <div class="clear"></div>

              </div>
              <div class="clear"></div>
              <div class="body_text border_top">
			  <br/>
                <input type="checkbox" name="visa_application[terms_id]" id="visa_application_terms_id" value="checkbox" />
              I understand that I will be required to comply with immigration/alien and other laws governing entry of immigrants into the country to which I now apply for Visa/Entry permit.<br/><br/></div>
              <div class="error_msg"> <?php echo $errorFields['Step 4'][] = $form['terms_id']->renderError(); ?></div>
            </div>
            <div class="clear"></div>
  <!--        </div>-->

<?php //echo $form ?>
          <input type="hidden" name="countryId" value="<?php echo $countryId;?>" />

          <?php echo $form['id']->render(); ?>
          <?php echo $form['visacategory_id']->render(); ?>
          <?php echo $form['zone_type_id']->render(); ?>
          <?php echo $form['ApplicantInfo']['id']->render(); ?>
          <?php echo $form['VisaDetails']['id']->render(); ?>
          <?php echo $form['VisaIntendedAddressNigeriaAddress']['id']->render(); ?>
          <?php echo $form['VisaRelativeEmployerAddress']['id']->render(); ?>
          <?php echo $form['VisaOfficeAddressForm']['id']->render(); ?>
          <?php echo $form['VisaPermanentAddressForm']['id']->render(); ?>
          <?php echo $form['PreviousHistory0']['id']->render(); ?>
          <?php echo $form['PreviousHistory1']['id']->render(); ?>
          <?php echo $form['PreviousHistory2']['id']->render(); ?>
          <?php echo $form['PreviousHistory2']['id']->render(); ?>
          <?php echo $form['VisaApplicantPreviousHistoryAddress0']['id']->render(); ?>
          <?php echo $form['VisaApplicantPreviousHistoryAddress1']['id']->render(); ?>
          <?php echo $form['VisaApplicantPreviousHistoryAddress2']['id']->render(); ?>
          <?php echo $form['FiveYears0']['id']->render(); ?>
          <?php echo $form['FiveYears1']['id']->render(); ?>
          <?php echo $form['FiveYears2']['id']->render(); ?>
          <?php echo $form['OneYears0']['id']->render(); ?>
          <?php echo $form['OneYears1']['id']->render(); ?>
          <?php echo $form['OneYears2']['id']->render(); ?>
  <!--    </div>--><!--</div>-->
    </div> <!-- END of <div class="container_main" id="printHTML"> -->
<!--    <div class="container_bottombg"></div>-->

    <div class="clear"></div>
    <div class="pixbr_new X50_new mg_left_zero">
        <p class="text_hide"><font color="red">*</font> - Compulsory fields</p>
        <div class="Y20_new">
        </div>
        <div class="border_top_new"></div>
        <div>
            <p><span class="notice-text" >PLEASE NOTE: PRINT AND SIGN THIS APPLICATION AND TAKE IT WITH TWO (2) PASSPORT PHOTOGRAPHS TO THE SELECTED PASSPORT OFFICE FOR FURTHER PROCESSING.</span></p>
        </div>
    </div>
    <div align="center">

    <div id="btn_visa" align="center">
     
      <div class="a_button" id="multiFormNavBack"><a href="#" class='normalbutton no_display' style="text-decoration:none; border:none;" id='multiFormNavBackid'  onclick="gotoTab('back');">Back</a></div>
      <div class="a_button" id="multiFormNavNext"><a href ="#" class='normalbutton pdleft15' id=''  onclick="gotoTab('next');">Next</a></div>
      <?php if ($form->getObject()->isNew()) {?>
      <div class="a_button" id="multiFormSubmit" >
      <a href="#" id="multiFormSubmitid"  value="Submit Application" class='normalbutton no_display' onclick="return selectPassportType();" >Submit Application</a></div>

        <?php  }
      else if (!$form->getObject()->isNew()) { ?>
      <div class="a_button" id="multiFormSubmit" > <a href="#" id="multiFormSubmitid"  value="Update Application" class='normalbutton no_display' onclick="return selectPassportType();" >Proceed</a></div>
      <?php } ?>

      <!--div class="a_button" id="multiFormSubmit1"><a href="javascript:void(0);" id="multiFormSubmit1" value="Print" class="input_btn"  onClick= "printPassportNew();return false;">Print</a></div-->
    </div>
    </div>
    <?php if ($form->getObject()->isNew()) {?>
      <?php }else if (!$form->getObject()->isNew()) { ?>
    <input type="hidden" name="sf_method" value="put" />
    <?php } ?>
<!-- </div> -->
  </form>
  <script>
    var ht='';
<?php

  if(count($errorFields) > 0){
    for($i=1;$i<=count($errorFields);$i++){
      if(count(array_filter($errorFields['Step '.$i])) > 0){
        //$errorPageNumber = $i;
        $error = array_filter($errorFields['Step '.$i]);
        ?>
        ht += "<div class='tmz-error-step-heading'><span id='<?php echo $i; ?>' onclick='expandDiv(\"Step "+<?php echo $i; ?>+"\","+<?php echo $i; ?>+")' style='cursor:pointer;'>[-]</span> <span class='tmz-step-heading'>Step "+<?php echo $i; ?>+"</span>&nbsp;&nbsp; [Please <span style='cursor:pointer' onclick='goToDirectTab(\"Step "+<?php echo $i; ?>+"\")'><b><u>Click Here</u></b></span> to see the errors  <input type ='hidden' id='hdn_"+ <?php echo $i; ?> +"' value='0'>]</div>";
        ht += '<div class ="tmz-error-step-info" style="display:blank;" id="div_'+<?php echo $i; ?>+'">';

        <?php foreach($error as $key=>$val){ ?>
          ht += "<ul><li><?php echo trim($val); ?></li></ul>";
       <?php } ?>
        ht += '</div>';
<?php
      }
    }
  }


$errorPageNumber = 0;
if(count($errorFields) > 0){
  for($i=1;$i<=count($errorFields);$i++){
    if(count(array_filter($errorFields['Step '.$i])) > 0){
      $errorPageNumber = $i-1;
      break;
    }
  }
}

?>
  if(ht !=''){
    $('#error_msg_div').show();
    $('#inner_error_div').html(ht);
  }
   
</script>

<div class="pixbr XY20"></div>
<div class="warningArea">
  <h2>Warning</h2>
  <ul>
    <li>PLEASE ENSURE THAT ALL INFORMATION PROVIDED ON YOUR APPLICATION IS CORRECT BEFORE YOU PROCEED TO PAYMENT. YOUR APPLICATION CANNOT BE
      EDITED ONCE PAYMENT IS INITIATED.
    </li>
  </ul>
</div>
<input type="hidden" name="activeTab" id="activeTab" value="1" />
<!--</div>-->

<script>

  // Showing error page...
  var errorPageNumber = '<?php echo $errorPageNumber;?>';
  if (errorPageNumber != -1){
      $('#activeTab').val(errorPageNumber);
      gotoTab('next');
  }


  // Setting default activeTab value...



</script>

</div>
</div>
<div class="content_wrapper_bottom"></div>
