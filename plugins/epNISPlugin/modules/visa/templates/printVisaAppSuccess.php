<?php
function getDateFormate($date)
{
 $dateFormate = date("d-m-Y", strtotime($date));

  return $dateFormate;
}


?>
<link rel="stylesheet" type="text/css" media="print" href="<?php  echo stylesheet_path('print');?>" />
<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">
  <?php if ($sf_user->hasFlash('error')): ?>
  <div id="flash_notice" class="alertBox" > <?php echo nl2br($sf_user->getFlash('error'));?></div>
  <?php  endif  ?>
  <div class="noscreen"> <img src="<?php echo image_path('nis.gif'); ?>" style="height: 63px; width: 45px;" alt="Test" class="pd4" /> </div>
    <?php include_partial('global/innerHeading',array('heading'=>"Entry Visa/Free Zone Application"));?>
    <div class="clear"></div>
    <div class="tmz-spacer"></div>
    <div id="printHTML">
      <div class="clear_print"></div>
      <div class="page_heading_new">Entry Visa/Free Zone Application</div>
      <div class="inst_bg">
          <div class="inst_container1">
                <ul>
                  <li>FOR ANY QUESTIONS AND/OR CONCERNS PLEASE CONTACT US AT <a href="mailto:support@swgloballlc.com" class="mail_link">support@swgloballlc.com</a></li>
                  <li>APPLICATION FEES PAID FOR VISA/FREEZONE ARE NON REFUNDABLE EXCEPT IN CASED OF DOUBLE PAYMENT MISTAKENLY MADE FOR THE SAME APPLICATION OR FRAUDULENT USE OF CREDIT/DEBIT CARD. TO SEEK A REFUND PLEASE CONTACT <a href="mailto:refund@swgloballlc.com" class="mail_link">refund@swgloballlc.com</a></li>
                  <li>PAYMENTS ARE VALID FOR SIX (6) MONTHS ONLY FROM THE PAYMENT DATE.</li>
                  <li>ONLY online payment is acceptable.</li>
                  <li>Anyone who pays otherwise and receives service is subject to prosecution and revocation of Visa.</li>
                  <li>If you have already completed an application, please check your application status rather than completing a duplicate application</li>
                  <li>Expatriate Quota is not a requirement for Free Zone Entry Visa</li>
                </ul>
              </div>
            </div>
            <div class="clear"></div>
            <div class='confirmation-msg'>
        <div class="confirmation-msg1">
                <p>
                  <center>
              <b class="red">BELOW IS YOUR APPLICATION ID AND REFERENCE NUMBER</b>
                  </center>
                </p>
                <p>
                  <center>
              <b class="red">KINDLY PRINT THE SCREEN AND KEEP IT SAFE</b>
                  </center>
                </p>
                <p>
                  <center>
              <b class="red">YOU WILL NEED THEM TO COMPLETE THE PROCESS AT THE EMBASSY, CONSULATE OR HIGH COMMISSION</b>
                  </center>
                </p>
              </div>
              <h5>
                <center>
            Application Id: <?php echo $visa_application[0]['id']; ?>
                </center>
              </h5>
              <h5>
                <center>
            Reference No: <?php echo $visa_application[0]['ref_no'] ?>
                </center>
              </h5>
            </div>
      <div class="clear">&nbsp;</div>
      <table cellpadding="0" cellspacing="0" border="0" class="no_border_new" width="695">
                  <tr>
          <td valign="top"  id="uiGroup_Step_1_of_4"><table cellpadding="0" cellspacing="0" border="0" class="no_border_new" width="695">
              <tr>
                <td width="347" height="25" valign="middle" id="form_new_heading">Personal Information</td>
                <td rowspan="32" align="center" valign="top" id="border_new_left"></td>
                <td width="347" height="25" valign="middle" id="form_new_heading">&nbsp;</td>
              </tr>
              <tr>
                <td valign="top" height="5"></td>
                <td valign="top" height="5"></td>
              </tr>
              <tr>
                <td valign="top"><table width="347" border="0" cellspacing="0" cellpadding="0" class="no_border_new">
                    <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_title">Title</label></td>
                    <td width="175" valign="top" class="lblfield_new"><span class="lblField">
                      <?php
                    if(!empty ($visa_application[0]['title'])){
                      echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['title']."'>";
                    }
                    else{
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                    }
                ?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_surname">Last Name (<i>Surname</i>)</label></td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php
                  if($visa_application[0]['surname']){
                    echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['surname']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }
                  ?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_other_name">First Name</label></td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php
                  if(!empty ($visa_application[0]['other_name'])){
                    echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['other_name']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_middle_name">Middle Name</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php
                  if(!empty ($visa_application[0]['middle_name'])){
                    echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['middle_name']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='' readonly = 'readonly' >";
                  }?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_gender">Gender</label></td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php
                  if(!empty ($visa_application[0]['gender'])){
                    echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['gender']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }?>
                      </span></td>
                  </tr>
                  <tr>
                    <td valign="top" class="label_name_new"><label for="visa_application_marital_status">Marital Status</label></td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php
                  if(!empty ($visa_application[0]['marital_status'])){
                    echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['marital_status']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }?>
                      </span></td>
                  </tr>
                  <tr>
                    <td valign="top" class="label_name_new"><label for="visa_application_email">Email</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php
                  if(!empty ($visa_application[0]['email'])){
                    echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['email']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }?>
                      </span></td>
                  </tr>
                  <tr>
                    <td valign="top" class="label_name_new"><label for="visa_application_date_of_birth">Date of Birth (dd-mm-yyyy)</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php
                  if(!empty ($visa_application[0]['date_of_birth'])){
                    $explode = explode('-',getDateFormate($visa_application[0]['date_of_birth']));
                    echo "<input type='text' class='printdate' readonly=readonly value='".$explode[0]."'>";
                    echo "<input type='text' class='printdate' readonly=readonly value='".$explode[1]."'>";
                    echo "<input type='text' class='printyear' readonly=readonly value='".$explode[2]."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }?>
                      </span></td>
                  </tr>
                  <tr>
                    <td valign="top" class="label_name_new"><label for="visa_application_place_of_birth">Place of Birth</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php
                  if(!empty ($visa_application[0]['place_of_birth'])){
                    echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['place_of_birth']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }?>
                      </span></td>
                  </tr>
                  </table></td>
                <td valign="top"><table width="347" border="0" cellspacing="0" cellpadding="0" class="no_border_new">
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_present_nationality_id">Present Nationality</label>
                    </td>
                    <td width="175" valign="top" class="lblfield_new"><span class="lblField">
                      <?php
                  if(!empty ($visa_application[0]['CurrentCountry']['country_name'])){
                  echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['CurrentCountry']['country_name']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_previous_nationality_id">Previous Nationality</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php
                  if(!empty ($visa_application[0]['previous_country'])){
                    echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['previous_country']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_hair_color">Color of Hair</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php
                  if(!empty ($visa_application[0]['hair_color'])){
                    echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['hair_color']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_eyes_color">Color of Eyes</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php
                  if(!empty ($visa_application[0]['eyes_color'])){
                  echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['eyes_color']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_id_marks">Identification Marks</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php
                  if(!empty ($visa_application[0]['id_marks'])){
                    echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['id_marks']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }?>
                      </span></td>
                  </tr>
                  <tr>
                    <td valign="top" class="label_name_new"><label for="visa_application_height">Height (in cm)</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php
                    if(!empty ($visa_application[0]['height'])){
                    echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['height']."'>";
                    }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }?>
                      </span></td>
                  </tr>
                  </table></td>
              </tr>
                  <tr>
                <td valign="top" height="5"></td>
                <td valign="top" height="5"></td>
              </tr>
              <tr>
                <td height="25" valign="middle" id="form_new_heading">Permanent Address</td>
                <td valign="middle" id="form_new_heading">Office Address</td>
              </tr>
              <tr>
                <td valign="top" height="5"></td>
                <td valign="top" height="5"></td>
              </tr>
              <tr>
                <td valign="top"><table width="347" border="0" cellspacing="0" cellpadding="0" class="no_border_new">
                    <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_VisaPermanentAddressForm_address_1">Address 1</label>
                    </td>
                    <td width="175" valign="top" class="lblfield_new"><span class="lblField">
                      <?php
                  if(!empty ($visa_application[0]['VisaPermanentAddress']['address_1'])){
                    echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['VisaPermanentAddress']['address_1']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_VisaPermanentAddressForm_address_2">Address 2</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php
                  if(!empty ($visa_application[0]['VisaPermanentAddress']['address_2'])){
                    echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['VisaPermanentAddress']['address_2']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='' readonly = 'readonly' >";
                  }?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_VisaPermanentAddressForm_city">City</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php

                  if(!empty ($visa_application[0]['VisaPermanentAddress']['city'])){
                    echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['VisaPermanentAddress']['city']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_VisaPermanentAddressForm_country_id">Country</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php

                  if(!empty ($visa_application[0]['VisaPermanentAddress']['Country']['country_name'])){
                    echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['VisaPermanentAddress']['Country']['country_name']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_VisaPermanentAddressForm_state">State</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php

                  if(!empty ($visa_application[0]['VisaPermanentAddress']['state'])){
                    echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['VisaPermanentAddress']['state']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }?>
                      </span></td>
                  </tr>
                  <tr>
                    <td valign="top" class="label_name_new"><label for="visa_application_VisaPermanentAddressForm_postcode">Postcode</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php

                  if(!empty ($visa_application[0]['VisaPermanentAddress']['postcode'])){
                    echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['VisaPermanentAddress']['postcode']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='' readonly = 'readonly' >";
                  }?>
                      </span></td>
                  </tr>
                  <tr>
                    <td valign="top" class="label_name_new"><label for="visa_application_perm_phone_no">Permanent Phone</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php
                  if(!empty ($visa_application[0]['perm_phone_no'])){
                    echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['perm_phone_no']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }?>
                      </span></td>
                  </tr>
                  <tr>
                    <td valign="top" class="label_name_new"><label for="visa_application_profession">Profession</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php

                  if(!empty ($visa_application[0]['profession'])){
                       echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['profession']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }?>
                      </span></td>
                  </tr>
                  </table></td>
                <td valign="top"><table width="347" border="0" cellspacing="0" cellpadding="0" class="no_border_new">
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_VisaOfficeAddressForm_address_1">Address 1</label>
                    </td>
                    <td width="175" valign="top" class="lblfield_new"><span class="lblField">
                      <?php
                  if(!empty ($visa_application[0]['VisaOfficeAddress']['address_1'])){
                    echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['VisaOfficeAddress']['address_1']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_VisaOfficeAddressForm_address_2">Address 2</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php
                  if(!empty ($visa_application[0]['VisaOfficeAddress']['address_1'])){
                    echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['VisaOfficeAddress']['address_1']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_VisaOfficeAddressForm_city">City</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php if(!empty ($visa_application[0]['VisaOfficeAddress']['city'])){
                    echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['VisaOfficeAddress']['city']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_VisaOfficeAddressForm_country_id">Country</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php

                  if(!empty ($visa_application[0]['VisaOfficeAddress']['Country']['country_name'])){
                    echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['VisaOfficeAddress']['Country']['country_name']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_VisaOfficeAddressForm_state">State</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php
                  if(!empty ($visa_application[0]['VisaOfficeAddress']['state'])){
                    echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['VisaOfficeAddress']['state']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }?>
                      </span></td>
                  </tr>
                  <tr>
                    <td valign="top" class="label_name_new"><label for="visa_application_VisaOfficeAddressForm_postcode">Postcode</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php
                  if(!empty ($visa_application[0]['VisaOfficeAddress']['postcode'])){
                    echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['VisaOfficeAddress']['postcode']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='' readonly = 'readonly' >";
                  }?>
                      </span></td>
                  </tr>
                  <tr>
                    <td valign="top" class="label_name_new"><label for="visa_application_office_phone_no">Office Phone</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php
                  if(!empty ($visa_application[0]['office_phone_no'])){
                    echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['office_phone_no']."'>";
                  }
                  else
                  {
                     echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }?>
                      </span></td>
                  </tr>
                  </table></td>
              </tr>
                  <tr>
                <td valign="top" height="5"></td>
                <td valign="top" height="5"></td>
              </tr>
              <tr>
                <td height="25" valign="middle" id="form_new_heading">If you have served in the military,please state</td>
                <td valign="top">&nbsp;</td>
              </tr>
              <tr>
                <td valign="top" height="5"></td>
                <td valign="top" height="5"></td>
              </tr>
              <tr>
                <td valign="top"><table width="347" border="0" cellspacing="0" cellpadding="0" class="no_border_new">
                    <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_milltary_in">In</label>
                    </td>
                    <td width="175" valign="top" class="lblfield_new"><span class="lblField">
                      <?php
                  if(!empty ($visa_application[0]['milltary_in'])){
                  echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['milltary_in']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_military_dt_from">From Date (dd-mm-yyyy)</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php
                  if(!empty ($visa_application[0]['military_dt_from'])){
                    $explode = explode('-',getDateFormate($visa_application[0]['military_dt_from']));
                    echo "<input type='text' class='printdate' readonly=readonly value='".$explode[0]."'>";
                    echo "<input type='text' class='printdate' readonly=readonly value='".$explode[1]."'>";
                    echo "<input type='text' class='printyear' readonly=readonly value='".$explode[2]."'>";
                  }
                  else
                  {
                    echo "<input type='text' class='printdate' readonly=readonly value='--'>";
                    echo "<input type='text' class='printdate' readonly=readonly value='--'>";
                    echo "<input type='text' class='printyear' readonly=readonly value='--'>";
                  }?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_military_dt_to">To Date (dd-mm-yyyy)</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php
                  if(!empty ($visa_application[0]['military_dt_to']))
                  {
                    $explode = explode('-',getDateFormate($visa_application[0]['military_dt_to']));
                   echo "<input type='text' class='printdate' readonly=readonly value='".$explode[0]."'>";
                    echo "<input type='text' class='printdate' readonly=readonly value='".$explode[1]."'>";
                    echo "<input type='text' class='printyear' readonly=readonly value='".$explode[2]."'>";
                  }
                  else
                  {
                    echo "<input type='text' class='printdate' readonly=readonly value='--'>";
                    echo "<input type='text' class='printdate' readonly=readonly value='--'>";
                    echo "<input type='text' class='printyear' readonly=readonly value='--'>";
                  }?>
                      </span></td>
                  </tr>
                  </table></td>
                <td valign="top">&nbsp;</td>
              </tr>
                  <tr>
                <td valign="top" height="5"></td>
                <td valign="top" height="5"></td>
              </tr>
            </table></td>
        </tr>
        <tr>
          <td valign="top" id="uiGroup_Step_2_of_4"><table cellpadding="0" cellspacing="0" border="0" class="no_border_new" width="695">
              <tr>
                <td width="347" height="25" valign="middle" id="form_new_heading">Passport Information</td>
                <td  height="25" rowspan="39" align="center" valign="top" id="border_new_left"></td>
                <td width="347" height="25" valign="middle" id="form_new_heading">Visa Processing Information</td>
              </tr>
              <tr>
                <td valign="top" height="5"></td>
                <td valign="top" height="5"></td>
              </tr>
              <tr>
                <td valign="top"><table width="347" border="0" cellspacing="0" cellpadding="0" class="no_border_new">
                    <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_ApplicantInfo_issusing_govt">Issuing Country</label>
                    </td>
                    <td width="175" valign="top" class="lblfield_new"><span class="lblField">
                        <?php if(!empty ($visa_application[0]['VisaApplicantInfo']['issusing_govt'])){echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['VisaApplicantInfo']['issusing_govt']."'>";
                  }else{
                echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                }?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_ApplicantInfo_passport_number">Passport Number</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php if(!empty ($visa_application[0]['VisaApplicantInfo']['passport_number'])){
                    echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['VisaApplicantInfo']['passport_number']."'>";
                  }else{


                echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                } ?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_ApplicantInfo_date_of_issue">Date of Issue (dd-mm-yyyy)</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php if(!empty ($visa_application[0]['VisaApplicantInfo']['date_of_issue']))
                  {

                    $explode = explode('-',getDateFormate($visa_application[0]['VisaApplicantInfo']['date_of_issue']));
                    echo "<input type='text' class='printdate' readonly=readonly value='".$explode[0]."'>";
                    echo "<input type='text' class='printdate' readonly=readonly value='".$explode[1]."'>";
                    echo "<input type='text' class='printyear' readonly=readonly value='".$explode[2]."'>";

                }else{
                    echo "<input type='text' class='printdate' readonly=readonly value='--'>";
                    echo "<input type='text' class='printdate' readonly=readonly value='--'>";
                    echo "<input type='text' class='printyear' readonly=readonly value='--'>";
                } ?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_ApplicantInfo_date_of_exp">Expiry Date (dd-mm-yyyy)</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php if(!empty ($visa_application[0]['VisaApplicantInfo']['date_of_exp'])){

                    $explode = explode('-',getDateFormate($visa_application[0]['VisaApplicantInfo']['date_of_exp']));
                    echo "<input type='text' class='printdate' readonly=readonly value='".$explode[0]."'>";
                    echo "<input type='text' class='printdate' readonly=readonly value='".$explode[1]."'>";
                    echo "<input type='text' class='printyear' readonly=readonly value='".$explode[2]."'>";
                  }else{
                    echo "<input type='text' class='printdate' readonly=readonly value='--'>";
                    echo "<input type='text' class='printdate' readonly=readonly value='--'>";
                    echo "<input type='text' class='printyear' readonly=readonly value='--'>";
                  } ?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_ApplicantInfo_place_of_issue">Place of Issue</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                        <?php if(!empty ($visa_application[0]['VisaApplicantInfo']['place_of_issue'])){echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['VisaApplicantInfo']['place_of_issue']."'>";}else{
                echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                } ?>
                      </span></td>
                  </tr>
                  </table></td>
                <td valign="top"><table width="347" border="0" cellspacing="0" cellpadding="0" class="no_border_new">
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_ApplicantInfo_visatype_id">Type of Visa Required</label>
                    </td>
                    <td width="175" valign="top" class="lblfield_new"><div class="lblField_radio">
                        <?php if(!empty ($visa_application[0]['VisaApplicantInfo']['VisaTypeId']['var_value'])){

                  $chkFlg = '';
                  foreach($visaType as $val){
                    if($val['id'] == $visa_application[0]['VisaApplicantInfo']['VisaTypeId']['id']){
                      echo "<input type='radio' value='".$val['id']."'' checked disabled class='align-right' >&nbsp;".$val['var_value'] ."<br />";
                    }else{
                      echo "<input type='radio' value='".$val['id']."''  disabled class='align-right'>&nbsp;".$val['var_value'] ."<br />";
                    }
                  }

                  }
                  else{
                    echo "--";
                  } ?>
                      </div></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_ApplicantInfo_applying_country_id">Country Applying From</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                        <?php if(!empty ($visa_application[0]['VisaApplicantInfo']['applying_country_id'])){echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['applying_country']."'>";}else{
                echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                } ?>
                      <div class="clear"></div>
                      <?php if(!empty($visa_application[0]['VisaApplicantInfo']['authority_id'])){ ?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_ApplicantInfo_licenced_freezone">While in Nigeria, do you intend working in a licenced Free Zone Enterprise/Company?</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php
                    if(!empty($visa_application[0]['VisaApplicantInfo']['authority_id'])){
                        echo "<input type='text' readonly=readonly value='Yes' class='print_input_box'>";
                    }else{
                      echo "<input type='text' readonly=readonly value='No' class='print_input_box'>";
                    }

                    ?>
                      </span>
                      <div class="clear"></div></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"></td>
                    <td valign="top" class="lblfield_new"><span class="lblField"> </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_ApplicantInfo_authority_id">If <b>Yes</b>, Choose your target free zone Authority</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php
                    if(!empty($visa_application[0]['VisaApplicantInfo']['authority_id'])){
                    echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['VisaApplicantInfo']['VisaProcessingCentre']['centre_name']."'>";
                    }else{
                    echo "<input type='text' readonly=readonly value='--'>";
                    } ?>
                      <?php }?>
                      <div class="clear"></div>
                      </span></td>
                  </tr>
                  </table></td>
              </tr>
                  <tr>
                <td valign="top" height="5"></td>
                <td valign="top" height="5"></td>
              </tr>
              <tr>
                <td height="25" valign="middle" id="form_new_heading">Visa Processing Information</td>
                <td height="25" valign="middle" id="form_new_heading">Visa Processing Information</td>
              </tr>
              <tr>
                <td valign="top" height="5"></td>
                <td valign="top" height="5"></td>
              </tr>
              <tr>
                <td valign="top"><table width="347" border="0" cellspacing="0" cellpadding="0" class="no_border_new">
                    <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_ApplicantInfo_embassy_of_pref_id">Embassy of Preference <br/>
                      (Where applicable)</label>
                    </td>
                    <td width="175" valign="top" class="lblfield_new"><span class="lblField">
                        <?php if(!empty ($visa_application[0]['VisaApplicantInfo']['EmbassyMaster']['embassy_name'])){echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['VisaApplicantInfo']['EmbassyMaster']['embassy_name']."'>";}else{
                echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                } ?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_ApplicantInfo_purpose_of_journey">Purpose of Journey</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php if(!empty ($visa_application[0]['VisaApplicantInfo']['purpose_of_journey'])){echo "<textarea style='' readonly=readonly>".$visa_application[0]['VisaApplicantInfo']['purpose_of_journey']."</textarea>";}else{
                echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                } ?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_ApplicantInfo_entry_type_id">Number of entries required</label>
                    </td>
                    <td valign="top" class="lblfield_new"><div class="lblField_radio">
                        <?php if(!empty ($visa_application[0]['VisaApplicantInfo']['EntryType']['var_value'])){
                    $multiFlg='';
                    $singleFlg = '';
                    if($visa_application[0]['VisaApplicantInfo']['EntryType']['var_value'] == "Single"){
                      $singleFlg = "checked";
                    }else if($visa_application[0]['VisaApplicantInfo']['EntryType']['var_value'] == "Multiple"){
                      $multiFlg = "checked";
                    }
                    echo "<input type='radio' disabled $singleFlg  class='align-right'><lable> Single </lable><br />";echo "<input type='radio' disabled $multiFlg class='align-right'><lable> Multiple </lable>";
                  }else{
                    echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  } ?>
                        <?php if($visa_application[0]['VisaApplicantInfo']['EntryType']['var_value'] == "Multiple") {?>
                      </div></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_ApplicantInfo_no_of_re_entry_type">No. of Entries<sup></sup></label>
                    </td>
                    <td width="175" valign="top" class="lblfield_new"><span class="lblField">
                      <?php if(!empty ($visa_application[0]['VisaApplicantInfo']['no_of_re_entry_type'])){echo "<input  class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['VisaApplicantInfo']['no_of_re_entry_type']."'>";}else{
                echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                } ?>
                      <?php } ?>
                      </span></td>
                  </tr>
                  </table></td>
                <td valign="top"><table width="347" border="0" cellspacing="0" cellpadding="0" class="no_border_new">
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_ApplicantInfo_money_in_hand">How much money do you <br/>
                      have for this trip (USD)</label>
                    </td>
                    <td width="175" valign="top" class="lblfield_new"><span class="lblField">
                        <?php if(!empty ($visa_application[0]['VisaApplicantInfo']['money_in_hand'])){echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['VisaApplicantInfo']['money_in_hand']."'>";}else{
                echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                } ?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_ApplicantInfo_stay_duration_days">Intended Duration of Stay (in days)</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                        <?php if(!empty ($visa_application[0]['VisaApplicantInfo']['stay_duration_days'])){echo "<input class='print_input_box'type='text' readonly=readonly value='".$visa_application[0]['VisaApplicantInfo']['stay_duration_days']."'>";}else{
                echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                } ?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_ApplicantInfo_proposed_date_of_travel">Proposed date of travel(dd-mm-yyyy)</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php if(!empty ($visa_application[0]['VisaApplicantInfo']['proposed_date_of_travel']))
                  {
                    $explode = explode('-',getDateFormate($visa_application[0]['VisaApplicantInfo']['proposed_date_of_travel']));
                    echo "<input type='text' class='printdate' readonly=readonly value='".$explode[0]."'>";
                      echo "<input type='text' class='printdate' readonly=readonly value='".$explode[1]."'>";
                      echo "<input type='text' class='printyear' readonly=readonly value='".$explode[2]."'>";
                  }else{
                    echo "<input type='text' class='printdate' readonly=readonly value='--'>";
                    echo "<input type='text' class='printdate' readonly=readonly value='--'>";
                    echo "<input type='text' class='printyear' readonly=readonly value='--'>";
                  } ?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_ApplicantInfo_mode_of_travel">Mode of travel to Nigeria</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php if(!empty ($visa_application[0]['VisaApplicantInfo']['mode_of_travel'])){
                    echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['VisaApplicantInfo']['mode_of_travel']."'>";
                  }else{echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";} ?>
                      </span></td>
                  </tr>
                  </table></td>
              </tr>
                  <tr>
                <td valign="top" height="5"></td>
                <td valign="top" height="5"></td>
                  </tr>
                  <tr>
                <td height="25" valign="middle" id="form_new_heading">If the purpose of your journey to Nigeria is for employment,state</td>
                <td height="25" valign="middle"></td>
                  </tr>
                  <tr>
                <td valign="top" height="5"></td>
                <td valign="top" height="5"></td>
                  </tr>
                  <tr>
                <td valign="top"><table width="347" border="0" cellspacing="0" cellpadding="0" class="no_border_new">
                    <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_VisaDetails_relative_employer_name">Name of Employer</label>
                    </td>
                    <td width="175" valign="top" class="lblfield_new"><span class="lblField">
                      <?php if(!empty ($visa_application[0]['VisaApplicationDetails']['relative_employer_name'])){
                      echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['VisaApplicationDetails']['employer_name']."'>";
                    }else{echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";} ?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_VisaDetails_position_occupied">Position to be occupied</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php if(!empty ($visa_application[0]['VisaApplicationDetails']['position_occupied'])){
                      echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['VisaApplicationDetails']['position_occupied']."'>";
                    }else{echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";} ?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_VisaDetails_job_description">Full description of job<br/>
                      in Nigeria (in months)</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php if(!empty ($visa_application[0]['VisaApplicationDetails']['job_description'])){
                      echo "<textarea style='' readonly=readonly>".$visa_application[0]['VisaApplicationDetails']['job_description']."</textarea>";
                    }else{echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";} ?>
                      </span></td>
                  </tr>
                  </table></td>
                <td valign="top">&nbsp;</td>
              </tr>
                  <tr>
                    <td valign="top">&nbsp;</td>
                    <td valign="top">&nbsp;</td>
                  </tr>
                  <tr>
                    <td height="25" valign="middle" id="form_new_heading">Give particulars of the employment of parents, spouse in Nigeria(if applicable)</td>
                    <td height="25" valign="middle" id="form_new_heading">Employer's Address</td>
                  </tr>
                  <tr>
                    <td height="5" valign="top"></td>
                    <td valign="top"></td>
                  </tr>
                  <tr>
                    <td valign="top"><table width="347" border="0" cellspacing="0" cellpadding="0" class="no_border_new">
                      <tr>
                        <td width="175" valign="top" class="label_name_new"><label for="visa_application_VisaDetails_relative_employer_name">Name of Employer</label></td>
                        <td width="175" valign="top" class="lblfield_new"><span class="lblField">
                      <?php if(!empty ($visa_application[0]['VisaApplicationDetails']['relative_employer_name'])){
                      echo "<input type='text' class='print_input_box' readonly=readonly value='".$visa_application[0]['VisaApplicationDetails']['relative_employer_name']."'>";
                    }else{echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";} ?>
                      </span></td>
                      </tr>
                      <tr>
                        <td width="175" valign="top" class="label_name_new"><label for="visa_application_VisaDetails_relative_employer_phone">Phone number of Employer</label></td>
                        <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php if(!empty ($visa_application[0]['VisaApplicationDetails']['relative_employer_phone'])){
                      echo "<input type='text'  class='print_input_box' readonly=readonly value='".$visa_application[0]['VisaApplicationDetails']['relative_employer_phone']."'>";
                    }else{echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";} ?>
                      </span></td>
                      </tr>
                      <tr>
                        <td width="175" valign="top" class="label_name_new"><label for="visa_application_VisaDetails_relative_nigeria_leaving_mth">How long have your parents/spouse been
                      in Nigeria (in months)</label></td>
                        <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php if(!empty ($visa_application[0]['VisaApplicationDetails']['relative_nigeria_leaving_mth'])){
                      echo "<input type='text' class='print_input_box' readonly=readonly value='".$visa_application[0]['VisaApplicationDetails']['relative_nigeria_leaving_mth']."'>";
                    }else{echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";} ?>
                      </span></td>
                      </tr>
                      <div class="custom_height"></div>
                    </table></td>
                    <td valign="top"><table width="347" border="0" cellspacing="0" cellpadding="0" class="no_border_new">
                      <tr>
                        <td width="175" valign="top" class="label_name_new"><label for="visa_application_VisaRelativeEmployerAddress_address_1">Address 1</label></td>
                        <td width="175" valign="top" class="lblfield_new"><span class="lblField">
                          <?php if(!empty ($visa_application[0]['VisaApplicationDetails']['VisaRelativeEmployerAddress']['address_1'])){
                      echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['VisaApplicationDetails']['VisaRelativeEmployerAddress']['address_1']."'>";
                    }else{echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";} ?>
                        </span></td>
                      </tr>
                      <tr>
                        <td width="175" valign="top" class="label_name_new"><label for="visa_application_VisaRelativeEmployerAddress_address_2">Address 2</label></td>
                        <td valign="top" class="lblfield_new"><span class="lblField">
                          <?php if(!empty ($visa_application[0]['VisaApplicationDetails']['VisaRelativeEmployerAddress']['address_2'])){
                      echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['VisaApplicationDetails']['VisaRelativeEmployerAddress']['address_2']."'>";
                    }else{echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";} ?>
                        </span></td>
                      </tr>
                      <tr>
                        <td width="175" valign="top" class="label_name_new"><label for="visa_application_VisaRelativeEmployerAddress_city">City</label></td>
                        <td valign="top" class="lblfield_new"><span class="lblField">
                          <?php if(!empty ($visa_application[0]['VisaApplicationDetails']['VisaRelativeEmployerAddress']['city'])){
                      echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['VisaApplicationDetails']['VisaRelativeEmployerAddress']['city']."'>";
                    }else{echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";} ?>
                        </span></td>
                      </tr>
                      <tr>
                        <td width="175" valign="top" class="label_name_new"><label for="visa_application_VisaRelativeEmployerAddress_country_id">Country</label></td>
                        <td valign="top" class="lblfield_new"><span class="lblField">
                          <?php if(!empty ($visa_application[0]['VisaApplicationDetails']['VisaRelativeEmployerAddress']['country_id'])){
                      echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['VisaApplicationDetails']['VisaRelativeEmployerAddress']['Country']['country_name']."'>";
                    }else{echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";} ?>
                        </span></td>
                      </tr>
                      <div class="custom_height"></div>
                      <tr>
                        <td width="175" valign="top" class="label_name_new"><label for="visa_application_VisaRelativeEmployerAddress_state">State</label></td>
                        <td valign="top" class="lblfield_new"><span class="lblField">
                          <?php if(!empty ($visa_application[0]['VisaApplicationDetails']['VisaRelativeEmployerAddress']['state'])){
                      echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['VisaApplicationDetails']['VisaRelativeEmployerAddress']['state']."'>";
                    }else{echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";} ?>
                        </span></td>
                      </tr>
                      <tr>
                        <td width="175" valign="top" class="label_name_new"><label for="visa_application_VisaRelativeEmployerAddress_postcode">Postcode</label></td>
                        <td valign="top" class="lblfield_new"><span class="lblField">
                          <?php if(!empty ($visa_application[0]['VisaApplicationDetails']['VisaRelativeEmployerAddress']['postcode'])){
                      echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['VisaApplicationDetails']['VisaRelativeEmployerAddress']['postcode']."'>";
                    }else{echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";} ?>
                        </span></td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                <td valign="top">&nbsp;</td>
                <td valign="top">&nbsp;</td>
              </tr>
              <tr>
                <td height="25" valign="middle" id="form_new_heading">Intended address in Nigeria</td>
                <td valign="top" height="5"></td>
              </tr>
              <tr>
                <td valign="top"><table width="347" border="0" cellspacing="0" cellpadding="0" class="no_border_new">
                    <tr>
                      <td colspan="2" valign="top" height="5"></td>
                    </tr>
                    <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_VisaIntendedAddressNigeriaAddress_address_1">Address 1</label>
                    </td>
                    <td width="175" valign="top" class="lblfield_new"><span class="lblField">
                      <?php if(!empty ($visa_application[0]['VisaApplicationDetails']['VisaIntendedAddressNigeriaAddress']['address_1'])){
                      echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['VisaApplicationDetails']['VisaIntendedAddressNigeriaAddress']['address_1']."'>";
                    }else{echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";} ?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_VisaIntendedAddressNigeriaAddress_address_2">Address 2</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php if(!empty ($visa_application[0]['VisaApplicationDetails']['VisaIntendedAddressNigeriaAddress']['address_2'])){
                      echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['VisaApplicationDetails']['VisaIntendedAddressNigeriaAddress']['address_2']."'>";
                    }else{echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";} ?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_VisaIntendedAddressNigeriaAddress_city">City</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php if(!empty ($visa_application[0]['VisaApplicationDetails']['VisaIntendedAddressNigeriaAddress']['city'])){
                      echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['VisaApplicationDetails']['VisaIntendedAddressNigeriaAddress']['city']."'>";
                    }else{echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";} ?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_VisaIntendedAddressNigeriaAddress_country_id">Country </label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php if(!empty ($visa_application[0]['VisaApplicationDetails']['VisaIntendedAddressNigeriaAddress']['country_id'])){
                     if($visa_application[0]['VisaApplicationDetails']['VisaIntendedAddressNigeriaAddress']['country_id'] == 'NG'){
                       echo "<input class='print_input_box' type='text' readonly=readonly value='Nigeria'>";
                     }
                    }
                    else
                    {echo "<input type='text' class='print_input_box' value='--' readonly = 'readonly' >";} ?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_VisaIntendedAddressNigeriaAddress_state">State</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php if(!empty ($visa_application[0]['VisaApplicationDetails']['VisaIntendedAddressNigeriaAddress']['state'])){
                      echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['VisaApplicationDetails']['VisaIntendedAddressNigeriaAddress']['State']['state_name']."'>";
                    }else{echo "<input type='text' class='print_input_box' value='--' readonly = 'readonly' >";} ?>
                      </span></td>
                  </tr>
                  </table></td>
                <td valign="top"><table width="347" border="0" cellspacing="0" cellpadding="0" class="no_border_new">
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_VisaIntendedAddressNigeriaAddress_lga_id">LGA</label>
                    </td>
                    <td width="175" valign="top" class="lblfield_new"><span class="lblField">
                      <?php if(!empty ($visa_application[0]['VisaApplicationDetails']['VisaIntendedAddressNigeriaAddress']['lga_id'])){
                      echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['VisaApplicationDetails']['VisaIntendedAddressNigeriaAddress']['LGA']['lga']."'>";
                    }else{echo "<input type='text' class='print_input_box' value='--' readonly = 'readonly' >";} ?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_VisaIntendedAddressNigeriaAddress_district">District</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php if(!empty ($visa_application[0]['VisaApplicationDetails']['VisaIntendedAddressNigeriaAddress']['district'])){
                      echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['VisaApplicationDetails']['VisaIntendedAddressNigeriaAddress']['district']."'>";
                    }else{echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";} ?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_VisaIntendedAddressNigeriaAddress_postcode">Postcode</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php if(!empty ($visa_application[0]['VisaApplicationDetails']['VisaIntendedAddressNigeriaAddress']['postcode'])){
                      echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['VisaApplicationDetails']['VisaIntendedAddressNigeriaAddress']['postcode']."'>";
                    }else{echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";} ?>
                      </span></td>
                  </tr>
                  </table></td>
              </tr>
                  <tr>
                <td valign="top" height="5"></td>
                <td valign="top" height="5"></td>
              </tr>
            </table></td>
        </tr>
        <tr>
          <td valign="top" id="uiGroup_Step_3_of_4"><table cellpadding="0" cellspacing="0" border="0" class="no_border_new" width="695">
              <tr>
                <td width="347" height="25" valign="middle" id="form_new_heading">Previous Application Information</td>
                <td  rowspan="32" align="center" valign="top" id="border_new_left"></td>
                <td width="347" height="25" valign="middle" id="form_new_heading">&nbsp;</td>
              </tr>
              <tr>
                <td valign="top" height="5"></td>
                <td valign="top" height="5"></td>
              </tr>
              <tr>
                <td valign="top"><table width="347" border="0" cellspacing="0" cellpadding="0" class="no_border_new">
                    <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_ApplicantInfo_applied_nigeria_visa">Have you ever applied for Nigerian Visa?</label>
                    </td>
                    <td width="175" valign="top" class="lblfield_new"><span class="lblField">
                      <?php if(!empty ($visa_application[0]['VisaApplicantInfo']['applied_nigeria_visa'])){
                      echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['VisaApplicantInfo']['applied_nigeria_visa']."'>";
                    }else{echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";} ?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_ApplicantInfo_nigeria_visa_applied_place">If Yes, where did you apply <br />
                      for the Visa?</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php if(!empty ($visa_application[0]['VisaApplicantInfo']['nigeria_visa_applied_place'])){
                      echo "<textarea class='print_input_box' readonly= readonly class='auto-height'>".$visa_application[0]['VisaApplicantInfo']['nigeria_visa_applied_place']."</textarea>";
                    }else{echo "<textarea readonly= readonly class='auto-height'></textarea>";} ?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_ApplicantInfo_applied_nigeria_visa_status">Was the Visa Granted or Rejected?</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php if(!empty ($visa_application[0]['VisaApplicantInfo']['applied_nigeria_visa_status'])){
                      echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['VisaApplicantInfo']['applied_nigeria_visa_status']."'>";
                    }else{echo  "<input type='text' class='print_input_box' value='--' readonly=readonly>";} ?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_ApplicantInfo_applied_nigeria_visa_reject_reason">If Rejected, please provide reason</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php if(!empty ($visa_application[0]['VisaApplicantInfo']['applied_nigeria_visa_reject_reason'])){
                      echo "<textarea class='print_input_box' readonly= readonly class='auto-height'>".$visa_application[0]['VisaApplicantInfo']['applied_nigeria_visa_reject_reason']."</textarea>";
                    }else{echo "<textarea readonly= readonly class='auto-height'></textarea>";} ?>
                      </span></td>
                  </tr>
                  </table></td>
                <td valign="top"><table width="347" border="0" cellspacing="0" cellpadding="0" class="no_border_new">
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_ApplicantInfo_have_visited_nigeria">Have you ever visited <br/>
                      Nigeria?</label>
                    </td>
                    <td width="175" valign="top" class="lblfield_new"><span class="lblField">
                      <?php if(!empty ($visa_application[0]['VisaApplicantInfo']['have_visited_nigeria'])){
                      echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['VisaApplicantInfo']['have_visited_nigeria']."'>";
                    }else{echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";} ?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_ApplicantInfo_visited_reason_type_id">If Yes, for what reason</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php if(!empty ($visa_application[0]['VisaApplicantInfo']['visited_reason_type_id'])){
                      echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['VisaApplicantInfo']['VisaTypeReason']['var_value']."'>";
                    }else{echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";} ?>
                      </span></td>
                  </tr>
                  </table></td>
              </tr>
              <tr>
                <td valign="top" height="5"></td>
                <td valign="top" height="5"></td>
              </tr>
              <tr>
                <td valign="top" id="form_new_heading">State the period of previous visits to Nigeria and address at which you stayed </td>
              <?php if(count($applicantPreviousNigeriaHistory) > 0){

              $i = 1;
              foreach($applicantPreviousNigeriaHistory as $val){
                ?>
              <?php if($i == 3){?>
              <!-- <div class="pagebreak"></div> -->
              <?php } ?>
                
              </tr>
                  <tr>
                <td valign="top" height="5"></td>
                <td valign="top" height="5"></td>
              </tr>
              <tr>
                <td height="25" valign="middle" id="form_new_heading">Period <?php echo $i;?></td>
                <td height="25" valign="middle" id="form_new_heading">Address</td>
              </tr>
              <tr>
                <td valign="top" height="5"></td>
                <td valign="top" height="5"></td>
              </tr>
              <tr>
                <td valign="top"><table width="347" border="0" cellspacing="0" cellpadding="0" class="no_border_new">
                    <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_PreviousHistory0_startdate">From(dd-mm-yyyy)</label>
                    </td>
                    <td width="175" valign="top" class="lblfield_new"><span class="lblField">
                      <?php if(!empty ($val['startdate'])){

                      $explode = explode('-',getDateFormate($val['startdate']));
                      echo "<input type='text' class='printdate' readonly=readonly value='".$explode[0]."'>";
                  echo "<input type='text' class='printdate' readonly=readonly value='".$explode[1]."'>";
                  echo "<input type='text' class='printyear' readonly=readonly value='".$explode[2]."'>";
                    }else{
                      echo "<input type='text' class='printdate' readonly=readonly value='--'>";
                    echo "<input type='text' class='printdate' readonly=readonly value='--'>";
                    echo "<input type='text' class='printyear' readonly=readonly value='--'>";} ?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_PreviousHistory0_endate">To(dd-mm-yyyy)</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php if(!empty ($val['endate'])){
                      $explode = explode('-',getDateFormate($val['endate']));
                      echo "<input type='text' class='printdate' readonly=readonly value='".$explode[0]."'>";
                  echo "<input type='text' class='printdate' readonly=readonly value='".$explode[1]."'>";
                  echo "<input type='text' class='printyear' readonly=readonly value='".$explode[2]."'>";
                    }else{echo "<input type='text' class='printdate' readonly=readonly value='--'>";
                    echo "<input type='text' class='printdate' readonly=readonly value='--'>";
                    echo "<input type='text' class='printyear' readonly=readonly value='--'>";} ?>
                      </span></td>
                  </tr>
                  </table></td>
                <td valign="top"><table width="347" border="0" cellspacing="0" cellpadding="0" class="no_border_new">
                    <tr>
                      <td width="175" valign="top" class="label_name_new"><label for="visa_application_VisaApplicantPreviousHistoryAddress0_address_1">Address 1</label>
                      </td>
                      <td width="175" valign="top" class="lblfield_new"><span class="lblField">
                      <?php if(!empty ($val['VisaApplicantPreviousHistoryAddress']['address_1'])){
                      echo "<input class='print_input_box' type='text' readonly=readonly value='".$val['VisaApplicantPreviousHistoryAddress']['address_1']."'>";
                    }else{echo "<input type='text' class='print_input_box'  value='--' readonly=readonly>";} ?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_VisaApplicantPreviousHistoryAddress0_address_2">Address 2</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php if(!empty ($val['VisaApplicantPreviousHistoryAddress']['address_2'])){
                      echo "<input class='print_input_box' type='text' readonly=readonly value='".$val['VisaApplicantPreviousHistoryAddress']['address_2']."'>";
                    }else{echo "<input type='text' class='print_input_box'  value='--' readonly=readonly>";} ?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"></td>
                    <td valign="top" class="lblfield_new"><span class="lblField"> </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_VisaApplicantPreviousHistoryAddress0_city">City</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php if(!empty ($val['VisaApplicantPreviousHistoryAddress']['city'])){
                      echo "<input class='print_input_box' type='text' readonly=readonly value='".$val['VisaApplicantPreviousHistoryAddress']['city']."'>";
                    }else{echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";} ?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_VisaApplicantPreviousHistoryAddress0_country_id">Country</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php if(!empty ($val['VisaApplicantPreviousHistoryAddress']['country_id'])){
                      echo "<input class='print_input_box' type='text' readonly=readonly value='Nigeria'>";
                    }else{echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";} ?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_VisaApplicantPreviousHistoryAddress0_state">State</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php if(!empty ($val['VisaApplicantPreviousHistoryAddress']['state'])){
                      echo "<input class='print_input_box' type='text' readonly=readonly value='".$val['VisaApplicantPreviousHistoryAddress']['State']['state_name']."'>";
                    }else{echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";} ?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_VisaApplicantPreviousHistoryAddress0_lga_id">LGA</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php if(!empty ($val['VisaApplicantPreviousHistoryAddress']['lga_id'])){
                      echo "<input class='print_input_box' type='text' readonly=readonly value='".$val['VisaApplicantPreviousHistoryAddress']['LGA']['lga']."'>";
                    }else{echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";} ?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_VisaApplicantPreviousHistoryAddress0_district">District</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php if(!empty ($val['VisaApplicantPreviousHistoryAddress']['district'])){
                      echo "<input class='print_input_box' type='text' readonly=readonly value='".$val['VisaApplicantPreviousHistoryAddress']['district']."'>";
                    }else{echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";} ?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_VisaApplicantPreviousHistoryAddress0_postcode">Postcode</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php if(!empty ($val['VisaApplicantPreviousHistoryAddress']['postcode'])){
                      echo "<input class='print_input_box' type='text' readonly=readonly value='".$val['VisaApplicantPreviousHistoryAddress']['postcode']."'>";
                    }else{echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";} ?>
                      </span></td>
                  </tr>
                </table>
              <?php $i++;}
              }else{?></td>
              </tr>
                  <tr>
                <td valign="top" height="5"></td>
                <td valign="top" height="5"></td>
              </tr>
              <tr>
                <td height="25" valign="middle" id="form_new_heading">Period 1 </td>
                <td height="25" valign="middle" id="form_new_heading">Address</td>
              </tr>
              <tr>
                <td valign="top"><table width="347" border="0" cellspacing="0" cellpadding="0" class="no_border_new">
                    <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_PreviousHistory1_startdate">From(dd-mm-yyyy)</label>
                    </td>
                      <td width="175" valign="top" class="lblfield_new"><span class="lblField"> <?php echo "<input type='text' class='printdate' readonly=readonly value='--'>";
                    echo "<input type='text' class='printdate' readonly=readonly value='--'>";
                    echo "<input type='text' class='printyear' readonly=readonly value='--'>"; ?> </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_PreviousHistory1_endate">To(dd-mm-yyyy)</label>
                    </td>
                      <td valign="top" class="lblfield_new"><span class="lblField"> <?php echo "<input type='text' class='printdate' readonly=readonly value='--'>";
                    echo "<input type='text' class='printdate' readonly=readonly value='--'>";
                    echo "<input type='text' class='printyear' readonly=readonly value='--'>"; ?> </span></td>
                  </tr>
                  </table></td>
                <td valign="top"><table width="347" border="0" cellspacing="0" cellpadding="0" class="no_border_new">
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_VisaApplicantPreviousHistoryAddress1_address_1">Address 1</label>
                    </td>
                    <td width="175" valign="top" class="lblfield_new"><span class="lblField"> -- </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_VisaApplicantPreviousHistoryAddress1_address_2">Address 2</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField"> -- </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_VisaApplicantPreviousHistoryAddress1_city">City</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField"> -- </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_VisaApplicantPreviousHistoryAddress1_country_id">Country</label>
                    </td>
                      <td valign="top" class="lblfield_new"><span class="lblField"> <?php echo "<input type='text' class='print_input_box' value='--' readonly=readonly>"; ?> </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_VisaApplicantPreviousHistoryAddress1_state">State</label>
                    </td>
                      <td valign="top" class="lblfield_new"><span class="lblField"> <?php echo "<input type='text' class='print_input_box' value='--' readonly=readonly>"; ?> </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_VisaApplicantPreviousHistoryAddress1_lga_id">LGA</label>
                    </td>
                      <td valign="top" class="lblfield_new"><span class="lblField"> <?php echo "<input type='text' class='print_input_box' value='--' readonly=readonly>"; ?> </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_VisaApplicantPreviousHistoryAddress1_district">District</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                        <input name="Input" type="" readonly="readonly" />
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_VisaApplicantPreviousHistoryAddress1_postcode">Postcode</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                        <input name="Input2" type="" readonly="readonly" />
                      </span></td>
                  </tr>
                </table>
                  <?php }?></td>
              </tr>
                  <tr>
                <td valign="top" height="5"></td>
                <td valign="top" height="5"></td>
              </tr>
            </table></td>
        </tr>
        <tr>
          <td valign="top" id="uiGroup_Step_4_of_4"><table cellpadding="0" cellspacing="0" border="0" class="no_border_new" width="695">
              <tr>
                <td width="347" height="25" valign="middle" id="form_new_heading">Travel History</td>
                <td  rowspan="32" align="center" valign="top" id="border_new_left"></td>
                <td width="347" height="25" valign="middle" id="form_new_heading">&nbsp;</td>
              </tr>
              <tr>
                <td valign="top" height="5"></td>
                <td valign="top" height="5"></td>
              </tr>
              <tr>
                <td valign="top"><table width="347" border="0" cellspacing="0" cellpadding="0" class="no_border_new">
                    <tr>
                      <td width="175" valign="top" class="label_name_new"><div class="lblName">
                          <label for="visa_application_ApplicantInfo_applying_country_duration">How long have you lived in the country from where you are applying for visa (in Years)?</label>
                        </div></td>
                      <td width="175" valign="top" class="lblfield_new"><span class="lblField">
                        <?php if(!empty ($visa_application[0]['VisaApplicantInfo']['applying_country_duration'])){
                echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['VisaApplicantInfo']['applying_country_duration']."'>";
              }else{echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";} ?>
                        </span></td>
                    </tr>
                    <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_ApplicantInfo_contagious_disease">Have you ever been infected by any contagious disease (e.g. Tuberculosis) or suffered serious mental illness?</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php if(!empty ($visa_application[0]['VisaApplicantInfo']['contagious_disease'])){
                      echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['VisaApplicantInfo']['contagious_disease']."'>";
                    }else{echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";} ?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_ApplicantInfo_police_case">Have you ever been arrested or convicted for an offence (even though subject to pardon)?</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php if(!empty ($visa_application[0]['VisaApplicantInfo']['police_case'])){
                      echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['VisaApplicantInfo']['police_case']."'>";
                    }else{echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";} ?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_ApplicantInfo_narcotic_involvement">Have you ever been involved in narcotic activity?</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php if(!empty ($visa_application[0]['VisaApplicantInfo']['narcotic_involvement'])){
                      echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['VisaApplicantInfo']['narcotic_involvement']."'>";
                    }else{echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";} ?>
                      </span></td>
                  </tr>
                  </table></td>
                <td valign="top"><table width="347" border="0" cellspacing="0" cellpadding="0" class="no_border_new">
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_ApplicantInfo_deported_status">Have you ever been deported?</label>
                    </td>
                    <td width="175" valign="top" class="lblfield_new"><span class="lblField">
                      <?php if(!empty ($visa_application[0]['VisaApplicantInfo']['deported_status'])){
                      echo"<input class='print_input_box' type='text' readonly=readonly value='". $visa_application[0]['VisaApplicantInfo']['deported_status']."'>";
                    }else{echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";} ?>
                      <?php if($visa_application[0]['VisaApplicantInfo']['deported_status'] == "Yes"){ ?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_ApplicantInfo_deported_county_id">If you have ever been deported from which country?</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php if(!empty ($visa_application[0]['VisaApplicantInfo']['deported_county_id'])){
                      echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['deported_country']."'>";
                    }else{echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";} ?>
                      <?php } ?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_ApplicantInfo_visa_fraud_status">Have you sought to obtain<br/>
                      visa by mis-representation or fraud?</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php if(!empty ($visa_application[0]['VisaApplicantInfo']['visa_fraud_status'])){
                      echo "<input class='print_input_box' type='text' readonly=readonly value='".$visa_application[0]['VisaApplicantInfo']['visa_fraud_status']."'>";
                    }else{echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";} ?>
                      </span></td>
                  </tr>
                  </table></td>
              </tr>
                  <tr>
                <td valign="top" height="5"></td>
                <td valign="top" height="5"></td>
              </tr>
              <tr>
                <td height="25" valign="middle" id="form_new_heading">Give a list of the countries you have lived for more than one year </td>
                <td valign="top" height="25" id="form_new_heading">&nbsp;</td>
              </tr>
              <tr>
                <td valign="top" height="5"></td>
                <td valign="top" height="5"></td>
              </tr>
              <tr>
                <td height="25" valign="middle" id="form_new_heading">Period 1</td>
                <td height="25" valign="middle" id="form_new_heading">Period 2</td>
              </tr>
              <tr>
                <td valign="top" height="5"></td>
                <td valign="top" height="5"></td>
              </tr>
              <tr>
                <td valign="top"><table width="347" border="0" cellspacing="0" cellpadding="0" class="no_border_new">
                    <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_FiveYears0_country_id">Country</label>
                    </td>
                    <td width="175" valign="top" class="lblfield_new"><span class="lblField">
                      <?php if(!empty ($applicantTravelHistory[0]['country_id'])){
                      echo "<input class='print_input_box' type='text' readonly=readonly value='".$applicantTravelHistory[0]['Country']['country_name']."'>";
                    }else{echo "<input type='text' class='print_input_box' readonly=readonly value='--'>";} ?>
                      </span></td>
                  </tr>
                  <tr>
				  <div class="custom_height"></div>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_FiveYears0_city">City</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php if(!empty ($applicantTravelHistory[0]['city'])){
                      echo "<input class='print_input_box' type='text' readonly=readonly value='".$applicantTravelHistory[0]['city']."'>";
                    }else{echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";} ?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_FiveYears0_date_of_departure">Date of departure(dd-mm-yyyy)</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                        <?php
                      if(!empty ($applicantTravelHistory[0]['date_of_departure'])){
                      $explode = explode('-',getDateFormate($applicantTravelHistory[0]['date_of_departure']));
                      echo "<input type='text' class='printdate' readonly=readonly value='".$explode[0]."'>";
                      echo "<input type='text' class='printdate' readonly=readonly value='".$explode[1]."'>";
                      echo "<input type='text' class='printyear' readonly=readonly value='".$explode[2]."'>";
                    }else{
                    echo "<input type='text' class='printdate' readonly=readonly value='--'>";
                    echo "<input type='text' class='printdate' readonly=readonly value='--'>";
                    echo "<input type='text' class='printyear' readonly=readonly value='--'>";
                  } ?>
                      </span></td>
                  </tr>
                  </table></td>
                <td valign="top"><table width="347" border="0" cellspacing="0" cellpadding="0" class="no_border_new">
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_FiveYears1_country_id">Country</label>
                    </td>
                    <td width="175" valign="top" class="lblfield_new"><span class="lblField">
                      <?php if(!empty ($applicantTravelHistory[1]['country_id'])){
                      echo "<input class='print_input_box' type='text' readonly=readonly value='".$applicantTravelHistory[1]['Country']['country_name']."'>";
                    }else{ echo "<input type='text' class='print_input_box' readonly=readonly value='--'>";} ?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_FiveYears1_city">City</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php if(!empty ($applicantTravelHistory[1]['city'])){
                      echo "<input class='print_input_box' type='text' readonly=readonly value='".$applicantTravelHistory[1]['city']."'>";
                    }else{echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";} ?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_FiveYears1_date_of_departure">Date of departure(dd-mm-yyyy)</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php if(!empty ($applicantTravelHistory[1]['date_of_departure'])){

                      $explode = explode('-',getDateFormate($applicantTravelHistory[1]['date_of_departure']));
                      echo "<input type='text' class='printdate' readonly=readonly value='".$explode[0]."'>";
                  echo "<input type='text' class='printdate' readonly=readonly value='".$explode[1]."'>";
                  echo "<input type='text' class='printyear' readonly=readonly value='".$explode[2]."'>";
                    }else{
                      echo "<input type='text' class='printdate' readonly=readonly value='--'>";
                    echo "<input type='text' class='printdate' readonly=readonly value='--'>";
                    echo "<input type='text' class='printyear' readonly=readonly value='--'>";
                    } ?>
                      </span></td>
                  </tr>
                  </table></td>
              </tr>
                  <tr>
                <td valign="top" height="5"></td>
                <td valign="top" height="5"></td>
              </tr>
              <tr>
                <td height="25" valign="middle" id="form_new_heading">Period 3 </td>
                <td valign="top">&nbsp;</td>
              </tr>
              <tr>
                <td valign="top" height="5"></td>
                <td valign="top" height="5"></td>
              </tr>
              <tr>
                <td valign="top"><table width="347" border="0" cellspacing="0" cellpadding="0" class="no_border_new">
                    <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_FiveYears2_country_id">Country</label>
                    </td>
                    <td width="175" valign="top" class="lblfield_new"><span class="lblField">
                      <?php if(!empty ($applicantTravelHistory[2]['country_id'])){
                      echo "<input class='print_input_box' type='text' readonly=readonly value='".$applicantTravelHistory[2]['Country']['country_name']."'>";
                    }else{echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";} ?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_FiveYears2_city">City</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php if(!empty ($applicantTravelHistory[2]['city'])){
                      echo "<input class='print_input_box' type='text' readonly=readonly value='".$applicantTravelHistory[2]['city']."'>";
                    }else{echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";} ?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_FiveYears2_date_of_departure">Date of departure(dd-mm-yyyy)</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php if(!empty ($applicantTravelHistory[2]['date_of_departure'])){
                      $explode = explode('-',getDateFormate($applicantTravelHistory[2]['date_of_departure']));
                      echo "<input type='text' class='printdate' readonly=readonly value='".$explode[0]."'>";
                      echo "<input type='text' class='printdate' readonly=readonly value='".$explode[1]."'>";
                      echo "<input type='text' class='printyear' readonly=readonly value='".$explode[2]."'>";
                    }else{
                      echo "<input type='text' class='printdate' readonly=readonly value='--'>";
                    echo "<input type='text' class='printdate' readonly=readonly value='--'>";
                    echo "<input type='text' class='printyear' readonly=readonly value='--'>";
                    } ?>
                      </span></td>
                  </tr>
                  </table></td>
                <td valign="top">&nbsp;</td>
              </tr>
                  <tr>
                <td valign="top" height="5"></td>
                <td valign="top" height="5"></td>
              </tr>
              <tr>
                <td valign="middle" id="form_new_heading" height="25">Give a list of the countries you have visited in the last twelve(12) months</td>
                <td valign="top" id="form_new_heading">&nbsp;</td>
              </tr>
              <tr>
                <td valign="top" height="5"></td>
                <td valign="top" height="5"></td>
              </tr>
              <tr>
                <td height="25" valign="middle" id="form_new_heading">Period 1</td>
                <td height="25" valign="middle" id="form_new_heading">Period 2 </td>
              </tr>
              <tr>
                <td valign="top"><table width="347" border="0" cellspacing="0" cellpadding="0" class="no_border_new">
                    <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_OneYears0_country_id">Country</label>
                    </td>
                    <td width="175" valign="top" class="lblfield_new"><span class="lblField">
                      <?php if(!empty ($applicantTravelHistory[3]['country_id'])){
                      echo "<input class='print_input_box' type='text' readonly=readonly value='".$applicantTravelHistory[3]['Country']['country_name']."'>";
                    }else{echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";} ?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_OneYears0_city">City</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php if(!empty ($applicantTravelHistory[3]['city'])){
                      echo "<input class='print_input_box' type='text' readonly=readonly value='".$applicantTravelHistory[3]['city']."'>";
                    }else{echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";} ?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_OneYears0_date_of_departure">Date of departure(dd-mm-yyyy)</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php if(!empty ($applicantTravelHistory[3]['date_of_departure'])){
                      $explode = explode('-',getDateFormate($applicantTravelHistory[3]['date_of_departure']));
                      echo "<input type='text' class='printdate' readonly=readonly value='".$explode[0]."'>";
                  echo "<input type='text' class='printdate' readonly=readonly value='".$explode[1]."'>";
                  echo "<input type='text' class='printyear' readonly=readonly value='".$explode[2]."'>";
                    }else{
                     echo "<input type='text' class='printdate' readonly=readonly value='--'>";
                    echo "<input type='text' class='printdate' readonly=readonly value='--'>";
                    echo "<input type='text' class='printyear' readonly=readonly value='--'>";
                    } ?>
                      </span></td>
                  </tr>
                  </table></td>
                <td valign="top"><table width="347" border="0" cellspacing="0" cellpadding="0" class="no_border_new">
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_OneYears1_country_id">Country</label>
                    </td>
                    <td width="175" valign="top" class="lblfield_new"><span class="lblField">
                      <?php if(!empty ($applicantTravelHistory[4]['country_id'])){
                      echo "<input class='print_input_box' type='text' readonly=readonly value='".$applicantTravelHistory[4]['Country']['country_name']."'>";
                    }else{echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";} ?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_OneYears1_city">City</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php if(!empty ($applicantTravelHistory[4]['city'])){
                      echo "<input class='print_input_box' type='text' readonly=readonly value='".$applicantTravelHistory[4]['city']."'>";
                    }else{echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";} ?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_OneYears1_date_of_departure">Date of departure(dd-mm-yyyy)</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php if(!empty ($applicantTravelHistory[4]['date_of_departure'])){
                      $explode = explode('-',getDateFormate($applicantTravelHistory[4]['date_of_departure']));
                       echo "<input type='text' class='printdate' readonly=readonly value='".$explode[0]."'>";
                  echo "<input type='text' class='printdate' readonly=readonly value='".$explode[1]."'>";
                  echo "<input type='text' class='printyear' readonly=readonly value='".$explode[2]."'>";
                    }else{
                      echo "<input type='text' class='printdate' readonly=readonly value='--'>";
                    echo "<input type='text' class='printdate' readonly=readonly value='--'>";
                    echo "<input type='text' class='printyear' readonly=readonly value='--'>";
                    } ?>
                      </span></td>
                  </tr>
                  </table></td>
              </tr>
                  <tr>
                <td valign="top" height="5"></td>
                <td valign="top" height="5"></td>
              </tr>
              <tr>
                <td height="25" valign="middle" id="form_new_heading">Period 3 </td>
                <td valign="top">&nbsp;</td>
              </tr>
              <tr>
                <td valign="top" height="5"></td>
                <td valign="top" height="5"></td>
              </tr>
              <tr>
                <td valign="top"><table width="347" border="0" cellspacing="0" cellpadding="0" class="no_border_new">
                    <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_OneYears2_country_id">Country</label>
                    </td>
                    <td width="175" valign="top" class="lblfield_new"><span class="lblField">
                      <?php if(!empty ($applicantTravelHistory[5]['country_id'])){
                      echo "<input class='print_input_box' type='text' readonly=readonly value='".$applicantTravelHistory[5]['Country']['country_name']."'>";
                    }else{echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";} ?>
                    </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_OneYears2_city">City</label>
                      </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                      <?php if(!empty ($applicantTravelHistory[5]['city'])){
                      echo "<input class='print_input_box' type='text' readonly=readonly value='".$applicantTravelHistory[5]['city']."'>";
                    }else{echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";} ?>
                      </span></td>
                  </tr>
                  <tr>
                    <td width="175" valign="top" class="label_name_new"><label for="visa_application_OneYears2_date_of_departure">Date of departure(dd-mm-yyyy)</label>
                    </td>
                    <td valign="top" class="lblfield_new"><span class="lblField">
                        <?php

                      if(!empty ($applicantTravelHistory[5]['date_of_departure'])){
                      $explode = explode('-',getDateFormate($applicantTravelHistory[5]['date_of_departure']));
                      echo "<input type='text' class='printdate' readonly=readonly value='".$explode[0]."'>";
                  echo "<input type='text' class='printdate' readonly=readonly value='".$explode[1]."'>";
                  echo "<input type='text' class='printyear' readonly=readonly value='".$explode[2]."'>";
                    }else{
                     echo "<input type='text' class='printdate' readonly=readonly value='--'>";
                    echo "<input type='text' class='printdate' readonly=readonly value='--'>";
                    echo "<input type='text' class='printyear' readonly=readonly value='--'>";

                    } ?>
                      </span></td>
                  </tr>
                  </table></td>
                <td valign="top"></td>
              </tr>
              
            </table></td>
        </tr>
                </table>
            <div class="clear"></div>
      <div class="body_text border_top"> <b><br/>
        I understand that I will be required to comply with the immigration / Alien and other laws governing entry of the immigrants into the country to which I now apply for Visa / Entry Permit.</b> </div>
          </div>
        <div class="clear"></div>
    <div class="clear noPrint">&nbsp;</div>
    <div class="border_top noPrint"></div>
        <div class="pixbr_new">
          <div class="Y20_new" >
            <div class="l_new">Date: _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _</div>
            <div class="r_new">Signature / Thumb Impression: _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _</div>
            <div class="pixbr_new"></div>
          </div>
      <p class="top-padding printtxt">PLEASE NOTE: PRINT AND SIGN THIS APPLICATION AND TAKE IT WITH TWO (2) PASSPORT PHOTOGRAPHS TO THE SELECTED PASSPORT OFFICE FOR FURTHER PROCESSING.</p>
        </div>
    <div align="center">
          <?php if(!$mode){?>
      <div align="center" class="noPrint">
            <center id="multiFormNav">
              <table align="center" class="nobrd">
                <tr>
              <td><div  id="btn_pnc">
                      <?php if($visa_application[0]['status'] == 'New'){ ?>
                  <input type="button" class="normalbutton noPrint"  value="Print & Continue" onclick="showHidePrintButtons('<?php echo $printUrl; ?>');">
                      <?php } else { ?>
                  <input type="button" class="normalbutton noPrint"  value="Print" onclick="printApplication('<?php echo $printUrl; ?>');">
                      <?php } ?>
                    </div>
                    <!-- changes for edit issue-->
                <input type="button"  disabled class="no_display normalbutton"  id="multiFormSubmit" value="Continue" onclick= "continueProcess();"/>
                  </td>
                </tr>
              </table>
            </center>
          </div>
          <?php } else{?>
      <div align="center" class="noPrint">
            <center id="multiFormNav">
              <table align="center" class="nobrd">
                <tr>
              <td><input type="button" class="normalbutton noPrint"  value="Print" onclick="printApplication('<?php echo $printUrl; ?>');">
                  </td>
              <td><input type="button" class="normalbutton noPrint"  value="Close" onclick="window.close();">
                  </td>
                </tr>
              </table>
            </center>
          </div>
          <?php } ?>
          <div class="print_note" align="center"><em>Please use 0.5 inch/ 12.7mm margin (on all sides of page) for best printing</em></div>
        </div>
        <div class="warningArea">
          <h2>Warning</h2>
          <ul>
            <li>PLEASE ENSURE THAT ALL INFORMATION PROVIDED ON YOUR APPLICATION IS CORRECT BEFORE YOU PROCEED TO PAYMENT. YOUR APPLICATION CANNOT BE
              EDITED ONCE PAYMENT IS INITIATED. </li>
          </ul>
        </div>
      </div>
<script>
  function showHidePrintButtons(url){
    $('#multiFormSubmit').show();
    $('#btn_pnc').hide();
    //window.print();
    printApplication(url);
    $('#multiFormSubmit').removeAttr('disabled');
  }

  function printApplication(url){
      openPopUp(url);
  }

  function openPopUp(url){
     window.open (url, "_blank","status=1,toolbar=0,width=1000px,height=800px,location=0,menubar=0,scrollbars=1");
  }

  function continueProcess(){

    window.location ="<?php echo url_for('visa/show?chk='.$chk.'&p='.$p.'&id='.$idParam)?>";
  }
  document.title = 'The Nigeria Immigration Service';
</script>
</div>
<div class="content_wrapper_bottom"></div>
