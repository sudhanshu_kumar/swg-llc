<?php include_stylesheets_for_form($form) ?>
<?php include_javascripts_for_form($form) ?>
<link rel="stylesheet" type="text/css" media="print" href="<?php  echo stylesheet_path('print');?>" />
<script>
<?php
$appReEntryTypeId= Doctrine::getTable('VisaZoneType')->getFreeZoneId();

$obj = sfContext::getInstance();
//  $appReEntryTypeName = $obj->getRequest()->getParameter('zone_type');

?>
  $(document).ready(function()
  {
    var state  = $("#visa_application_ReEntryApplicantInfo_visa_state_id").val();

    $("input[name='visa_application[ReEntryApplicantInfo][re_entry_type]']").click(function(){
      labelText = $(this).siblings('label').text();
      if(this.checked && labelText=='Single'){
        var appVisaTypeId= <?php echo Doctrine::getTable('FreezoneSingleVisaType')->getFreeZoneVisaTypeId();?>;
        $("#visa_application_ReEntryApplicantInfo_visa_type_id").val(appVisaTypeId);
      }
      if(this.checked && labelText=='Multiple'){
        var appVisaTypeId= <?php echo Doctrine::getTable('FreezoneMultipleVisaType')->getFreeZoneVisaTypeId();?>;
        $("#visa_application_ReEntryApplicantInfo_visa_type_id").val(appVisaTypeId);
      }

    });

  });

  /********************************************************* Addresses *************************************************/
  $(document).ready(function()
  {
    // Change of reference Address  country
    $("#visa_application_ReEntryVisaReferenceAddress0_country_id").change(function(){
      var countryId = $(this).val();
      var url = "<?php echo url_for('visa/getState/'); ?>";
      $("#visa_application_ReEntryVisaReferenceAddress0_state").load(url, {country_id: countryId,add_option: '--- Please Select ---'});
      $("#visa_application_ReEntryVisaReferenceAddress0_lga_id").html( '<option value="">--Please Select--</option>');
      $("#visa_application_ReEntryVisaReferenceAddress0_district_id").html( '<option value="">--Please Select--</option>');
      document.getElementById('visa_application_ReEntryVisaReferenceAddress0_postcode').value="";
    })

    // Change of reference Address state
    $("#visa_application_ReEntryVisaReferenceAddress0_state").change(function(){
      var stateId = $(this).val();
      var url = "<?php echo url_for('visa/getLga/'); ?>";
      $("#visa_application_ReEntryVisaReferenceAddress0_lga_id").load(url, {state_id: stateId});
      $("#visa_application_ReEntryVisaReferenceAddress0_district_id").html( '<option value="">--Please Select--</option>');
      document.getElementById('visa_application_ReEntryVisaReferenceAddress0_postcode').value="";
    });
    // if in near future , client provide postalcodes then it will be uncomment
    /*
$("#visa_application_ReEntryVisaReferenceAddress0_lga_id").change(function(){
var lgaId = $(this).val();
var url = "<?php// echo url_for('visa/getDistrict/'); ?>";
$("#visa_application_ReEntryVisaReferenceAddress0_district_id").load(url, {lga_id: lgaId});
document.getElementById('visa_application_ReEntryVisaReferenceAddress0_postcode').value="";
});

$("#visa_application_ReEntryVisaReferenceAddress0_district_id").change(function(){
var districtId = $(this).val();
var urlDistrict = "<?php// echo url_for('visa/getPostalCode/'); ?>";

$.get(urlDistrict, {district_id: districtId},function(data){   $("#visa_application_ReEntryVisaReferenceAddress0_postcode").val(data);},'text');

});
     */
    // Change of reference Address  country
    $("#visa_application_ReEntryVisaReferenceAddress1_country_id").change(function(){
      var countryId = $(this).val();
      var url = "<?php echo url_for('visa/getState/'); ?>";
      $("#visa_application_ReEntryVisaReferenceAddress1_state").load(url, {country_id: countryId,add_option: '--- Please Select ---'});
      $("#visa_application_ReEntryVisaReferenceAddress1_lga_id").html( '<option value="">--Please Select--</option>');
      $("#visa_application_ReEntryVisaReferenceAddress1_district_id").html( '<option value="">--Please Select--</option>');
      document.getElementById('visa_application_ReEntryVisaReferenceAddress1_postcode').value="";
    })

    // Change of reference Address state
    $("#visa_application_ReEntryVisaReferenceAddress1_state").change(function(){
      var stateId = $(this).val();
      var url = "<?php echo url_for('visa/getLga/'); ?>";
      $("#visa_application_ReEntryVisaReferenceAddress1_lga_id").load(url, {state_id: stateId});
      document.getElementById('visa_application_ReEntryVisaReferenceAddress1_postcode').value="";
    });
    // if in near future , client provide postalcodes then it will be uncomment
    /*
$("#visa_application_ReEntryVisaReferenceAddress1_lga_id").change(function(){
var lgaId = $(this).val();
var url = "<?php //echo url_for('visa/getDistrict/'); ?>";
$("#visa_application_ReEntryVisaReferenceAddress1_district_id").load(url, {lga_id: lgaId});
document.getElementById('visa_application_ReEntryVisaReferenceAddress1_postcode').value="";
});

$("#visa_application_ReEntryVisaReferenceAddress1_district_id").change(function(){
var districtId = $(this).val();
var urlDistrict = "<?php //echo url_for('visa/getPostalCode/'); ?>";

$.get(urlDistrict, {district_id: districtId},function(data){   $("#visa_application_ReEntryVisaReferenceAddress1_postcode").val(data);},'text');

});
     */
  });
  /********************************************************* End Of Addresses *************************************************/
  //changes for edit issue
  function checkNameAuthentication()
  {
<?php if (!$form->getObject()->isNew()) { ?>
    var updatedName = document.getElementById('visa_application_title').value+' '+document.getElementById('visa_application_other_name').value+' '+document.getElementById('visa_application_middle_name').value+' '+document.getElementById('visa_application_surname').value;
    $('#updateNameVal').html(updatedName);
    //return pop2();
  <?php }?>
      return true;
    }

    function newApplicationAction()
    {
      window.location = 'http://' + '<?php echo $_SERVER['HTTP_HOST']?>'+"<?php echo url_for('visa/reEntryForm'); ?>";
    }

    function updateExistingAction()
    {
      document.visa_form.submit();
    }

</script>
  <?php
  $sf = sfContext::getInstance()->getUser();
  if($sf->hasFlash('error')){ ?>
  <div id="flash_error" class="alertBox" >
    <?php
    echo nl2br($sf->getFlash('error'));
    ?>
  </div>
  <?php }?>
<?php if (!$form->getObject()->isNew()) { $updatedName = "";


  //echo ePortal_popup("<p>You are trying to EDIT an EXITING APPLICATION. </p>","<P>Following is the summary of name changes</p><h5>Application Id: ".$form->getObject()->getId()."</h5><h5> Reference No: ".$form->getObject()->getRefNo()."</h5><h5>&nbsp;</h5><h5>Name to update: ".$form->getObject()->getTitleId().' '.$form->getObject()->getFirstName().' '.$form->getObject()->getMidName().' '.$form->getObject()->getLastName()."</h5><h5>Changed Name: <span id='updateNameVal'></span></h5><h5>&nbsp;</h5><h5><a id='updateAction' href='#' onclick='updateExistingAction();'>Update Application</a>&nbsp;&nbsp;<a id='updateAction' href='#' onclick='newApplicationAction();'>Create New Application</a></h5> ");
  $popData = array(
    'appId'=>$form->getObject()->getId(),
    'refId'=>$form->getObject()->getRefNo(),
    'oldName'=>$form->getObject()->getTitle().' '.$form->getObject()->getOtherName().' '.$form->getObject()->getMiddleName().' '.$form->getObject()->getSurname()
  );
  include_partial('global/editPop',$popData);

}
?>

<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">
 <?php include_partial('global/innerHeading', array('heading' => 'New Re-Entry Visa Application (Free Zone) (STEP 2)')); ?>
  <div class="clear"></div>
  <div class="tmz-spacer"></div>
  <form name="visa_form" action="<?php echo url_for('visa/'.($form->getObject()->isNew() ? 'createreentry' : 'updatereentry').(!$form->getObject()->isNew() ? '?id='.$encriptedAppId : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?> id="multiForm" class="dlForm">
      <div id="printHTML">
	  <div class="clear_print"></div>
	  <div class="page_heading_new">New Re-Entry Visa Application (Free Zone)</div>
        <div class="new_container">
          <div class="form_inside_section">
            <?php if ($form->getObject()->isNew()){
              if($free_zone_name == 'Free Zone'){?>
            <div class="inst_container1">
              <div class="inst_top"></div>
              <div class="inst_bg">
                <div class="inst_container1">
                  <ul>
                    <li>FOR ANY QUESTIONS AND/OR CONCERNS PLEASE CONTACT US AT <a href="mailto:support@swgloballlc.com" class="mail_link">support@swgloballlc.com</a></li>
                    <li>APPLICATION FEES PAID FOR VISA/FREEZONE ARE NON REFUNDABLE EXCEPT IN CASED OF DOUBLE PAYMENT MISTAKENLY MADE FOR THE SAME APPLICATION OR FRAUDULENT USE OF CREDIT/DEBIT CARD. TO SEEK A REFUND PLEASE CONTACT <a href="mailto:refund@swgloballlc.com" class="mail_link">refund@swgloballlc.com</a></li>
                    <li>PAYMENTS ARE VALID FOR SIX (6) MONTHS ONLY FROM THE PAYMENT DATE.</li>
                    <li>ONLY online payment is acceptable.</li>
                    <li>Anyone who pays otherwise and receives service is subject to prosecution and revocation of Visa.</li>
                    <li>If you have already completed an application, please check your application status rather than completing a duplicate application</li>
                  </ul>
                </div>
              </div>
              <div class="inst_bottom"></div>
              <div class="clear"></div>
            </div>
            <?php }else {

            echo ePortal_highlight('If you have submitted an application and obtained an Application ID and a Reference Number, Please check your application status.','',array('class'=>'red'));
          }
        }else
        if (!$form->getObject()->isNew()){?>
            <input type="hidden" name="sf_method" value="put" />
            <?php } ?>

            <div class="bodyContent2" id="uiGroup_">
              <div class="formH3">
                <h2>Personal Information</h2>
                <h2 class="pdLeft10">Passport Information</h2>
              </div>
              <div class="formContent_new formBrd">
                <div class="lblName"><?php echo $form['title']->renderLabel(); ?><span class="redLbl">*</span></div>
                <div class="lblField">
                  <?php echo $form['title']->render(); ?>
                  <div class="error_msg"> <?php echo $form['title']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['surname']->renderLabel(); ?><span class="redLbl">*</span></div>
                                <div class="lblField">
                  <?php echo $form['surname']->render(); ?>
                  <div class="error_msg"> <?php echo $form['surname']->renderError(); ?></div>
                  <i><span class="small-text" >(Please avoid placing suffixes with your surname)</span></i>
                </div>
                <div class="lblName"><?php echo $form['other_name']->renderLabel(); ?><span class="redLbl">*</span></div>
                <div class="lblField">
                  <?php echo $form['other_name']->render(); ?>
                  <div class="error_msg"> <?php echo $form['other_name']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['middle_name']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField">
                  <?php echo $form['middle_name']->render(); ?>
                  <div class="error_msg"> <?php echo $form['middle_name']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['gender']->renderLabel(); ?><span class="redLbl">*</span></div>
                <div class="lblField">
                  <?php echo $form['gender']->render(); ?>
                  <div class="error_msg"> <?php echo $form['gender']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['email']->renderLabel(); ?><span class="redLbl">*</span></div>
                <div class="lblField">
                  <?php echo $form['email']->render(); ?>
                  <div class="error_msg"> <?php echo $form['email']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['date_of_birth']->renderLabel(); ?><span class="redLbl">*</span></div>
                <div class="lblField">
                  <?php echo $form['date_of_birth']->render(); ?>
                  <div class="error_msg"> <?php echo $form['date_of_birth']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['place_of_birth']->renderLabel(); ?><span class="redLbl">*</span></div>
                <div class="lblField">
                  <?php echo $form['place_of_birth']->render(); ?>
                  <div class="error_msg"> <?php echo $form['place_of_birth']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['present_nationality_id']->renderLabel(); ?><span class="redLbl">*</span></div>
                <div class="lblField" id="width_dropbox">
                  <?php echo $form['present_nationality_id']->render(); ?>
                  <div class="error_msg"> <?php echo $form['present_nationality_id']->renderError(); ?></div>
                </div>
              </div>
              <div class="formContent_new pdLeft10">
                <div class="lblName"><?php echo $form['ReEntryApplicantInfo']['passport_number']->renderLabel(); ?><span class="redLbl">*</span></div>
                <div class="lblField" >
                  <?php echo $form['ReEntryApplicantInfo']['passport_number']->render(); ?>
                  <div class="error_msg"> <?php echo $form['ReEntryApplicantInfo']['passport_number']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['ReEntryApplicantInfo']['issusing_govt']->renderLabel(); ?><span class="redLbl">*</span></div>
                <div class="lblField" id="width_dropbox">
                  <?php echo $form['ReEntryApplicantInfo']['issusing_govt']->render(); ?>
                  <div class="error_msg"> <?php echo $form['ReEntryApplicantInfo']['issusing_govt']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['ReEntryApplicantInfo']['date_of_issue']->renderLabel(); ?><span class="redLbl">*</span></div>
                <div class="lblField" >
                  <?php echo $form['ReEntryApplicantInfo']['date_of_issue']->render(); ?>
                  <div class="error_msg"> <?php echo $form['ReEntryApplicantInfo']['date_of_issue']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['ReEntryApplicantInfo']['date_of_exp']->renderLabel(); ?><span class="redLbl">*</span></div>
                <div class="lblField" >
                  <?php echo $form['ReEntryApplicantInfo']['date_of_exp']->render(); ?>
                  <div class="error_msg"> <?php echo $form['ReEntryApplicantInfo']['date_of_exp']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['ReEntryApplicantInfo']['place_of_issue']->renderLabel(); ?><span class="redLbl">*</span></div>
                <div class="lblField" >
                  <?php echo $form['ReEntryApplicantInfo']['place_of_issue']->render(); ?>
                  <div class="error_msg"> <?php echo $form['ReEntryApplicantInfo']['place_of_issue']->renderError(); ?></div>
                </div>


              </div>

              <div class="formH3">
                <h2>Address</h2>
                <h2 class="pdLeft10">&nbsp;</h2>
              </div>
              <div class="formContent_new formBrd">

                <div class="lblName"><?php echo $form['ReEntryVisaAddress']['address_1']->renderLabel(); ?><span class="redLbl">*</span></div>
                <div class="lblField" >
                  <?php echo $form['ReEntryVisaAddress']['address_1']->render(); ?>
                  <div class="error_msg"> <?php echo $form['ReEntryVisaAddress']['address_1']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['ReEntryVisaAddress']['address_2']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField" >
                  <?php echo $form['ReEntryVisaAddress']['address_2']->render(); ?>
                  <div class="error_msg"> <?php echo $form['ReEntryVisaAddress']['address_2']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['ReEntryVisaAddress']['city']->renderLabel(); ?><span class="redLbl">*</span></div>
                <div class="lblField" >
                  <?php echo $form['ReEntryVisaAddress']['city']->render(); ?>
                  <div class="error_msg"> <?php echo $form['ReEntryVisaAddress']['city']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['ReEntryVisaAddress']['country_id']->renderLabel(); ?><span class="redLbl">*</span></div>
                <div class="lblField" id="width_dropbox">
                  <?php echo $form['ReEntryVisaAddress']['country_id']->render(); ?>
                  <div class="error_msg"> <?php echo $form['ReEntryVisaAddress']['country_id']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['ReEntryVisaAddress']['state']->renderLabel(); ?><span class="redLbl">*</span></div>
                <div class="lblField" >
                  <?php echo $form['ReEntryVisaAddress']['state']->render(); ?>
                  <div class="error_msg"> <?php echo $form['ReEntryVisaAddress']['state']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['ReEntryVisaAddress']['postcode']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField" >
                  <?php echo $form['ReEntryVisaAddress']['postcode']->render(); ?>
                  <div class="error_msg"> <?php echo $form['ReEntryVisaAddress']['postcode']->renderError(); ?></div>
                </div>


              </div>
              <div class="formContent_new pdLeft10">
                <div class="lblName"><?php echo $form['ReEntryApplicantInfo']['profession']->renderLabel(); ?><span class="redLbl">*</span></div>
                <div class="lblField" >
                  <?php echo $form['ReEntryApplicantInfo']['profession']->render(); ?>
                  <div class="error_msg"> <?php echo $form['ReEntryApplicantInfo']['profession']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['ReEntryApplicantInfo']['reason_for_visa_requiring']->renderLabel(); ?><span class="redLbl">*</span></div>
                <div class="lblField" >
                  <?php echo $form['ReEntryApplicantInfo']['reason_for_visa_requiring']->render(); ?>
                  <div class="error_msg"> <?php echo $form['ReEntryApplicantInfo']['reason_for_visa_requiring']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['ReEntryApplicantInfo']['last_arrival_in_nigeria']->renderLabel(); ?><span class="redLbl">*</span></div>
                <div class="lblField">
                  <?php echo $form['ReEntryApplicantInfo']['last_arrival_in_nigeria']->render(); ?>
                  <div class="error_msg"> <?php echo $form['ReEntryApplicantInfo']['last_arrival_in_nigeria']->renderError(); ?></div>
                </div>
                <div class="clear"></div>
                <div class="lblName"><?php echo $form['ReEntryApplicantInfo']['re_entry_category']->renderLabel(); ?><span class="redLbl">*</span></div>
                <div class="lblField">
                  <?php echo $form['ReEntryApplicantInfo']['re_entry_category']->render(); ?>
                  <div class="error_msg"> <?php echo $form['ReEntryApplicantInfo']['re_entry_category']->renderError(); ?></div>
                </div>
                <div class="clear"></div>
                <div class="lblName"><?php echo $form['ReEntryApplicantInfo']['re_entry_type']->renderLabel(); ?><span class="redLbl">*</span></div>
                <div class="lblField" >
                 <?php echo $form['ReEntryApplicantInfo']['re_entry_type']->render(); ?>
                  <div class="error_msg"> <?php echo $form['ReEntryApplicantInfo']['re_entry_type']->renderError(); ?></div>
                </div>


              </div>
              <div class="formH3">
                <h2>Employer's Information</h2>
                <h2 class="pdLeft10">Employer's Address</h2>
              </div>
              <div class="formContent_new">
                <div class="lblName"><?php echo $form['ReEntryApplicantInfo']['employer_name']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField" >
                  <?php echo $form['ReEntryApplicantInfo']['employer_name']->render(); ?>
                  <div class="error_msg"> <?php echo $form['ReEntryApplicantInfo']['employer_name']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['ReEntryApplicantInfo']['employer_phone']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField" >
                  <?php echo $form['ReEntryApplicantInfo']['employer_phone']->render(); ?>
                  <div class="error_msg"> <?php echo $form['ReEntryApplicantInfo']['employer_phone']->renderError(); ?></div>
                </div>



              </div>
              <div class="formContent_new pdLeft10 formBrd_left">
                <div class="lblName"><?php echo $form['ReEntryVisaEmployerAddress']['address_1']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField" >
                  <?php echo $form['ReEntryVisaEmployerAddress']['address_1']->render(); ?>
                  <div class="error_msg"> <?php echo $form['ReEntryVisaEmployerAddress']['address_1']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['ReEntryVisaEmployerAddress']['address_2']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField" >
                  <?php echo $form['ReEntryVisaEmployerAddress']['address_2']->render(); ?>
                  <div class="error_msg"> <?php echo $form['ReEntryVisaEmployerAddress']['address_2']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['ReEntryVisaEmployerAddress']['city']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField" >
                  <?php echo $form['ReEntryVisaEmployerAddress']['city']->render(); ?>
                  <div class="error_msg"> <?php echo $form['ReEntryVisaEmployerAddress']['city']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['ReEntryVisaEmployerAddress']['country_id']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField" id="width_dropbox">
                  <?php echo $form['ReEntryVisaEmployerAddress']['country_id']->render(); ?>
                  <div class="error_msg"> <?php echo $form['ReEntryVisaEmployerAddress']['country_id']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['ReEntryVisaEmployerAddress']['state']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField" >
                  <?php echo $form['ReEntryVisaEmployerAddress']['state']->render(); ?>
                  <div class="error_msg"> <?php echo $form['ReEntryVisaEmployerAddress']['state']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['ReEntryVisaEmployerAddress']['postcode']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField" >
                  <?php echo $form['ReEntryVisaEmployerAddress']['postcode']->render(); ?>
                  <div class="error_msg"> <?php echo $form['ReEntryVisaEmployerAddress']['postcode']->renderError(); ?></div>
                </div>


              </div>
              <div class="formH3">
                <h2>Reference - Refree</h2>
                <h2 class="pdLeft10">CERPAC Information (Where Applicable)</h2>
              </div>
              <div class="formContent_new">
                <div class="lblName"><?php echo $form['Reference0']['name_of_refree']->renderLabel(); ?><span class="redLbl">*</span></div>
                <div class="lblField" >
                  <?php echo $form['Reference0']['name_of_refree']->render(); ?>
                  <div class="error_msg"> <?php echo $form['Reference0']['name_of_refree']->renderError(); ?></div>
                </div>

                <div class="lblName"><?php echo $form['ReEntryVisaReferenceAddress0']['address_1']->renderLabel(); ?><span class="redLbl">*</span></div>
                <div class="lblField" >
                  <?php echo $form['ReEntryVisaReferenceAddress0']['address_1']->render(); ?>
                  <div class="error_msg"> <?php echo $form['ReEntryVisaReferenceAddress0']['address_1']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['Reference0']['phone_of_refree']->renderLabel(); ?><span class="redLbl">*</span></div>
                <div class="lblField" >
                  <?php echo $form['Reference0']['phone_of_refree']->render(); ?>
                  <div class="error_msg"> <?php echo $form['Reference0']['phone_of_refree']->renderError(); ?></div>
                </div>




              </div>
              <div class="formContent_new pdLeft10 formBrd_left">
                <div class="lblName"><?php echo $form['ReEntryApplicantInfo']['cerpa_quota']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField" >
                  <?php echo $form['ReEntryApplicantInfo']['cerpa_quota']->render(); ?>
                  <div class="error_msg"> <?php echo $form['ReEntryApplicantInfo']['cerpa_quota']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['ReEntryApplicantInfo']['cerpac_issuing_state']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField" >
                  <?php echo $form['ReEntryApplicantInfo']['cerpac_issuing_state']->render(); ?>
                  <div class="error_msg"> <?php echo $form['ReEntryApplicantInfo']['cerpac_issuing_state']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['ReEntryApplicantInfo']['cerpac_date_of_issue']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField">
                  <?php echo $form['ReEntryApplicantInfo']['cerpac_date_of_issue']->render(); ?>
                  <div class="error_msg"> <?php echo $form['ReEntryApplicantInfo']['cerpac_date_of_issue']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['ReEntryApplicantInfo']['cerpac_exp_date']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField" >
                  <?php echo $form['ReEntryApplicantInfo']['cerpac_exp_date']->render(); ?>
                  <div class="error_msg"> <?php echo $form['ReEntryApplicantInfo']['cerpac_exp_date']->renderError(); ?></div>
                </div>
                <div class="lblName"><?php echo $form['ReEntryApplicantInfo']['issuing_cerpac_office']->renderLabel(); ?><span class="redLbl"></span></div>
                <div class="lblField" >
                  <?php echo $form['ReEntryApplicantInfo']['issuing_cerpac_office']->render(); ?>
                  <div class="error_msg"> <?php echo $form['ReEntryApplicantInfo']['issuing_cerpac_office']->renderError(); ?></div>
                </div>



              </div>
              <div class="formH3">
                <h2>Visa Processing Information</h2>
              </div>
              <div class="formContent_new">
                <div class="lblName"><?php echo $form['ReEntryApplicantInfo']['processing_centre_id']->renderLabel(); ?><span class="redLbl">*</span></div>
                <div class="lblField" >
                  <?php echo $form['ReEntryApplicantInfo']['processing_centre_id']->render(); ?>
                  <div class="error_msg"> <?php echo $form['ReEntryApplicantInfo']['processing_centre_id']->renderError(); ?></div>
                </div>
              </div>

              <!--<div class="formH3">
<h2>ID Proof</h2>
</div>
<div class="formContent_new">
<div class="lblName"><?php //echo $form['scan_address_name']->renderLabel(); ?><span class="redLbl">*</span></div>
<div class="lblField">
<?php //echo $form['scan_address_name']->render(); ?>
<div class="error_msg"  id="visa_application_scan_address_name_error" > <?php //echo $form['scan_address_name']->renderError(); ?></div>
</div>
<div class="inst_container1">
<div class="inst_top"></div>
<div class="inst_bg">
<div class="inst_container1" style="padding-left:15px;">
Please upload scanned copy of your passport as per following instructions:<br>
<ul>
<li>Scanned copy of your international passport which should clearly show the first page (with photograph, full name, date &nbsp;&nbsp;&nbsp; of birth).</li>
</ul>
<br><b>Please upload JPG/GIF/PNG images only - (DO NO UPLOAD IMAGE LARGER THAN 500 KB).</b>
</div>
</div>
<div class="inst_bottom"></div>
<div class="clear"></div>
</div>
</div>

-->



            </div>
            <?php echo $form['id']->render(); ?>
            <?php echo $form['visacategory_id']->render(); ?>
            <?php echo $form['zone_type_id']->render(); ?>
            <?php echo $form['payment_gateway_id']->render(); ?>
            <?php echo $form['ReEntryApplicantInfo']['id']->render(); ?>
            <?php echo $form['ReEntryApplicantInfo']['visa_type_id']->render(); ?>

            <?php echo $form['Reference0']['id']->render(); ?>
            <?php echo $form['ReEntryVisaReferenceAddress0']['id']->render(); ?>
            <?php echo $form['ReEntryVisaAddress']['id']->render(); ?>
            <?php echo $form['ReEntryVisaEmployerAddress']['id']->render(); ?>
        </div></div>
        <div class="clear"></div>
    <div class="pixbr_new X50_new mg_left_zero">
          <p>I will be required to comply with the Immigration and other laws governing entry of the immigrants into the country to which I now apply for Visa</p>
          <!-- <p><strong>Please take printed, and signed form together with evidence of payment to the Nigerian Embassy / High Commission of your residence for further processing.</strong></p> -->
      <br/>
	  <p class="text_hide"><span class="red">*</span> - Compulsory fields</p>
      <div class="Y20_new"> </div>
      <div class="border_top_new"></div>
           <div>
        <p><span class="notice-text" >PLEASE NOTE: PRINT AND SIGN THIS APPLICATION AND TAKE IT WITH TWO (2) PASSPORT PHOTOGRAPHS TO THE SELECTED PASSPORT OFFICE FOR FURTHER PROCESSING.</span></p>
        </div>
        </div>




          <center id="multiFormNav">
            <table align="center" class="nobrd">
        <tr>
          <td><!-- input type="button" id="multiFormSubmit1" value="print" class="" onClick= "javaScript:window.open('<?php //echo url_for('visa/visaPrint?type=r') ?>','MyPage','width=750,height=700,scrollbars=1,resizable=yes')"/-->
                <!--  <input type="button" id="multiFormSubmit1" value="print" class="" onClick= "window.print();"/> -->
                  <!-- changes for edit issue-->
                  <?php if ($form->getObject()->isNew()){?>
            <input type="submit"  class="normalbutton"  id="multiFormSubmit" value="Submit Application"/>
                    <?php  } else if (!$form->getObject()->isNew()){ ?>
            <input type="submit" id="multiFormSubmit"  class="normalbutton"  value="Proceed" onclick="return checkNameAuthentication()"/>
                    <?php } ?>
          </td>
        </tr>
      </table>
          </center>
        <div class="warningArea">
          <h2>Warning</h2>
          <ul>
            <li>PLEASE ENSURE THAT ALL INFORMATION PROVIDED ON YOUR APPLICATION IS CORRECT BEFORE YOU PROCEED TO PAYMENT. YOUR APPLICATION CANNOT BE
          EDITED ONCE PAYMENT IS INITIATED. </li>
          </ul>
        </div>
      </div>
    </div>
  </form>


<script>
    /* Start Ashwani This function is using jquery print  */
    function printPassportNew(){
     //printElem({});
     //printElem({ printMode: 'popup' });
     var cssUrl1 = '<?php echo image_path('/css/container.css', true); ?>';
     var cssUrl2 = '<?php echo image_path('/css/print.css', true); ?>';
     printElem({ overrideElementCSS: [cssUrl2, cssUrl1] });
   }

   function printElem(options){
     $('#printHTML').printElement(options);
   }
   /* End Ashwani This function is using jquery print  */

   if($('#visa_application_date_of_birth_day').length > 0){
      $('#visa_application_date_of_birth_day').addClass('small_dropbox');
      $('#visa_application_date_of_birth_month').addClass('small_dropbox');
      $('#visa_application_date_of_birth_year').addClass('small_dropbox');

      $('#visa_application_ReEntryApplicantInfo_date_of_issue_day').addClass('small_dropbox');
      $('#visa_application_ReEntryApplicantInfo_date_of_issue_month').addClass('small_dropbox');
      $('#visa_application_ReEntryApplicantInfo_date_of_issue_year').addClass('small_dropbox');

      $('#visa_application_ReEntryApplicantInfo_date_of_exp_day').addClass('small_dropbox');
      $('#visa_application_ReEntryApplicantInfo_date_of_exp_month').addClass('small_dropbox');
      $('#visa_application_ReEntryApplicantInfo_date_of_exp_year').addClass('small_dropbox');

      $('#visa_application_ReEntryApplicantInfo_last_arrival_in_nigeria_day').addClass('small_dropbox');
      $('#visa_application_ReEntryApplicantInfo_last_arrival_in_nigeria_month').addClass('small_dropbox');
      $('#visa_application_ReEntryApplicantInfo_last_arrival_in_nigeria_year').addClass('small_dropbox');

      $('#visa_application_ReEntryApplicantInfo_cerpac_date_of_issue_day').addClass('small_dropbox');
      $('#visa_application_ReEntryApplicantInfo_cerpac_date_of_issue_month').addClass('small_dropbox');
      $('#visa_application_ReEntryApplicantInfo_cerpac_date_of_issue_year').addClass('small_dropbox');

      $('#visa_application_ReEntryApplicantInfo_cerpac_exp_date_day').addClass('small_dropbox');
      $('#visa_application_ReEntryApplicantInfo_cerpac_exp_date_month').addClass('small_dropbox');
      $('#visa_application_ReEntryApplicantInfo_cerpac_exp_date_year').addClass('small_dropbox');
      
   }

   /* ## User can not change value of below given fields  ## */
//   $(document).ready(function(){
//        $('#visa_application_other_name').focus(function(){
//           $('#visa_application_title').focus();
//           return false;
//        });
//        $('#visa_application_surname').focus(function(){
//           $('#visa_application_title').focus();
//           return false;
//        });
//        $('#visa_application_gender').focus(function(){
//           $('#visa_application_title').focus();
//           return false;
//        });
//        $('#visa_application_date_of_birth_day').focus(function(){
//           $('#visa_application_title').focus();
//           return false;
//        });
//        $('#visa_application_date_of_birth_month').focus(function(){
//           $('#visa_application_title').focus();
//           return false;
//        });
//        $('#visa_application_date_of_birth_year').focus(function(){
//           $('#visa_application_title').focus();
//           return false;
//        });
//        $('#visa_application_place_of_birth').focus(function(){
//           $('#visa_application_title').focus();
//           return false;
//        });
//        $('#visa_application_email').focus(function(){
//           $('#visa_application_title').focus();
//           return false;
//        });
//    });



</script>
</div>
<div class="content_wrapper_bottom"></div>