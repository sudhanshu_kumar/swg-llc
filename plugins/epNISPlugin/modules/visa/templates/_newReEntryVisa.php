<?php include_stylesheets_for_form($form) ?>
<?php include_javascripts_for_form($form) ?>
<script>
<?php
$appReEntryTypeId= Doctrine::getTable('VisaZoneType')->getFreeZoneId();
$obj = sfContext::getInstance();
?>  
</script>
  <?php
  $sf = sfContext::getInstance()->getUser();
  if($sf->hasFlash('error')){ ?>

  <div id="flash_error" class="alertBox" >
    <?php
    echo nl2br($sf->getFlash('error'));
    ?>
  </div>
  <?php }?>
<?php if (!$form->getObject()->isNew()) { $updatedName = "";

  //echo ePortal_popup("<p>You are trying to EDIT an EXITING APPLICATION. </p>","<P>Following is the summary of name changes</p><h5>Application Id: ".$form->getObject()->getId()."</h5><h5> Reference No: ".$form->getObject()->getRefNo()."</h5><h5>&nbsp;</h5><h5>Name to update: ".$form->getObject()->getTitleId().' '.$form->getObject()->getFirstName().' '.$form->getObject()->getMidName().' '.$form->getObject()->getLastName()."</h5><h5>Changed Name: <span id='updateNameVal'></span></h5><h5>&nbsp;</h5><h5><a id='updateAction' href='#' onclick='updateExistingAction();'>Update Application</a>&nbsp;&nbsp;<a id='updateAction' href='#' onclick='newApplicationAction();'>Create New Application</a></h5> ");
  $popData = array(
    'appId'=>$form->getObject()->getId(),
    'refId'=>$form->getObject()->getRefNo(),
    'oldName'=>$form->getObject()->getTitle().' '.$form->getObject()->getOtherName().' '.$form->getObject()->getMiddleName().' '.$form->getObject()->getSurname()
  );
  include_partial('global/editPop',$popData);

}
?>
<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">
    <?php include_partial('global/innerHeading', array('heading' => 'New Re-Entry Visa Application (Free Zone) (STEP 1)')); ?>
  <form name="visa_form" id="visa_form" action="<?php echo url_for('visa/'.($form->getObject()->isNew() ? 'createreentry' : 'updatereentry').(!$form->getObject()->isNew() ? '?id='.$encriptedAppId : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>  class="dlForm">
	  <div class="clear_print"></div>
	  <div class="page_heading_new">New Re-Entry Visa Application (Free Zone)</div>
      <!--<div class="new_container">-->
      <!--<div class="form_inside_section">-->
            <?php if ($form->getObject()->isNew()){
            
            }else if (!$form->getObject()->isNew()){?>
            <input type="hidden" name="sf_method" value="put" />
            <?php } ?>
            <div class="bodyContent2" id="uiGroup_">
              <div class="formH3">
                <h2>Personal Information</h2>                
              </div>
              <div class="formContent_new">                
                <div class="lblName_new"><?php echo $form['surname']->renderLabel(); ?><span class="redLbl">*</span></div>
          <div class="lblField"> <?php echo $form['surname']->render(); ?>
                  <div class="error_msg" id="visa_application_surname_error"> <?php echo $form['surname']->renderError(); ?></div>
            <i><span class="small-text" style="font-family: Arial,Helvetica,sans-serif;font-size: 10.5px; color: #555555;">(Please avoid placing suffixes with your surname)</span></i> </div>
                <div class="lblName_new"><?php echo $form['other_name']->renderLabel(); ?><span class="redLbl">*</span></div>
          <div class="lblField"> <?php echo $form['other_name']->render(); ?>
                  <div class="error_msg" id="visa_application_other_name_error"> <?php echo $form['other_name']->renderError(); ?></div>
                </div>                
                <div class="lblName_new"><?php echo $form['middle_name']->renderLabel(); ?><span class="redLbl"></span></div>
          <div class="lblField"> <?php echo $form['middle_name']->render(); ?>
                  <div class="error_msg" id="visa_application_other_name_error"> <?php echo $form['middle_name']->renderError(); ?></div>
                </div>
                <div class="lblName_new"><?php echo $form['gender']->renderLabel(); ?><span class="redLbl">*</span></div>
          <div class="lblField"> <?php echo $form['gender']->render(); ?>
                  <div class="error_msg" id="visa_application_gender_error"> <?php echo $form['gender']->renderError(); ?></div>
                </div>
                <div class="lblName_new"><?php echo $form['email']->renderLabel(); ?><span class="redLbl">*</span></div>
          <div class="lblField"> <?php echo $form['email']->render(); ?>
                  <div class="error_msg" id="visa_application_email_error"> <?php echo $form['email']->renderError(); ?></div>
                </div>
                <div class="lblName_new"><?php echo $form['date_of_birth']->renderLabel(); ?><span class="redLbl">*</span></div>
          <div class="lblField"> <?php echo $form['date_of_birth']->render(); ?>
                  <div class="error_msg" id="visa_application_date_of_birth_error"> <?php echo $form['date_of_birth']->renderError(); ?></div>
                </div>
                <div class="lblName_new"><?php echo $form['place_of_birth']->renderLabel(); ?><span class="redLbl">*</span></div>
          <div class="lblField"> <?php echo $form['place_of_birth']->render(); ?>
                  <div class="error_msg" id="visa_application_place_of_birth_error"> <?php echo $form['place_of_birth']->renderError(); ?></div>
                </div>                
              </div>             
            </div>            
        <div class="clear">&nbsp;</div>
        <div class="border_top"></div>
      <div class="pixbr_new X50_new">
          <p class="text_hide"><font color="red">*</font> - Compulsory fields</p>
        </div>
          <center id="multiFormNav">
            <table align="center" class="nobrd">
            <tr>
              <td align="left"><input type="button"  class="normalbutton"  id="multiFormSubmit" value="Next" onclick="checkForm();" />
                  <input type="hidden" id="appId" name="appId"  class="" value="" />
                  <input type="hidden" id="appSatusFlag" name="appSatusFlag"  class="" value="" />
                  <input type="hidden" id="printUrl" name="printUrl"  class="" value="" />
                  <input type="hidden" name="countryId" id="countryId" value="" />
              </td>
            </tr>
          </table>
          </center>     
  </form>
<div id="popupContent"></div>
<div id="backgroundPopup"></div>
<div id="loading" class="no_display"><?php echo image_tag('/images/ajax-loader_1.gif',array('alt'=>'Loading ...', 'align' => 'absmiddle'));?><br/>&nbsp;&nbsp;Please wait...</div>
<script>
    function validateAppName(str)
    {
      var reg = /^[A-Za-z \-\.]*$/; //allow alphabet and spaces only...
      if(reg.test(str) == false) {
        return 1;
      }

      return 0;
    }

    function validateEmail(email) {

    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    if(reg.test(email) == false) {

      return 1;
    }

    return 0;
  }
    function formValidate(){
        var first_name = jQuery.trim($('#visa_application_other_name').val());
        var last_name = jQuery.trim($('#visa_application_surname').val());
        var gender_id = jQuery.trim($('#visa_application_gender').val());
        var day = jQuery.trim($('#visa_application_date_of_birth_day').val());
        var month = jQuery.trim($('#visa_application_date_of_birth_month').val());
        var year = jQuery.trim($('#visa_application_date_of_birth_year').val());
        var place_of_birth = jQuery.trim($('#visa_application_place_of_birth').val());
        var email = jQuery.trim($('#visa_application_email').val());        


        var err  = 0;
        if(first_name == "")
        {
          $('#visa_application_other_name_error').html("Please enter first name");
          err = err+1;
        }else if(validateAppName(first_name) != 0)
        {
          $('#visa_application_other_name_error').html("Please enter valid first name");
          err = err+1;
        }
        else
        {
          $('#visa_application_other_name_error').html("");
        }

        if(last_name == "")
        {
          $('#visa_application_surname_error').html("Please enter last name");
          err = err+1;
        }else if(validateAppName(last_name) != 0)
        {
          $('#visa_application_surname_error').html("Please enter valid last name");
          err = err+1;
        }
        else
        {
          $('#visa_application_surname_error').html("");
        }


        if(gender_id == "")
        {
          $('#visa_application_gender_error').html("Please select gender");
          err = err+1;
        }
        else
        {
          $('#visa_application_gender_error').html("");
        }

        var date_of_birth_flag = false;
        if(day == "")
        {
          date_of_birth_flag = true;
        }
        if(month == "")
        {
          date_of_birth_flag = true;
        }
        if(year == "")
        {
          date_of_birth_flag = true;
        }

        var current_date  = new Date();

        if(date_of_birth_flag){
            $('#visa_application_date_of_birth_error').html("Please select date of birth");
            err = err+1;
        }else{

            var date_of_birth = new Date(year,month-1,day);
            if(date_of_birth.getTime() > current_date.getTime()) {
                $('#visa_application_date_of_birth_error').html("Date of birth cannot be future date");
                err = err+1;
            }else{
                $('#visa_application_date_of_birth_error').html("");
            }
        }

        if(place_of_birth == "")
        {
          $('#visa_application_place_of_birth_error').html("Please enter place of birth");
          err = err+1;
        }else if(validateAppName(place_of_birth) != 0)
        {
          $('#visa_application_place_of_birth_error').html("Please enter valid place of birth");
          err = err+1;
        }
        else
        {
          $('#visa_application_place_of_birth_error').html("");
        }






        if(email == "")
        {
            $('#visa_application_email_error').html("Please enter Email");
            err = err+1;
        }
        else if(validateEmail(email) != 0)
        {
            $('#visa_application_email_error').html("Please enter Valid Email");
            err = err+1;
        }
        else
        {
            $('#visa_application_email_error').html("");
        }

        if(err == 0){
            return true;
        }else{
            return false;
        }

    }

    var visacategory_id = '102';

    function checkForm() {

        if(!formValidate()){
            return false;
        }

        $('#multiFormSubmit').attr("disabled","disabled");
        $('#multiFormSubmit').val("Please wait...");

        var url = "<?php echo url_for('visa/newReEntryVisaAjax'); ?>";

        centerPopup();
        var elementPos = findElementPosition('multiFormSubmit');
        elementPos = elementPos.split(',');
        $("#popupContent").css({
                "position": "absolute",
                "top": (parseInt(elementPos[1])-190)+'px',
                "left":(parseInt(elementPos[0])-90)+'px',
                "width":'230px'
        });
        $("#popupContent").html($('#loading').html());
        loadPopup();

        $.ajax({
            type: "POST",
            url: url,
            data: $('#visa_form').serialize(),
            success: function (data) {
                data = jQuery.trim(data);
                if(data == 'notfound'){
                    var url = '<?php echo url_for('visa/reentryvisa');?>';
                    $('#appSatusFlag').val('new');
                    document.visa_form.action = url;
                    document.visa_form.submit();
                }else if(data == 'error'){
                    $('#popupContent').hide('slow');
                    alert('Oops! Some technical problem occur. Please try it again.');
                }else{
                    $('#popupContent').html(data);
                    $("#popupContent").css({
                        "width": '600px'
                    });

                    var elementPos = findElementPosition('multiFormSubmit');
                    elementPos = elementPos.split(',');
                    $("#popupContent").css({
                        "position": "absolute",
                        "top": (parseInt(elementPos[1])-290)+'px',
                        "left":(parseInt(elementPos[0])-240)+'px'
                    });

                    $('#multiFormSubmit').attr("disabled","");
                    $('#multiFormSubmit').val("Next");
                }
            }//End of success: function (data) {...
        }); 
       
    }//End of function checkForm() {...


    function openExistingApp(appId, postUrl, processingCountryId){
       $('#appSatusFlag').val('old');           
       if(appId != ''){           
           document.visa_form.action = postUrl;
           $('#appId').val(appId);
           $('#countryId').val(processingCountryId);
           document.visa_form.submit();
       }else{
           alert("application does not exist");
       }        
    }

    function openNewApp(){
        var url = '<?php echo url_for('visa/reentryvisa');?>';
        location.href = url;
    }

    function printExistingApp(printUrl){
        location.href = printUrl;
    }
</script>
  </div>
</div>
<div class="content_wrapper_bottom"></div>
