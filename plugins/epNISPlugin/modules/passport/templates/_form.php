<?php include_stylesheets_for_form($form) ?>
<?php include_javascripts_for_form($form) ?>
<link rel="stylesheet" type="text/css" media="print" href="<?php  echo stylesheet_path('print');?>" />
<script>
  //Workaround for resolving fieldset print issue in Mozilla
  function printPassport(){
    if($('#printFrame').length){
      $('#printFrame').remove();
    }
    printFrame = "<iframe id='printFrame' width='1' height='1' src='<?php echo url_for('@printPassport'); ?>'></iframe>";
    $('body').append(printFrame);

    $('#printFrame').load(function(){
      //             html = $('#'+contentId).html();

      var str = '';
      var elem = document.getElementById('frm').elements;
      for(var i = 0; i < elem.length; i++)
      {
        //            alert(elem[4].id) ;
        //            alert(document.getElementById(elem[4].id).value) ;
        //            document.getElementById(elem[4].id).value = elem[4].value ;
        if(elem[i].id && 'text' == document.getElementById(elem[i].id).type)
          $('#printFrame').contents().find("body #"+elem[i].id).val(document.getElementById(elem[i].id).value);


        if(elem[i].id && 'select-one' == document.getElementById(elem[i].id).type){
          var w = document.getElementById(elem[i].id).selectedIndex;
          var selected_text = document.getElementById(elem[i].id).options[w].text;
          $('#printFrame').contents().find("body #"+elem[i].id).val(selected_text);

          //                var eyeframe;
          //                eyeframe = document.getElementById('printFrame');
          //
          //                var eyeframedoc;
          //                eyeframedoc = eyeframe.contentWindow ? eyeframe.contentWindow.document: eyeframe.contentDocument;
          //
          //                if(eyeframedoc.getElementById(elem[i].id) != null)
          //                    eyeframedoc.getElementById(elem[i].id).innerHTML= "<option>"+selected_text+"</option>";
        }

        if(elem[i].id && 'checkbox' == document.getElementById(elem[i].id).type){
          //                alert(document.getElementById(elem[i].id).value) ;
          if(document.getElementById(elem[i].id).value && document.getElementById(elem[i].id).checked && document.getElementById(elem[i].id).value == "on"){

            var eyeframe;
            eyeframe = document.getElementById('printFrame');

            var eyeframedoc;
            eyeframedoc = eyeframe.contentWindow ? eyeframe.contentWindow.document: eyeframe.contentDocument;

            if(eyeframedoc.getElementById(elem[i].id) != null)
              eyeframedoc.getElementById(elem[i].id).checked= "checked";
          }
          //                        $('#printFrame').contents().find("body #"+elem[i].id).checked ;
        }
      }
      objFrame = window.frames[0];
      objFrame.focus();

      objFrame.print();
      return;
    });
  }

  function selectPassportType()
  {
    if(document.getElementById('passport_application_terms_id').checked==false)
    {
      alert('Please select “I Accept” checkbox.');
      document.getElementById('passport_application_terms_id').focus();
      return false;
    }
    document.getElementById('passport_application_terms_id').checked = false;
    //resolve edit application issue
<?php if (!$form->getObject()->isNew()) { ?>
    var updatedName = document.getElementById('passport_application_title_id').value+' '+document.getElementById('passport_application_first_name').value+' '+document.getElementById('passport_application_mid_name').value+' '+document.getElementById('passport_application_last_name').value;
    $('#updateNameVal').html(updatedName);
    //return pop2();
  <?php }?>
      return true;
    }
    //display pop mssage for confirmation
    function newApplicationAction()
    {
      var passportType;
      var passportRedirectURL;
      passportType = '<?php echo $passportType?>';
      switch(passportType)
      {

        case 'Standard ePassport Application Form':
          passportRedirectURL = 'estandard';
          break;
        case 'Official ePassport Application Form':
          passportRedirectURL = 'eofficial';
          break;
        case 'MRP Seamans Passport Application Form':
          passportRedirectURL = 'mrpseamans';
          break;
        case 'MRP Official Passport Application Form':
          passportRedirectURL = 'mrpofficial';
          break;
        case 'MRP Diplomatic Passport Application Form':
          passportRedirectURL = 'mrpdiplomatic';
          break;
        case 'MRP Standard Passport Application Form':
          passportRedirectURL = 'mrpstandard';
          break;

      }
      window.location = 'http://' + '<?php echo $_SERVER['HTTP_HOST']?>'+"<?php echo url_for('passport/'); ?>"+passportRedirectURL;
    }

    function updateExistingAction()
    {
      document.frm.submit();
    }

<?php if ($form->getObject()->isNew()) { ?>
  $(document).ready(function()
  {
    $("#passport_application_processing_country_id").ready(function(){
      //  var country = $(this).val();
      var country = document.getElementById('passport_application_processing_country_id').value;

      try{
        var obj=document.getElementById('passport_application_MRP_TYPE').value;
      }
      catch(e){
        var obj=null;
      }

      if(obj==null){
        var passport_type ='';
      }
      else{
        var passport_type ='MRPType';
      }

      try{
        var obj_official=document.getElementById('passport_application_OFFICIAL_TYPE').value;
      }
      catch(e){
        var obj_official=null;
      }
      if(obj_official==null){
        var passport_official ='';
      }
      else{
        var passport_official ='OFFICIALType';
      }

      var url = "<?php echo url_for('passport/getState/'); ?>";
      $("#passport_application_processing_state_id").load(url, {country_id: country, passport_type_id: passport_type, passport_official_type: passport_official} );

      if(country == 'NG')
      {

        $("#passport_application_processing_embassy_id").html( '<option value="">No Value</option>');

        $("#passport_application_processing_embassy_id_row").hide();
        $("#passport_application_processing_state_id_row").show();
        $("#passport_application_processing_passport_office_id_row").show();

      }
      else
      {
        $("#passport_application_processing_embassy_id_row").show();
        $("#passport_application_processing_state_id_row").hide();
        $("#passport_application_processing_passport_office_id_row").hide();

  <?php if ($form->getObject()->isNew()) { ?>
        var processing_embassy_id = '<?php echo $processing_embassy_id; ?>';
        if(processing_embassy_id != '' && !isNaN(processing_embassy_id)){
           var embassy_id = processing_embassy_id;
        }else{
           var embassy_id = '';
        }
    <?php if (!$form->isBound()){ ?>
            $("#passport_application_processing_passport_office_id").html( '<option value="">No Value</option>');
            var url = "<?php echo url_for('passport/getEmbassy/'); ?>";
            $("#passport_application_processing_embassy_id").load(url, {country_id: country, embassy_id: embassy_id});

      <?php }else{?>              
              $("#passport_application_processing_passport_office_id").html( '<option value="">No Value</option>');
              var url = "<?php echo url_for('passport/getEmbassy/'); ?>";
              $("#passport_application_processing_embassy_id").load(url, {country_id: country, embassy_id: embassy_id});
      <?php }
  }?>
        }
      });
      // End of change country

      // Start of change state
      $("#passport_application_processing_state_id").change(function(){
        var stateId = $(this).val();
        var passportType = '<?php echo $passportType; ?>';
        var url = "<?php echo url_for('passport/getOffice/'); ?>";
        $("#passport_application_processing_passport_office_id").load(url, {state_id: stateId, passportType: passportType});
      });


      if($("#passport_application_id").val()==''){
        try{
          var obj=document.getElementById('passport_application_MRP_TYPE').value;
        }
        catch(e){
          var obj=null;
        }

        if(obj==null){
          var passport_type ='';
        }
        else{
          var passport_type ='MRPType';
        }
        try{
          var obj_official=document.getElementById('passport_application_OFFICIAL_TYPE').value;
        }
        catch(e){
          var obj_official=null;
        }
        if(obj_official==null){
          var passport_official ='';
        }
        else{
          var passport_official ='OFFICIALType';
        }

        if(passport_official == 'OFFICIALType')
        {
          var url = "<?php echo url_for('passport/getState/'); ?>";
          var country = 'NG';
        }
      }
    });
  <?php } ?>
    // End of change country

    /* start of part first section, validating part first fields and set the values in part second*/
    //passport_application_passporttype_id value equals to 60 , for checking semens type form 60 is comming from global master table for passport type

    $(document).ready(function()
    {
      try{
        var obj_seamans=document.getElementById('passport_application_MRP_SEAMANS_TYPE').value;
      }
      catch(e){
        var obj_seamans=null;
      }

      if(obj_seamans==null){
        var passport_seamans ='';
      }
      else{
        var passport_seamans ='MRPSeamansType';
      }


      if( '<?php echo $passportType; ?>' == 'MRP Seamans Passport Application Form')
      {
<?php if (!$form->getObject()->isNew()) { ?>
      $('#mrpSeamansDivIdPart1').css('display','none');
      $('#mrpSeamansDivIdPart2').css('display','block');
  <?php }
else if ($form->isBound()){ ?>
      $('#mrpSeamansDivIdPart1').css('display','none');
      $('#mrpSeamansDivIdPart2').css('display','block');
  <?php } else { ?>
        $('#mrpSeamansDivIdPart2').css('display','none');
        $('#mrpSeamansDivIdPart1').css('display','block');

        var url = "<?php echo url_for('passport/getState/'); ?>";
        var country_id = 'NG';
        //GET STATE LIST
        $("#passport_application_processing_state_id").load(url, {country_id: country_id, passport_type_id: 'MRPType', passport_seamans_type: passport_seamans});
  <?php }  ?>
        document.getElementById('chkAgree').disabled = false;
        document.getElementById('chkAgree').checked = false;
        //make first name middle name and last name as read only field in case of semans field

        document.getElementById('passport_application_first_name').readOnly = true;
        document.getElementById('passport_application_mid_name').readOnly = true;
        document.getElementById('passport_application_last_name').readOnly = true;

      }
    });

    /*************************************  address block***************************/
    $(document).ready(function()
    {
      // Change of Permanent Address  country
      $("#passport_application_PermanentAddress_country_id").change(function(){
        var countryId = $(this).val();
        var url = "<?php echo url_for('passport/getState/'); ?>";
        $("#passport_application_PermanentAddress_state").load(url, {country_id: countryId,add_option: '-- Please Select --'});
        $("#passport_application_PermanentAddress_lga_id").html( '<option value="">--Please Select--</option>');
        $("#passport_application_PermanentAddress_district_id").html( '<option value="">--Please Select--</option>');
        document.getElementById('passport_application_PermanentAddress_postcode').value="";
      })

      // Change of Permanent Address state
      $("#passport_application_PermanentAddress_state").change(function(){
        var stateId = $(this).val();
        var url = "<?php echo url_for('passport/getLga/'); ?>";
        $("#passport_application_PermanentAddress_lga_id").load(url, {state_id: stateId});
        $("#passport_application_PermanentAddress_district_id").html( '<option value="">--Please Select--</option>');
        document.getElementById('passport_application_PermanentAddress_postcode').value="";
      });

      // Change of kin address country
      $("#passport_application_PassportKinAddress_country").change(function(){
        var countryId = $(this).val();
        var url = "<?php echo url_for('passport/getState/'); ?>";
        $("#passport_application_PassportKinAddress_state").load(url, {country_id: countryId,add_option: '-- Please Select --'});
        $("#passport_application_PassportKinAddress_lga_id").html( '<option value="">--Please Select--</option>');
        $("#passport_application_PassportKinAddress_district_id").html( '<option value="">--Please Select--</option>');
        document.getElementById('passport_application_PassportKinAddress_postcode').value="";
      })

      // Change of kin address state
      $("#passport_application_PassportKinAddress_state").change(function(){
        var stateId = $(this).val();
        var url = "<?php echo url_for('passport/getLga/'); ?>";
        $("#passport_application_PassportKinAddress_lga_id").load(url, {state_id: stateId});
        $("#passport_application_PassportKinAddress_district_id").html( '<option value="">--Please Select--</option>');
        document.getElementById('passport_application_PassportKinAddress_postcode').value="";
      });     

      // Change of mother address state
      $("#passport_application_PassportMotherAddress_state").change(function(){
        var stateId = $(this).val();
        var url = "<?php echo url_for('passport/getLga/'); ?>";
        $("#passport_application_PassportMotherAddress_lga_id").load(url, {state_id: stateId});
        $("#passport_application_PassportMotherAddress_district_id").html( '<option value="">--Please Select--</option>');
        document.getElementById('passport_application_PassportMotherAddress_postcode').value="";
      });
     

      // Change of father address state
      $("#passport_application_PassportFatherAddress_state").change(function(){
        var stateId = $(this).val();
        var url = "<?php echo url_for('passport/getLga/'); ?>";
        $("#passport_application_PassportFatherAddress_lga_id").load(url, {state_id: stateId});
        $("#passport_application_PassportFatherAddress_district_id").html( '<option value="">--Please Select--</option>');
        document.getElementById('passport_application_PassportFatherAddress_postcode').value="";
      });      
      
      // Change of reference1 address state
      $("#passport_application_PassportReferenceAddress1_state").change(function(){
        var stateId = $(this).val();
        var url = "<?php echo url_for('passport/getLga/'); ?>";
        $("#passport_application_PassportReferenceAddress1_lga_id").load(url, {state_id: stateId});
        $("#passport_application_PassportReferenceAddress1_district_id").html( '<option value="">--Please Select--</option>');
        document.getElementById('passport_application_PassportReferenceAddress1_postcode').value="";
      });
      
      // Change of reference2 address state
      $("#passport_application_PassportReferenceAddress2_state").change(function(){
        var stateId = $(this).val();
        var url = "<?php echo url_for('passport/getLga/'); ?>";
        $("#passport_application_PassportReferenceAddress2_lga_id").load(url, {state_id: stateId});
        $("#passport_application_PassportReferenceAddress2_district_id").html( '<option value="">--Please Select--</option>');
        document.getElementById('passport_application_PassportReferenceAddress2_postcode').value="";
      });
    });

    /************************************************  End of Address *****************************************/


    function showMrpSeamansDivIdPart2()
    {
      checkMrpSeamansPart2();
      document.getElementById('passport_application_first_name').value= document.getElementById('txtFName').value;
      document.getElementById('passport_application_mid_name').value =  document.getElementById('txtMName').value;
      document.getElementById('passport_application_last_name').value= document.getElementById('txtLName').value;
      document.getElementById('passport_application_first_name').readOnly = true;

      document.getElementById('passport_application_mid_name').readOnly = true;
      document.getElementById('passport_application_last_name').readOnly = true;



      document.getElementById('chkAgree').disabled = true;
      document.getElementById('txtFName').readOnly = true;
      document.getElementById('txtMName').readOnly = true;
      document.getElementById('txtLName').readOnly = true;
      document.getElementById('txtDay').readOnly = true;
      document.getElementById('ddlMonth').disabled = true;
      document.getElementById('txtYear').readOnly = true;
      document.getElementById('ContinueBtnId').disabled = true;

      //$('#mrpSeamansDivIdPart1').slideUp('slow',function(){
      $('#mrpSeamansDivIdPart2').slideDown('slow',function(){
      });
    }

    function checkMrpSeamansPart2()
    {
      if(document.getElementById('chkAgree').checked==true)
      {
        var flag = false;
        if(document.getElementById('txtFName').value=="")
        {
          document.getElementById('txtLPartFirstError').innerHTML = "<font class='error'>First Name is required.</font>";
          flag =true;
        }
        else if(flag==false)
          document.getElementById('txtLPartFirstError').innerHTML = "";

        if(document.getElementById('txtLName').value=="" && flag==false){
          document.getElementById('txtLPartFirstError').innerHTML = "<font class='error'>Last name is required.</font>";
          flag =true;
        }
        else if(flag==false)
          document.getElementById('txtLPartFirstError').innerHTML = "";

        if(document.getElementById('txtDay').value=="" && flag==false){
          document.getElementById('txtLPartFirstError').innerHTML = "<font class='error'>Current day is required.</font>";
          flag =true;
        }
        else if(flag==false)
        {
          var d = new Date()
          if(d.getDate()!=document.getElementById('txtDay').value)
          {
            document.getElementById('txtLPartFirstError').innerHTML = "<font class='error'>Current day is required.</font>";
            flag =true;
          } else
            document.getElementById('txtLPartFirstError').innerHTML = "";
        }

        if(document.getElementById('ddlMonth').value==0 && flag==false){
          document.getElementById('txtLPartFirstError').innerHTML = "<font class='error'>Current month is required.</font>";
          flag =true;
        }
        else if(flag==false)
        {
          var d = new Date();
          if((d.getMonth()+1)!=document.getElementById('ddlMonth').value)
          {
            document.getElementById('txtLPartFirstError').innerHTML = "<font class='error'>Current month is required.</font>";
            flag =true;
          } else
            document.getElementById('txtLPartFirstError').innerHTML = "";
        }



        if(document.getElementById('txtYear').value=="" && flag==false){
          document.getElementById('txtLPartFirstError').innerHTML = "<font class='error'>Current year is required.</font>";
          flag =true;
        }
        else if(flag==false)
        {
          var d = new Date()
          if(d.getFullYear()!=document.getElementById('txtYear').value)
          {
            document.getElementById('txtLPartFirstError').innerHTML = "<font class='error'>Current year is required.</font>";
            flag =true;
          }else
            document.getElementById('txtLPartFirstError').innerHTML = "";
        }

        if(flag ==true){
          document.getElementById('ContinueBtnId').disabled = true;
          document.getElementById('chkAgree').checked = false;
        }
        else
          document.getElementById('ContinueBtnId').disabled = false;
      }
      else
      {
        document.getElementById('txtLPartFirstError').innerHTML = "";
        document.getElementById('ContinueBtnId').disable = true;


      }
    }

    var RE_SSN = /^[A-Za-z \' \` \- \.]*$/; //allow special characters ` ' - . and space

    function validateNumber(frm)
    {
      if(!RE_SSN.test(frm.value))
        frm.value = frm.value.substring(0, frm.value.length - frm.value.length)
    }
</script>



  <?php
  $sf = sfContext::getInstance()->getUser();
  if($sf->hasFlash('error')){ ?>
  <div id="flash_error" class="alertBox" >
    <?php
    echo nl2br($sf->getFlash('error'));
    ?>
  </div>
  <?php }?>
<?php if ($form->getObject()->isNew()){?>

  <?php   }
?>



<?php if (!$form->getObject()->isNew()) {

  //echo ePortal_popup("<p>You are trying to EDIT an EXITING APPLICATION. </p>","<P>Following is the summary of name changes</p><h5>Application Id: ".$form->getObject()->getId()."</h5><h5> Reference No: ".$form->getObject()->getRefNo()."</h5><h5>&nbsp;</h5><h5>Name to update: ".$form->getObject()->getTitleId().' '.$form->getObject()->getFirstName().' '.$form->getObject()->getMidName().' '.$form->getObject()->getLastName()."</h5><h5>Changed Name: <span id='updateNameVal'></span></h5><h5>&nbsp;</h5><h5><a id='updateAction' href='#' onclick='updateExistingAction();'>Update Application</a>&nbsp;&nbsp;<a id='updateAction' href='#' onclick='newApplicationAction();'>Create New Application</a></h5> ");
  $popData = array(
  'appId'=>$form->getObject()->getId(),
  'refId'=>$form->getObject()->getRefNo(),
  'oldName'=>$form->getObject()->getTitleId().' '.$form->getObject()->getFirstName().' '.$form->getObject()->getMidName().' '.$form->getObject()->getLastName()
  );
  include_partial('global/editPop',$popData);

}?>

<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">
  <?php include_partial('global/innerHeading', array('heading' => 'Standard ePassport Application Form (STEP 2)')); ?>
  <div class="clear"></div>
  <div class="tmz-spacer"></div>
  <div style="display: block;" id="mrpSeamansDivIdPart2">
    <form name="frm" id="frm" action="<?php echo url_for('passport/'.($form->getObject()->isNew() ? $name : 'update').(!$form->getObject()->isNew() ? '?id='.$encriptedAppId : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?> class='dlForm' onsubmit="updateValue()">
      <?php if (!$form->getObject()->isNew()){?>
      <input type="hidden" name="sf_method" value="put" />
      <?php }?>
      <input type="hidden" name="countryId" value="<?php echo  $countryId ;?>" >
          <div id="printHTML">
              <div class="page_heading_new">Standard ePassport Application Form</div> 
          <div class="new_container">
                <div class="inst_bg">
                  <div class="inst_container1">
                    <ul>
                      <li>FOR ANY QUESTIONS AND/OR CONCERNS PLEASE CONTACT US AT <a href="mailto:support@swgloballlc.com" class="mail_link">support@swgloballlc.com</a></li>
                      <li>APPLICATION FEES PAID FOR PASSPORT IS REFUNDABLE IN CASES OF DOUBLE PAYMENT MISTAKENLY MADE FOR THE SAME APPLICATION OR FRAUDULENT USE OF CREDIT/DEBIT CARD. TO SEEK A REFUND PLEASE CONTACT  <a href="mailto:refund@swgloballlc.com" class="mail_link">refund@swgloballlc.com</a></li>
                      <li>PAYMENTS ARE VALID FOR SIX (6) MONTHS ONLY FROM THE PAYMENT DATE.</li>
                      <li>ONLY online payment is acceptable. </li>
                      <li>Anyone who pays otherwise and receives service is subject to prosecution and revocation of Passport.</li>
                      <li>If you have already completed an application, please check your application status rather than completing a duplicate application</li>
                    </ul>
                  </div>
                </div>
                <div class="clear"></div>
              <div class="bodyContent2">
                <div class="formH3">
                  <h2>General Information</h2>
                  <h2 class="pdLeft9">Permanent Address (in Nigeria)</h2>
                </div>
                <?php  // echo $form;die; ?>
                <div class="formContent_new formBrd">
                  <div class="lblName"><?php echo $form['title_id']->renderLabel(); ?><span class="redLbl">*</span></div>
                  <div class="lblField">
                    <?php echo $form['title_id']->render(); ?>
                    <div class="error_msg"> <?php echo $form['title_id']->renderError(); ?></div>
                  </div>

                  <div class="lblName"><?php echo $form['last_name']->renderLabel(); ?><span class="redLbl">*</span></div>
                  <div class="lblField">
                    <?php echo $form['last_name']->render(); ?>
                    <div class="error_msg"> <?php echo $form['last_name']->renderError(); ?></div>
                    <i><span class="small-text">(Please avoid placing suffixes with your surname)</span></i>
                  </div>
                  <div class="lblName"><?php echo $form['first_name']->renderLabel(); ?> <span class="redLbl">*</span></div>
                  <div class="lblField">
                    <?php echo $form['first_name']->render(); ?>
                    <div class="error_msg"> <?php echo $form['first_name']->renderError(); ?></div>
                  </div>
                  <div class="lblName"><?php echo $form['mid_name']->renderLabel(); ?> <span class="redLbl"></span></div>
                  <div class="lblField">
                    <?php echo $form['mid_name']->render(); ?>
                    <div class="error_msg"> <?php echo $form['mid_name']->renderError(); ?></div>
                  </div>

                  <div class="lblName"><?php echo $form['gender_id']->renderLabel(); ?> <span class="redLbl">*</span></div>
                  <div class="lblField">
                    <?php echo $form['gender_id']->render(); ?>
                    <div class="error_msg"> <?php echo $form['gender_id']->renderError(); ?></div>
                  </div>
                  <div class="lblName"><?php echo $form['date_of_birth']->renderLabel(); ?> <span class="redLbl">*</span></div>
                  <div class="lblField">
                    <?php echo $form['date_of_birth']->render(); ?>
                    <div class="error_msg"> <?php echo $form['date_of_birth']->renderError(); ?></div>
                  </div>
                  <div class="lblName"><?php echo $form['place_of_birth']->renderLabel(); ?><span class="redLbl">*</span></div>
                  <div class="lblField">
                    <?php echo $form['place_of_birth']->render(); ?>
                    <div class="error_msg"> <?php echo $form['place_of_birth']->renderError(); ?></div>
                  </div>
                </div>
                <div class="formContent_new pdLeft9">
                  <div class="lblName"><?php echo $form['PermanentAddress']['address_1']->renderLabel(); ?></div>
                  <div class="lblField">
                    <?php echo $form['PermanentAddress']['address_1']->render(); ?>
                    <div class="error_msg"> <?php echo $form['PermanentAddress']['address_1']->renderError(); ?></div>
                  </div>
                  <div class="lblName"><?php echo $form['PermanentAddress']['address_2']->renderLabel(); ?><span class="redLbl"></span></div>
                  <div class="lblField">
                    <?php echo $form['PermanentAddress']['address_2']->render(); ?>
                    <div class="error_msg"> <?php echo $form['PermanentAddress']['address_2']->renderError(); ?></div>
                  </div>
                  <div class="lblName"><?php echo $form['PermanentAddress']['city']->renderLabel(); ?></div>
                  <div class="lblField">
                    <?php echo $form['PermanentAddress']['city']->render(); ?>
                    <div class="error_msg"> <?php echo $form['PermanentAddress']['city']->renderError(); ?></div>
                  </div>

                  <div class="lblName"><?php echo $form['PermanentAddress']['country_id']->renderLabel(); ?></div>
                  <div class="lblField">
                    <?php echo $form['PermanentAddress']['country_id']->render(); ?>
                    <div class="error_msg"> <?php echo $form['PermanentAddress']['country_id']->renderError(); ?></div>
                  </div>
                  <div class="lblName"><?php echo $form['PermanentAddress']['state']->renderLabel(); ?></div>

                  <div class="lblField">
                    <?php echo $form['PermanentAddress']['state']->render(); ?>
                    <div class="error_msg"> <?php echo $form['PermanentAddress']['state']->renderError(); ?></div>
                  </div>
                  <div class="lblName"><?php echo $form['PermanentAddress']['lga_id']->renderLabel(); ?></div>
                  <div class="lblField" id="width_dropbox">
                    <?php echo $form['PermanentAddress']['lga_id']->render(); ?>
                    <div class="error_msg"> <?php echo $form['PermanentAddress']['lga_id']->renderError(); ?></div>
                  </div>


                  <div class="lblName"><?php echo $form['PermanentAddress']['district']->renderLabel(); ?><span class="redLbl"></span></div>
                  <div class="lblField">
                    <?php echo $form['PermanentAddress']['district']->render(); ?>
                    <div class="error_msg"> <?php echo $form['PermanentAddress']['district']->renderError(); ?></div>
                  </div>
                  

                  <div class="lblName printleft"><?php echo $form['PermanentAddress']['postcode']->renderLabel(); ?><span class="redLbl"></span></div>
                  <div class="lblField">
                    <?php echo $form['PermanentAddress']['postcode']->render(); ?>
                    <div class="error_msg"> <?php echo $form['PermanentAddress']['postcode']->renderError(); ?></div>
                  </div>

                </div>
                <div class="formH3">
                  <h2>Contact Information</h2>
                  <h2 class="pdLeft9">Other Information</h2>
                </div>
                <div class="formContent_new formBrd">
                  <div class="lblName"><?php echo $form['Contact Info']['contact_phone']->renderLabel(); ?><span class="redLbl">*</span></div>
                  <div class="lblField">
                    <?php echo $form['Contact Info']['contact_phone']->render(); ?>
                    <div class="error_msg"> <?php echo $form['Contact Info']['contact_phone']->renderError(); ?></div>
                    <i><span class="small-text">(e.g: +1234567891)</span></i>
                  
                  </div>
                  
                  <div class="lblName"><?php echo $form['email']->renderLabel(); ?><span class="redLbl">*</span></div>
                  <div class="lblField">
                    <?php echo $form['email']->render(); ?>
                    <div class="error_msg"> <?php echo $form['email']->renderError(); ?></div>
                  </div>
                  <div class="lblName"><?php echo $form['Contact Info']['mobile_phone']->renderLabel(); ?><span class="redLbl"></span></div>
                  <div class="lblField">
                    <?php echo $form['Contact Info']['mobile_phone']->render(); ?>
                    <div class="error_msg"> <?php echo $form['Contact Info']['mobile_phone']->renderError(); ?></div>
                    <i><span class="small-text">(e.g: +1234567891)</span></i>
                    
                  </div>

                  <div class="lblName"><?php echo $form['ContactAddress']['address_1']->renderLabel(); ?><span class="redLbl">*</span></div>
                  <div class="lblField">
                    <?php echo $form['ContactAddress']['address_1']->render(); ?>
                    <div class="error_msg"> <?php echo $form['ContactAddress']['address_1']->renderError(); ?></div>
                  </div>
                  <div class="lblName"><?php echo $form['ContactAddress']['address_2']->renderLabel(); ?><span class="redLbl"></span></div>
                  <div class="lblField">
                    <?php echo $form['ContactAddress']['address_2']->render(); ?>
                    <div class="error_msg"> <?php echo $form['ContactAddress']['address_2']->renderError(); ?></div>
                  </div>
                  <div class="lblName"><?php echo $form['ContactAddress']['city']->renderLabel(); ?><span class="redLbl">*</span></div>
                  <div class="lblField">
                    <?php echo $form['ContactAddress']['city']->render(); ?>
                    <div class="error_msg"> <?php echo $form['ContactAddress']['city']->renderError(); ?></div>
                  </div>
                  <div class="lblName"><?php echo $form['ContactAddress']['state']->renderLabel(); ?><span class="redLbl">*</span></div>
                  <div class="lblField">
                    <?php echo $form['ContactAddress']['state']->render(); ?>
                    <div class="error_msg"> <?php echo $form['ContactAddress']['state']->renderError(); ?></div>
                  </div>
                  <div class="lblName"><?php echo $form['ContactAddress']['country_id']->renderLabel(); ?><span class="redLbl">*</span></div>
                  <div class="lblField" id="width_dropbox">
                    <?php echo $form['ContactAddress']['country_id']->render(); ?>
                    <div class="error_msg"> <?php echo $form['ContactAddress']['country_id']->renderError(); ?></div>
                  </div>
                  <div class="lblName"><?php echo $form['ContactAddress']['postcode']->renderLabel(); ?><span class="redLbl"></span></div>
                  <div class="lblField">
                    <?php echo $form['ContactAddress']['postcode']->render(); ?>
                    <div class="error_msg"> <?php echo $form['ContactAddress']['postcode']->renderError(); ?></div>
                  </div>
                </div>
                <div class="formContent_new  pdLeft9">
                  <div class="lblName"><?php echo $form['maid_name']->renderLabel(); ?><span class="redLbl"></span></div>
                  <div class="lblField">
                    <?php echo $form['maid_name']->render(); ?>
                    <div class="error_msg"> <?php echo $form['maid_name']->renderError(); ?></div>
                  </div>
                  <div class="lblName"><?php echo $form['Details']['stateoforigin']->renderLabel(); ?><span class="redLbl">*</span></div>
                  <div class="lblField">
                    <?php echo $form['Details']['stateoforigin']->render(); ?>
                    <div class="error_msg"> <?php echo $form['Details']['stateoforigin']->renderError(); ?></div>
                  </div>
                  <div class="lblName"><?php echo $form['Contact Info']['nationality_id']->renderLabel(); ?><span class="redLbl">*</span></div>
                  <div class="lblField">
                    <?php echo $form['Contact Info']['nationality_id']->render(); ?>
                    <div class="error_msg"> <?php echo $form['Contact Info']['nationality_id']->renderError(); ?></div>
                  </div>
                  <div class="lblName"><?php echo $form['Contact Info']['home_town']->renderLabel(); ?><span class="redLbl">*</span></div>
                  <div class="lblField">
                    <?php echo $form['Contact Info']['home_town']->render(); ?>
                    <div class="error_msg"> <?php echo $form['Contact Info']['home_town']->renderError(); ?></div>
                  </div>
                  <div class="lblName"><?php echo $form['Details']['specialfeatures']->renderLabel(); ?><span class="redLbl"></span></div>
                  <div class="lblField">
                    <?php echo $form['Details']['specialfeatures']->render(); ?>
                    <div class="error_msg"> <?php echo $form['Details']['specialfeatures']->renderError(); ?></div>
                  </div>
                  <div class="lblName"><?php echo $form['next_kin']->renderLabel(); ?><span class="redLbl">*</span></div>
                  <div class="lblField">
                    <?php echo $form['next_kin']->render(); ?>
                    <div class="error_msg"> <?php echo $form['next_kin']->renderError(); ?></div>
                  </div>
                  <div class="lblName"><?php echo $form['relation_with_kin']->renderLabel(); ?><span class="redLbl">*</span></div>
                  <div class="lblField">
                    <?php echo $form['relation_with_kin']->render(); ?>
                    <div class="error_msg"> <?php echo $form['relation_with_kin']->renderError(); ?></div>
                  </div>
                  <div class="lblName"><?php echo $form['next_kin_phone']->renderLabel(); ?><span class="redLbl">*</span></div>
                  <div class="lblField">
                    <?php echo $form['next_kin_phone']->render(); ?>
                    <div class="error_msg"> <?php echo $form['next_kin_phone']->renderError(); ?></div>
                    <i><span class="small-text">(e.g: +1234567891)</span></i>
                  </div>
                </div>
                <div class="formH3">
                  <h2>Personal Information</h2>
                  <h2 class="pdLeft9">Next of Kin Address</h2>
                </div>
                <div class="formContent_new">
                  <div class="lblName"><?php echo $form['marital_status_id']->renderLabel(); ?><span class="redLbl">*</span></div>
                  <div class="lblField">
                    <?php echo $form['marital_status_id']->render(); ?>
                    <div class="error_msg"> <?php echo $form['marital_status_id']->renderError(); ?></div>
                  </div>
                  <div class="lblName"><?php echo $form['color_eyes_id']->renderLabel(); ?><span class="redLbl">*</span></div>
                  <div class="lblField">
                    <?php echo $form['color_eyes_id']->render(); ?>
                    <div class="error_msg"> <?php echo $form['color_eyes_id']->renderError(); ?></div>
                  </div>
                  <div class="lblName"><?php echo $form['color_hair_id']->renderLabel(); ?><span class="redLbl">*</span></div>
                  <div class="lblField">
                    <?php echo $form['color_hair_id']->render(); ?>
                    <div class="error_msg"> <?php echo $form['color_hair_id']->renderError(); ?></div>
                  </div>
                  <div id="passport_application_height_row">
                    <div class="lblName"><?php echo $form['height']->renderLabel(); ?><span class="redLbl">*</span></div>
                    <div class="lblField">
                      <?php echo $form['height']->render(); ?>
                      <div class="error_msg"> <?php echo $form['height']->renderError(); ?></div>
                    </div>
                  </div>
                </div>
                <div class="formContent_new pdLeft9 formBrd_left">

                  <div class="lblName"><?php echo $form['PassportKinAddress']['address_1']->renderLabel(); ?><span class="redLbl">*</span></div>
                  <div class="lblField">
                    <?php echo $form['PassportKinAddress']['address_1']->render(); ?>
                    <div class="error_msg"> <?php echo $form['PassportKinAddress']['address_1']->renderError(); ?></div>
                  </div>
                  <div class="lblName"><?php echo $form['PassportKinAddress']['address_2']->renderLabel(); ?><span class="redLbl"></span></div>
                  <div class="lblField">
                    <?php echo $form['PassportKinAddress']['address_2']->render(); ?>
                    <div class="error_msg"> <?php echo $form['PassportKinAddress']['address_2']->renderError(); ?></div>
                  </div>
                  <div class="lblName"><?php echo $form['PassportKinAddress']['city']->renderLabel(); ?><span class="redLbl">*</span></div>
                  <div class="lblField">
                    <?php echo $form['PassportKinAddress']['city']->render(); ?>
                    <div class="error_msg"> <?php echo $form['PassportKinAddress']['city']->renderError(); ?></div>
                  </div>
                  <div class="lblName"><?php echo $form['PassportKinAddress']['country_id']->renderLabel(); ?><span class="redLbl"></span></div>
                  <div class="lblField">
                    <?php echo $form['PassportKinAddress']['country_id']->render(); ?>
                    <div class="error_msg"> <?php echo $form['PassportKinAddress']['country_id']->renderError(); ?></div>
                  </div>
                  <div class="lblName printleft"><?php echo $form['PassportKinAddress']['state']->renderLabel(); ?><span class="redLbl">*</span></div>
                  <div class="lblField">
                    <?php echo $form['PassportKinAddress']['state']->render(); ?>
                    <div class="error_msg"> <?php echo $form['PassportKinAddress']['state']->renderError(); ?></div>
                  </div>
                  <div class="lblName"><?php echo $form['PassportKinAddress']['lga_id']->renderLabel(); ?><span class="redLbl">*</span></div>
                  <div class="lblField">
                    <?php echo $form['PassportKinAddress']['lga_id']->render(); ?>
                    <div class="error_msg"> <?php echo $form['PassportKinAddress']['lga_id']->renderError(); ?></div>
                  </div>
                  <div class="lblName"><?php echo $form['PassportKinAddress']['district']->renderLabel(); ?><span class="redLbl"></span></div>
                  <div class="lblField">
                    <?php echo $form['PassportKinAddress']['district']->render(); ?>
                    <div class="error_msg"> <?php echo $form['PassportKinAddress']['district']->renderError(); ?></div>
                  </div>
                  <div class="lblName"><?php echo $form['PassportKinAddress']['postcode']->renderLabel(); ?><span class="redLbl"></span></div>
                  <div class="lblField">
                    <?php echo $form['PassportKinAddress']['postcode']->render(); ?>
                    <div class="error_msg"> <?php echo $form['PassportKinAddress']['postcode']->renderError(); ?></div>
                  </div>

                </div>
                <!--  <div class="formH3">
<h2>ID Proof</h2>
</div>
<div class="formContent_new">
<div class="lblName"><?php //echo $form['scan_address_name']->renderLabel(); ?><span class="redLbl">*</span></div>
<div class="lblField">
<?php //echo $form['scan_address_name']->render(); ?>
<div class="error_msg"> <?php //echo $form['scan_address_name']->renderError(); ?></div>
</div>
<div class="inst_container1">
<div class="inst_top"></div>
<div class="inst_bg">
<div class="inst_container1" style="padding-left:15px;">
Please upload id proof image as per following instructions:<br>
<ul>
<li> If you are applying for your first passport<br>&nbsp;&nbsp;&nbsp;-&nbsp;Scanned copy of the address proof. In case you are minor, please provide address proof of your parent/gauradian.</li>
<li> If you are applying for passport renewal<br>&nbsp;&nbsp;&nbsp;-&nbsp;Scanned copy of your existing passport which should clearly show the first page (with photograph, full name, date of birth) and the address page (clearly showing your address).</li>
</ul>
<br>
<b>Please upload JPG/GIF/PNG images only - (DO NO UPLOAD IMAGE LARGER THAN 500 KB).</b>
</div>
</div>
<div class="inst_bottom"></div>
<div class="clear"></div>
</div>
                </div> -->

                <div class="formH3" >
                  <h2>Passport Processing Country, State and Office</h2>
                </div>
                <div class="formContent_new">
                  <div class="lblName"><?php echo $form['processing_country_id']->renderLabel(); ?><span class="redLbl">*</span></div>
                  <div class="lblField">
                    <?php echo $form['processing_country_id']->render(); ?>
                    <div class="error_msg"> <?php echo $form['processing_country_id']->renderError(); ?></div>
                  </div>
                  <div class="lblName"><?php echo $form['processing_embassy_id']->renderLabel(); ?><span class="redLbl"></span></div>
                  <div class="lblField">
                    <?php echo $form['processing_embassy_id']->render(); ?>
                    <div class="error_msg"> <?php echo $form['processing_embassy_id']->renderError(); ?></div>
                  </div>
                  <?php echo $form['id']->render(); ?>
                  <?php echo $form['passporttype_id']->render(); ?>
                  <?php echo $form['Details']['id']->render(); ?>
                  <?php echo $form['Contact Info']['id']->render(); ?>
                  <?php echo $form['PermanentAddress']['id']->render(); ?>
                  <?php echo $form['ContactAddress']['id']->render(); ?>
                  <?php echo $form['PassportKinAddress']['id']->render(); ?>



                </div>
                <div class="clear"></div>
                <div class="body_text">
                  <div class="lblName"><span class="redLbl"></span></div>
                  <div class="lblField">
                    <input type="checkbox" name="passport_application[terms_id]" id="passport_application_terms_id" class="input-height" />
                    <span class="redLbl">*</span> <span class="text-small">I Accept full responsibility for the information provided in this form.</span>

                    <div class="error_msg"> <?php echo $form['terms_id']->renderError(); ?></div>
                  </div>
				  <br/>
				  <div class="pixbr_new X50_new">
            <p class="text_hide"><font color="red">*</font> - Compulsory fields</p>
			<br/>
            <div class="border_top_new"></div>
            <p><b>Submission</b><br>
              Any false declaration on this form  may lead to the withdrawal of the passport and / or prosecution of the applicant<br>

            </p>
            <div class="Y20_new">
                <div class="passport_hide">
              <div class="l_new">Date: _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _</div>
              <div class="r_new">Signature / Thumb Impression: _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _</div>
              <div class="pixbr_new"></div>
                </div>
            </div>
            <div>
                <p><span class="notice-text">PLEASE NOTE: PRINT AND SIGN THIS APPLICATION AND TAKE IT WITH TWO (2) PASSPORT PHOTOGRAPHS TO THE SELECTED PASSPORT OFFICE FOR FURTHER PROCESSING.</span></p>
            </div>
          </div>

          </div>
            
              </div>
        <!--    </div>-->
     <!--     <div class="container_bottombg"></div>-->

          </div>
          <div align="center">

            <?php if ($form->getObject()->isNew()){?>
            <input type="submit" id="multiFormSubmit"  class="normalbutton" value="Submit Application" onClick= "return selectPassportType();"/>
            <?php  } else if (!$form->getObject()->isNew()){ ?>
            <input type="submit" id="multiFormSubmit"  class="normalbutton" value="Proceed" onClick= "return selectPassportType();"/>
            <?php } ?>            
          </div>

  <div class="clear"></div>
          <div class="warningArea">
            <h2>Warning</h2>
            <ul>
              <li>PLEASE ENSURE THAT ALL INFORMATION PROVIDED ON YOUR APPLICATION IS CORRECT BEFORE YOU PROCEED TO PAYMENT. YOUR APPLICATION CANNOT BE
                EDITED ONCE PAYMENT IS INITIATED.
              </li>
            </ul>
          </div>
  <!--      </div>-->
<!--      </div>-->
      <?php // echo ePortal_highlight('PLEASE ENSURE THAT ALL INFORMATION PROVIDED ON YOUR APPLICATION IS CORRECT BEFORE YOU PROCEED TO PAYMENT.  YOUR APPLICATION CANNOT BE EDITED ONCE PAYMENT IS INITIATED.','WARNING',array('class'=>'yellow'));?>
  </form>

<div id="popupContent">
    <div id="newAppId" class="no_display">
        Application with the same data is already <strong>Registered</strong> with us. Do you want to continue with the same application?
        <div class="btnArea"><input type="button" id="continue" value="Yes" />&nbsp;&nbsp;<input type="button" id="no" value="No" /> </div>
    </div>
    <div id="paidAppId" class="no_display">
        Application with the same data is already <strong>Registered</strong> and <strong>Paid</strong> with us.<br />Please try with another application.
        <div class="btnArea"><input type="button" id="cancel" value="Cancel" /> </div>
    </div>
</div>
</div>
<div id="backgroundPopup"></div>

<script>
  var countryID = '';
  countryID  = document.getElementById('passport_application_processing_country_id').value;
  if(countryID == 'NG')
  {

    $("#passport_application_processing_embassy_id").html( '<option value="">No Value</option>');

    $("#passport_application_processing_embassy_id_row").hide();
    $("#passport_application_processing_state_id_row").show();
    $("#passport_application_processing_passport_office_id_row").show();

  }
  else if(countryID!="")
  {
    $("#passport_application_processing_embassy_id_row").show();
    $("#passport_application_processing_state_id_row").hide();
    $("#passport_application_processing_passport_office_id_row").hide();
        
  }


  if($('#passport_application_date_of_birth_day').length > 0){
      $('#passport_application_date_of_birth_day').addClass('small_dropbox');
      $('#passport_application_date_of_birth_month').addClass('small_dropbox');
      $('#passport_application_date_of_birth_year').addClass('small_dropbox');
  }

function enableSubmitButton(){
  window.print();
  $('#multiFormSubmit').removeAttr('disabled');  
}

</script>
  </div>
</div>
</div>
<div class="content_wrapper_bottom"></div>
