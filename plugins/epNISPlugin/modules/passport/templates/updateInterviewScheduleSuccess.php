<h1>Applicant's Details</h1>
 <form action="#" method="POST" class="dlForm multiForm">
 <input type="hidden" name="id" value="<?php echo $passport_details[0]['id'] ?>">
  <div class='dlForm' >

    <fieldset class="bdr">
      <?php echo ePortal_legend("Interview Reschedule Details"); ?>
      <dl>
        <dt><label >Passport type:</label ></dt>
        <dd><?php echo $passport_details[0]['passportType']; ?>&nbsp;</dd>
      </dl>
      <?php if($passport_details[0]['PassportApplicationDetails']['request_type_id']) { ?>
      <dl>
        <dt><label >Request Type:</label ></dt>
        <dd><?php echo $passport_details[0]['PassportApplicationDetails']['request_type_id']; ?>&nbsp;</dd>
      </dl>
      <?php } ?>
      <dl>
        <dt><label >Date:</label ></dt>
        <dd><?php $datetime = date_create($passport_details[0]['created_at']); echo date_format($datetime, 'd/F/Y');?>&nbsp;</dd>
      </dl>
      <dl>
        <dt><label >Passport Application Id:</label ></dt>
        <dd><?php echo $passport_details[0]['id']; ?>&nbsp;</dd>
      </dl>
      <dl>
        <dt><label >Reference No:</label ></dt>
        <dd><?php echo $passport_details[0]['ref_no']; ?>&nbsp;</dd>
      </dl>

    </fieldset>
    <fieldset class="bdr">
      <?php echo ePortal_legend("Passport Processing"); ?>
      <dl>
        <dt><label >Preferred State:</label ></dt>
        <dd><?php if($passport_details[0]['passportPState']!='')echo $passport_details[0]['passportPState']; else echo 'Not Applicable'; ?>&nbsp;</dd>
      </dl>
      <dl>
        <dt><label >Passport Office:</label ></dt>
        <dd><?php if($passport_details[0]['passportPOffice']!='') echo $passport_details[0]['passportPOffice']; else echo 'Not Applicable'; ?>&nbsp;</dd>
      </dl>
      <dl>
        <dt><label >Old Interview Date:</label ></dt>
        <dd><?php $datetime = date_create($old_interview_date); echo date_format($datetime, 'd/F/Y'); ?>&nbsp;</dd>
      </dl>
      <dl>
        <dt><label >New Interview Date:</label ></dt>
        <dd>
          <?php
          if($passport_details[0]['ispaid'] == 1)
          {
            if(($passport_details[0]['paid_local_currency_amount'] == 0) && ($passport_details[0]['paid_dollar_amount'] == 0))
            {
              echo "Check the Passport Office / Embassy / High Commission";
            }else
            {
              $datetime = date_create($passport_details[0]['interview_date']); echo date_format($datetime, 'd/F/Y');
            }
          }else{
            echo "Available after Payment";
          }
          ?></dd>
      </dl>
    </fieldset>

    <div class="pixbr XY20">
      <center id="multiFormNav">
          <input type="button" name="back" value="Back" onclick="location='<?php echo url_for('passport/InterviewReschedule') ?>'"/>&nbsp;&nbsp;
        <input type="button" value="Print" onclick="window.print();">
      </center>
    </div>
  </div>
</form>
<br>