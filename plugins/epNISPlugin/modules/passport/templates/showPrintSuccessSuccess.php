<?php function getDateFormate($date)
{
 $dateFormate = date("d-m-Y", strtotime($date));

  return $dateFormate;
} ?>
<link rel="stylesheet" type="text/css" media="print" href="<?php  echo stylesheet_path('print');?>" />
  <div class="noscreen" style="margin-top: -5px; position:absolute;"> <img src="<?php echo image_path('nis.gif'); ?>" alt="Test" class="pd4" /> </div>
  <div >
    <?php if ($sf_user->hasFlash('error')): ?>
    <div id="flash_notice" class="alertBox" > <?php echo nl2br($sf_user->getFlash('error'));?></div>
    <?php  endif  ?>
  <div class="content_wrapper_top"></div>
  <div class="content_wrapper_bg">
    <div class="content_container">
      <?php include_partial('global/innerHeading', array('heading' => 'Standard ePassport Application Form')); ?>
      <div class="clear"></div>
      <div class="tmz-spacer"></div>
        <div id="printHTML">
          <div class="page_heading_new">Standard ePassport Application Form</div>
                <div class="inst_bg">
                  <div class="inst_container1">
                    <ul>
                      <li>FOR ANY QUESTIONS AND/OR CONCERNS PLEASE CONTACT US AT <a href="mailto:support@swgloballlc.com" class="mail_link">support@swgloballlc.com</a></li>
                      <li>APPLICATION FEES PAID FOR PASSPORT IS REFUNDABLE IN CASES OF DOUBLE PAYMENT MISTAKENLY MADE FOR THE SAME APPLICATION OR FRAUDULENT USE OF CREDIT/DEBIT CARD. TO SEEK A REFUND PLEASE CONTACT <a href="mailto:refund@swgloballlc.com" class="mail_link">refund@swgloballlc.com</a></li>
                      <li>PAYMENTS ARE VALID FOR SIX (6) MONTHS ONLY FROM THE PAYMENT DATE.</li>
                      <li>ONLY online payment is acceptable. </li>
                      <li>Anyone who pays otherwise and receives service is subject to prosecution and revocation of Passport.</li>
                      <li>If you have already completed an application, please check your application status rather than completing a duplicate application</li>
                    </ul>
                  </div>
                </div>
                <div class="clear"></div>
              <div class='confirmation-msg'>
          <div class="confirmation-msg1">
                  <p>
                    <center>
                <b class="red">BELOW IS YOUR APPLICATION ID AND REFERENCE NUMBER</b>
                    </center>
                  </p>
                  <p>
                    <center>
                <b class="red">KINDLY PRINT THE SCREEN AND KEEP IT SAFE</b>
                    </center>
                  </p>
                  <p>
                    <center>
                <b class="red">YOU WILL NEED THEM TO COMPLETE THE PROCESS AT THE EMBASSY, CONSULATE OR HIGH COMMISSION</b>
                    </center>
                  </p>
                </div>
                <h5>
                  <center>
              Application Id: <?php echo $passport_application[0]['id']; ?>
                  </center>
                </h5>
                <h5>
                  <center>
              Reference No: <?php echo $passport_application[0]['ref_no'] ?>
                  </center>
                </h5>
              </div>
        <div class="clear">&nbsp;</div>
        <table cellpadding="0" cellspacing="0" border="0" class="no_border_new" width="695">
                    <tr>
            <td width="347" height="25" valign="middle" id="form_new_heading">General Information</td>
            <td valign="middle" id="border_new_left" rowspan="35"></td>
            <td width="347" height="25" valign="middle" id="form_new_heading">Permanent Address (in Nigeria)</td>
          </tr>
          <tr>
            <td valign="top"><table width="345" border="0" cellspacing="0" cellpadding="0" class="no_border_new">
                <tr>
                  <td valign="top" height="5"></td>
                  <td valign="top" height="5"></td>
                </tr>
                <tr>
                  <td width="175" height="18" valign="top" class="label_name_new"><label for="passport_application_title_id">Title</label></td>
                  <td width="175" height="18" valign="top" class="lblfield_new"><span class="lblField">
                        <?php
                  if(isset($passport_application[0]['title_id']) && $passport_application[0]['title_id']!='')
                  {
                      echo "<input class='print_input_box' type='text' readonly=readonly value='".$passport_application[0]['title_id']."'>";

                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }
                  ?>
                        </span></td>
                    </tr>
                    <tr>
                  <td width="175" height="18" valign="top" class="label_name_new"><label for="passport_application_last_name">Last name (<i>Surname</i>) </label></td>
                  <td height="18" valign="top" class="lblfield_new"><span class="lblField">
                        <?php
                 if(isset($passport_application[0]['last_name']) && $passport_application[0]['last_name']!='')
                 {
                     echo "<input type='text' class='print_input_box' readonly=readonly value='".$passport_application[0]['last_name']."'>";

                 }
                 else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }
                  ?>
                        </span></td>
                    </tr>
                    <tr>
                  <td width="175" height="18" valign="top" class="label_name_new"><label for="passport_application_first_name">First name </label></td>
                  <td height="18" valign="top" class="lblfield_new"><span class="lblField">
                        <?php
                  if(isset($passport_application[0]['first_name']) && $passport_application[0]['first_name']!='')
                  {
                      echo "<input type='text' class='print_input_box' readonly=readonly value='".$passport_application[0]['first_name']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }

                  ?>
                        </span></td>
                    </tr>
                    <tr>
                  <td width="175" height="18" valign="top" class="label_name_new"><label for="passport_application_mid_name">Middle name</label>
                        <span class="redLbl"></span> </td>
                  <td height="18" valign="top" class="lblfield_new"><span class="lblField">
                        <?php
                  if(isset($passport_application[0]['mid_name']) && $passport_application[0]['mid_name']!='')
                  {
                      echo "<input type='text' class='print_input_box' readonly=readonly value='".$passport_application[0]['mid_name']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }
                  ?>
                        </span></td>
                    </tr>
                    <tr>
                  <td width="175" height="18" valign="top" class="label_name_new"><label for="passport_application_gender_id">Gender</label></td>
                  <td height="18" valign="top" class="lblfield_new"><span class="lblField">
                        <?php
                  if(isset($passport_application[0]['gender_id']) && $passport_application[0]['gender_id']!='')
                  {
                      echo "<input type='text' class='print_input_box' readonly=readonly value='".$passport_application[0]['gender_id']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }
                  ?>
                        </span></td>
                    </tr>
                    <tr>
                  <td height="18" valign="top" class="label_name_new"><label for="passport_application_date_of_birth">Date of birth (dd-mm-yyyy)</label></td>
                  <td height="18" valign="top" class="lblfield_new"><div class="lblField">
                          <?php
                  if(isset($passport_application[0]['date_of_birth']) && $passport_application[0]['date_of_birth']!='')
                  {
                      $explode = explode('-',getDateFormate($passport_application[0]['date_of_birth']));
                      echo "<input type='text' class='printdate' readonly=readonly value='".$explode[0]."'>";
                      echo "<input type='text' class='printdate' readonly=readonly value='".$explode[1]."'>";
                      echo "<input type='text' class='printyear' readonly=readonly value='".$explode[2]."'>";
                  }
                  else
                  {
                    echo "<input type='text' class='printdate' readonly=readonly value='--'>";
                    echo "<input type='text' class='printdate' readonly=readonly value='--'>";
                    echo "<input type='text' class='printyear' readonly=readonly value='--'>";
                  }
                  ?>
                        </div></td>
                    </tr>
                    <tr>
                  <td height="18" valign="top" class="label_name_new"><label for="passport_application_place_of_birth">Place of birth </label>
                      </td>
                  <td height="18" valign="top" class="lblfield_new"><span class="lblField">
                        <?php
                  if(isset($passport_application[0]['place_of_birth']) && $passport_application[0]['place_of_birth']!='')
                  {
                      echo "<input type='text' class='print_input_box' readonly=readonly value='".$passport_application[0]['place_of_birth']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }
                  ?>
                        </span></td>
                    </tr>
              </table></td>
            <td valign="top"><table width="345" border="0" cellspacing="0" cellpadding="0" class="no_border_new">
                    <tr>
                  <td valign="top" height="5"></td>
                  <td valign="top" height="5"></td>
                </tr>
                <tr>
                  <td width="175" height="18" valign="top" class="label_name_new"><label for="passport_application_PermanentAddress_address_1">Address 1</label>
                      </td>
                  <td width="175" height="18" valign="top" class="lblfield_new"><span class="lblField">
                        <?php
                  if(isset($passport_application[0]['PassportApplicationDetails']['PassportPermanentAddress']['address_1']) && $passport_application[0]['PassportApplicationDetails']['PassportPermanentAddress']['address_1']!='')
                  {
                      echo "<input type='text' class='print_input_box' readonly=readonly value='".$passport_application[0]['PassportApplicationDetails']['PassportPermanentAddress']['address_1']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }
                  ?>
                        </span></td>
                    </tr>
                    <tr>
                  <td width="175" height="18" valign="top" class="label_name_new"><label for="passport_application_PermanentAddress_address_2">Address 2</label>
                      </td>
                  <td height="18" valign="top" class="lblfield_new"><span class="lblField">
                        <?php
                    if(isset($passport_application[0]['PassportApplicationDetails']['PassportPermanentAddress']['address_2']) && $passport_application[0]['PassportApplicationDetails']['PassportPermanentAddress']['address_2']!='')
                    {
                        echo "<input type='text' class='print_input_box' readonly='readonly' value='".$passport_application[0]['PassportApplicationDetails']['PassportPermanentAddress']['address_2']."''>";
                    }
                    else
                    {
                        echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                    }
                    ?>
                        </span></td>
                    </tr>
                    <tr>
                  <td width="175" height="18" valign="top" class="label_name_new"><label for="passport_application_PermanentAddress_city">City</label>
                      </td>
                  <td height="18" valign="top" class="lblfield_new"><span class="lblField">
                        <?php
                if(isset($passport_application[0]['PassportApplicationDetails']['PassportPermanentAddress']['city']) && $passport_application[0]['PassportApplicationDetails']['PassportPermanentAddress']['city']!='')
                {
                    echo "<input type='text' class='print_input_box' readonly=readonly value='".$passport_application[0]['PassportApplicationDetails']['PassportPermanentAddress']['city']."''>";
                }
                else
                {
                 echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                }
                  ?>
                        </span></td>
                    </tr>
                    <tr>
                  <td width="175" height="18" valign="top" class="label_name_new"><label for="passport_application_PermanentAddress_country_id">Country</label>
                      </td>
                  <td height="18" valign="top" class="lblfield_new"><span class="lblField">
                        <?php

                  if($passport_application[0]['PassportApplicationDetails']['PassportPermanentAddress']['country_id'] !='' && $passport_application[0]['PassportApplicationDetails']['PassportPermanentAddress']['country_id'] == 'NG'){
                    echo "<input type='text' class='print_input_box' readonly=readonly value='Nigeria'>";
                  }else{
                    echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }

                  ?>
                        </span></td>
                    </tr>
                    <tr>
                  <td width="175" height="18" valign="top" class="label_name_new"><label for="passport_application_PermanentAddress_state">State</label>
                      </td>
                  <td height="18" valign="top" class="lblfield_new"><span class="lblField">
                        <?php
                  if(isset($passport_application[0]['perm_state']) && $passport_application[0]['perm_state']!='')
                  {
                    echo "<input type='text' class='print_input_box' readonly=readonly value='".$passport_application[0]['perm_state']."''>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }

                  ?>
                        </span></td>
                    </tr>
                    <tr>
                  <td height="18" valign="top" class="label_name_new"><label for="passport_application_PermanentAddress_lga_id">LGA</label>
                      </td>
                  <td height="18" valign="top" class="lblfield_new"><span class="lblField">
                        <?php
                 if(isset($passport_application[0]['perm_lga']) && $passport_application[0]['perm_lga']!='')
                 {
                    echo "<input type='text' class='print_input_box' readonly=readonly value='".$passport_application[0]['perm_lga']."''>";
                 }
                 else
                 {
                     echo "<input type='text' readonly=readonly value='--' class='print_input_box'>";
                 }
                  ?>
                        </span></td>
                    </tr>
                    <tr>
                  <td height="18" valign="top" class="label_name_new"><label for="passport_application_PermanentAddress_district">District</label>
                      </td>
                  <td height="18" valign="top" class="lblfield_new"><span class="lblField">
                        <?php
                  if(isset($passport_application[0]['PassportApplicationDetails']['PassportPermanentAddress']['district']) && $passport_application[0]['PassportApplicationDetails']['PassportPermanentAddress']['district']!='')
                  {
                    echo "<input type='text' class='print_input_box' readonly=readonly value='".$passport_application[0]['PassportApplicationDetails']['PassportPermanentAddress']['district']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }
                  ?>
                        </span></td>
                    </tr>
                    <tr>
                  <td height="18" valign="top" class="label_name_new"><label for="passport_application_PermanentAddress_postcode">Postcode</label>
                      </td>
                  <td height="18" valign="top" class="lblfield_new"><span class="lblField">
                        <?php
                  if(isset($passport_application[0]['PassportApplicationDetails']['PassportPermanentAddress']['postcode']) && $passport_application[0]['PassportApplicationDetails']['PassportPermanentAddress']['postcode']!='')
                  {
                    echo "<input type='text' class='print_input_box' readonly=readonly value='".$passport_application[0]['PassportApplicationDetails']['PassportPermanentAddress']['postcode']."''>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }

                  ?>
                        </span></td>
                    </tr>
              </table></td>
            <td valign="top">&nbsp;</td>
          </tr>
                    <tr>
            <td valign="top" height="5"></td>
            <td valign="top" height="5"></td>
            <td valign="top"></td>
          </tr>
          <tr>
            <td height="25" valign="middle" id="form_new_heading">Contact Information </td>
            <td height="25" valign="middle" id="form_new_heading">Other Information </td>
            <td valign="middle" id="form_new_heading">&nbsp;</td>
          </tr>
          <tr>
            <td valign="top"><table width="345" border="0" cellspacing="0" cellpadding="0" class="no_border_new">
                <tr>
                  <td valign="top" height="5"></td>
                  <td valign="top" height="5"></td>
                </tr>
                <tr>
                  <td width="175" height="18" valign="top" class="label_name_new"><label for="passport_application_Contact_Info_contact_phone">Contact phone</label>
                      </td>
                  <td width="175" height="18" valign="top" class="lblfield_new"><span class="lblField">
                        <?php
                if(isset($passport_application[0]['PassportApplicantContactinfo']['contact_phone']) && $passport_application[0]['PassportApplicantContactinfo']['contact_phone']!='')
                  {
                    echo "<input type='text' class='print_input_box' readonly=readonly value='".$passport_application[0]['PassportApplicantContactinfo']['contact_phone']."''>";
                  }
                  else
                  {
                    echo "<input type='text' class='print_input_box' value='--' readonly=readonly>" ;
                  }
                  ?>
                        </span></td>
                    </tr>
                    <tr>
                  <td width="175" height="18" valign="top" class="label_name_new"><label for="passport_application_email">Email</label>
                      </td>
                  <td height="18" valign="top" class="lblfield_new"><span class="lblField">
                        <?php
                  if(isset($passport_application[0]['email']) && $passport_application[0]['email']!='')
                  {
                      echo "<input type='text' class='print_input_box' readonly=readonly value='".$passport_application[0]['email']."''>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }
                  ?>
                        </span></td>
                    </tr>
                    <tr>
                  <td width="175" height="18" valign="top" class="label_name_new"><label for="passport_application_Contact_Info_mobile_phone">Mobile phone</label>
                      </td>
                  <td height="18" valign="top" class="lblfield_new"><span class="lblField">
                        <?php
                  if(isset($passport_application[0]['PassportApplicantContactinfo']['mobile_phone']) && $passport_application[0]['PassportApplicantContactinfo']['mobile_phone']!='')
                  {
                      echo  "<input type='text' class='print_input_box' readonly=readonly value='".$passport_application[0]['PassportApplicantContactinfo']['mobile_phone']."''>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }
                  ?>
                        </span></td>
                    </tr>
                    <tr>
                  <td width="175" height="18" valign="top" class="label_name_new"><label for="passport_application_ContactAddress_address_1">Address 1</label>
                      </td>
                  <td height="18" valign="top" class="lblfield_new"><span class="lblField">
                        <?php
                  if(isset($passport_application[0]['PassportApplicationDetails']['PassportContactAddress']['address_1']) && $passport_application[0]['PassportApplicationDetails']['PassportContactAddress']['address_1']!='')
                  {
                      echo "<input type='text' class='print_input_box' readonly=readonly value='".$passport_application[0]['PassportApplicationDetails']['PassportContactAddress']['address_1']."''>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }
                  ?>
                        </span></td>
                    </tr>
                    <tr>
                  <td width="175" height="18" valign="top" class="label_name_new"><label for="passport_application_ContactAddress_address_2">Address 2</label>
                      </td>
                  <td height="18" valign="top" class="lblfield_new"><span class="lblField">
                        <?php
                  if(isset($passport_application[0]['PassportApplicationDetails']['PassportContactAddress']['address_2']) && $passport_application[0]['PassportApplicationDetails']['PassportContactAddress']['address_2']!='')
                  {
                      echo "<input type='text' class='print_input_box' readonly=readonly value='".$passport_application[0]['PassportApplicationDetails']['PassportContactAddress']['address_2']."''>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }
                  ?>
                        </span></td>
                    </tr>
                    <tr>
                  <td height="18" valign="top" class="label_name_new"><label for="passport_application_ContactAddress_city">City</label>
                      </td>
                  <td height="18" valign="top" class="lblfield_new"><span class="lblField">
                        <?php
                  if(isset($passport_application[0]['PassportApplicationDetails']['PassportContactAddress']['city']) && $passport_application[0]['PassportApplicationDetails']['PassportContactAddress']['city']!='')
                  {
                      echo "<input type='text' class='print_input_box' readonly=readonly value='".$passport_application[0]['PassportApplicationDetails']['PassportContactAddress']['city']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }
                  ?>
                        </span></td>
                    </tr>
                    <tr>
                  <td height="18" valign="top" class="label_name_new"><label for="passport_application_ContactAddress_state">State</label>
                      </td>
                  <td height="18" valign="top" class="lblfield_new"><span class="lblField">
                        <?php
                  if(isset($passport_application[0]['PassportApplicationDetails']['PassportContactAddress']['state']) && $passport_application[0]['PassportApplicationDetails']['PassportContactAddress']['state']!='')
                  {
                      echo "<input type='text' class='print_input_box' readonly=readonly value='".$passport_application[0]['PassportApplicationDetails']['PassportContactAddress']['state']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }
                  ?>
                        </span></td>
                    </tr>
                    <tr>
                  <td height="18" valign="top" class="label_name_new"><label for="passport_application_ContactAddress_country_id">Country</label>
                      </td>
                  <td height="18" valign="top"><span class="lblField">
                        <?php
                  if(isset($passport_application[0]['contact_address-country']) && $passport_application[0]['contact_address-country']!='')
                  {
                      echo "<input type='text' class='print_input_box' readonly=readonly value='".$passport_application[0]['contact_address-country']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }
                  ?>
                        </span></td>
                    </tr>
                    <tr>
                  <td height="18" valign="top" class="label_name_new"><label for="passport_application_ContactAddress_postcode">Postcode</label>
                      </td>
                  <td height="18" valign="top"><span class="lblField">
                        <?php
                    if(isset($passport_application[0]['PassportApplicationDetails']['PassportContactAddress']['postcode']) && $passport_application[0]['PassportApplicationDetails']['PassportContactAddress']['postcode']!='')
                    {
                        echo "<input type='text' class='print_input_box' readonly=readonly value='".$passport_application[0]['PassportApplicationDetails']['PassportContactAddress']['postcode']."'>";
                    }
                    else
                    {
                        echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                    }
                  ?>
                        </span></td>
                    </tr>
              </table></td>
            <td valign="top"><table width="345" border="0" cellspacing="0" cellpadding="0" class="no_border_new">
                    <tr>
                  <td valign="top" height="5"></td>
                  <td valign="top" height="5"></td>
                </tr>
                <tr>
                  <td width="175" height="18" valign="top" class="label_name_new"><label for="passport_application_maid_name">Maiden name</label>
                      </td>
                  <td width="175" height="18" valign="top"><span class="lblField">
                        <?php
                 if(isset($passport_application[0]['maid_name']) && $passport_application[0]['maid_name']!='')
                 {
                    echo "<input type='text' class='print_input_box' readonly=readonly value='".$passport_application[0]['maid_name']."'>";
                 }
                 else
                 {
                     echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                 }
                  ?>
                        </span></td>
                    </tr>
                    <tr>
                  <td width="175" height="18" valign="top" class="label_name_new"><label for="passport_application_Details_stateoforigin">State of Origin</label>
                      </td>
                  <td height="18" valign="top"><span class="lblField">
                        <?php
                  if(isset($passport_application[0]['sName']) && $passport_application[0]['sName']!='')
                  {
                      echo "<input type='text' class='print_input_box' readonly=readonly value='".$passport_application[0]['sName']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }
                  ?>
                        </span></td>
                    </tr>
                    <tr>
                  <td width="175" height="18" valign="top" class="label_name_new"><label for="passport_application_Contact_Info_nationality_id">Nationality </label>
                      </td>
                  <td height="18" valign="top"><span class="lblField">
                        <?php
                if($passport_application[0]['PassportApplicantContactinfo']['nationality_id'] == 'NG'){
                  echo "<input type='text' class='print_input_box' readonly=readonly value='Nigeria'>";
                }else{
                  echo 'N/A';
                }

                  ?>
                        </span></td>
                    </tr>
                    <tr>
                  <td width="175" height="18" valign="top" class="label_name_new"><label for="passport_application_Contact_Info_home_town">Home town </label>
                      </td>
                  <td height="18" valign="top"><span class="lblField">
                        <?php
                if(isset($passport_application[0]['PassportApplicantContactinfo']['home_town']) &&$passport_application[0]['PassportApplicantContactinfo']['home_town']!='')
                {
                echo "<input type='text' class='print_input_box' readonly=readonly value='".$passport_application[0]['PassportApplicantContactinfo']['home_town']."'>";
                }
                else
                {
                    echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                }
                  ?>
                        </span></td>
                    </tr>
                    <tr>
                  <td width="175" height="18" valign="top" class="label_name_new"><label for="passport_application_Details_specialfeatures">Special Features</label>
                      </td>
                  <td height="18" valign="top"><span class="lblField">
                        <?php
                if(isset($passport_application[0]['PassportApplicationDetails']['specialfeatures']) && $passport_application[0]['PassportApplicationDetails']['specialfeatures']!='')
                {
                    echo "<input type='text' class='print_input_box' readonly=readonly value='".$passport_application[0]['PassportApplicationDetails']['specialfeatures']."'>";
                }
                else
                {
                    echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                }
                  ?>
                        </span></td>
                    </tr>
                    <tr>
                  <td width="175" height="18" valign="top" class="label_name_new"><label for="passport_application_next_kin">Next of kin name </label>
                      </td>
                  <td height="18" valign="top"><span class="lblField">
                        <?php
                  if(isset($passport_application[0]['next_kin']) && $passport_application[0]['next_kin']!='')
                  {
                      echo "<input type='text' class='print_input_box' readonly=readonly value='".$passport_application[0]['next_kin']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }
                  ?>
                        </span></td>
                    </tr>
                    <tr>
                  <td width="175" height="18" valign="top" class="label_name_new"><label for="passport_application_relation_with_kin">Relationship with next of kin</label>
                      </td>
                  <td height="18" valign="top"><span class="lblField">
                        <?php
                  if(isset($passport_application[0]['relation_with_kin']) && $passport_application[0]['relation_with_kin']!='')
                  {
                    echo "<input type='text' class='print_input_box' readonly=readonly value='".$passport_application[0]['relation_with_kin']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }
                  ?>
                        </span></td>
                    </tr>
                    <tr>
                  <td width="175" height="18" valign="top" class="label_name_new"><label for="passport_application_next_kin_phone">Contact number of next of kin</label>
                      </td>
                  <td height="18" valign="top"><span class="lblField">
                        <?php
                               if(isset($passport_application[0]['next_kin_phone']) && $passport_application[0]['next_kin_phone']!='')
                               {
                                   echo "<input type='text' class='print_input_box' readonly=readonly value='".$passport_application[0]['next_kin_phone']."'>";
                               }
                               else
                               {
                                   echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                               }
                  ?>
                        </span></td>
                    </tr>
              </table></td>
            <td valign="top">&nbsp;</td>
          </tr>
                    <tr>
            <td valign="top" height="5"></td>
            <td valign="top" height="5"></td>
            <td valign="top"></td>
          </tr>
          <tr>
            <td height="25" valign="middle" id="form_new_heading">Personal Information</td>
            <td height="25" valign="middle" id="form_new_heading">Next of Kin Address</td>
            <td valign="middle" id="form_new_heading">&nbsp;</td>
          </tr>
          <tr>
            <td valign="top"><table width="345" border="0" cellspacing="0" cellpadding="0" class="no_border_new">
              <tr>
                <td valign="top" height="5"></td>
                <td valign="top" height="5"></td>
              </tr>
              <tr>
                <td width="175" height="18" valign="top" class="label_name_new"><label for="passport_application_marital_status_id">Marital status </label></td>
                <td width="175" height="18" valign="top"><span class="lblField">
                  <?php
                  if(isset($passport_application[0]['marital_status_id']) && $passport_application[0]['marital_status_id']!='')
                  {
                      echo "<input type='text' class='print_input_box' readonly=readonly value='".$passport_application[0]['marital_status_id']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }
                  ?>
                </span></td>
              </tr>
              <tr>
                <td width="175" height="18" valign="top" class="label_name_new"><label for="passport_application_color_eyes_id">Color of eyes </label></td>
                <td height="18" valign="top"><span class="lblField">
                  <?php
                  if(isset($passport_application[0]['color_eyes_id']) && $passport_application[0]['color_eyes_id']!='')
                  {
                      echo "<input type='text' class='print_input_box' readonly=readonly value='".$passport_application[0]['color_eyes_id']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }
                  ?>
                </span></td>
              </tr>
              <tr>
                <td width="175" height="18" valign="top" class="label_name_new"><label for="passport_application_color_hair_id">Color of hair </label></td>
                <td height="18" valign="top"><span class="lblField">
                  <?php
                  if(isset($passport_application[0]['color_hair_id']) && $passport_application[0]['color_hair_id']!='')
                  {
                      echo "<input type='text' class='print_input_box' readonly=readonly value='".$passport_application[0]['color_hair_id']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' readonly=readonly value='--'>";
                  }
                  ?>
                </span></td>
              </tr>
              <tr id="id="passport_application_height_row="passport_application_height_row""">
                <td width="175" height="18" valign="top" class="label_name_new"><label for="passport_application_height">Height (in cm)</label></td>
                <td height="18" valign="top"><span class="lblField">
                  <?php
                  if(isset($passport_application[0]['height']) && $passport_application[0]['height']!='')
                  {
                      echo "<input type='text' class='print_input_box' readonly=readonly value='".$passport_application[0]['height']."'>";
                 }
                 else
                 {
                     echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                 }
                  ?>
                </span></td>
              </tr>
            </table></td>
            <td valign="top"><table width="345" border="0" cellspacing="0" cellpadding="0" class="no_border_new">
              <tr>
                <td valign="top" height="5"></td>
                <td valign="top" height="5"></td>
              </tr>
              <tr>
                <td width="175" height="18" valign="top" class="label_name_new"><label for="passport_application_PassportKinAddress_address_3">Address 1</label></td>
                <td width="175" height="18" valign="top"><span class="lblField">
                  <?php
                  if(isset($passport_application[0]['PassportKinAddress']['address_1']) && $passport_application[0]['PassportKinAddress']['address_1']!='')
                  {
                      echo "<input type='text' class='print_input_box' readonly=readonly value='".$passport_application[0]['PassportKinAddress']['address_1']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }
                  ?>
                </span></td>
              </tr>
              <tr>
                <td width="175" height="18" valign="top" class="label_name_new"><label for="passport_application_PassportKinAddress_address_4">Address 2</label></td>
                <td height="18" valign="top"><span class="lblField">
                  <?php
                  if(isset($passport_application[0]['PassportKinAddress']['address_2']) && $passport_application[0]['PassportKinAddress']['address_2']!='')
                  {
                    echo "<input type='text' class='print_input_box' readonly=readonly value='".$passport_application[0]['PassportKinAddress']['address_2']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }
                  ?>
                </span></td>
              </tr>
              <tr>
                <td width="175" height="18" valign="top" class="label_name_new"><label for="passport_application_PassportKinAddress_city2">City</label></td>
                <td height="18" valign="top"><span class="lblField">
                  <?php
                    if(isset($passport_application[0]['PassportKinAddress']['city']) && $passport_application[0]['PassportKinAddress']['city']!='')
                    {
                        echo "<input type='text' class='print_input_box' readonly=readonly value='".$passport_application[0]['PassportKinAddress']['city']."'>";
                    }
                    else
                    {
                        echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                    }
                  ?>
                </span></td>
              </tr>
              <tr>
                <td width="175" height="18" valign="top" class="label_name_new"><label for="passport_application_PassportKinAddress_country_id2">Country </label></td>
                <td height="18" valign="top"><span class="lblField">
                  <?php
                    if($passport_application[0]['PassportKinAddress']['country_id'] !='' && $passport_application[0]['PassportKinAddress']['country_id'] == 'NG'){
                    echo "<input type='text' class='print_input_box' readonly=readonly value='Nigeria'>";
                  }else{
                    echo 'N/A';
                  }
                    //echo $passport_application[0]['PassportKinAddress']['country_id'];
                  ?>
                </span></td>
              </tr>
              <tr>
                <td width="175" height="18" valign="top" class="label_name_new"><label for="passport_application_PassportKinAddress_state2">State</label></td>
                <td height="18" valign="top"><span class="lblField">
                  <?php
                        if(isset($passport_application[0]['kin_state']) && $passport_application[0]['kin_state']!='')
                        {
                            echo "<input type='text' class='print_input_box' readonly=readonly value='".$passport_application[0]['kin_state']."'>";
                        }
                        else
                        {
                            echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                        }
                  ?>
                </span></td>
              </tr>
              <tr>
                <td height="18" valign="top" class="label_name_new"><label for="passport_application_PassportKinAddress_lga_id2">LGA</label></td>
                <td height="18" valign="top"><span class="lblField">
                  <?php
                  if(isset($passport_application[0]['kin_lga']) && $passport_application[0]['kin_lga']!='')
                  {
                      echo "<input type='text' class='print_input_box' readonly=readonly value='".$passport_application[0]['kin_lga']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }
                  ?>
                </span></td>
              </tr>
              <tr>
                <td height="18" valign="top" class="label_name_new"><label for="passport_application_PassportKinAddress_district2">District</label></td>
                <td height="18" valign="top"><span class="lblField">
                  <?php
                  if(isset($passport_application[0]['PassportKinAddress']['district']) && $passport_application[0]['PassportKinAddress']['district']!='')
                  {
                    echo "<input type='text' class='print_input_box' readonly=readonly value='".$passport_application[0]['PassportKinAddress']['district']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }
                  ?>
                </span></td>
              </tr>
              <tr>
                <td height="18" valign="top" class="label_name_new"><label for="passport_application_PassportKinAddress_postcode2">Postcode</label></td>
                <td height="18" valign="top"><span class="lblField">
                  <?php
                  if(isset($passport_application[0]['PassportKinAddress']['postcode']) && $passport_application[0]['PassportKinAddress']['postcode']!='')
                  {
                    echo "<input type='text' class='print_input_box' readonly=readonly value='".$passport_application[0]['PassportKinAddress']['postcode']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }
                  ?>
                </span></td>
              </tr>
            </table></td>
            <td valign="top">&nbsp;</td>
          </tr>
          <tr>
            <td valign="top" id="form_new_heading">Passport Processing Country, State and Office</td>
            <td valign="top">&nbsp;</td>
            <td valign="top">&nbsp;</td>
          </tr>
          <tr>
            <td valign="top"><table width="345" border="0" cellspacing="0" cellpadding="0" class="no_border_new">
              <tr>
                <td valign="top" height="5"></td>
                <td valign="top" height="5"></td>
              </tr>
              <tr>
                <td width="175" height="18" valign="top" class="label_name_new"><label for="passport_application_processing_country_id">Processing Country</label></td>
                <td width="175" height="18" valign="top"><span class="lblField">
                  <?php
                  if(isset($passport_application[0]['passportPCountry']) && $passport_application[0]['passportPCountry']!='')
                  {
                      echo "<input type='text' class='print_input_box' readonly=readonly value='".$passport_application[0]['passportPCountry']."'>";
                  }
                  else
                  {
                  echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }
                  ?>
                </span></td>
              </tr>
              <tr>
                <td width="175" height="18" valign="top" class="label_name_new"><label for="passport_application_processing_embassy_id">Processing Embassy </label></td>
                <td height="18" valign="top"><span class="lblField">
                  <?php
                  if(isset($passport_application[0]['passportPEmbassy']) && $passport_application[0]['passportPEmbassy']!='')
                  {
                    echo "<input type='text' class='print_input_box' readonly=readonly value='".$passport_application[0]['passportPEmbassy']."'>";
                  }
                  else
                  {
                      echo "<input type='text' class='print_input_box' value='--' readonly=readonly>";
                  }
                  ?>
                </span></td>
              </tr>
            </table></td>
            <td valign="top">&nbsp;</td>
            <td valign="top">&nbsp;</td>
          </tr>
          <tr>
            <td valign="top">&nbsp;</td>
            <td valign="top">&nbsp;</td>
            <td valign="top">&nbsp;</td>
          </tr>
              
         
          
            </table>
<div class="clear"></div>
                <div class="body_text border_top">
                  <div class="lblName"><span class="redLbl"></span></div>
                  <div class="lblField">
                    <!--<input type="checkbox" name="passport_application[terms_id]" id="passport_application_terms_id" class="input-height" /> -->
            <span class="text-small"><strong>I Accept full responsibility for the information provided in this form.</strong></span> </div>
                </div>
          <div class="clear"></div>
          <?php if($passport_application[0]['status'] == 'New'){ ?>
             <div class="pixbr_new">
          <div class=""> <br/>
              <p class="noPrint"><b>Submission</b><br>
                <b>Any false declaration on this form  may lead to the withdrawal of the passport and / or prosecution of the applicant<br>
                </b> </p>
                <div class="clear noPrint">&nbsp;</div>
                <div class="border_top noPrint"></div>
          <div class="Y20_new">
            <div class="l_new">Date: _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _</div>
            <div class="r_new">Signature / Thumb Impression: _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _</div>
            <div class="pixbr_new"></div>
          </div>
            </div>
          <p class="top-padding printtxt">PLEASE NOTE: PRINT AND SIGN THIS APPLICATION AND TAKE IT WITH TWO (2) PASSPORT PHOTOGRAPHS TO THE SELECTED PASSPORT OFFICE FOR FURTHER PROCESSING.</p>
        </div>
          <?php } ?>
        </div>
        <?php if(!$mode){?>
      <div style="display:block" id="btn_pnc" align="center">
          <?php if($passport_application[0]['status'] == 'New'){ ?>
        <input type="button" class="noPrint normalbutton"  value="Print & Continue" onClick="showHideButton('<?php echo $printUrl; ?>')">
          <?php } else { ?>
        <input type="button" class="normalbutton noPrint"  value="Print" onClick="printApplication('<?php echo $printUrl; ?>');">
          <?php } ?>
        </div>
      <div style="display:none" id="btn_div" align="center">
        <input type="submit" disabled id="multiFormSubmit" class="normalbutton noPrint" value="Continue Application" onClick= "continueProcess();"/>
        </div>
        <?php } else{
          ?>
      <div  id="btn_pnc" align="center">
        <input type="button" class="normalbutton noPrint"  value="Print" onClick="printApplication('<?php echo $printUrl; ?>');">
        <input type="button" class="normalbutton noPrint"  value="Close" onClick="window.close()">
        </div>
        <?php } ?>
		<div class="print_note" align="center"><em>Please use 0.5 inch/ 12.7mm margin (on all sides of page) for best printing</em></div>
        <div class="warningArea">
          <h2>Warning</h2>
          <ul>
            <li>PLEASE ENSURE THAT ALL INFORMATION PROVIDED ON YOUR APPLICATION IS CORRECT BEFORE YOU PROCEED TO PAYMENT. YOUR APPLICATION CANNOT BE
              EDITED ONCE PAYMENT IS INITIATED. </li>
          </ul>
        </div>
      </div>
<script>

  function continueProcess(){
    window.location=  "<?php echo url_for('passport/show?chk='.$chk.'&p='.$p.'&id='.$encriptedAppId)?>";
  }

  function showHideButton(url){
    $('#btn_div').show();
    $('#btn_pnc').hide();
    //window.print();
    printApplication(url);
    $('#multiFormSubmit').removeAttr('disabled');
  }

  function printApplication(url){
      openPopUp(url);
  }
  
  function openPopUp(url){     
     window.open (url, "_blank","staus=1,toolbar=0,width=1000px,height=800px,location=0,menubar=0,scrollbars=1");
  }

  function enableSubmitButton(){
  window.print();
  $('#multiFormSubmit').removeAttr('disabled');

}
document.title = 'The Nigeria Immigration Service';
</script>
  </div>
</div>
<div class="content_wrapper_bottom"></div>
