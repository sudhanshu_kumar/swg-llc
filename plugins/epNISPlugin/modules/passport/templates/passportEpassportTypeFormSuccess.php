<script>
  function validateForm()
   {
     if(document.getElementById('passport_type').value=='')
       {
         alert('Please select passport type.');
         document.getElementById('passport_type').focus();
         return false;
       }
   }
  </script>

<h1>Apply For New Passport</h1>
<div class="XY20">
  <form name='passportEditForm' action='<?php echo url_for('passport/epassport');?>' method='post' class='dlForm multiForm'>
  <fieldset class="bdr">
  <?php echo ePortal_legend("Select a passport type"); ?>
    <dl>
        <dt><label >Passport Type:</label ></dt>
        <dd>
          <select name="passport_type" id='passport_type'>
            <option value="">-- Please Select --</option>
            <?php if(count($passportTypeArray) > 0 ){ foreach($passportTypeArray as $data){?>
              <option value="<?php echo $data['var_value'];?>"><?php echo $data['var_value'];?></option>
            <?php } } ?>
          </select><br/>&nbsp;
          </dd>
    </dl>
  </fieldset>
  <div class="XY20"><center><input type='submit' value='Start Application' onclick='return validateForm();'></center></div>
  </form>
</div>