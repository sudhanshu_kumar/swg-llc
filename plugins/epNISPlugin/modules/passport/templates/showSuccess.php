<div class="noscreen"> <img src="<?php echo image_path('nis.gif'); ?>" alt="Test" class="pd4" /> </div>
<?php use_javascript('jquery.printElement.js'); ?>
<link rel="stylesheet" type="text/css" media="print" href="<?php  echo stylesheet_path('printPopUpMessage');?>" />
<script>
$(document).ready(function(){
    $('#popPrint').click(function(){
        printSelection();
    });
});
/* Start Ashwani This function is using jquery print  */
    function printSelection(){
     printElem({});
   }
   function printElem(options){
     $('#mainDiv').printElement(options);
   }
</script>
<!--<div class="global_content4 clearfix">-->
<?php

//if($sf_request->getParameter('p') !='i') {?>
<?php
if($msgFlag){
    echo ePortal_popup("<div class='highlight red' id='flash_notice'><b>Important!!! You have to generate tracking number for this application to complete the payment process, by clicking on 'Proceed to Online Payment' from 'CART' page.</b></div>"."<div class='confirmation-msg'><font style='color: #FF0000'><center><b>BELOW IS YOUR APPLICATION ID AND REFERENCE NUMBER</b></center>","<p><center><b>PRINT THIS SCREEN OR WRITE DOWN THESE NUMBERS AND KEEP THEM SAFE</b></center></p><p><center><b>YOU WILL NEED THEM TO COMPLETE THE PROCESS AND/OR AT THE EMBASSY/HIGH COMMISSION</b></center></p></font><h5><center>Application Id: ".$passport_application[0]['id']."</center></h5><h5><center> Reference No: ".$passport_application[0]['ref_no']."</center></h5></div>",array('width'=>'650'));
}else{
    //echo ePortal_popup("<div class='confirmation-msg'><font style='color: #FF0000'><center><b>BELOW IS YOUR APPLICATION ID AND REFERENCE NUMBER</b></center>","<p><center><b>PRINT THIS SCREEN OR WRITE DOWN THESE NUMBERS AND KEEP THEM SAFE</b></center></p><p><center><b>YOU WILL NEED THEM TO COMPLETE THE PROCESS AND/OR AT THE EMBASSY/HIGH COMMISSION</b></center></p></font><h5><center>Application Id: ".$passport_application[0]['id']."</center></h5><h5><center> Reference No: ".$passport_application[0]['ref_no']."</center></h5></div>",array('width'=>'650'));
}

?>
<?php //if(isset($chk)){ echo "<script>pop();</script>"; } ?>
<?php //} ?>
<?php if($isStatusReport == 1) { ?>
<center>
  <?php if((isset($countFailedAttempt) && $countFailedAttempt > 0) && ($passport_application[0]['ispaid'] != 1))
  {
    echo ePortal_highlight("<b>You made $countFailedAttempt payment attempts.None of the payment attempt(s) returned a message.<br /> Please attempt(s) Payment ONLY If You Are Sure The Previous attempt(s) (IF ANY) Was Not Successful.</b>",'Payment Attempts Failed',array('class'=>'black'));
  }
  else {
    echo ($passport_application[0]['ispaid'] == 1)?"":ePortal_highlight("<b>No Previous Payment Attempt History Found</b>",'',array('class'=>'black'));
  }
}
?>
<!--div id="waitPayment" style="display:none;position:fixed;_position:absolute;top:20%;left:45%;padding:50px 40px;background:#fff;border:1px solid #999;z-index:99;line-height:30px;"> <?php echo image_tag('/images/ajax-loader.gif', array('alt' => 'Checking your Payment Attempts status.','border'=>0 )); ?><br/> Loading payment status...</div-->
</center>
<?php
$sf = sfContext::getInstance()->getUser();
if($sf->hasFlash('notice')){ ?>
    <div id="flash_notice" class="alertBox" >
<?php
    echo nl2br($sf->getFlash('notice'));
?>
</div>
<?php }?>
<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">
    <?php include_partial('global/innerHeading',array('heading'=>"Applicant's Details"));?>
<?php
$sf = sfContext::getInstance()->getUser();
if($sf->hasFlash('error')){ ?>
    <div id="flash_error" class="alertBox" >
<?php
    echo nl2br($sf->getFlash('error'));
?>
    </div>
    <br/>
<?php }?>
    <form action="<?php echo url_for('cart/list');// echo secure_url_for('payments/ApplicationPayment') ?>" method="POST" class="dlForm multiForm">
  <div  id="Reciept" >
    <?php if($show_details==1){ ?>
        <div class="clear"></div>
        <div class="tmz-spacer"></div>
        <table cellspacing="0" cellpadding="0" border="0" width="99%" class="show_table">
          <tbody>
            <tr>
              <td colspan="2" width="30%" valign="top" class="blbar"><strong><?php echo ePortal_legend("Profile"); ?></strong></td>
            </tr>
            <tr>
              <td width="30%" valign="top"><label >Full name:</label ></td>
              <td width="70%" valign="top"><?php echo ePortal_displayName($passport_application[0]['title_id'],$passport_application[0]['first_name'],@$passport_application[0]['mid_name'],$passport_application[0]['last_name']);?> </td>
            </tr>
            <tr>
              <td width="30%" valign="top"><label >Gender:</label ></td>
              <td width="70%" valign="top"><?php echo $passport_application[0]['gender_id']; ?> </td>
            </tr>
            <tr>
              <td><label >Date of birth:</label ></td>
              <td><?php $datetime = date_create($passport_application[0]['date_of_birth']); echo date_format($datetime, 'd/F/Y'); ?></td>
            </tr>
            <tr>
              <td><label >Email:</label ></td>
              <td><?php if($passport_application[0]['email']!='') echo $passport_application[0]['email']; else echo "--"?></td>
            </tr>
            <tr>
              <td><label >Country Of Origin:</label ></td>
              <td><?php echo $passport_application[0]['cName']; ?></td>
            </tr>
      <?php if($passport_application[0]['sName']!='') { ?>
            <tr>
              <td><label >State of origin:</label ></td>
              <td><?php echo $passport_application[0]['sName']; ?></td>
            </tr>
      <?php } ?>
            <?php //if($passport_application[0]['occupation']!=""){ echo $passport_application[0]['occupation'];}else{ echo '--';} ?>
    <?php } ?>
            <tr>
              <td colspan="2" class="blbar"><strong><?php echo ePortal_legend("Application Information"); ?></strong> </td>
            </tr>
      <?php if($show_details==1){ ?>
            <tr>
              <td><label >Passport type:</label ></td>
              <td><?php echo $passport_application[0]['passportType']; ?></td>
            </tr>
      <?php if(isset($passport_application[0]['PassportApplicationDetails'])) { ?>
            <tr>
              <td><label >Request Type:</label ></td>
              <td><?php echo $passport_application[0]['PassportApplicationDetails']['request_type_id']; ?></td>
            </tr>
      <?php } ?>
            <tr>
              <td><label >Date:</label ></td>
              <td><?php $applicationDatetime = date_create($passport_application[0]['created_at']); echo date_format($applicationDatetime, 'd/F/Y');?></td>
            </tr>
      <?php } ?>
            <tr>
              <td><label >Application Id:</label ></td>
              <td><?php echo $passport_application[0]['id']; ?></td>
            </tr>
            <tr>
              <td><label >Reference No:</label ></td>
              <td><?php echo $passport_application[0]['ref_no']; ?></td>
            </tr>
            <tr>
              <td colspan="2" class="blbar"><strong><?php echo ePortal_legend("Processing Information"); ?></strong></td>
            </tr>
      <?php if($show_details==1){ ?>
            <tr>
              <td><label >Country:</label ></td>
              <td><?php echo $passport_application[0]['passportPCountry']; ?>&nbsp;</td>
            </tr>
            <tr>
              <td><label >State:</label ></td>
              <td><?php if($passport_application[0]['passportPState']!='')echo $passport_application[0]['passportPState']; else echo 'Not Applicable'; ?></td>
            </tr>
            <tr>
              <td><label >Embassy:</label ></td>
              <td><?php if($passport_application[0]['passportPEmbassy']=='') echo 'Not Applicable'; else echo $passport_application[0]['passportPEmbassy']; ?></td>
            </tr>
            <tr>
              <td><label >Passport Office:</label ></td>
              <td><?php if($passport_application[0]['passportPOffice']!='') echo $passport_application[0]['passportPOffice']; else echo 'Not Applicable'; ?></td>
            </tr>
      <?php } ?>
      <?php if($show_payment==1){ ?>
            <tr>
              <td><label >Interview Date:</label ></td>
              <td><?php
          if($passport_application[0]['ispaid'] == 1)
          {
            if(($passport_application[0]['paid_local_currency_amount'] == 0) && ($passport_application[0]['paid_dollar_amount'] == 0))
            {
              echo "Check the Passport Office / Embassy / High Commission";
            }else
            {
              $interviewDatetime = date_create($passport_application[0]['interview_date']); echo date_format($interviewDatetime, 'd/F/Y');
            }
          }else{
            echo "Available after Payment";
          }
          ?></td>
            </tr>
      <?php } ?>
   <?php 
   $isPaid = $passport_application[0]['ispaid'];
   if($show_payment==1 || $isPaid==0){ ?>
          <table cellspacing="0" cellpadding="0" border="0" width="99%" class="show_table">
            <tr>
              <td colspan="2" class="blbar"><strong><?php echo ePortal_legend("Payment Information"); ?></strong></td>
            </tr>
          <?php          
//          if($passport_application[0]['ispaid'] == 1)
//          {
//            if($currencySymbol=='NGN')
//            {
//              echo "<tr><td width='30%'><label >Naira Amount:</label ></td><td>NGN ".$passport_application[0]['paid_local_currency_amount']."&nbsp;</td></tr>";
//            }
//          }else
//          {
//            if(isset($passport_fee[0]['naira_amount'])){ echo "<tr><td width='30%'><label >Naira Amount:</label ></td><td>NGN ".$passport_fee[0]['naira_amount']."&nbsp;</dd>";}else{ echo'<tr><td><label >Naira Amount:</label ></td><td>--&nbsp;</tr>';}
//          }
          ?>
          <?php
          if($passport_application[0]['ispaid'] == 1)
          {
            if($currencySymbol=='USD')
            {
              echo " </tr> <tr> <td><label >Dollar Amount:</label ></td><td>USD ".$passport_application[0]['paid_dollar_amount']."&nbsp;</td></tr>";
            }
          }else
          {             
            if(isset($passport_fee[0]['dollar_amount'])){ echo "  </tr> <tr> <td width='30%'><label >Amount:</label ></td><td>".$currencySymbol . $appAmount."&nbsp;</td></tr>";}else{ echo'</tr> <tr> <td><label >Amount:</label ></td><td>--</td></tr>';}
          }
          ?>
         <tr>
            <td><label >Payment Status:</label ></td>
            <td><?php
          if($passport_application[0]['ispaid'] == 1)
          {
            if(($passport_application[0]['paid_local_currency_amount'] == 0) && ($passport_application[0]['paid_dollar_amount'] == 0))
            {
              echo "This Application is Gratis (Requires No Payment)";
            }else
            {
              echo "Payment Done";
            }
          }else{
            echo "Available After Payment";
          }
          ?></td>
          </tr>
          <tr>
            <td><label >Payment Gateway:</label ></td>
            <td><?php
          if($passport_application[0]['ispaid'] == 1)
          {
            if(isset($PaymentGatewayType)){
              echo $PaymentGatewayType;
            }else{
              echo "None";
            }
          }else{
            echo "Available After Payment";
          }
          ?></td>
          </tr>
          <tr>
            <td><label >Amount Paid:</label ></td>
            <td><?php
          if($passport_application[0]['ispaid'] == 1)
          {
            if(($passport_application[0]['paid_local_currency_amount'] != 0) && ($passport_application[0]['paid_dollar_amount'] != 0))
            {
              echo html_entity_decode($currencySymbol).' '.$paidAmount;
            }else
            {
              echo "Not Applicable";
            }
          }else{
            echo "Available After Payment";
          }
          ?></td>
          </tr>
    <?php } ?>
          <?php if($passport_application[0]['status'] != 'New' && $passport_application[0]['status'] != 'Paid'){?>
          <tr>
            <td colspan="2" class="blbar"><?php echo ePortal_legend("Application Status"); ?></td>
          </tr>
          <tr>
            <td><label>Current Application Status:</label></td>
            <td><?php echo $passport_application[0]['status'] ;?></td>
          </tr><?php }?>
          </table>
          
          </tbody>
  
        </table>
        
    <div class="pixbr XY20">
      <center id="multiFormNav">
      <?php
      if(($paidAmount != 0)){
        if($passport_application[0]['ispaid'] == 1 && $show_details==1 && sfConfig::get('app_enable_pay4me_validation')==1){  ?>
            <table align="center" class="nobrd">
             <tr>
                <td>
        <input type="hidden" name="AppTypes"  value="<?php echo SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT(2)); ?>"/>
        <input type="hidden" name="AppId"  value="<?php echo SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT($passport_application[0]['id'])); ?>"/>
        <input type="hidden" name="RefId"  value="<?php echo SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT($passport_application[0]['ref_no'])); ?>"/>
        <?php if($data['pay4me'] && !$data['validation_number']){?>
        <input type="hidden" name="ErrorPage"  value="1"/>
        <input type="hidden" name="GatewayType"  value="1"/>
        <input type="hidden" name="id"  value="<?php echo SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT($passport_application[0]['id'])); ?>"/>
                  <input type="submit" class="normalbutton"  id="multiFormSubmit"  value='View your application payment status' onClick="this.form.action='<?php echo secure_url_for('passport/show'); ?>';" />
        <?php } else{?>
                  <input type="submit" class="normalbutton"  id="multiFormSubmit"  value='View your application payment status' onClick="this.form.action='<?php echo secure_url_for('visa/OnlineQueryStatus'); ?>';" />
                  </td>
	  </tr>
                </table>
        <?php }?>
        <?php } ?>
        <?php if($show_payment==1){ ?>
        <!--<input type="button" class="button"  value="Print Receipt" onclick="javascript:window.open('<?php echo url_for('passport/passportPaymentSlip?id='.$encriptedAppId) ?>','MyPage','width=750,height=700,scrollbars=1,resizable=yes');">&nbsp;
        <input type="button" class="button"  value="Print Acknowledgment Slip" onclick="javascript:window.open('<?php echo url_for('passport/passportAcknowledgmentSlip?id='.$encriptedAppId) ?>','MyPage','width=750,height=700,scrollbars=1,resizable=yes');">-->
        <?php } ?>
        <?php } ?>
      <?php if($passport_application[0]['ispaid']!=1 ) { ?>
            <div class="clear">&nbsp;</div>
          <table align="center" class="nobrd noPrint">
          <tr>
                
                <td align="center"><input type="submit" class="normalbutton"  id="multiFormSubmit"  value='Proceed To Checkout'>
                </td>
	  </tr>
            </table>
            <?php }

      ?>
      </center>
    </div>
  </div>
<?php  
 if($passport_application[0]['ispaid'] == 1){
     if($show_payment==1){
        echo ePortal_highlight('YOUR PAYMENT PROCESS WAS SUCCESSFULL! PLEASE PRINT YOUR ACKNOWLEDGMENT & RECEIPT SLIP ON NIS','',array('class'=>'green'));
     }
}else{
 
 // echo  ePortal_highlight('PLEASE CONFIRM YOUR ORDER BEFORE PROCEEDING TO PAYMENTS. NIS WILL NOT REFUND APPLICANT FOR A WRONG ORDER / PAYMENT','WARNING',array('class'=>'yellow'));
  echo ePortal_highlight('PLEASE ENSURE THAT ALL INFORMATION PROVIDED ON YOUR APPLICATION IS CORRECT BEFORE YOU PROCEED TO PAYMENT.  YOUR APPLICATION CANNOT BE EDITED ONCE PAYMENT IS INITIATED.<br/>','WARNING',array('class'=>'yellow'));
} ?>
<input type ="hidden" value="<?php echo $getAppIdToPaymentProcess;?>" name="appDetails" id="appDetails"/>
</form>
</div>
</div>
<div class="content_wrapper_bottom"></div>
