<?php include_stylesheets_for_form($form) ?>
<?php include_javascripts_for_form($form) ?>
<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">
  <?php
  $sf = sfContext::getInstance()->getUser();
  if($sf->hasFlash('error')){ ?>
  <div id="flash_error" class="alertBox" >
    <?php
    echo nl2br($sf->getFlash('error'));
    ?>
  </div>
  <?php }?>
<?php if ($form->getObject()->isNew()){?>
  <?php   }
?>
<?php if (!$form->getObject()->isNew()) {

  //echo ePortal_popup("<p>You are trying to EDIT an EXITING APPLICATION. </p>","<P>Following is the summary of name changes</p><h5>Application Id: ".$form->getObject()->getId()."</h5><h5> Reference No: ".$form->getObject()->getRefNo()."</h5><h5>&nbsp;</h5><h5>Name to update: ".$form->getObject()->getTitleId().' '.$form->getObject()->getFirstName().' '.$form->getObject()->getMidName().' '.$form->getObject()->getLastName()."</h5><h5>Changed Name: <span id='updateNameVal'></span></h5><h5>&nbsp;</h5><h5><a id='updateAction' href='#' onclick='updateExistingAction();'>Update Application</a>&nbsp;&nbsp;<a id='updateAction' href='#' onclick='newApplicationAction();'>Create New Application</a></h5> ");
  $popData = array(
  'appId'=>$form->getObject()->getId(),
  'refId'=>$form->getObject()->getRefNo(),
  'oldName'=>$form->getObject()->getTitleId().' '.$form->getObject()->getFirstName().' '.$form->getObject()->getMidName().' '.$form->getObject()->getLastName()
  );
  include_partial('global/editPop',$popData);

}?>
<?php //echo $form->renderGlobalErrors();?>
  <div style="display: block" id="mrpSeamansDivIdPart2">
   
        <?php include_partial('global/innerHeading', array('heading' => 'Standard ePassport Application Form  (STEP 1)')); ?>
              <div class="page_heading_new">Standard ePassport Application Form</div> 
              <div class="bodyContent2">
                <div class="formH3">
                  <h2>General Information</h2>
                  <h2 class="pdLeft9">Contact Information</h2>
                </div>      
                  
              </div>    
                 <form name="frm" id="frm" action="<?php echo url_for('passport/'.($form->getObject()->isNew() ? $name : 'update').(!$form->getObject()->isNew() ? '?id='.$encriptedAppId : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?> class='dlForm' >
      <?php if (!$form->getObject()->isNew()){?>
      <input type="hidden" name="sf_method" value="put" />
      <?php }?>
      <input type="hidden" name="countryId" id="countryId" value="<?php echo  $countryId ;?>" >  
                <div class="formContent_new formBrd">
                  <div class="lblName"><?php echo $form['last_name']->renderLabel(); ?><span class="redLbl">*</span></div>
              <div class="lblField"> <?php echo $form['last_name']->render(); ?>
                    <div class="error_msg" id="passport_application_last_name_error"> <?php echo $form['last_name']->renderError(); ?></div>
                <i><span class="small-text">(Please avoid placing suffixes with your surname)</span></i> </div>
                  <div class="lblName"><?php echo $form['first_name']->renderLabel(); ?> <span class="redLbl">*</span></div>
              <div class="lblField"> <?php echo $form['first_name']->render(); ?>
                    <div class="error_msg" id="passport_application_first_name_error"> <?php echo $form['first_name']->renderError(); ?></div>
                  </div>
                  <div class="lblName"><?php echo $form['mid_name']->renderLabel(); ?> <span class="redLbl"></span></div>
              <div class="lblField"> <?php echo $form['mid_name']->render(); ?>
                    <div class="error_msg" id="passport_application_first_name_error"> <?php echo $form['mid_name']->renderError(); ?></div>
                  </div>
                  <div class="lblName"><?php echo $form['gender_id']->renderLabel(); ?> <span class="redLbl">*</span></div>
              <div class="lblField"> <?php echo $form['gender_id']->render(); ?>
                    <div class="error_msg" id="passport_application_gender_id_error"> <?php echo $form['gender_id']->renderError(); ?></div>
                  </div>
                  <div class="lblName"><?php echo $form['date_of_birth']->renderLabel(); ?> <span class="redLbl">*</span></div>
              <div class="lblField"> <?php echo $form['date_of_birth']->render(); ?>
                    <div class="error_msg" id="passport_application_date_of_birth_error"> <?php echo $form['date_of_birth']->renderError(); ?></div>
                  </div>
                  <div class="lblName"><?php echo $form['place_of_birth']->renderLabel(); ?><span class="redLbl">*</span></div>
              <div class="lblField"> <?php echo $form['place_of_birth']->render(); ?>
                    <div class="error_msg" id="passport_application_place_of_birth_error"> <?php echo $form['place_of_birth']->renderError(); ?></div>
                  </div>
                </div>
                <div class="formContent_new pdLeft9">
                  <div class="lblName"><?php echo $form['Contact Info']['contact_phone']->renderLabel(); ?><span class="redLbl">*</span></div>
              <div class="lblField"> <?php echo $form['Contact Info']['contact_phone']->render(); ?>
                <div class="error_msg" id="passport_application_Contact_Info_contact_phone_error"> <?php echo $form['Contact Info']['contact_phone']->renderError(); ?> </div>
                <i><span class="small-text">(e.g: +1234567891)</span></i> </div>
                  <div class="lblName"><?php echo $form['email']->renderLabel(); ?><span class="redLbl">*</span></div>
              <div class="lblField"> <?php echo $form['email']->render(); ?>
                    <div class="error_msg" id="passport_application_email_error"> <?php echo $form['email']->renderError(); ?></div>
                  </div>
                </div>                
                <div class="clear"></div>
                <div class="body_text border_top"></div>                
              
          <div class="clear"></div>
        <div class="pixbr_new X50_new">
            <p class="text_hide"><font color="red">*</font> - Compulsory fields</p>
          </div>
        <div align="center">
          <input type="button" id="multiFormSubmit"  class="normalbutton" value="Next" onclick="checkForm();" />
          <input type="hidden" id="appId" name="appId"  class="normalbutton" value="" />
          <input type="hidden" id="appStatusFlag" name="appStatusFlag"  class="normalbutton" value="" />
          <input type="hidden" id="type" name="type"  class="normalbutton" value="<?php echo $type; ?>" />
          <input type="hidden" id="printUrl" name="printUrl"  class="normalbutton" value="" />
          </div>          
      </form>
        </div>
<div id="popupContent"></div>
</div>
</div>
<div class="content_wrapper_bottom"></div>
<div id="backgroundPopup"></div>
<div id="loading" class="no_display"><?php echo image_tag('/images/ajax-loader_1.gif',array('alt'=>'Loading ...', 'align' => 'absmiddle'));?><br/>&nbsp;&nbsp;Please wait...</div>
<script>
    function validateAppName(str)
    {
      var reg = /^[A-Za-z \-\.]*$/; //allow alphabet and spaces only...
      if(reg.test(str) == false) {
        return 1;
      }

      return 0;
    }

    function validateEmail(email) {

    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    if(reg.test(email) == false) {

      return 1;
    }

    return 0;
  }

    function validatePhone(str)
    {
      var reg = /^[0-9\+\-]*$/; //allow alphabet and spaces only...
      if(reg.test(str) == false) {
        return 1;
      }

      return 0;
    }
    function formValidate(){
        var first_name = jQuery.trim($('#passport_application_first_name').val());
        var last_name = jQuery.trim($('#passport_application_last_name').val());
        var gender_id = jQuery.trim($('#passport_application_gender_id').val());
        var day = jQuery.trim($('#passport_application_date_of_birth_day').val());
        var month = jQuery.trim($('#passport_application_date_of_birth_month').val());
        var year = jQuery.trim($('#passport_application_date_of_birth_year').val());
        var place_of_birth = jQuery.trim($('#passport_application_place_of_birth').val());        
        var email = jQuery.trim($('#passport_application_email').val());
        var contact_phone = jQuery.trim($('#passport_application_Contact_Info_contact_phone').val());

        var err  = 0;
        if(first_name == "")
        {
          $('#passport_application_first_name_error').html("Please enter first name");
          err = err+1;
        }else if(validateAppName(first_name) != 0)
        {
          $('#passport_application_first_name_error').html("Please enter valid first name");
          err = err+1;
        }
        else
        {
          $('#passport_application_first_name_error').html("");
        }

        if(last_name == "")
        {
          $('#passport_application_last_name_error').html("Please enter last name");
          err = err+1;
        }else if(validateAppName(last_name) != 0)
        {
          $('#passport_application_last_name_error').html("Please enter valid last name");
          err = err+1;
        }
        else
        {
          $('#passport_application_last_name_error').html("");
        }


        if(gender_id == "")
        {
          $('#passport_application_gender_id_error').html("Please select gender");
          err = err+1;
        }
        else
        {
          $('#passport_application_gender_id_error').html("");
        }

        var date_of_birth_flag = false;
        if(day == "")
        {
          date_of_birth_flag = true;
        }
        if(month == "")
        {
          date_of_birth_flag = true;
        }
        if(year == "")
        {         
          date_of_birth_flag = true;
        }

        var current_date  = new Date();
        
        if(date_of_birth_flag){
            $('#passport_application_date_of_birth_error').html("Please select date of birth");
            err = err+1;
        }else{
            
            var date_of_birth = new Date(year,month-1,day);
            if(date_of_birth.getTime() > current_date.getTime()) {
                $('#passport_application_date_of_birth_error').html("Date of birth cannot be future date");
                err = err+1;
            }else{
                $('#passport_application_date_of_birth_error').html("");
            }
        }

        if(place_of_birth == "")
        {
          $('#passport_application_place_of_birth_error').html("Please enter place of birth");
          err = err+1;
        }else if(validateAppName(place_of_birth) != 0)
        {
          $('#passport_application_place_of_birth_error').html("Please enter valid place of birth");
          err = err+1;
        }
        else
        {
          $('#passport_application_place_of_birth_error').html("");
        }

        if(email == "")
        {
            $('#passport_application_email_error').html("Please enter Email");
            err = err+1;
        }
        else if(validateEmail(email) != 0)
        {
            $('#passport_application_email_error').html("Please enter Valid Email");
            err = err+1;
        }
        else
        {
            $('#passport_application_email_error').html("");
        }
        if(contact_phone == ""){
            $('#passport_application_Contact_Info_contact_phone_error').html("Please enter contact phone");
            err = err+1;
        }else if(validatePhone(contact_phone) != 0)
        {
            $('#passport_application_Contact_Info_contact_phone_error').html("Please enter valid contact phone");
            err = err+1;
        }
        else if(contact_phone.length < 6)
        {
            $('#passport_application_Contact_Info_contact_phone_error').html("Phone number should be minimum 6 digits including +");
            err = err+1;
        }
        else{
            $('#passport_application_Contact_Info_contact_phone_error').html("");
        }

        if(err == 0){
            return true;
        }else{
            return false;
        }
        
    }

    function checkForm() {

        if(!formValidate()){
            return false;
        }

        $('#multiFormSubmit').attr("disabled","disabled");
        $('#multiFormSubmit').val("Please wait...");
        var url = "<?php echo url_for('passport/newApplicationAjax/'); ?>";

        centerPopup();
        var elementPos = findElementPosition('multiFormSubmit');
        elementPos = elementPos.split(',');
        $("#popupContent").css({
                "position": "absolute",
                "top": (parseInt(elementPos[1])-190)+'px',
                "left":(parseInt(elementPos[0])-90)+'px',
                "width":'230px'
        });        
        $("#popupContent").html($('#loading').html());
        loadPopup();
        
        $.ajax({
            type: "POST",
            url: url,
            data: $('#frm').serialize(),
            success: function (data) {
                data = jQuery.trim(data);                
                if(data == 'notfound'){                    
                    $('#appStatusFlag').val('new');
                    document.frm.submit();
                }else if(data == 'error'){
                    $('#popupContent').hide('slow');
                    alert('Oops! Some technical problem occur. Please try it again.');
                }else{
                    $('#popupContent').html(data);
                    $("#popupContent").css({
                        "width": '600px'
                    });

                    var elementPos = findElementPosition('multiFormSubmit');
                    elementPos = elementPos.split(',');
                    $("#popupContent").css({
                        "position": "absolute",
                        "top": (parseInt(elementPos[1])-290)+'px',
                        "left":(parseInt(elementPos[0])-240)+'px'
                    });

                    $('#multiFormSubmit').attr("disabled","");
                    $('#multiFormSubmit').val("Next");
                }
            }//End of success: function (data) {...
        }); 
        
    }//End of function checkForm() {...

    function openExistingApp(appId){
       $('#appStatusFlag').val('old');

       var countryId = $('#countryId').val();

       if(appId != '' && countryId != ''){
           var url = '<?php echo url_for('passport/newApplication');?>';
           $('#appId').val(appId);
           document.frm.action = url;
           document.frm.submit();
       }else{
           alert("application does not exist");
       }
    }

    function openNewApp(){
        var url = '<?php echo url_for('passport/estandard?type='.base64_encode($countryId));?>';
        location.href = url;
    }

    function printExistingApp(printUrl){
        location.href = printUrl;
    }
</script>