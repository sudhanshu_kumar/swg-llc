<?php

/**
 * passport actions.
 * @package    nisng
 * @subpackage passport
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12474 2008-10-31 10:41:27Z fabien $
 */
class passportActions extends sfActions {

    public function executeIndex(sfWebRequest $request) {
        $this->redirect('@homepage');
    }

    /**
     * @menu.description:: Apply for ePassport
     * @menu.text:: Apply for ePassport
     */
    public function executeEpassport(sfWebRequest $request) {
        if ($request->getPostParameters()) {
            $postValue = $request->getPostParameters();
            switch ($postValue['passport_type']) {
                case 'Standard ePassport':
                    $this->redirect('passport/estandard');
                    break;
                case 'Official ePassport':
                    $this->redirect('passport/eofficial');
                    break;
                default:
                    $this->redirect('passport/epassport');
                    break;
            }
        } else {
            $this->passportTypeArray = Doctrine::getTable('PassportAppType')->getPassportTypeNameArray('Standard ePassport', 'Official ePassport');
            $this->setTemplate('passportEpassportTypeForm');
        }
    }

    public function executeEstandard(sfWebRequest $request) {
        if ($request->getPostParameters()) {
            $this->form = new EstandardPassportForm();

            $this->forward404Unless($request->isMethod('post'));
            $this->countryId = $request->getParameter('countryId');
            
            $formVal = $request->getParameter($this->form->getName());
            $type = $request->getPostParameter('type');
            $this->appStatusFlag = $request->getPostParameter('appStatusFlag');

            if($this->appStatusFlag != ''){

                $passportTypeIdArray = Doctrine::getTable('PassportAppType')->getPassportTypeIdArray('Standard ePassport');
                $this->form->setDefault('passporttype_id', $passportTypeIdArray[0]['id']);
                $this->form->setDefault('processing_country_id', $this->countryId);

                $first_name = trim($request->getPostParameter('passport_application[first_name]'));
                $this->form->setDefault('first_name', $first_name);
                
                $mid_name = trim($request->getPostParameter('passport_application[mid_name]'));
                $this->form->setDefault('mid_name', $mid_name);

                $last_name = trim($request->getPostParameter('passport_application[last_name]'));
                $this->form->setDefault('last_name', $last_name);

                $gender_id = trim($request->getPostParameter('passport_application[gender_id]'));
                $this->form->setDefault('gender_id', array('default',$gender_id) );

                $day = trim($request->getPostParameter('passport_application[date_of_birth][day]'));
                $month = trim($request->getPostParameter('passport_application[date_of_birth][month]'));
                $year = trim($request->getPostParameter('passport_application[date_of_birth][year]'));
                $date_of_birth = date("Y-m-d H:i:s", mktime(0,0,0,date($month), date($day), date($year)));                
                $this->form->setDefault('date_of_birth', array('day' => $day, 'month' => $month, 'year' => $year));
                
                $place_of_birth = trim($request->getPostParameter('passport_application[place_of_birth]'));
                $this->form->setDefault('place_of_birth', $place_of_birth);

                $email = trim($request->getPostParameter('passport_application[email]'));
                $this->form->setDefault('email', $email);

                $contact_phone = trim($request->getPostParameter('passport_application[Contact Info][contact_phone]'));

            }


            if($this->appStatusFlag == ''){
                $formVal = $request->getParameter($this->form->getName());
                $this->processForm($request, $this->form);
                if ($this->form->getObject()->getid()) {
                    $id = "";
                    $id = (int) $this->form->getObject()->getid();
                    $request->setParameter('id', $id);
                    $this->forward($this->moduleName, 'show');
                }
            }
        } else {
            $request->setParameter('countryId', base64_decode($request->getParameter('type')));
            $this->form = new EstandardPassportForm();
            $passportTypeIdArray = Doctrine::getTable('PassportAppType')->getPassportTypeIdArray('Standard ePassport');
            $this->form->setDefault('passporttype_id', $passportTypeIdArray[0]['id']);
            $this->form->setDefault('processing_country_id', $request->getParameter('countryId'));

            //prefield email and phone number fields
            $loggedUserDetailsObj = $this->getUser()->getGuardUser()->getUserDetail();
            $email = $loggedUserDetailsObj->getEmail();
            $this->form->setDefault('email', $email);

            if ($request->hasParameter("type"))
                $this->countryId = base64_decode($request->getParameter('type'));
            else
                $this->forward404Unless($request->hasParameter("type"));

            $this->appStatusFlag = '';
        }

        $this->processing_embassy_id = $request->getPostParameter('passport_application[processing_embassy_id]');

        $this->setVar('formName', 'estandard');
        $this->setVar('passport_type', 'Standard ePassport Application Form');
        $this->setTemplate('new');
    }

    public function executeEofficial(sfWebRequest $request) {
        if ($request->getPostParameters()) {
            $this->form = new EofficialPassportForm();
            $this->forward404Unless($request->isMethod('post'));
            $this->processForm($request, $this->form);

            if ($this->form->getObject()->getid()) {
                $id = "";
                $id = (int) $this->form->getObject()->getid();
                $request->setParameter('id', $id);
                $this->forward($this->moduleName, 'show');
            }
        } else {
            $this->form = new EofficialPassportForm();
            $passportTypeIdArray = Doctrine::getTable('PassportAppType')->getPassportTypeIdArray('Official ePassport');
            $dataArr = Doctrine::getTable('PassportApplication')->getOfficialPassportStateoffice();
            if (isset($dataArr) && is_array($dataArr) && count($dataArr) > 0) {
                $this->form->setDefault('processing_state_id', $dataArr[0]['office_state_id']);
                $this->form->setDefault('processing_passport_office_id', $dataArr[0]['id']);
            }
            $this->form->setDefault('passporttype_id', $passportTypeIdArray[0]['id']);
        }
        $this->setVar('formName', 'eofficial');
        $this->setVar('passport_type', 'Official ePassport Application Form');
        $this->setTemplate('new');
    }

    /**
     * @menu.description:: Apply for MRP-Passport
     * @menu.text:: Apply for MRP-Passport
     */
    public function executeMrppassport(sfWebRequest $request) {
        $this->redirect('passport/mrpseamans');
        if ($request->getPostParameters()) {
            $postValue = $request->getPostParameters();
            switch ($postValue['passport_type']) {
                case 'MRP Standard':
                    $this->redirect('passport/mrpstandard');
                    break;
                case 'MRP Official':
                    $this->redirect('passport/mrpofficial');
                    break;
                case 'MRP Diplomatic':
                    $this->redirect('passport/mrpdiplomatic');
                    break;
                case 'MRP Seamans':
                    $this->redirect('passport/mrpseamans');
                    break;
                default:
                    $this->redirect('passport/mrppassport');
                    break;
            }
        } else {
            $this->passportTypeArray = Doctrine::getTable('PassportAppType')->getPassportTypeNameArrayMrp('MRP Standard', 'MRP Official', 'MRP Diplomatic', 'MRP Seamans');
            $this->setTemplate('passportStandardTypeForm');
        }
    }

    public function executeMrpstandard(sfWebRequest $request) {
        $this->redirect('passport/mrpseamans');
        if ($request->getPostParameters()) {
            $this->form = new StandardPassportForm();
            $this->forward404Unless($request->isMethod('post'));
            $this->processForm($request, $this->form);

            if ($this->form->getObject()->getid()) {
                $id = "";
                $id = (int) $this->form->getObject()->getid();
                $request->setParameter('id', $id);
                $this->forward($this->moduleName, 'show');
            }
        } else {
            $this->form = new StandardPassportForm();
            $passportTypeIdArray = Doctrine::getTable('PassportAppType')->getPassportTypeIdArray('MRP Standard');
            $this->form->setDefault('passporttype_id', $passportTypeIdArray[0]['id']);
        }
        $this->setVar('formName', 'mrpstandard');
        $this->setVar('passport_type', 'MRP Standard Passport Application Form');
        $this->setTemplate('new');
    }

    public function executeMrpofficial(sfWebRequest $request) {
        $this->redirect('passport/mrpseamans');
        if ($request->getPostParameters()) {
            $this->form = new OfficialPassportForm();
            $this->forward404Unless($request->isMethod('post'));
            $this->processForm($request, $this->form);

            if ($this->form->getObject()->getid()) {
                $id = "";
                $id = (int) $this->form->getObject()->getid();
                $request->setParameter('id', $id);
                $this->forward($this->moduleName, 'show');
            }
        } else {
            $this->form = new OfficialPassportForm();
            $passportTypeIdArray = Doctrine::getTable('PassportAppType')->getPassportTypeIdArray('MRP Official');
            $this->form->setDefault('passporttype_id', $passportTypeIdArray[0]['id']);
        }
        $this->setVar('formName', 'mrpofficial');
        $this->setVar('passport_type', 'MRP Official Passport Application Form');
        $this->setTemplate('new');
    }

    public function executeMrpdiplomatic(sfWebRequest $request) {
        $this->redirect('passport/mrpseamans');
        if ($request->getPostParameters()) {
            $this->form = new DiplomaticPassportForm();
            $this->forward404Unless($request->isMethod('post'));
            $this->processForm($request, $this->form);

            if ($this->form->getObject()->getid()) {
                $id = "";
                $id = (int) $this->form->getObject()->getid();
                $request->setParameter('id', $id);
                $this->forward($this->moduleName, 'show');
            }
        } else {
            $this->form = new DiplomaticPassportForm();
            $passportTypeIdArray = Doctrine::getTable('PassportAppType')->getPassportTypeIdArray('MRP Diplomatic');
            $this->form->setDefault('passporttype_id', $passportTypeIdArray[0]['id']);
        }
        $this->setVar('formName', 'mrpdiplomatic');
        $this->setVar('passport_type', 'MRP Diplomatic Passport Application Form');
        $this->setTemplate('new');
    }

    public function executeMrpseamans(sfWebRequest $request) {
        if ($request->getPostParameters()) {
            $this->form = new SeamansPassportForm();
            $this->forward404Unless($request->isMethod('post'));
            $this->processForm($request, $this->form);

            if ($this->form->getObject()->getid()) {
                $id = "";
                $id = (int) $this->form->getObject()->getid();
                $request->setParameter('id', $id);
                $this->forward($this->moduleName, 'show');
            }
        } else {
            $this->form = new SeamansPassportForm();
            $passportTypeIdArray = Doctrine::getTable('PassportAppType')->getPassportTypeIdArray('MRP Seamans');
            $this->form->setDefault('passporttype_id', $passportTypeIdArray[0]['id']);
            $nigeriaContryId = Doctrine::getTable('Country')->getNigeriaId();
            $this->form->setDefault('processing_country_id', $nigeriaContryId);
        }
        $this->setVar('formName', 'mrpseamans');
        $this->setVar('passport_type', 'MRP Seamans Passport Application Form');
        $this->setTemplate('new');
    }

    /**
     * @menu.description:: Passport Application Status
     * @menu.text:: Passport Application Status
     */
    public function executePassportPaymentStatus(sfWebRequest $request) {
        $this->setTemplate('paymentPassportStatus');
    }

    public function executeCheckPassportStatus(sfWebRequest $request) {
        $PassportRef = $request->getParameter('passport_app_refId');
        $PassportAppID = $request->getParameter('passport_app_id');
        $errorPage = $request->getParameter('ErrorPage');
        $GatewayType = $request->getParameter('GatewayType');
        $validation_number = $request->getParameter('validation_number');
        $pay4MeCheck = $request->getParameter('pay4meCheck');
        $passportReturn = Doctrine::getTable('PassportApplication')->getPassportAppIdRefId($PassportAppID, $PassportRef);
        $show_error = false;
        if (empty($PassportAppID)) {
            $show_error = true;
            $this->getUser()->setFlash('error', 'Please enter Application ID.', false);
        } else if (empty($PassportRef)) {
            $show_error = true;
            $this->getUser()->setFlash('error', 'Please enter Reference ID.', false);
        }
        if ($show_error == true) {
            if ($errorPage == 1) {
                $this->setTemplate('OnlineQueryStatus');
            } else {
                $this->setTemplate('paymentPassportStatus');
            }
            return;
        }
        if ($passportReturn) {
            if ($GatewayType > 0 && $errorPage == 1 && (sfConfig::get('app_enable_pay4me_validation'))) {  //viewing payment status
                $pay4me_validation = Doctrine::getTable('PassportApplication')->PassportValidatePay4mePayment($PassportAppID, $validation_number, $GatewayType, $pay4MeCheck);
                if ($pay4me_validation == false) {
                    $this->getUser()->setFlash('error', 'Payment Gateway or Validation number is not correct!! Please try again.', false);
                    if ($errorPage == 1) {
                        $this->forward('visa', 'OnlineQueryStatus');
                    }
                    return;
                }
            }

            $request->setParameter('statusReport', '1');
            // incrept application id for security//
            $encriptedAppId = SecureQueryString::ENCRYPT_DECRYPT($passportReturn[0]['id']);
            $encriptedAppId = SecureQueryString::ENCODE($encriptedAppId);

            $request->setParameter('id', $encriptedAppId);
            $request->setParameter('popup', 'i');
            $this->forward($this->getModuleName(), 'show');
        } else {
            $this->getUser()->setFlash('error', 'Application Not Found!! Please try again.', false);
        }

        if ($errorPage == 1) {
            $this->forward('visa', 'OnlineQueryStatus');
        } else {
            $this->setTemplate('paymentPassportStatus');
        }
    }

    public function executeDisplayPaymentStatus(sfWebRequest $request) {
        $this->passport_application = $this->getPassportRecord($request->getParameter('id'));
        $this->setVar('appId', $request->getParameter('id'));
        $this->forward404Unless($this->passport_application);
    }

    /**
     * @menu.description:: Edit Passport Application
     * @menu.text:: Edit Passport Application
     */
    public function executeEditPassportApplication(sfWebRequest $request) {
        $this->setTemplate('editPassportStaticForm');
    }

    public function executeCheckPassportAppRef(sfWebRequest $request) {
        $PassportRef['passport_app_id'] = $request->getParameter("passport_app_id");
        $PassportRef['passport_app_refId'] = $request->getParameter("passport_app_refId");
        $PassportRef['country_id'] = $request->getParameter("country_id");

        $passportReturn = Doctrine::getTable('PassportApplication')->getPassportAppIdRefId($PassportRef['passport_app_id'], $PassportRef['passport_app_refId']);

        //find if any payment request create
        $paymentHelper = new paymentHelper();
        $paymentRequestStatus = $paymentHelper->getPaymentRequestStatus($PassportRef['passport_app_id'], 'NIS PASSPORT');
        if (!$paymentRequestStatus) {
            $this->getUser()->setFlash('error', "Your application under payment awaited state!! you can not edit this application.");
            $this->setTemplate('editPassportStaticForm');
            return;
        }
        $passportReturn = Doctrine::getTable('PassportApplication')->getPassportAppIdRefId($PassportRef['passport_app_id'], $PassportRef['passport_app_refId']);

        if ($passportReturn) {
            if ($passportReturn[0]['ispaid'] == 1) {
                $this->getUser()->setFlash('error', "Your application is already in process. This application cann't be change.");
            } else {
                $request->setParameter('id', $passportReturn[0]['id']);
                $request->setParameter('countryId', $PassportRef['country_id']);
                $this->forward($this->moduleName, 'edit');
            }
        } else {
            $this->getUser()->setFlash('error', 'Application not found! Please check parameters and try again.', false);
        }
        $this->setTemplate('editPassportStaticForm');
    }

    public function executeShow(sfWebRequest $request) {
        $errorPage = $request->getParameter('ErrorPage');
        $GatewayType = $request->getParameter('GatewayType');

        $this->show_payment = 0;
        $this->show_details = 1;
        if (isset($GatewayType) && $errorPage == 1 && (sfConfig::get('app_enable_pay4me_validation'))) {  //viewing payment status
            $this->show_payment = 1;
            $this->show_details = 0;
        }
        if (sfConfig::get('app_enable_pay4me_validation') == 0) {
            $this->show_payment = 1;
            $this->show_details = 1;
        }
        $this->chk = $request->getParameter('chk');
        // Find out the Status Report is true or not
        $this->isStatusReport = 0;
        $this->isStatusReport = $request->getParameter('statusReport');
        // incrept application id for security//
        $getAppId = SecureQueryString::DECODE($request->getParameter('id'));
        $getAppId = SecureQueryString::ENCRYPT_DECRYPT($getAppId);

        $this->encriptedAppId = $request->getParameter('id');

        $this->gatewayType = $request->getParameter('gtype');

        $passport_application = $this->getPassportRecord($getAppId);

        $this->passport_application = $passport_application;
        //   check processing country payment gateway
        $paymentOptions = Doctrine::getTable('CountryPaymentMode')->getPaymentOptions($passport_application[0]['processing_passport_id']);
        if ($paymentOptions === 0) {
            $paymentOptions = Doctrine::getTable('CountryPaymentMode')->getPaymentOptions('ALL');
        }

        $msgFlag = false;
        if ($paymentOptions) {
            $arrPaymentOption = explode(',', $paymentOptions);
            $arrPaymentOptionCount = count($arrPaymentOption);
            if ($arrPaymentOptionCount <= 2) {
                for ($i = 0; $i < $arrPaymentOptionCount; $i++) {
                    if ($arrPaymentOption[$i] == 'Wt' || $arrPaymentOption[$i] == 'Mo') {
                        $msgFlag = true;
                    } else {
                        $msgFlag = false;
                        break;
                    }
                }//End of for($i=0;$i<$arrPaymentOptionCount;$i++){...
            }//End of if($arrPaymentOptionCount <=2){...
        }//End of if($paymentOptions){...

        $this->msgFlag = $msgFlag;
        //end code
        $passport_fee = Doctrine::getTable('PassportFee')
                        ->createQuery('a')
                        ->where('a.passporttype_id = ?', $passport_application[0]['passporttype_id'])
                        ->execute()->toArray(true);

        $this->passport_fee = $passport_fee;
        $this->paidAmount = 0;
        if (isset($this->passport_application[0]['payment_gateway_id'])) {
            $this->PaymentGatewayType = "";
        }
        //check payment
        //      $this->data = Doctrine::getTable('PaymentRequest')->getValidationNumberStatus($getAppId,'PASSPORT');
        if ($passport_application[0]['ispaid'] == 1) {
            $this->PaymentGatewayType = Doctrine::getTable('PaymentGatewayType')->getGatewayName($passport_application[0]['payment_gateway_id']);
            if ($this->PaymentGatewayType == 'eTranzact' || $this->PaymentGatewayType == 'Interswitch' || $this->PaymentGatewayType == 'Pay4me (Interswitch)' || $this->PaymentGatewayType == 'Pay4me (Bank)' || $this->PaymentGatewayType == 'Pay4me (eWallet)') {
                $this->paidAmount = $passport_application[0]['paid_local_currency_amount'];
                $this->currencySymbol = 'NGN';
                //$this->paidAmountInNiara = $passport_application[0]['paid_naira_amount'] ;
                //$this->paidAmountInDollar = 'Not Applicable';
            } else {
                if ($this->data['pay4me']) {
                    if ($this->data['currency'] == 'naira') {
                        $this->paidAmount = $passport_application[0]['paid_local_currency_amount'];
                        $this->currencySymbol = 'NGN';
                    } else {
                        $this->paidAmount = $passport_application[0]['paid_dollar_amount'];
                        $this->currencySymbol = 'USD';
                    }
                } else {
                    $this->paidAmount = $passport_application[0]['paid_dollar_amount'];
                    $this->currencySymbol = 'USD';
                    //$this->paidAmountInDollar = $passport_application[0]['paid_dollar_amount'] ;
                    //$this->paidAmountInNiara = 'Not Applicable';
                }
            }
        } else {
            //check if ispaid status is false then check on transaction responce table to check for any successfull transaction
            //if found then check status from gatways service if return status is true ,
            //then update application table and redirect again to this link
            //        $this->successResponceArray = array();
            //        $this->successResponceArray = FeeHelper::getPaymentSuccessHistory($this->passport_application[0]['id'],'passport');
        }

        //Get Previous Faild Attempt history
        //      if(isset($passport_application[0]['payment_gateway_id']))
        //      {
        //          $PaymentGatewayType = Doctrine::getTable('PaymentGatewayType')->getGatewayName($this->passport_application[0]['payment_gateway_id']);
        //
        //          $this->test = FeeHelper::paymentHistory($PaymentGatewayType,$this->passport_application[0]['id'],'passport');
        //          $previousHistoryDetails = $this->test;
        //          $this->countFailedAttempt = 0;
        //          if(count($previousHistoryDetails) > 0 )
        //          {
        //              foreach($previousHistoryDetails as $k => $v)
        //              {
        //                  if($v['status'] != 0)
        //                  {
        //                      $this->countFailedAttempt ++;
        //                  }
        //              }
        //          }
        //      }

        if (isset($this->passport_application[0]['processing_passport_office_id'])) {
            $passportPOfficeObj = Doctrine::getTable('PassportOffice')->find(array($this->passport_application[0]['processing_passport_office_id']));
            $this->passportPOffice = $passportPOfficeObj->getOfficeName();
        }
        if ($passport_application[0]['ispaid'] != 1) {
            if (/* If the naira amount is not set - means it doesn't exists */
                    ((!isset($passport_fee[0]['naira_amount'])) ||
                    /* OR if it exists but is zero */
                    (!$passport_fee[0]['naira_amount'])) && /* AND the dollar condition and naira conditions */
                    /* If the dollar amount is not set - means it doesn't exists */
                    ((!isset($passport_fee[0]['dollar_amount'])) ||
                    /* OR if dollar amount is zero */
                    (!$passport_fee[0]['dollar_amount']))) {

                //Call Notify Payment function
                $this->notifyNoAmountPayment($getAppId);
                $this->passport_application = $this->getPassportRecord($getAppId);
            }
        }

        if (isset($this->passport_application[0]['PassportApplicantContactinfo']['mobile_phone']) && $this->passport_application[0]['PassportApplicantContactinfo']['mobile_phone'] != "") {
            $mobile_no = $this->passport_application[0]['PassportApplicantContactinfo']['mobile_phone'];
        } else {
            $mobile_no = "";
        }

        if (isset($this->passport_application[0]['email']) && $this->passport_application[0]['email'] != "") {
            $email = $this->passport_application[0]['email'];
        } else {
            $email = "";
        }

        //check payment
        //      $this->data = Doctrine::getTable('PaymentRequest')->getValidationNumberStatus($getAppId,'PASSPORT');
        //SET SESSION VARIABLES FOR PAYMENT SYSTEM
        $applicant_name = $this->passport_application[0]['last_name'] . ', ' . $this->passport_application[0]['first_name'];
        $user = $this->getUser();
        $user->setAttribute("ref_no", $this->passport_application[0]['ref_no']);
        $user->setAttribute('applicant_name', $applicant_name);
        $user->setAttribute('app_id', $getAppId);
        $user->setAttribute('app_type', 'Passport');
        $user->setAttribute('email_id', $email);
        $user->setAttribute('processing_country', $this->passport_application[0]['processing_passport_id']);
        if ($this->passport_application[0]['is_email_valid'] == 1)
            $user->setAttribute('valid', true);
        else
            $user->setAttribute('valid', false);
        $user->setAttribute('mobile_no', $mobile_no);

        //application details sent as increpted to payment system
        $getAppIdToPaymentProcess = $getAppId . '~' . 'passport';
        //      require_once(sfConfig::get('sf_lib_dir').'/paygateways/google/model/paymentHistoryClass.php');
        //      $hisObj = new paymentHistoryClass($getAppId, "passport");
        //      $lastAttemptedTime = $hisObj->getDollarPaymentTime();
        //      $this->IsProceed = true;
        //      if($lastAttemptedTime){
        //        $this->IsProceed = false;
        //      }
        $getAppIdToPaymentProcess = SecureQueryString::ENCRYPT_DECRYPT($getAppIdToPaymentProcess);
        $this->getAppIdToPaymentProcess = SecureQueryString::ENCODE($getAppIdToPaymentProcess);

        if (isset($passport_fee[0]['naira_amount']))
            $user->setAttribute('naira_amount', $passport_fee[0]['naira_amount']);
        else
            $user->setAttribute('naira_amount', 0);

        if (isset($passport_fee[0]['dollar_amount']))
            $user->setAttribute('dollar_amount', $passport_fee[0]['dollar_amount']);
        else
            $user->setAttribute('dollar_amount', 0);

        /**
         * [WP: 087] => CR: 126
         */
        $destCurrencyId = CurrencyManager::getDestinationCurrency($this->passport_application[0]['processing_passport_id']);
        $currencyObj = Doctrine::getTable('Currency')->find($destCurrencyId);
        $this->currencySymbol = $currencyObj->getCodeiso();
        if(1 != $destCurrencyId){
            $this->appAmount = CurrencyManager::getConvertedAmount($passport_fee[0]['dollar_amount'], 1, $destCurrencyId);
        }else{            
            $this->appAmount = $passport_fee[0]['dollar_amount'];
        }



        $this->forward404Unless($this->passport_application);
    }

    public function executePassportAcknowledgmentSlip(sfWebRequest $request) {
        $paidAmount = "";
        $getAppId = SecureQueryString::DECODE($request->getParameter('id'));
        $getAppId = SecureQueryString::ENCRYPT_DECRYPT($getAppId);

        $this->gatewayType = '';

        $this->passport_application = $this->getPassportRecord($getAppId);
        $passport_fee = Doctrine::getTable('PassportFee')
                        ->createQuery('a')
                        ->where('a.passporttype_id = ?', $this->passport_application[0]['passporttype_id'])
                        ->execute()->toArray(true);
        $this->passport_fee = $passport_fee;
        if ($this->passport_application[0]['ispaid'] == 1) {
            $datetime = $this->passport_application[0]['interview_date'];
        } else {
            $datetime = "Available after Payment";
        }
        //Get Payment status
        $PaymentGatewayType = "";

        $payment_status = "";

        $payment_status = "";
        if (count($this->passport_fee) > 0) {
            $dollarAmt = $this->passport_fee[0]['dollar_amount'];
            $nairaAmt = $this->passport_fee[0]['naira_amount'];
        }

        if ($this->passport_application[0]['ispaid'] == 1) {
            $dollarAmt = $this->passport_application[0]['paid_dollar_amount'];
            $nairaAmt = $this->passport_application[0]['paid_local_currency_amount'];
            $PaymentGatewayType = Doctrine::getTable('PaymentGatewayType')->getGatewayName($this->passport_application[0]['payment_gateway_id']);
            if (($this->passport_application[0]['paid_local_currency_amount'] == 0) && ($this->passport_application[0]['paid_dollar_amount'] == 0)) {
                $payment_status = "This Application is Gratis (Requires No Payment)";
                $paidAmount = 'Not Applicable';
            } else {
                if ($PaymentGatewayType == 'eTranzact' || $PaymentGatewayType == 'Interswitch' || $PaymentGatewayType == 'Pay4me (Interswitch)' || $PaymentGatewayType == 'Pay4me (Bank)' || $PaymentGatewayType == 'Pay4me (eWallet)') {
                    $paidAmount = $this->passport_application[0]['paid_local_currency_amount'];
                    if ($paidAmount != 0.00) {
                        $nairaAmt = $paidAmount;
                        $dollarAmt = 'Not Applicable';
                        $paidAmount = "NGN&nbsp;" . $paidAmount;
                    } else {
                        $nairaAmt = "Not Applicable";
                        $dollarAmt = 'Not Applicable';
                        $paidAmount = 'Not Applicable';
                    }
                } else {

                    $this->data = Doctrine::getTable('PaymentRequest')->getValidationNumberStatus($getAppId, 'PASSPORT');
                    if (isset($this->data['pay4me']) && isset($this->data['currency']) && $this->data['currency'] == 'naira') {
                        $paidAmount = $this->passport_application[0]['paid_local_currency_amount'];
                        if ($paidAmount != 0.00) {
                            $nairaAmt = $paidAmount;
                            $dollarAmt = 'Not Applicable';
                            $paidAmount = "NGN&nbsp;" . $paidAmount;
                        } else {
                            $nairaAmt = "Not Applicable";
                            $dollarAmt = 'Not Applicable';
                            $paidAmount = 'Not Applicable';
                        }
                    } else {
                        $paidAmount = $this->passport_application[0]['paid_dollar_amount'];
                        if ($paidAmount != 0.00) {
                            $dollarAmt = $paidAmount;
                            $nairaAmt = "Not Applicable";
                            $paidAmount = "USD&nbsp;" . $paidAmount;
                        } else {
                            $dollarAmt = "Not Applicable";
                            $nairaAmt = "Not Applicable";
                            $paidAmount = "Not Applicable";
                        }
                    }
                }

                //        $paidAmount = ($dollarAmt!="")?"USD ".$dollarAmt:($nairaAmt!="")?"NGN ".$nairaAmt:0;
                $payment_status = "Payment Done";
            }
        }
        if (isset($this->passport_application[0]['next_kin_address']) && $this->passport_application[0]['next_kin_address'] != "") {
            $kinAddress = $this->passport_application[0]['next_kin_address'];
        }
        else
            $kinAddress = "";

        $isGratis = false; //set gratis false for all passport application
        //Pass data to view
        $data = array(
            "profile" => array(
                "title" => $this->passport_application[0]['title_id'],
                "first_name" => $this->passport_application[0]['first_name'],
                "middle_name" => $this->passport_application[0]['mid_name'],
                "last_name" => $this->passport_application[0]['last_name'],
                "date_of_birth" => $this->passport_application[0]['date_of_birth'],
                "maiden_name" => "",
                "sex" => $this->passport_application[0]['gender_id'],
                "country_origin" => $this->passport_application[0]['cName'],
                "state_origin" => $this->passport_application[0]['sName'],
                "occupation" => $this->passport_application[0]['occupation']),
            "contact_info" => array(
                // "permanent_address"=>$this->passport_application[0]['PassportApplicationDetails']['permanent_address'],
                "phone" => $this->passport_application[0]['PassportApplicantContactinfo']['contact_phone'],
                "email" => $this->passport_application[0]['email'],
                "mobile" => $this->passport_application[0]['PassportApplicantContactinfo']['mobile_phone'],
                "home_town" => $this->passport_application[0]['PassportApplicantContactinfo']['home_town'],
                "country_origin" => $this->passport_application[0]['cName'],
                "state_origin" => $this->passport_application[0]['PassportApplicantContactinfo']['state_of_origin'],
            ),
            "personal_info" => array(
                "marital_status" => $this->passport_application[0]['marital_status_id'],
                "eye_color" => $this->passport_application[0]['color_eyes_id'],
                "hair_color" => $this->passport_application[0]['color_hair_id'],
                "height" => $this->passport_application[0]['height'],
                "complexion" => "",
                "mark" => "",
            ),
            "other_info" => array(
                "special_feature" => "",
                "kin_name" => $this->passport_application[0]['next_kin'],
                "kin_address" => $kinAddress,
            ),
            "app_info" => array(
                "application_category" => "",
                "application_type" => $this->passport_application[0]['passportType'],
                "request_type" => $this->passport_application[0]['PassportApplicationDetails']['request_type_id'],
                "application_date" => $this->passport_application[0]['created_at'],
                "application_id" => $this->passport_application[0]['id'],
                "reference_no" => $this->passport_application[0]['ref_no'],
                "form_type" => "passport",
                "isGratis" => $isGratis
            ),
            "process_info" => array(
                "country" => $this->passport_application[0]['passportPCountry'],
                "state" => $this->passport_application[0]['passportPState'],
                "embassy" => $this->passport_application[0]['passportPEmbassy'],
                "office" => $this->passport_application[0]['passportPOffice'],
                "interview_date" => $datetime,
            ),
            "payment_info" => array(
                "naira_amount" => $nairaAmt,
                "dollor_amount" => $dollarAmt,
                "payment_status" => $payment_status,
                "payment_gateway" => $PaymentGatewayType,
                "paid_amount" => $paidAmount
            ),
        );



        $this->forward404Unless($this->passport_application);
        $this->setVar('data', $data);
        $this->setLayout('layout_print');
    }

    public function executePassportPaymentSlip(sfWebRequest $request) {

        //$this->passport_application = Doctrine::getTable('PassportApplication')->find(array($request->getParameter('id')));
        $getAppId = SecureQueryString::DECODE($request->getParameter('id'));
        $getAppId = SecureQueryString::ENCRYPT_DECRYPT($getAppId);


        $this->passport_application = $this->getPassportRecord($getAppId);
        $passport_fee = Doctrine::getTable('PassportFee')
                        ->createQuery('a')
                        ->where('a.passporttype_id = ?', $this->passport_application[0]['passporttype_id'])
                        ->execute()->toArray(true);
        $this->passport_fee = $passport_fee;
        if (($this->passport_application[0]['paid_local_currency_amount'] == 0) && ($this->passport_application[0]['paid_dollar_amount'] == 0)) {
            $datetime = "Check the Passport Office / Embassy / High Commission";
        } else {
            $datetime = $this->passport_application[0]['interview_date'];
        }
        //Get Payment status
        $isGratis = false;
        if ($this->passport_application[0]['ispaid'] == 1) {
            if (($this->passport_application[0]['paid_local_currency_amount'] == 0) && ($this->passport_application[0]['paid_dollar_amount'] == 0)) {
                $payment_status = "This Application is Gratis (Requires No Payment)";
                $paidAmount = 'Not Applicable';
                $isGratis = true;
            } else {
                $PaymentGatewayType = Doctrine::getTable('PaymentGatewayType')->getGatewayName($this->passport_application[0]['payment_gateway_id']);

                if ($PaymentGatewayType == 'eTranzact' || $PaymentGatewayType == 'Interswitch' || $PaymentGatewayType == 'Pay4me (Interswitch)' || $PaymentGatewayType == 'Pay4me (Bank)' || $PaymentGatewayType == 'Pay4me (eWallet)') {
                    $paidAmount = $this->passport_application[0]['paid_local_currency_amount'];
                    if ($paidAmount != 0.00) {
                        $paidAmountInNiara = $paidAmount;
                        $paidAmountInDollar = 'Not Applicable';
                        $paidAmount = "NGN&nbsp;" . $paidAmount;
                    } else {
                        $paidAmountInNiara = "Not Applicable";
                        $paidAmountInDollar = 'Not Applicable';
                        $paidAmount = 'Not Applicable';
                    }
                } else {

                    $this->data = Doctrine::getTable('PaymentRequest')->getValidationNumberStatus($getAppId, 'PASSPORT');
                    if (isset($this->data['pay4me']) && isset($this->data['currency']) && $this->data['currency'] == 'naira') {
                        $paidAmount = $this->passport_application[0]['paid_local_currency_amount'];
                        if ($paidAmount != 0.00) {
                            $nairaAmt = $paidAmount;
                            $dollarAmt = 'Not Applicable';
                            $paidAmount = "NGN&nbsp;" . $paidAmount;
                        } else {
                            $nairaAmt = "Not Applicable";
                            $dollarAmt = 'Not Applicable';
                            $paidAmount = 'Not Applicable';
                        }
                    } else {
                        $paidAmount = $this->passport_application[0]['paid_dollar_amount'];
                        if ($paidAmount != 0.00) {
                            $paidAmountInDollar = $paidAmount;
                            $paidAmountInNiara = "Not Applicable";
                            $paidAmount = "USD&nbsp;" . $paidAmount;
                        } else {
                            $paidAmountInDollar = "Not Applicable";
                            $paidAmountInNiara = "Not Applicable";
                            $paidAmount = "Not Applicable";
                        }
                    }
                }

                //$paidAmount = ($this->passport_application[0]['paid_dollar_amount']!="")?"USD ".$this->passport_application[0]['paid_dollar_amount']:($this->passport_application[0]['paid_naira_amount']!="")?"NGN ".$this->passport_application[0]['paid_naira_amount']:0;
                $payment_status = "Payment Done";
            }
        }

        //Get payment gateway type
        // $PaymentGatewayType = Doctrine::getTable('PaymentGatewayType')->getGatewayName($this->passport_application[0]['payment_gateway_id']);
        //Pass data to view
        $data = array(
            "profile" => array(
                "title" => $this->passport_application[0]['title_id'],
                "first_name" => $this->passport_application[0]['first_name'],
                "middle_name" => $this->passport_application[0]['mid_name'],
                "last_name" => $this->passport_application[0]['last_name'],
                "date_of_birth" => $this->passport_application[0]['date_of_birth'],
                "sex" => $this->passport_application[0]['gender_id'],
                "country_origin" => $this->passport_application[0]['cName'],
                "state_origin" => $this->passport_application[0]['sName'],
                "occupation" => $this->passport_application[0]['occupation']),
            "app_info" => array(
                "application_category" => "",
                "application_type" => $this->passport_application[0]['passportType'],
                "request_type" => $this->passport_application[0]['PassportApplicationDetails']['request_type_id'],
                "application_date" => $this->passport_application[0]['created_at'],
                "application_id" => $this->passport_application[0]['id'],
                "reference_no" => $this->passport_application[0]['ref_no'],
                "isGratis" => $isGratis),
            "process_info" => array(
                "country" => $this->passport_application[0]['passportPCountry'],
                "state" => $this->passport_application[0]['passportPState'],
                "embassy" => $this->passport_application[0]['passportPEmbassy'],
                "office" => $this->passport_application[0]['passportPOffice'],
                "interview_date" => $datetime,
            ),
            "payment_info" => array(
                "naira_amount" => $paidAmountInNiara,
                "dollor_amount" => $paidAmountInDollar,
                "payment_status" => $payment_status,
                "payment_gateway" => $PaymentGatewayType,
                "paid_amount" => $paidAmount
            ),
        );


        $this->forward404Unless($this->passport_application);
        $this->setVar('data', $data);
        $this->setLayout('layout_print');
    }

    public function executePayment(sfWebRequest $request) {
        //Call from google check ou
        //die('test');
        if (!$request->hasParameter('trans_id')) {
            $request->setParameter('trans_id', ORDER_ID);
            $request->setParameter('status_id', APP_STATUS);
            $request->setParameter('app_id', APP_ID);
            $request->setParameter('gtype', GATEWAY_TYPE);
        }
        $transid = $request->getParameter('trans_id');
        $statusid = $request->getParameter('status_id');

        if ($request->hasParameter('gtype')) {
            $gatewayType = $request->getParameter('gtype');
        } else {
            $gatewayType = $this->getUser()->getAttribute('gtype');
            $this->getUser()->getAttributeHolder()->remove('gtype');
        }
        if ($request->hasParameter('app_id')) {
            $appid = $request->getParameter('app_id');
        } else {
            $appid = $this->getUser()->getAttribute('app_id');
            $this->getUser()->getAttributeHolder()->remove('app_id');
        }
        $payObj = new paymentHelper();
        //check whether application is paid---
        $pArr = Doctrine::getTable('PassportApplication')->getPassportAppIdRefId($appid);
        $response['naira_amount'] = 0;
        $response['dollar_amount'] = 0;
        if ($pArr[0]['ispaid'] != 1) {
            if ($statusid == 1) {
                $response = '';
                switch ($gatewayType) {
                    case 'Interswitch':
                        $response = $payObj->fetch_local_Itransaction($transid, $appid, 'NIS PASSPORT');
                        if ($response['response'] != '00') {
                            $this->getUser()->setFlash('error', 'No Payment Record Found', false);
                            $this->forward($this->moduleName, 'passportPaymentStatus');
                        }
                        break;
                    case 'eTranzact':
                        $response = $payObj->fetch_local_Etransaction($transid, $appid, 'NIS PASSPORT');
                        if ($response['response'] != '0') {
                            $this->getUser()->setFlash('error', 'No Payment Record Found', false);
                            $this->forward($this->moduleName, 'passportPaymentStatus');
                        }
                        break;
                    case 'google':
                        $response = $payObj->fetch_Gtransaction($transid, $appid, 'NIS PASSPORT');
                        if ($response['response'] != 1) {
                            $this->getUser()->setFlash('error', 'No Payment Record Found', false);
                            $this->forward($this->moduleName, 'passportPaymentStatus');
                        }
                        break;
                    case 'amazon':
                        $response = $payObj->fetch_Atransaction($transid, $appid, 'NIS PASSPORT');

                        if ($response['response'] != 1) {
                            $this->getUser()->setFlash('error', 'No Payment Record Found', false);
                            $this->forward($this->moduleName, 'passportPaymentStatus');
                        }
                        break;
                    default:
                        $this->getUser()->setFlash('error', 'No Payment Record Found', false);
                        $this->forward($this->moduleName, 'passportPaymentStatus');
                }
            }

            $dollarAmount = $response['dollar_amount'];
            //    $this->getUser()->getAttribute('dollar_amount');
            $nairaAmount = $response['naira_amount'];
            //    $this->getUser()->getAttribute('naira_amount');

            $this->getUser()->getAttributeHolder()->remove('dollar_amount');
            $this->getUser()->getAttributeHolder()->remove('naira_amount');

            if (!$dollarAmount) {
                $dollarAmount = 0;
            }
            if (!$nairaAmount) {
                $nairaAmount = 0;
            }
            $status_id = ($statusid == 1) ? true : false;

            // $gatewayTypeId = Doctrine::getTable('PaymentGatewayType')->getId($gatewayType);
            $gatewayTypeId = Doctrine::getTable('PaymentGatewayType')->getGatewayId($gatewayType);
            $transArr = array(
                PassportWorkflow::$PASSPORT_TRANS_SUCCESS_VAR => $status_id,
                PassportWorkflow::$PASSPORT_TRANSACTION_ID_VAR => $transid,
                PassportWorkflow::$PASSPORT_APPLICATION_ID_VAR => $appid,
                PassportWorkflow::$PASSPORT_DOLLAR_AMOUNT_VAR => $dollarAmount,
                PassportWorkflow::$PASSPORT_NAIRA_AMOUNT_VAR => $nairaAmount,
                PassportWorkflow::$PASSPORT_GATEWAY_TYPE_VAR => $gatewayTypeId);


            $this->dispatcher->notify(new sfEvent($transArr, 'passport.application.payment'));

            if ($gatewayType == 'google') {
                $this->setLayout(null);
                $Gresponse = new GoogleResponse(MERCHANT_ID, MERCHANT_KEY);
                $Gresponse->SendAck();
                die;
            } else if ($gatewayType == 'amazon') {
                exit;
            } else {
                if ($statusid) {
                    $this->getUser()->setFlash('notice', 'Payment Successfully Done. Your Transaction ID is ' . $transid, false);
                    $request->setParameter('statusReport', 0);
                } else if ($statusid == false) {
                    if ($gatewayType == 'Interswitch') {
                        $errorResponce = Doctrine::getTable('InterswitchResp')->getResponce($transid);
                        $this->getUser()->setFlash('error', ' Payment Transaction Failed, Reason: ' . $errorResponce . '. Your Transaction ID is ' . $transid, false);
                    } else {
                        $this->getUser()->setFlash('error', ' Payment Transaction Failed. Your Transaction ID is ' . $transid, false);
                    }
                    $request->setParameter('statusReport', 1);
                }
            }
            //incrypt $appid and decrypt it, forpassing it to show action
        }

        if ($statusid) {
            $this->getUser()->setFlash('notice', 'Payment Successfully Done. Your Transaction ID is ' . $transid, false);
            $request->setParameter('statusReport', 0);
        }
        $encriptedAppId = SecureQueryString::ENCRYPT_DECRYPT($appid);
        $encriptedAppId = SecureQueryString::ENCODE($encriptedAppId);
        $request->setParameter('id', $encriptedAppId);
        $this->forward($this->moduleName, 'show');
    }

    public function executeEdit(sfWebRequest $request) {

        // incrept application id for security//
        $encriptedAppId = SecureQueryString::ENCRYPT_DECRYPT($request->getParameter('id'));
        $encriptedAppId = SecureQueryString::ENCODE($encriptedAppId);
        $this->setVar('encriptedAppId', $encriptedAppId);

        $this->setVar('formName', 'update');

        $this->forward404Unless($passport_application = Doctrine::getTable('PassportApplication')->find(array($request->getParameter('id'))), sprintf('Object passport_application does not exist (%s).', array($request->getParameter('id'))));
        $passportTypeName = Doctrine::getTable('PassportAppType')->getPassportTypeName($passport_application['passporttype_id']);

        switch ($passportTypeName) {
            //      case 'MRP Standard':
            //        $this->setVar('passport_type','MRP Standard Passport Application Form');
            //        $request->setParameter('passport_type','MRP Standard Passport Application Form');
            //        $this->form = new StandardPassportForm($passport_application);
            //        break;
            //      case 'MRP Official':
            //        $this->setVar('passport_type','MRP Official Passport Application Form');
            //        $request->setParameter('passport_type','MRP Official Passport Application Form');
            //        $this->form = new OfficialPassportForm($passport_application);
            //        break;
            //      case 'MRP Diplomatic':
            //        $this->setVar('passport_type','MRP Diplomatic Passport Application Form');
            //        $request->setParameter('passport_type','MRP Diplomatic Passport Application Form');
            //        $this->form = new DiplomaticPassportForm($passport_application);
            //        break;
            //      case 'MRP Seamans':
            //        $this->setVar('passport_type','MRP Seamans Passport Application Form');
            //        $request->setParameter('passport_type','MRP Seamans Passport Application Form');
            //        $this->form = new SeamansPassportForm($passport_application);
            //        break;
            case 'Standard ePassport':
                $this->setVar('passport_type', 'Standard ePassport Application Form');
                $request->setParameter('passport_type', 'Standard ePassport Application Form');
                $this->form = new EstandardPassportForm($passport_application);
                $mid_name = trim($request->getPostParameter('passport_application[mid_name]'));
                if(!empty ($mid_name)){
                  $this->form->setDefault('mid_name', $mid_name);
                }
                $this->countryId = $request->getParameter("countryId");
                break;
            //      case 'Official ePassport':
            //        $this->setVar('passport_type','Official ePassport Application Form');
            //        $request->setParameter('passport_type','Official ePassport Application Form');
            //        $this->form = new EofficialPassportForm($passport_application);
            //        break;
            //      default:
            //        $this->form = new PassportApplicationForm($passport_application);
        }
    }

    public function executeUpdate(sfWebRequest $request) {
        $getAppId = SecureQueryString::DECODE($request->getParameter('id'));
        $getAppId = SecureQueryString::ENCRYPT_DECRYPT($getAppId);

        $this->setVar('formName', 'update');
        $this->setVar('encriptedAppId', $request->getParameter('id'));
        $this->forward404Unless($request->isMethod('post') || $request->isMethod('put'));
        $this->forward404Unless($passport_application = Doctrine::getTable('PassportApplication')->find(array($getAppId)), sprintf('Object passport_application does not exist (%s).', array($getAppId)));

        $passportTypeName = Doctrine::getTable('PassportAppType')->getPassportTypeName($passport_application['passporttype_id']);
        sfContext::getInstance()->getLogger()->info("---- $passportTypeName ---");

        switch ($passportTypeName) {
            //        case 'MRP Standard':
            //          $this->setVar('passport_type','MRP Standard Passport Application Form');
            //          $this->form = new StandardPassportForm($passport_application);
            //          break;
            //        case 'MRP Official':
            //          $this->setVar('passport_type','MRP Official Passport Application Form');
            //          $this->form = new OfficialPassportForm($passport_application);
            //          break;
            //        case 'MRP Diplomatic':
            //          $this->setVar('passport_type','MRP Diplomatic Passport Application Form');
            //          $this->form = new DiplomaticPassportForm($passport_application);
            //          break;
            //        case 'MRP Seamans':
            //          $this->setVar('passport_type','MRP Seamans Passport Application Form');
            //          $this->form = new SeamansPassportForm($passport_application);
            //          break;
            case 'Standard ePassport':
                $this->setVar('passport_type', 'Standard ePassport Application Form');
                $this->form = new EstandardPassportForm($passport_application);
                $this->countryId = $request->getParameter("countryId");
                break;
            //        case 'Official ePassport':
            //          $this->setVar('passport_type','Official ePassport Application Form');
            //          $this->form = new EofficialPassportForm($passport_application);
            //          break;
        }

        $this->processForm($request, $this->form);
        $this->setTemplate('edit');
    }

    public function executeDelete(sfWebRequest $request) {
        $request->checkCSRFProtection();

        $this->forward404Unless($passport_application = Doctrine::getTable('PassportApplication')->find(array($request->getParameter('id'))), sprintf('Object passport_application does not exist (%s).', array($request->getParameter('id'))));
        $passport_application->delete();

        $this->redirect('passport/index');
    }

    protected function processForm(sfWebRequest $request, sfForm $form) {
        // $form->b
        $requestParam = $request->getParameter($form->getName());
        //      echo "<pre>";print_r($requestParam);die;
        //if user has not payment rights then  he can not summit NIS application
        if ($this->getUser()->getGuardUser()->getUserDetail()->getPaymentRights()) {
            $this->redirect("paymentProcess/blockUserMsg");
        }
        if ($form->getObject()->getIsEmailValid()) {
            if (isset($requestParam['id']) && $requestParam['id'] != '') {
                $requestParam['email'] = $form->getObject()->getEmail();
            }
        }
        ## binding form fields and file tag as well
        //$form->bind($requestParam, $request->getFiles($form->getName()));
        $form->bind($requestParam);


        if ($form->isValid()) {
            if (!isset($requestParam['passporttype_id']) || !is_int((int) $requestParam['passporttype_id']) || $requestParam['passporttype_id'] == null) {
                $this->getUser()->setFlash('error', "Invalid Passport Type");
                return;
            }
            $arrForVerification = array();
            $arrForVerification['fname'] = $requestParam['first_name'];
            $arrForVerification['mname'] = $requestParam['mid_name'];
            $arrForVerification['lname'] = $requestParam['last_name'];
            $arrForVerification['dob'] = $requestParam['date_of_birth'];
            if (TblBlockApplicantTable::getInstance()->isApplicantBlocked($arrForVerification)) {
                $this->getUser()->setFlash('error', "You are blocked for applying a Passport on this portal.");
                return;
            }
            $isNew = 'i';
            $type = 'p'; //for passport
            if ($form->getObject()->isNew()) {
                $isNew = 'n';
                /*
                  ### START UPLOADING FILE HERE ###
                  $formValues = $request->getFiles($form->getName());
                  $formValues = $formValues['scan_address_name'];

                  $userID = sfContext::getInstance()->getUser()->getGuarduser()->getId();
                  $address_scan = sfConfig::get('sf_upload_dir').'/address_scan/';

                  ## Creating Path if not exist...
                  if(!file_exists($address_scan)){
                  mkdir($address_scan);
                  chmod($address_scan, 0777);
                  }//End of if(!file_exists($address_scan)){...

                  ## finding file extension...
                  $ext = substr(strrchr($formValues['name'], '.'), 1);

                  $path = $address_scan.$userID.'/';
                  ## Creating Path if not exist...
                  if(!file_exists($path)){
                  mkdir($path);
                  chmod($path, 0777);
                  }//End of if(!file_exists($path)){...

                  ### END UPLOADING FILE HERE ###

                 */
                $passport_application = $form->save();
                $id = "";
                $id = (int) $passport_application['id'];


                /*    ## Creating file name and adding into the folder...
                  $path = $path.'pass_'.$id.'.'.$ext;

                  ## finding image width and height...
                  ## $image_info[0] is width and $image_info[1] is height...
                  $image_info = getimagesize($formValues['tmp_name']);
                  $sizeInKb = (int)($formValues['size']/1024);

                  ## If size is more than 100 KB then convert it into small size...
                  if($sizeInKb > 100){
                  if($image_info[0] >= 700 && $image_info[1] >= 700 ){
                  $image_info[0] = 700;
                  $image_info[1] = 700;
                  }
                  }//End of if($sizeInKb > 100){...

                  $img = new sfImage($formValues['tmp_name'], 'image/'.$ext);
                  $img->thumbnail($image_info[0],$image_info[1]);
                  $img->setQuality(50);
                  $img->saveAs($path);

                  //if(!copy($formValues['tmp_name'], $path)){
                  //echo "error";
                  //}

                 */
                
                //send mail to the customer, for application submission
                $mailUrl = "notification/appSubmissionMail";
                $mailTaskId = EpjobsContext::getInstance()->addJob('NisApplicationMail', $mailUrl, array('app_id' => $id, 'app_type' => $type, 'user_id' => $this->getUser()->getGuardUser()->getId()));


                $applicationArr = array(PassportWorkflow::$PASSPORT_APPLICATION_ID_VAR => $id);
                //invoke workflow listner start workflow and insert new passport workflow instance in workpool table
                sfContext::getInstance()->getLogger()->info('Posting passport.new.application with id ' . $id);
                $this->dispatcher->notify(new sfEvent($applicationArr, 'passport.new.application'));
            } else {
                $form_data = $request->getParameter($form->getName());
                $app_id = $form_data['id'];
                $current_application_status = Doctrine::getTable('PassportApplication')->find($form_data['id']);
                $is_paid = $current_application_status->getStatus();
                if ($is_paid == 'New') {
                    //find if any payment request create
                    $paymentHelper = new paymentHelper();
                    $paymentRequestStatus = $paymentHelper->getPaymentRequestStatus($form_data['id'], 'NIS PASSPORT');
                    $paymentRequestStatus = true; //commented by ashwani kumar...
                    if (!$paymentRequestStatus) {
                        $this->getUser()->setFlash('error', 'Your application under payment awaited state!! you can not edit this application.', true);
                        $this->redirect('passport/editPassportApplication');
                    } else {
                        $passport_application = $form->save();
                        $id = "";
                        $id = (int) $passport_application['id'];
                       
                    }
                } else {
                    $this->getUser()->setFlash('error', "Your application is already in process. This application cann't be change.", true);                    
                    $id = "";
                    $id = $form_data['id'];
                }
            }
            //$request->setParameter('id', $id);
            //$this->forward($this->moduleName, 'show');
            // incrept application id for security//
            $encriptedAppId = SecureQueryString::ENCRYPT_DECRYPT($id);
            $encriptedAppId = SecureQueryString::ENCODE($encriptedAppId);
            $this->redirect('passport/showPrintSuccess?chk=1&p=' . $isNew . '&id=' . $encriptedAppId);
        }
    }

    public function executeGetState(sfWebRequest $request) {
        $nigeriaContryId = Doctrine::getTable('Country')->getNigeriaId();
        if ($request->getParameter('country_id') == $nigeriaContryId) {
            if (trim($request->getParameter('passport_official_type')) == 'OFFICIALType') {
                $q = Doctrine::getTable('State')->createQuery('st')
                                ->addWhere('country_id = ?', $request->getParameter('country_id'))
                                ->andWhere('is_official_state = 1')
                                ->orderBy('state_name ASC')
                                ->execute()->toArray();
            } else if (trim($request->getParameter('passport_type_id')) == 'MRPType' && trim($request->getParameter('passport_official_type')) == '' && trim($request->getParameter('passport_seamans_type')) == '') {
                $q = Doctrine::getTable('State')->createQuery('st')
                                ->addWhere('country_id = ?', $request->getParameter('country_id'))
                                ->andWhere('is_mrp_state = 1')
                                ->orderBy('state_name ASC')
                                ->execute()->toArray();
            } else if (trim($request->getParameter('passport_type_id')) == 'MRPType' && trim($request->getParameter('passport_official_type')) == '' && trim($request->getParameter('passport_seamans_type')) == 'MRPSeamansType') {
                $q = Doctrine::getTable('State')->createQuery('st')
                                ->addWhere('country_id = ?', $request->getParameter('country_id'))
                                //          ->andWhere('is_mrp_state = 1')
                                ->andWhere('is_mrp_seamans_state = 1')
                                ->orderBy('state_name ASC')
                                ->execute()->toArray();
            } else {
                $q = Doctrine::getTable('State')->createQuery('st')->
                                addWhere('country_id = ?', $request->getParameter('country_id'))
                                ->orderBy('state_name ASC')
                                ->execute()
                                ->toArray();
            }

            $str = '<option value="">-- Please Select --</option>';
            if (count($q) > 0) {
                foreach ($q as $key => $value) {
                    $str .= '<option value="' . $value['id'] . '" >' . $value['state_name'] . '</option>';
                }
            }
            return $this->renderText($str);
        } else if ($request->getParameter('add_option') != "") {
            $str = '<option value="">' . $request->getParameter('add_option') . '</option>';
        } else {
            $str = '<option value="">Not Applicable</option>';
        }
        return $this->renderText($str);
    }

    public function executeGetLga(sfWebRequest $request) {

        $q = Doctrine::getTable('LGA')->createQuery('st')->
                        addWhere('branch_state = ?', $request->getParameter('state_id'))->execute()->toArray();
        $str = '<option value="">-- Please Select --</option>';
        if (count($q) > 0) {
            foreach ($q as $key => $value) {
                $str .= '<option value="' . $value['id'] . '" >' . $value['lga'] . '</option>';
            }
        }
        return $this->renderText($str);
    }

    // if in near future , client provide postalcodes then it will be uncomment
    /*
      public function executeGetDistrict(sfWebRequest $request)
      {

      $q = Doctrine::getTable('PostalCodes')->createQuery('st')->
      addWhere('lga_id = ?', $request->getParameter('lga_id'))
      ->orderBy('district')
      ->execute()->toArray();
      $str = '<option value="">-- Please Select --</option>';
      if(count($q) > 0 ){
      foreach($q as $key => $value){
      $str .= '<option value="'.$value['id'].'" >'.$value['district'].'</option>';
      }
      }
      return $this->renderText($str);
      }

      public function executeGetPostalCode(sfWebRequest $request)
      {

      $q = Doctrine::getTable('PostalCodes')->createQuery('st')->select('st.postcode')
      ->addWhere('id = ?', $request->getParameter('district_id'))
      ->orderBy('district')
      ->execute()->toArray();
      if(count($q) > 0 ){
      $str = $q[0]['postcode'];
      }
      return $this->renderText($str);
      }
     */
    public function executeGetEmbassy(sfWebRequest $request) {
        sfContext::getInstance()->getLogger()->info("+++" . $request->getParameter('country_id'));
        $q = Doctrine::getTable('EmbassyMaster')->createQuery('st')->
                        addWhere('embassy_country_id = ?', $request->getPostParameter('country_id'))->execute()->toArray();
        //get
        $arrExcludeEmbassy = sfConfig::get('app_excludeEmbassy');

        $embassy_id = $request->getParameter('embassy_id');

        $str = '<option value="" selected="selected">-- Please Select --</option>';
        if (count($q) > 0) {
            foreach ($q as $key => $value) {

                $sel = ($embassy_id == $value['id'])?'selected':'';

                if (!empty($arrExcludeEmbassy)) {
                    if (!in_array(trim($value['embassy_name']), $arrExcludeEmbassy)) {
                        $str .= '<option value="' . $value['id'] . '" '.$sel.' >' . $value['embassy_name'] . '</option>';
                    }
                } else {
                    $str .= '<option value="' . $value['id'] . '" '.$sel.' >' . $value['embassy_name'] . '</option>';
                }
            }
        }
        return $this->renderText($str);
    }

    public function executeGetOffice(sfWebRequest $request) {
        $passportType = $request->getParameter('passportType');
        sfContext::getInstance()->getLogger()->info("+++" . $request->getParameter('state_id'));
        if ($passportType == 'Standard ePassport Application Form' || $passportType == 'Official ePassport Application Form' || $passportType == 'MRP Official Passport Application Form') {
            $q = Doctrine::getTable('PassportOffice')->createQuery('st')->
                            addWhere('office_state_id = ?', $request->getParameter('state_id'))->
                            andWhere('is_epassport_active =?', '1')->execute()->toArray();
        } else {
            $q = Doctrine::getTable('PassportOffice')->createQuery('st')->
                            addWhere('office_state_id = ?', $request->getParameter('state_id'))->execute()->toArray();
        }

        $str = '<option value="">-- Please Select --</option>';
        $selected = '';
        $selected_office_id = $request->getParameter('selected_id');
        if (count($q) > 0) {
            foreach ($q as $key => $value) {
                if (!empty($selected_office_id)) {
                    $selected = ($selected_office_id == $value['id']) ? "selected='selected'" : '';
                }
                $str .= "<option value='{$value['id']}'{$selected}> {$value['office_name']} </option>";
            }
        }
        return $this->renderText($str);
    }

    //Passport Gurantar Form
    public function executePassportGuarantor(sfWebRequest $request) {

    }

    public function executeCheckPassportGuarantorStatus(sfWebRequest $request) {
        $PassportRef = $request->getPostParameters();
        $passportReturn = Doctrine::getTable('PassportApplication')->getPassportAppIdRefId($PassportRef['passport_app_id'], $PassportRef['passport_app_refId']);


        if ($passportReturn) {
            $request->setParameter('id', $passportReturn[0]['id']);
            $this->forward($this->moduleName, 'ShowGuarantorForm');
        } else {
            $this->setVar('errMsg', 'Application not found! Please check parameters and try again.');
        }
        $this->setTemplate('PassportGuarantor');
    }

    public function executeShowGuarantorForm(sfWebRequest $request) {
        $this->passport_application = Doctrine_Query::create()
                        ->select("pa.*, pd.*, pci.*, c.country_name as cName, s.state_name as sName")
                        ->from('PassportApplication pa')
                        ->leftJoin('pa.PassportApplicationDetails pd')
                        ->leftJoin('pa.PassportApplicantContactinfo pci')
                        ->leftJoin('pci.Country c', 'c.id=pci.nationality_id')
                        ->leftJoin('pd.State s', 's.id=pd.stateoforigin')
                        ->Where('pa.id=' . $request->getParameter('id'))
                        ->execute()->toArray(true);
        $this->setLayout('layout_print');
    }

    public function executeShowGuarantorFormNext(sfWebRequest $request) {
        $this->passport_application = Doctrine_Query::create()
                        ->select("pa.*, pd.*, pci.*, c.country_name as cName, s.state_name as sName")
                        ->from('PassportApplication pa')
                        ->leftJoin('pa.PassportApplicationDetails pd')
                        ->leftJoin('pa.PassportApplicantContactinfo pci')
                        ->leftJoin('pci.Country c', 'c.id=pci.nationality_id')
                        ->leftJoin('pd.State s', 's.id=pd.stateoforigin')
                        ->Where('pa.id=' . $request->getParameter('id'))
                        ->execute()->toArray(true);
        $this->setLayout('layout_print');
    }

    protected function notifyNoAmountPayment($appid) {
        $status_id = true;
        $transid = 1;
        $dollarAmount = 0;
        $nairaAmount = 0;
        $gtypeTble = Doctrine::getTable('PaymentGatewayType');
        $gatewayTypeId = $gtypeTble->getGatewayId(PaymentGatewayTypeTable::$TYPE_NONE);

        $transArr = array(
            PassportWorkflow::$PASSPORT_TRANS_SUCCESS_VAR => $status_id,
            PassportWorkflow::$PASSPORT_TRANSACTION_ID_VAR => $transid,
            PassportWorkflow::$PASSPORT_APPLICATION_ID_VAR => $appid,
            PassportWorkflow::$PASSPORT_DOLLAR_AMOUNT_VAR => $dollarAmount,
            PassportWorkflow::$PASSPORT_NAIRA_AMOUNT_VAR => $nairaAmount,
            PassportWorkflow::$PASSPORT_GATEWAY_TYPE_VAR => $gatewayTypeId);

        $this->dispatcher->notify(new sfEvent($transArr, 'passport.application.payment'));
    }

    protected function getPassportRecord($id) {

        $passportObj = Doctrine::getTable('PassportApplication')->find($id);
        if(!empty($passportObj)){
            $passport_application = Doctrine_Query::create()
            ->select("pa.*,pa.processing_country_id as processing_passport_id, pd.*, pci.*, c.country_name as cName, s.state_name as sName, pc.country_name as passportPCountry, ps.state_name as passportPState, em.embassy_name as passportPEmbassy,
                   po.office_name as passportPOffice, pt.id, pt.var_value as passportType,cs.state_name as contact_state,ad.*,ppd.*,pcd.*,
                   ks.state_name as kin_state,prs.state_name as perm_state,cac.country_name as contact_address-country,pal.lga as perm_lga,adl.lga as kin_lga")
            ->from('PassportApplication pa')
            ->leftJoin('pa.PassportApplicationDetails pd')
            ->leftJoin('pa.PassportApplicantContactinfo pci')
            ->leftJoin('pci.Country c', 'c.id=pci.nationality_id')
            ->leftJoin('pci.State cs', 'cs.id=pci.state_of_origin')
            ->leftJoin('pd.State s', 's.id=pd.stateoforigin')
            ->leftJoin('pa.Country pc', 'pc.id=pa.processing_country_id')
            ->leftJoin('pa.State ps', 'ps.id=pa.processing_state_id')
            ->leftJoin('pa.EmbassyMaster em', 'em.id=pa.processing_embassy_id')
            ->leftJoin('pa.PassportOffice po', 'po.id=pa.processing_passport_office_id')
            ->leftJoin('pa.PassportAppType pt')
            ->leftJoin('pa.PassportKinAddress ad')
            ->leftJoin('pd.PassportPermanentAddress ppd')
            ->leftJoin('pd.PassportContactAddress pcd')
            ->leftJoin('ad.State ks', 'ks.id=ad.state')
            ->leftJoin('ppd.State prs', 'prs.id=ppd.state')
            ->leftJoin('pcd.Country cac', 'cac.id=pcd.country_id')
            ->leftJoin('ppd.LGA pal', 'pal.id=ppd.lga_id')
            ->leftJoin('ad.LGA adl', 'adl.id=pcd.lga_id')
            ->Where('pa.id=' . $id)
            ->execute()->toArray(true);

            if (isset($passport_application) && is_array($passport_application) && count($passport_application) > 0) {
                if ($passport_application[0]['sName'] == '') {
                    $passport_application[0]['sName'] = $passport_application[0]['contact_state'];
                }
            }
            return $passport_application;
        }else{
            $this->getUser()->setFlash('notice','Tempering URL is not allowed!');
            $this->redirect('cart/list#msg');
        }
    }

    //For Interview Reschedule
    public function executeInterviewReschedule(sfWebRequest $request) {
        $this->setTemplate('interviewReschedule');
    }

    public function executeCheckPassportApplication(sfWebRequest $request) {
        $PassportRef = $request->getPostParameters();
        $passportReturn = Doctrine::getTable('PassportApplication')->getPassportAppIdRefId($PassportRef['passport_app_id'], $PassportRef['passport_app_refId']);
        if ($passportReturn) {
            if ($passportReturn[0]['ispaid'] == 1 && $passportReturn[0]['status'] == "Paid") {
                $request->setParameter('id', $PassportRef['passport_app_id']);
                $this->forward($this->moduleName, 'InterviewDetails');
            } else {
                $this->getUser()->setFlash('error', "You can not reschedule interview date.", false);
            }
        } else {
            $this->getUser()->setFlash('error', 'Application not found! Please check parameters and try again.', false);
        }
        $this->setTemplate('interviewReschedule');
    }

    public function executeInterviewDetails(sfWebRequest $request) {
        $this->passport_details = $this->getPassportRecord($request->getParameter('id'));
        $this->setTemplate('interviewDetails');
    }

    public function executeUpdateReschedule(sfWebRequest $request) {
        $appId = $request->getParameter('id');
        $ref_num = $request->getParameter('ref_num');
        $country_id = $request->getParameter('country_id');
        $passportReturn = false;
        if (isset($appId) && is_numeric($appId) && isset($ref_num) && is_numeric($ref_num) && $appId != '' && $ref_num != '')
            $passportReturn = Doctrine::getTable('PassportApplication')->getPassportAppIdRefId($appId, $ref_num);

        if ($passportReturn) {
            if ($passportReturn[0]['ispaid'] == 1 && $passportReturn[0]['status'] == "Paid") {

                $this->old_interview_date = $request->getParameter('old_interview_date');

                $officeType = "";
                $processingOfficeId = "";
                //get interview date
                if ($passportReturn[0]['processing_embassy_id'] != "") {
                    $processingOfficeId = $passportReturn[0]['processing_embassy_id'];
                    $officeType = 'embassy';
                } else {
                    $processingOfficeId = $passportReturn[0]['processing_passport_office_id'];
                    $officeType = 'passport';
                }

                $interviewDate = InterviewSchedulerHelper::getInstance()->getInterviewDateForPassport($country_id, $appId, $processingOfficeId, $officeType);

                //    $new_interview_date = $request->getParameter('interview_date');
                $q = Doctrine_Query::create()
                                ->update('PassportApplication')
                                ->set('interview_date', "'" . $interviewDate . "'")
                                ->where('id =?', $appId)
                                ->execute();
                $datetime = date_create($interviewDate);
                $msg = "Your interview have been rescheduled. \rYour new interview date is :" . date_format($datetime, 'd/F/Y');
                $this->getUser()->setFlash('notice', $msg);
                $encriptedAppId = SecureQueryString::ENCRYPT_DECRYPT($appId);
                $encriptedAppId = SecureQueryString::ENCODE($encriptedAppId);
                $this->redirect('passport/show?id=' . $encriptedAppId);
            } else {
                $this->getUser()->setFlash('error', "You can not reschedule interview date.", false);
                $this->setTemplate('interviewReschedule');
            }
        } else {
            $this->getUser()->setFlash('error', 'Application not found! Please check parameters and try again.', false);
            $this->setTemplate('interviewReschedule');
        }
    }

    public function executeSendEmail(sfWebRequest $request) {
        $this->setLayout(false);
        $applicant_email = $request->getParameter('applicant_email');
        $this->applicant_name = $request->getParameter('applicant_name');
        $this->appId = $request->getParameter('appId');
        $this->refNo = $request->getParameter('refNo');
        $this->status = $request->getParameter('status');
        $this->image_header = $request->getParameter('image_header');
        $mailBody = $this->getPartial($request->getParameter('partialName'));
        $sendMailObj = new EmailHelper();
        $mailInfo = $sendMailObj->sendEmail($mailBody, $applicant_email, sfConfig::get('app_mailserver_subject'));
        return $this->renderText('Mail sent successfully');
    }

    public function executeApplicationPayment(sfWebRequest $request) {
        if ($request->getParameter('appDetails') == "") {
            $this->redirect('pages/errorUser');
        }

        /**
         * [WP: 091] => CR: 131
         * Removing POS session variable...
         */
        $this->getUser()->getAttributeHolder()->remove('posPayment');

        //start:kuldeep code for check if visa is right
        $appDetails = $request->getParameter('appDetails');
        $isVisaFreezone = false;
//        foreach ($appDetails as $details) {
//            $appDetail = SecureQueryString::DECODE($details);
//            $appDetail = SecureQueryString::ENCRYPT_DECRYPT($appDetail);
//            $appDetail = explode('~', $appDetail);
//            if ($appDetail[1] == 'visa') {
//                //get visa category id
//                $visaCatType = Doctrine::getTable('VisaApplication')->getVisaCategory($appDetail[0]);
//                if ($visaCatType == 'NIS FREEZONE') {
//                    $isVisaFreezone = true;
//                }
//            }
//        }
        if ($isVisaFreezone) {
            $this->getUser()->setFlash("notice", 'Your visa application has been changed. Please remove it from the cart and add it again', true);
            $this->redirect("cart/list");
        }
        //end:kuldeep
        $paformeLibObj = new iPayformeLib();

        //   $returnValueByCurl =  $paformeLibObj->parsePostData($appDetails[0]);
        $returnValueByCurl = $paformeLibObj->postMultipleItems($appDetails);
        if ($returnValueByCurl == "already_paid") {
            $this->getUser()->setFlash("error", "Application is Already Paid.");
            $this->redirect("cart/list");
        }
        //    $this->getUser()->getAttributeHolder()->remove("cartPayment");
        $this->redirect($returnValueByCurl);
    }
    
    public function executeShowPrintSuccess(sfWebRequest $request){
      $this->mode = trim($request->getParameter('mode'));
      if($this->mode){
        $this->setLayout('popupLayout');
      }
      $this->chk = $request->getParameter('chk');
      $this->p = $request->getParameter('p');

      $getAppId = SecureQueryString::DECODE($request->getParameter('id'));
      $getAppId = SecureQueryString::ENCRYPT_DECRYPT($getAppId);

      $passport_application = $this->getPassportRecord($getAppId);      

      if($passport_application[0]['status'] == 'New' && $this->mode == ''){
          $cartObj = new cartManager();
          $cartValidationObj = new CartValidation();
          $status = $cartObj->addToCart('p', $getAppId);
          //$user = sfContext::getInstance()->getUser();
          //start:Kuldeep for check cart capacity
          $arrCartCapacity = array();
          $arrCartCapacityLimit = $cartValidationObj->checkUserCartCapacity();
          if (!empty($arrCartCapacityLimit) && count($arrCartCapacityLimit)) {
              $arrCartCapacity = $arrCartCapacityLimit;
          }
          //end:Kuldeep
          if ($status == "capacity_full") {
              if ($arrCartCapacity['cart_capacity'] > 1) {
                  $msg = $arrCartCapacity['cart_capacity'] . " applications";
              } else {
                  $msg = $arrCartCapacity['cart_capacity'] . " application";
              }
              $this->getUser()->setFlash("notice", "Cart Capacity has been full. You can add maximum upto " . $msg . " in cart", true);
          } else if ($status == "amount_capacity_full") {
              $confVal = $arrCartCapacity['cart_amount_capacity'];
              $msg = ($confVal > $cartObj->getCartCurrValue()) ? $confVal : $cartObj->getCartCurrValue();
              $this->getUser()->setFlash("notice", sprintf("Cart Capacity has been full. Total value of cart cannot be above $" . $msg . ""));
          } else if ($status == "common_opt_not_available") {
              $this->getUser()->setFlash("notice", sfConfig::get("app_cart_err_message_save"), true);
          } else if ($status == "application_edit") {
              $this->getUser()->setFlash("notice", 'Your visa application has been changed. Please remove it from the cart and add it again', true);
          } else if($status == "only_vap_allowed"){
            $this->getUser()->setFlash('notice', sprintf('Passport can not be added into the cart with Visa on Arrival application.'));
          }
      }
      
      $this->encriptedAppId = $request->getParameter('id');
      $this->passport_application = $passport_application;
      /**
       * [WP: 096]
       */
      $this->printUrl = Functions::printApplicationURL('passport', $this->encriptedAppId);
    }



     public function executeNewApplication(sfWebRequest $request) {
        $appId = $request->getParameter('appId');
        $countryId = $request->getParameter('countryId');
        $appStatusFlag = $request->getParameter('appStatusFlag'); 

        
        
        ## If application id and processing country id exist then send it to edit passport page...
        if($appId != '' && $countryId != ''){
            $request->setParameter('id', $appId);
            $request->setParameter('countryId', $countryId);
            $request->setParameter('appStatusFlag', $appStatusFlag);
            
            $this->forward('passport', 'edit');
        }else{
            $this->type = $request->getParameter('type');
            $request->setParameter('countryId', base64_decode($request->getParameter('type')));
            $this->form = new EstandardPassportForm();
            $passportTypeIdArray = Doctrine::getTable('PassportAppType')->getPassportTypeIdArray('Standard ePassport');
            $this->form->setDefault('passporttype_id', $passportTypeIdArray[0]['id']);
            $this->form->setDefault('processing_country_id', $request->getParameter('countryId'));

            //prefield email and phone number fields
            $loggedUserDetailsObj = $this->getUser()->getGuardUser()->getUserDetail();
            $email = $loggedUserDetailsObj->getEmail();
            $this->form->setDefault('email', $email);

            if ($request->hasParameter("type")){
                $this->countryId = base64_decode($request->getParameter('type'));

                /**
                 * [WP: 104] => CR: 149
                 * Setting flag true for jna service to change address defined in bottom of the page...
                 */
                $sess_jna_processing = Functions::setFooterAddressFlag($this->countryId);
                $this->getUser()->setAttribute('sess_jna_processing', $sess_jna_processing);
            }
            else{
                $this->forward404Unless($request->hasParameter("type"));
            }

            $this->setVar('formName', 'estandard');
            $this->setVar('passport_type', 'Standard ePassport Application Form');
        }
        
    }

    

    public function executeNewApplicationAjax(sfWebRequest $request) {
        $first_name = trim($request->getPostParameter('passport_application[first_name]'));
        $last_name = trim($request->getPostParameter('passport_application[last_name]'));
        $gender_id = trim($request->getPostParameter('passport_application[gender_id]'));

        $day = trim($request->getPostParameter('passport_application[date_of_birth][day]'));
        $month = trim($request->getPostParameter('passport_application[date_of_birth][month]'));
        $year = trim($request->getPostParameter('passport_application[date_of_birth][year]'));

        $date_of_birth = date("Y-m-d H:i:s", mktime(0,0,0,date($month), date($day), date($year)));
        $place_of_birth = trim($request->getPostParameter('passport_application[place_of_birth]'));
        $email = trim($request->getPostParameter('passport_application[email]'));
        $contact_phone = trim($request->getPostParameter('passport_application[Contact Info][contact_phone]'));

        if($first_name != '' && $last_name != '' && $gender_id != '' && $date_of_birth != ''  && $place_of_birth != '' && $email != '' &&  $contact_phone != ''){
            $data = Doctrine::getTable('PassportApplication')->checkDuplicateApplication($first_name, $last_name, $gender_id, $date_of_birth, $place_of_birth, $email, $contact_phone);
            
            if(is_array($data)){
                if(count($data) > 0){

                    $this->isProcessMO = "no";
                    $this->act = 'found';
                    $this->status = strtolower($data[0]['status']);
                    $this->appId = $data[0]['appId'];
                    $this->ref_no = $data[0]['ref_no'];

                    sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
                    $this->printUrl = url_for("passport/showPrintSuccess?id=".SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT($data[0]['appId'])));

                    ## Below code checks weather application is associated with money order or not...                    
                    if($data[0]['status'] == 'New'){
                        $cartValidation = new CartValidation();
                        $processMO = $cartValidation->isAppProcessed($data[0]['appId'], 'p');
                        if($processMO){
                            $this->isProcessMO = "yes";
                        }
                    }

                    $paymentHelper = new paymentHelper();
                    $paymentRequestStatus = $paymentHelper->getPaymentRequestStatus($data[0]['appId'], 'NIS PASSPORT');
                    if(!$paymentRequestStatus){
                        $this->isAppProcess = "yes";
                    }else{
                        $this->isAppProcess = "no";
                    }
                    
                    //$text = json_encode(array('processMO' => $isProcessMO, 'act' => 'found', 'appId' => $data[0]['appId'], 'ref_no' => $data[0]['ref_no'], 'status' => $data[0]['status'], 'printUrl' => $printUrl));
                }else{                    
                    return $this->renderText('notfound');
                }
            }else{                
                return $this->renderText('error');
            }
        }else{            
            return $this->renderText('error');
        }
    }//End of public function executeNewApplicationAjax(sfWebRequest $request) {...

    /**
     * [WP: 091] => CR: 131
     * @param <type> $request 
     */
    public function executeApplicationPOSPayment(sfWebRequest $request) {
        
        $appDetails = $request->getParameter('appDetails');
        /**
         * [WP: 091] => CR: 131
         * Getting varibale for POS payment system...
         * If exists then setting session varibale...
         */
        $posPayment = $request->getParameter('posPayment');
        
        if($posPayment == 1){
            $this->getUser()->setAttribute('posPayment', true);
        }else{
            $this->getUser()->setAttribute('posPayment', false);
        }        
        $paformeLibObj = new iPayformeLib();
        $returnValueByCurl = $paformeLibObj->postMultipleItems($appDetails);
         if ($returnValueByCurl == "already_paid") {            
            $this->getUser()->setFlash('notice','This Application is already Paid.');
            $this->redirect("configurationManager/checkVapPaymentStatus");
        }
        $this->redirect($returnValueByCurl);
    }
    
}
