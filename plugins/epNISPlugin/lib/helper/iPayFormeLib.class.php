<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of paformeLib
 *
 * @author sdutt
 */
class iPayformeLib {

  private $paymentDataArray;
  public  $XMLData;
  private $schemaXsi = 'http://www.w3.org/2001/XMLSchema-instance';
  private $schemaLocationipay4me = 'http://www.ipay4me.com/schema/ipay4meorder/v1 ipay4meorderV1.xsd';
  private $defaultXmlNsipay4me = 'http://www.ipay4me.com/schema/ipay4meorder/v1';
  private $browserInstance = null;

  public function parsePostData($postData)
  {
      $appDetails = SecureQueryString::DECODE($postData);
      $appDetails = SecureQueryString::ENCRYPT_DECRYPT($appDetails);

      $appDetails = explode('~',$appDetails);
      $app_id = $appDetails[0];
      $app_type =  $this->getAppType($appDetails[1]);

      $itemObj = Doctrine::getTable("IPaymentRequest")->isExist($app_type,$app_id);
      if(!$itemObj){
         $itemReqObj = new IPaymentRequest();
        switch ($app_type){
          case "NIS PASSPORT":
            $itemReqObj->setPassportId($app_id);
            break;
          case "NIS VISA":
            $itemReqObj->setVisaId($app_id);
            break;
          case "NIS FREEZONE":
            $itemReqObj->setFreezoneId($app_id);
            break;
        }
        $itemReqObj->save();
        $itemId = $itemReqObj->id;
        $cartObj = new CartMaster();
        $cartObj->save();
        $cartId = $cartObj->id;
        $cartInfo = new CartItemsInfo();
        $cartInfo->setCartId($cartId);
        $cartInfo->setItemId($itemId);
        $cartInfo->save();
      }else{
        $itemId = $itemObj->getId();
        $cartId = Doctrine::getTable("CartItemsInfo")->getCartInfoByItemNumber($itemId);
        $cartDetails= Doctrine::getTable("CartItemsInfo")->getCartDetials($cartId);                
        if(isset ($cartDetails) && is_array($cartDetails) && count($cartDetails)==1){
          $cartObj = new CartMaster();
          $cartObj->save();
          $cartId = $cartObj->id;
          $cartInfo = new CartItemsInfo();
          $cartInfo->setCartId($cartId);
          $cartInfo->setItemId($itemId);
          $cartInfo->save();
        }
      }


        $cartDetails = Doctrine::getTable("CartItemsInfo")->getCartDetials($cartId);
        $transaction_number = $this->getRandomTransactionNumber($cartId);
        $amount = null;
        $i = 0;
//        if(count($cartDetails)==1){
//          $action->redirect('pages/errorUser');
//          exit;
//        }
        foreach ($cartDetails as $key => $value){
          if($key !="cart_id"){
            $amount = $value['amount'];
            $cartItemTransactions = new CartItemsTransactions();
            $cartItemTransactions->setCartId($cartId);
            $cartItemTransactions->setItemId($value['item_id']);
            $cartItemTransactions->setTransactionNumber($transaction_number);
            $name = $value['name'];
            $cartItemTransactions->setAttribute(Doctrine::ATTR_VALIDATE, Doctrine::VALIDATE_NONE);
            $cartItemTransactions->save();
          }
        }
        $finalPayArr['amountDollar'] = $amount;
        $finalPayArr['name'] = $name;
        $finalPayArr['cart_id'] = $cartId;
        $finalPayArr['trans_no'] = $transaction_number;
        $finalPayArr['description'] = $app_type;
         $finalPayArr['merchant_service'] = array(
                                             'merchant_id'=>sfConfig::get('app_ipay4me_payment_service_detail_nis_merchant_id'),
                                             'merchant_code'=>sfConfig::get('app_ipay4me_payment_service_detail_nis_merchant_code'),
                                             'merchant_key'=>sfConfig::get('app_ipay4me_payment_service_detail_nis_merchant_key')
                                            );
        $this->paymentDataArray = $finalPayArr;
        echo "<pre>"; print_r($finalPayArr);die;
        $XMLData = $this->creatXML();
        return $this->postXML($XMLData);
  }

  protected function decryptAppType($data) {
      $appDetails = SecureQueryString::DECODE($data);
      $appDetails = SecureQueryString::ENCRYPT_DECRYPT($appDetails);

      $appDetails = explode('~',$appDetails);
      $app_type =  $this->getAppType($appDetails[1]);
      return $app_type;
  }

  public function postMultipleItems($applications) {
    $cartId = null;
    foreach ($applications as $anApp) {
      $cartId = $this->addToCart($anApp, $cartId);
    }

    $app_type = $this->decryptAppType($applications[0]);

    $cartDetails = Doctrine::getTable("CartItemsInfo")->getCartDetials($cartId);
    $transaction_number = $this->getRandomTransactionNumber($cartId);
    $amount = null;
    $numOfApplications = 0;
    $amount = 0;
    if(count($cartDetails) == 1){
      return "already_paid";
    }
    foreach ($cartDetails as $key => $value) {
      if ($key != "cart_id") {
        $numOfApplications++;
        $amount = $amount + $value['amount'];
        $cartItemTransactions = new CartItemsTransactions();
        $cartItemTransactions->setCartId($cartId);
        $cartItemTransactions->setItemId($value['item_id']);
        $cartItemTransactions->setTransactionNumber($transaction_number);
        $name = $value['name'];
        $cartItemTransactions->setAttribute(Doctrine::ATTR_VALIDATE,
                Doctrine::VALIDATE_NONE);
        $cartItemTransactions->save();
      }
    }
    
    $finalPayArr['amountDollar'] = $amount;
    if ($numOfApplications > 1) {
      $finalPayArr['name'] = "$numOfApplications";
    } else {
      $finalPayArr['name'] = $name;
    }
    $finalPayArr['cart_id'] = $cartId;
    $finalPayArr['trans_no'] = $transaction_number;
    if($numOfApplications == 1) {
          $finalPayArr['description'] = "$numOfApplications NIS Application"; ///$app_type;

    }
    else {
    $finalPayArr['description'] = "$numOfApplications NIS Applications"; ///$app_type;
    }
    $finalPayArr['merchant_service'] = array(
        'merchant_id' => sfConfig::get('app_ipay4me_payment_service_detail_nis_merchant_id'),
        'merchant_code' => sfConfig::get('app_ipay4me_payment_service_detail_nis_merchant_code'),
        'merchant_key' => sfConfig::get('app_ipay4me_payment_service_detail_nis_merchant_key')
    );
    $this->paymentDataArray = $finalPayArr;
//     echo "<pre>"; print_r($finalPayArr);die;
    $XMLData = $this->creatXML();
    return $this->postXML($XMLData);
  }

  public function addToCart($application, $cartId=0) {
    $appDetails = SecureQueryString::DECODE($application);
    $appDetails = SecureQueryString::ENCRYPT_DECRYPT($appDetails);

    $appDetails = explode('~', $appDetails);
    $app_id = $appDetails[0];
    $app_type = $this->getAppType($appDetails[1]);

    $itemReqObj = new IPaymentRequest();
    switch ($app_type) {
      case "NIS PASSPORT":
        $itemReqObj->setPassportId($app_id);
        break;
      case "NIS VISA":
        $itemReqObj->setVisaId($app_id);
        break;
      case "NIS FREEZONE":
        $itemReqObj->setFreezoneId($app_id);
        break;
      case "NIS VAP":
        $itemReqObj->setVisaArrivalProgramId($app_id);
        break;

    }
    $itemReqObj->save();
    $itemId = $itemReqObj->id;
    if ($cartId == 0) {
      $cartObj = new CartMaster();
      $cartObj->save();
      $cartId = $cartObj->id;
    }
    $cartInfo = new CartItemsInfo();
    $cartInfo->setCartId($cartId);
    $cartInfo->setItemId($itemId);
    $cartInfo->save();

    return $cartId;
  }

  public function getAppType($app_type){
        return FeeHelper::getApplicationType($app_type);
  }


  protected function getRandomTransactionNumber($appId) {
    // TODO - replace it with appropriate implementation delegator here

      $uniqueNumber = time().''.$appId;
      $transaction_id = sfConfig::get('app_nis_ipayforme_transaction_number_start_series').$uniqueNumber; //.rand(0,99); production issue...
      return $transaction_id;
  }
 

  private function creatXML()
  {
//    $xml_data = new gc_XmlBuilder();
//    $headerArr = array('xmlns:xsi'=>$this->schemaXsi,'xmlns' => $this->defaultXmlNsipay4me,'xsi:schemaLocation'=>$this->schemaLocationipay4me);
//    $xml_data->Push('order',$headerArr);
//    $xml_data->Push('merchant', array('id'=>$this->paymentDataArray['merchant_service']['merchant_id']));
//    $xml_data->Element('item-number', $this->paymentDataArray['cart_id']);
//    $xml_data->Element('retry-id', $this->paymentDataArray['trans_no']);
//    if($this->paymentDataArray['count'] == 1){
//      $xml_data->Element('name', $this->paymentDataArray['name']);
//    }else{
//      $xml_data->Element('name', $this->paymentDataArray['count']);
//    }
//    $xml_data->Element('description', $this->paymentDataArray['description']);
//    $xml_data->Element('currency', $this->paymentDataArray['amountDollar'], array('type' => 'usd'));
//    $xml_data->Push('revenue-share', array('fixed-share'=>(($this->paymentDataArray['amountDollar'])*(sfConfig::get("app_ipay4me_payment_service_detail_revenue_share_application"))),'pays-fee'=>"false"));
//    $xml_data->Push('other-shares');
//    $xml_data->Element('other-share',(($this->paymentDataArray['amountDollar'])*(sfConfig::get("app_ipay4me_payment_service_detail_revenue_share_swglobal"))),array('merchant-code'=>sfConfig::get('app_ipay4me_payment_service_detail_swglobal_merchant_code'),'pays-fee'=>"true"));
//    $xml_data->Pop('other-shares');
//    $xml_data->Pop('revenue-share');
//    $xml_data->Pop('merchant');
//    $xml_data->Pop('order');


        $doc = new DomDocument('1.0');
        $doc->formatOutput = true;
        $xmlType='order';
        $root = $doc->createElement($xmlType);
        $root->setAttribute ( 'xmlns:xsi',$this->schemaXsi );
        $root->setAttribute ( 'xmlns' , $this->defaultXmlNsipay4me );
        $root->setAttribute ('xsi:schemaLocation',$this->schemaLocationipay4me);
        $root = $doc->appendChild($root);
        $merchant = $doc->createElement("merchant");
        $merchant->setAttribute ( "id", $this->paymentDataArray['merchant_service']['merchant_id'] );
        

        $item_number = $doc->createElement("item-number");
        $item_number->appendChild($doc->createTextNode($this->paymentDataArray['cart_id']));
        
        $retry_id = $doc->createElement("retry-id");
        $retry_id->appendChild($doc->createTextNode($this->paymentDataArray['trans_no']));
        
        $name = $doc->createElement( "name");
        $name->appendChild($doc->createTextNode($this->paymentDataArray['name']));
        
        $description = $doc->createElement( "description");
        $description->appendChild($doc->createTextNode($this->paymentDataArray['description']));

        $currency = $doc->createElement( "currency");
        $currency->setAttribute('type' , 'usd');
        $currency->appendChild($doc->createTextNode($this->paymentDataArray['amountDollar']));

        $revenue_share = $doc->createElement( "revenue-share");
        $revenue_share->setAttribute("fixed-share",(($this->paymentDataArray['amountDollar'])*(sfConfig::get("app_ipay4me_payment_service_detail_revenue_share_application"))));
        $revenue_share->setAttribute('pays-fee',"false");
//        $revenue_share = $doc->appendChild($revenue_share);
        $other_shares = $doc->createElement( "other-shares");
        $other_share = $doc->createElement( "other-share");
        $other_share->setAttribute('merchant-code',sfConfig::get('app_ipay4me_payment_service_detail_swglobal_merchant_code'));
        $other_share->setAttribute('pays-fee',"true");
        $other_share->appendChild($doc->createTextNode((($this->paymentDataArray['amountDollar'])*(sfConfig::get("app_ipay4me_payment_service_detail_revenue_share_swglobal")))));
        $other_shares->appendChild($other_share);
        $revenue_share->appendChild($other_shares);
        //$other_shares = $doc->appendChild($revenue_share);
        $merchant->appendChild($item_number);
        $merchant->appendChild($retry_id);
        $merchant->appendChild($name);
        $merchant->appendChild($description);
        $merchant->appendChild($currency);
        $merchant->appendChild($revenue_share);        
        $root->appendChild($merchant);
        
        $xmldata = $doc->saveXML();

    return $xmldata;
  }

  private function postXML($xmlData)
  {
    $logger = sfContext::getInstance()->getLogger();
//    echo "<pre>";print_r(get_class_methods(sfContext::getInstance()->useHelper()));
    $browser = $this->getBrowser();
      $merchantCode = $this->paymentDataArray['merchant_service']['merchant_code'];
      $merchantKey = $this->paymentDataArray['merchant_service']['merchant_key'];
      $merchantAuthString = $merchantCode .":" .$merchantKey;
     // $logger->info("auth string: $merchantAuthString");
      $merchantAuth = base64_encode($merchantAuthString) ;

      $header['Authorization'] = "Basic $merchantAuth";
      $header['Content-Type'] =  "application/xml;charset=UTF-8";
      $header['Accept'] = "application/xml;charset=UTF-8";
      $uniqueCombination = 'ItemOrder';

      $this->setLogResponseData($xmlData,'ItemOrderXML',$uniqueCombination);
       sfContext::getInstance()->getConfiguration()->loadHelpers('Url');
        $payProcessVersion = sfConfig::get('app_ipay4me_payment_service_detail_ipay4me_process_version');
        $uri = sfConfig::get('app_ipay4me_payment_service_detail_payment_URI');
        $uri = url_for($uri);
        if(!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off'  || $_SERVER['SERVER_PORT'] == 443)
            $uri = "https://".$_SERVER['HTTP_HOST'].$uri;
        else
            $uri = "http://".$_SERVER['HTTP_HOST'].$uri;
        $uri = $uri."/payprocess/$payProcessVersion/PID/$merchantCode";


    $logger->debug("Sending To: $uri");
    $logger->debug("Sending : $xmlData");
    $logger->debug("Headers : " + print_r($header, true));
    $browser->post($uri, $xmlData, $header);
    //log data in web/log/payment
    $logger->debug("Response Text: ".$browser->getResponseText());

    $uniqueCombination = 'OrderRedirect';
    $this->setLogResponseData($browser->getResponseText(),$uniqueCombination);

    if($browser->getResponseCode()==200)
    {
      $xdoc = new DomDocument;
      $isLoaded = $xdoc->LoadXML($browser->getResponseText());
      $redirectURL = $xdoc->getElementsByTagName('redirect-url')->item(0)->nodeValue;
//      die("first   ".$redirectURL);
      if(!isset ($redirectURL) || $redirectURL==''){
        $error = $xdoc->getElementsByTagName('error-message')->item(0)->nodeValue;
        if( 'Merchant Request – Payment has already been made for this Item' == $error){
           $redirectURL = 'pages/error404?message=waitingResponse';
        }
        else
        {
          $redirectURL = 'pages/error404';
        }
      }
     // $logger->info("XML data from payforme as notification:".$browser->getResponseText());
    }
    elseif($browser->getResponseCode()==400)
    {
      $responseXML = $browser->getResponseText();
      if('Merchant Request - Payment has already been made for this Item'==$responseXML)
      {
        $redirectURL = 'pages/error404?message=waitingResponse';
      }
      else
      {
        $redirectURL = 'pages/error404';
      }
    }
    else
    {
      //error log
     // $logger->err('Error: receiving server response code- '.$browser->getResponseCode()."\n".$browser->getResponseText());
      $redirectURL = 'pages/error404';
    }
    return $redirectURL;


  }

  public function getBrowser() {
  if(!$this->browserInstance) {
    $this->browserInstance = new sfWebBrowser(array(), 'sfCurlAdapter',
      array('SSL_VERIFYPEER' => false, 'SSL_VERIFYHOST' => false));
  }
  return $this->browserInstance;
 }

  public function setLogResponseData($xmlData,$uniqueNumber)
  {
    $path = $this->getLogPath();

    $file_name = $path."/".($uniqueNumber.'-'.date('Y_m_d_H:i:s')).".txt";
    $i=1;
    while(file_exists($file_name)) {
      $file_name = $path."/".($uniqueNumber.'-'.date('Y_m_d_H:i:s-')).$i.".txt";
      $i++;
    }
    @file_put_contents($file_name, $xmlData);
  }

  public function getLogPath()
  {

    $logPath = sfConfig::get('sf_log_dir').'/'.sfConfig::get('app_ipay4me_payment_service_detail_log_path');
    $logPath = $logPath.'/'.date('Y-m-d');

    if(is_dir($logPath)=='')
    {
      $dir_path=$logPath."/";
      mkdir($dir_path,0777,true);
    }
    return $logPath;
  }
          }
?>
