<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of paymentHelper
 *
 * @author nikhil
 */
class paymentHelper {
  var $interswitchTypeId = null;
  var $etranzactTypeId = null;
  var $payformeTypeId = null;

  public function fetch_Itransaction($transRef,$all=0){
      $confPathArr = sfConfig::get('app_naira_payment_url_callback');
      switch(sfConfig::get('app_naira_payments_const_payment_gateway_type')){
        case 'demo':
          $getewayUrl = $confPathArr['interswitch_query_demo'];
          break;
        case 'live':
          $getewayUrl = $confPathArr['interswitch_query_live'];
          break;
      }
      $iconf_arr = sfConfig::get('app_naira_payments_const_interswitch');
      $client = new SoapClient($getewayUrl,array('trace' => 1));
      $SoapCallParameters=array('product_id' => $iconf_arr['split_product_id'], 'trans_ref' =>$transRef);
      $client->__soapCall('getTransactionData', array('parameters' => $SoapCallParameters));

      //    echo "Response:\n" . $client->__getLastResponse() . "\n";
      $pieces = array($client->__getLastResponse());
      if($all)
        return $pieces;
      $abc=$pieces[0];
      $resp = explode(":", $abc);
      $finalresp= explode(">",$resp[9]);
      $response=$finalresp[2];
      return $response;
  }

  public function fetch_local_Itransaction($transRef,$app_id,$appType,$isForward=1){
      $recArr = Doctrine::getTable('InterswitchResp')->getTransactionRecord($transRef,$app_id,$appType);
      if(count($recArr)>0){
          switch($appType){
              case 'NIS PASSPORT':
                  $pptInfoArr = $this->getPassportFeeFromDB($app_id);
                  $retArr['naira_amount']=$pptInfoArr['naira_amount'];
                  $retArr['dollar_amount']=$pptInfoArr['dollar_amount'];
                  if(round(intval($recArr['InterswitchResp_0']['appr_amt'])/100)>=intval($pptInfoArr['naira_amount']))
                  {
                      $retArr['response']='00';
                      return $retArr;
                  }
                  else{
                    if($isForward){
                      sfContext::getInstance()->getUser()->setFlash('error','Amount Paid is not Sufficient!',false);
                      sfContext::getInstance()->getController()->forward('passport', 'passportPaymentStatus');
                      exit;
                    }
                    else{
                      sfContext::getInstance()->getUser()->setFlash('error','Amount Paid is not Sufficient!');
                      $retArr['response']='insufficient';
                      return $retArr;
                    }
                  }
                  break;
              case 'NIS VISA':
                  $payment_details = $this->getVisaFeeFromDB($app_id);
                  $retArr['naira_amount']=$payment_details['naira_amount'];
                  $retArr['dollar_amount']=$payment_details['dollar_amount'];
                  if(round(intval($recArr['InterswitchResp_0']['appr_amt'])/100)>=intval($payment_details['naira_amount']))
                  {
                      $retArr['response']='00';
                      return $retArr;
                  }
                  else{
                    if($isForward){
                      sfContext::getInstance()->getUser()->setFlash('error','Amount Paid is not Sufficient!',false);
                      sfContext::getInstance()->getController()->forward('visa', 'visaStatus');
                      exit;
                    }
                    else{
                      sfContext::getInstance()->getUser()->setFlash('error','Amount Paid is not Sufficient!');
                      $retArr['response']='insufficient';
                      return $retArr;
                    }
                  }
                  break;
              case 'NIS FREEZONE':
                  $payment_details = $this->getFreezoneFeeFromDB($app_id);
                  $retArr['naira_amount']=$payment_details['naira_amount'];
                  $retArr['dollar_amount']=$payment_details['dollar_amount'];
                  if(round(intval($recArr['InterswitchResp_0']['appr_amt'])/100)>=intval($payment_details['naira_amount']))
                  {
                      $retArr['response']='00';
                      return $retArr;
                  }
                  else{
                    if($isForward){
                      sfContext::getInstance()->getUser()->setFlash('error','Amount Paid is not Sufficient!',false);
                      sfContext::getInstance()->getController()->forward('visa', 'visaStatus');
                      exit;
                    }
                    else{
                      sfContext::getInstance()->getUser()->setFlash('error','Amount Paid is not Sufficient!');
                      $retArr['response']='insufficient';
                      return $retArr;
                    }
                  }
                  break;
          }
      }
      $retArr['response']='NA';
      return $retArr;
  }
  public function fetch_local_Etransaction($transRef,$app_id,$appType,$isForward=1){
      $recArr = Doctrine::getTable('EtranzactResp')->getTransactionRecord($transRef,$app_id,$appType);
      if(count($recArr)>0){
          switch($appType){
              case 'NIS PASSPORT':
                  $pptInfoArr = $this->getPassportFeeFromDB($app_id);
                  $retArr['naira_amount']=$pptInfoArr['naira_amount'];
                  $retArr['dollar_amount']=$pptInfoArr['dollar_amount'];
                  if(intval($recArr['EtranzactResp_0']['amount'])>=intval($pptInfoArr['naira_amount']))
                  {
                      $retArr['response']='0';
                      return $retArr;
                  }
                  else{
                    if($isForward){
                      sfContext::getInstance()->getUser()->setFlash('error','Amount Paid is not Sufficient!',false);
                      sfContext::getInstance()->getController()->forward('passport', 'passportPaymentStatus');
                      exit;
                    }
                    else{
                      sfContext::getInstance()->getUser()->setFlash('error','Amount Paid is not Sufficient!');
                      $retArr['response']='insufficient';
                      return $retArr;
                    }
                  }
                  break;
              case 'NIS VISA':
                  $payment_details = $this->getVisaFeeFromDB($app_id);
                  $retArr['naira_amount']=$payment_details['naira_amount'];
                  $retArr['dollar_amount']=$payment_details['dollar_amount'];
                  if(intval($recArr['EtranzactResp_0']['amount'])>=intval($payment_details['naira_amount']))
                  {
                      $retArr['response']='0';
                      return $retArr;
                  }
                  else{
                    if($isForward){
                      sfContext::getInstance()->getUser()->setFlash('error','Amount Paid is not Sufficient!',false);
                      sfContext::getInstance()->getController()->forward('visa', 'visaStatus');
                      exit;
                    }
                    else{
                      sfContext::getInstance()->getUser()->setFlash('error','Amount Paid is not Sufficient!');
                      $retArr['response']='insufficient';
                      return $retArr;
                    }
                  }
                  break;
              case 'NIS FREEZONE':
                  $payment_details = $this->getFreezoneFeeFromDB($app_id);
                  $retArr['naira_amount']=$payment_details['naira_amount'];
                  $retArr['dollar_amount']=$payment_details['dollar_amount'];
                  if(intval($recArr['EtranzactResp_0']['amount'])>=intval($payment_details['naira_amount']))
                  {
                      $retArr['response']='0';
                      return $retArr;
                  }
                  else{
                    if($isForward){
                      sfContext::getInstance()->getUser()->setFlash('error','Amount Paid is not Sufficient!',false);
                      sfContext::getInstance()->getController()->forward('visa', 'visaStatus');
                      exit;
                    }
                    else{
                      sfContext::getInstance()->getUser()->setFlash('error','Amount Paid is not Sufficient!');
                      $retArr['response']='insufficient';
                      return $retArr;
                    }
                  }
                  break;
          }
      }
      $retArr['response']='NA';
      return $retArr;
  }

  public function fetch_local_ItransactionByAppIdAppType($app_id,$appType){
      $recArr = Doctrine::getTable('InterswitchResp')->getTransactionRecordByAppIdAppStatus($app_id,$appType);
      if(count($recArr)>0){
          return $recArr;
      }
      else{
          return 'NA';
      }
  }

  public function fetch_local_EtransactionByAppIdAppType($app_id,$appType){
      $recArr = Doctrine::getTable('EtranzactResp')->getTransactionRecordByAppIdAppStatus($app_id,$appType);
      if(count($recArr)>0){
          return $recArr;
      }
      else{
          return 'NA';
      }
  }

  public function fetch_Etransaction($transRef){
      $confPathArr = sfConfig::get('app_naira_payment_url_callback');
      switch(sfConfig::get('app_naira_payments_const_payment_gateway_type')){
        case 'demo':
          $getewayUrl = $confPathArr['eTranzact_query_demo'];
          $terminal_id=$confPathArr['eTranzact_terminal_id_demo'];
          break;
        case 'live':
          $getewayUrl = $confPathArr['eTranzact_query_live'];
          $terminal_id=$confPathArr['eTranzact_terminal_id_live'];
          break;
      }
      $curlPost = 'TRANSACTION_ID='.$transRef.'&TERMINAL_ID='.$terminal_id;
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $getewayUrl);
      curl_setopt($ch, CURLOPT_HEADER, 0);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $curlPost);
      $data = curl_exec($ch);
      curl_close($ch);

      $rr = explode('<body topmargin="0" leftmargin="0" >',$data);
      $value = explode('TRANS_DATE=',trim($rr[1]));
      $date2 = explode('SUCCESS=',$value[1]);
      $date= $date2[0];
      $success = substr(trim($date2[1]), 0, -18);
      return($success);
  }

  public function fetch_Gtransaction($transRef,$app_id,$appType){

       $gretArr = Doctrine_Manager::getInstance()->getCurrentConnection()->fetchAssoc(
                    "
                        SELECT gc_order_state_change_notification.new_financial_order_state,gc_order_state_change_notification.new_fulfillment_order_state
                        FROM gc_new_order_notification
                        LEFT JOIN gc_order_state_change_notification ON gc_order_state_change_notification.google_order_number = gc_new_order_notification.google_order_number
                        LEFT JOIN gc_items on gc_items.order_id = gc_new_order_notification.id
                        WHERE gc_new_order_notification.google_order_number='".$transRef."' and gc_items.item_name='".$appType."'
                        and gc_new_order_notification.google_order_number not in
                        (
                          select google_order_number from gc_refund_amount_notification
                        )
                        ORDER BY gc_order_state_change_notification.id desc
                        LIMIT 0,1
                    ");
      $retArr['naira_amount']=0;
      $retArr['dollar_amount']=0;
      if(count($gretArr)>0)
      {
          if($gretArr[0]['new_financial_order_state']=='CHARGED'){

              switch($appType){
                  case 'NIS PASSPORT':
                      $pptInfoArr = $this->getPassportFeeFromDB($app_id);
                      $retArr['naira_amount']=$pptInfoArr['naira_amount'];
                      $retArr['dollar_amount']=$pptInfoArr['dollar_amount'];
                      $retArr['response']=1;
                      break;
                  case 'NIS VISA':
                      $payment_details = $this->getVisaFeeFromDB($app_id);
                      $retArr['naira_amount']=$payment_details['naira_amount'];
                      $retArr['dollar_amount']=$payment_details['dollar_amount'];
                      $retArr['response']=1;
                      break;
                 case 'NIS FREEZONE':
                      $payment_details = $this->getFreezoneFeeFromDB($app_id);
                      $retArr['naira_amount']=$payment_details['naira_amount'];
                      $retArr['dollar_amount']=$payment_details['dollar_amount'];
                      $retArr['response']=1;
                      break;
              }
              return $retArr;
          }
      }
      $retArr['response']=0;
      return $retArr;
  }

  public function fetch_Atransaction($transRef,$app_id,$appType){
      $response_array=Doctrine_Query::create()
      ->select('t.id,s.id,s.caller_reference as caller_reference')
      ->from('AmazonTokenPaymentDetails t')
      ->leftJoin('t.AmazonBuyerTokenMaster s')
      ->where('t.transaction_id=?',$transRef)
      ->execute(array(),Doctrine::HYDRATE_ARRAY);
//      print_r($response_array);
//      exit;
      $ipn_arr=Doctrine::getTable('AmazonIpn')->getTransactionDetails($transRef);

//      print_r($ipn_arr);
//      exit;

      $retArr['naira_amount']=0;
      $retArr['dollar_amount']=0;

     if(strtoupper($ipn_arr['transaction_status'])=='SUCCESS')
      {
            $paymentDetails = Doctrine::getTable('AmazonPaymentRequest')->findByCallerReference($response_array[0]['caller_reference']);
             switch($appType){
                  case 'NIS PASSPORT':
                      $pptInfoArr = $this->getPassportFeeFromDB($app_id);
                      if(intval($paymentDetails[0]->getAmount())>=intval($pptInfoArr['dollar_amount']))
                      {
                          $retArr['naira_amount']=$pptInfoArr['naira_amount'];
                          $retArr['dollar_amount']=$pptInfoArr['dollar_amount'];
                          $retArr['response']=1;
                      }
                      else{
                        $retArr['response']='insufficient';
                      }
                      break;
                  case 'NIS VISA':
                      $pptInfoArr = $this->getVisaFeeFromDB($app_id);
                      if(intval($paymentDetails[0]->getAmount())>=intval($pptInfoArr['dollar_amount']))
                      {
                          $retArr['naira_amount']=$pptInfoArr['naira_amount'];
                          $retArr['dollar_amount']=$pptInfoArr['dollar_amount'];
                          $retArr['response']=1;
                      }
                      else{
//                        sfContext::getInstance()->getUser()->setFlash('error','Amount Paid is not Sufficient!');
                        $retArr['response']='insufficient';
                      }
                      break;
                 case 'NIS FREEZONE':
                      $pptInfoArr = $this->getFreezoneFeeFromDB($app_id);
                      if(intval($paymentDetails[0]->getAmount())>=intval($pptInfoArr['dollar_amount']))
                      {
                          $retArr['naira_amount']=$pptInfoArr['naira_amount'];
                          $retArr['dollar_amount']=$pptInfoArr['dollar_amount'];
                          $retArr['response']=1;
                      }
                      else{
//                        sfContext::getInstance()->getUser()->setFlash('error','Amount Paid is not Sufficient!');
                        $retArr['response']='insufficient';
                      }
                      break;
              }
              return $retArr;
      }
      $retArr['response']=0;
      return $retArr;
  }
  public function getVisaFeeFromDB($app_id){
      $isFreshEntry = Doctrine::getTable('VisaApplication')->isFreshEntry($app_id);
      if($isFreshEntry){
          $visa_application =  Doctrine::getTable('VisaApplication')->getVisaFreshRecord($app_id);
          $cat_id = $visa_application[0]['visacategory_id'];
          $country_id = $visa_application[0]['present_nationality_id'];
          $visa_id = $visa_application[0]['VisaApplicantInfo']['visatype_id'];
          $entry_id = $visa_application[0]['VisaApplicantInfo']['entry_type_id'];
          $zone_id = $visa_application[0]['zone_type_id'];
          $no_of_entry = $visa_application[0]['VisaApplicantInfo']['no_of_re_entry_type'];
          $visa_count_id = $visa_application[0]['VisaApplicantInfo']['applying_country_id'];
      }
      else
      {
          $visa_application =  Doctrine::getTable('VisaApplication')->getVisaReEntryRecord($app_id);
          $cat_id = $visa_application[0]['visacategory_id'];
          $country_id = $visa_application[0]['present_nationality_id'];
          $visa_id = $visa_application[0]['ReEntryVisaApplication']['visa_type_id'];
          $entry_id = $visa_application[0]['ReEntryVisaApplication']['re_entry_type'];
          $no_of_entry = $visa_application[0]['ReEntryVisaApplication']['no_of_re_entry_type'];
          $zone_id = $visa_application[0]['zone_type_id'];
      }
      $payment_details = Doctrine::getTable('VisaFee')->getVisaFee($country_id,$cat_id,$visa_id,$entry_id,$no_of_entry,$zone_id, true);
      return $payment_details;
  }

  public function getFreezoneFeeFromDB($app_id){
      $isFreezoneFreshEntry = Doctrine::getTable('VisaApplication')->isFreezoneFreshEntry($app_id);
      if($isFreezoneFreshEntry){
          $visa_application =  Doctrine::getTable('VisaApplication')->getVisaFreshRecord($app_id);
          $cat_id = $visa_application[0]['visacategory_id'];
          $country_id = $visa_application[0]['present_nationality_id'];
          $visa_id = $visa_application[0]['VisaApplicantInfo']['visatype_id'];
          $entry_id = $visa_application[0]['VisaApplicantInfo']['entry_type_id'];
          $zone_id = $visa_application[0]['zone_type_id'];
          $no_of_entry = $visa_application[0]['VisaApplicantInfo']['no_of_re_entry_type'];
          $visa_count_id = $visa_application[0]['VisaApplicantInfo']['applying_country_id'];
      }
      else
      {
          $visa_application =  Doctrine::getTable('VisaApplication')->getVisaReEntryRecord($app_id);
          $cat_id = $visa_application[0]['visacategory_id'];
          $country_id = $visa_application[0]['present_nationality_id'];
          if(isset($visa_application[0]['ReEntryVisaApplication']['visa_type_id'])){
            $visa_id = $visa_application[0]['ReEntryVisaApplication']['visa_type_id'];
          }else{
            $visa_id=Doctrine::getTable('ReEntryVisaApplication')->getVisaTypeId($app_id);
            }
          $entry_id = $visa_application[0]['ReEntryVisaApplication']['re_entry_type'];
          $no_of_entry = $visa_application[0]['ReEntryVisaApplication']['no_of_re_entry_type'];
          $zone_id = $visa_application[0]['zone_type_id'];
      }
      $payment_details = Doctrine::getTable('VisaFee')->getVisaFee($country_id,$cat_id,$visa_id,$entry_id,$no_of_entry,$zone_id, true);
      return $payment_details;
  }

  public function getPassportFeeFromDB($app_id){
        $pptInfoArr = Doctrine::getTable('PassportApplication')->getFeeByPassportId($app_id);
        return $pptInfoArr;
  }

  public function getVisaArrivalFeeFromDB($app_id){

      $visa_application =  Doctrine::getTable('VapApplication')->find($app_id);
      if(count($visa_application) > 0){
          $country_id = $visa_application['present_nationality_id'];
          $visaType = $visa_application['type_of_visa'];
          $payment_details = Doctrine::getTable('VisaFee')->getVisaArrivalFee($country_id, $visaType);
      }else{
          $payment_details = '';
      }
      return $payment_details;
  }

  //Find if there is any record in Google Payment tables
  public function check_Gtransaction_on_DB($transRef){

     $gretArr = Doctrine_Manager::getInstance()->getCurrentConnection()->fetchAssoc(
                    " SELECT gc_new_order_notification.google_order_number
                      FROM gc_new_order_notification
                      WHERE gc_new_order_notification.google_order_number='".trim($transRef)."'");

     if (isset($gretArr[0]))
        return true;
      else
        return false;
  }
  /* methods used by support tool for google payment */

  public function fetch_Gtransaction_by_support_tool($transRef){

       $gretArr = Doctrine_Manager::getInstance()->getCurrentConnection()->fetchAssoc(
                    " SELECT gc_order_state_change_notification.new_financial_order_state,
                               gc_order_state_change_notification.new_fulfillment_order_state,
                               gc_items.merchant_item_id,gc_items.item_name
                        FROM gc_items LEFT JOIN gc_new_order_notification ON gc_items.order_id = gc_new_order_notification.id
                        LEFT JOIN gc_order_state_change_notification ON gc_new_order_notification.google_order_number = gc_order_state_change_notification.google_order_number
                        WHERE gc_order_state_change_notification.google_order_number='".trim($transRef)."'
                        ORDER BY gc_order_state_change_notification.id desc
                        LIMIT 0,1");

       if (isset($gretArr[0]))
        return array('financial'=>$gretArr[0]['new_financial_order_state'],'fulfillment'=>$gretArr[0]['new_fulfillment_order_state']);
       else
        return false;
  }


  public function fetch_Gtransaction_with_application($transRef)
  {
    $gretArr = Doctrine_Manager::getInstance()->getCurrentConnection()->fetchAssoc(
                    " SELECT gc_order_state_change_notification.new_financial_order_state,
                               gc_order_state_change_notification.new_fulfillment_order_state,
                               gc_items.merchant_item_id,gc_items.item_name,
                               gc_new_order_notification.financial_order_state,
                               gc_new_order_notification.fulfillment_order_state
                        FROM gc_items LEFT JOIN gc_new_order_notification ON gc_items.order_id = gc_new_order_notification.id
                        LEFT JOIN gc_order_state_change_notification ON gc_order_state_change_notification.google_order_number = gc_new_order_notification.google_order_number
                        WHERE gc_new_order_notification.google_order_number='".trim($transRef)."'
                        ORDER BY gc_order_state_change_notification.created_at desc
                        LIMIT 0,1");
    if(isset($gretArr[0]))
    {
      if($gretArr[0]['new_financial_order_state']=='' && $gretArr[0]['new_fulfillment_order_state']=='')
      {
        $gretArr[0]['new_financial_order_state'] = $gretArr[0]['financial_order_state'];
        $gretArr[0]['new_fulfillment_order_state'] = $gretArr[0]['fulfillment_order_state'];
      }
      return $this->findRecordOnNISPortal($gretArr,$transRef);
    }
    else
    {
      return 'NoPaymentREcord';
    }
  }

  public function fetch_Gtransaction_no_of_ack($orderRef)
  {
    $gretArr = Doctrine_Manager::getInstance()->getCurrentConnection()->fetchAssoc(
                    " SELECT  gc_order_state_change_notification.new_fulfillment_order_state,
                              gc_order_state_change_notification.new_financial_order_state
                       FROM gc_items LEFT JOIN gc_new_order_notification ON gc_items.order_id = gc_new_order_notification.id
                        LEFT JOIN gc_order_state_change_notification ON gc_order_state_change_notification.google_order_number = gc_new_order_notification.google_order_number
                        WHERE gc_order_state_change_notification.google_order_number='".trim($orderRef)."'
                        ORDER BY gc_order_state_change_notification.created_at asc
                 ");
    if(isset($gretArr[0]))
    {
      return $gretArr;
    }
    else
      return true;
  }

  public function addOrderStateChangeNotification_by_support_tool($google_order_number='',$serial_number='',$new_financial_order_state='',$new_fulfillment_order_state='',$previous_financial_order_state='',$previous_fulfillment_order_state='',$timestamp='')
  {
    $query=" INSERT INTO `gc_order_state_change_notification` (
        `id` ,
        `google_order_number` ,
        `serial_number` ,
        `new_financial_order_state` ,
        `new_fulfillment_order_state` ,
        `previous_financial_order_state` ,
        `previous_fulfillment_order_state` ,
        `timestamp` ,
        `created_at` ,
        `updated_at`
        )
        VALUES (
        NULL , '".$google_order_number."', '".$serial_number."', '".$new_financial_order_state."', '".$new_fulfillment_order_state."', '".$previous_financial_order_state."', '".$previous_fulfillment_order_state."', '".$timestamp."', NOW(), NOW()
        ) ";
    Doctrine_Manager::getInstance()->getCurrentConnection()->execute($query);
  }

  public function findRecordOnNISPortal($gretArr,$transRef)
  {
      if($gretArr[0]['item_name']=='NIS VISA')
      {
        $RecordReturn = Doctrine::getTable('VisaApplication')->getVisaInfo($gretArr[0]['merchant_item_id']);
        if(is_array($RecordReturn))
        {
          $returnData['g_orderNo'] = $transRef;
          $returnData['g_newFinancialOrderState'] = ($gretArr[0]['new_financial_order_state']!="")?$gretArr[0]['new_financial_order_state']:'Not Available';
          $returnData['g_newFulfillmentOrderState'] = ($gretArr[0]['new_fulfillment_order_state']!="")?$gretArr[0]['new_fulfillment_order_state']:'Not Available';

          $returnData['a_id'] = $RecordReturn[0]['id'];
          $returnData['a_refNo'] = $RecordReturn[0]['ref_no'];
          $returnData['a_title'] = $RecordReturn[0]['title'];
          $returnData['a_lastName'] = $RecordReturn[0]['surname'];
          $returnData['a_midName'] = $RecordReturn[0]['middle_name'];
          $returnData['a_firstName'] = $RecordReturn[0]['other_name'];
          $returnData['a_amount'] = $RecordReturn[0]['paid_dollar_amount'];
          $returnData['status'] = $RecordReturn[0]['status'];
          $returnData['trans_id'] = $RecordReturn[0]['payment_trans_id'];
          $returnData['payment_gatway'] = Doctrine::getTable('PaymentGatewayType')->getGatewayName($RecordReturn[0]['payment_gateway_id']);

          $ReEntryId = Doctrine::getTable('VisaCategory')->getReEntryId();
          if(count($ReEntryId) > 0)
          {
            if($RecordReturn[0]['visacategory_id'] == $ReEntryId)
            {
              $returnData['a_type'] =  "ReEntry Visa";
            }else{
              $returnData['a_type'] = "Fresh Visa";
            }
          }
          return $returnData;
        }
        else
        {
          return 'NoVisaApplication';
        }
      }
      else
      if($gretArr[0]['item_name']=='NIS PASSPORT')
      {
        $RecordReturn = Doctrine::getTable('PassportApplication')->getPassportInfo($gretArr[0]['merchant_item_id']);
        if(is_array($RecordReturn))
        {
          $returnData['g_orderNo'] = $transRef;
          $returnData['g_newFinancialOrderState'] = isset($gretArr[0]['new_financial_order_state'])? $gretArr[0]['new_financial_order_state']:'Not Available';
          $returnData['g_newFulfillmentOrderState'] = isset($gretArr[0]['new_fulfillment_order_state'])? $gretArr[0]['new_fulfillment_order_state']:'Not Available';


          $returnData['a_id'] = $RecordReturn[0]['id'];
          $returnData['a_refNo'] = $RecordReturn[0]['ref_no'];
          $returnData['a_title'] = $RecordReturn[0]['title_id'];
          $returnData['a_lastName'] = $RecordReturn[0]['last_name'];
          $returnData['a_midName'] = $RecordReturn[0]['mid_name'];
          $returnData['a_firstName'] = $RecordReturn[0]['first_name'];
          $returnData['a_amount'] = $RecordReturn[0]['paid_dollar_amount'];
          $returnData['status'] = $RecordReturn[0]['status'];
          $returnData['trans_id'] = $RecordReturn[0]['payment_trans_id'];
          $returnData['payment_gatway'] = Doctrine::getTable('PaymentGatewayType')->getGatewayName($RecordReturn[0]['payment_gateway_id']);

          $passType = Doctrine::getTable('PassportAppType')->getPassportTypeName($RecordReturn[0]['passporttype_id']);
          $returnData['a_type'] = $passType;
          return $returnData;
        }
        else
        {
          return 'NoPassportApplication';
        }
      }else
      if($gretArr[0]['item_name']=='NIS FREEZONE')
      {
        $RecordReturn = Doctrine::getTable('VisaApplication')->getVisaInfo($gretArr[0]['merchant_item_id']);
        if(is_array($RecordReturn))
        {
          $returnData['g_orderNo'] = $transRef;
          $returnData['g_newFinancialOrderState'] = ($gretArr[0]['new_financial_order_state']!="")?$gretArr[0]['new_financial_order_state']:'Not Available';
          $returnData['g_newFulfillmentOrderState'] = ($gretArr[0]['new_fulfillment_order_state']!="")?$gretArr[0]['new_fulfillment_order_state']:'Not Available';
//echo "<pre>";print_r($RecordReturn);die;
          $returnData['a_id'] = $RecordReturn[0]['id'];
          $returnData['a_refNo'] = $RecordReturn[0]['ref_no'];
          $returnData['a_title'] = $RecordReturn[0]['title'];
          $returnData['a_lastName'] = $RecordReturn[0]['surname'];
          $returnData['a_midName'] = $RecordReturn[0]['middle_name'];
          $returnData['a_firstName'] = $RecordReturn[0]['other_name'];
          $returnData['a_amount'] = $RecordReturn[0]['paid_dollar_amount'];
          $returnData['status'] = $RecordReturn[0]['status'];
          $returnData['trans_id'] = $RecordReturn[0]['payment_trans_id'];
          $returnData['payment_gatway'] = Doctrine::getTable('PaymentGatewayType')->getGatewayName($RecordReturn[0]['payment_gateway_id']);

          $ReEntryId = Doctrine::getTable('VisaCategory')->getReEntryFreezoneId();
          if(count($ReEntryId) > 0)
          {
            if($RecordReturn[0]['visacategory_id'] == $ReEntryId)
            {
              $returnData['a_type'] =  "ReEntry Freezone Visa";
            }else{
              $returnData['a_type'] = "Freezone Fresh Visa";
            }
          }
          return $returnData;
        }
        else
        {
          return 'NoVisaApplication';
        }
      }
      else
      {
        return 'applicationTypeMismatch';
      }
  }

  public function checkChargeAmountNotification($orderNo)
  {
    $gretArr = Doctrine_Manager::getInstance()->getCurrentConnection()->fetchAssoc(
                      "SELECT gc_chargeback_amount_notification.id
                       FROM gc_chargeback_amount_notification
                       WHERE gc_chargeback_amount_notification.google_order_number='".$orderNo."'");

    if(isset($gretArr[0]))
      return true;
    else
      return false;
  }

  public function getGoogleGatewayTypeId() {
    $this->googleTypeId = Doctrine::getTable('PaymentGatewayType')->getGatewayId('google'); //google
    return $this->googleTypeId;
  }

  public function getAmazonGatewayTypeId() {
    $this->amazonTypeId = Doctrine::getTable('PaymentGatewayType')->getGatewayId('amazon'); //amazon
    return $this->amazonTypeId;
  }

  public function insertChargeAmountNotification($orderNo,$chargeAmountNotificationData)
  {
    $query = "INSERT INTO `gc_charge_amount_notification` (
    `id` ,
    `google_order_number` ,
    `serial_number` ,
    `latest_charge_amount` ,
    `total_charge_amount` ,
    `timestamp` ,
    `created_at` ,
    `updated_at`
    )
    VALUES (
    NULL , '".$orderNo."', '".$chargeAmountNotificationData['serial-number']."','".$chargeAmountNotificationData['latest-charge-amount']['VALUE']."', '".$chargeAmountNotificationData['total-charge-amount']['VALUE']."', '".$chargeAmountNotificationData['timestamp']['VALUE']."',NOW(),NOW()
    )";
    Doctrine_Manager::getInstance()->getCurrentConnection()->execute($query);
  }

  public function checkRiskInfoNotification($orderNo)
  {
    $gretArr = Doctrine_Manager::getInstance()->getCurrentConnection()->fetchAssoc(
                      "SELECT gc_risk_information_notification.id
                       FROM gc_risk_information_notification
                       WHERE gc_risk_information_notification.google_order_number='".$orderNo."'");

    if(isset($gretArr[0]))
      return true;
    else
      return false;
  }

  public function insertRiskInfoNotification($orderNo,$chargeAmountNotificationData)
  {
     $query = " INSERT INTO `gc_risk_information_notification` (
        `id` ,
        `serial_number` ,
        `google_order_number` ,
        `eligible_for_protection` ,
        `avs_response` ,
        `cvn_response` ,
        `partial_cc_number` ,
        `ip_address` ,
        `buyer_account_age` ,
        `timestamp` ,
        `created_at` ,
        `updated_at`
        )
        VALUES (
        NULL , '".$chargeAmountNotificationData['serial-number']."', '".$orderNo."', '".$chargeAmountNotificationData['risk-information']['eligible-for-protection']['VALUE']."', '".$chargeAmountNotificationData['risk-information']['avs-response']['VALUE']."', '".$chargeAmountNotificationData['risk-information']['cvn-response']['VALUE']."', '".$chargeAmountNotificationData['risk-information']['partial-cc-number']['VALUE']."',
 '".$chargeAmountNotificationData['risk-information']['ip-address']['VALUE']."', '".$chargeAmountNotificationData['risk-information']['buyer-account-age']['VALUE']."', '".$chargeAmountNotificationData['timestamp']['VALUE']."', NOW(), NOW()
    ) ";
    Doctrine_Manager::getInstance()->getCurrentConnection()->execute($query);
  }

  /* end of support tool methods */


  public function authenticateUserSession($type){
      if($type==''){
          sfContext::getInstance()->getUser()->setFlash('error','Session Expired!.Please put your application credentials again',false);
          sfContext::getInstance()->getController()->forward('visa', 'OnlineQueryStatus');
          exit;
      }
      if(sfContext::getInstance()->getUser()->getAttribute('app_id')=='')
          switch($type){
              case 'NIS PASSPORT':
                  sfContext::getInstance()->getUser()->setFlash('error','Session Expired!.Please put your Passport application credentials again',false);
                  sfContext::getInstance()->getController()->forward('passport', 'passportPaymentStatus');
                  exit;
                  break;
              case 'NIS VISA':
                  sfContext::getInstance()->getUser()->setFlash('error','Session Expired!.Please put your Visa application credentials again',false);
                  sfContext::getInstance()->getController()->forward('visa', 'visaStatus');
                  exit;
                  break;
              case 'NIS FREEZONE':
                  sfContext::getInstance()->getUser()->setFlash('error','Session Expired!.Please put your Visa application credentials again',false);
                  sfContext::getInstance()->getController()->forward('visa', 'visaStatus');
                  exit;
                  break;
      }
  }

  public function getInterswitchGatewayTypeId() {
    if (!$this->interswitchTypeId) {
      $this->interswitchTypeId = Doctrine::getTable('PaymentGatewayType')->getGatewayId('Interswitch'); //Interswitch , eTranzact
    }
    return $this->interswitchTypeId;
  }

  public function getEtranzactGatewayTypeId() {
    if(!$this->etranzactTypeId) {
      $this->etranzactTypeId = Doctrine::getTable('PaymentGatewayType')->getGatewayId('eTranzact'); //Interswitch , eTranzact
    }
    return $this->etranzactTypeId;
  }

  public function getPayformeBankGatewayTypeId() {
    if(!$this->payformeTypeId) {
      $this->payformeTypeId = Doctrine::getTable('PaymentGatewayType')->getGatewayId('Pay4me (Bank)'); //payforme Bank
    }
    return $this->payformeTypeId;
  }

  public function getPayformeInterswitchGatewayTypeId() {
    if(!$this->payformeTypeId) {
      $this->payformeTypeId = Doctrine::getTable('PaymentGatewayType')->getGatewayId('Pay4me (Interswitch)'); //payforme Interswitch
    }
    return $this->payformeTypeId;
  }

  public function getPayformeEwalletGatewayTypeId() {
    if(!$this->payformeTypeId) {
      $this->payformeTypeId = Doctrine::getTable('PaymentGatewayType')->getGatewayId('Pay4me (eWallet)'); //payforme Ewallet
    }
    return $this->payformeTypeId;
  }

  public function getPayformeCcGatewayTypeId() {
    if(!$this->payformeTypeId) {
      $this->payformeTypeId = Doctrine::getTable('PaymentGatewayType')->getGatewayId('Pay4me (Credit Card)'); //payforme Ewallet
    }
    return $this->payformeTypeId;
  }

  public function getiPayformeGatewayTypeId() {
    if(!$this->payformeTypeId) {
      $this->payformeTypeId = Doctrine::getTable('PaymentGatewayType')->getGatewayId('SW Global LLC'); //payforme Ewallet
    }
    return $this->payformeTypeId;
  }

  public function updatePassportStatus($gateway,$appId,$transId,$paymentStatus=true){
    $fee = $this->getPassportFeeFromDB($appId);
    if(strtolower($gateway)=='interswitch'){
      $gatewayTypeId = $this->getInterswitchGatewayTypeId();
    }else if(strtolower($gateway)=='etranzact'){
      $gatewayTypeId = $this->getEtranzactGatewayTypeId();
    }else if(strtolower($gateway)=='payforme_bank'){
      $gatewayTypeId = $this->getPayformeBankGatewayTypeId();
    }else if(strtolower($gateway)=='payforme_interswitch'){
      $gatewayTypeId = $this->getPayformeInterswitchGatewayTypeId();
    }else if(strtolower($gateway)=='payforme_ewallet'){
      $gatewayTypeId = $this->getPayformeEwalletGatewayTypeId();
    }else if(strtolower($gateway)=='payforme_credit_card'){
      $gatewayTypeId = $this->getPayformeCcGatewayTypeId();
    }else if($gateway=='iPay4Me'){
      $gatewayTypeId = $this->getiPayformeGatewayTypeId();
    }else{
      throw new Exception("Gateway: $gateway not supported");
    }
    $dollarAmount = $fee['dollar_amount'];
    $nairaAmount = $fee['naira_amount'];
    $transArr = array(
      PassportWorkflow::$PASSPORT_TRANS_SUCCESS_VAR=>$paymentStatus,
      PassportWorkflow::$PASSPORT_TRANSACTION_ID_VAR=>$transId,
      PassportWorkflow::$PASSPORT_APPLICATION_ID_VAR=>$appId,
      PassportWorkflow::$PASSPORT_DOLLAR_AMOUNT_VAR=>$dollarAmount,
      PassportWorkflow::$PASSPORT_NAIRA_AMOUNT_VAR=>$nairaAmount,
      PassportWorkflow::$PASSPORT_GATEWAY_TYPE_VAR=>$gatewayTypeId);
    $cntxt = sfContext::getInstance();
    $dispatcher = $cntxt->getEventDispatcher();
    $dispatcher->notify(new sfEvent($transArr, 'passport.application.payment'));
  }

  public function updateVisaStatus($gateway,$appId,$transId,$paymentStatus=true){
    $visaAppType = Doctrine::getTable('VisaApplication')->getVisaCategory($appId);
    if($visaAppType == 'NIS VISA')
    $fee = $this->getVisaFeeFromDB($appId);
    if($visaAppType == 'NIS FREEZONE')
    $fee = $this->getFreezoneFeeFromDB($appId);
    if(strtolower($gateway)=='interswitch'){
      $gatewayTypeId = $this->getInterswitchGatewayTypeId();
    }else if(strtolower($gateway)=='etranzact'){
      $gatewayTypeId = $this->getEtranzactGatewayTypeId();
    }else if(strtolower($gateway)=='payforme_bank'){
      $gatewayTypeId = $this->getPayformeBankGatewayTypeId();
    }else if(strtolower($gateway)=='payforme_interswitch'){
      $gatewayTypeId = $this->getPayformeInterswitchGatewayTypeId();
    }else if(strtolower($gateway)=='payforme_ewallet'){
      $gatewayTypeId = $this->getPayformeEwalletGatewayTypeId();
    }else if(strtolower($gateway)=='payforme_credit_card'){
      $gatewayTypeId = $this->getPayformeCcGatewayTypeId();
    }else if($gateway=='iPay4Me'){
      $gatewayTypeId = $this->getiPayformeGatewayTypeId();
    }else{
      throw new Exception("Gateway: $gateway not supported");
    }
    $dollarAmount = $fee['dollar_amount'];
    $nairaAmount = $fee['naira_amount'];
    $transArr = array(
      VisaWorkflow::$VISA_TRANS_SUCCESS_VAR=>$paymentStatus,
      VisaWorkflow::$VISA_TRANSACTION_ID_VAR=>$transId,
      VisaWorkflow::$VISA_APPLICATION_ID_VAR=>$appId,
      VisaWorkflow::$VISA_DOLLAR_AMOUNT_VAR=>$dollarAmount,
      VisaWorkflow::$VISA_NAIRA_AMOUNT_VAR=>$nairaAmount,
      VisaWorkflow::$VISA_GATEWAY_TYPE_VAR=>$gatewayTypeId);
    $cntxt = sfContext::getInstance();
    $dispatcher = $cntxt->getEventDispatcher();
    $dispatcher->notify(new sfEvent($transArr, 'visa.application.payment'));

  }

  public function updateEcowasStatus($gateway,$appId,$transId,$paymentStatus=true){
    $fee = $this->getEcowasFee($appId);
    if(strtolower($gateway)=='interswitch'){
      $gatewayTypeId = $this->getInterswitchGatewayTypeId();
    }else if(strtolower($gateway)=='etranzact'){
      $gatewayTypeId = $this->getEtranzactGatewayTypeId();
    }else if(strtolower($gateway)=='payforme_bank'){
      $gatewayTypeId = $this->getPayformeBankGatewayTypeId();
    }else if(strtolower($gateway)=='payforme_interswitch'){
      $gatewayTypeId = $this->getPayformeInterswitchGatewayTypeId();
    }else if(strtolower($gateway)=='payforme_ewallet'){
      $gatewayTypeId = $this->getPayformeEwalletGatewayTypeId();
    }else if(strtolower($gateway)=='payforme_credit_card'){
      $gatewayTypeId = $this->getPayformeCcGatewayTypeId();
    }else{
      throw new Exception("Gateway: $gateway not supported");
    }

    $nairaAmount = $fee['naira_amount'];
    $transArr = array(
      EcowasWorkflow::$ECOWAS_TRANS_SUCCESS_VAR=>$paymentStatus,
      EcowasWorkflow::$ECOWAS_TRANSACTION_ID_VAR=>$transId,
      EcowasWorkflow::$ECOWAS_APPLICATION_ID_VAR=>$appId,
      EcowasWorkflow::$ECOWAS_NAIRA_AMOUNT_VAR=>$nairaAmount,
      EcowasWorkflow::$ECOWAS_GATEWAY_TYPE_VAR=>$gatewayTypeId);

    $cntxt = sfContext::getInstance();
    $dispatcher = $cntxt->getEventDispatcher();
    $dispatcher->notify(new sfEvent($transArr, 'ecowas.application.payment'));
  }
  public function updateEcowasCardStatus($gateway,$appId,$transId,$paymentStatus=true){
    $fee = $this->getEcowasCardFee($appId);
    if(strtolower($gateway)=='interswitch'){
      $gatewayTypeId = $this->getInterswitchGatewayTypeId();
    }else if(strtolower($gateway)=='etranzact'){
      $gatewayTypeId = $this->getEtranzactGatewayTypeId();
    }else if(strtolower($gateway)=='payforme_bank'){
      $gatewayTypeId = $this->getPayformeBankGatewayTypeId();
    }else if(strtolower($gateway)=='payforme_interswitch'){
      $gatewayTypeId = $this->getPayformeInterswitchGatewayTypeId();
    }else if(strtolower($gateway)=='payforme_ewallet'){
      $gatewayTypeId = $this->getPayformeEwalletGatewayTypeId();
    }else if(strtolower($gateway)=='payforme_credit_card'){
      $gatewayTypeId = $this->getPayformeCcGatewayTypeId();
    }else{
      throw new Exception("Gateway: $gateway not supported");
    }
    $nairaAmount = $fee['naira_amount'];
    $transArr = array(
      EcowasCardWorkflow::$ECOWAS_CARD_TRANS_SUCCESS_VAR=>$paymentStatus,
      EcowasCardWorkflow::$ECOWAS_CARD_TRANSACTION_ID_VAR=>$transId,
      EcowasCardWorkflow::$ECOWAS_CARD_APPLICATION_ID_VAR=>$appId,
      EcowasCardWorkflow::$ECOWAS_CARD_NAIRA_AMOUNT_VAR=>$nairaAmount,
      EcowasCardWorkflow::$ECOWAS_CARD_GATEWAY_TYPE_VAR=>$gatewayTypeId);
    $cntxt = sfContext::getInstance();
    $dispatcher = $cntxt->getEventDispatcher();
    $dispatcher->notify(new sfEvent($transArr, 'ecowascard.application.payment'));
  }

  function getPaymentHistory($gateway,$app_id,$app_type)
  {
    $row1=array();
    $arr1=array();
    switch($gateway){
      case 'eTranzact':
        $detArr = Doctrine::getTable('EtranzactResp')->getEtDetails($app_id,$app_type);
        foreach($detArr as $row)
        {
          $row1['app_id']=$row['app_id'];
          $row1['status']=$row['success'];
          $arr1[]= $row1;
        }
        break;
      case 'interswitch':
        $detArr = Doctrine::getTable('InterswitchResp')->getInDetails($app_id,$app_type);
        foreach($detArr as $row)
        {
          $row1['app_id']=$row['app_id'];
          $row1['status']=$row['responce'];
          $arr1[]= $row1;
        }
        break;
    }
    return $arr1;
  }

  function getSuccessResponce($app_id,$app_type){
    $row1=array();
    $arr1=array();
    $resultSet = Doctrine::getTable('EtranzactResp')->getEtDetails($app_id,$app_type);
    if($resultSet && count($resultSet)>0)
    {
      foreach($resultSet as $row)
      {
        $row1['app_id']=$row['app_id'];
        $row1['transaction_id']=$row['transaction_id'];
        $row1['gateway_type']='etranzact';
        $row1['app_type']=$app_type;
        $arr1[]= $row1;
        $arr = null;
      }
    }
    $resultSet = Doctrine::getTable('InterswitchResp')->getInDetails($app_id,$app_type);
    if($resultSet && count($resultSet)>0)
    {
      foreach($resultSet as $row)
      {
        $row1['app_id']=$row['app_id'];
        $row1['transaction_id']=$row['txn_ref_id'];
        $row1['gateway_type']='interswitch';
        $arr1[]= $row1;
        $arr = null;
      }
    }
    return $arr1;
  }

  public function getEcowasFee($app_id = null)
  {
     $feeInfoArr = Doctrine::getTable('EcowasApplication')->getFeeByEcowasId($app_id);
     return $feeInfoArr;
  }

  public function getEcowasCardFee($app_id = null)
  {
     $feeInfoArr = Doctrine::getTable('EcowasCardApplication')->getFeeByEcowasCardId($app_id);
     return $feeInfoArr;
  }

public function getPaymentRequestStatus($appId = null, $appType = null)
{
  switch ($appType)
  {
    case 'ecowas_travel_certificate':
      //payment attampts with pay4me
      $paymenyAttampt = Doctrine::getTable('PaymentRequest')->findByEcowasId($appId)->count();
      if($paymenyAttampt>0)
        return false;
        //if no entry found
        return true;
        break;

    case 'ecowas_residence_card' :
      //payment attampts with pay4me
      $paymenyAttampt = Doctrine::getTable('PaymentRequest')->findByEcowasCardId($appId)->count();
      if($paymenyAttampt>0)
        return false;
        //if no entry found
        return true;
        break;

    case 'NIS PASSPORT' :
      //payment attampts with pay4me
      $paymenyAttampt = Doctrine::getTable('PaymentRequest')->findByPassportId($appId)->count();
      if($paymenyAttampt>0)
        return false;

      //payment attampts with google
      //$dql =
//      $paymenyAttampt = Doctrine::getTable('GcItems')->findByDql("merchant_item_id='$appId' AND item_name='$appType'")->count();
//      if($paymenyAttampt>0)
//        return false;

        //check payment request of ipay4me
      $ipaymenyAttampt = Doctrine::getTable('IPaymentRequest')->checkIpaymentAttempt($appId,$appType);
      if($ipaymenyAttampt)
        return false;

        //if no entry found
        return true;
        break;


    case 'NIS VISA' :
      //payment attampts with pay4me
      $paymenyAttampt = Doctrine::getTable('PaymentRequest')->findByVisaId($appId)->count();
      if($paymenyAttampt>0)
        return false;

      //payment attampts with google
//      $paymenyAttampt = Doctrine::getTable('GcItems')->findByDql("merchant_item_id='$appId' AND item_name='$appType'")->count();
//      if($paymenyAttampt>0)
//        return false;

        //check payment request of ipay4me
      $ipaymenyAttampt = Doctrine::getTable('IPaymentRequest')->checkIpaymentAttempt($appId,$appType);
      if($ipaymenyAttampt)
        return false;
        //if no entry found
        return true;
        break;

    case 'NIS FREEZONE' :

        //check payment request of ipay4me
      $ipaymenyAttampt = Doctrine::getTable('IPaymentRequest')->checkIpaymentAttempt($appId,$appType);
      if($ipaymenyAttampt)
        return false;
        //if no entry found
        return true;
        break;
    case 'NIS VISA ARRIVAL' :

        //check payment request of ipay4me
      $ipaymenyAttampt = Doctrine::getTable('IPaymentRequest')->checkIpaymentAttempt($appId,$appType);
      if($ipaymenyAttampt)
        return false;
        //if no entry found
        return true;
        break;


  }
}
 public function getAmazonCallerRef($application_param = NULL)
  {
    $applicationInputParams = sfConfig::get('app_amazon_caller_reference_settings');
    $callerRef = '';
    foreach ($applicationInputParams as $k =>$v)
    {
      if($v == 'true')
      {
        if($application_param[$k] != '')
        {
          if($callerRef == '')
          $callerRef = $application_param[$k];
          else
          $callerRef = $callerRef."_".$application_param[$k];
        }
      }
    }
    return $callerRef;
  }
  public function getApplicationParam($application_param = NULL,$param = NULL)
  {
    $applicationInputParams = sfConfig::get('app_amazon_caller_reference_settings');
    foreach ($applicationInputParams as $k =>$v)
    {
      if($k == $param)
      {
        if($v)
        {
          return $application_param[$k];
        }
        else
        return '';
      }
    }
    return '';
  }
    public function getEmailIdDetails($appData = null, $emailId = null){
    if(isset($appData) && $appData != ''){
      $appDetails = $appData;
    }
    else{
      $appDetails = sfContext::getInstance()->getRequest()->getParameter('appDetailsForPayment');
    }
    $getAppIdToPaymentProcess = SecureQueryString::DECODE($appDetails);
    $getAppIdToPaymentProcess = SecureQueryString::ENCRYPT_DECRYPT($getAppIdToPaymentProcess);
    $appData = explode('~', $getAppIdToPaymentProcess);
    $appId = $appData[0];
    $appType = $appData[1];
    $returnEmail = '';

    switch($appType){
      case 'visa':
      case 'freezone':
        {
          $visaObj = Doctrine::getTable('VisaApplication')->find($appId);
          if(isset($visaObj) && $visaObj!=''){
            $email = $visaObj->getEmail();
            $isActive = $visaObj->getIsEmailValid();
            $refNo = $visaObj->getRefNo();
            $applicantName = $visaObj->getSurname()." ".$visaObj->getOtherName();
            $returnEmail = $email;
            if(isset ($emailId) && $emailId !=''){
              if(!$isActive){
                $visaObj->setEmail($emailId);
                $visaObj->save();
                $returnEmail = $emailId;
              }
            }
            return array('email'=>$returnEmail,'active'=>$isActive,'app_id'=>$appId,'app_type'=>$appType,'ref_num'=>$refNo,'applicant_name'=>$applicantName);
          }else{
           return false;
          }
        }
        break;
      case 'passport':
        {
          $passportObj = Doctrine::getTable('PassportApplication')->find($appId);
          if(isset($passportObj) && $passportObj!=''){
            $email = $passportObj->getEmail();
            $isActive = $passportObj->getIsEmailValid();
            $refNo = $passportObj->getRefNo();
            $applicantName = $passportObj->getLastName()." ".$passportObj->getFirstName();
            $returnEmail = $email;
            if(isset ($emailId) && $emailId !=''){
              if(!$isActive){
                $passportObj->setEmail($emailId);
                $passportObj->save();
                $returnEmail = $emailId;
              }
            }
            return array('email'=>$returnEmail,'active'=>$isActive,'app_id'=>$appId,'app_type'=>$appType,'ref_num'=>$refNo,'applicant_name'=>$applicantName);
          }else{
           return false;
          }
        }
        break;
    }
  }
  public function generateEmailLink($appData,$email = null){
    if(isset($appData) && $appData != ''){
      $appDetails = $appData;
    }
    else{
      $appDetails = sfContext::getInstance()->getRequest()->getParameter('appDetailsForPayment');
    }
    $getAppIdToPaymentProcess = SecureQueryString::DECODE($appDetails);
    $getAppIdToPaymentProcess = SecureQueryString::ENCRYPT_DECRYPT($getAppIdToPaymentProcess);
    $appData = explode('~', $getAppIdToPaymentProcess);
    $appId = $appData[0];
    $appType = $appData[1];
    $returnEmail = '';
    switch($appType){
      case 'visa':
      case 'freezone':
        {
          $visaObj = Doctrine::getTable('VisaApplication')->find($appId);
          if(isset($visaObj) && $visaObj!=''){
            $getPaymentAll = "";
            $getPaymentAll = $this->getLastDollerPaymentTime($appId,$appType);
            $generatedUrlTime = date('Y-m-d h:m:s');
            $emailUrlParams = $appId."~".$appType."~".$getPaymentAll."~".$email."~".$generatedUrlTime;
            $emailUrlParamsEmail = SecureQueryString::ENCRYPT_DECRYPT($emailUrlParams);
            $emailUrlParamsEmail = SecureQueryString::ENCODE($emailUrlParamsEmail);

            return $emailUrlParamsEmail;
          }
        }
        break;
      case 'passport':
        {
          $passportObj = Doctrine::getTable('PassportApplication')->find($appId);
          if(isset($passportObj) && $passportObj!=''){
            $getPaymentAll = "";
            $getPaymentAll = $this->getLastDollerPaymentTime($appId,$appType);
            $generatedUrlTime = date('Y-m-d h:m:s');
            $emailUrlParams = $appId."~".$appType."~".$getPaymentAll."~".$email."~".$generatedUrlTime;
            $emailUrlParamsEmail = SecureQueryString::ENCRYPT_DECRYPT($emailUrlParams);
            $emailUrlParamsEmail = SecureQueryString::ENCODE($emailUrlParamsEmail);

            return $emailUrlParamsEmail;

        }
        break;
    }
   }
  }
    public function getLastDollerPaymentTime($appId,$appType){

    //last payment attampt for Google
    $googleAppType = "NIS ".strtoupper($appType);
    $gQuery = Doctrine_Query::create()
              ->select('t.last_update')
              ->from('GcPaymentTime t')
              ->where('t.merchant_item_id=?',$appId)
              ->andWhere("t.item_type='$googleAppType'")
              ->orderBy('t.last_update DESC')
              ->limit(1)
              ->execute(array(),Doctrine::HYDRATE_ARRAY);
    if(isset ($gQuery) && count($gQuery)>0){
      $lastGoogleTime = $gQuery[0]['last_update'];
    }else{
      $lastGoogleTime = "1970-01-01 00:00:00";
    }

    //get last payment attempt form Amazon
    $amazonAppType = ucfirst($appType);
    $aQuery = Doctrine_Query::create()
              ->select('t.updated_at')
              ->from('AmazonPaymentRequest t')
              ->where('t.application_id=?',$appId)
              ->andWhere("t.app_type='$amazonAppType'")
              ->orderBy('t.updated_at DESC')
              ->limit(1)
              ->execute(array(),Doctrine::HYDRATE_ARRAY);

    if(isset ($aQuery) && count($aQuery)>0){
      $lastAmazonTime = $aQuery[0]['updated_at'];
    }else{
      $lastAmazonTime = "1970-01-01 00:00:00";
    }
    if($lastAmazonTime == $lastGoogleTime)
    return $lastGoogleTime;
    else{
      //get latest attempt
//      echo  strtotime($lastAmazonTime), "   ",strtotime("1970-01-01 00:00:00");die;
    if($lastAmazonTime > $lastGoogleTime)
      $lastAttemptTime = $lastAmazonTime;
    else
      $lastAttemptTime = $lastGoogleTime;
      return $lastAttemptTime;
    }
  }
    public function getLastPaymentAttemptedTime($appId,$appType){
      $amazonAttempts = 0;
      $googleAttempts = 0;
      $googleAppType = "NIS ".strtoupper($appType);

      $con = Doctrine_Manager::getInstance()->connection();
      // execute SQL query, receive Doctrine_Connection_Statement
      $st = $con->execute("SELECT * FROM gc_payment_time
        WHERE merchant_item_id = '".$appId."' && LOWER(item_type)=LOWER('".$googleAppType."')
        AND last_update>=now()-interval 15 MINUTE");
      // fetch query result
      $gResult = $st->fetchAll();


      if(isset ($gResult) && count($gResult)>0){
          $googleAttempts = count($gResult);
      }
      //get last payment attempt form Amazon
      $amazonAppType = ucfirst($appType);
      $st1 = $con->execute("
    SELECT az_buyer_token_master.* FROM az_buyer_token_master
    LEFT JOIN az_payment_request on az_payment_request.caller_reference = az_buyer_token_master.caller_reference
    WHERE az_payment_request.application_id = '".$appId."' AND LOWER(az_payment_request.app_type)=LOWER('".$amazonAppType."')
    AND az_buyer_token_master.created_at>=now()-interval 15 MINUTE
    ");
      // fetch query result
      $aResult = $st1->fetchAll();

      if(isset ($aResult) && count($aResult)>0){
          $amazonAttempts = count($aResult);
      }
//      echo $amazonAttempts, "  ",$googleAttempts;
      $amazonAttempts = (int)$amazonAttempts;
      $googleAttempts = (int)$googleAttempts;
      $total = ($amazonAttempts + $googleAttempts);
//      echo "dsfgsdf  ".$total;die;
      if($total>1){
        return true;
      }
      else{
        return false;
      }
  }

  public function getApplicationStatusByAmazonTransactionId($amazonTransactionId){
    $query = Doctrine_Query::create()
    ->select('t.*')
    ->from('AmazonIpn t')
    ->where("t.transaction_id='$amazonTransactionId'")
    ->execute(array(),Doctrine::HYDRATE_ARRAY);
    if(isset($query) && is_array($query) && count($query)){
      $app_id = $query[0]['app_id'];
      $app_type = $query[0]['app_type'];
      switch($app_type){
        case 'Visa' :
        case 'Freezone':
          {
            $visaobj = Doctrine::getTable('VisaApplication')->find($app_id);

            if($visaobj->getIspaid()){
              return true;
            }
            else{
              return false;
            }
          }
          break;
        case 'Passport':
          {
            $passportobj = Doctrine::getTable('PassportApplication')->find($app_id);

              if(isset ($passportobj) && $passportobj!="" && $passportobj->getIsPaid()){
                  return true;
              }
              else{
                return false;
              }
          }
          break;
          default :
            return false;
     }
    }
    else{
      return false;
    }
  }
//  public function isValidRefundRequest($paymentGateway = null, $paymentTransactionId = null){
//    return true;
//  }
  public function addAmazonRefundAmountNotification($app_type,$app_id,$order_id,$paymentGatewayType){
    sfContext::getInstance()->getLogger()->err("CHANGES for Param  ".$app_type."    ",$app_id."   ".$order_id,"   ".$paymentGatewayType);
        //get number of payment for an application id
        //$order_id is the transaction number of payment success
        if($this->getAmazonGatewayTypeId() == $paymentGatewayType)
        {
          $amazonIpnCount=Doctrine::getTable('AmazonIpn')->getDuplicateTransactions($app_type,$app_id,$order_id);
          sfContext::getInstance()->getLogger()->err("{REFUND Amazon PaymentIPNResponseAction} Duplicate count:".$amazonIpnCount);

          if($amazonIpnCount==0)
          {
            switch($app_type)
            {
                case 'Passport':
                    Doctrine::getTable('PassportApplication')->setRefundStatus($app_id);
                    Doctrine::getTable('PassportVettingQueue')->setRefundStatus($app_id);
                    $workpool=Doctrine::getTable('Workpool')->getWorkpoolByAppId($app_id,'PassportWorkflow');
                    break;
                case 'Visa':
                case 'Freezone':
                    Doctrine::getTable('VisaApplication')->setRefundStatus($app_id);
                    Doctrine::getTable('VisaVettingQueue')->setRefundStatus($app_id);
                    $workpool=Doctrine::getTable('Workpool')->getWorkpoolByAppId($app_id,'VisaWorkflow');
                    break;
            }
            $total_workpool=$workpool->count();
            if($total_workpool){
                $workpool_arr=$workpool->toArray();
                $ex_id=$workpool_arr['0']['execution_id'];
                if($ex_id!='')
                {
                    Doctrine::getTable('Workpool')->deleteByExecutionId($ex_id);
                    Doctrine::getTable('Execution')->deleteByExecutionId($ex_id);
                    Doctrine::getTable('ExecutionState')->deleteByExecutionId($ex_id);
                }
            }
          }
        }
  }
  public function addAmazonChargeBackAmountNotification($app_type,$app_id,$order_id,$paymentGatewayType){
    sfContext::getInstance()->getLogger()->err("CHANGES for Param  ".$app_type."    ",$app_id."   ".$order_id,"   ".$paymentGatewayType);
        //get number of payment for an application id
        //$order_id is the transaction number of payment success
//        die($this->getAmazonGatewayTypeId().  "   ".$paymentGatewayType);
        if($this->getAmazonGatewayTypeId() == $paymentGatewayType)
        {
          $amazonIpnCount=Doctrine::getTable('AmazonIpn')->getDuplicateTransactions($app_type,$app_id,$order_id);
          sfContext::getInstance()->getLogger()->err("{REFUND Amazon PaymentIPNResponseAction} Duplicate count:".$amazonIpnCount);

          if($amazonIpnCount==0)
          {
            switch($app_type)
            {
                case 'Passport':
                    Doctrine::getTable('PassportApplication')->setRefundStatus($app_id);
                    Doctrine::getTable('PassportVettingQueue')->setRefundStatus($app_id);
                    $workpool=Doctrine::getTable('Workpool')->getWorkpoolByAppId($app_id,'PassportWorkflow');
                    break;
                case 'Visa':
                case 'Freezone':
                    Doctrine::getTable('VisaApplication')->setRefundStatus($app_id);
                    Doctrine::getTable('VisaVettingQueue')->setRefundStatus($app_id);
                    $workpool=Doctrine::getTable('Workpool')->getWorkpoolByAppId($app_id,'VisaWorkflow');
                    break;
            }
            $total_workpool=$workpool->count();
            if($total_workpool){
                $workpool_arr=$workpool->toArray();
                $ex_id=$workpool_arr['0']['execution_id'];
                if($ex_id!='')
                {
                    Doctrine::getTable('Workpool')->deleteByExecutionId($ex_id);
                    Doctrine::getTable('Execution')->deleteByExecutionId($ex_id);
                    Doctrine::getTable('ExecutionState')->deleteByExecutionId($ex_id);
                }
            }
          }
        }
  }
}
