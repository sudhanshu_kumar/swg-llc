<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apundir
 */
interface INisCartItem {
  /** VISA Application */
  const TYPE_VISA = "Visa";
  
  /** VISA Application */
  const TYPE_FREEZONE = "Freezone";

  /** Paspport Application */
  const TYPE_PASSPORT = "Passport";

  /*  VISA ON ARRIVAL Application */
  const TYPE_VAP = "Vap";

  /**
   * Test if this cart item is passport or not
   * @return <boolean> true if passport, false otherwise
   */
  public function isPassport();

  /**
   * Test if this cart item is visa or not
   * @return <boolean> - true if visa, false otherwise
   */
  public function isVisa();

  /**
   * Determines type of application, visa or passport
   * @return <string> - eithe of TYPE_XXXX constant of this interface
   */
  public function getType();

  /**
   * Fetches the application object (Doctrine object) and returns it
   * @return <object> - of applicaiton type, depends on getType()
   */
  public function getApplication();

  /**
   * Get the application id for the cart item
   * @return <int> - application id
   */
  public function getAppId();

  /**
   * Get the name of the applicant for which this cart item belongs
   * @return <string> - full name of the applicant
   */
  public function getName();


  /**
   * Get the price of this application
   */
  public function getPrice();

  /**
   * Returns the application data
   */
  public function getAppData();
}

?>
