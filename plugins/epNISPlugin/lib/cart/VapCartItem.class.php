<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of VapCartItem
 *
 * @author apundir
 */
class VapCartItem extends NisCartItem {

  /**
   * Construct a vap cart item.
   * @param <integer> $appid application id
   * @param <type> $name full name of the applicant
   */
  public function VapCartItem($appid, $name, $fee, $type=INisCartItem::TYPE_VAP) {
    $this->setAppId($appid);
    $this->setName($name);
    $this->setPrice($fee);
    $this->setType($type);
  }

  public function getApplication() {
    return Doctrine::getTable("VapApplication")->find($this->getAppId());
  }
}
?>
