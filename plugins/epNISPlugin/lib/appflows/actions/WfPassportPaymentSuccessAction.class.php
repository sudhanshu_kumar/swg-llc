<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class WfPassportPaymentSuccessAction implements ezcWorkflowServiceObject {
  public function execute( ezcWorkflowExecution $execution ) {
    // get the transaction id
    $transid = $execution->getVariable(PassportWorkflow::$PASSPORT_TRANSACTION_ID_VAR);
    $appid = $execution->getVariable(PassportWorkflow::$PASSPORT_APPLICATION_ID_VAR);
    //$reffid = $execution->getVariable(PassportWorkflow::$PASSPORT_REFERENCE_ID);
    $transstatus = $execution->getVariable(PassportWorkflow::$PASSPORT_TRANS_SUCCESS_VAR);

    $dollarAmount = $execution->getVariable(PassportWorkflow::$PASSPORT_DOLLAR_AMOUNT_VAR);
    $nairaAmount = $execution->getVariable(PassportWorkflow::$PASSPORT_NAIRA_AMOUNT_VAR);
    $gatewayType = $execution->getVariable(PassportWorkflow::$PASSPORT_GATEWAY_TYPE_VAR);

    $gTypeName = Doctrine::getTable('PaymentGatewayType')->find(array($gatewayType));
    $gTypeName = $gTypeName->__toString();

    // TODO:
    // 1. update the passport application table with paid status set to true
    // 2.            and insert the transaction id in the same table
    // 3. If all done successfully return true
    // 4. If all not done successfully return false - log everything


    $q = Doctrine::getTable('PassportApplication')
    ->createQuery('qr')->select('qr.ref_no,qr.processing_country_id,qr.processing_embassy_id,qr.processing_passport_office_id')
    ->where('qr.id = ?', $appid)
    ->execute()->toArray();

    $officeType = "";
    $processingOfficeId = "";
    //get interview date
    if($q[0]['processing_embassy_id']!=""){
        $processingOfficeId =$q[0]['processing_embassy_id'];
        $officeType = 'embassy';
    }
    else{
        $processingOfficeId =$q[0]['processing_passport_office_id'];
        $officeType = 'passport';
    }    

    $interviewDate = InterviewSchedulerHelper::getInstance()->getInterviewDateForPassport($q[0]['processing_country_id'],$appid,$processingOfficeId,$officeType);

/*
    //set default server time zone
    date_default_timezone_set(InterviewSchedulerHelper::getInstance()->get_server_time_zone());
 */

    // *  store the transaction-status payment-transaction-id in passport _application table
    $query = Doctrine_Query::create()
      ->update('PassportApplication pa')
      ->set('pa.ispaid',$transstatus)
      ->set('pa.payment_trans_id',"'".$transid."'")
      ->set('pa.status',"'Paid'")
      ->set('pa.paid_at',"'".date('Y-m-d')."'")
      ->set('pa.interview_date',"'".$interviewDate."'");


    sfContext::getInstance()->getLogger()->err(
    "{Passport PaymentSuccessAction} Passed in var APPID:".$appid.
    ", Xns id:". $transid. ", XnsSts: ".$transstatus.", XnsGtypeName: ".$gTypeName);
      
      $query->set('pa.paid_dollar_amount',"'".$dollarAmount."'")
              ->set('pa.paid_local_currency_amount',"'".$nairaAmount."'");

      $query->set('pa.payment_gateway_id',"'".$gatewayType."'")
            ->where('pa.id = ?', $appid)
            ->execute();
    
    // *  store the app-id, refernce-id in passport_vetting_queue table
    $passportVettingQueue = new PassportVettingQueue();
    $passportVettingQueue->setRefId($q[0]['ref_no']);
    $passportVettingQueue->setApplicationId($appid);
    $passportVettingQueue->setAttribute(Doctrine::ATTR_VALIDATE, Doctrine::VALIDATE_NONE);
    $passportVettingQueue->save();

    
    sfContext::getInstance()->getLogger()->info(
    "{Passport PaymentSuccessAction} Passed in var APPID:".$appid.
    ", Xns id:". $transid. ", XnsSts: ".$transstatus);


      // Mailing process for successfull payment
      $applicantDetail=Doctrine::getTable('PassportApplication')->getPassportDetailsByAppId($appid);
      $gTypeName = Doctrine::getTable('PaymentGatewayType')->find(array($applicantDetail['payment_gateway_id']));
      $payment_gateway_name=$gTypeName->getVarValue();

      if($applicantDetail['is_email_valid']){
       if($payment_gateway_name=='google' || $payment_gateway_name=='amazon'){
          $applicant_name = $applicantDetail['last_name'].' '.$applicantDetail['first_name'];
          $appId = $applicantDetail['id'];
          $refNo = $applicantDetail['ref_no'];
          $status = $applicantDetail['status'];
          $moduleName = 'passport';
          $subject = sfConfig::get('app_mailserver_subject');
          $partialName = 'paymentMailBody' ;
          sfLoader::loadHelpers('Asset');
          $filepath_credit = _compute_public_path('top_bkbd_print.jpg', 'images','','true');
          $taskId = EpjobsContext::getInstance()->addJob('SendMailNotification',$moduleName."/sendEmail", array('applicant_email'=>$applicantDetail['email'],'partialName'=>$partialName,'applicant_name'=>$applicant_name,'appId'=>$appId,'refNo'=>$refNo,'status'=>$status,'image_header'=>$filepath_credit));
       }
      }

    return true;
  }

  public function __toString() {
    return "Passport Payment Successful Actions";
  }
}
