<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class WfVisaPaymentSuccessAction implements ezcWorkflowServiceObject {
  public function execute1( ezcWorkflowExecution $execution ) {
      return true;
  }
  public function execute( ezcWorkflowExecution $execution ) {
    // get the transaction id
    $transid = $execution->getVariable(VisaWorkflow::$VISA_TRANSACTION_ID_VAR);
    $appid = $execution->getVariable(VisaWorkflow::$VISA_APPLICATION_ID_VAR);
    
    $transstatus = $execution->getVariable(VisaWorkflow::$VISA_TRANS_SUCCESS_VAR);

    $dollarAmount = $execution->getVariable(VisaWorkflow::$VISA_DOLLAR_AMOUNT_VAR);
    $nairaAmount = $execution->getVariable(VisaWorkflow::$VISA_NAIRA_AMOUNT_VAR);
    $gatewayType = $execution->getVariable(VisaWorkflow::$VISA_GATEWAY_TYPE_VAR);

    $gTypeName = Doctrine::getTable('PaymentGatewayType')->find(array($gatewayType));
    $gTypeName = $gTypeName->__toString();


    // TODO:
    // 1. update the visa application table with paid status set to true
    // 2.            and insert the transaction id in the same table
    // 3. If all done successfully return true
    // 4. If all not done successfully return false - log everything


    $q = Doctrine::getTable('VisaApplication')
    ->createQuery('qr')->select('qr.ref_no,qr.present_nationality_id,visacategory_id')
    ->where('qr.id = ?', $appid)
    ->execute()->toArray();

    $FreshEntryID = Doctrine::getTable('VisaCategory')->getFreshEntryId();
    $FreshFreeZoneEntryID = Doctrine::getTable('VisaCategory')->getFreshEntryFreezoneId();
    $FreezoneReEntryID = Doctrine::getTable('VisaCategory')->getReEntryFreezoneId();
    $visaTypeID = $q[0]['visacategory_id'];
    if($visaTypeID == $FreshEntryID || $visaTypeID == $FreshFreeZoneEntryID)
    {
      $processingOfficeId = Doctrine::getTable('VisaApplicantInfo')->getOfficeID($appid);
      $officeType = 'embassy';

      //GET PROCESSING COUNTRY ID//
      $coun = Doctrine::getTable('VisaApplicantInfo')
        ->createQuery('con')->select('con.applying_country_id')
        ->where('con.application_id = ?', $appid)
        ->execute()->toArray();
      
      $countryId = ($coun[0]['applying_country_id']!="")?$coun[0]['applying_country_id']:'';
      $interviewDate = InterviewSchedulerHelper::getInstance()->getInterviewDateForVisa($countryId,$appid,$processingOfficeId,$officeType);

    }else if($visaTypeID == $FreezoneReEntryID){
      $processingOfficeId = Doctrine::getTable('ReEntryVisaApplication')->getProcessingCentreID($appid);
      $officeType = 'center';
      $countryId = 'NG';
      $interviewDate = InterviewSchedulerHelper::getInstance()->getInterviewDateForFreezoneVisa($countryId,$appid,$processingOfficeId,$officeType);
    }else
    {
      $processingOfficeId = Doctrine::getTable('ReEntryVisaApplication')->getOfficeID($appid);
      $officeType = 'visa';
      $countryId = 'NG';
      $interviewDate = InterviewSchedulerHelper::getInstance()->getInterviewDateForVisa($countryId,$appid,$processingOfficeId,$officeType);
    }
    //get interview date


//    $interviewDate = InterviewSchedulerHelper::getInstance()->getInterviewDateForVisa($countryId,$appid,$processingOfficeId,$officeType);

/*
    //set default server time zone
    date_default_timezone_set(InterviewSchedulerHelper::getInstance()->get_server_time_zone());
 */

    // *  store the transaction-status payment-transaction-id in Visa _application table
    $query = Doctrine_Query::create()
      ->update('VisaApplication pa')
      ->set('pa.ispaid',$transstatus)
      ->set('pa.payment_trans_id',"'".$transid."'")
      ->set('pa.status',"'Paid'")
      ->set('pa.paid_at',"'".date('Y-m-d')."'")
      ->set('pa.interview_date',"'".$interviewDate."'")
      ->set('pa.payment_gateway_id',"'".$gatewayType."'");

//      if(strtolower($gTypeName)!='google'){
//
//      }
      $query->set('pa.paid_dollar_amount',"'".$dollarAmount."'")
        ->set('pa.paid_naira_amount',"'".$nairaAmount."'");
      $query->where('pa.id = ?', $appid)
      ->execute();
    
    // *  store the app-id, refernce-id in Visa_vetting_queue table
    $VisaVettingQueue = new VisaVettingQueue();
    $VisaVettingQueue->setRefId($q[0]['ref_no']);
    $VisaVettingQueue->setApplicationId($appid);
    $VisaVettingQueue->setAttribute(Doctrine::ATTR_VALIDATE, Doctrine::VALIDATE_NONE);
    $VisaVettingQueue->save();




    sfContext::getInstance()->getLogger()->info(
    "{Visa PaymentSuccessAction} Passed in var APPID:".$appid.
    ", Xns id:". $transid. ", XnsSts: ".$transstatus);


      // Mailing process for successfull payment
      $applicantDetail=Doctrine::getTable('VisaApplication')->getVisaDetailsByAppId($appid);
      $gTypeName = Doctrine::getTable('PaymentGatewayType')->find(array($applicantDetail['payment_gateway_id']));
      $payment_gateway_name=$gTypeName->getVarValue();
      $gZoneTypeName = Doctrine::getTable('VisaZoneType')->getFreeZoneId();

      if($applicantDetail['is_email_valid']){
       if($payment_gateway_name=='google' || $payment_gateway_name=='amazon'){
          $applicant_name = $applicantDetail['title'].' '.$applicantDetail['surname'].' '.$applicantDetail['other_name'];
          $appId = $applicantDetail['id'];
          $refNo = $applicantDetail['ref_no'];
          $status = $applicantDetail['status'];
          $zone_type_id = $applicantDetail['zone_type_id'];
          $moduleName = 'visa';
          if($zone_type_id==$gZoneTypeName){
            $zone_name='Free Zone';
          }else{
            $zone_name='Visa';
          }
          $subject = sfConfig::get('app_mailserver_subject');
          $partialName = 'paymentMailBody' ;
          sfLoader::loadHelpers('Asset');
          $filepath_credit = _compute_public_path('top_bkbd_print.jpg', 'images','','true');
          $taskId = EpjobsContext::getInstance()->addJob('SendMailNotification',$moduleName."/sendEmail", array('applicant_email'=>$applicantDetail['email'],'partialName'=>$partialName,'applicant_name'=>$applicant_name,'appId'=>$appId,'refNo'=>$refNo,'status'=>$status,'zone_name'=>$zone_name,'image_header'=>$filepath_credit));
       }
      }

    
    return true;
  }

  public function __toString() {
    return "Visa Payment Successful Actions";
  }
}
