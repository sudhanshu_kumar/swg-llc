<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

  class WfPassportVetterInputSuccessAction implements ezcWorkflowServiceObject {



  public function __construct()
  {

  }
  public function execute( ezcWorkflowExecution $execution ) {

    //$approverStatus = $execution->getVariable(PassportWorkflow::$APPROVER_SUCCESS_VAR);
    $appid = $execution->getVariable(PassportWorkflow::$PASSPORT_APPLICATION_ID_VAR_FROM_VETTER);
    // $comments = $execution->getVariable(PassportWorkflow::$APPROVER_COMMENT_VAR);


    //GET vettring grant status id from global table
    $gid = Doctrine::getTable('PassportVettingRecommendation')->getGrantId();

    $q = Doctrine::getTable('PassportVettingInfo')
    ->createQuery('qr')->select('recomendation_id')
    ->where('application_id = ?', $appid)
    ->execute()->toArray();
    
       
    if ( $q[0]['recomendation_id'] == $gid) {
      sfContext::getInstance()->getLogger()->info(
      "Passport Application Id:".$appid.", Vetter status:Approved");
      $execution->setVariable(PassportWorkflow::$PASSPORT_SUCCESS_VAR_FROM_VETTER,true);
      return true;
    }
    else
    {
      sfContext::getInstance()->getLogger()->info(
      "Passport Application Id:".$appid.", Vetter status:Rejected");
      $execution->setVariable(PassportWorkflow::$PASSPORT_SUCCESS_VAR_FROM_VETTER,false);
      return true;
    }

    /*if($q[0]['flow_id']==PassportVettingStatusTable::$STATUS_GRANT)
    {
      sfContext::getInstance()->getLogger()->info(
      "Application Id:".$appid.", Vetter status:Approved");
      return true;
    }
    else
    {
      sfContext::getInstance()->getLogger()->info(
      "Application Id:".$appid.", Vetter status:Rejected");
      return false;
    }*/
 }
  public function __toString() {
    return "Passport Vetter Successful Actions";
  }
}
