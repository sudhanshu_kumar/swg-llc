<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class WfEcowasCardVetterSuccessAction implements ezcWorkflowServiceObject {



  public function __construct()
  {

  }
  public function execute( ezcWorkflowExecution $execution ) {

    //$comments = $execution->getVariable(EcowasWorkflow::$VETTER_COMMENT_VAR);
    //$vetterStatus = $execution->getVariable(EcowasWorkflow::$VETTER_SUCCESS_VAR);
    $appid = $execution->getVariable(EcowasCardWorkflow::$ECOWAS_CARD_APPLICATION_ID_VAR_FROM_VETTER);

    //$reffid = $execution->getVariable(EcowasWorkflow::$ECOWAS_REFERENCE_ID);

    // get ecowas number
  // $ecowasNo = EcowasNumberGeneratorHelper::getInstance()->getEcowasNumber();

    $q1 = Doctrine::getTable('EcowasCardApplication')
    ->createQuery('qr')->select('qr.ref_no')
    ->where('qr.id = ?', $appid)
    ->execute()->toArray();
    $ecowasNo = $q1[0]['ref_no'];

    Doctrine_Query::create()
      ->update('EcowasCardApplication pa')
      ->set('pa.status',"'Vetted'")
      ->set('pa.status_updated_date',"'".date('Y-m-d')."'")
     // ->set('pa.ecowas_tc_no',"'".$ecowasNo."'")
      ->where('pa.id = ?', $appid)
      ->execute();

    $q = Doctrine::getTable('EcowasCardApplication')
    ->createQuery('qr')->select('qr.ref_no')
    ->where('qr.id = ?', $appid)
    ->execute()->toArray();

    // *  store the app-id, refernce-id in ecowas_approval_queue table
    $ecowasApprovalQueue = new EcowasCardApprovalQueue();
    $ecowasApprovalQueue->setRefId($q[0]['ref_no']);
    $ecowasApprovalQueue->setApplicationId($appid);
    $ecowasApprovalQueue->setAttribute(Doctrine::ATTR_VALIDATE, Doctrine::VALIDATE_NONE);
    $ecowasApprovalQueue->save();

    $deleted = Doctrine_Query::create()
    ->delete()
    ->from('EcowasCardVettingQueue u')
    ->where('u.application_id = ?', $appid)
    ->andWhere('u.ref_id = ?', $q[0]['ref_no'])
    ->execute();

    sfContext::getInstance()->getLogger()->info(
      "Ecowas Card vetter status: Success");

    return  true;
  }

  public function __toString() {
    return "Ecowas Card Vetter Successful Actions";
  }
}
