<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class WfEcowasVetterFailureAction implements ezcWorkflowServiceObject {
  


  public function __construct( )
  {

  }
  public function execute( ezcWorkflowExecution $execution ) {

    $appid = $execution->getVariable(EcowasWorkflow::$ECOWAS_APPLICATION_ID_VAR_FROM_VETTER);
    sfContext::getInstance()->getLogger()->info(
      "Ecowas Application Id:".$appid." ,vetter status: rejected");

    $q = Doctrine::getTable('EcowasApplication')
    ->createQuery('qr')->select('qr.ref_no')
    ->where('qr.id = ?', $appid)
    ->execute()->toArray();

    Doctrine_Query::create()
      ->update('EcowasApplication pa')
      ->set('pa.status',"'Rejected by Vetter'")
      ->where('pa.id = ?', $appid)
      ->execute();
    
    $deleted = Doctrine_Query::create()
    ->delete()
    ->from('EcowasVettingQueue u')
    ->where('u.application_id = ?', $appid)
    ->andWhere('u.ref_id = ?', $q[0]['ref_no'])
    ->execute();
   
    return true;
  }

  public function __toString() {
    return "Ecowas Vetter Failure Actions";
  }
}
