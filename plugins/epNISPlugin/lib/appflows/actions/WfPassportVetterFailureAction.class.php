<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class WfPassportVetterFailureAction implements ezcWorkflowServiceObject {
  


  public function __construct( )
  {

  }
  public function execute( ezcWorkflowExecution $execution ) {

    $appid = $execution->getVariable(PassportWorkflow::$PASSPORT_APPLICATION_ID_VAR_FROM_VETTER);
    sfContext::getInstance()->getLogger()->info(
      "Passport Application Id:".$appid." ,vetter status: rejected");

    $q = Doctrine::getTable('PassportApplication')
    ->createQuery('qr')->select('qr.ref_no')
    ->where('qr.id = ?', $appid)
    ->execute()->toArray();

    Doctrine_Query::create()
      ->update('PassportApplication pa')
      ->set('pa.status',"'Rejected by Vetter'")
      ->where('pa.id = ?', $appid)
      ->execute();
    
    $deleted = Doctrine_Query::create()
    ->delete()
    ->from('PassportVettingQueue u')
    ->where('u.application_id = ?', $appid)
    ->andWhere('u.ref_id = ?', $q[0]['ref_no'])
    ->execute();


      // Mailing process for successfull payment
      $applicantDetail=Doctrine::getTable('PassportApplication')->getPassportDetailsByAppId($appid);
      $gTypeName = Doctrine::getTable('PaymentGatewayType')->find(array($applicantDetail['payment_gateway_id']));
      $payment_gateway_name=$gTypeName->getVarValue();

      if($applicantDetail['is_email_valid']){
       if($payment_gateway_name=='google' || $payment_gateway_name=='amazon'){
          $applicant_name = $applicantDetail['last_name'].' '.$applicantDetail['first_name'];
          $appId = $applicantDetail['id'];
          $refNo = $applicantDetail['ref_no'];
          $status = $applicantDetail['status'];
          $moduleName = 'passport';
          $subject = sfConfig::get('app_mailserver_subject');
          $partialName = 'paymentMailBody' ;
          sfLoader::loadHelpers('Asset');
          $filepath_credit = _compute_public_path('top_bkbd_print.jpg', 'images','','true');
          $taskId = EpjobsContext::getInstance()->addJob('SendMailNotification',$moduleName."/sendEmail", array('applicant_email'=>$applicantDetail['email'],'partialName'=>$partialName,'applicant_name'=>$applicant_name,'appId'=>$appId,'refNo'=>$refNo,'status'=>$status,'image_header'=>$filepath_credit));
       }
      }


    return true;
  }

  public function __toString() {
    return "Passport Vetter Failure Actions";
  }
}
