<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class VisaListener {


  public static function getExecutionIdFromAppId($appid) {
    $q = Doctrine::getTable('Workpool')
    ->createQuery('qr')->select('execution_id')
    ->where('application_id = ?', $appid)
    ->andWhere('flow_name = ?', VisaWorkflow::$VISA_FLOW_NAME)
    ->execute()->toArray();
    
    if(!isset($q[0]['execution_id']))
        throw new Exception('OldExecutionState');
      else
        return $q[0]['execution_id'];
  }

  public static function getDBInstance() {
    $db = null;
    $db = WorkflowHelper::getDbInstance();

    ezcDbInstance::set( $db );
    return $db = ezcDbInstance::get();
  }
  public static function getDBExecuter($appid = null) {
    // TODO: get these values from symfony config file
    $workflowID  = null;
    $db = VisaListener::getDBInstance();
    $executer = null;
    if ($appid == null) {
      $executer = new ezcWorkflowDatabaseExecution( $db );
    }
    else{
        try{
              $workflowID  = VisaListener::getExecutionIdFromAppId($appid);
              $executer = new ezcWorkflowDatabaseExecution( $db,intval($workflowID));
        }
        catch(Exception $e)
        {
              throw new Exception($e->getMessage());
        }
    }
    return $executer;
  }

  public static function newApplication (sfEvent $event) {
    // * fetch the application id from event
    $evtData = $event->getSubject();
    $appid = $evtData [ VisaWorkflow::$VISA_APPLICATION_ID_VAR ];

    // * instantiate the new workflow object

    //$workflow = new VisaWorkflow();

    $excuter =  VisaListener::getDBExecuter();

    // Set up workflow definition storage (database).
    $db =  VisaListener::getDBInstance();
    $definition = new ezcWorkflowDatabaseDefinitionStorage($db);

    // Load latest version of workflow named "Registration".
    $workflow = $definition->loadByName(VisaWorkflow::$VISA_FLOW_NAME);

    $excuter->workflow = $workflow;
    $workflowId = $excuter->start();


    // TODO: Find out name uniqueness funda
    $workflowName = VisaWorkflow::$VISA_FLOW_NAME;

    // *  store the app-id, worflow id, workflow name in workpool table
    $workpool = new Workpool();
    $workpool->setFlowName($workflowName);
    $workpool->setExecutionId($workflowId);
    $workpool->setApplicationId($appid);
    $workpool->save();
  }

  public static function newPayment (sfEvent $event) {
    // fetch the variables of interests - these are the one
    // which needs to be passed to workflow

    $evtData = $event->getSubject();

    $appid = $evtData [VisaWorkflow::$VISA_APPLICATION_ID_VAR ];
    $trasid = $evtData [VisaWorkflow::$VISA_TRANSACTION_ID_VAR ];
    $tranStatus = $evtData [VisaWorkflow::$VISA_TRANS_SUCCESS_VAR ];
    $dollarAmount = $evtData [VisaWorkflow::$VISA_DOLLAR_AMOUNT_VAR ];
    $nairaAmount = $evtData [VisaWorkflow::$VISA_NAIRA_AMOUNT_VAR ];
    $gatewayType = $evtData [ VisaWorkflow::$VISA_GATEWAY_TYPE_VAR];

    if(empty($dollarAmount)){$dollarAmount=0;}
    if(empty($nairaAmount)){$nairaAmount=0;}

    // load the workflow
    try{
      $excuter = VisaListener::getDBExecuter($appid);
    }
    catch(Exception $e)
    {
        if($e->getMessage()=='OldExecutionState'){
            VisaListener::newApplication($event);
            VisaListener::newPayment($event);
            $excuter = VisaListener::getDBExecuter($appid);
        }
        $ctx = sfContext::getInstance();
        $action = $ctx->getActionStack()->getLastEntry()->getActionInstance();
        if($e->getMessage()!='OldExecutionState'){
            $action->redirect('admin/error404');
        }
    }
    
    $excuter->resume(array(
        VisaWorkflow::$VISA_TRANS_SUCCESS_VAR=>$tranStatus,
        VisaWorkflow::$VISA_TRANSACTION_ID_VAR=>$trasid,
        VisaWorkflow::$VISA_APPLICATION_ID_VAR=>$appid,
        VisaWorkflow::$VISA_DOLLAR_AMOUNT_VAR=>$dollarAmount,
        VisaWorkflow::$VISA_NAIRA_AMOUNT_VAR=>$nairaAmount,
        VisaWorkflow::$VISA_GATEWAY_TYPE_VAR=>$gatewayType
      ));

    
  }

  public static function newVetter (sfEvent $event) {
    // fetch the variables of interests - these are the one
    // which needs to be passed to workflow

    $evtData = $event->getSubject();

    $appid = $evtData [ VisaWorkflow::$VISA_APPLICATION_ID_VAR_FROM_VETTER ];

    // load the workflow
    try
    {
      $excuter = VisaListener::getDBExecuter($appid);
    }
    catch(Exception $e)
    {
       $ctx = sfContext::getInstance();
       $action = $ctx->getActionStack()->getLastEntry()->getActionInstance();
       $action->redirect('admin/error404');
    }

      // * resume workflow for next input node
    $excuter->resume(array(
        VisaWorkflow::$VISA_APPLICATION_ID_VAR_FROM_VETTER=>$appid
      ));
   
  }
  public static function newApprover (sfEvent $event) {
    // fetch the variables of interests - these are the one
    // which needs to be passed to workflow

    $evtData = $event->getSubject();

    $appid = $evtData [ VisaWorkflow::$VISA_APPLICATION_ID_VAR_FROM_APPROVER ];
    
    // load the workflow
    try
    {
      $excuter = VisaListener::getDBExecuter($appid);
    }
    catch(Exception $e)
    {
       $ctx = sfContext::getInstance();
       $action = $ctx->getActionStack()->getLastEntry()->getActionInstance();
       $action->redirect('admin/error404');
    }
    
    // * resume workflow for next input node
    $excuter->resume(array(
        VisaWorkflow::$VISA_APPLICATION_ID_VAR_FROM_APPROVER=>$appid
      ));

  }
}