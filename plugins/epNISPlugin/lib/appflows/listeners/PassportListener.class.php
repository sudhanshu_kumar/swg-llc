<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class PassportListener {
 

  public static function getExecutionIdFromAppId($appid) {
   $q = Doctrine::getTable('Workpool')
      ->createQuery('qr')->select('execution_id')
      ->where('application_id = ?', $appid)
      ->andWhere('flow_name = ?', PassportWorkflow::$PASSPORT_FLOW_NAME)
      ->execute()->toArray();
      if(!isset($q[0]['execution_id']))
        throw new Exception('OldExecutionState');
      else
        return $q[0]['execution_id'];
      
  }

  public static function getDBInstance() {
    $db = null;
    $db = WorkflowHelper::getDbInstance();
    ezcDbInstance::set( $db );
    return $db = ezcDbInstance::get();
  }
  public static function getDBExecuter($appid = null) {
    // TODO: get these values from symfony config file
    $workflowID  = null;
    $db = PassportListener::getDBInstance();
    $executer = null;
    if ($appid == null) {
      $executer = new ezcWorkflowDatabaseExecution( $db );
    } else {

          try {
              $workflowID  = PassportListener::getExecutionIdFromAppId($appid);
              $executer = new ezcWorkflowDatabaseExecution( $db,intval($workflowID));
          } catch (Exception $e) {
              throw new Exception($e->getMessage());
          }

    }
    return $executer;
  }

  public static function newApplication (sfEvent $event) {
    // * fetch the application id from event
    $evtData = $event->getSubject();
    $appid = $evtData [ PassportWorkflow::$PASSPORT_APPLICATION_ID_VAR ];

    // * instantiate the new workflow object
    //$workflow = new PassportWorkflow();
    $excuter = PassportListener::getDBExecuter();

    // Set up workflow definition storage (database).
    $db = PassportListener::getDBInstance();
    $definition = new ezcWorkflowDatabaseDefinitionStorage($db);

    // Load latest version of workflow named "Registration".
    $workflow = $definition->loadByName(PassportWorkflow::$PASSPORT_FLOW_NAME);

    $excuter->workflow = $workflow;
    $workflowId = $excuter->start();


    // TODO: Find out name uniqueness funda
    $workflowName = PassportWorkflow::$PASSPORT_FLOW_NAME;

    // *  store the app-id, worflow id, workflow name in workpool table
    $workpool = new Workpool();
    $workpool->setFlowName($workflowName);
    $workpool->setExecutionId($workflowId);
    $workpool->setApplicationId($appid);
    $workpool->save();
  }

  public static function newPayment (sfEvent $event) {
    // fetch the variables of interests - these are the one
    // which needs to be passed to workflow

    $evtData = $event->getSubject();

    $appid = $evtData [ PassportWorkflow::$PASSPORT_APPLICATION_ID_VAR ];
    $trasid = $evtData [ PassportWorkflow::$PASSPORT_TRANSACTION_ID_VAR ];
    $tranStatus = $evtData [ PassportWorkflow::$PASSPORT_TRANS_SUCCESS_VAR ];
    $dollarAmount = $evtData [ PassportWorkflow::$PASSPORT_DOLLAR_AMOUNT_VAR];
    $nairaAmount = $evtData [ PassportWorkflow::$PASSPORT_NAIRA_AMOUNT_VAR];
    $gatewayType = $evtData [ PassportWorkflow::$PASSPORT_GATEWAY_TYPE_VAR];
    if(empty($dollarAmount)){$dollarAmount=0;}
    if(empty($nairaAmount)){$nairaAmount=0;}

   // $reffid = $evtData [ PassportWorkflow::$PASSPORT_REFERENCE_ID];

    // load the workflow
    try{
      $excuter = PassportListener::getDBExecuter($appid);
    }
    catch(Exception $e)
    {
//        echo "<pre>exception '".__CLASS__ ."' with message '".$e->getMessage()."' in ".$e->getFile().":".$e->getLine()."\nStack trace:\n".$e->getTraceAsString();
//        die();
        if($e->getMessage()=='OldExecutionState'){
            PassportListener::newApplication($event);
            PassportListener::newPayment($event);
            $excuter = PassportListener::getDBExecuter($appid);
        }
        else{
           $ctx = sfContext::getInstance();
           $action = $ctx->getActionStack()->getLastEntry()->getActionInstance();
           if($e->getMessage()!='OldExecutionState'){
                $action->redirect('admin/error404');
           }
        }
    }
    $excuter->resume(array(
        PassportWorkflow::$PASSPORT_TRANS_SUCCESS_VAR=>$tranStatus,
        PassportWorkflow::$PASSPORT_TRANSACTION_ID_VAR=>$trasid,
        PassportWorkflow::$PASSPORT_APPLICATION_ID_VAR=>$appid,
        PassportWorkflow::$PASSPORT_DOLLAR_AMOUNT_VAR=>$dollarAmount,
        PassportWorkflow::$PASSPORT_NAIRA_AMOUNT_VAR=>$nairaAmount,
        PassportWorkflow::$PASSPORT_GATEWAY_TYPE_VAR=>$gatewayType
      ));

    // PassportWorkflow::$PASSPORT_REFERENCE_ID=>$reffid
      
  }

  public static function newVetter (sfEvent $event) {
    // fetch the variables of interests - these are the one
    // which needs to be passed to workflow

    $evtData = $event->getSubject();

   $appid = $evtData [ PassportWorkflow::$PASSPORT_APPLICATION_ID_VAR_FROM_VETTER ];
   
    // load the workflow
    try{
      $excuter = PassportListener::getDBExecuter($appid);
    }
    catch(Exception $e)
    {
        $ctx = sfContext::getInstance();
       $action = $ctx->getActionStack()->getLastEntry()->getActionInstance();
       $action->redirect('admin/error404');
    }
        
      // * resume workflow for next input node
    $excuter->resume(array(
        PassportWorkflow::$PASSPORT_APPLICATION_ID_VAR_FROM_VETTER=>$appid
      ));
    
    //$excuter->resume(array(
      //  PassportWorkflow::$PASSPORT_SUCCESS_VAR_FROM_VETTER=>true
      //));
  }
  public static function newApprover (sfEvent $event) {
    // fetch the variables of interests - these are the one
    // which needs to be passed to workflow

    $evtData = $event->getSubject();

    $appid = $evtData [ PassportWorkflow::$PASSPORT_APPLICATION_ID_VAR_FROM_APPROVER ];
    //$approverStatus = $evtData [ PassportWorkflow::$APPROVER_SUCCESS_VAR ];
    //$approverComments = $evtData [ PassportWorkflow::$APPROVER_COMMENT_VAR ];

    // load the workflow
    try{
      $excuter = PassportListener::getDBExecuter($appid);
    }
    catch(Exception $e)
    {        
       $ctx = sfContext::getInstance();
       $action = $ctx->getActionStack()->getLastEntry()->getActionInstance();
       $action->redirect('admin/error404');
       
    }
    //$excuter->resume(array('approverInput'=>'NextStep'));
    
    // * resume workflow for next input node
    $excuter->resume(array(
        PassportWorkflow::$PASSPORT_APPLICATION_ID_VAR_FROM_APPROVER=>$appid
      ));


  }
}