<?php
/**
 * This helper class provides functionatlity
 * related to workflow
 */

class WorkflowHelper {

  public static function getDatabaseDsn() {
    // TODO: Change it so that this is picked from the
    // application configuration too

   $conn =Doctrine_Manager::connection()->getManager()->getConnections();
   $db = $conn['connection_nis']->getOptions();
       
    $dbdsn = explode(':',$db['dsn']);
    $dbarray = explode(';',$dbdsn[1]);
    $dbhost = explode('=',$dbarray[0]);
    $dbname = explode('=',$dbarray[1]);
    
    return $dbdsn[0]."://".$db['username'].":".$db['password']."@".$dbhost[1]."/".$dbname[1];
  }

  public static function getDbInstance() {
    return ezcDbFactory::create(self::getDatabaseDsn());
  }
}