<?php

/**
 * PluginVisaTypeTable
 *
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class PluginVisaTypeTable extends GlobalMasterTable
{
  public static $FREE_ZONE_VISA = "Freezone Visa";
    /**
     * Returns an instance of this class.
     *
     * @return object PluginVisaTypeTable
     */
  public static function getInstance()
  {
    return Doctrine_Core::getTable('PluginVisaType');
  }

  public static function getCachedQuery() {
    $query = Doctrine_Query::create()
    ->from('VisaType vt')
    //    ->useResultCache(true)
    ;
    return $query;
  }
  public function getFreeZoneVisaTypeId() {
    return $this->getVisaTypeId(self::$FREE_ZONE_VISA);
  }
  private function getVisaTypeId($cType) {
    // TODO: Make it cached one
    $feid = $this->createQuery('ca')
    ->select('ca.id')
    ->addWhere('ca.var_value=?',$cType)
    //    ->useResultCache(true)
    ->execute(array(), Doctrine::HYDRATE_ARRAY);
    if(isset($feid[0]['id'])){
      return $feid[0]['id'];
    }else{
      return false;
    }
  }
  public static function getCachedQueryFreezone() {
    $query = Doctrine_Query::create()
    ->from('VisaType vt')
    ->andWhere("var_value = 'Freezone Visa'")
    //    ->useResultCache(true)
    ;
    return $query;
  }

  /**
   * [WP: 089] => CR: 128
   * @param <type> $countryId
   * @return <type> 
   */
  public static function getVisaTypeCachedQueryForVoap($countryId='') {
    $query = Doctrine_Query::create()
    ->from('VisaType vt');
    if($countryId == 'KE'){
        $query->whereIn('vt.id',array(32,43)); // 32 means Transit and 43 means Tourist/Visitor        
    }else{
        $query->where('vt.id = ?', 30); // 30 means business
    }
    return $query;
  }
  
}