<?php

/**
 * PluginVisaVettingQueueTable
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class PluginVisaVettingQueueTable extends Doctrine_Table
{
    /**
     * Returns an instance of this class.
     *
     * @return object PluginVisaVettingQueueTable
     */
    public static function getInstance()
    {
        return Doctrine_Core::getTable('PluginVisaVettingQueue');
    }
}