<?php

/**
 * PluginPassportFatherAddressTable
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class PluginPassportFatherAddressTable extends AddressMasterTable
{
    /**
     * Returns an instance of this class.
     *
     * @return object PluginPassportFatherAddressTable
     */
    public static function getInstance()
    {
        return Doctrine_Core::getTable('PluginPassportFatherAddress');
    }
}