<?php

/**
 * CurrencyMaster form.
 *
 * @package    form
 * @subpackage CurrencyMaster
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class VisaArrivalForm extends VapApplicationForm
{

    public function configure()
    {
        //Unset Field
        unset($this['created_at'],$this['updated_at'],$this['document_3'],$this['document_4'],$this['document_5'],$this['vap_company_id'],$this['customer_service_number'], $this['ref_no'],$this['payment_trans_id'],$this['ispaid'],$this['term_chk_flg'],$this['is_email_valid']);

        ## step 1 validations...
        $this->step1Validation();

        ## step 2 validations...
        $this->step2Validation();

        ## step 3 validations...
        $this->step3Validation();

        ## step 4 validations...
        $this->step4Validation();
      

        $this->validatorSchema->setOption('allow_extra_fields', true);


        //Compare issues date and compare date
        $this->validatorSchema->setPostValidator(new sfValidatorAnd(array(
                    new sfValidatorCallback(array('callback' => array($this, 'checkIssueDate')))
                )
            ));
    }

    private function step3Validation(){
      $this->widgetSchema['deported_status'] = new sfWidgetFormChoice(array('choices' => array('No' => 'No', 'Yes' => 'Yes')),array('onChange'=>'getDeportedCountryStatus()'));
      $this->widgetSchema->setLabel('deported_status','Have you ever been deported?');

      $this->widgetSchema['contagious_disease'] = new sfWidgetFormChoice(array('choices' => array('No' => 'No', 'Yes' => 'Yes')));
      $this->widgetSchema->setLabel('contagious_disease','Have you ever been infected by any contagious disease (e.g. Tuberculosis) or suffered serious mental illness?');
      
      $this->widgetSchema['police_case'] = new sfWidgetFormChoice(array('choices' => array('No' => 'No', 'Yes' => 'Yes')));
      $this->widgetSchema->setLabel('police_case','Have you ever been arrested or convicted for an offence (even though subject to pardon)?');

      $this->widgetSchema['narcotic_involvement'] = new sfWidgetFormChoice(array('choices' => array('No' => 'No', 'Yes' => 'Yes')));
      $this->widgetSchema->setLabel('narcotic_involvement','Have you ever been involved in narcotic activity?');
      
      $this->widgetSchema->setLabel('deported_status','Have you ever been deported?');

      $this->widgetSchema['visa_fraud_status'] = new sfWidgetFormChoice(array('choices' => array('No' => 'No', 'Yes' => 'Yes')));
      $this->widgetSchema->setLabel('visa_fraud_status','Have you sought to obtain<br/> visa by mis-representation or fraud?');
      
      $this->widgetSchema->setLabel('deported_county_id','If you have ever been deported from which country?');
      
      $this->widgetSchema['deported_county_id']->setOption('add_empty','-- Please Select --');
      $this->widgetSchema['deported_county_id']->setOption('query',CountryTable::getCachedQuery());      
      if(isset($_REQUEST['vap_application']['deported_status']) && $_REQUEST['vap_application']['deported_status'] == 'No'){
        $deported_county_required = false;
      }else {
        $deported_county_required = true;      
      }
      $this->validatorSchema['deported_county_id'] = new sfValidatorDoctrineChoice(array('model' => 'Country','required'=> $deported_county_required), array('required'=>'Please select deported country.'));
      
      $this->validatorSchema->setPostValidator(new sfValidatorAnd(array(
          new sfValidatorCallback(array('callback' => array($this, 'checkDebortedCountry'))))
      ));
    }
    private function step4Validation(){

        $countryId = $this->getOption('applying_country_id');

        if($countryId == 'KE'){
            $requiredFlag = false;
        }else{
            $requiredFlag = true;
        }

        
        $this->widgetSchema['vap_company_id'] =    new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('VapCompany'), 'add_empty' => true));
        $this->widgetSchema['vap_company_id']->setOption('add_empty','-- Please Select --');
        $this->widgetSchema['customer_service_number'] = new sfWidgetFormInputText();
        $this->widgetSchema['customer_service_number']->setAttributes(array('maxlength'=>'155'));
        if($requiredFlag){

            $this->widgetSchema['applicant_type'] =    new sfWidgetFormSelectRadio(array('default'=> 'GB','choices' => array('GB' => 'Government Business', 'PB' => 'Private Business')),array('onclick'=>'checkDisplayCompanyCustomer()'));
            
            for($i=1;$i<=2;$i++){
              $this->widgetSchema['document_'.$i] = new sfWidgetFormInputFile();
            }
        }
        $this->widgetSchema['boarding_place'] = new sfWidgetFormInputText();
        $this->widgetSchema['flight_number'] = new sfWidgetFormInputText();
        $this->widgetSchema['flight_carrier'] = new sfWidgetFormInputText();

        $arrCenter = array(''=>'Please Select Center','1'=>'Nnamdi Azikiwe international Airport,Abuja','2'=>'Mallam Aminu Kano Airport, Kano','3'=>'Murtala Mohammed Airport, Lagos'
                      ,'4'=>'Margret Ekpo Airport,Calabar','5'=>'PortHarcourt International Airport, Rivers.');
        $this->widgetSchema['processing_country_id'] =    new sfWidgetFormChoice(array('choices' => array('NG'=>'Nigeria')));
        $this->widgetSchema['processing_center_id'] =    new sfWidgetFormDoctrineChoice(array('model' => 'VapProcessingCentre','method' => 'getVar_value','add_empty' => 'Please select center'));


        ## Term and condition
        $this->widgetSchema['terms_id'] = new sfWidgetFormInputCheckbox(array(),array('style'=>''));
        $this->widgetSchema->setLabels(array('terms_id' => '&nbsp;'));
        $this->widgetSchema->setHelps(array('terms_id' => 'I understand that I will be required to comply with the immigration / Alien
     and other laws governing entry of the immigrants into the country to which I now apply for Visa / Entry Permit.'),array('style'=>''));
        $this->validatorSchema['terms_id'] = new sfValidatorString(array('required' => false));
        $this->validatorSchema['boarding_place'] = new sfValidatorString(array('required' => $requiredFlag),array('required'=>'Please enter bording place'));
        $this->validatorSchema['flight_carrier'] = new sfValidatorString(array('required' => false));
        $this->validatorSchema['flight_number'] = new sfValidatorString(array('required' => $requiredFlag),array('required'=>'Please enter flight number'));

        
        if($requiredFlag){

            ## Validate address proofs
            for($i=1;$i<=2;$i++){

              if ($this->getObject()->isNew()) {
                  $isRequired = true;
              }else{
                  $isRequired = false;
              }

              if($i == 3 || $i == 4 || $i == 5){
                $isRequired = false;
              }

              $this->validatorSchema['document_'.$i]       = new sfValidatorFile(array(
                                'required'   => $isRequired,
                                'path'       => '',
                                'mime_types' => array(
                                                    'image/jpeg',
                                                    'image/png',
                                                    'image/gif',
                                                    'application/pdf'),
                                'max_size' => '410623',
              ),array('required'=>"Please upload proofs.","mime_types"=>"Please upload JPG/GIF/PNG/PDF images only.","max_size"=>"Proof can not be more then 400 KB."));
            }
    
            $this->widgetSchema['sponsore_type'] =    new sfWidgetFormSelectRadio(array('default'=> 'CS','choices' => array('CS' => 'Company Sponsored Businessman', 'SS' => 'Self Sponsored Businessman')),array('onclick'=>'checkSponsoredBusinessman()'));
            $this->widgetSchema->setLabels(array('sponsore_type'=>'Are you?'));
            $this->widgetSchema->setLabels(array('document_1'=>'International Passport (First page, having photograph)'));

            $argument = $this->getOption('sponsore_type');
            $argument = $this->getObject();
            
            if($argument->sponsore_type == 'SS'){
                 $this->widgetSchema->setLabels(array('document_2'=>'Proof of Funds'));
            }else{
                $this->widgetSchema->setLabels(array('document_2'=>'Letter of Invitation'));
            }        
        }
    }

    private function step2Validation(){

        //Expiry date
        $expiry_date =  strtotime('+ 6 months');
        $purposed_date = strtotime('now');

        $countryId = $this->getOption('applying_country_id');

        if($countryId == 'KE'){
            $requiredFlag = false;
        }else{
            $requiredFlag = true;
        }
        
        $this->widgetSchema['date_of_issue'] = new sfWidgetFormDateCal(array('years'=>WidgetHelpers::getDateRanges()));
        $this->widgetSchema['date_of_exp'] = new sfWidgetFormDateCal(array('years'=>WidgetHelpers::getDateRanges()));

        /**
         * [WP: 089] => Cr: 128
         * Adding type_of_visa according to processing country id...
         */
        $this->widgetSchema['type_of_visa'] = new sfWidgetFormDoctrineChoice(array('model' => 'VisaType','expanded' => 'true','query'=>VisaTypeTable::getVisaTypeCachedQueryForVoap($countryId)));
        
        $this->widgetSchema['trip_money'] = new sfWidgetFormInputText(array('label' => 'How much money do you have for this trip (USD)*(Eg: 100)'));
        $this->widgetSchema['local_company_name'] = new sfWidgetFormInputText(array('label' => 'Company Name (in Nigeria)'));

        $q = Doctrine_Query::create()
           ->from('Country c')
           ->where('c.id != ?', 'NG')
           ->orderBy('c.country_name');

//        $this->widgetSchema['a']  = new sfWidgetFormDoctrineChoice(
//                                                        array('model' => 'Country',
//                                                        'query' => $q, 'label' => 'Country Applying From'));
        
        $this->widgetSchema['applying_country_id']->setOption('query',CountryTable::getSelectedCountryCachedQuery($countryId));

        $countryListArr = array(''=>'-- Please Select --');
        $validcountryListArr = array();
        $countryData = $q->execute()->toArray();

        foreach ($countryData as $k => $v){
            $countryListArr[$v['country_name']] = $v['country_name'];
            $validcountryListArr[$v['country_name']] = $v['country_name'];
        }

        $this->widgetSchema['issusing_govt']  = new sfWidgetFormChoice(array('choices' => $countryListArr));

        $this->widgetSchema->setLabels(array( 'issusing_govt'    => 'Issuing Country'));
        $this->widgetSchema->setLabels(array('date_of_issue' => 'Date of Issue (dd-mm-yyyy)'));
        $this->widgetSchema->setLabels(array('date_of_exp' => 'Expiry Date (dd-mm-yyyy)'));


        $this->validatorSchema['issusing_govt']  = new sfValidatorChoice(array('choices' => $validcountryListArr,'required'=>true),array('required' => 'Issuing country is required.'));

        $this->validatorSchema['applying_country_id'] = new sfValidatorDoctrineChoice(array('model' => 'Country'),array('required' => 'Applying country is required.'));
        $this->validatorSchema['trip_money'] = new sfValidatorString(array('max_length' => 13),array('required' => 'Money in hand is required.','max_length'=>'Money in hand should not be more than 10 digits.'));

        $this->validatorSchema['local_company_name'] = new sfValidatorString(array( 'max_length' => 50,'required'=> $requiredFlag),array('required' => 'Local company name is required.','max_length'=>'Local company name is not more than 50 characters.'));

        $this->validatorSchema['passport_number'] =  new sfValidatorAnd(array(
        new sfValidatorString(array('max_length' => 20),array('required' => 'Passport number is required.','max_length'=>'Passport number can not be more than 20 characters.')),
        new sfValidatorRegex(array('pattern' => '/^[a-zA-Z0-9]*$/'),array('invalid' => 'Passport number is invalid.','required' => 'Passport number is required.'))),
      array('halt_on_error' => true),
      array('required' => 'Passport number is required.')
    );

    $this->validatorSchema['date_of_issue'] = new sfValidatorDate(array('max'=> time()),array('required' => 'Date of issue is required.','max'=>'Passport issue date is not valid.', 'invalid' => 'Date of issue is invalid.'));
    $this->validatorSchema['date_of_exp'] = new sfValidatorDate(array('min' => $expiry_date),array('required' => 'Date of expiry is required.','min' => 'Passport must be valid for at least 6 months.', 'invalid' => 'Date of expiry is invalid.'));
    $this->validatorSchema['place_of_issue'] = new sfValidatorString(array('max_length' => 30),array('required' => 'Place of issue is required.','max_length'=>'Place of issue can not be more than 30 characters.'));
    $this->validatorSchema['mode_of_travel']  = new sfValidatorString(array('max_length' => 64, 'required' => false),array('max_length' => 'Mode of travel can not be more than 64 characters.'));

    $this->validatorSchema['type_of_visa'] = new sfValidatorDoctrineChoice(array('model' => 'VisaType'),array('required' => 'Visa Type is required.'));

      //Embed visa office address Applicant Info Form
      $VapLocalComanyAddress = $this->getObject()->getVapLocalComanyAddress();
      $VapLocalComanyAddressForm = new VapLocalComanyAddressForm($VapLocalComanyAddress, array('requiredFlag' => $requiredFlag));
      $this->embedForm('VapLocalComanyAddressForm', $VapLocalComanyAddressForm);
    }


    private function step1Validation(){

        $countryId = $this->getOption('applying_country_id');
        
        $this->widgetSchema['marital_status'] = new sfWidgetFormChoice(array('choices' => array('Single' => 'Single', 'Married' => 'Married', 'Widowed' => 'Widowed', 'Divorced' => 'Divorced')));
        $this->widgetSchema['hair_color'] = new sfWidgetFormChoice(array('choices' => array('Black' => 'Black', 'Brown' => 'Brown', 'White' => 'White', 'Gray' => 'Gray','Red'=>'Red')));
        $this->widgetSchema['eyes_color'] = new sfWidgetFormChoice(array('choices' => array('Brown' => 'Brown', 'Blue' => 'Blue', 'Green' => 'Green', 'Gray' => 'Gray')));

        ## Set Field Type
        $this->widgetSchema['date_of_birth'] = new sfWidgetFormDateCal(array('years'=>WidgetHelpers::getDateRanges()));
        $this->widgetSchema['arrival_date'] = new sfWidgetFormDateCal(array('years'=>WidgetHelpers::getDateRanges()));


        $this->widgetSchema['title'] = new sfWidgetFormChoice(array('choices' => array('' =>'-- Please Select --','MR' => 'Mr', 'MRS' => 'Mrs', 'MISS' => 'Miss', 'DR' => 'Dr')));
        $this->widgetSchema['gender'] = new sfWidgetFormChoice(array('choices' => array('' =>'-- Please Select --','Male' => 'Male', 'Female' => 'Female')));
        $this->widgetSchema['marital_status'] = new sfWidgetFormChoice(array('choices' => array('' =>'-- Please Select --','Single' => 'Single', 'Married' => 'Married', 'Widowed' => 'Widowed', 'Divorced' => 'Divorced')));
        $this->widgetSchema['present_nationality_id']->setOption('add_empty','-- Please Select --'); //order_by
        $this->widgetSchema['present_nationality_id']->setOption('query',CountryTable::getNonNigeriaCountryList('NG'));
        ##$this->widgetSchema['previous_nationality_id']->setOption('add_empty','-- Please Select --');
        ##$this->widgetSchema['previous_nationality_id']->setOption('query',CountryTable::getNonNigeriaCountryList('NG'));
        $this->widgetSchema['hair_color'] = new sfWidgetFormChoice(array('choices' => array('' =>'-- Please Select --','Black' => 'Black', 'Brown' => 'Brown', 'Gray' => 'Gray','Red'=>'Red', 'White' => 'White')));
        $this->widgetSchema['eyes_color'] = new sfWidgetFormChoice(array('choices' => array('' =>'-- Please Select --','Brown' => 'Brown', 'Blue' => 'Blue', 'Green' => 'Green', 'Gray' => 'Gray')));
        $this->widgetSchema['email'] = new sfWidgetFormInput();
        $this->widgetSchema['business_address'] = new sfWidgetFormTextarea();

        if(!$this->isNew()){
            $isValidEmail = $this->getObject()->getIsEmailValid();
            if($isValidEmail){
                $this->widgetSchema['email']->setAttribute('readonly','readonly');
            }
        }
        //Set Field Label
        $this->widgetSchema->setLabels(
            array(
                      //'visacategory_id'    => 'Visa Category',
                      'type_of_visa'    => 'Visa Type',
                      'title'    => 'Title',
                      'gender'      => 'Gender',
                      'id_marks'      => 'Identification Marks',
                      'height'   => 'Height (in cm)',
                      'eyes_color'   => 'Color of Eyes',
                      'hair_color'   => 'Color of Hair',
                      'marital_status'   => 'Marital Status',
                      'mid_name'   => 'Middle Name',
                      //'employername'   => 'Name of Employer',

                      //'employerphone'   => 'Employer\'s Phone Number',
                      //'perm_phone_no'   => 'Permanent Phone',
                      'office_phone_no'   => 'Office Phone',
                      //'profession'      => 'Profession',
                      'present_nationality_id'      => 'Present Nationality',
                      //'previous_nationality_id'      => 'Previous Nationality',
                      'surname'      => 'Last Name (<i>Surname</i>)',
                      'first_name'      => 'First Name',
                      'middle_name'      => 'Middle Name',
                      'date_of_birth'      => 'Date of Birth (dd-mm-yyyy)',
                      'place_of_birth'      => 'Place of Birth',
                      'business_address'    => 'Corresponding Government Agency in Nigeria (include address)',
                      'arrival_date'      => 'Date of Arrival (dd-mm-yyyy)',


            ));

         # change label of permanent address field in case of Kenya country
        if($countryId == 'KE'){
            $this->widgetSchema->setLabels(array('office_phone_no'   => 'Phone'));
        }

        //Set Maxlength Field Validation
        //$this->widgetSchema['height']->setAttribute('maxlength','3');
        $this->widgetSchema['height']->setAttributes(array('maxlength'=>'3','class'=>'popHeight'));
        //$this->widgetSchema['perm_phone_no']->setAttribute('maxlength','20');
        $this->widgetSchema['office_phone_no']->setAttribute('maxlength','20');
        // Set Field Validation

        $this->validatorSchema['title'] = new sfValidatorChoice(array('choices' => array('MR' => 'MR', 'MRS' => 'MRS', 'MISS' => 'MISS', 'DR' => 'DR')),array('required'=>'Title is required.'));

        $this->validatorSchema['surname'] = new sfValidatorAnd(array(
                new sfValidatorString(array('max_length' => 20),array('required' => 'Last name (<i>Surname</i>) is required.','max_length'=>'Last name (<i>Surname</i>) can not  be more than 20 characters.')),
                new sfValidatorRegex(array('pattern' => sfConfig::get('app_name_exception_pattern')),array('invalid' => 'Last name (<i>Surname</i>) is invalid.','required' => 'Last name (<i>Surname</i>) is required.'))),
            array('halt_on_error' => true),
            array('required' => 'Last name (<i>Surname</i>) is required')
        );
        $this->validatorSchema['middle_name'] = new sfValidatorRegex(array('pattern' => sfConfig::get('app_name_exception_pattern'),'max_length' => 20,
         'required' => false),array('max_length' =>'Middle Name can not  be more than 20 characters.',
         'invalid' =>'Middle name is invalid.' ));
        $this->validatorSchema['first_name'] =  new sfValidatorAnd(array(
                new sfValidatorString(array('max_length' => 20),array('required' => 'First name is required.','max_length'=>'First name can not  be more than 20 characters.')),
                new sfValidatorRegex(array('pattern' => sfConfig::get('app_name_exception_pattern')),array('invalid' => 'First name is invalid.','required' => 'First name is required.'))),
            array('halt_on_error' => true),
            array('required' => 'First name is required')
        );
        $this->validatorSchema['gender'] = new sfValidatorChoice(array('choices' => array('Male' => 'Male', 'Female' => 'Female')),array('required' => 'Gender is required.'));
        $this->validatorSchema['marital_status'] = new sfValidatorChoice(array('choices' => array('Single' => 'Single', 'Married' => 'Married', 'Widowed' => 'Widowed', 'Divorced' => 'Divorced', 'None' => 'None')),array('required'=>'Marital Status is required.'));
        $this->validatorSchema['date_of_birth']  = new sfValidatorDate(array('max'=> time()), array('required' => 'Date of birth is required.','max'=>'Date of birth should be less than today.', 'invalid' => 'Invalid date of birth.'));

        ## Fetching application created date...
        ## This varibale is being used to match arrival date...
        ## Arrival date should not more then 6 months of creating application if not paid...
        ## else updated_at fields will be used.

        //$arrival_update_limit = sfConfig::get('app_visa_arrival_flight_update_limit');
        $no_of_months =  (int)sfConfig::get('app_visa_arrival_flight_update_limit')/30;
        
        /**
         * Again adding ([WP: 080] => CR: 116)
         */
        if(!$this->isNew()){
            $arrival_date_validation = $this->getOption('arrival_date_validation');
            $application_created_at = $this->getOption('application_created_at');
            if($application_created_at != ''){
                list($dates, $hours) = explode(' ', $application_created_at);
                list($year, $month, $day) = explode('-', $dates);                
                //list($hour, $minute, $second) = explode(':', $hours);
                $expiry_arrival_date = mktime(0, 0, 0, $month+$no_of_months, $day, $year);
            }else{
                $expiry_arrival_date =  strtotime('+ '.$no_of_months.' months');
            }
        }else{
            $expiry_arrival_date =  strtotime('+ '.$no_of_months.' months');
            $arrival_date_validation = true;
        }

        $expiry_arrival_date = strtotime('- 1 day', $expiry_arrival_date);

        if($arrival_date_validation == true){
            $minTime = time() - (1 * 24 * 60 * 60);
            $this->validatorSchema['arrival_date'] = new sfValidatorDate(array('min' => $minTime, 'max' => $expiry_arrival_date),array('required' => 'Date of arrival is required.', 'min' => 'Date of arrival can not be past date.', 'max' => 'Date of arrival can not be greater than '.$no_of_months.' months from Today.', 'invalid' => 'Date of arrival is invalid.'));
        }
        
        $this->validatorSchema['place_of_birth'] = new sfValidatorString(array('max_length' => 50),array('required' => 'Place of birth is required.','max_length'=>'Place of birth can not  be more than 50 characters.'));
        $this->validatorSchema['hair_color'] = new sfValidatorChoice(array('choices' => array('Black' => 'Black', 'Brown' => 'Brown', 'White' => 'White', 'Gray' => 'Gray', 'None' => 'None','Red'=>'Red')),array('required' => 'Color of Hair is required.'));
        $this->validatorSchema['eyes_color'] = new sfValidatorChoice(array('choices' => array('Brown' => 'Brown', 'Blue' => 'Blue', 'Green' => 'Green', 'Gray' => 'Gray', 'None' => 'None')),array('required' => 'Color of Eyes is required.'));
        $this->validatorSchema['id_marks'] = new sfValidatorString(array('max_length' => 30),array('required' => 'At least one identification mark is required.',
        'max_length'=>'Identification marks can not  be more than 30 characters.'));
        $this->validatorSchema['present_nationality_id'] = new sfValidatorDoctrineChoice(array('model' => 'Country'),array('required' => 'Present nationality is required.'));
        // $this->validatorSchema['permanent_address'] = new sfValidatorString(array('max_length' => 255),array('required' => 'Permanent address is required.','max_length'=>'Permanent address can not  be more than 255 characters.'));
//        $this->validatorSchema['perm_phone_no'] = new sfValidatorAnd(array(
//                new sfValidatorString(array('max_length' => 20,'min_length' => 6),array('required' => 'Permanent phone is required.','max_length' => 'Phone number can not  be more than 20 digits.','min_length'=>'Phone number should be minimum 6 digits.')),
//                new sfValidatorRegex(array('pattern' => '/^[0-9+-]*$/'),array('invalid' => 'Phone number is not valid.'))
//            ),
//            array('halt_on_error' => true),
//            array('required' => 'Permanent phone is required')
//        );
        $this->validatorSchema['office_phone_no'] = new sfValidatorRegex(array('pattern' => '/^[0-9+-]*$/','max_length' => 20, 'min_length' => 6,
         'required' => false),array('max_length' =>'Office phone can not  be more than 20 digits.',
          'min_length' =>'Office phone is too short(minimum 6 digits).',
          'invalid' =>'Office phone is invalid.' ));
        //$this->validatorSchema['profession'] = new sfValidatorString(array('max_length'=>50),array('max_length'=>'Profession can not  be more than 50 characters.','required'=>'Profession is required.'));

        // $this->validatorSchema['office_address'] = new sfValidatorString(array('max_length' => 255),array('required' => 'Office address is required.','max_length'=>'Office address can not  be more than 255 characters.'));
        //$this->validatorSchema['height'] = new sfValidatorInteger(array('max'=>300,'required' => true),array('max'=>'Height can not be more than 300 cm','invalid' => 'Height is invalid','required'=>'Height is required.'));
        $this->validatorSchema['height'] = new sfValidatorString(array('required'=>'Height is required.'));
        //$this->validatorSchema['milltary_in'] = new sfValidatorString(array('max_length' => 30, 'required' => false),array('max_length'=>'Military id can not  be more than 30 characters.'));
        $this->validatorSchema['email'] = new sfValidatorEmail(array('max_length' => 70, 'required' => true),array('max_length'=>'Email id can not  be more than 70 characters.','invalid'=>'Invalid Email Address','required'=>'Email is required'));

        //Embed visa office address Applicant Info Form
        $visaOfficeAddress = $this->getObject()->getVapVisaOfficeAddress();
        $VapVisaOfficeAddressForm = new VapVisaOfficeAddressForm($visaOfficeAddress);
        $this->embedForm('VapVisaOfficeAddressForm', $VapVisaOfficeAddressForm);


        //Embed visa office address Applicant Info Form
//        $visaPermanentAddress = $this->getObject()->getVapVisaPermanentAddress();
//        $VapVisaPermanentAddressForm = new VapVisaPermanentAddressForm($visaPermanentAddress);
//        $this->embedForm('VapVisaPermanentAddressForm', $VapVisaPermanentAddressForm);

    }



    public function updateNigeriaAddDependents ($formPrefix) {

        if((isset($_POST) && count($_POST)>0) && ($this->getObject() && ($this->getObject()->isNew()))) {

            $curr_country_id = (isset($_POST['vap_application'][$formPrefix]["country_id"]))?$_POST['vap_application'][$formPrefix]["country_id"]:'';
            $curr_state_id = (isset($_POST['vap_application'][$formPrefix]["state"]))?$_POST['vap_application'][$formPrefix]["state"]:'';
            $lga_id = (isset($_POST['vap_application'][$formPrefix]["lga_id"]))?$_POST['vap_application'][$formPrefix]["lga_id"]:'';


            if($curr_country_id) {
                $this->setdefaults(array('country_id' => $curr_country_id));

                if($curr_country_id=='NG'){
                    //$this->widgetSchema[$formPrefix]['state']  = new sfWidgetFormDoctrineSelect(
                    //  array('model' => 'State', 'add_empty' => '-- Please Select --', 'default' => $curr_state_id));
                    $this->widgetSchema[$formPrefix]['state']  = new sfWidgetFormDoctrineChoice(
                        array('default' => $curr_state_id,'model' =>'', 'add_empty' => '-- Please Select --'));
                    $this->widgetSchema[$formPrefix]['state']->setOption('query',StateTable::getCachedQuery());
                }
                else
                {
                    $this->widgetSchema[$formPrefix]['state']  = new sfWidgetFormSelect(array('choices' => array('' =>'-- Please Select --')));
                }
            }

            if($curr_state_id!='') {
                $q = Doctrine_Query::create()
                ->from('LGA L')
                ->where('L.branch_state = ?', $curr_state_id);

                $this->widgetSchema[$formPrefix]['lga_id']  = new sfWidgetFormDoctrineChoice(
                    array('model' => 'LGA',
                  'query' => $q,
                  'add_empty' => '-- Please Select --', 'default' => $lga_id,'label' => 'LGA'));
            } else {
                $this->widgetSchema[$formPrefix]['lga_id']  = new sfWidgetFormSelect(array('choices' => array('' =>'-- Please Select --'),'label' => 'LGA'));
            }
            // if in near future , client provide postalcodes then it will be uncomment
        /*if($lga_id!='') {
          $q = Doctrine_Query::create()->select('P.id, P.district')
          ->from('PostalCodes P')
          ->where('P.lga_id = ?', $lga_id)->execute()->toArray();
          $opt =array();
          foreach ($q as $v){
            $opt[$v['id']] = $v['district'] ;
          }
          $this->widgetSchema[$formPrefix]['district_id']  = new sfWidgetFormSelect(
            array('choices' => $opt,
                  'default' => $district_id,'label' => 'District'));
        } else {
          $this->widgetSchema[$formPrefix]['district_id']  = new sfWidgetFormSelect(array('choices' => array('' =>'-- Please Select --'),'label' => 'District'));
        }
        */
        }
    }

    //Check passport issue date and date of birth
    public function checkIssueDate($validator, $values) {
        $issue_date = $_REQUEST['vap_application']['date_of_issue'];
        $issue_date_year = (($issue_date['year']=="")? '0':$issue_date['year']);
        $issue_date_month = (($issue_date['month']<10)? '0'.$issue_date['month']:$issue_date['month']);
        $issue_date_day = (($issue_date['day']<10)? '0'.$issue_date['day']:$issue_date['day']);

        $issue_date = mktime(0,0,0,$issue_date_month,$issue_date_day,$issue_date_year);
        $issue_date = date("Y-m-d", $issue_date);

        if (($issue_date != '0-0-0') && ($values['date_of_birth'] != '')) {
            // strtotime give previous date as small numeric value like if dates are 2008-07-01 and 2009-07-02   1246386600,1246473000,
            //then the date comarision will be made like 1246386600 > 1246473000
            if(strtotime($issue_date) < strtotime($values['date_of_birth'])){
                $error = new sfValidatorError($validator, 'Date of birth can not be greater than issue date of passport.');
                throw new sfValidatorErrorSchema($validator, array('date_of_birth' => $error));
            }
        }
        return $values;
    }

    public function checkDebortedCountry($validator, $values) {
      if (isset($_REQUEST['vap_application']['deported_status']) && ($_REQUEST['vap_application']['deported_status'] != '') && ($_REQUEST['vap_application']['deported_status'] == 'Yes')) {

        if($_REQUEST['vap_application']['deported_county_id'] =='') {
          $error = new sfValidatorError($validator, 'Deported country is required.');
          throw new sfValidatorErrorSchema($validator, array('deported_county_id' => $error));
        }
      }
      return $values;
    }
}