<?php

class VisaArrivalFlightDetailForm extends VisaArrivalForm
{
    public function configure()
    {
        unset ($this['id'],$this['ref_no'],$this['title'],$this['first_name'],$this['surname'],$this['middle_name'],$this['gender'],$this['previous_nationality_id'],$this['applicant_type'],$this['date_of_birth'],$this['interview_date'],$this['hair_color'],$this['permanent_address_id'],$this['perm_phone_no'],$this['eyes_color'],$this['marital_status'],$this['email'],$this['occupation'],$this['place_of_birth'],$this['ispaid'],$this['payment_trans_id'],$this['height'],$this['processing_country_id'],$this['processing_center_id'],$this['next_kin_address_id'],$this['status'],$this['payment_gateway_id'],$this['paid_dollar_amount'],$this['is_email_valid'],$this['created_at'],$this['updated_at'],$this['present_nationality_id'],$this['id_marks'],$this['office_address_id'],$this['office_phone_no'],$this['vap_company_id'],$this['customer_service_number'],$this['document_1'],$this['document_2'],$this['document_3'],$this['document_4'],$this['document_5'],$this['issusing_govt'],$this['passport_number'],$this['date_of_issue'],$this['date_of_exp'],$this['place_of_issue'],$this['applying_country_id'],$this['type_of_visa'],$this['trip_money'],$this['local_company_name'],$this['local_company_address_id'],$this['contagious_disease'],$this['police_case'],$this['narcotic_involvement'],$this['deported_status'],$this['deported_county_id'],$this['visa_fraud_status'],$this['paid_naira_amount'],$this['paid_date'],$this['profession'],$this['office_address_id'],$this['office_address_id'],$this['applied_nigeria_visa'],$this['nigeria_visa_applied_place'],$this['applied_nigeria_visa_status'],$this['applied_nigeria_visa_reject_reason'],$this['have_visited_nigeria'],$this['visited_reason_type_id'],$this['applying_country_duration'],$this['business_address'],$this['sponsore_type']);

       
//        $this->widgetSchema['id'] = new sfWidgetFormInputHidden();
//        $this->widgetSchema['ref_no'] = new sfWidgetFormInputHidden();
        $this->widgetSchema['boarding_place'] = new sfWidgetFormInputText();
        $this->widgetSchema['flight_number'] = new sfWidgetFormInputText();
        $this->widgetSchema['flight_carrier'] = new sfWidgetFormInputText();
        $this->widgetSchema['arrival_date'] = new sfWidgetFormDateCal(array('years'=>WidgetHelpers::getDateRanges()));


        $this->validatorSchema['flight_carrier'] = new sfValidatorString(array('required' => false),array('required'=>'Please enter flight carrier'));
        $this->validatorSchema['flight_number'] = new sfValidatorString(array('required' => true),array('required'=>'Please enter flight number'));
        $this->validatorSchema['boarding_place'] = new sfValidatorString(array('required' => true),array('required'=>'Please enter bording place'));
//        $expiry_arrival_date =  strtotime('+ 90 days');
//        $this->validatorSchema['arrival_date'] = new sfValidatorDate(array('min' => time(), 'max' => $expiry_arrival_date),array('required' => 'Date of arrival is required.', 'min' => 'Date of arrival can not be past date.', 'max' => 'Date of arrival can not be greater than 90 days of creation application.', 'invalid' => 'Date of arrival is invalid.'));

        ## Fetching application created date...
        ## This varibale is being used to match arrival date...
        ## Arrival date should not more then 90 days of creating application if not paid...
        ## else updated_at fields will be used.
        
            $application_created_at = $this->getOption('application_created_at');
            $arrival_date_validation = $this->getOption('arrival_date_validation');
            $expiry_arrival_days = sfConfig::get('app_visa_arrival_flight_update_limit');
            $no_of_months =  (int)sfConfig::get('app_visa_arrival_flight_update_limit')/30;

            
            
            $dateLabel = $this->getOption('date_label');
            if($application_created_at != ''){
                list($dates, $hours) = explode(' ', $application_created_at);
                list($year, $month, $day) = explode('-', $dates);
                list($hour, $minute, $second) = explode(':', $hours);
                $expiry_arrival_date = mktime($hour, $minute, $second, $month+$no_of_months, $day, $year);
            }else{
                $expiry_arrival_date =  strtotime('+ '.$no_of_months.' months');
            }

            $expiry_arrival_date = strtotime('- 1 day', $expiry_arrival_date);

        if($arrival_date_validation == true){
            $minTime = time() - (1 * 24 * 60 * 60);
            $this->validatorSchema['arrival_date'] = new sfValidatorDate(array('min' => $minTime, 'max' => $expiry_arrival_date),array('required' => 'Date of arrival is required.', 'min' => 'Date of arrival can not be past date.', 'max' => 'Date of arrival can not be greater than '.$no_of_months.' months from '.$dateLabel.' .', 'invalid' => 'Date of arrival is invalid.'));
        }



        $this->widgetSchema->setLabels(array('arrival_date'=> 'Date of Arrival (dd-mm-yyyy)'));
        $this->widgetSchema->setNameFormat('vap_application[%s]');

//        $this->validatorSchema->setPostValidator(new sfValidatorAnd(array(
//          new sfValidatorCallback(array('callback' => array($this, 'checkDebortedCountry'))))
//      ));

    }//End of public function configure()...

//     public function checkDebortedCountry($validator, $values) {
//     echo "<pre>";print_r($values);die('aa');
//      return $values;
//    }

}//End of class VisaArrivalFlightDetailForm...