<?php
/**
 * Displays a Search Application form.
 * @package    form
 * @subpackage PassportApplication
 */
class SearchApplicationForm extends BaseFormStatic
{
  public function configure()
  {

      $this->widgetSchema['application']  = new sfWidgetFormChoice(array('choices'=>array(''=>'-- Please Select --','Visa'=>'Visa','Passport'=>'Passport','Freezone'=>'Freezone','Ecowas'=>'ECOWAS TC','EcowasCard'=>'ECOWAS RC')),array('onChange'=>'showEmail()'));
      $this->widgetSchema['first_name']  = new sfWidgetFormInput();
      $this->widgetSchema['middle_name']  = new sfWidgetFormInput();
      $this->widgetSchema['last_name']  = new sfWidgetFormInput();
      $this->widgetSchema['date_of_birth']  = new sfWidgetFormDateCal(array('years'=>WidgetHelpers::getDateRanges()));
      $this->widgetSchema['email']  = new sfWidgetFormInput();

    $this->validatorSchema['last_name'] = new sfValidatorRegex(array('pattern' => sfConfig::get('app_name_exception_pattern'),'max_length' => 20,
     'required' => false),array('max_length' =>'Last Name can not  be more than 20 characters.',
     'invalid' =>'Last name is invalid.' ));
    
   $this->validatorSchema['middle_name'] = new sfValidatorRegex(array('pattern' => sfConfig::get('app_name_exception_pattern'),'max_length' => 20,
     'required' => false),array('max_length' =>'Middle Name can not  be more than 20 characters.',
     'invalid' =>'Middle name is invalid.' ));

    $this->validatorSchema['first_name'] =  new sfValidatorAnd(array(
          new sfValidatorString(array('max_length' => 20),array('required' => 'Other name is required.','max_length'=>'First name can not  be more than 20 characters.')),
          new sfValidatorRegex(array('pattern' => sfConfig::get('app_name_exception_pattern')),array('invalid' => 'First name is invalid.','required' => 'First name is required.'))),
          array('halt_on_error' => true),
          array('required' => 'First Name is required')
      );
    $this->validatorSchema['email'] = new sfValidatorEmail(array('required'=>false,'max_length'=>50),array('invalid'=>'Email id is invalid','max_length'=>'Email Id can not be more than 50 characters'));
    $this->validatorSchema['date_of_birth']  = new sfValidatorDate(array('max'=> time()), array('required' => 'Date of Birth is required.', 'max'=>'Date of birth can not be future date.'));
    $this->validatorSchema['application'] = new sfValidatorChoice(array('choices' => array('Visa'=>'Visa','Passport'=>'Passport','Freezone'=>'Freezone','Ecowas'=>'Ecowas','EcowasCard'=>'EcowasCard')),array('required' => 'Please Select Application Type.'));

    $this->widgetSchema->setLabels(
      array(
            'first_name'=>'First Name',
            'middle_name'=>'Middle Name',
            'last_name'=>'Last Name',
            'date_of_birth'=>'Date Of Birth',
            'email'=>'Email Id',
            'application'=>'Application Type',

    ));
    $this->widgetSchema->setNameFormat(get_class($this).'[%s]');
  }
}