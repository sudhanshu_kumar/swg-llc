/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 *Function to compare two dates
 **/
function compareDates(st_date,end_date)
{
    var str1  = st_date;
    var str2  = end_date;
    var yr1   = parseInt(str1.substring(0,4),10);
    var mon1  = parseInt(str1.substring(5,7),10);
    var dt1   = parseInt(str1.substring(8,10),10);
    var yr2   = parseInt(str2.substring(0,4),10);
    var mon2  = parseInt(str2.substring(5,7),10);
    var dt2   = parseInt(str2.substring(8,10),10);
    var date1 = new Date(yr1, mon1-1, dt1);
    var date2 = new Date(yr2, mon2-1, dt2);
    if(date2 < date1)
    {
      return false;
    }
    return true;
}
