<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class EpAuditListener{
  public static function processEvent(sfEvent $event){
    $evtData = $event->getSubject();
    if (!self::auditingEnabled($evtData)) {
      return;
    }
    $eventObj = $evtData->getEventObject();
    $eventObj->save();
//    echo "<pre>";
//    print_r($eventObj->toArray(true));die;

  }

  public static function auditingEnabled (EpAuditEventHolder $eventData) {
    if(!sfConfig::has('app_ep_audit_ignore_users')) {
      return true;
    }
    $ignoredUserStr = sfConfig::get('app_ep_audit_ignore_users');
    if (count($ignoredUserStr) == 0) {
      return true;
    }
    //$ignoredUsers = explode(',', $ignoredUserStr);
    $auditedUser = $eventData->getUserName();
    // check if $auditedUser exists in $ignoredUsers - return fals if it does
    foreach($ignoredUserStr as $key=>$value){
      if(strpos($auditedUser,$value) !== false){
        return false;
      }
    }
    return true;
  }
}

?>
