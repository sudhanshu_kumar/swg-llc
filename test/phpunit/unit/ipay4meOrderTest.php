<?php
require_once dirname(__FILE__).'/../bootstrap/unit.php';

//echo dirname(__FILE__).'/../bootstrap/unit.php';

//require_once 'test/phpunit/bootstrap/unit.php';


class unit_ipay4meOrderTest extends sfPHPUnitBaseTestCase
{

  public function SetUp() {
    $configuration = ProjectConfiguration::getApplicationConfiguration('frontend', 'test', true);
    sfContext::createInstance($configuration);
    //new sfDatabaseManager($configuration);
  }

  protected function _start()
  {
    $this->getTest()->diag('test is starting');
  }

  protected function _end()
  {
    $this->getTest()->diag('test is ending');
    echo "\n\n";
  }

 public function testCheckDuplicacy()
 {
      $t = $this->getTest();
      
      $order_number = '131848885112983';
      $return = Doctrine::getTable('ipay4meOrder')->checkDuplicacy($order_number);
      $t->is(true,$return > 0, 'ipay4meOrder->checkDuplicacy($order_number) for real value');

      $order_number = null;
      $return = Doctrine::getTable('ipay4meOrder')->checkDuplicacy($order_number);
      $t->is(true,$return == 0, 'ipay4meOrder->checkDuplicacy($order_number) for null values');

      $order_number = '';
      $return = Doctrine::getTable('ipay4meOrder')->checkDuplicacy($order_number);
      $t->is(true,$return == 0, 'ipay4meOrder->checkDuplicacy($order_number) for blank values');

      $order_number = '1234565454';
      $return = Doctrine::getTable('ipay4meOrder')->checkDuplicacy($order_number);
      $t->is(true,$return == 0, 'ipay4meOrder->checkDuplicacy($order_number) for wrong values');

  } 

  public function testGetOrderRequestDetails()
  {
    $t = $this->getTest();    

    $order_number = '131848885112983';
    $gateway_id = '1';
    $return = Doctrine::getTable('ipay4meOrder')->getOrderRequestDetails($order_number, $gateway_id);    
    $t->is(true, count($return) > 0, 'ipay4meOrder->getOrderRequestDetails($order_number, $gateway_id) for real values');

    $order_number = '131848885112983';
    $gateway_id = '';
    $return = Doctrine::getTable('ipay4meOrder')->getOrderRequestDetails($order_number, $gateway_id);
    $t->is(true, count($return) > 0 , 'ipay4meOrder->getOrderRequestDetails($order_number, $gateway_id) for real values');

    $order_number = 'abcdefgd';
    $gateway_id = '1';
    $return = Doctrine::getTable('ipay4meOrder')->getOrderRequestDetails($order_number, $gateway_id);
    $t->is(true, $return == 0 , 'ipay4meOrder->getOrderRequestDetails($order_number, $gateway_id) for wrong values');

    $order_number = null;
    $gateway_id = null;
    $return = Doctrine::getTable('ipay4meOrder')->getOrderRequestDetails($order_number, $gateway_id);
    $t->is(true, $return == 0 , 'ipay4meOrder->getOrderRequestDetails($order_number, $gateway_id) for null values');
    

    $order_number = '';
    $gateway_id = '';
    $return = Doctrine::getTable('ipay4meOrder')->getOrderRequestDetails($order_number, $gateway_id);
    $t->is(true, $return == 0 , 'ipay4meOrder->getOrderRequestDetails($order_number, $gateway_id) for blank values');
    

    $order_number = '131848885112983';
    $gateway_id = '2';
    $return = Doctrine::getTable('ipay4meOrder')->getOrderRequestDetails($order_number, $gateway_id);
    $t->is(true, $return == 0 , 'ipay4meOrder->getOrderRequestDetails($order_number, $gateway_id) for no values');

  }

 public function testGetReceipt()
 {
      $t = $this->getTest();

      $receiptId = '131848885112983';
      $return = Doctrine::getTable('ipay4meOrder')->getReceipt($receiptId);
      $t->is(true,count($return) > 0, 'ipay4meOrder->getReceipt($receiptId) for real value');

      $receiptId = null;
      $return = Doctrine::getTable('ipay4meOrder')->getReceipt($receiptId);
      $t->is(true,$return == 0, 'ipay4meOrder->getReceipt($receiptId) for null values');

      $receiptId = '';
      $return = Doctrine::getTable('ipay4meOrder')->getReceipt($receiptId);
      $t->is(true,$return == 0, 'ipay4meOrder->getReceipt($receiptId) for blank values');

      $receiptId = '1234565454';
      $return = Doctrine::getTable('ipay4meOrder')->getReceipt($receiptId);
      $t->is(true,$return == 0, 'ipay4meOrder->getReceipt($receiptId) for wrong values');

  }


  public function testGetPurchaseHistory()
  {
      $t = $this->getTest();

      $userId = '162';
      $query = Doctrine::getTable('ipay4meOrder')->getPurchaseHistory($userId);
      $return = $query->execute();
      $t->is(true,count($return) > 0, 'ipay4meOrder->getPurchaseHistory($userId) for real value');


      $userId = '';
      $query = Doctrine::getTable('ipay4meOrder')->getPurchaseHistory($userId);
      $return = $query->execute();
      $t->is(true,count($return) == 0, 'ipay4meOrder->getPurchaseHistory($userId) for blank value');


      $userId = null;
      $query = Doctrine::getTable('ipay4meOrder')->getPurchaseHistory($userId);
      $return = $query->execute();
      $t->is(true,count($return) == 0, 'ipay4meOrder->getPurchaseHistory($userId) for null value');
      

      $userId = '12321';
      $query = Doctrine::getTable('ipay4meOrder')->getPurchaseHistory($userId);
      $return = $query->execute();
      $t->is(true,count($return) == 0, 'ipay4meOrder->getPurchaseHistory($userId) for wrong value');
  }


  public function testGetPaymentReport()
  {
      $t = $this->getTest();
      
      $from_date = '';
      $to_date = '';
      $oder_no = '131848885112983';
      $bill_no = '';
      $query = Doctrine::getTable('ipay4meOrder')->getPaymentReport($from_date, $to_date, $oder_no, $bill_no);
      $return = $query->execute();      
      $t->is(true,count($return) > 0, 'ipay4meOrder->getPaymentReport($from_date, $to_date, $oder_no, $bill_no) for real value');

  }


  public function testFindSuccessfulPaymentDetail()
  {
      $t = $this->getTest();

      $orderId = '2547';
      $return = Doctrine::getTable('ipay4meOrder')->findSuccessfulPaymentDetail($orderId);      
      $t->is(true, is_array($return), 'ipay4meOrder->findSuccessfulPaymentDetail($orderId');

      $orderId = '254777';
      $return = Doctrine::getTable('ipay4meOrder')->findSuccessfulPaymentDetail($orderId);
      $t->is(false, $return, 'ipay4meOrder->findSuccessfulPaymentDetail($orderId');

      $orderId = '';
      $return = Doctrine::getTable('ipay4meOrder')->findSuccessfulPaymentDetail($orderId);
      $t->is(false, $return, 'ipay4meOrder->findSuccessfulPaymentDetail($orderId');

      $orderId = null;
      $return = Doctrine::getTable('ipay4meOrder')->findSuccessfulPaymentDetail($orderId);
      $t->is(false, $return, 'ipay4meOrder->findSuccessfulPaymentDetail($orderId');

      $orderId = 'ss2547';
      $return = Doctrine::getTable('ipay4meOrder')->findSuccessfulPaymentDetail($orderId);
      $t->is(false, $return, 'ipay4meOrder->findSuccessfulPaymentDetail($orderId');

  }


  public function testPaymentDetails(){
      $t = $this->getTest();

      $orderNumber = '131848885112983';
      $return = Doctrine::getTable('ipay4meOrder')->paymentDetails($orderNumber);
      $t->is(true, is_array($return), 'ipay4meOrder->paymentDetails($orderNumber');

      $orderNumber = '23232323232323';
      $return = Doctrine::getTable('ipay4meOrder')->paymentDetails($orderNumber);
      $t->is(true, $return == 0, 'ipay4meOrder->paymentDetails($orderNumber');

      $orderNumber = '';
      $return = Doctrine::getTable('ipay4meOrder')->paymentDetails($orderNumber);
      $t->is(true, $return == 0, 'ipay4meOrder->paymentDetails($orderNumber');

      $orderNumber = null;
      $return = Doctrine::getTable('ipay4meOrder')->paymentDetails($orderNumber);
      $t->is(true, $return == 0, 'ipay4meOrder->paymentDetails($orderNumber');
      
  }
  

  public function testGetNoOfCardUsed(){
      $t = $this->getTest();

      $card_Num = '4111111111111111';
      $timeInMinutes = '';
      $return = Doctrine::getTable('ipay4meOrder')->getNoOfCardUsed($card_Num, $timeInMinutes);
      $t->is(true, $return < 1, 'ipay4meOrder->getNoOfCardUsed($card_Num, $timeInMinutes');
  }

  
  
  
}